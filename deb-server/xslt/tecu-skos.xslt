<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:skos="http://www.w3.org/2004/02/skos/core" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns">
    <xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:param name="perm"/>
<xsl:param name="type"/>
<xsl:param name="servername"/>

<xsl:template match="/">
  <skos:Collection>
    <xsl:apply-templates />
  </skos:Collection>
</xsl:template>

<xsl:template match="entry">
  <skos:Concept rdf:about="{$servername}/tezaurus/term/{@id}">
    <skos:prefLabel><xsl:value-of select="terms/term[@lang='cz'][1]"/></skos:prefLabel>
    <xsl:for-each select="terms/term[position()>1]">
      <skos:altLabel xml:lang="{@lang}"><xsl:value-of select="."/></skos:altLabel>
    </xsl:for-each>
    <xsl:for-each select="hyper">
      <skos:broader rdf:resource="{$servername}/tezaurus/term/{@id}"/>
    </xsl:for-each>
    <xsl:for-each select="exlinks/exlink">
      <skos:related rdf:resource="{.}"/>
    </xsl:for-each>
    <xsl:for-each select="defs/def">
      <skos:definition><xsl:value-of select="text"/></skos:definition>
    </xsl:for-each>

  </skos:Concept>
</xsl:template>

<xsl:template match="term" mode="trans">
  <div class="tr box-line">
    <!--<span class="badge badge-trans"><xsl:value-of select="@lang"/></span><xsl:text> </xsl:text><a target="concordance" href="https://makaria07.fi.muni.cz/bonito/run.cgi/first?corpname=tecu_{@lang}&amp;iquery={./text()}" title="Otevřít v korpusu"><xsl:value-of select="."/></a>-->
    <span class="badge badge-trans"><xsl:value-of select="@lang"/></span><xsl:text> </xsl:text><xsl:value-of select="."/>
     <xsl:if test="@morf!=''"> 
      (<xsl:value-of select="@morf"/>)
    </xsl:if>
  </div>
</xsl:template>

<xsl:template match="domains">
   <div class="box-heading">
    <i class="fa fa-map-signs"></i> Obory
   </div>
   <div class="box-inside">
    <ul>
      <xsl:apply-templates select="dom"/>
    </ul>
  </div>
</xsl:template>

<xsl:template match="defs">   
  <ol>
    <xsl:apply-templates select="def"/>
  </ol>
</xsl:template>

<xsl:template match="refs">
  <div class="row row-pad">
    <div class="col-lg-12 refs">      
       <div class="box-heading">
        <span class="glyphicon glyphicon-book"> </span>
         Reference
       </div>
       <div class="box-inside">
        <ul>
          <xsl:apply-templates select="ref"/>
        </ul>          
      </div>      
    </div>
  </div>
</xsl:template>

<xsl:template match="notes">
  <div class="row row-pad">
    <div class="col-lg-12 notes">
      <div class="notes box">
         
         <div class="box-heading">
          <i class="fa fa-history"></i> 
          Historie editace
         </div>

         <div id="notes-detail" class="hide-this box-inside">
           <xsl:apply-templates select="note"/>
        </div>

        <div class="show-hide-button" onclick="showHide($(this))">
          <span class="glyphicon glyphicon-chevron-down"></span> Zobrazit
        </div>

      </div>
    </div>
  </div>
</xsl:template>

<xsl:template match="alsos">
   <div class="col-lg-6 alsos row-pad">
       <div class="box-heading second-level">                
          <i class="fa fa-thumb-tack"></i>
          Také
       </div>
       <div class="box-inside second-level label-center">

       <xsl:for-each select="also">
           <xsl:if test="not(@entry-id) or @entry-id=/entry/@id">
            <div class="also label-box label-nonactive"> 
             <xsl:value-of select="."/>
            </div>
           </xsl:if>
           
           <xsl:if test="@entry-id!='' and @entry-id!=/entry/@id">
             <a class="label-box label-active" href="tezaurus.html?id={@entry-id}&amp;opentree=true"><xsl:value-of select="."/></a>            
           </xsl:if>
         
       </xsl:for-each>

      </div>
   </div>
</xsl:template>

<xsl:template match="sees">
    <div class="col-lg-6 sees row-pad">
	<div class="box-heading second-level">
            <i class="fa fa-thumb-tack"></i>
            Viz
         </div>
         <div class="box-inside second-level label-center">
            
            <xsl:for-each select="see">              
                <xsl:if test="not(@entry-id) or @entry-id=/entry/@id">
                  <span class="see label-box label-nonactive"> 
                    <xsl:value-of select="."/>
                  </span>
                </xsl:if>
            
              <xsl:if test="@entry-id!='' and @entry-id!=/entry/@id">
                  <span class="see label-box label-active"> 
                  <a href="tezaurus.html?id={@entry-id}&amp;opentree=true"><xsl:value-of select="."/></a>
                </span>
              </xsl:if>
            
            </xsl:for-each>
        </div>
    </div>
</xsl:template>

<xsl:template match="hyper">
         <a class="hyperlink" href="#" onclick="get_id_html(this); return false;" data-id="{@id}"><xsl:value-of select="@term"/></a>
     <xsl:choose><xsl:when test="position() != last()">; </xsl:when></xsl:choose>
</xsl:template>

<xsl:template match="path_node">  
    <a class="hyperlink label-box label-active" data-id="{@id}" href="tezaurus.html?id={@id}&amp;opentree=true"><xsl:value-of select="@term"/></a>
    <xsl:choose>
      <xsl:when test="position() != last()">        
        <span class="label-triangle glyphicon glyphicon-triangle-bottom"></span>
      </xsl:when>
    </xsl:choose>
</xsl:template>

<xsl:template match="def">
    <li class="def">
      <xsl:apply-templates/>
    </li>
</xsl:template>

<xsl:template match="def/a">
  <a href="{@href}&amp;opentree=true"><xsl:value-of select="text()"/></a>
</xsl:template>

<xsl:template match="dom">  
  <li class="dom box-line"><xsl:value-of select="."/></li>  
</xsl:template>

<xsl:template match="ref">
  <li><xsl:value-of select="."/></li>

</xsl:template>

<xsl:template match="note">
  <div class="row">
    
    <div class="col-lg-2">
      <span class="note label label-default pull-right">
        <span class="glyphicon glyphicon-user"> </span> <xsl:value-of select="@author"/>        
      </span>
    </div>

    <div class="col-lg-3">
      <span class="glyphicon glyphicon-time"> </span> <xsl:value-of select="@time"/>
    </div>

    <div class="col-lg-7">
      <span class="glyphicon glyphicon-comment"> </span> <xsl:value-of select="."/>
    </div>

  </div>
  
</xsl:template>


<xsl:template name="split">
  <xsl:param name="text" select="."/>
  <xsl:param name="separator" select="' '"/>
  <xsl:choose>
    <xsl:when test="not(contains($text, $separator))">
      [lc="<xsl:value-of select="normalize-space($text)"/>"|lemma_lc="<xsl:value-of select="normalize-space($text)"/>"]
    </xsl:when>
    <xsl:otherwise>
      [lc="<xsl:value-of select="normalize-space(substring-before($text, $separator))"/>"|lemma_lc="<xsl:value-of select="normalize-space(substring-before($text, $separator))"/>"]
      <xsl:call-template name="split">
        <xsl:with-param name="text" select="substring-after($text, $separator)"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


</xsl:stylesheet>
