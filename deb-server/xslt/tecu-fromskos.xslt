<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <xsl:output encoding="utf-8" method="xml"/>
  <xsl:template match="/">
    <tecu skos_count="{count(//skos:Concept)}">
      <xsl:apply-templates select="//skos:Concept" />
    </tecu>
  </xsl:template>
  <xsl:template match="skos:Concept">
    <entry id="{@rdf:about}" fromskos="{@fromskos}">
      <terms>
        <xsl:for-each select="skos:prefLabel"><term lang="{@xml:lang}"><xsl:value-of select="."/></term></xsl:for-each>
      </terms>
      <synonyms>
        <xsl:for-each select="skos:altLabel"><synonym><xsl:value-of select="."/></synonym></xsl:for-each>
      </synonyms>
      <exlinks>
        <xsl:for-each select="skos:related"><exlink><xsl:value-of select="@rdf:resource"/></exlink></xsl:for-each>
      </exlinks>
      <xsl:for-each select="skos:broader">
        <!--<xsl:variable name="resid" select="@rdf:resource" />
        <xsl:for-each select="//skos:Concept[@rdf:about=$resid]">
          <hyper><xsl:value-of select="skos:prefLabel[1]"/></hyper>
        </xsl:for-each>-->
        <hyper id="{@rdf:resource}"/>
      </xsl:for-each>
      <defs>
        <xsl:for-each select="skos:definition"><def><xsl:value-of select="."/></def></xsl:for-each>
      </defs>
    </entry>
  </xsl:template>

</xsl:stylesheet>
