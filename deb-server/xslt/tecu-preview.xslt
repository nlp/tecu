<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:param name="perm"/>
<xsl:param name="type"/>
<xsl:param name="local"/>

<xsl:template match="/">
  <html>
    <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" charset="utf-8"/>
<meta charset="utf-8"/>
<link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet"/>
<link href="/editor/preview.css" rel="stylesheet"/>
<xsl:if test="$local='true'">
<link href="file:///var/lib/deb-server/files/tecu/preview.css" rel="stylesheet" />
</xsl:if>
    </head>
    <body>
      <xsl:if test="/tecu/@skos_count=0">Termín nenalezen v externích SKOS tezaurech.</xsl:if>
      <xsl:apply-templates/>
    </body>
  </html>
</xsl:template>
<xsl:template match="entry">
  <div class="ui text container">
    <h2 class="ui header"><xsl:value-of select="terms/term[@lang='cz'][1]"/></h2>
    <br/>
  </div>
  <xsl:if test="@fromskos!=''">
    <div class="ui text container">
      <h3 class="ui header">SKOS tezaurus <xsl:value-of select="@fromskos"/></h3>
      <br/>
    </div>
  </xsl:if>
  <div style="text-align:left">
    <h3 class="ui top attached header"><i aria-hidden="true" class="translate icon"></i>Překlady</h3>
    <br/>
    <div class="ui bottom attached segment">
      <xsl:apply-templates select="terms/term"/>
    </div>
    <xsl:if test="$type!='short'">
      <h3 class="ui top attached header"><i aria-hidden="true" class="comment alternate icon"></i>Definice</h3>
      <br/>
      <div class="ui bottom attached segment">
        <ol class="defs" style="">
          <xsl:apply-templates select="defs/def"/>
        </ol>
      </div>
    </xsl:if>
    <h3 class="ui top attached header"><i aria-hidden="true" class="linkify icon"></i>Odkazy</h3>
    <br/>
    <div class="ui attached segment">
      <h3 class="ui header">Nadřazené pojmy</h3>
      <br/>
      <xsl:for-each select="paths/path">
        <div class="ui compact segment">
          <xsl:for-each select="path_node">
            <div><xsl:if test="position()>1"><i class="angle down icon"/></xsl:if><a class="sees-node" href="/tezaurus/term/{@id}"><xsl:value-of select="@term"/></a></div>
          </xsl:for-each>
        </div>
      </xsl:for-each>
    </div>
    <div class="ui bottom attached segment">
      <xsl:if test="synonyms/synonym">
        <h3 class="ui header">Synonyma</h3>
        <br/>
        <xsl:for-each select="synonyms/synonym">
          <xsl:if test="@entry-id">
            <a class="sees-node" href="/tezaurus/term/{@entry-id}"><xsl:value-of select="."/></a>
          </xsl:if>
          <xsl:if test="not(@entry-id)">
            <span class="newSees-node"><xsl:value-of select="."/></span>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="antonyms/antonym">
        <h3 class="ui header">Antonyma</h3>
        <br/>
        <xsl:for-each select="antonyms/antonym">
          <xsl:if test="@entry-id">
            <a class="sees-node" href="/tezaurus/term/{@entry-id}"><xsl:value-of select="."/></a>
          </xsl:if>
          <xsl:if test="not(@entry-id)">
            <span class="newSees-node"><xsl:value-of select="."/></span>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="homonyms/homonym">
        <h3 class="ui header">Homonyma</h3>
        <br/>
        <xsl:for-each select="homonyms/homonym">
          <xsl:if test="@entry-id">
            <a class="sees-node" href="/tezaurus/term/{@entry-id}"><xsl:value-of select="."/></a>
          </xsl:if>
          <xsl:if test="not(@entry-id)">
            <span class="newSees-node"><xsl:value-of select="."/></span>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>
    </div>
    <xsl:if test="imgs/img">
      <h3 class="ui top attached header"><i aria-hidden="true" class="paint brush icon"></i>Obrázky</h3>
      <br/>
      <div class="ui bottom attached segment">
        <xsl:for-each select="imgs/img">
          <div><a href="{.}" target="_blank"><img src="{.}"/></a></div>
        </xsl:for-each>
      </div>
    </xsl:if>
    <xsl:if test="videos/video">
      <h3 class="ui top attached header"><i aria-hidden="true" class="file video outline icon"></i>Videa</h3>
      <br/>
      <div class="ui bottom attached segment">
        <xsl:for-each select="videos/video">
          <div><a href="{.}" target="_blank"><xsl:value-of select="."/></a></div>
        </xsl:for-each>
      </div>
    </xsl:if>
    <xsl:if test="exlinks/exlink">
      <h3 class="ui top attached header"><i aria-hidden="true" class="globe icon"></i>Odkazy na externí tezaury</h3>
      <br/>
      <div class="ui bottom attached segment">
        <xsl:for-each select="exlinks/exlink">
          <div><a href="{.}" target="_blank"><xsl:value-of select="."/></a></div>
        </xsl:for-each>
      </div>
    </xsl:if>
  </div>
</xsl:template>

<xsl:template match="term">
  <p><i title="{@lang}" class="{@lang} flag"></i><xsl:value-of select="."/><xsl:if test="@pron!=''"><span style="opacity: 0.6; font-weight: bold;"> [<i><xsl:value-of select="@pron"/></i>]</span></xsl:if><xsl:if test="@note!=''"><span style="opacity: 0.4;"> (<xsl:value-of select="@note"/>)</span></xsl:if></p>
</xsl:template>

<xsl:template match="def">
  <li>
    <p><i title="{@lang}" class="{@lang} flag" style="margin: 0px 4px 0px 2px;"/><xsl:value-of select="text"/></p>
    <xsl:if test="zdroj!=''"><p><b>Zdroj/citace:</b> <xsl:value-of select="zdroj"/></p></xsl:if>
    <xsl:if test="law!=''"><p><b>Zdroj ze zákona:</b> <xsl:value-of select="law"/></p></xsl:if>
    <xsl:if test="druh!=''"><p><b>Druh definice:</b> <xsl:value-of select="druh"/></p></xsl:if>
    <xsl:if test="type!=''"><p><b>Typ definice:</b> <xsl:value-of select="type"/></p></xsl:if>
    <xsl:if test="druh!=''"><p><b>Druh definice:</b> <xsl:value-of select="druh"/></p></xsl:if>
    <xsl:if test="@obor!=''"><p><b>Obor:</b> <xsl:value-of select="@obor"/></p></xsl:if>
    <xsl:if test="math!=''"><p><b>Vzorec:</b> <img style="height:1.4em" id="math{position()}" src="data:image/png;base64,{math_base64}"/></p>
    </xsl:if>
    <xsl:if test="table!=''"><p><b>Tabulka:</b> <img style="height:3em" id="table{position()}" src="data;image/png;base64,{table_base64}"/></p>
    </xsl:if>
  </li>
</xsl:template>

<xsl:template name="split">
  <xsl:param name="text" select="."/>
  <xsl:param name="separator" select="' '"/>
  <xsl:choose>
    <xsl:when test="not(contains($text, $separator))">
      [lc="<xsl:value-of select="normalize-space($text)"/>"|lemma_lc="<xsl:value-of select="normalize-space($text)"/>"]
    </xsl:when>
    <xsl:otherwise>
      [lc="<xsl:value-of select="normalize-space(substring-before($text, $separator))"/>"|lemma_lc="<xsl:value-of select="normalize-space(substring-before($text, $separator))"/>"]
      <xsl:call-template name="split">
        <xsl:with-param name="text" select="substring-after($text, $separator)"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


</xsl:stylesheet>
