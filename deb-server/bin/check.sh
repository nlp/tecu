#!/bin/bash

server=$1
result=`lwp-request -s -d -t 50 $server`
code=${result:0:3}

if [ $code == "200" ]; then
  exit 0
else
  exit 2
fi

