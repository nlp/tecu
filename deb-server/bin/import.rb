#!/usr/bin/ruby1.8
$LOAD_PATH.unshift('/var/lib/deb-server/lib')

require 'import'
require 'optparse'

delete = false
overwrite = false
source_xml = ''
collection = ''
key = ''
root = ''
log_file = ''

ARGV.options do |p|
  p.banner = "usage: #{$0} [options]\n"
  p.on('-x','--source-xml=PATH', String, 'source XML file') { |source_xml| }
  p.on('-c','--collection=DB', String, 'database collection code') { |collection| }
  p.on('-k','--key=XPATH', String, 'database unique key XPath') { |key| }
  p.on('-r','--root=ELEMENT',String, 'name of root element') { |root| }
  p.on('-d','--delete', 'delete original data') { |d| delete = true }
  p.on('-o','--overwrite', 'overwrite existing entries') { |o| overwrite = true }
  p.on('-l','--logfile=FILE',String,'log file') { |log_file| }

  begin
    p.parse!(ARGV)
  rescue
    puts p
    exit(1)
  end
end

if source_xml == ''
  puts 'Specify source XML file'
end

if collection == ''
  puts 'Specify database collection'
end

if key == ''
  puts 'Specify database entry XPath'
end

if root == ''
  puts 'Specify entry root element'
end

if source_xml == '' or collection == '' or key == '' or root == ''
  exit(1)
end

if log_file == ''
  log_file = '/var/log/deb-server/'+collection+Time.now.strftime('%Y%m%d-%H%M') +'.log'
end


puts 'Import started ' + source_xml +' to collection '+collection
puts 'Log file: '+log_file
import(collection, source_xml, log_file, key, root, delete, overwrite)

