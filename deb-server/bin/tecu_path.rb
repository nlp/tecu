#!/usr/bin/ruby
require 'rubygems'
require 'xml/libxml'
require 'sedna'
require 'json'

  def get(key)
    begin
      res = @sedna.query("doc('"+key+"', 'tecu')")
      xml = res[0].gsub!('<?xml version="1.0" standalone="yes"?>','')
      return xml
    rescue
      return ''
    end
  end

  def get_path(id)
    path = []
    xml = get(id)
    if xml != ''
      path = get_up(id, [[]], 0, 0)
    end
    return path.sort{|a,b| b.length<=>a.length}
  end
  def get_up(id, path_array, path_num, max_ar)
    xml = get(id)
    return path_array if xml == ''
    doc = XML::Document.string(xml, :encoding => XML::Encoding::UTF_8)
    paths = {}
    offset = 0
    doc.root.find('//hyper').each{|h|
      if offset == 0
        paths[h['id']] = path_num
      else
        max_ar += 1
        path_array[max_ar] = path_array[path_num].clone
        paths[h['id']] = max_ar
      end
      offset += 1
    }

    if doc.root.find('//hyper').length > 0
      doc.root.find('//hyper').each{|h|
        act_path = paths[h['id']]
        next if path_array[act_path].include?(h['id'])
        path_array[act_path] << h['id']
        path_array = get_up(h['id'].to_s, path_array, act_path, max_ar)
      }
    else
      path_array[path_num] << '0'
    end
    return path_array
  end

@sedna = Sedna.connect({:database=>'deb'})
exit if ARGV[0].nil?
puts get_path(ARGV[0]).to_json
