#!/bin/bash
cd /var/lib/deb-server/client/tezaurus
git reset --hard origin/master && git pull
/var/lib/deb-server/bin/tecu_design.rb
cd /var/lib/deb-server/client/tezaurus
npm run b
rm -rf /var/lib/deb-server/files/tezaurus/*
cp -r build/* /var/lib/deb-server/files/tezaurus
