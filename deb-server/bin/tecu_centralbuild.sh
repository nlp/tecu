#!/bin/bash
cd /var/lib/deb-server/client/tezaurus
git reset --hard origin/master && git pull
npm run bc
rm -rf /var/lib/deb-server/files/central/*
cp -r build/* /var/lib/deb-server/files/central
