#!/bin/bash

#check if Sedna governor is running
se_rc
if [ $? -ne 0 ] ; then
  se_gov 
fi

#try to run 'deb' database
result=`se_sm deb 2>&1`
#if not successful, create it first
if [[ "$result" =~ "no database" ]] ; then
  se_cdb deb
  se_sm deb
fi

if [ `se_term -query 'count(fn:doc("$collections")//collection[@name="admininfo"])' deb` -eq "0" ] ; then
  se_term -query 'CREATE COLLECTION "admininfo"' deb
fi
if [ `se_term -query 'count(fn:doc("$collections")//collection[@name="tecu_user"])' deb` -eq "0" ] ; then
  se_term -query 'CREATE COLLECTION "tecu_user"' deb
fi
if [ `se_term -query 'count(fn:doc("$collections")//collection[@name="tecu"])' deb` -eq "0" ] ; then
  se_term -query 'CREATE COLLECTION "tecu"' deb
fi
if [ `se_term -query 'count(fn:doc("$collections")//collection[@name="tecudel"])' deb` -eq "0" ] ; then
  se_term -query 'CREATE COLLECTION "tecudel"' deb
fi
if [ `se_term -query 'count(fn:doc("$collections")//collection[@name="tecuwork"])' deb` -eq "0" ] ; then
  se_term -query 'CREATE COLLECTION "tecuwork"' deb
fi
if [ `se_term -query 'count(fn:doc("$collections")//collection[@name="registr"])' deb` -eq "0" ] ; then
  se_term -query 'CREATE COLLECTION "registr"' deb
fi
if [ `se_term -query 'count(fn:doc("$collections")//collection[@name="settings"])' deb` -eq "0" ] ; then
  se_term -query 'CREATE COLLECTION "settings"' deb
fi
if [ `se_term -query 'count(fn:doc("$collections")//collection[@name="thesaurus"])' deb` -eq "0" ] ; then
  se_term -query 'CREATE COLLECTION "thesaurus"' deb
fi
if [ `se_term -query 'count(fn:doc("$collections")//collection[@name="tecuhist"])' deb` -eq "0" ] ; then
  se_term -query 'CREATE COLLECTION "tecuhist"' deb
fi

for file in /var/lib/deb-server/init/tecu/[dict,service]*xml; do
  x=${file##/var*/}
  xml=${x%%.xml}
  if [ `se_term -query 'count(fn:doc("$documents")/documents/collection[@name="admininfo"]/document[@name="'$xml'"])' deb` -eq 0 ] ; then
    se_term -query 'LOAD "'$file'" "'$xml'" "admininfo"' deb
  else
    echo $xml" already initialized"
  fi
done
for file in /var/lib/deb-server/init/tecu/user*xml; do
  x=${file##/var*/user}
  xml=${x%%.xml}
  if [ `se_term -query 'count(fn:doc("$documents")/documents/collection[@name="tecu_user"]/document[@name="'$xml'"])' deb` -eq 0 ] ; then
    se_term -query 'LOAD "'$file'" "'$xml'" "tecu_user"' deb
  else
    echo $xml" already initialized"
  fi
done
