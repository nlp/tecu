#!/bin/bash

debdir=/var/lib/deb-server
if [ -f $debdir/.norestart ]
then
  exit
fi

$debdir/bin/check.sh http://deb:deb@127.0.0.1:8002/hello
if [ $PIPESTATUS != 0 ]; then
  sudo /etc/init.d/debserver-common restart tecu
fi

$debdir/bin/check.sh http://127.0.0.1:8001/hello
if [ $PIPESTATUS != 0 ]; then
  sudo /etc/init.d/debserver-common restart tecupublic
fi

