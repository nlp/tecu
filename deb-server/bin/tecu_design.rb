#!/usr/bin/ruby
require 'rubygems'
require 'xml/libxml'
require 'sedna'
require 'base64'

sedna = Sedna.connect({:database=>'deb'})
xml = sedna.query('doc("design","settings")')[0]
css = {'mainColor' => 'black',
'mainBckg' => '#F5F5F5',
'segmentBckg' => 'white',
'menuColor' => 'white',
'menuBckg' => '#2362A2',
'blueTreeNode' => '#337DBD',
'visColor' => '#ECAE1A',
'positive' => '#6FBD2C',
'focusColor' => '#DD9F0C',
'error' => '#E04857'}

if xml != ''
  doc = XML::Document.string(xml, :encoding => XML::Encoding::UTF_8)
  css['mainColor'] = doc.find('mainColor').first.content.to_s if not doc.find('mainColor').first.nil?
  css['mainBckg'] = doc.find('mainBckg').first.content.to_s if not doc.find('mainBckg').first.nil?
  css['segmentBckg'] = doc.find('segmentBckg').first.content.to_s if not doc.find('segmentBckg').first.nil?
  css['menuColor'] = doc.find('menuColor').first.content.to_s if not doc.find('menuColor').first.nil?
  css['menuBckg'] = doc.find('menuBckg').first.content.to_s if not doc.find('menuBckg').first.nil?
  css['blueTreeNode'] = doc.find('blueTreeNode').first.content.to_s if not doc.find('blueTreeNode').first.nil?
  css['visColor'] = doc.find('visColor').first.content.to_s if not doc.find('visColor').first.nil?
  css['positive'] = doc.find('positive').first.content.to_s if not doc.find('positive').first.nil?
  css['focusColor'] = doc.find('focusColor').first.content.to_s if not doc.find('focusColor').first.nil?
  css['error'] = doc.find('error').first.content.to_s if not doc.find('error').first.nil?
  if not doc.find('logo').first.nil?
    logo64 = doc.find('logo').first.content.to_s
    logodata = Base64.decode64(logo64)
    File.write('/var/lib/deb-server/client/tezaurus/src/assets/img/logo.png', logodata)
  end
end
cssfile = ''
css.each{|k,v|
  cssfile += '$' + k + ': ' + v + ";\n"
}
File.write('/var/lib/deb-server/client/tezaurus/src/styles/_variables.scss', cssfile)
system("cd /var/lib/deb-server/client/tezaurus/; sass src/styles/style.scss src/styles/style.css")
#system("cd /home/builder/tezaurus; npm run b && cp -r build/* /var/lib/deb-server/files/tezaurus")
