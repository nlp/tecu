#!/usr/bin/ruby

require 'optparse'
require 'webrick'

$LOAD_PATH.unshift('../lib/')

class Object
  def edump(msg='')
    $stderr.puts("edump #{msg}: " + self.to_s)
    return self
  end
end

class FileTecu < WEBrick::HTTPServlet::FileHandler
  def set_filename(req, res)
    res.filename = @root.dup
    path_info = req.path_info.scan(%r|/[^/]*|)

    path_info.unshift("")  # dummy for checking @root dir
    while base = path_info.first
      break if base == "/"
      break unless File.directory?(File.expand_path(res.filename + base))
      shift_path_info(req, res, path_info)
      call_callback(:DirectoryCallback, req, res)
    end

    if base = path_info.first
      if base == "/"
        if file = search_index_file(req, res)
          shift_path_info(req, res, path_info, file)
          call_callback(:FileCallback, req, res)
          return true
        end
        shift_path_info(req, res, path_info)
      elsif file = search_file(req, res, base)
        shift_path_info(req, res, path_info, file)
        call_callback(:FileCallback, req, res)
        return true         
      else                  
        indexfile = '/var/lib/deb-server/files/central/index.html'
        st = File::stat(indexfile)
        res['content-type'] = 'text/html'
        res['content-length'] = st.size.to_s
        res.body = File.read(indexfile)
        res.filename = indexfile
        return true
      end
    end

    return false
  end

end



require 'logger'
require 'rexml/document'
require 'socket'
require 'cgi'
require 'openssl'
require 'webrick'
require 'rubygems'
require 'xml/libxml'
require 'xml/xslt'
require 'sedna'
require 'twitter_cldr'
require 'base64'
require 'net/smtp'


# DEB modules
require 'deb_common'
require 'http_server'
require 'dbpasswd'
require 'dict/dict-sedna'
require 'dict/admin'
require 'dict/tezcs'
require 'dict/tecu'
require 'servlets/dict'
require 'servlets/hello'
require 'servlets/admin'
require 'servlets/central'
require 'servlets/tecu'

server = nil

port      = 8001
daemon    = nil
log_level = Logger::DEBUG
log_file  = nil
db_path   = nil
xslt_path = nil
base_path = '/var/lib/deb-server'
ssl       = false
auth      = false
ssl_ca_cert = nil
database = 'deb'

debug_levels = {
  :debug => Logger::DEBUG,
  :info  => Logger::INFO,
  :warn  => Logger::WARN,
  :error => Logger::ERROR,
  :fatal => Logger::FATAL,
}

ARGV.options do |p|
  p.banner = "usage: #{$0} [options]\n"
  p.on('-p','--port=NUMBER',    String, 'cislo portu')      { |p| port=p.to_i }
  p.on('--db-path=PATH',        String, 'cesta k databazim') { |v| db_path=v}
  p.on('--xslt-path=PATH',      String, 'cesta k sablobnam') { |v| xslt_path=v }
  p.on('--base-path=PATH',      String, 'cesta k zakladnimu adresari') { |v| base_path=v }
  p.on('-d','--daemon',                 'daemonizovat') { |d| daemon = true }
  p.on('-s','--ssl',                    'use SSL') { |s| ssl = true }
  p.on(     '--ssl-cert=STRING',String, 'ssl cert/key base') { |v| ssl_cert_base=v }
  p.on(     '--ssl-ca-cert=STRING',String, 'ssl CA certificate') { |v| ssl_ca_cert=v }
  p.on(     '--auth',                   'use test auth') { |a| auth = true }
  p.on(     '--text-log=FILE',  String, 'text log file') { |v| text_log=v }
  p.on('-v','--verbose=LEVEL',  String, 'log level') do |log_level|

    symb = log_level.downcase.intern
    if debug_levels.has_key?(symb)
      log_level = debug_levels[symb]
    else
      puts p
      puts "Invalid log level #{log_level}"
      exit(1)
    end
  end
  p.on('-l','--logfile=FILE','log file') { |v| log_file=v }

  begin
    p.parse!(ARGV)
  rescue
    puts p
    exit(1)
  end
end

if daemon
  require 'daemonize'
  include Daemonize
  daemonize
end

ssl_cert_base = base_path + '/certs/host' if ssl_cert_base == nil
template_path = base_path + '/lib/templates/central'

$logger = Logger.new( STDERR )
$logger.level = log_level
$access_log = Logger.new( log_file ? log_file : STDERR )
$access_log.level = log_level

$sedna = Sedna.connect({:database=>database})

if $sedna.query('fn:doc("$collections")//collection[@name="admininfo"]').size == 0
    puts 'Can\'t open admin database. Run debinit.sh first'
    exit
end


info = AdminDict.new('admininfo', db_path)
info.xslt_path = xslt_path
info.service_name = 'central'
info.template_path = template_path
userdb = AdminDict.new('tecu_user', db_path)
userdb.service_name = 'tecu'
userdb.template_path = base_path + '/lib/templates/czj'
userdb.xslt_path = xslt_path


server = create_http_server({
  :auth         => auth,
  :dbpasswd => userdb,
  :loadperms => true,
  :port         => port,
  :ssl          => ssl,
  :ssl_cert_base => ssl_cert_base,
  :ssl_ca_cert => ssl_ca_cert,
  :logger       => $logger,
  :access_log   => $access_log
})
server.info = info

#server.config[:DirectoryIndex] = ['tezaurus.html']

info.remount(server, 'central', nil)
info.array['tecu_user'] = userdb
server.mount "/hello", HelloServlet, info
server.mount "/central",  FileTecu, base_path + '/files/central'
server.mount_proc('/tecu') {|req, resp|
  resp.set_redirect(WEBrick::HTTPStatus::SeeOther, '/public/registr?'+req.query_string)
}
server.mount_proc('/') {|req, resp|
  resp.set_redirect(WEBrick::HTTPStatus::SeeOther, 'https://'+req.host.to_s+'/public/central/')
}




trap("TERM"){
  $stderr.puts "GOT TERM"
  server_shutdown(server, info)
}
trap("KILL"){
  $stderr.puts "GOT KILL"
  server_shutdown(server, info)
}

begin
  server.start
rescue => e
  $logger.error( "exception #{e.class}: #{e.message}\n" + e.backtrace.join("\n\t\t") )
  raise
end
