#!/bin/bash
#1-aplikace,2-data,3-logy
FILES=""
if [ "$1" = "y" ]; then
  FILES="$FILES /var/lib/deb-server/lib /var/lib/deb-server/bin /var/lib/deb-server/files /var/lib/deb-server/xslt /var/lib/deb-server/client /var/lib/deb-server/certs /var/lib/deb-server/init /var/lib/deb-server/termcheck"
fi
if [ "$2" = "y" ]; then
  FILES="$FILES /var/lib/deb-server/db /var/lib/deb-server/backup/*latest*"
fi
if [ "$3" = "y" ]; then
  FILES="$FILES /var/log/deb-server/"
fi

BAK="/tmp/tezaurus$(date +"%Y%m%d-%H%M%S").tgz"
echo "Ukladam soubory pro zalohu do souboru: $BAK"
tar cvzf $BAK $FILES
echo "Zaloha ulozena jako soubor: $BAK"

#samba settings
BAK_PATH=`se_term -query 'doc("tech","settings")/tech/backup_path/text()' deb`
BAK_PASS=`se_term -query 'doc("tech","settings")/tech/backup_pass/text()' deb`
BAK_USER=`se_term -query 'doc("tech","settings")/tech/backup_user/text()' deb`
BAK_URL=`se_term -query 'doc("tech","settings")/tech/backup_url/text()' deb`

if [ "$BAK_PATH" != "" ] && [ "$BAK_URL" != "" ]; then
  FILE=`basename $BAK`
  echo "Zalohovani na $BAK_URL/$BAK_PATH"
  smbclient -U $BAK_USER%$BAK_PASS //$BAK_URL/$BAK_PATH -c 'lcd "/tmp"; put "'$FILE'"' 2>&1
else
  echo "Neni nastaven Samba server a cesta pro zalohy. Zaloha neprovedena"
fi

