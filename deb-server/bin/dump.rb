#!/usr/bin/ruby

# add debserver library path
$LOAD_PATH.unshift('/var/lib/deb-server/lib/')

# include libraries
require 'rubygems'
require 'sedna'
require 'dump'
require 'optparse'
require 'fileutils'

# set database paths
out_path = '/var/lib/deb-server/backup/'
log_path = '/var/lib/deb-server/backup/'
database = nil
ruby = false
gzip = false

ARGV.options do |p|
  p.banner = "usage: #{$0} [options]\n"
  p.on('--out-path=PATH', String, 'backup path') { |v|out_path=v }
  p.on('--log-path=PATH', String, 'log path') { |v| log_path=v }
  p.on('--database=NAME', String, 'database name') { |v| database=v }
  p.on('-r','--ruby', 'dump ruby form') { |r| ruby = true }
  p.on('-z','--gzip', 'gzip outfile') { |g| gzip = true }
  
  begin
    p.parse!(ARGV)
  rescue
    puts p
    exit(1)
  end
end

if not log_path[-1,1] == '/'
  log_path += '/'
end
if not out_path[-1,1] == '/'
  out_path += '/'
end

sedna = Sedna.connect({:database=>'deb'})

if sedna.query('fn:doc("$collections")//collection[@name="'+database+'"]').size == 0
  puts "Database #{database} not found"
  exit(1)
end

time = Time.now.strftime('%Y%m%d-%H%M')
extension = ruby ? '.rb' : '.xml'
backupname = database + time + extension
logname = database + time + '.log'

puts "Dumping database #{database} to #{out_path}#{backupname} (gzip: #{gzip}), log #{log_path}#{logname}"

dump(database, out_path+backupname, log_path+logname, ruby, gzip)

backupname += '.gz' if gzip
extension += '.gz' if gzip
link = out_path + database + '-latest' + extension
filelink = '/var/lib/deb-server/files/' + database + '-latest' + extension

File.unlink(link) if File.symlink?(link)
exit 0 if File.exist?(link)

File.unlink(filelink) if File.exists?(filelink)

File.symlink(out_path + backupname, link)
FileUtils.copy(link, filelink)
