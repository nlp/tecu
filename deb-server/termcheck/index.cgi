#!/usr/bin/env python

import sys
import cgi
import os
import subprocess
import string
import random
import manatee
import wmap
import cgitb
import re
import time

cgitb.enable()

DIR = '/var/www/termcheck'
LOG = DIR + '/process.log'
MAJKA_PIPE = '/opt/majka_pipe/majka-czech.sh'

SGML_TAG = ur"""
    (?:                         # make enclosing parantheses non-grouping
    <!-- .*? -->                # XML/SGML comment
    |                           # -- OR --
    <[!?/]?(?!\d)\w[-\.:\w]*    # Start of tag/directive
    (?:                         # Attributes
        [^>'"]*                 # - attribute name (+whitespace +equal sign)
        (?:'[^']*'|"[^"]*")     # - attribute value
    )* 
    \s*                         # Spaces at the end
    /?                          # Forward slash at the end of singleton tags
    \s*                         # More spaces at the end
    >                           # +End of tag/directive
    )"""
SGML_TAG_RE = re.compile(SGML_TAG, re.UNICODE | re.VERBOSE | re.DOTALL)
GLUE_TAG = u'<g/>'

def log(msg):
    logf = open(LOG, 'a')
    logf.write(msg + '\n')
    logf.flush()
    logf.close()

def vert2plain(lines):
    output_string = ''
    glue = False
    for line in lines:
        l = line.strip()
        if SGML_TAG_RE.match(line):
            if l == GLUE_TAG:
                glue = True
                continue
            # keep annotation with <em class="xxx">...</em>
            if l.startswith('<em ') or l.startswith('</em>'):
                if l.startswith('<em '):
                    output_string += ' ' + l
                else:
                    output_string += l + ' '
                glue = True
                continue
            if l.startswith('<s ') or l.startswith('</s>'):
                glue = True
                continue
            #output_string += l + '\n'
        else:
            token = l.split('\t')[0]
            if not glue:
                output_string += ' '
            output_string += token
        glue = False
    return output_string

def prevert2vert(f):
    pass

if __name__ == '__main__':
    print "Content-Type: text/html; charset=utf-8"
    print

    form = cgi.FieldStorage()
    txt = form.getvalue('txt', '').decode('utf-8')
    
    prevert_fn = DIR + '/tmp.prevert'
    vert_fn = DIR + '/tmp.vert'
    prevert_f = open(prevert_fn, 'w')
    prevert_f.write('<s>\n')
    prevert_f.write(txt.strip().encode('utf-8') + '\n')
    prevert_f.write('</s>')
    prevert_f.close()

    # tag prevert
    cmd = 'cat %s | %s | grep -v "^<g/>" > %s' % (prevert_fn, MAJKA_PIPE, DIR + '/tmp.vert')
    p1 = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    time.sleep(4)

    # get annotated tems
    terms = {}
    t = []
    tid = ''
    for line in open('tecu_terms.vert'):
        if line.startswith('<sent '):
            tid = line.replace('<sent id="', '').replace('">', '').strip()
            continue
        if line.startswith('</sent>'):
            if t:
                terms.setdefault(t[0], [])
                terms[t[0]].append((t, tid))
            t = []
            continue
        if line.startswith('<g'):
            continue
        t.append(line.decode('utf-8').split('\t')[1])

    vert_f = open(vert_fn)
    vert_lines = vert_f.readlines()

    i = 0
    annots = []
    while i < len(vert_lines):
        if vert_lines[i].startswith('<'):
            i += 1
            continue
        try:
            _, l, _, _ = vert_lines[i].decode('utf-8').strip().split('\t')
        except ValueError:
            _, l, _ = vert_lines[i].decode('utf-8').strip().split('\t')
        if l in terms:
            log('%d:MATCH FIRST %s' % (i, l.encode('utf-8')))
            matches = []
            ts = list(terms[l])
            maxlen = max(map(len, terms[l]))
            log('  %s maxlen' % maxlen)
            j = 1
            while i+j < len(vert_lines) and j < maxlen:
                if vert_lines[i+j].startswith('<s') or vert_lines[i+j].startswith('</s>'):
                    break
                try:
                    _, l2, _, _ = vert_lines[i+j].decode('utf-8').split('\t')
                except ValueError:
                    _, l2, _ = vert_lines[i+j].decode('utf-8').split('\t')
                k = 0
                while k < len(ts):
                    if j < len(ts[k][0]):
                        if ts[k][0][j] != l2 and l2 != '<g/>':
                            del ts[k]
                            continue
                    if len(ts[k][0]) == j:
                        log('    %d:MATCH %s' % (k, ' '.join(ts[k][0]).encode('utf-8')))
                        matches.append((len(ts[k][0]), ts[k][0], ts[k][1]))
                    k += 1
                j += 1
            k = 0
            while k < len(ts):
                if len(ts[k][0]) == j:
                    matches.append((len(ts[k][0]), ts[k][0], ts[k][1]))
                k += 1
            matches.sort()
            if matches and (not annots or (annots[-1][0] + annots[-1][1]) < i + matches[-1][0]):
                annots.append((i, matches[-1][0], matches[-1][2]))
        i += 1

    vert_f.close()
    vert_f = open(vert_fn)
    vert_lines = vert_f.readlines()
    i = 0
    while i < len(vert_lines):
        for item in annots:
            if item[0] == i:
                sys.stdout.write('<a href="https://abulafia.fi.muni.cz:8040/editor/tezaurus.html?id=%s">' % item[2])
                j = 0
                while j < item[1] and i < len(vert_lines):
                    sys.stdout.write(vert_lines[i+j].strip().split('\t')[0])
                    if j != item[1] - 1:
                        sys.stdout.write(' ')
                    j += 1
                sys.stdout.write('</a> ')
                i = i + j
                break
        if vert_lines[i].startswith('<'):
            i += 1
            continue
        sys.stdout.write(vert_lines[i].strip().split('\t')[0] + ' ')
        i += 1
