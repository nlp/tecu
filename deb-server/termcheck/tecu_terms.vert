<sent id="7185">
polární	polární	k2eAgInSc1d1	polární
kruh	kruh	k1gInSc1	kruh
</sent>
<sent id="10000">
těleso	těleso	k1gNnSc4	těleso
</sent>
<sent id="10001">
topologické	topologický	k2eAgNnSc1d1	topologické
těleso	těleso	k1gNnSc1	těleso
</sent>
<sent id="10002">
strukturní	strukturní	k2eAgMnSc1d1	strukturní
primitivum	primitivum	k?	
</sent>
<sent id="10003">
křižovatka	křižovatka	k1gFnSc1	křižovatka
</sent>
<sent id="10004">
strukturní	strukturní	k2eAgInSc4d1	strukturní
komplex	komplex	k1gInSc4	komplex
</sent>
<sent id="10005">
síť	síť	k1gFnSc1	síť
</sent>
<sent id="10006">
strom	strom	k1gInSc1	strom
</sent>
<sent id="10007">
geometrický	geometrický	k2eAgInSc1d1	geometrický
objekt	objekt	k1gInSc1	objekt
</sent>
<sent id="10008">
geometrický	geometrický	k2eAgInSc1d1	geometrický
komplex	komplex	k1gInSc1	komplex
</sent>
<sent id="10009">
geometrický	geometrický	k2eAgInSc1d1	geometrický
agregát	agregát	k1gInSc1	agregát
</sent>
<sent id="10010">
topologický	topologický	k2eAgInSc4d1	topologický
objekt	objekt	k1gInSc4	objekt
</sent>
<sent id="10011">
topologický	topologický	k2eAgInSc4d1	topologický
komplex	komplex	k1gInSc4	komplex
</sent>
<sent id="10012">
topologický	topologický	k2eAgInSc4d1	topologický
výraz	výraz	k1gInSc4	výraz
</sent>
<sent id="10013">
strukturní	strukturní	k2eAgInSc4d1	strukturní
objekt	objekt	k1gInSc4	objekt
</sent>
<sent id="10014">
bod	bod	k1gInSc1	bod
mříže	mříž	k1gFnSc2	mříž
</sent>
<sent id="10015">
křivka	křivka	k1gFnSc1	křivka
mříže	mříže	k1gFnSc1	mříže
</sent>
<sent id="10016">
buńka	buńka	k1gFnSc1	buńka
mříže	mříže	k1gFnSc1	mříže
</sent>
<sent id="10017">
primární	primární	k2eAgInSc4d1	primární
DMÚ	DMÚ	kA	
</sent>
<sent id="10018">
sekundární	sekundární	k2eAgInSc4d1	sekundární
DMÚ	DMÚ	kA	
</sent>
<sent id="10019">
vektorový	vektorový	k2eAgInSc1d1	vektorový
objekt	objekt	k1gInSc1	objekt
</sent>
<sent id="10020">
mřížový	mřížový	k2eAgInSc4d1	mřížový
objekt	objekt	k1gInSc4	objekt
</sent>
<sent id="10021">
mříž	mříž	k1gFnSc1	mříž
(	(	kIx(	
<g/>
grid	grid	k1gInSc1	grid
<g/>
)	)	kIx)	
</sent>
<sent id="10022">
digitální	digitální	k2eAgInSc4d1	digitální
model	model	k1gInSc4	model
</sent>
<sent id="10023">
digitální	digitální	k2eAgInSc1d1	digitální
objektový	objektový	k2eAgInSc1d1	objektový
model	model	k1gInSc1	model
</sent>
<sent id="10024">
tematický	tematický	k2eAgInSc4d1	tematický
prostorový	prostorový	k2eAgInSc4d1	prostorový
objekt	objekt	k1gInSc4	objekt
</sent>
<sent id="10025">
přírodní	přírodní	k2eAgInSc4d1	přírodní
objekt	objekt	k1gInSc4	objekt
</sent>
<sent id="10026">
socioekonomický	socioekonomický	k2eAgInSc4d1	socioekonomický
objekt	objekt	k1gInSc4	objekt
</sent>
<sent id="10027">
časový	časový	k2eAgInSc4d1	časový
objekt	objekt	k1gInSc4	objekt
</sent>
<sent id="10028">
jednorázový	jednorázový	k2eAgInSc1d1	jednorázový
časový	časový	k2eAgInSc1d1	časový
objekt	objekt	k1gInSc1	objekt
</sent>
<sent id="10029">
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
časový	časový	k2eAgInSc4d1	časový
objekt	objekt	k1gInSc4	objekt
</sent>
<sent id="10030">
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
časový	časový	k2eAgInSc1d1	časový
objekt	objekt	k1gInSc1	objekt
</sent>
<sent id="10031">
geoinformační	geoinformační	k2eAgFnSc1d1	geoinformační
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
</sent>
<sent id="10032">
globální	globální	k2eAgFnSc1d1	globální
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
prostorových	prostorový	k2eAgNnPc2d1	prostorové
dat	datum	k1gNnPc2	datum
</sent>
<sent id="10033">
národní	národní	k2eAgMnSc1d1	národní
</sent>
<sent id="10034">
geoinformační	geoinformační	k2eAgInSc4d1	geoinformační
produkt	produkt	k1gInSc4	produkt
</sent>
<sent id="10035">
analogový	analogový	k2eAgInSc4d1	analogový
model	model	k1gInSc4	model
</sent>
<sent id="10036">
geoinformační	geoinformační	k2eAgInSc4d1	geoinformační
proces	proces	k1gInSc4	proces
</sent>
<sent id="20000">
nezařazené	zařazený	k2eNgInPc1d1	nezařazený
termíny	termín	k1gInPc1	termín
</sent>
<sent id="7431">
dron	dron	k1gInSc1	dron
</sent>
<sent id="6916">
dokončovací	dokončovací	k2eAgFnSc1d1	dokončovací
výroba	výroba	k1gFnSc1	výroba
</sent>
<sent id="7415">
prostorový	prostorový	k2eAgInSc4d1	prostorový
objekt	objekt	k1gInSc4	objekt
</sent>
<sent id="1000">
obnova	obnova	k1gFnSc1	obnova
pozemkového	pozemkový	k2eAgInSc2d1	pozemkový
katastru	katastr	k1gInSc2	katastr
</sent>
<sent id="1001">
polohové	polohový	k2eAgFnPc1d1	polohová
určení	určení	k1gNnSc4	určení
nemovitosti	nemovitost	k1gFnSc2	nemovitost
a	a	k8xC	
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
</sent>
<sent id="1002">
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
pozemková	pozemkový	k2eAgFnSc1d1	pozemková
úprava	úprava	k1gFnSc1	úprava
</sent>
<sent id="1003">
poznámky	poznámka	k1gFnSc2	poznámka
v	v	k7c6	
pozemkové	pozemkový	k2eAgFnSc6d1	pozemková
knize	kniha	k1gFnSc6	kniha
</sent>
<sent id="1005">
Katastrální	katastrální	k2eAgInSc1d1	katastrální
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
systém	systém	k1gInSc1	systém
svatoštěpánský	svatoštěpánský	k2eAgInSc1d1	svatoštěpánský
</sent>
<sent id="1006">
údaj	údaj	k1gInSc1	údaj
pozemkového	pozemkový	k2eAgInSc2d1	pozemkový
katastru	katastr	k1gInSc2	katastr
</sent>
<sent id="1007">
vklad	vklad	k1gInSc1	vklad
práv	práv	k2eAgInSc1d1	práv
k	k	k7c3	
nemovitosti	nemovitost	k1gFnSc3	nemovitost
do	do	k7c2	
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="1008">
analogová	analogový	k2eAgFnSc1d1	analogová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="1010">
aplikační	aplikační	k2eAgInSc4d1	aplikační
schéma	schéma	k1gNnSc1	schéma
</sent>
<sent id="1011">
archiv	archiv	k1gInSc1	archiv
</sent>
<sent id="1012">
astronomická	astronomický	k2eAgFnSc1d1	astronomická
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="1013">
atribut	atribut	k1gInSc1	atribut
</sent>
<sent id="1014">
atributová	atributový	k2eAgFnSc1d1	atributová
tabulka	tabulka	k1gFnSc1	tabulka
</sent>
<sent id="1015">
automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
digitalizace	digitalizace	k1gFnSc1	digitalizace
</sent>
<sent id="1016">
automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="1018">
báze	báze	k1gFnSc1	báze
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1019">
báze	báze	k1gFnSc2	báze
dat	datum	k1gNnPc2	datum
geografického	geografický	k2eAgNnSc2d1	geografické
názvosloví	názvosloví	k1gNnSc2	názvosloví
</sent>
<sent id="1020">
báze	báze	k1gFnSc2	báze
geografických	geografický	k2eAgNnPc2d1	geografické
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1021">
báze	báze	k1gFnSc2	báze
kartografických	kartografický	k2eAgNnPc2d1	kartografické
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1022">
bezešvá	bezešvý	k2eAgFnSc1d1	bezešvá
báze	báze	k1gFnSc1	báze
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1023">
bitová	bitový	k2eAgFnSc1d1	bitová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="1024">
bod	bod	k1gInSc1	bod
</sent>
<sent id="1026">
bonitace	bonitace	k1gFnSc1	bonitace
půdy	půda	k1gFnSc2	půda
</sent>
<sent id="1027">
bonitovaná	bonitovaný	k2eAgFnSc1d1	bonitovaná
půdně	půdně	k6eAd1	
ekologická	ekologický	k2eAgFnSc1d1	ekologická
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	
<g/>
BPEJ	BPEJ	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="1028">
břehová	břehový	k2eAgFnSc1d1	Břehová
čára	čára	k1gFnSc1	čára
</sent>
<sent id="1029">
budova	budova	k1gFnSc1	budova
</sent>
<sent id="1032">
časová	časový	k2eAgFnSc1d1	časová
poloha	poloha	k1gFnSc1	poloha
</sent>
<sent id="1033">
časová	časový	k2eAgFnSc1d1	časová
přesnost	přesnost	k1gFnSc1	přesnost
</sent>
<sent id="1034">
časové	časový	k2eAgNnSc4d1	časové
rozlišení	rozlišení	k1gNnSc4	rozlišení
</sent>
<sent id="1035">
časový	časový	k2eAgInSc1d1	časový
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
</sent>
<sent id="1036">
časový	časový	k2eAgInSc1d1	časový
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
systém	systém	k1gInSc1	systém
</sent>
<sent id="1037">
členitost	členitost	k1gFnSc1	členitost
(	(	kIx(	
<g/>
terénního	terénní	k2eAgInSc2d1	terénní
<g/>
)	)	kIx)	
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="1038">
dálkový	dálkový	k2eAgInSc4d1	dálkový
průzkum	průzkum	k1gInSc4	průzkum
Země	zem	k1gFnSc2	zem
(	(	kIx(	
<g/>
DPZ	DPZ	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="1040">
datový	datový	k2eAgInSc4d1	datový
záznam	záznam	k1gInSc4	záznam
</sent>
<sent id="1042">
deformace	deformace	k1gFnSc1	deformace
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="1043">
diferenciální	diferenciální	k2eAgInSc4d1	diferenciální
překreslení	překreslení	k1gNnSc1	překreslení
</sent>
<sent id="1044">
digitalizace	digitalizace	k1gFnSc1	digitalizace
</sent>
<sent id="1045">
digitální	digitální	k2eAgInSc4d1	digitální
data	datum	k1gNnSc2	datum
</sent>
<sent id="1046">
digitální	digitální	k2eAgInSc4d1	digitální
kartografický	kartografický	k2eAgInSc4d1	kartografický
model	model	k1gInSc4	model
</sent>
<sent id="1047">
digitální	digitální	k2eAgFnSc1d1	digitální
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="1048">
digitální	digitální	k2eAgInSc4d1	digitální
mapování	mapování	k1gNnSc1	mapování
</sent>
<sent id="1049">
digitální	digitální	k2eAgInSc4d1	digitální
model	model	k1gInSc4	model
povrchu	povrch	k1gInSc2	povrch
</sent>
<sent id="1051">
digitální	digitální	k2eAgInSc4d1	digitální
obraz	obraz	k1gInSc4	obraz
</sent>
<sent id="1052">
digitální	digitální	k2eAgInSc4d1	digitální
model	model	k1gInSc4	model
území	území	k1gNnSc2	území
(	(	kIx(	
<g/>
DMÚ	DMÚ	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4415">
členitý	členitý	k2eAgInSc4d1	členitý
terén	terén	k1gInSc4	terén
</sent>
<sent id="1054">
digitizér	digitizér	k1gInSc1	digitizér
</sent>
<sent id="1055">
distribuovaná	distribuovaný	k2eAgFnSc1d1	distribuovaná
databáze	databáze	k1gFnSc1	databáze
</sent>
<sent id="1056">
dolní	dolní	k2eAgFnSc1d1	dolní
mezní	mezní	k2eAgFnSc1d1	mezní
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="1057">
dolní	dolní	k2eAgInSc1d1	dolní
mezní	mezní	k2eAgInSc1d1	mezní
rozměr	rozměr	k1gInSc1	rozměr
</sent>
<sent id="1058">
druh	druh	k1gInSc1	druh
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="1059">
družicová	družicový	k2eAgFnSc1d1	družicová
altimetrie	altimetrie	k1gFnSc1	altimetrie
</sent>
<sent id="1060">
entita	entita	k1gFnSc1	entita
</sent>
<sent id="1061">
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
(	(	kIx(	
<g/>
EN	EN	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="1062">
export	export	k1gInSc1	export
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1063">
extravilán	extravilán	k1gInSc1	extravilán
</sent>
<sent id="1064">
falešná	falešný	k2eAgFnSc1d1	falešná
barva	barva	k1gFnSc1	barva
</sent>
<sent id="1065">
federované	federovaný	k2eAgFnSc2d1	Federovaná
databáze	databáze	k1gFnSc2	databáze
</sent>
<sent id="1066">
formát	formát	k1gInSc1	formát
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1067">
funkce	funkce	k1gFnSc2	funkce
</sent>
<sent id="1068">
funkční	funkční	k2eAgFnSc1d1	funkční
norma	norma	k1gFnSc1	norma
</sent>
<sent id="1069">
generalizační	generalizační	k2eAgInSc4d1	generalizační
operátor	operátor	k1gInSc4	operátor
</sent>
<sent id="1070">
geografická	geografický	k2eAgFnSc1d1	geografická
data	datum	k1gNnSc2	datum
</sent>
<sent id="1071">
geografická	geografický	k2eAgFnSc1d1	geografická
informace	informace	k1gFnSc1	informace
</sent>
<sent id="4936">
bodový	bodový	k2eAgInSc4d1	bodový
kartodiagram	kartodiagram	k1gInSc4	kartodiagram
</sent>
<sent id="4937">
kyvadlová	kyvadlový	k2eAgFnSc1d1	kyvadlová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="1074">
geografický	geografický	k2eAgInSc4d1	geografický
identifikátor	identifikátor	k1gInSc4	identifikátor
</sent>
<sent id="1075">
geografický	geografický	k2eAgInSc1d1	geografický
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
(	(	kIx(	
<g/>
GIS	gis	k1gNnSc1	gis
<g/>
)	)	kIx)	
</sent>
<sent id="1076">
geografický	geografický	k2eAgInSc1d1	geografický
rejstřík	rejstřík	k1gInSc1	rejstřík
</sent>
<sent id="1077">
geoinformatika	geoinformatika	k1gFnSc1	geoinformatika
</sent>
<sent id="1078">
geokód	geokód	k1gInSc1	geokód
</sent>
<sent id="1079">
geomatika	geomatika	k1gFnSc1	geomatika
</sent>
<sent id="1080">
geometrická	geometrický	k2eAgFnSc1d1	geometrická
konzistence	konzistence	k1gFnSc1	konzistence
</sent>
<sent id="1081">
geometrické	geometrický	k2eAgInPc4d1	geometrický
primitivum	primitivum	k?	
</sent>
<sent id="1082">
geometrické	geometrický	k2eAgNnSc4d1	geometrické
určení	určení	k1gNnSc4	určení
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="1083">
geometrické	geometrický	k2eAgNnSc4d1	geometrické
určení	určení	k1gNnSc4	určení
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
</sent>
<sent id="1084">
geometrický	geometrický	k2eAgInSc1d1	geometrický
plán	plán	k1gInSc1	plán
</sent>
<sent id="7209">
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
ústav	ústav	k1gInSc1	ústav
geodetický	geodetický	k2eAgInSc1d1	geodetický
</sent>
<sent id="1087">
geosystém	geosystý	k2eAgInSc6d1	geosystý
</sent>
<sent id="1088">
grafické	grafický	k2eAgNnSc4d1	grafické
znázornění	znázornění	k1gNnSc4	znázornění
</sent>
<sent id="1089">
Katastrální	katastrální	k2eAgInSc1d1	katastrální
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
systém	systém	k1gInSc1	systém
gusterbergský	gusterbergský	k2eAgInSc1d1	gusterbergský
</sent>
<sent id="1090">
horní	horní	k2eAgFnSc1d1	horní
mezní	mezní	k2eAgFnSc1d1	mezní
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="1091">
horní	horní	k2eAgInSc1d1	horní
mezní	mezní	k2eAgInSc1d1	mezní
rozměr	rozměr	k1gInSc1	rozměr
</sent>
<sent id="1092">
hrana	hrana	k1gFnSc1	hrana
</sent>
<sent id="1093">
hranice	hranice	k1gFnSc1	hranice
</sent>
<sent id="1094">
hranice	hranice	k1gFnSc1	hranice
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="1095">
hranice	hranice	k1gFnSc1	hranice
parcely	parcela	k1gFnSc2	parcela
</sent>
<sent id="1096">
hraniční	hraniční	k2eAgInSc4d1	hraniční
bod	bod	k1gInSc4	bod
</sent>
<sent id="1097">
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="1098">
identický	identický	k2eAgInSc4d1	identický
bod	bod	k1gInSc4	bod
</sent>
<sent id="1099">
identifikace	identifikace	k1gFnSc2	identifikace
parcel	parcela	k1gFnPc2	parcela
</sent>
<sent id="1103">
import	import	k1gInSc1	import
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1104">
informace	informace	k1gFnSc1	informace
(	(	kIx(	
<g/>
sg.	sg.	k?	
i	i	k8xC	
pl.	pl.	k?	
<g/>
)	)	kIx)	
</sent>
<sent id="1105">
informační	informační	k2eAgInSc4d1	informační
systém	systém	k1gInSc4	systém
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
(	(	kIx(	
<g/>
ISKN	ISKN	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="3000">
AAT	AAT	kA	
</sent>
<sent id="1108">
interakční	interakční	k2eAgFnSc1d1	interakční
digitalizace	digitalizace	k1gFnSc1	digitalizace
</sent>
<sent id="1110">
intravilán	intravilán	k1gInSc1	intravilán
</sent>
<sent id="1111">
kvalita	kvalita	k1gFnSc1	kvalita
</sent>
<sent id="1112">
jednotná	jednotný	k2eAgFnSc1d1	jednotná
evidence	evidence	k1gFnSc1	evidence
půdy	půda	k1gFnSc2	půda
(	(	kIx(	
<g/>
JEP	JEP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="1114">
Josefský	josefský	k2eAgInSc4d1	josefský
katastr	katastr	k1gInSc4	katastr
</sent>
<sent id="1115">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
délka	délka	k1gFnSc1	délka
</sent>
<sent id="1117">
kartografické	kartografický	k2eAgNnSc4d1	kartografické
dílo	dílo	k1gNnSc4	dílo
</sent>
<sent id="1119">
katalog	katalog	k1gInSc1	katalog
kódování	kódování	k1gNnSc2	kódování
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	
atributů	atribut	k1gInPc2	atribut
</sent>
<sent id="1120">
katastr	katastr	k1gInSc1	katastr
</sent>
<sent id="1121">
katastr	katastr	k1gInSc1	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
(	(	kIx(	
<g/>
KN	KN	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="1122">
katastr	katastr	k1gInSc1	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
ČR	ČR	kA	
</sent>
<sent id="1123">
katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="1125">
klasifikace	klasifikace	k1gFnSc1	klasifikace
</sent>
<sent id="1126">
klient	klient	k1gMnSc1	klient
</sent>
<sent id="1127">
kódování	kódování	k1gNnSc4	kódování
</sent>
<sent id="1128">
kompatibilita	kompatibilita	k1gFnSc1	kompatibilita
</sent>
<sent id="1129">
komprimace	komprimace	k1gFnSc1	komprimace
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1130">
konceptuální	konceptuální	k2eAgInSc4d1	konceptuální
model	model	k1gInSc4	model
</sent>
<sent id="1131">
konceptuální	konceptuální	k2eAgInSc4d1	konceptuální
schéma	schéma	k1gNnSc1	schéma
</sent>
<sent id="1132">
kontrola	kontrola	k1gFnSc1	kontrola
měření	měření	k1gNnSc2	měření
</sent>
<sent id="1133">
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
měření	měření	k1gNnSc1	měření
</sent>
<sent id="1134">
konverze	konverze	k1gFnSc1	konverze
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1135">
kopírování	kopírování	k1gNnSc4	kopírování
</sent>
<sent id="4203">
korekční	korekční	k2eAgInSc1d1	korekční
součinitel	součinitel	k1gInSc1	součinitel
</sent>
<sent id="1137">
kótovaný	kótovaný	k2eAgInSc4d1	kótovaný
bod	bod	k1gInSc4	bod
</sent>
<sent id="1139">
křivka	křivka	k1gFnSc1	křivka
</sent>
<sent id="1140">
kurzor	kurzor	k1gInSc1	kurzor
</sent>
<sent id="1141">
kvadrantový	kvadrantový	k2eAgInSc1d1	kvadrantový
strom	strom	k1gInSc1	strom
</sent>
<sent id="1142">
logická	logický	k2eAgFnSc1d1	logická
konzistence	konzistence	k1gFnSc1	konzistence
</sent>
<sent id="1143">
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="1144">
mapa	mapa	k1gFnSc1	mapa
v	v	k7c6	
digitální	digitální	k2eAgFnSc6d1	digitální
formě	forma	k1gFnSc6	forma
</sent>
<sent id="1145">
mapování	mapování	k1gNnSc2	mapování
</sent>
<sent id="1147">
matematická	matematický	k2eAgFnSc1d1	matematická
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="1148">
maticová	maticový	k2eAgFnSc1d1	maticová
data	datum	k1gNnSc2	datum
</sent>
<sent id="1149">
médium	médium	k1gNnSc4	médium
</sent>
<sent id="1150">
metadata	metadata	k1gFnSc1	metadata
</sent>
<sent id="1151">
měřená	měřený	k2eAgFnSc1d1	měřená
hodnota	hodnota	k1gFnSc1	hodnota
</sent>
<sent id="1152">
měřický	měřický	k2eAgInSc4d1	měřický
bod	bod	k1gInSc4	bod
</sent>
<sent id="1153">
měřický	měřický	k2eAgInSc1d1	měřický
náčrt	náčrt	k1gInSc1	náčrt
</sent>
<sent id="1154">
měřický	měřický	k2eAgInSc4d1	měřický
operát	operát	k1gInSc4	operát
</sent>
<sent id="1155">
měřický	měřický	k2eAgInSc1d1	měřický
operát	operát	k1gInSc1	operát
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="1157">
měřítko	měřítko	k1gNnSc4	měřítko
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="1158">
mikrografie	mikrografie	k1gFnSc1	mikrografie
</sent>
<sent id="1159">
místní	místní	k2eAgInSc1d1	místní
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
systém	systém	k1gInSc1	systém
</sent>
<sent id="1161">
model	model	k1gInSc1	model
</sent>
<sent id="1162">
modelově	modelově	k6eAd1	
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="1163">
modelování	modelování	k1gNnSc4	modelování
</sent>
<sent id="1164">
mříž	mříž	k1gFnSc1	mříž
</sent>
<sent id="1165">
myš	myš	k1gFnSc1	myš
</sent>
<sent id="1166">
nabídka	nabídka	k1gFnSc1	nabídka
</sent>
<sent id="1168">
nemovitost	nemovitost	k1gFnSc1	nemovitost
</sent>
<sent id="1169">
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
trojúhelníková	trojúhelníkový	k2eAgFnSc1d1	trojúhelníková
síť	síť	k1gFnSc1	síť
</sent>
<sent id="1170">
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
poloha	poloha	k1gFnSc1	poloha
</sent>
<sent id="1171">
nepřímý	přímý	k2eNgInSc1d1	nepřímý
prostorový	prostorový	k2eAgInSc1d1	prostorový
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
</sent>
<sent id="1172">
nezpracovaná	zpracovaný	k2eNgFnSc1d1	nezpracovaná
data	datum	k1gNnSc2	datum
</sent>
<sent id="3002">
AGS	AGS	kA	
</sent>
<sent id="1174">
objektově	objektově	k6eAd1	
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
systém	systém	k1gInSc1	systém
</sent>
<sent id="1175">
obrazová	obrazový	k2eAgNnPc4d1	obrazové
data	datum	k1gNnPc4	datum
</sent>
<sent id="1176">
obrys	obrys	k1gInSc1	obrys
</sent>
<sent id="1177">
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="1178">
odvozená	odvozený	k2eAgFnSc1d1	odvozená
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4215">
ovlivňující	ovlivňující	k2eAgFnSc1d1	ovlivňující
veličina	veličina	k1gFnSc1	veličina
</sent>
<sent id="1182">
otevřený	otevřený	k2eAgInSc4d1	otevřený
GIS	gis	k1gNnSc1	gis
</sent>
<sent id="1183">
ověření	ověření	k1gNnSc4	ověření
geometrického	geometrický	k2eAgInSc2d1	geometrický
plánu	plán	k1gInSc2	plán
</sent>
<sent id="1184">
ověřovací	ověřovací	k2eAgInSc4d1	ověřovací
měření	měření	k1gNnSc1	měření
</sent>
<sent id="4145">
pevná	pevný	k2eAgFnSc1d1	pevná
měřická	měřický	k2eAgFnSc1d1	měřická
přímka	přímka	k1gFnSc1	přímka
</sent>
<sent id="1186">
papír	papír	k1gInSc1	papír
</sent>
<sent id="1187">
parcela	parcela	k1gFnSc1	parcela
</sent>
<sent id="1188">
parketování	parketování	k1gNnSc4	parketování
</sent>
<sent id="1189">
pevný	pevný	k2eAgInSc4d1	pevný
bod	bod	k1gInSc4	bod
</sent>
<sent id="1190">
písemný	písemný	k2eAgInSc4d1	písemný
operát	operát	k1gInSc4	operát
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="1191">
pixel	pixel	k1gInSc1	pixel
</sent>
<sent id="1192">
plán	plán	k1gInSc1	plán
</sent>
<sent id="1193">
plocha	plocha	k1gFnSc1	plocha
</sent>
<sent id="1194">
zvětšení	zvětšení	k1gNnSc2	zvětšení
/	/	kIx~	
zmenšení	zmenšení	k1gNnSc2	zmenšení
(	(	kIx(	
<g/>
plynulé	plynulý	k2eAgNnSc1d1	plynulé
<g/>
)	)	kIx)	
</sent>
<sent id="1195">
počítačová	počítačový	k2eAgFnSc1d1	počítačová
grafika	grafika	k1gFnSc1	grafika
</sent>
<sent id="1196">
podrobný	podrobný	k2eAgInSc4d1	podrobný
bod	bod	k1gInSc4	bod
</sent>
<sent id="1197">
pokrytí	pokrytí	k1gNnSc4	pokrytí
</sent>
<sent id="1198">
poledník	poledník	k1gInSc1	poledník
</sent>
<sent id="1199">
polohová	polohový	k2eAgFnSc1d1	polohová
přesnost	přesnost	k1gFnSc1	přesnost
</sent>
<sent id="1200">
polygon	polygon	k1gInSc1	polygon
</sent>
<sent id="1201">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
polygrafie	polygrafie	k1gFnSc1	polygrafie
</sent>
<sent id="1202">
popisná	popisný	k2eAgNnPc1d1	popisné
data	datum	k1gNnPc1	datum
</sent>
<sent id="1203">
potvrzení	potvrzení	k1gNnPc4	potvrzení
geometrického	geometrický	k2eAgInSc2d1	geometrický
plánu	plán	k1gInSc2	plán
</sent>
<sent id="1204">
povýšení	povýšení	k1gNnSc6	povýšení
</sent>
<sent id="1205">
pozemek	pozemek	k1gInSc1	pozemek
</sent>
<sent id="1206">
pozemková	pozemkový	k2eAgFnSc1d1	pozemková
kniha	kniha	k1gFnSc1	kniha
</sent>
<sent id="1208">
pozemkový	pozemkový	k2eAgInSc1d1	pozemkový
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
</sent>
<sent id="1209">
pozemkový	pozemkový	k2eAgInSc4d1	pozemkový
katastr	katastr	k1gInSc4	katastr
</sent>
<sent id="1210">
pracovní	pracovní	k2eAgFnSc1d1	pracovní
stanice	stanice	k1gFnSc1	stanice
</sent>
<sent id="1211">
pravá	pravý	k2eAgFnSc1d1	pravá
hodnota	hodnota	k1gFnSc1	hodnota
</sent>
<sent id="1212">
primární	primární	k2eAgInSc4d1	primární
systém	systém	k1gInSc4	systém
</sent>
<sent id="1213">
produkt	produkt	k1gInSc1	produkt
s	s	k7c7	
přidanou	přidaný	k2eAgFnSc7d1	přidaná
hodnotou	hodnota	k1gFnSc7	hodnota
</sent>
<sent id="1214">
profil	profil	k1gInSc1	profil
</sent>
<sent id="1215">
prostorová	prostorový	k2eAgFnSc1d1	prostorová
analýza	analýza	k1gFnSc1	analýza
</sent>
<sent id="1216">
prostorová	prostorový	k2eAgFnSc1d1	prostorová
data	datum	k1gNnSc2	datum
</sent>
<sent id="1217">
prostorová	prostorový	k2eAgFnSc1d1	prostorová
informace	informace	k1gFnSc1	informace
</sent>
<sent id="1219">
prostorové	prostorový	k2eAgNnSc4d1	prostorové
rozlišení	rozlišení	k1gNnSc4	rozlišení
</sent>
<sent id="1220">
prostorový	prostorový	k2eAgInSc1d1	prostorový
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
</sent>
<sent id="1222">
přejímací	přejímací	k2eAgFnPc4d1	přejímací
podmínky	podmínka	k1gFnPc4	podmínka
</sent>
<sent id="1223">
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
přesnost	přesnost	k1gFnSc4	přesnost
měření	měření	k1gNnSc2	měření
</sent>
<sent id="1224">
převzorkování	převzorkování	k1gNnSc4	převzorkování
</sent>
<sent id="1225">
přichycení	přichycení	k1gNnSc4	přichycení
</sent>
<sent id="1226">
přímá	přímý	k2eAgFnSc1d1	přímá
poloha	poloha	k1gFnSc1	poloha
</sent>
<sent id="1227">
přímý	přímý	k2eAgInSc1d1	přímý
prostorový	prostorový	k2eAgInSc1d1	prostorový
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
</sent>
<sent id="1228">
původní	původní	k2eAgFnSc1d1	původní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="1229">
rasterizace	rasterizace	k1gFnSc1	rasterizace
</sent>
<sent id="5990">
nárazník	nárazník	k1gInSc1	nárazník
</sent>
<sent id="1231">
rastrová	rastrový	k2eAgFnSc1d1	rastrová
data	datum	k1gNnSc2	datum
</sent>
<sent id="1232">
rastrová	rastrový	k2eAgFnSc1d1	rastrová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="1233">
reambulace	reambulace	k1gFnSc1	reambulace
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="1234">
reambulovaný	reambulovaný	k2eAgInSc4d1	reambulovaný
katastr	katastr	k1gInSc4	katastr
</sent>
<sent id="1235">
relační	relační	k2eAgFnSc1d1	relační
databáze	databáze	k1gFnSc1	databáze
</sent>
<sent id="1236">
definiční	definiční	k2eAgInSc4d1	definiční
bod	bod	k1gInSc4	bod
</sent>
<sent id="1237">
reprografie	reprografie	k1gFnSc1	reprografie
</sent>
<sent id="1238">
rozlišení	rozlišení	k1gNnSc4	rozlišení
</sent>
<sent id="1241">
ruční	ruční	k2eAgFnSc1d1	ruční
digitalizace	digitalizace	k1gFnSc1	digitalizace
</sent>
<sent id="1242">
sada	sada	k1gFnSc1	sada
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1243">
sběr	sběr	k1gInSc1	sběr
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1244">
sekční	sekční	k2eAgFnSc1d1	sekční
čára	čára	k1gFnSc1	čára
</sent>
<sent id="1246">
sémantická	sémantický	k2eAgFnSc1d1	sémantická
konzistence	konzistence	k1gFnSc1	konzistence
</sent>
<sent id="1248">
simulace	simulace	k1gFnSc2	simulace
</sent>
<sent id="1249">
skenování	skenování	k1gNnSc4	skenování
</sent>
<sent id="1250">
skutečný	skutečný	k2eAgInSc4d1	skutečný
rozměr	rozměr	k1gInSc4	rozměr
</sent>
<sent id="1251">
slučka	slučka	k1gFnSc1	slučka
</sent>
<sent id="1253">
směrodatná	směrodatný	k2eAgFnSc1d1	směrodatná
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="1254">
soubor	soubor	k1gInSc1	soubor
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1255">
soubor	soubor	k1gInSc1	soubor
geodetických	geodetický	k2eAgFnPc2d1	geodetická
informací	informace	k1gFnPc2	informace
</sent>
<sent id="1256">
soubor	soubor	k1gInSc1	soubor
popisných	popisný	k2eAgFnPc2d1	popisná
informací	informace	k1gFnPc2	informace
</sent>
<sent id="1257">
souřadnicový	souřadnicový	k2eAgInSc4d1	souřadnicový
systém	systém	k1gInSc4	systém
</sent>
<sent id="1258">
Souřadnicový	souřadnicový	k2eAgInSc4d1	souřadnicový
systém	systém	k1gInSc4	systém
Jednotné	jednotný	k2eAgFnSc2d1	jednotná
trigonometrické	trigonometrický	k2eAgFnSc2d1	trigonometrická
sítě	síť	k1gFnSc2	síť
katastrální	katastrální	k2eAgFnSc2d1	katastrální
(	(	kIx(	
<g/>
S-JTSK	S-JTSK	k1gFnSc2	S-JTSK
<g/>
)	)	kIx)	
</sent>
<sent id="1259">
spektrální	spektrální	k2eAgFnSc1d1	spektrální
charakteristika	charakteristika	k1gFnSc1	charakteristika
</sent>
<sent id="1260">
spektrální	spektrální	k2eAgFnSc4d1	spektrální
odrazivost	odrazivost	k1gFnSc4	odrazivost
</sent>
<sent id="1261">
správce	správka	k1gFnSc6	správka
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1262">
stabilní	stabilní	k2eAgInSc4d1	stabilní
katastr	katastr	k1gInSc4	katastr
</sent>
<sent id="1263">
stavební	stavební	k2eAgFnSc1d1	stavební
parcela	parcela	k1gFnSc1	parcela
</sent>
<sent id="1264">
stěna	stěna	k1gFnSc1	stěna
</sent>
<sent id="1265">
strukturovaný	strukturovaný	k2eAgInSc1d1	strukturovaný
dotazovací	dotazovací	k2eAgInSc1d1	dotazovací
jazyk	jazyk	k1gInSc1	jazyk
</sent>
<sent id="1266">
super	super	k1gInPc2	super
<g/>
(	(	kIx(	
<g/>
im	im	k?	
<g/>
)	)	kIx)	
<g/>
pozice	pozice	k1gFnSc1	pozice
</sent>
<sent id="1268">
systém	systém	k1gInSc1	systém
řízení	řízení	k1gNnSc2	řízení
báze	báze	k1gFnSc1	báze
dat	datum	k1gNnPc2	datum
</sent>
<sent id="3004">
ASPI	ASPI	kA	
</sent>
<sent id="3005">
AT	AT	kA	
</sent>
<sent id="1271">
tematická	tematický	k2eAgFnSc1d1	tematická
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="1272">
teoretická	teoretický	k2eAgFnSc1d1	teoretická
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="1273">
těsnost	těsnost	k1gFnSc1	těsnost
shody	shoda	k1gFnSc2	shoda
měření	měření	k1gNnSc2	měření
</sent>
<sent id="1274">
textura	textura	k1gFnSc1	textura
</sent>
<sent id="3742">
držba	držba	k1gFnSc1	držba
</sent>
<sent id="1277">
tolerance	tolerance	k1gFnSc2	tolerance
</sent>
<sent id="1278">
topografická	topografický	k2eAgFnSc1d1	topografická
data	datum	k1gNnSc2	datum
</sent>
<sent id="1279">
topografická	topografický	k2eAgFnSc1d1	topografická
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="1280">
topologické	topologický	k2eAgNnSc1d1	topologické
primitivum	primitivum	k?	
</sent>
<sent id="1281">
topologie	topologie	k1gFnSc2	topologie
</sent>
<sent id="1282">
třída	třída	k1gFnSc1	třída
</sent>
<sent id="1283">
třída	třída	k1gFnSc1	třída
přesnosti	přesnost	k1gFnSc2	přesnost
mapování	mapování	k1gNnSc2	mapování
</sent>
<sent id="1285">
údržba	údržba	k1gFnSc1	údržba
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="1287">
ukládání	ukládání	k1gNnSc2	ukládání
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1289">
úprava	úprava	k1gFnSc1	úprava
</sent>
<sent id="1290">
určení	určení	k1gNnSc6	určení
polohy	poloha	k1gFnSc2	poloha
</sent>
<sent id="1291">
úředně	úředně	k6eAd1	
oprávněný	oprávněný	k2eAgMnSc1d1	oprávněný
zeměměřický	zeměměřický	k2eAgMnSc1d1	zeměměřický
inženýr	inženýr	k1gMnSc1	inženýr
</sent>
<sent id="1292">
uzel	uzel	k1gInSc1	uzel
</sent>
<sent id="1293">
vektor	vektor	k1gInSc1	vektor
</sent>
<sent id="1294">
vektorizace	vektorizace	k1gFnSc1	vektorizace
</sent>
<sent id="1295">
vektorová	vektorový	k2eAgFnSc1d1	vektorová
data	datum	k1gNnSc2	datum
</sent>
<sent id="1296">
vektorová	vektorový	k2eAgFnSc1d1	vektorová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="1298">
vizualizace	vizualizace	k1gFnSc1	vizualizace
</sent>
<sent id="1299">
vlícovací	vlícovací	k2eAgInSc4d1	vlícovací
bod	bod	k1gInSc4	bod
</sent>
<sent id="1300">
vojenská	vojenský	k2eAgFnSc1d1	vojenská
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="1301">
vojenské	vojenský	k2eAgNnSc4d1	vojenské
kartografické	kartografický	k2eAgNnSc4d1	kartografické
dílo	dílo	k1gNnSc4	dílo
</sent>
<sent id="1302">
vojenský	vojenský	k2eAgInSc4d1	vojenský
zeměpisný	zeměpisný	k2eAgInSc4d1	zeměpisný
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="1303">
voxel	voxel	k1gInSc1	voxel
</sent>
<sent id="1304">
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="1305">
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="4230">
výsledek	výsledek	k1gInSc1	výsledek
měření	měření	k1gNnSc2	měření
</sent>
<sent id="1307">
vydání	vydání	k1gNnPc4	vydání
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="1308">
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1309">
georeferencování	georeferencování	k1gNnSc4	georeferencování
</sent>
<sent id="1310">
výměnný	výměnný	k2eAgInSc4d1	výměnný
formát	formát	k1gInSc4	formát
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1311">
výměra	výměra	k1gFnSc1	výměra
parcely	parcela	k1gFnSc2	parcela
</sent>
<sent id="1312">
vytvoření	vytvoření	k1gNnSc4	vytvoření
reálného	reálný	k2eAgInSc2d1	reálný
obrazu	obraz	k1gInSc2	obraz
počítačem	počítač	k1gInSc7	počítač
</sent>
<sent id="4741">
aritmetický	aritmetický	k2eAgInSc4d1	aritmetický
průměr	průměr	k1gInSc4	průměr
</sent>
<sent id="1315">
vzhled	vzhled	k1gInSc1	vzhled
</sent>
<sent id="1316">
Základní	základní	k2eAgFnSc1d1	základní
báze	báze	k1gFnSc1	báze
geografických	geografický	k2eAgNnPc2d1	geografické
dat	datum	k1gNnPc2	datum
(	(	kIx(	
<g/>
ZABAGED	ZABAGED	kA	
<g/>
®	®	k?	
<g/>
)	)	kIx)	
</sent>
<sent id="4372">
základní	základní	k2eAgInSc4d1	základní
úhel	úhel	k1gInSc4	úhel
</sent>
<sent id="1318">
zápisník	zápisník	k1gInSc1	zápisník
(	(	kIx(	
<g/>
měřených	měřený	k2eAgInPc2d1	měřený
údajů	údaj	k1gInPc2	údaj
<g/>
)	)	kIx)	
</sent>
<sent id="1319">
zdroj	zdroj	k1gInSc1	zdroj
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1320">
shlazení	shlazení	k1gNnSc4	shlazení
</sent>
<sent id="1321">
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
</sent>
<sent id="1323">
zpracování	zpracování	k1gNnSc2	zpracování
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1500">
audit	audit	k1gInSc1	audit
</sent>
<sent id="1501">
cíl	cíl	k1gInSc1	cíl
kvality	kvalita	k1gFnSc2	kvalita
</sent>
<sent id="1502">
dodavatel	dodavatel	k1gMnSc1	dodavatel
</sent>
<sent id="1503">
dokument	dokument	k1gInSc1	dokument
</sent>
<sent id="3347">
délková	délkový	k2eAgFnSc1d1	délková
deformace	deformace	k1gFnSc1	deformace
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3349">
charakteristika	charakteristika	k1gFnSc1	charakteristika
přesnosti	přesnost	k1gFnSc2	přesnost
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="1507">
management	management	k1gInSc1	management
kvality	kvalita	k1gFnSc2	kvalita
</sent>
<sent id="1508">
metrologická	metrologický	k2eAgFnSc1d1	metrologická
konfirmace	konfirmace	k1gFnSc1	konfirmace
</sent>
<sent id="1509">
měřicí	měřice	k1gFnSc7	měřice
zařízení	zařízení	k1gNnSc2	zařízení
</sent>
<sent id="1510">
neshoda	neshoda	k1gFnSc1	neshoda
</sent>
<sent id="3348">
grafická	grafický	k2eAgFnSc1d1	grafická
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="1513">
ověřování	ověřování	k1gNnSc4	ověřování
</sent>
<sent id="1514">
politika	politika	k1gFnSc1	politika
kvality	kvalita	k1gFnSc2	kvalita
</sent>
<sent id="1517">
produkt	produkt	k1gInSc1	produkt
</sent>
<sent id="7096">
proces	proces	k1gInSc1	proces
měření	měření	k1gNnSc2	měření
</sent>
<sent id="1519">
projekt	projekt	k1gInSc1	projekt
</sent>
<sent id="1520">
přezkoumání	přezkoumání	k1gNnSc4	přezkoumání
</sent>
<sent id="1521">
řízení	řízení	k1gNnSc6	řízení
kvality	kvalita	k1gFnSc2	kvalita
</sent>
<sent id="1524">
systém	systém	k1gInSc1	systém
</sent>
<sent id="1525">
systém	systém	k1gInSc1	systém
managementu	management	k1gInSc2	management
</sent>
<sent id="1526">
systém	systém	k1gInSc1	systém
managementu	management	k1gInSc2	management
kvality	kvalita	k1gFnSc2	kvalita
</sent>
<sent id="3350">
chyba	chyba	k1gFnSc1	chyba
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="1529">
vada	vada	k1gFnSc1	vada
</sent>
<sent id="1530">
validace	validace	k1gFnSc1	validace
</sent>
<sent id="1531">
zákazník	zákazník	k1gMnSc1	zákazník
</sent>
<sent id="1534">
způsobilost	způsobilost	k1gFnSc1	způsobilost
</sent>
<sent id="2000">
písemný	písemný	k2eAgInSc4d1	písemný
operát	operát	k1gInSc4	operát
pozemkového	pozemkový	k2eAgInSc2d1	pozemkový
katastru	katastr	k1gInSc2	katastr
</sent>
<sent id="2001">
hon	hon	k1gInSc1	hon
</sent>
<sent id="2002">
analogová	analogový	k2eAgFnSc1d1	analogová
katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="2004">
areál	areál	k1gInSc1	areál
budov	budova	k1gFnPc2	budova
</sent>
<sent id="2006">
báňská	báňský	k2eAgFnSc1d1	báňská
kniha	kniha	k1gFnSc1	kniha
</sent>
<sent id="2007">
budova	budova	k1gFnSc1	budova
</sent>
<sent id="2010">
číselný	číselný	k2eAgInSc4d1	číselný
kód	kód	k1gInSc4	kód
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
</sent>
<sent id="2011">
číselný	číselný	k2eAgInSc4d1	číselný
výpočet	výpočet	k1gInSc4	výpočet
výměry	výměra	k1gFnSc2	výměra
parcely	parcela	k1gFnSc2	parcela
</sent>
<sent id="2012">
číslo	číslo	k1gNnSc4	číslo
budovy	budova	k1gFnSc2	budova
</sent>
<sent id="2013">
číslo	číslo	k1gNnSc4	číslo
evidenčního	evidenční	k2eAgInSc2d1	evidenční
listu	list	k1gInSc2	list
</sent>
<sent id="3856">
styková	stykový	k2eAgFnSc1d1	styková
čára	čára	k1gFnSc1	čára
</sent>
<sent id="2016">
číslování	číslování	k1gNnPc2	číslování
parcel	parcela	k1gFnPc2	parcela
</sent>
<sent id="2017">
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
sáh	sáh	k1gInSc4	sáh
</sent>
<sent id="2018">
díl	díl	k1gInSc1	díl
parcely	parcela	k1gFnSc2	parcela
</sent>
<sent id="2019">
dočasné	dočasný	k2eAgNnSc1d1	dočasné
užívání	užívání	k1gNnSc1	užívání
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="3741">
železniční	železniční	k2eAgFnSc1d1	železniční
kniha	kniha	k1gFnSc1	kniha
</sent>
<sent id="2021">
držitel	držitel	k1gMnSc1	držitel
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="2022">
evidenční	evidenční	k2eAgInSc4d1	evidenční
číslo	číslo	k1gNnSc1	číslo
budovy	budova	k1gFnSc2	budova
</sent>
<sent id="2023">
evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
obec	obec	k1gFnSc1	obec
</sent>
<sent id="2024">
hektar	hektar	k1gInSc1	hektar
</sent>
<sent id="2027">
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
obec	obec	k1gFnSc1	obec
</sent>
<sent id="2028">
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
plocha	plocha	k1gFnSc1	plocha
obce	obec	k1gFnSc2	obec
</sent>
<sent id="2029">
hranice	hranice	k1gFnSc2	hranice
druhů	druh	k1gMnPc2	druh
pozemků	pozemek	k1gInPc2	pozemek
</sent>
<sent id="2030">
hranice	hranice	k1gFnSc1	hranice
držby	držba	k1gFnSc2	držba
</sent>
<sent id="2031">
hranice	hranice	k1gFnSc1	hranice
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
</sent>
<sent id="2032">
hranice	hranice	k1gFnSc1	hranice
územní	územní	k2eAgFnSc2d1	územní
správní	správní	k2eAgFnSc2d1	správní
jednotky	jednotka	k1gFnSc2	jednotka
</sent>
<sent id="2033">
charakteristika	charakteristikon	k1gNnPc4	charakteristikon
druhů	druh	k1gMnPc2	druh
pozemků	pozemek	k1gInPc2	pozemek
</sent>
<sent id="2034">
individuální	individuální	k2eAgInSc4d1	individuální
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
</sent>
<sent id="2036">
katastrální	katastrální	k2eAgInSc4d1	katastrální
jitro	jitro	k1gNnSc1	jitro
</sent>
<sent id="2037">
katastrální	katastrální	k2eAgInSc1d1	katastrální
výtěžek	výtěžek	k1gInSc1	výtěžek
</sent>
<sent id="2038">
kmenové	kmenový	k2eAgNnSc1d1	kmenové
parcelní	parcelní	k2eAgNnSc1d1	parcelní
číslo	číslo	k1gNnSc1	číslo
</sent>
<sent id="2040">
komasační	komasační	k2eAgFnSc1d1	komasační
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="2041">
komposesorát	komposesorát	k1gInSc1	komposesorát
</sent>
<sent id="2042">
korec	korec	k1gInSc1	korec
</sent>
<sent id="2045">
nestálá	stálý	k2eNgFnSc1d1	nestálá
hranice	hranice	k1gFnSc1	hranice
</sent>
<sent id="2046">
neznatelná	znatelný	k2eNgFnSc1d1	neznatelná
hranice	hranice	k1gFnSc1	hranice
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="2047">
operáty	operát	k1gInPc4	operát
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="2048">
operát	operát	k1gInSc1	operát
jednotné	jednotný	k2eAgFnSc2d1	jednotná
evidence	evidence	k1gFnSc2	evidence
půdy	půda	k1gFnSc2	půda
</sent>
<sent id="2049">
operát	operát	k1gInSc1	operát
pozemkového	pozemkový	k2eAgInSc2d1	pozemkový
katastru	katastr	k1gInSc2	katastr
</sent>
<sent id="2050">
orientační	orientační	k2eAgFnPc1d1	orientační
číslo	číslo	k1gNnSc4	číslo
budovy	budova	k1gFnSc2	budova
</sent>
<sent id="2051">
označování	označování	k1gNnSc2	označování
hranic	hranice	k1gFnPc2	hranice
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="2052">
parcelní	parcelní	k2eAgInSc4d1	parcelní
číslo	číslo	k1gNnSc1	číslo
</sent>
<sent id="2053">
parcelní	parcelní	k2eAgInSc4d1	parcelní
protokol	protokol	k1gInSc4	protokol
</sent>
<sent id="2054">
parifikát	parifikát	k1gInSc1	parifikát
</sent>
<sent id="2056">
poddělené	poddělený	k2eAgNnSc1d1	poddělený
parcelní	parcelní	k2eAgNnSc1d1	parcelní
číslo	číslo	k1gNnSc1	číslo
</sent>
<sent id="2057">
popisné	popisný	k2eAgFnPc1d1	popisná
číslo	číslo	k1gNnSc4	číslo
budovy	budova	k1gFnSc2	budova
</sent>
<sent id="2059">
pozemnostní	pozemnostní	k2eAgInSc4d1	pozemnostní
arch	arch	k1gInSc4	arch
</sent>
<sent id="2060">
přespolní	přespolní	k2eAgInSc4d1	přespolní
pozemek	pozemek	k1gInSc4	pozemek
</sent>
<sent id="2061">
převzatá	převzatý	k2eAgFnSc1d1	převzatá
hranice	hranice	k1gFnSc1	hranice
</sent>
<sent id="2062">
příruční	příruční	k2eAgFnSc1d1	příruční
(	(	kIx(	
<g/>
indikační	indikační	k2eAgFnSc1d1	indikační
<g/>
)	)	kIx)	
katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="2063">
rozhraní	rozhraní	k1gNnSc6	rozhraní
způsobu	způsob	k1gInSc2	způsob
využití	využití	k1gNnSc2	využití
pozemků	pozemek	k1gInPc2	pozemek
</sent>
<sent id="2064">
sáh	sáh	k1gInSc1	sáh
</sent>
<sent id="2065">
sbírka	sbírka	k1gFnSc1	sbírka
listin	listina	k1gFnPc2	listina
</sent>
<sent id="2066">
silniční	silniční	k2eAgFnSc1d1	silniční
kniha	kniha	k1gFnSc1	kniha
</sent>
<sent id="2067">
sporná	sporný	k2eAgFnSc1d1	sporná
hranice	hranice	k1gFnSc1	hranice
</sent>
<sent id="7097">
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
hranice	hranice	k1gFnSc1	hranice
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
</sent>
<sent id="2069">
spůlná	spůlný	k2eAgFnSc1d1	spůlný
(	(	kIx(	
<g/>
společná	společný	k2eAgFnSc1d1	společná
<g/>
)	)	kIx)	
parcela	parcela	k1gFnSc1	parcela
</sent>
<sent id="2071">
údržba	údržba	k1gFnSc1	údržba
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="2072">
údržba	údržba	k1gFnSc1	údržba
evidenční	evidenční	k2eAgFnSc2d1	evidenční
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="2073">
údržba	údržba	k1gFnSc1	údržba
písemného	písemný	k2eAgInSc2d1	písemný
operátu	operát	k1gInSc2	operát
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="2074">
údržba	údržba	k1gFnSc1	údržba
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="2075">
údržba	údržba	k1gFnSc1	údržba
pracovní	pracovní	k2eAgFnSc2d1	pracovní
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="2076">
vedení	vedení	k1gNnSc6	vedení
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="2077">
vedení	vedení	k1gNnSc6	vedení
souboru	soubor	k1gInSc2	soubor
geodetických	geodetický	k2eAgFnPc2d1	geodetická
informací	informace	k1gFnPc2	informace
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="2078">
vedení	vedení	k1gNnSc6	vedení
souboru	soubor	k1gInSc2	soubor
popisných	popisný	k2eAgFnPc2d1	popisná
informací	informace	k1gFnPc2	informace
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="2079">
vlastnická	vlastnický	k2eAgFnSc1d1	vlastnická
hranice	hranice	k1gFnSc1	hranice
</sent>
<sent id="2080">
vložka	vložka	k1gFnSc1	vložka
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
knihy	kniha	k1gFnSc2	kniha
</sent>
<sent id="2081">
vodní	vodní	k2eAgFnSc1d1	vodní
kniha	kniha	k1gFnSc1	kniha
</sent>
<sent id="2082">
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
</sent>
<sent id="3003">
APV	APV	kA	
</sent>
<sent id="2500">
paralaktický	paralaktický	k2eAgInSc4d1	paralaktický
článek	článek	k1gInSc4	článek
s	s	k7c7	
vodorovnou	vodorovný	k2eAgFnSc7d1	vodorovná
základnovou	základnový	k2eAgFnSc7d1	základnová
latí	lať	k1gFnSc7	lať
</sent>
<sent id="2501">
autentifikace	autentifikace	k1gFnSc1	autentifikace
</sent>
<sent id="2502">
bodové	bodový	k2eAgFnSc2d1	bodová
pole	pole	k1gFnSc2	pole
</sent>
<sent id="2503">
bod	bod	k1gInSc1	bod
podrobného	podrobný	k2eAgNnSc2d1	podrobné
polohového	polohový	k2eAgNnSc2d1	polohové
bodového	bodový	k2eAgNnSc2d1	bodové
pole	pole	k1gNnSc2	pole
</sent>
<sent id="2504">
bod	bod	k1gInSc1	bod
podrobného	podrobný	k2eAgNnSc2d1	podrobné
výškového	výškový	k2eAgNnSc2d1	výškové
bodového	bodový	k2eAgNnSc2d1	bodové
pole	pole	k1gNnSc2	pole
</sent>
<sent id="2505">
dělení	dělení	k1gNnSc2	dělení
na	na	k7c6	
rámu	rám	k1gInSc6	rám
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="2506">
digitalizace	digitalizace	k1gFnSc1	digitalizace
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="2507">
doplňková	doplňkový	k2eAgFnSc1d1	doplňková
vrstevnice	vrstevnice	k1gFnSc1	vrstevnice
</sent>
<sent id="2508">
geodetický	geodetický	k2eAgInSc4d1	geodetický
bod	bod	k1gInSc4	bod
</sent>
<sent id="2509">
geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
</sent>
<sent id="2510">
měřítkové	měřítkový	k2eAgNnSc1d1	měřítkové
číslo	číslo	k1gNnSc1	číslo
</sent>
<sent id="2511">
podrobné	podrobný	k2eAgNnSc4d1	podrobné
bodové	bodový	k2eAgNnSc4d1	bodové
pole	pole	k1gNnSc4	pole
</sent>
<sent id="2512">
pomocná	pomocný	k2eAgFnSc1d1	pomocná
vrstevnice	vrstevnice	k1gFnSc1	vrstevnice
</sent>
<sent id="2513">
trvale	trvale	k6eAd1	
signalizovaný	signalizovaný	k2eAgInSc4d1	signalizovaný
bod	bod	k1gInSc4	bod
</sent>
<sent id="2514">
trvale	trvale	k6eAd1	
stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
bod	bod	k1gInSc1	bod
</sent>
<sent id="2515">
vrstevnice	vrstevnice	k1gFnSc1	vrstevnice
</sent>
<sent id="2517">
základní	základní	k2eAgFnSc4d1	základní
bodové	bodový	k2eAgNnSc4d1	bodové
pole	pole	k1gNnSc4	pole
(	(	kIx(	
<g/>
ZBP	ZBP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="2518">
základní	základní	k2eAgFnSc1d1	základní
vrstevnice	vrstevnice	k1gFnSc1	vrstevnice
</sent>
<sent id="2519">
zhušťovací	zhušťovací	k2eAgInSc4d1	zhušťovací
bod	bod	k1gInSc4	bod
</sent>
<sent id="6457">
sklonový	sklonový	k2eAgInSc4d1	sklonový
nomogram	nomogram	k1gInSc4	nomogram
</sent>
<sent id="3007">
BAAT	BAAT	kA	
</sent>
<sent id="3008">
BERNESE	BERNESE	kA	
</sent>
<sent id="3009">
BEV	BEV	kA	
</sent>
<sent id="3010">
BIH	BIH	kA	
</sent>
<sent id="3012">
BKG	BKG	kA	
</sent>
<sent id="3014">
Bpv	Bpv	k1gFnSc1	Bpv
</sent>
<sent id="3017">
CAGI	CAGI	kA	
</sent>
<sent id="3018">
CERCO	CERCO	kA	
</sent>
<sent id="3020">
CLGE	CLGE	kA	
</sent>
<sent id="3021">
CORINE	CORINE	kA	
</sent>
<sent id="3022">
CTRS	CTRS	kA	
</sent>
<sent id="3023">
CZEPOS	CZEPOS	kA	
</sent>
<sent id="3026">
ČNI	čnět	k5eAaImRp2nS	
</sent>
<sent id="3028">
ČSGK	ČSGK	kA	
</sent>
<sent id="3029">
ČSN	ČSN	kA	
</sent>
<sent id="3030">
ČSNS	ČSNS	kA	
</sent>
<sent id="3035">
DATAZ	DATAZ	kA	
</sent>
<sent id="3036">
DGPS	DGPS	kA	
</sent>
<sent id="3037">
DKM	DKM	kA	
</sent>
<sent id="3038">
DMA	dmout	k5eAaImSgInS	
</sent>
<sent id="3039">
DMP	DMP	kA	
</sent>
<sent id="3040">
DMR	DMR	kA	
</sent>
<sent id="3041">
DMT	DMT	kA	
</sent>
<sent id="3042">
DMÚ	DMÚ	kA	
</sent>
<sent id="3043">
DOPNUL	dopnout	k5eAaPmAgInS	
</sent>
<sent id="3044">
DORIS	DORIS	kA	
</sent>
<sent id="3045">
DOS	DOS	kA	
</sent>
<sent id="3046">
DP	DP	kA	
</sent>
<sent id="3047">
dpi	dpi	kA	
</sent>
<sent id="3049">
DREF	DREF	kA	
</sent>
<sent id="3050">
DRG	DRG	kA	
</sent>
<sent id="3052">
ED87	ED87	k1gFnSc1	ED87
</sent>
<sent id="3053">
EN	EN	kA	
</sent>
<sent id="3054">
ETRF	ETRF	kA	
</sent>
<sent id="3055">
ETRS	ETRS	kA	
</sent>
<sent id="3056">
EULIS	EULIS	kA	
</sent>
<sent id="3057">
EUREF	EUREF	kA	
</sent>
<sent id="3058">
EuroGeographics	EuroGeographics	k1gInSc1	EuroGeographics
</sent>
<sent id="3059">
EUROGI	EUROGI	kA	
</sent>
<sent id="3060">
EuroSDR	EuroSDR	k1gFnSc1	EuroSDR
</sent>
<sent id="3061">
EUVN	EUVN	kA	
</sent>
<sent id="3062">
FIG	FIG	kA	
</sent>
<sent id="3063">
FNM	FNM	kA	
</sent>
<sent id="3065">
FSv	FSv	k1gFnSc1	FSv
</sent>
<sent id="3069">
GEODYN	GEODYN	kA	
</sent>
<sent id="3070">
GEONAMES	GEONAMES	kA	
</sent>
<sent id="3071">
GeoSl	GeoSl	k1gInSc1	GeoSl
AČR	AČR	kA	
</sent>
<sent id="3072">
GI	GI	kA	
</sent>
<sent id="3073">
GIS	gis	k1gNnSc4	gis
</sent>
<sent id="3076">
GLONASS	GLONASS	kA	
</sent>
<sent id="3077">
GO	GO	kA	
</sent>
<sent id="3078">
GOPE	GOPE	kA	
</sent>
<sent id="3079">
GP	GP	kA	
</sent>
<sent id="3080">
GPS	GPS	kA	
</sent>
<sent id="3081">
GRS80	GRS80	k1gFnSc1	GRS80
</sent>
<sent id="3082">
GTK	GTK	kA	
</sent>
<sent id="3085">
HTÚP	HTÚP	kA	
</sent>
<sent id="3088">
IACS	IACS	kA	
</sent>
<sent id="3089">
IAG	IAG	kA	
</sent>
<sent id="3090">
ICA	ICA	kA	
</sent>
<sent id="3091">
ICRCM	ICRCM	kA	
</sent>
<sent id="3092">
IDMS	IDMS	kA	
</sent>
<sent id="3093">
IERS	IERS	kA	
</sent>
<sent id="3094">
IfAG	IfAG	k1gFnSc1	IfAG
</sent>
<sent id="3095">
IGN	IGN	kA	
</sent>
<sent id="3096">
IGS	IGS	kA	
</sent>
<sent id="3098">
IMIP	IMIP	kA	
</sent>
<sent id="3099">
IMU	IMU	kA	
</sent>
<sent id="3100">
IMW	IMW	kA	
</sent>
<sent id="3101">
INSPIRE	INSPIRE	kA	
</sent>
<sent id="3102">
ISKN	ISKN	kA	
</sent>
<sent id="3103">
ISO	ISO	kA	
</sent>
<sent id="3104">
ISPRS	ISPRS	kA	
</sent>
<sent id="3105">
ISÚ	ISÚ	kA	
</sent>
<sent id="3106">
ISVS	ISVS	kA	
</sent>
<sent id="3108">
ITRF	ITRF	kA	
</sent>
<sent id="3109">
ITRS	ITRS	kA	
</sent>
<sent id="3111">
IZGARD	IZGARD	kA	
</sent>
<sent id="3112">
JAGS	JAGS	kA	
</sent>
<sent id="3114">
JOG	JOG	kA	
</sent>
<sent id="3115">
JPÚ	JPÚ	kA	
</sent>
<sent id="3116">
JRC	JRC	kA	
</sent>
<sent id="3117">
JTSK	JTSK	kA	
</sent>
<sent id="3118">
JŽM	JŽM	kA	
</sent>
<sent id="3119">
KGK	KGK	kA	
</sent>
<sent id="3120">
KI	KI	kA	
ISVS	ISVS	kA	
</sent>
<sent id="3121">
KM	km	kA	
</sent>
<sent id="3122">
KM-D	KM-D	k1gFnSc1	KM-D
</sent>
<sent id="3123">
KN	KN	kA	
</sent>
<sent id="3124">
KO	KO	kA	
</sent>
<sent id="3126">
KPÚ	KPÚ	kA	
</sent>
<sent id="3128">
k.	k.	k?	
ú.	ú.	k?	
</sent>
<sent id="3129">
KZEN	KZEN	kA	
</sent>
<sent id="3130">
LAN	lano	k1gNnPc2	lano
</sent>
<sent id="3131">
LAGEOS	LAGEOS	kA	
</sent>
<sent id="3132">
Landsat	Landsat	k1gMnSc2	Landsat
TM	TM	kA	
</sent>
<sent id="3133">
LIS	lis	k1gInSc1	lis
</sent>
<sent id="3134">
LLR	LLR	kA	
</sent>
<sent id="3135">
LMS	LMS	kA	
</sent>
<sent id="3136">
LV	LV	kA	
</sent>
<sent id="3137">
MATKART	MATKART	kA	
</sent>
<sent id="3138">
MEGRIN	MEGRIN	kA	
</sent>
<sent id="3139">
MIDAS	Midas	k1gMnSc1	Midas
</sent>
<sent id="3140">
MIS	mísa	k1gFnPc2	mísa
</sent>
<sent id="3142">
ML	ml	kA	
</sent>
<sent id="3144">
NAVSTAR	NAVSTAR	kA	
</sent>
<sent id="3145">
NEMOFORUM	NEMOFORUM	kA	
</sent>
<sent id="3146">
NGII	NGII	kA	
</sent>
<sent id="3147">
NIMA	NIMA	k?	
</sent>
<sent id="3148">
NMA	NMA	kA	
</sent>
<sent id="3149">
NP	NP	kA	
</sent>
<sent id="3150">
NULRAD	NULRAD	kA	
</sent>
<sent id="3151">
NVF	NVF	kA	
</sent>
<sent id="3152">
OB	Ob	k1gInSc1	Ob
</sent>
<sent id="3153">
ODIS	ODIS	kA	
</sent>
<sent id="3154">
OEEPE	OEEPE	kA	
</sent>
<sent id="3155">
OS	OS	kA	
</sent>
<sent id="3157">
PB	PB	kA	
</sent>
<sent id="3158">
PBPP	PBPP	kA	
</sent>
<sent id="3159">
PČB	PČB	kA	
</sent>
<sent id="3161">
PF	PF	kA	
ČR	ČR	kA	
</sent>
<sent id="3162">
PK	PK	kA	
</sent>
<sent id="3163">
PN	PN	kA	
</sent>
<sent id="3164">
PNS	PNS	kA	
</sent>
<sent id="3165">
PPBP	PPBP	kA	
</sent>
<sent id="3168">
PVBP	PVBP	kA	
</sent>
<sent id="3169">
RDP	RDP	kA	
</sent>
<sent id="3171">
RETM	RETM	kA	
</sent>
<sent id="3172">
RINEX	RINEX	kA	
</sent>
<sent id="3173">
RPB	RPB	kA	
</sent>
<sent id="3174">
RTK	RTK	kA	
</sent>
<sent id="3175">
RZM	RZM	kA	
</sent>
<sent id="3176">
S-42	S-42	k1gFnSc1	S-42
</sent>
<sent id="3177">
S-	S-	k1gFnSc1	S-
<g/>
42	#num#	k4	
<g/>
/	/	kIx~	
<g/>
83	#num#	k4	
</sent>
<sent id="3178">
SABE	SABE	kA	
</sent>
<sent id="3180">
SGI	SGI	kA	
</sent>
<sent id="3181">
S-Gr	S-Gr	k1gInSc1	S-Gr
<g/>
95	#num#	k4	
</sent>
<sent id="3183">
SIS	SIS	kA	
</sent>
<sent id="3185">
S-JTSK	S-JTSK	k1gFnSc1	S-JTSK
</sent>
<sent id="3186">
S-JTSK	S-JTSK	k1gFnPc2	S-JTSK
<g/>
/	/	kIx~	
<g/>
95	#num#	k4	
</sent>
<sent id="3187">
SLOVGERENET	SLOVGERENET	kA	
</sent>
<sent id="3188">
SLR	SLR	kA	
</sent>
<sent id="3189">
SM	SM	kA	
5	#num#	k4	
</sent>
<sent id="3190">
SM	SM	kA	
5	#num#	k4	
R	R	kA	
</sent>
<sent id="3191">
SMD	SMD	kA	
</sent>
<sent id="3192">
SMO	SMO	kA	
5	#num#	k4	
</sent>
<sent id="3193">
SPI	spát	k5eAaImRp2nS	
</sent>
<sent id="3194">
SPOT	spot	k1gInSc1	spot
</sent>
<sent id="3196">
S-SK	S-SK	k1gFnSc1	S-SK
</sent>
<sent id="3199">
SW	SW	kA	
</sent>
<sent id="3200">
TB	TB	kA	
</sent>
<sent id="3201">
TC	tc	k0	
211	#num#	k4	
<g/>
/	/	kIx~	
<g/>
ISO	ISO	kA	
</sent>
<sent id="3202">
THM	THM	kA	
</sent>
<sent id="3203">
TI	ten	k3xDgMnPc1	
</sent>
<sent id="3204">
TIN	Tina	k1gFnPc2	Tina
</sent>
<sent id="3205">
TL	TL	kA	
</sent>
<sent id="3206">
TM	TM	kA	
</sent>
<sent id="3207">
TMM	TMM	kA	
</sent>
<sent id="3208">
TN	TN	kA	
</sent>
<sent id="3209">
TS	ts	k0	
</sent>
<sent id="3210">
ÚAZK	ÚAZK	kA	
</sent>
<sent id="3211">
UDZ	UDZ	kA	
</sent>
<sent id="3212">
UEGN	UEGN	kA	
</sent>
<sent id="3213">
UELN	UELN	kA	
</sent>
<sent id="3214">
ÚGKK	ÚGKK	kA	
SR	SR	kA	
</sent>
<sent id="3215">
ÚHDP	ÚHDP	kA	
</sent>
<sent id="3216">
UNIGRACE	UNIGRACE	kA	
</sent>
<sent id="3218">
ÚPO	Úpa	k1gFnSc5	Úpa
</sent>
<sent id="3220">
ÚPÚ	ÚPÚ	kA	
</sent>
<sent id="3221">
ÚP	Úpa	k1gFnPc2	Úpa
VÚC	VÚC	kA	
</sent>
<sent id="3222">
UT	UT	kA	
</sent>
<sent id="3223">
UTM	UTM	kA	
</sent>
<sent id="3224">
ÚVIS	ÚVIS	kA	
</sent>
<sent id="3225">
VGGFIS	VGGFIS	kA	
</sent>
<sent id="3227">
VGIS	VGIS	kA	
</sent>
<sent id="3228">
VISÚ	VISÚ	kA	
</sent>
<sent id="3229">
VKM	VKM	kA	
</sent>
<sent id="3232">
VMap	VMap	k1gInSc1	VMap
<g/>
1	#num#	k4	
</sent>
<sent id="3233">
VPN	VPN	kA	
</sent>
<sent id="3236">
VÚGTK	VÚGTK	kA	
</sent>
<sent id="3237">
VÚMOP	VÚMOP	kA	
</sent>
<sent id="3241">
WAN	WAN	kA	
</sent>
<sent id="3243">
WGS	WGS	kA	
84	#num#	k4	
</sent>
<sent id="3245">
WWW	WWW	kA	
</sent>
<sent id="3246">
ZAaPÚ	ZAaPÚ	k1gFnSc1	ZAaPÚ
</sent>
<sent id="3247">
ZABAGED	ZABAGED	kA	
<g/>
®	®	k?	
</sent>
<sent id="3248">
ZB	ZB	kA	
GIS	gis	k1gNnSc1	gis
</sent>
<sent id="3249">
ZE	z	k7c2	
</sent>
<sent id="3250">
ZhB	ZhB	k1gFnSc1	ZhB
</sent>
<sent id="3252">
ZM	ZM	kA	
ČR	ČR	kA	
</sent>
<sent id="3253">
ZMVM	ZMVM	kA	
</sent>
<sent id="3254">
ZNB	ZNB	kA	
</sent>
<sent id="3255">
ZPBP	ZPBP	kA	
</sent>
<sent id="3256">
ZPF	ZPF	kA	
</sent>
<sent id="3257">
ZPMZ	ZPMZ	kA	
</sent>
<sent id="3258">
ZTBP	ZTBP	kA	
</sent>
<sent id="3259">
ZÚ	zú	k0	
</sent>
<sent id="3260">
ZVBP	ZVBP	kA	
</sent>
<sent id="3263">
družice	družice	k1gFnSc1	družice
LANDSAT	LANDSAT	kA	
</sent>
<sent id="3265">
emisivita	emisivita	k1gFnSc1	emisivita
</sent>
<sent id="3266">
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
analýza	analýza	k1gFnSc1	analýza
</sent>
<sent id="3267">
frekvence	frekvence	k1gFnSc1	frekvence
skenování	skenování	k1gNnSc2	skenování
</sent>
<sent id="3269">
intenzita	intenzita	k1gFnSc1	intenzita
záření	záření	k1gNnSc2	záření
</sent>
<sent id="3271">
nadir	nadir	k1gInSc1	nadir
</sent>
<sent id="3275">
výška	výška	k1gFnSc1	výška
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
</sent>
<sent id="3276">
azimutální	azimutální	k2eAgFnSc1d1	azimutální
neperspektivní	perspektivní	k2eNgNnSc4d1	neperspektivní
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3277">
azimutální	azimutální	k2eAgInSc4d1	azimutální
zobrazení	zobrazení	k1gNnSc1	zobrazení
</sent>
<sent id="3279">
Benešovo	Benešův	k2eAgNnSc4d1	Benešovo
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3280">
Cassiniho	Cassini	k1gMnSc2	Cassini
zobrazení	zobrazení	k1gNnSc2	zobrazení
</sent>
<sent id="3281">
centrální	centrální	k2eAgFnSc1d1	centrální
projekce	projekce	k1gFnSc1	projekce
</sent>
<sent id="3282">
ekvatoreální	ekvatoreální	k2eAgFnSc1d1	ekvatoreální
poloha	poloha	k1gFnSc1	poloha
</sent>
<sent id="3283">
ekvidistantní	ekvidistantní	k2eAgFnSc1d1	ekvidistantní
azimutální	azimutální	k2eAgNnSc4d1	azimutální
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3284">
ekvidistantní	ekvidistantní	k2eAgInSc4d1	ekvidistantní
zobrazení	zobrazení	k1gNnSc1	zobrazení
</sent>
<sent id="3285">
ekvivalentní	ekvivalentní	k2eAgInSc4d1	ekvivalentní
zobrazení	zobrazení	k1gNnSc1	zobrazení
</sent>
<sent id="3286">
elipsa	elipsa	k1gFnSc1	elipsa
zkreslení	zkreslení	k1gNnSc2	zkreslení
</sent>
<sent id="3287">
Faschingovo	Faschingův	k2eAgNnSc4d1	Faschingův
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3288">
flexografie	flexografie	k1gFnSc1	flexografie
</sent>
<sent id="3289">
fotoreprodukce	fotoreprodukce	k1gFnSc1	fotoreprodukce
</sent>
<sent id="3290">
Gauss-Krügerovo	Gauss-Krügerův	k2eAgNnSc4d1	Gauss-Krügerovo
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3291">
globulární	globulární	k2eAgInSc4d1	globulární
zobrazení	zobrazení	k1gNnSc1	zobrazení
</sent>
<sent id="3292">
hlubotisk	hlubotisk	k1gInSc1	hlubotisk
</sent>
<sent id="3293">
hologram	hologram	k1gInSc1	hologram
</sent>
<sent id="3294">
chemigrafie	chemigrafie	k1gFnSc1	chemigrafie
</sent>
<sent id="3295">
izocylindrické	izocylindrický	k2eAgNnSc4d1	izocylindrický
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3296">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
projekce	projekce	k1gFnSc1	projekce
</sent>
<sent id="3297">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
předloha	předloha	k1gFnSc1	předloha
</sent>
<sent id="3298">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
typografie	typografie	k1gFnSc1	typografie
</sent>
<sent id="3299">
kartografické	kartografický	k2eAgNnSc4d1	kartografické
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3301">
konformní	konformní	k2eAgInSc4d1	konformní
zobrazení	zobrazení	k1gNnSc1	zobrazení
</sent>
<sent id="3302">
kopírování	kopírování	k1gNnSc2	kopírování
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="3303">
Křovákovo	Křovákův	k2eAgNnSc4d1	Křovákovo
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3304">
kuželové	kuželový	k2eAgNnSc4d1	kuželové
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3305">
Lambertovo	Lambertův	k2eAgNnSc1d1	Lambertovo
ekvivalentní	ekvivalentní	k2eAgNnSc1d1	ekvivalentní
kuželové	kuželový	k2eAgNnSc1d1	kuželové
zobrazení	zobrazení	k1gNnSc1	zobrazení
</sent>
<sent id="3306">
mapový	mapový	k2eAgInSc4d1	mapový
obraz	obraz	k1gInSc4	obraz
</sent>
<sent id="3307">
Mercatorovo	Mercatorův	k2eAgNnSc1d1	Mercatorovo
příčné	příčný	k2eAgNnSc1d1	příčné
univerzální	univerzální	k2eAgNnSc1d1	univerzální
zobrazení	zobrazení	k1gNnSc1	zobrazení
</sent>
<sent id="3308">
Mercatorovo	Mercatorův	k2eAgNnSc4d1	Mercatorovo
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3309">
mikrodenzitometr	mikrodenzitometr	k1gInSc1	mikrodenzitometr
</sent>
<sent id="3351">
chyba	chyba	k1gFnSc1	chyba
popisu	popis	k1gInSc2	popis
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3743">
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
nemovitost	nemovitost	k1gFnSc1	nemovitost
</sent>
<sent id="3318">
mnohokuželové	mnohokuželový	k2eAgNnSc4d1	mnohokuželový
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3319">
mikroštítek	mikroštítka	k1gFnPc2	mikroštítka
</sent>
<sent id="3320">
montáž	montáž	k1gFnSc1	montáž
</sent>
<sent id="3321">
nátisk	nátisk	k1gInSc1	nátisk
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3322">
nekonvenční	konvenčnět	k5eNaImIp3nS	
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3323">
ortografická	ortografický	k2eAgFnSc1d1	ortografická
projekce	projekce	k1gFnSc1	projekce
</sent>
<sent id="3324">
polyedrické	polyedrický	k2eAgNnSc4d1	polyedrický
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3325">
průtisk	průtisk	k1gInSc1	průtisk
</sent>
<sent id="3327">
referenční	referenční	k2eAgFnSc1d1	referenční
plocha	plocha	k1gFnSc1	plocha
</sent>
<sent id="3328">
reprodukce	reprodukce	k1gFnSc2	reprodukce
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="3329">
retuš	retuš	k1gFnSc1	retuš
</sent>
<sent id="3331">
rozměrová	rozměrový	k2eAgFnSc1d1	rozměrová
stálost	stálost	k1gFnSc1	stálost
</sent>
<sent id="3333">
stereografická	stereografický	k2eAgFnSc1d1	stereografická
projekce	projekce	k1gFnSc1	projekce
</sent>
<sent id="3334">
stereotypie	stereotypie	k1gFnSc1	stereotypie
</sent>
<sent id="3335">
světlotisk	světlotisk	k1gInSc1	světlotisk
</sent>
<sent id="3337">
technologie	technologie	k1gFnSc1	technologie
zpracování	zpracování	k1gNnSc2	zpracování
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3338">
termografie	termografie	k1gFnSc1	termografie
</sent>
<sent id="3339">
tisk	tisk	k1gInSc1	tisk
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="3340">
válcové	válcový	k2eAgNnSc4d1	válcové
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="3341">
volba	volba	k1gFnSc1	volba
zobrazení	zobrazení	k1gNnSc2	zobrazení
</sent>
<sent id="3342">
zobrazení	zobrazení	k1gNnSc2	zobrazení
v	v	k7c6	
normální	normální	k2eAgFnSc6d1	normální
poloze	poloha	k1gFnSc6	poloha
</sent>
<sent id="3343">
zobrazení	zobrazení	k1gNnSc2	zobrazení
v	v	k7c6	
obecné	obecný	k2eAgFnSc6d1	obecná
poloze	poloha	k1gFnSc6	poloha
</sent>
<sent id="3344">
zobrazení	zobrazení	k1gNnSc2	zobrazení
v	v	k7c6	
příčné	příčný	k2eAgFnSc6d1	příčná
poloze	poloha	k1gFnSc6	poloha
</sent>
<sent id="3345">
zobrazovací	zobrazovací	k2eAgFnSc1d1	zobrazovací
plocha	plocha	k1gFnSc1	plocha
</sent>
<sent id="3346">
zobrazovací	zobrazovací	k2eAgFnSc1d1	zobrazovací
rovina	rovina	k1gFnSc1	rovina
</sent>
<sent id="3352">
chyba	chyba	k1gFnSc1	chyba
z	z	k7c2	
reprodukce	reprodukce	k1gFnSc2	reprodukce
</sent>
<sent id="3353">
kartometrická	kartometrický	k2eAgFnSc1d1	kartometrický
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="3354">
kartometrie	kartometrie	k1gFnSc1	kartometrie
</sent>
<sent id="3355">
křivkoměr	křivkoměr	k1gInSc1	křivkoměr
</sent>
<sent id="3356">
měření	měření	k1gNnSc4	měření
délky	délka	k1gFnSc2	délka
na	na	k7c6	
mapě	mapa	k1gFnSc6	mapa
</sent>
<sent id="3357">
měřicí	měřicí	k2eAgFnSc1d1	měřicí
lupa	lupa	k1gFnSc1	lupa
</sent>
<sent id="3358">
nastavovací	nastavovací	k2eAgInSc4d1	nastavovací
odpichovátko	odpichovátko	k1gNnSc1	odpichovátko
</sent>
<sent id="3359">
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
deformace	deformace	k1gFnSc1	deformace
podložky	podložka	k1gFnSc2	podložka
</sent>
<sent id="3360">
plošná	plošný	k2eAgFnSc1d1	plošná
deformace	deformace	k1gFnSc1	deformace
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3361">
redukční	redukční	k2eAgInSc4d1	redukční
kružítko	kružítko	k1gNnSc1	kružítko
</sent>
<sent id="3363">
srážka	srážka	k1gFnSc1	srážka
fotografického	fotografický	k2eAgInSc2d1	fotografický
filmu	film	k1gInSc2	film
</sent>
<sent id="3364">
srážka	srážka	k1gFnSc1	srážka
papíru	papír	k1gInSc2	papír
</sent>
<sent id="3371">
veřejná	veřejný	k2eAgFnSc1d1	veřejná
listina	listina	k1gFnSc1	listina
</sent>
<sent id="3372">
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3373">
apogeum	apogeum	k1gNnSc4	apogeum
</sent>
<sent id="3374">
apsida	apsida	k1gFnSc1	apsida
</sent>
<sent id="3375">
argument	argument	k1gInSc1	argument
deklinace	deklinace	k1gFnSc2	deklinace
</sent>
<sent id="3376">
argument	argument	k1gInSc1	argument
perigea	perigeum	k1gNnSc2	perigeum
</sent>
<sent id="3377">
argument	argument	k1gInSc1	argument
šířky	šířka	k1gFnSc2	šířka
</sent>
<sent id="3379">
astázování	astázování	k1gNnSc4	astázování
</sent>
<sent id="3380">
asteroid	asteroid	k1gInSc1	asteroid
</sent>
<sent id="3381">
astrodynamika	astrodynamika	k1gFnSc1	astrodynamika
</sent>
<sent id="3382">
astroláb	astroláb	k1gInSc1	astroláb
</sent>
<sent id="3383">
astrokompas	astrokompas	k1gInSc1	astrokompas
</sent>
<sent id="3384">
astrometrie	astrometrie	k1gFnSc1	astrometrie
</sent>
<sent id="3385">
astronomická	astronomický	k2eAgFnSc1d1	astronomická
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3386">
astronomicko-gravimetrický	astronomicko-gravimetrický	k2eAgInSc4d1	astronomicko-gravimetrický
bod	bod	k1gInSc4	bod
</sent>
<sent id="3387">
astronomický	astronomický	k2eAgInSc4d1	astronomický
azimut	azimut	k1gInSc4	azimut
</sent>
<sent id="3388">
astronomický	astronomický	k2eAgInSc4d1	astronomický
bod	bod	k1gInSc4	bod
</sent>
<sent id="3389">
astronomie	astronomie	k1gFnSc2	astronomie
</sent>
<sent id="3390">
atmosféra	atmosféra	k1gFnSc1	atmosféra
</sent>
<sent id="3392">
azimut	azimut	k1gInSc1	azimut
</sent>
<sent id="3393">
barycentrum	barycentrum	k1gNnSc4	barycentrum
</sent>
<sent id="3394">
Besselův	Besselův	k2eAgInSc4d1	Besselův
rok	rok	k1gInSc4	rok
</sent>
<sent id="3395">
blok	blok	k1gInSc1	blok
družic	družice	k1gFnPc2	družice
GPS	GPS	kA	
-	-	kIx~	
NAVSTAR	NAVSTAR	kA	
</sent>
<sent id="3396">
blok	blok	k1gInSc1	blok
efemeridových	efemeridový	k2eAgFnPc2d1	efemeridový
zpráv	zpráva	k1gFnPc2	zpráva
</sent>
<sent id="3397">
bod	bod	k1gInSc1	bod
grafické	grafický	k2eAgFnSc2d1	grafická
triangulace	triangulace	k1gFnSc2	triangulace
</sent>
<sent id="3398">
bod	bod	k1gInSc1	bod
letního	letní	k2eAgInSc2d1	letní
slunovratu	slunovrat	k1gInSc2	slunovrat
</sent>
<sent id="3399">
bod	bod	k1gInSc1	bod
poledníkového	poledníkový	k2eAgInSc2d1	poledníkový
oblouku	oblouk	k1gInSc2	oblouk
</sent>
<sent id="3400">
bod	bod	k1gInSc1	bod
základního	základní	k2eAgNnSc2d1	základní
bodového	bodový	k2eAgNnSc2d1	bodové
pole	pole	k1gNnSc2	pole
</sent>
<sent id="3401">
Bouguerova	Bouguerův	k2eAgFnSc1d1	Bouguerův
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3402">
denní	denní	k2eAgFnSc1d1	denní
aberace	aberace	k1gFnSc1	aberace
</sent>
<sent id="3403">
excentrická	excentrický	k2eAgFnSc1d1	excentrická
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3404">
Fayeova	Fayeův	k2eAgFnSc1d1	Fayeův
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3405">
geodetická	geodetický	k2eAgFnSc1d1	geodetická
astronomie	astronomie	k1gFnSc1	astronomie
</sent>
<sent id="3406">
geodetický	geodetický	k2eAgInSc4d1	geodetický
azimut	azimut	k1gInSc4	azimut
</sent>
<sent id="3407">
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
rok	rok	k1gInSc4	rok
</sent>
<sent id="3408">
hranolový	hranolový	k2eAgInSc4d1	hranolový
astroláb	astroláb	k1gInSc4	astroláb
</sent>
<sent id="3409">
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
rok	rok	k1gInSc4	rok
</sent>
<sent id="3410">
inerciální	inerciální	k2eAgInSc4d1	inerciální
azimut	azimut	k1gInSc4	azimut
</sent>
<sent id="3411">
izostatická	izostatický	k2eAgFnSc1d1	izostatický
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3413">
koncový	koncový	k2eAgInSc4d1	koncový
bod	bod	k1gInSc4	bod
geodetické	geodetický	k2eAgFnSc2d1	geodetická
základny	základna	k1gFnSc2	základna
</sent>
<sent id="3414">
Laplaceova	Laplaceův	k2eAgFnSc1d1	Laplaceova
podmínka	podmínka	k1gFnSc1	podmínka
</sent>
<sent id="3415">
Laplaceův	Laplaceův	k2eAgInSc4d1	Laplaceův
azimut	azimut	k1gInSc4	azimut
</sent>
<sent id="3416">
Laplaceův	Laplaceův	k2eAgInSc4d1	Laplaceův
bod	bod	k1gInSc4	bod
</sent>
<sent id="3417">
magnetická	magnetický	k2eAgFnSc1d1	magnetická
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3418">
magnetický	magnetický	k2eAgInSc4d1	magnetický
azimut	azimut	k1gInSc4	azimut
</sent>
<sent id="3419">
opticky	opticky	k6eAd1	
efektivní	efektivní	k2eAgFnSc1d1	efektivní
atmosféra	atmosféra	k1gFnSc1	atmosféra
</sent>
<sent id="3420">
paralaktická	paralaktický	k2eAgFnSc1d1	paralaktická
diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
aberace	aberace	k1gFnSc1	aberace
</sent>
<sent id="3421">
planetární	planetární	k2eAgFnSc1d1	planetární
aberace	aberace	k1gFnSc1	aberace
</sent>
<sent id="3422">
poziční	poziční	k2eAgFnSc1d1	poziční
astronomie	astronomie	k1gFnSc1	astronomie
</sent>
<sent id="3423">
pravá	pravý	k2eAgFnSc1d1	pravá
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3424">
referenční	referenční	k2eAgInSc4d1	referenční
bod	bod	k1gInSc4	bod
tíhového	tíhový	k2eAgNnSc2d1	tíhové
zrychlení	zrychlení	k1gNnSc2	zrychlení
</sent>
<sent id="3425">
roční	roční	k2eAgFnSc1d1	roční
aberace	aberace	k1gFnSc1	aberace
</sent>
<sent id="3426">
rok	rok	k1gInSc1	rok
</sent>
<sent id="3427">
sekulární	sekulární	k2eAgFnSc1d1	sekulární
aberace	aberace	k1gFnSc1	aberace
</sent>
<sent id="3428">
setrvačníkový	setrvačníkový	k2eAgInSc4d1	setrvačníkový
azimut	azimut	k1gInSc4	azimut
</sent>
<sent id="3429">
sférická	sférický	k2eAgFnSc1d1	sférická
astronomie	astronomie	k1gFnSc1	astronomie
</sent>
<sent id="3430">
smíšená	smíšený	k2eAgFnSc1d1	smíšená
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3431">
stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
bod	bod	k1gInSc1	bod
technické	technický	k2eAgFnSc2d1	technická
nivelace	nivelace	k1gFnSc2	nivelace
</sent>
<sent id="3432">
střední	střední	k2eAgFnSc1d1	střední
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3433">
tíhová	tíhový	k2eAgFnSc1d1	tíhová
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3434">
úplná	úplný	k2eAgFnSc1d1	úplná
Bouguerova	Bouguerův	k2eAgFnSc1d1	Bouguerův
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="3435">
úsekový	úsekový	k2eAgInSc4d1	úsekový
bod	bod	k1gInSc4	bod
geodetické	geodetický	k2eAgFnSc2d1	geodetická
základny	základna	k1gFnSc2	základna
</sent>
<sent id="3436">
základní	základní	k2eAgInSc4d1	základní
bod	bod	k1gInSc4	bod
geodetického	geodetický	k2eAgInSc2d1	geodetický
systému	systém	k1gInSc2	systém
</sent>
<sent id="3437">
základní	základní	k2eAgFnSc4d1	základní
polohové	polohový	k2eAgNnSc4d1	polohové
bodové	bodový	k2eAgNnSc4d1	bodové
pole	pole	k1gNnSc4	pole
(	(	kIx(	
<g/>
ZPBP	ZPBP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="3438">
základní	základní	k2eAgFnSc4d1	základní
tíhové	tíhový	k2eAgNnSc4d1	tíhové
bodové	bodový	k2eAgNnSc4d1	bodové
pole	pole	k1gNnSc4	pole
(	(	kIx(	
<g/>
ZTBP	ZTBP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="3439">
základní	základní	k2eAgFnSc4d1	základní
výškové	výškový	k2eAgNnSc4d1	výškové
bodové	bodový	k2eAgNnSc4d1	bodové
pole	pole	k1gNnSc4	pole
(	(	kIx(	
<g/>
ZVBP	ZVBP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="3440">
zpětný	zpětný	k2eAgInSc4d1	zpětný
azimut	azimut	k1gInSc4	azimut
</sent>
<sent id="3441">
zrcadlový	zrcadlový	k2eAgInSc4d1	zrcadlový
astroláb	astroláb	k1gInSc4	astroláb
</sent>
<sent id="3442">
náhradní	náhradní	k2eAgInSc4d1	náhradní
užívání	užívání	k1gNnSc4	užívání
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="3443">
anomální	anomální	k2eAgFnSc1d1	anomální
magnetická	magnetický	k2eAgFnSc1d1	magnetická
deklinace	deklinace	k1gFnSc1	deklinace
</sent>
<sent id="3444">
astronomická	astronomický	k2eAgFnSc1d1	astronomická
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
</sent>
<sent id="3446">
Besselova	Besselův	k2eAgFnSc1d1	Besselova
denní	denní	k2eAgFnSc1d1	denní
čísla	číslo	k1gNnSc2	číslo
</sent>
<sent id="3447">
centrace	centrace	k1gFnSc1	centrace
měřického	měřický	k2eAgInSc2d1	měřický
stolu	stol	k1gInSc2	stol
</sent>
<sent id="3448">
centrace	centrace	k1gFnSc1	centrace
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="3449">
čára	čára	k1gFnSc1	čára
apsid	apsida	k1gFnPc2	apsida
</sent>
<sent id="3450">
Collinsův	Collinsův	k2eAgInSc4d1	Collinsův
bod	bod	k1gInSc4	bod
</sent>
<sent id="3451">
čára	čára	k1gFnSc1	čára
normálového	normálový	k2eAgInSc2d1	normálový
řezu	řez	k1gInSc2	řez
</sent>
<sent id="3452">
čas	čas	k1gInSc1	čas
průchodu	průchod	k1gInSc2	průchod
hvězdy	hvězda	k1gFnSc2	hvězda
</sent>
<sent id="3453">
datová	datový	k2eAgFnSc1d1	datová
čára	čára	k1gFnSc1	čára
</sent>
<sent id="3454">
čas	čas	k1gInSc1	čas
průchodu	průchod	k1gInSc2	průchod
planety	planeta	k1gFnSc2	planeta
periheliem	perihelium	k1gNnSc7	perihelium
</sent>
<sent id="3455">
deklinace	deklinace	k1gFnSc1	deklinace
</sent>
<sent id="3456">
deklinace	deklinace	k1gFnSc1	deklinace
vztažená	vztažený	k2eAgNnPc1d1	vztažené
ke	k	k7c3	
kilometrové	kilometrový	k2eAgFnSc3d1	kilometrová
síti	síť	k1gFnSc3	síť
</sent>
<sent id="3457">
délka	délka	k1gFnSc1	délka
nivelační	nivelační	k2eAgFnSc2d1	nivelační
sestavy	sestava	k1gFnSc2	sestava
</sent>
<sent id="3458">
délka	délka	k1gFnSc1	délka
perihelia	perihelium	k1gNnSc2	perihelium
</sent>
<sent id="3459">
délka	délka	k1gFnSc1	délka
silové	silový	k2eAgFnSc2d1	silová
křivky	křivka	k1gFnSc2	křivka
</sent>
<sent id="3460">
délka	délka	k1gFnSc1	délka
stupně	stupeň	k1gInSc2	stupeň
</sent>
<sent id="3461">
délka	délka	k1gFnSc1	délka
výstupního	výstupní	k2eAgInSc2d1	výstupní
uzlu	uzel	k1gInSc2	uzel
</sent>
<sent id="3463">
délka	délka	k1gFnSc1	délka
záměry	záměra	k1gFnSc2	záměra
</sent>
<sent id="3464">
dočasně	dočasně	k6eAd1	
signalizovaný	signalizovaný	k2eAgInSc4d1	signalizovaný
bod	bod	k1gInSc4	bod
</sent>
<sent id="3465">
dočasně	dočasně	k6eAd1	
stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
bod	bod	k1gInSc1	bod
</sent>
<sent id="3466">
družicový	družicový	k2eAgInSc4d1	družicový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3468">
excentrický	excentrický	k2eAgInSc4d1	excentrický
bod	bod	k1gInSc4	bod
</sent>
<sent id="3469">
ekliptická	ekliptický	k2eAgFnSc1d1	ekliptický
délka	délka	k1gFnSc1	délka
</sent>
<sent id="3470">
fundamentální	fundamentální	k2eAgInSc1d1	fundamentální
družicový	družicový	k2eAgInSc1d1	družicový
geodetický	geodetický	k2eAgInSc1d1	geodetický
bod	bod	k1gInSc1	bod
</sent>
<sent id="3471">
galaktická	galaktický	k2eAgFnSc1d1	Galaktická
délka	délka	k1gFnSc1	délka
</sent>
<sent id="3472">
geodetická	geodetický	k2eAgFnSc1d1	geodetická
čára	čára	k1gFnSc1	čára
</sent>
<sent id="3473">
graficky	graficky	k6eAd1	
určený	určený	k2eAgInSc4d1	určený
bod	bod	k1gInSc4	bod
</sent>
<sent id="3474">
grádové	grádová	k1gFnSc6	grádová
dělení	dělení	k1gNnSc1	dělení
</sent>
<sent id="3475">
gravimetrický	gravimetrický	k2eAgInSc4d1	gravimetrický
bod	bod	k1gInSc4	bod
</sent>
<sent id="3476">
greenwichský	greenwichský	k2eAgInSc4d1	greenwichský
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
čas	čas	k1gInSc4	čas
</sent>
<sent id="3477">
greenwichský	greenwichský	k2eAgInSc4d1	greenwichský
čas	čas	k1gInSc4	čas
</sent>
<sent id="3478">
greenwichský	greenwichský	k2eAgInSc4d1	greenwichský
střední	střední	k2eAgInSc4d1	střední
čas	čas	k1gInSc4	čas
</sent>
<sent id="3479">
hlavní	hlavní	k2eAgInSc1d1	hlavní
tíhový	tíhový	k2eAgInSc1d1	tíhový
bod	bod	k1gInSc1	bod
</sent>
<sent id="3480">
hlavní	hlavní	k2eAgInSc1d1	hlavní
výškový	výškový	k2eAgInSc1d1	výškový
bod	bod	k1gInSc1	bod
</sent>
<sent id="3481">
hlavní	hlavní	k2eAgInSc1d1	hlavní
výškový	výškový	k2eAgInSc1d1	výškový
indikační	indikační	k2eAgInSc1d1	indikační
bod	bod	k1gInSc1	bod
</sent>
<sent id="3482">
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
čísla	číslo	k1gNnSc2	číslo
</sent>
<sent id="3483">
hvězdné	hvězdný	k2eAgNnSc4d1	Hvězdné
greenwichské	greenwichský	k2eAgNnSc4d1	greenwichský
datum	datum	k1gNnSc4	datum
</sent>
<sent id="3484">
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
čas	čas	k1gInSc4	čas
</sent>
<sent id="3485">
indikační	indikační	k2eAgInSc4d1	indikační
tíhový	tíhový	k2eAgInSc4d1	tíhový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3486">
inflexní	inflexní	k2eAgInSc4d1	inflexní
bod	bod	k1gInSc4	bod
</sent>
<sent id="3487">
jarní	jarní	k2eAgInSc4d1	jarní
bod	bod	k1gInSc4	bod
</sent>
<sent id="3488">
juliánské	juliánský	k2eAgFnSc2d1	juliánská
datum	datum	k1gInSc4	datum
</sent>
<sent id="3489">
koordinovaný	koordinovaný	k2eAgInSc4d1	koordinovaný
univerzální	univerzální	k2eAgInSc4d1	univerzální
čas	čas	k1gInSc4	čas
</sent>
<sent id="3490">
kyvadlový	kyvadlový	k2eAgInSc4d1	kyvadlový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3491">
magnetická	magnetický	k2eAgFnSc1d1	magnetická
deklinace	deklinace	k1gFnSc1	deklinace
</sent>
<sent id="3492">
magnetometrický	magnetometrický	k2eAgInSc4d1	magnetometrický
bod	bod	k1gInSc4	bod
</sent>
<sent id="3493">
měřená	měřený	k2eAgFnSc1d1	měřená
délka	délka	k1gFnSc1	délka
</sent>
<sent id="3494">
Metonův	Metonův	k2eAgInSc1d1	Metonův
(	(	kIx(	
<g/>
měsíční	měsíční	k2eAgInSc1d1	měsíční
<g/>
)	)	kIx)	
cyklus	cyklus	k1gInSc1	cyklus
</sent>
<sent id="3495">
místní	místní	k2eAgInSc4d1	místní
čas	čas	k1gInSc4	čas
</sent>
<sent id="3496">
místní	místní	k2eAgInSc4d1	místní
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
čas	čas	k1gInSc4	čas
</sent>
<sent id="3497">
modifikované	modifikovaný	k2eAgNnSc4d1	modifikované
juliánské	juliánský	k2eAgNnSc4d1	juliánské
datum	datum	k1gNnSc4	datum
</sent>
<sent id="3498">
náhradní	náhradní	k2eAgInSc4d1	náhradní
trigonometrický	trigonometrický	k2eAgInSc4d1	trigonometrický
bod	bod	k1gInSc4	bod
</sent>
<sent id="3501">
nivelační	nivelační	k2eAgInSc1d1	nivelační
bod	bod	k1gInSc1	bod
</sent>
<sent id="3502">
normální	normální	k2eAgFnSc1d1	normální
magnetická	magnetický	k2eAgFnSc1d1	magnetická
deklinace	deklinace	k1gFnSc1	deklinace
</sent>
<sent id="3503">
nulový	nulový	k2eAgInSc4d1	nulový
výškový	výškový	k2eAgInSc4d1	výškový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3504">
ověřovací	ověřovací	k2eAgInSc4d1	ověřovací
bod	bod	k1gInSc4	bod
</sent>
<sent id="3505">
paralaktický	paralaktický	k2eAgInSc4d1	paralaktický
článek	článek	k1gInSc4	článek
</sent>
<sent id="3506">
paralaktický	paralaktický	k2eAgInSc4d1	paralaktický
článek	článek	k1gInSc4	článek
s	s	k7c7	
pomocnou	pomocný	k2eAgFnSc7d1	pomocná
základnou	základna	k1gFnSc7	základna
</sent>
<sent id="3508">
pásmový	pásmový	k2eAgInSc1d1	pásmový
čas	čas	k1gInSc1	čas
</sent>
<sent id="3509">
podrobný	podrobný	k2eAgInSc4d1	podrobný
polohový	polohový	k2eAgInSc4d1	polohový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3510">
podrobný	podrobný	k2eAgInSc4d1	podrobný
výškový	výškový	k2eAgInSc4d1	výškový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3511">
podzemní	podzemní	k2eAgInSc4d1	podzemní
bod	bod	k1gInSc4	bod
</sent>
<sent id="3512">
podzimní	podzimní	k2eAgInSc4d1	podzimní
bod	bod	k1gInSc4	bod
</sent>
<sent id="3513">
polohový	polohový	k2eAgInSc4d1	polohový
geodetický	geodetický	k2eAgInSc4d1	geodetický
bod	bod	k1gInSc4	bod
</sent>
<sent id="3514">
polygonový	polygonový	k2eAgInSc4d1	polygonový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3515">
porovnávací	porovnávací	k2eAgInSc4d1	porovnávací
bod	bod	k1gInSc4	bod
</sent>
<sent id="3516">
pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
bod	bod	k1gInSc1	bod
</sent>
<sent id="3517">
pravá	pravý	k2eAgFnSc1d1	pravá
deklinace	deklinace	k1gFnSc1	deklinace
</sent>
<sent id="3518">
pravý	pravý	k2eAgInSc4d1	pravý
greenwichský	greenwichský	k2eAgInSc4d1	greenwichský
čas	čas	k1gInSc4	čas
</sent>
<sent id="3519">
pravý	pravý	k2eAgInSc4d1	pravý
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
čas	čas	k1gInSc4	čas
</sent>
<sent id="3520">
pravý	pravý	k2eAgInSc4d1	pravý
jarní	jarní	k2eAgInSc4d1	jarní
bod	bod	k1gInSc4	bod
</sent>
<sent id="3521">
pravý	pravý	k2eAgInSc4d1	pravý
místní	místní	k2eAgInSc4d1	místní
čas	čas	k1gInSc4	čas
</sent>
<sent id="3522">
připojovací	připojovací	k2eAgInSc4d1	připojovací
bod	bod	k1gInSc4	bod
</sent>
<sent id="3523">
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
délka	délka	k1gFnSc1	délka
</sent>
<sent id="3524">
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
délka	délka	k1gFnSc1	délka
geodetické	geodetický	k2eAgFnSc2d1	geodetická
čáry	čára	k1gFnSc2	čára
</sent>
<sent id="3525">
referenční	referenční	k2eAgInSc4d1	referenční
bod	bod	k1gInSc4	bod
</sent>
<sent id="3526">
referenční	referenční	k2eAgInSc4d1	referenční
výškový	výškový	k2eAgInSc4d1	výškový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3527">
relativní	relativní	k2eAgInSc4d1	relativní
tíhový	tíhový	k2eAgInSc4d1	tíhový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3528">
reverzní	reverzní	k2eAgInSc4d1	reverzní
bod	bod	k1gInSc4	bod
</sent>
<sent id="3529">
rotační	rotační	k2eAgInSc4d1	rotační
čas	čas	k1gInSc4	čas
</sent>
<sent id="3530">
severní	severní	k2eAgInSc4d1	severní
bod	bod	k1gInSc4	bod
</sent>
<sent id="3531">
signalizovaný	signalizovaný	k2eAgInSc4d1	signalizovaný
bod	bod	k1gInSc4	bod
</sent>
<sent id="3532">
směrový	směrový	k2eAgInSc4d1	směrový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3533">
stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
bod	bod	k1gInSc1	bod
</sent>
<sent id="3534">
střední	střední	k2eAgFnSc1d1	střední
deklinace	deklinace	k1gFnSc1	deklinace
</sent>
<sent id="3535">
střední	střední	k2eAgInSc4d1	střední
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
čas	čas	k1gInSc4	čas
</sent>
<sent id="3536">
střední	střední	k2eAgInSc4d1	střední
jarní	jarní	k2eAgInSc4d1	jarní
bod	bod	k1gInSc4	bod
</sent>
<sent id="3538">
subdružicový	subdružicový	k2eAgInSc4d1	subdružicový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3539">
světový	světový	k2eAgInSc4d1	světový
čas	čas	k1gInSc4	čas
</sent>
<sent id="3540">
tíhový	tíhový	k2eAgInSc4d1	tíhový
geodetický	geodetický	k2eAgInSc4d1	geodetický
bod	bod	k1gInSc4	bod
</sent>
<sent id="3541">
topocentrická	topocentrický	k2eAgFnSc1d1	topocentrický
deklinace	deklinace	k1gFnSc1	deklinace
</sent>
<sent id="3542">
triangulační	triangulační	k2eAgInSc1d1	triangulační
bod	bod	k1gInSc1	bod
</sent>
<sent id="3543">
trigonometrický	trigonometrický	k2eAgInSc4d1	trigonometrický
bod	bod	k1gInSc4	bod
</sent>
<sent id="3544">
uzlová	uzlový	k2eAgFnSc1d1	uzlová
čára	čára	k1gFnSc1	čára
</sent>
<sent id="3545">
uzlový	uzlový	k2eAgInSc4d1	uzlový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3546">
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
</sent>
<sent id="3547">
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
</sent>
<sent id="3548">
vrcholový	vrcholový	k2eAgInSc4d1	vrcholový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3549">
východní	východní	k2eAgFnSc1d1	východní
(	(	kIx(	
<g/>
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
<g/>
)	)	kIx)	
délka	délka	k1gFnSc1	délka
</sent>
<sent id="3550">
výchozí	výchozí	k2eAgInSc4d1	výchozí
bod	bod	k1gInSc4	bod
</sent>
<sent id="3551">
vyrovnaná	vyrovnaný	k2eAgFnSc1d1	vyrovnaná
délka	délka	k1gFnSc1	délka
</sent>
<sent id="3552">
výškový	výškový	k2eAgInSc4d1	výškový
geodetický	geodetický	k2eAgInSc4d1	geodetický
bod	bod	k1gInSc4	bod
</sent>
<sent id="7300">
geoportál	geoportál	k1gInSc1	geoportál
</sent>
<sent id="3554">
zajišťovací	zajišťovací	k2eAgInSc4d1	zajišťovací
bod	bod	k1gInSc4	bod
</sent>
<sent id="3555">
základní	základní	k2eAgInSc4d1	základní
bod	bod	k1gInSc4	bod
</sent>
<sent id="3556">
základní	základní	k2eAgInSc1d1	základní
nivelační	nivelační	k2eAgInSc1d1	nivelační
bod	bod	k1gInSc1	bod
</sent>
<sent id="3557">
základní	základní	k2eAgInSc4d1	základní
trigonometrický	trigonometrický	k2eAgInSc4d1	trigonometrický
bod	bod	k1gInSc4	bod
</sent>
<sent id="3558">
základnový	základnový	k2eAgInSc4d1	základnový
bod	bod	k1gInSc4	bod
</sent>
<sent id="3559">
západní	západní	k2eAgFnSc1d1	západní
(	(	kIx(	
<g/>
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
<g/>
)	)	kIx)	
délka	délka	k1gFnSc1	délka
</sent>
<sent id="3560">
západní	západní	k2eAgInSc4d1	západní
bod	bod	k1gInSc4	bod
</sent>
<sent id="3561">
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
deklinace	deklinace	k1gFnSc1	deklinace
</sent>
<sent id="3563">
zničený	zničený	k2eAgInSc4d1	zničený
bod	bod	k1gInSc4	bod
</sent>
<sent id="3566">
Besselův	Besselův	k2eAgInSc4d1	Besselův
elipsoid	elipsoid	k1gInSc4	elipsoid
</sent>
<sent id="3567">
deprese	deprese	k1gFnSc1	deprese
horizontu	horizont	k1gInSc2	horizont
</sent>
<sent id="3568">
Bouguerova	Bouguerův	k2eAgFnSc1d1	Bouguerův
deska	deska	k1gFnSc1	deska
</sent>
<sent id="3569">
digrese	digrese	k1gFnSc1	digrese
</sent>
<sent id="3573">
dráha	dráha	k1gFnSc1	dráha
družice	družice	k1gFnSc1	družice
</sent>
<sent id="3574">
družice	družice	k1gFnSc1	družice
</sent>
<sent id="3575">
družice	družice	k1gFnSc1	družice
pro	pro	k7c4	
výzkum	výzkum	k1gInSc4	výzkum
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
</sent>
<sent id="3576">
družice	družice	k1gFnSc1	družice
pro	pro	k7c4	
výzkum	výzkum	k1gInSc4	výzkum
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
</sent>
<sent id="3577">
dublinské	dublinský	k2eAgNnSc4d1	Dublinské
modifikované	modifikovaný	k2eAgNnSc4d1	modifikované
datum	datum	k1gNnSc4	datum
</sent>
<sent id="3578">
dvojřada	dvojřada	k1gFnSc1	dvojřada
</sent>
<sent id="3579">
dynamika	dynamika	k1gFnSc1	dynamika
dráhy	dráha	k1gFnSc2	dráha
družice	družice	k1gFnSc1	družice
</sent>
<sent id="3580">
efemerida	efemerida	k1gFnSc1	efemerida
</sent>
<sent id="3581">
ekliptika	ekliptika	k1gFnSc1	ekliptika
</sent>
<sent id="3582">
ekvinokcium	ekvinokcium	k1gNnSc4	ekvinokcium
</sent>
<sent id="3583">
elipsoid	elipsoid	k1gInSc1	elipsoid
1967	#num#	k4	
</sent>
<sent id="3584">
elipsoid	elipsoid	k1gInSc1	elipsoid
GRS	GRS	kA	
1980	#num#	k4	
(	(	kIx(	
<g/>
GRS	GRS	kA	
80	#num#	k4	
<g/>
)	)	kIx)	
</sent>
<sent id="3585">
geodetická	geodetický	k2eAgFnSc1d1	geodetická
dokumentace	dokumentace	k1gFnSc1	dokumentace
</sent>
<sent id="3586">
geodetická	geodetický	k2eAgFnSc1d1	geodetická
družice	družice	k1gFnSc1	družice
</sent>
<sent id="3587">
geodynamická	geodynamický	k2eAgFnSc1d1	geodynamická
družice	družice	k1gFnSc1	družice
</sent>
<sent id="3588">
geostacionární	geostacionární	k2eAgFnSc1d1	geostacionární
družice	družice	k1gFnSc1	družice
</sent>
<sent id="3589">
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
den	den	k1gInSc4	den
</sent>
<sent id="3590">
juliánský	juliánský	k2eAgInSc4d1	juliánský
den	den	k1gInSc4	den
</sent>
<sent id="3591">
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
den	den	k1gInSc4	den
</sent>
<sent id="3592">
komunikační	komunikační	k2eAgFnSc1d1	komunikační
družice	družice	k1gFnSc1	družice
</sent>
<sent id="3593">
maximální	maximální	k2eAgFnSc1d1	maximální
digrese	digrese	k1gFnSc1	digrese
</sent>
<sent id="3594">
meridiánová	meridiánový	k2eAgFnSc1d1	Meridiánová
elipsa	elipsa	k1gFnSc1	elipsa
</sent>
<sent id="3595">
modifikovaný	modifikovaný	k2eAgInSc4d1	modifikovaný
juliánský	juliánský	k2eAgInSc4d1	juliánský
den	den	k1gInSc4	den
</sent>
<sent id="3596">
navigační	navigační	k2eAgFnSc1d1	navigační
družice	družice	k1gFnSc1	družice
</sent>
<sent id="3597">
nepřímý	přímý	k2eNgInSc4d1	nepřímý
efekt	efekt	k1gInSc4	efekt
</sent>
<sent id="3598">
normální	normální	k2eAgFnSc1d1	normální
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
</sent>
<sent id="3599">
nutační	nutační	k2eAgFnSc1d1	nutační
elipsa	elipsa	k1gFnSc1	elipsa
</sent>
<sent id="3600">
oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
</sent>
<sent id="3601">
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
</sent>
<sent id="3602">
oskulační	oskulační	k2eAgInPc4d1	oskulační
elementy	element	k1gInPc4	element
</sent>
<sent id="3603">
pasivní	pasivní	k2eAgFnSc1d1	pasivní
družice	družice	k1gFnSc1	družice
</sent>
<sent id="3604">
polární	polární	k2eAgFnSc1d1	polární
družice	družice	k1gFnSc1	družice
</sent>
<sent id="3605">
pravý	pravý	k2eAgInSc1d1	pravý
sluneční	sluneční	k2eAgInSc1d1	sluneční
den	den	k1gInSc1	den
</sent>
<sent id="3606">
prvky	prvek	k1gInPc7	prvek
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
</sent>
<sent id="3607">
přerušovaná	přerušovaný	k2eAgFnSc1d1	přerušovaná
dráha	dráha	k1gFnSc1	dráha
tělesa	těleso	k1gNnSc2	těleso
na	na	k7c6	
kosmickém	kosmický	k2eAgInSc6d1	kosmický
snímku	snímek	k1gInSc6	snímek
</sent>
<sent id="3608">
rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
družice	družice	k1gFnSc1	družice
</sent>
<sent id="3609">
střední	střední	k2eAgInSc4d1	střední
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
den	den	k1gInSc4	den
</sent>
<sent id="3613">
číselný	číselný	k2eAgInSc4d1	číselný
výpočet	výpočet	k1gInSc4	výpočet
výměr	výměr	k1gInSc1	výměr
</sent>
<sent id="3614">
dělení	dělení	k1gNnSc6	dělení
parcely	parcela	k1gFnSc2	parcela
</sent>
<sent id="3615">
dělení	dělení	k1gNnSc6	dělení
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="3616">
delimitace	delimitace	k1gFnSc1	delimitace
půdního	půdní	k2eAgInSc2d1	půdní
fondu	fond	k1gInSc2	fond
</sent>
<sent id="3617">
digitalizovaná	digitalizovaný	k2eAgFnSc1d1	digitalizovaná
katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="3618">
digitální	digitální	k2eAgFnSc1d1	digitální
katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="3619">
dokumentace	dokumentace	k1gFnSc2	dokumentace
výsledků	výsledek	k1gInPc2	výsledek
šetření	šetření	k1gNnSc1	šetření
a	a	k8xC	
měření	měření	k1gNnSc1	měření
</sent>
<sent id="3620">
dokumentace	dokumentace	k1gFnSc2	dokumentace
vytyčené	vytyčený	k2eAgFnSc2d1	vytyčená
hranice	hranice	k1gFnSc2	hranice
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="3621">
druh	druh	k1gInSc1	druh
číslování	číslování	k1gNnSc2	číslování
parcel	parcela	k1gFnPc2	parcela
</sent>
<sent id="3623">
družstevní	družstevní	k2eAgFnSc4d1	družstevní
socialistické	socialistický	k2eAgNnSc1d1	socialistické
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
</sent>
<sent id="3624">
evidenční	evidenční	k2eAgFnSc1d1	evidenční
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="3626">
evidenční	evidenční	k2eAgFnSc1d1	evidenční
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="3627">
grafický	grafický	k2eAgInSc4d1	grafický
výpočet	výpočet	k1gInSc4	výpočet
výměr	výměr	k1gInSc1	výměr
</sent>
<sent id="3629">
charakteristika	charakteristika	k1gFnSc1	charakteristika
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="3630">
identifikační	identifikační	k2eAgInSc4d1	identifikační
číslo	číslo	k1gNnSc1	číslo
</sent>
<sent id="3632">
jméno	jméno	k1gNnSc1	jméno
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
</sent>
<sent id="3633">
jméno	jméno	k1gNnSc4	jméno
obce	obec	k1gFnSc2	obec
</sent>
<sent id="3634">
katastrální	katastrální	k2eAgInSc4d1	katastrální
mapování	mapování	k1gNnSc1	mapování
</sent>
<sent id="3635">
katastrální	katastrální	k2eAgInSc4d1	katastrální
měření	měření	k1gNnSc1	měření
</sent>
<sent id="3636">
katastrální	katastrální	k2eAgInSc4d1	katastrální
operát	operát	k1gInSc4	operát
</sent>
<sent id="7591">
nivelační	nivelační	k2eAgInSc1d1	nivelační
oddíl	oddíl	k1gInSc1	oddíl
</sent>
<sent id="7592">
nivelační	nivelační	k2eAgFnSc1d1	nivelační
sestava	sestava	k1gFnSc1	sestava
</sent>
<sent id="3638">
katastrální	katastrální	k2eAgInSc4d1	katastrální
řízení	řízení	k1gNnSc1	řízení
</sent>
<sent id="3639">
katastrální	katastrální	k2eAgInSc4d1	katastrální
území	území	k1gNnSc1	území
</sent>
<sent id="3640">
komplexní	komplexní	k2eAgFnSc2d1	komplexní
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
úpravy	úprava	k1gFnSc2	úprava
</sent>
<sent id="3641">
kopie	kopie	k1gFnSc1	kopie
katastrální	katastrální	k2eAgFnSc2d1	katastrální
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3642">
kopie	kopie	k1gFnSc1	kopie
z	z	k7c2	
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3643">
kvalita	kvalita	k1gFnSc1	kvalita
výměry	výměra	k1gFnSc2	výměra
</sent>
<sent id="3644">
lesní	lesní	k2eAgInSc4d1	lesní
pozemek	pozemek	k1gInSc4	pozemek
</sent>
<sent id="3645">
lesní	lesní	k2eAgInSc4d1	lesní
půdní	půdní	k2eAgInSc4d1	půdní
fond	fond	k1gInSc4	fond
</sent>
<sent id="3647">
mapa	mapa	k1gFnSc1	mapa
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
knihy	kniha	k1gFnSc2	kniha
</sent>
<sent id="3648">
mapa	mapa	k1gFnSc1	mapa
pozemkového	pozemkový	k2eAgInSc2d1	pozemkový
katastru	katastr	k1gInSc2	katastr
</sent>
<sent id="3649">
měřický	měřický	k2eAgInSc4d1	měřický
operát	operát	k1gInSc4	operát
pozemkového	pozemkový	k2eAgInSc2d1	pozemkový
katastru	katastr	k1gInSc2	katastr
</sent>
<sent id="3650">
místní	místní	k2eAgInSc4d1	místní
jméno	jméno	k1gNnSc1	jméno
</sent>
<sent id="3651">
místní	místní	k2eAgInSc4d1	místní
názvosloví	názvosloví	k1gNnSc1	názvosloví
</sent>
<sent id="3652">
místní	místní	k2eAgInSc4d1	místní
šetření	šetření	k1gNnSc1	šetření
</sent>
<sent id="3653">
místní	místní	k2eAgInSc4d1	místní
vztah	vztah	k1gInSc4	vztah
</sent>
<sent id="3655">
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
stavba	stavba	k1gFnSc1	stavba
</sent>
<sent id="3656">
náhradní	náhradní	k2eAgInSc4d1	náhradní
pozemek	pozemek	k1gInSc4	pozemek
</sent>
<sent id="3658">
nájemní	nájemní	k2eAgInSc4d1	nájemní
právo	právo	k1gNnSc1	právo
</sent>
<sent id="3659">
nesjednocený	sjednocený	k2eNgInSc4d1	nesjednocený
operát	operát	k1gInSc4	operát
</sent>
<sent id="3661">
nezemědělský	zemědělský	k2eNgInSc4d1	nezemědělský
pozemek	pozemek	k1gInSc4	pozemek
</sent>
<sent id="3662">
nové	nový	k2eAgNnSc4d1	nové
katastrální	katastrální	k2eAgNnSc4d1	katastrální
měření	měření	k1gNnSc4	měření
</sent>
<sent id="3663">
obec	obec	k1gFnSc1	obec
</sent>
<sent id="3664">
obecní	obecní	k2eAgInSc4d1	obecní
sektorový	sektorový	k2eAgInSc4d1	sektorový
přehled	přehled	k1gInSc4	přehled
o	o	k7c6	
plochách	plocha	k1gFnPc6	plocha
druhů	druh	k1gMnPc2	druh
pozemků	pozemek	k1gInPc2	pozemek
</sent>
<sent id="3666">
obnova	obnova	k1gFnSc1	obnova
měřického	měřický	k2eAgInSc2d1	měřický
operátu	operát	k1gInSc2	operát
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="3667">
obnova	obnova	k1gFnSc1	obnova
písemného	písemný	k2eAgInSc2d1	písemný
operátu	operát	k1gInSc2	operát
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="3669">
oceňování	oceňování	k1gNnSc4	oceňování
pozemků	pozemek	k1gInPc2	pozemek
</sent>
<sent id="3670">
ohlašovací	ohlašovací	k2eAgInSc4d1	ohlašovací
list	list	k1gInSc4	list
</sent>
<sent id="3671">
ohlašování	ohlašování	k1gNnPc2	ohlašování
změn	změna	k1gFnPc2	změna
(	(	kIx(	
<g/>
v	v	k7c6	
katastru	katastr	k1gInSc6	katastr
<g/>
)	)	kIx)	
</sent>
<sent id="3673">
omezení	omezení	k1gNnSc2	omezení
dispozičních	dispoziční	k2eAgNnPc2d1	dispoziční
práv	právo	k1gNnPc2	právo
</sent>
<sent id="3674">
omezení	omezení	k1gNnSc6	omezení
převodu	převod	k1gInSc2	převod
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="3675">
oprávněný	oprávněný	k2eAgMnSc1d1	oprávněný
uživatel	uživatel	k1gMnSc1	uživatel
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="3676">
oprávněný	oprávněný	k2eAgInSc4d1	oprávněný
zájem	zájem	k1gInSc4	zájem
</sent>
<sent id="3677">
osobní	osobní	k2eAgInSc4d1	osobní
užívání	užívání	k1gNnSc4	užívání
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="3678">
parcelace	parcelace	k1gFnSc1	parcelace
</sent>
<sent id="3679">
písemný	písemný	k2eAgInSc4d1	písemný
operát	operát	k1gInSc4	operát
</sent>
<sent id="3680">
plomba	plomba	k1gFnSc1	plomba
(	(	kIx(	
<g/>
k	k	k7c3	
nemovitostem	nemovitost	k1gFnPc3	nemovitost
<g/>
)	)	kIx)	
</sent>
<sent id="3681">
plombování	plombování	k1gNnSc4	plombování
</sent>
<sent id="3682">
plošná	plošný	k2eAgFnSc1d1	plošná
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="3683">
podniková	podnikový	k2eAgFnSc1d1	podniková
evidence	evidence	k1gFnSc1	evidence
pozemků	pozemek	k1gInPc2	pozemek
</sent>
<sent id="3684">
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
bytu	byt	k1gInSc2	byt
</sent>
<sent id="3685">
podzemní	podzemní	k2eAgFnSc1d1	podzemní
stavba	stavba	k1gFnSc1	stavba
</sent>
<sent id="3686">
politická	politický	k2eAgFnSc1d1	politická
obec	obec	k1gFnSc1	obec
</sent>
<sent id="3687">
pomístní	pomístní	k2eAgInSc4d1	pomístní
jméno	jméno	k1gNnSc1	jméno
</sent>
<sent id="3688">
pomístní	pomístní	k2eAgInSc4d1	pomístní
názvosloví	názvosloví	k1gNnSc1	názvosloví
</sent>
<sent id="3689">
popisové	popisový	k2eAgFnSc2d1	popisová
pole	pole	k1gFnSc2	pole
</sent>
<sent id="3691">
pozemková	pozemkový	k2eAgFnSc1d1	pozemková
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="3692">
pozemková	pozemkový	k2eAgFnSc1d1	pozemková
reforma	reforma	k1gFnSc1	reforma
</sent>
<sent id="3693">
pozemkové	pozemkový	k2eAgFnPc4d1	pozemková
úpravy	úprava	k1gFnPc4	úprava
</sent>
<sent id="3694">
poznámka	poznámka	k1gFnSc1	poznámka
k	k	k7c3	
druhu	druh	k1gInSc3	druh
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="3695">
poznámka	poznámka	k1gFnSc1	poznámka
o	o	k7c6	
právech	právo	k1gNnPc6	právo
k	k	k7c3	
nemovitostem	nemovitost	k1gFnPc3	nemovitost
</sent>
<sent id="3696">
pracovní	pracovní	k2eAgFnSc1d1	pracovní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="3697">
protokol	protokol	k1gInSc1	protokol
o	o	k7c4	
vytyčení	vytyčení	k1gNnSc4	vytyčení
hranice	hranice	k1gFnSc2	hranice
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="3698">
prvotní	prvotní	k2eAgInSc4d1	prvotní
doklad	doklad	k1gInSc4	doklad
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="3699">
předkupní	předkupní	k2eAgNnSc4d1	předkupní
právo	právo	k1gNnSc4	právo
</sent>
<sent id="3700">
přehled	přehled	k1gInSc4	přehled
úhrnných	úhrnný	k2eAgInPc2d1	úhrnný
údajů	údaj	k1gInPc2	údaj
evidenčních	evidenční	k2eAgInPc2d1	evidenční
listů	list	k1gInPc2	list
</sent>
<sent id="3701">
přehled	přehled	k1gInSc1	přehled
záznamů	záznam	k1gInPc2	záznam
podrobného	podrobný	k2eAgNnSc2d1	podrobné
měření	měření	k1gNnSc2	měření
změn	změna	k1gFnPc2	změna
</sent>
<sent id="3702">
příložná	příložný	k2eAgFnSc1d1	příložná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="3704">
půdní	půdní	k2eAgInSc1d1	půdní
celek	celek	k1gInSc1	celek
</sent>
<sent id="3706">
půdní	půdní	k2eAgInSc1d1	půdní
fond	fond	k1gInSc1	fond
</sent>
<sent id="3708">
původní	původní	k2eAgInSc4d1	původní
pozemek	pozemek	k1gInSc4	pozemek
</sent>
<sent id="3709">
registr	registr	k1gInSc1	registr
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
(	(	kIx(	
<g/>
REN	REN	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="3710">
rejstřík	rejstřík	k1gInSc1	rejstřík
parcel	parcela	k1gFnPc2	parcela
</sent>
<sent id="3711">
rejstřík	rejstřík	k1gInSc4	rejstřík
uživatelů	uživatel	k1gMnPc2	uživatel
a	a	k8xC	
vlastníků	vlastník	k1gMnPc2	vlastník
</sent>
<sent id="3712">
rejstřík	rejstřík	k1gInSc1	rejstřík
vlastníků	vlastník	k1gMnPc2	vlastník
(	(	kIx(	
<g/>
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
)	)	kIx)	
a	a	k8xC	
jiných	jiný	k2eAgFnPc2d1	jiná
oprávněných	oprávněný	k2eAgFnPc2d1	oprávněná
osob	osoba	k1gFnPc2	osoba
</sent>
<sent id="3713">
revize	revize	k1gFnSc1	revize
údajů	údaj	k1gInPc2	údaj
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="3714">
rodné	rodný	k2eAgNnSc4d1	rodné
číslo	číslo	k1gNnSc4	číslo
</sent>
<sent id="3715">
řemenová	řemenový	k2eAgFnSc1d1	řemenová
parcela	parcela	k1gFnSc1	parcela
</sent>
<sent id="3716">
vyznačení	vyznačení	k1gNnSc6	vyznačení
změny	změna	k1gFnSc2	změna
v	v	k7c6	
katastru	katastr	k1gInSc6	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="3717">
drobná	drobný	k2eAgFnSc1d1	drobná
stavba	stavba	k1gFnSc1	stavba
</sent>
<sent id="3718">
jméno	jméno	k1gNnSc4	jméno
části	část	k1gFnSc2	část
obce	obec	k1gFnSc2	obec
</sent>
<sent id="3719">
jméno	jméno	k1gNnSc4	jméno
katastrální	katastrální	k2eAgFnSc2d1	katastrální
trati	trať	k1gFnSc2	trať
</sent>
<sent id="3720">
kód	kód	k1gInSc1	kód
bonitované	bonitovaný	k2eAgFnSc2d1	bonitovaná
půdně	půdně	k6eAd1	
ekologické	ekologický	k2eAgFnSc2d1	ekologická
jednotky	jednotka	k1gFnSc2	jednotka
</sent>
<sent id="3721">
nájemní	nájemní	k2eAgInSc4d1	nájemní
právo	právo	k1gNnSc1	právo
k	k	k7c3	
pozemkům	pozemek	k1gInPc3	pozemek
</sent>
<sent id="3722">
obnova	obnova	k1gFnSc1	obnova
katastrálního	katastrální	k2eAgInSc2d1	katastrální
operátu	operát	k1gInSc2	operát
přepracováním	přepracování	k1gNnSc7	přepracování
</sent>
<sent id="3723">
obvod	obvod	k1gInSc1	obvod
budovy	budova	k1gFnSc2	budova
</sent>
<sent id="3724">
pojmenování	pojmenování	k1gNnSc4	pojmenování
nebytového	bytový	k2eNgInSc2d1	nebytový
prostoru	prostor	k1gInSc2	prostor
</sent>
<sent id="3726">
pozemková	pozemkový	k2eAgFnSc1d1	pozemková
trať	trať	k1gFnSc1	trať
</sent>
<sent id="3727">
pozemky	pozemek	k1gInPc1	pozemek
sloučené	sloučený	k2eAgInPc1d1	sloučený
do	do	k7c2	
půdního	půdní	k2eAgInSc2d1	půdní
celku	celek	k1gInSc2	celek
</sent>
<sent id="3728">
poznámka	poznámka	k1gFnSc1	poznámka
o	o	k7c6	
sporné	sporný	k2eAgFnSc6d1	sporná
hranici	hranice	k1gFnSc6	hranice
</sent>
<sent id="3729">
průmět	průmět	k1gInSc1	průmět
vnějšího	vnější	k2eAgInSc2d1	vnější
obvodu	obvod	k1gInSc2	obvod
budovy	budova	k1gFnSc2	budova
na	na	k7c4	
terén	terén	k1gInSc4	terén
</sent>
<sent id="3730">
průnik	průnik	k1gInSc1	průnik
vnějšího	vnější	k2eAgInSc2d1	vnější
obvodu	obvod	k1gInSc2	obvod
budovy	budova	k1gFnSc2	budova
s	s	k7c7	
terénem	terén	k1gInSc7	terén
</sent>
<sent id="3731">
přídělový	přídělový	k2eAgInSc4d1	přídělový
plán	plán	k1gInSc4	plán
</sent>
<sent id="3732">
příslušnost	příslušnost	k1gFnSc4	příslušnost
hospodařit	hospodařit	k5eAaImF	
</sent>
<sent id="3734">
příslušnost	příslušnost	k1gFnSc1	příslušnost
parcely	parcela	k1gFnSc2	parcela
ke	k	k7c3	
katastrálnímu	katastrální	k2eAgNnSc3d1	katastrální
území	území	k1gNnSc3	území
</sent>
<sent id="3735">
rozestavěná	rozestavěný	k2eAgFnSc1d1	rozestavěná
budova	budova	k1gFnSc1	budova
</sent>
<sent id="3736">
rozestavěná	rozestavěný	k2eAgFnSc1d1	rozestavěná
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="3880">
kartografický	kartografický	k2eAgInSc4d1	kartografický
originál	originál	k1gInSc4	originál
popisu	popis	k1gInSc2	popis
</sent>
<sent id="3738">
geodézie	geodézie	k1gFnSc1	geodézie
</sent>
<sent id="7227">
automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
modelová	modelový	k2eAgFnSc1d1	modelová
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="3746">
osobní	osobní	k2eAgInSc4d1	osobní
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
</sent>
<sent id="3749">
příslušnost	příslušnost	k1gFnSc4	příslušnost
hospodařit	hospodařit	k5eAaImF	
s	s	k7c7	
majetkem	majetek	k1gInSc7	majetek
státu	stát	k1gInSc2	stát
</sent>
<sent id="3750">
rozbor	rozbor	k1gInSc1	rozbor
změn	změna	k1gFnPc2	změna
v	v	k7c6	
půdním	půdní	k2eAgInSc6d1	půdní
fondu	fond	k1gInSc6	fond
</sent>
<sent id="3751">
řízení	řízení	k1gNnSc2	řízení
o	o	k7c6	
námitkách	námitka	k1gFnPc6	námitka
</sent>
<sent id="3752">
scelování	scelování	k1gNnSc4	scelování
</sent>
<sent id="3753">
seznam	seznam	k1gInSc4	seznam
nesouladů	nesoulad	k1gInPc2	nesoulad
mezi	mezi	k7c7	
skutečným	skutečný	k2eAgInSc7d1	skutečný
a	a	k8xC	
evidovaným	evidovaný	k2eAgInSc7d1	evidovaný
stavem	stav	k1gInSc7	stav
pozemků	pozemek	k1gInPc2	pozemek
</sent>
<sent id="3754">
seznam	seznam	k1gInSc4	seznam
uživatelů	uživatel	k1gMnPc2	uživatel
a	a	k8xC	
vlastníků	vlastník	k1gMnPc2	vlastník
(	(	kIx(	
<g/>
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
)	)	kIx)	
</sent>
<sent id="3755">
slučování	slučování	k1gNnSc2	slučování
parcel	parcela	k1gFnPc2	parcela
</sent>
<sent id="3756">
slučování	slučování	k1gNnSc4	slučování
pozemků	pozemek	k1gInPc2	pozemek
</sent>
<sent id="3757">
soukromé	soukromý	k2eAgNnSc4d1	soukromé
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
právnické	právnický	k2eAgFnSc2d1	právnická
osoby	osoba	k1gFnSc2	osoba
</sent>
<sent id="3758">
soupis	soupis	k1gInSc1	soupis
parcel	parcela	k1gFnPc2	parcela
</sent>
<sent id="3759">
správa	správa	k1gFnSc1	správa
národního	národní	k2eAgInSc2d1	národní
majetku	majetek	k1gInSc2	majetek
</sent>
<sent id="3760">
správa	správa	k1gFnSc1	správa
nemovitostí	nemovitost	k1gFnPc2	nemovitost
ve	v	k7c6	
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
státu	stát	k1gInSc2	stát
</sent>
<sent id="3761">
stavební	stavební	k2eAgInSc4d1	stavební
pozemek	pozemek	k1gInSc4	pozemek
</sent>
<sent id="3762">
sumarizační	sumarizační	k2eAgInPc4d1	sumarizační
výkazy	výkaz	k1gInPc4	výkaz
</sent>
<sent id="3763">
trojmezí	trojmezí	k1gNnSc4	trojmezí
</sent>
<sent id="3764">
trvalé	trvalá	k1gFnSc2	trvalá
užívání	užívání	k1gNnSc2	užívání
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="3765">
typ	typ	k1gInSc1	typ
budovy	budova	k1gFnSc2	budova
</sent>
<sent id="3766">
typ	typ	k1gInSc1	typ
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	
<g/>
bytu	byt	k1gInSc2	byt
nebo	nebo	k8xC	
nebytového	bytový	k2eNgInSc2d1	nebytový
prostoru	prostor	k1gInSc2	prostor
<g/>
)	)	kIx)	
</sent>
<sent id="3767">
údaj	údaj	k1gInSc1	údaj
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="3768">
údaj	údaj	k1gInSc1	údaj
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="3769">
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	
budově	budova	k1gFnSc6	budova
</sent>
<sent id="3770">
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	
bytu	byt	k1gInSc6	byt
a	a	k8xC	
nebytovém	bytový	k2eNgInSc6d1	nebytový
prostoru	prostor	k1gInSc6	prostor
</sent>
<sent id="3771">
záznamy	záznam	k1gInPc7	záznam
v	v	k7c6	
pozemkové	pozemkový	k2eAgFnSc6d1	pozemková
knize	kniha	k1gFnSc6	kniha
</sent>
<sent id="3773">
číslo	číslo	k1gNnSc4	číslo
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	
<g/>
bytu	byt	k1gInSc2	byt
a	a	k8xC	
nebytového	bytový	k2eNgInSc2d1	nebytový
prostoru	prostor	k1gInSc2	prostor
<g/>
)	)	kIx)	
</sent>
<sent id="3774">
evidenční	evidenční	k2eAgInSc4d1	evidenční
list	list	k1gInSc4	list
</sent>
<sent id="3925">
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
(	(	kIx(	
<g/>
digitálních	digitální	k2eAgNnPc2d1	digitální
<g/>
)	)	kIx)	
geografických	geografický	k2eAgNnPc2d1	geografické
dat	datum	k1gNnPc2	datum
</sent>
<sent id="3777">
prvotní	prvotní	k2eAgInSc4d1	prvotní
doklad	doklad	k1gInSc4	doklad
</sent>
<sent id="3778">
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
</sent>
<sent id="3779">
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	
parcele	parcela	k1gFnSc6	parcela
</sent>
<sent id="3781">
urbariát	urbariát	k1gInSc1	urbariát
</sent>
<sent id="3782">
územně	územně	k6eAd1	
správní	správní	k2eAgFnSc1d1	správní
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="3783">
územně	územně	k6eAd1	
technická	technický	k2eAgFnSc1d1	technická
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="3784">
územní	územní	k2eAgFnSc1d1	územní
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="3785">
užívací	užívací	k2eAgFnSc1d1	užívací
hranice	hranice	k1gFnSc1	hranice
</sent>
<sent id="3786">
užívání	užívání	k1gNnSc6	užívání
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="3787">
uživatel	uživatel	k1gMnSc1	uživatel
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="3788">
věcné	věcný	k2eAgNnSc4d1	věcné
břemeno	břemeno	k1gNnSc4	břemeno
</sent>
<sent id="3790">
věcné	věcný	k2eAgNnSc4d1	věcné
právo	právo	k1gNnSc4	právo
k	k	k7c3	
nemovitosti	nemovitost	k1gFnSc3	nemovitost
</sent>
<sent id="3791">
veřejné	veřejný	k2eAgFnSc2d1	veřejná
knihy	kniha	k1gFnSc2	kniha
</sent>
<sent id="3793">
veřejnost	veřejnost	k1gFnSc1	veřejnost
katastrálního	katastrální	k2eAgInSc2d1	katastrální
operátu	operát	k1gInSc2	operát
</sent>
<sent id="3794">
vklad	vklad	k1gInSc1	vklad
práva	právo	k1gNnSc2	právo
(	(	kIx(	
<g/>
intabulace	intabulace	k1gFnSc1	intabulace
<g/>
)	)	kIx)	
</sent>
<sent id="3795">
vlastnické	vlastnický	k2eAgNnSc4d1	vlastnické
právo	právo	k1gNnSc4	právo
</sent>
<sent id="3796">
vlastnický	vlastnický	k2eAgInSc4d1	vlastnický
vztah	vztah	k1gInSc4	vztah
</sent>
<sent id="3797">
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
</sent>
<sent id="3799">
vlastník	vlastník	k1gMnSc1	vlastník
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="3800">
vydržení	vydržení	k1gNnSc1	vydržení
práv	právo	k1gNnPc2	právo
k	k	k7c3	
nemovitostem	nemovitost	k1gFnPc3	nemovitost
</sent>
<sent id="3801">
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
platnosti	platnost	k1gFnSc2	platnost
obnoveného	obnovený	k2eAgInSc2d1	obnovený
operátu	operát	k1gInSc2	operát
</sent>
<sent id="3802">
výkaz	výkaz	k1gInSc1	výkaz
úhrnných	úhrnný	k2eAgFnPc2d1	úhrnná
hodnot	hodnota	k1gFnPc2	hodnota
druhů	druh	k1gMnPc2	druh
pozemků	pozemek	k1gInPc2	pozemek
</sent>
<sent id="3803">
výkaz	výkaz	k1gInSc1	výkaz
změn	změna	k1gFnPc2	změna
</sent>
<sent id="3804">
výměra	výměra	k1gFnSc1	výměra
dílu	díl	k1gInSc2	díl
bonitované	bonitovaný	k2eAgFnSc2d1	bonitovaná
půdně	půdně	k6eAd1	
ekologické	ekologický	k2eAgFnSc2d1	ekologická
jednotky	jednotka	k1gFnSc2	jednotka
</sent>
<sent id="3805">
výměra	výměra	k1gFnSc1	výměra
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
</sent>
<sent id="3806">
výpis	výpis	k1gInSc1	výpis
z	z	k7c2	
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="3807">
výpis	výpis	k1gInSc1	výpis
z	z	k7c2	
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="3808">
základní	základní	k2eAgFnSc1d1	základní
územní	územní	k2eAgFnSc1d1	územní
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="3809">
vlastnické	vlastnický	k2eAgNnSc4d1	vlastnické
právo	právo	k1gNnSc4	právo
k	k	k7c3	
nemovitosti	nemovitost	k1gFnSc3	nemovitost
</sent>
<sent id="3810">
úhrnné	úhrnný	k2eAgFnPc1d1	úhrnná
hodnoty	hodnota	k1gFnPc1	hodnota
druhů	druh	k1gInPc2	druh
pozemků	pozemek	k1gInPc2	pozemek
(	(	kIx(	
<g/>
ÚHDP	ÚHDP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="3811">
příslušnost	příslušnost	k1gFnSc1	příslušnost
k	k	k7c3	
vlastníku	vlastník	k1gMnSc3	vlastník
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="3812">
jiný	jiný	k2eAgInSc4d1	jiný
oprávněný	oprávněný	k2eAgInSc4d1	oprávněný
</sent>
<sent id="3813">
podrobnější	podrobný	k2eAgInSc1d2	podrobnější
údaj	údaj	k1gInSc1	údaj
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="3814">
právo	právo	k1gNnSc4	právo
hospodaření	hospodaření	k1gNnSc2	hospodaření
</sent>
<sent id="3815">
právo	právo	k1gNnSc4	právo
hospodařit	hospodařit	k5eAaImF	
</sent>
<sent id="3816">
řízení	řízení	k1gNnSc2	řízení
(	(	kIx(	
<g/>
v	v	k7c6	
katastru	katastr	k1gInSc6	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
)	)	kIx)	
</sent>
<sent id="3817">
sdružený	sdružený	k2eAgInSc4d1	sdružený
pozemek	pozemek	k1gInSc4	pozemek
</sent>
<sent id="3819">
seznam	seznam	k1gInSc4	seznam
vlastníků	vlastník	k1gMnPc2	vlastník
nemovitostí	nemovitost	k1gFnPc2	nemovitost
a	a	k8xC	
jiných	jiný	k2eAgInPc6d1	jiný
oprávněných	oprávněný	k2eAgInPc6d1	oprávněný
</sent>
<sent id="3820">
srovnávací	srovnávací	k2eAgInSc4d1	srovnávací
sestavení	sestavení	k1gNnSc1	sestavení
parcel	parcela	k1gFnPc2	parcela
</sent>
<sent id="3821">
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	
vlastníku	vlastník	k1gMnSc6	vlastník
nemovitosti	nemovitost	k1gFnSc2	nemovitost
a	a	k8xC	
jiném	jiný	k2eAgInSc6d1	jiný
oprávněném	oprávněný	k2eAgInSc6d1	oprávněný
</sent>
<sent id="3823">
číslo	číslo	k1gNnSc4	číslo
listu	list	k1gInSc2	list
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
</sent>
<sent id="3824">
sumarizační	sumarizační	k2eAgFnSc1d1	sumarizační
územní	územní	k2eAgFnSc1d1	územní
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="3825">
šetření	šetření	k1gNnSc2	šetření
(	(	kIx(	
<g/>
komisionální	komisionální	k2eAgInPc4d1	komisionální
<g/>
)	)	kIx)	
</sent>
<sent id="3826">
územní	územní	k2eAgInSc4d1	územní
celek	celek	k1gInSc4	celek
</sent>
<sent id="3827">
výkaz	výkaz	k1gInSc1	výkaz
výměr	výměr	k1gInSc4	výměr
</sent>
<sent id="3828">
atlasová	atlasový	k2eAgFnSc1d1	Atlasová
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="3829">
atlasová	atlasový	k2eAgFnSc1d1	Atlasová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="3830">
autoatlas	autoatlas	k1gInSc1	autoatlas
</sent>
<sent id="3832">
číslo	číslo	k1gNnSc4	číslo
poledníkového	poledníkový	k2eAgInSc2d1	poledníkový
pásu	pás	k1gInSc2	pás
</sent>
<sent id="3835">
didaktická	didaktický	k2eAgFnSc1d1	didaktická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="3836">
důlní	důlní	k2eAgFnSc1d1	důlní
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="3837">
formát	formát	k1gInSc1	formát
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="3838">
fyzická	fyzický	k2eAgFnSc1d1	fyzická
školní	školní	k2eAgFnSc1d1	školní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="3839">
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="3841">
klad	klad	k1gInSc4	klad
mapových	mapový	k2eAgInPc2d1	mapový
listů	list	k1gInPc2	list
</sent>
<sent id="3842">
mapový	mapový	k2eAgInSc4d1	mapový
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="3843">
přehled	přehled	k1gInSc1	přehled
kladu	klad	k1gInSc2	klad
mapových	mapový	k2eAgInPc2d1	mapový
listů	list	k1gInPc2	list
</sent>
<sent id="3844">
rám	rám	k1gInSc1	rám
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3846">
souřadnicová	souřadnicový	k2eAgFnSc1d1	souřadnicová
síť	síť	k1gFnSc1	síť
</sent>
<sent id="3847">
školní	školní	k2eAgFnSc1d1	školní
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="3848">
školní	školní	k2eAgFnSc1d1	školní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="3849">
textová	textový	k2eAgFnSc1d1	textová
část	část	k1gFnSc1	část
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="3850">
třídění	třídění	k1gNnSc4	třídění
atlasů	atlas	k1gInPc2	atlas
</sent>
<sent id="3851">
typ	typ	k1gInSc1	typ
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="3852">
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
rámová	rámový	k2eAgFnSc1d1	rámová
čára	čára	k1gFnSc1	čára
</sent>
<sent id="3853">
katalog	katalog	k1gInSc4	katalog
topografických	topografický	k2eAgInPc2d1	topografický
objektů	objekt	k1gInPc2	objekt
</sent>
<sent id="3854">
tyflokartografie	tyflokartografie	k1gFnSc1	tyflokartografie
</sent>
<sent id="4166">
koherentní	koherentní	k2eAgInSc4d1	koherentní
systém	systém	k1gInSc4	systém
měřicích	měřicí	k2eAgFnPc2d1	měřicí
jednotek	jednotka	k1gFnPc2	jednotka
</sent>
<sent id="3859">
veřejné	veřejný	k2eAgNnSc1d1	veřejné
prostranství	prostranství	k1gNnSc1	prostranství
</sent>
<sent id="3860">
analogová	analogový	k2eAgFnSc1d1	analogová
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="3861">
analytická	analytický	k2eAgFnSc1d1	analytická
aerotriangulace	aerotriangulace	k1gFnSc1	aerotriangulace
</sent>
<sent id="3862">
analytická	analytický	k2eAgFnSc1d1	analytická
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="3863">
bloková	blokový	k2eAgFnSc1d1	bloková
aerotriangulace	aerotriangulace	k1gFnSc1	aerotriangulace
</sent>
<sent id="3864">
družicová	družicový	k2eAgFnSc1d1	družicová
aerotriangulace	aerotriangulace	k1gFnSc1	aerotriangulace
</sent>
<sent id="3865">
družicová	družicový	k2eAgFnSc1d1	družicová
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="3866">
dvojsnímková	dvojsnímkový	k2eAgFnSc1d1	dvojsnímkový
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="3867">
fotogrammetrie	fotogrammetrie	k1gFnSc2	fotogrammetrie
</sent>
<sent id="3868">
fotogrammetrie	fotogrammetrie	k1gFnSc2	fotogrammetrie
v	v	k7c6	
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
</sent>
<sent id="3869">
fotointerpretace	fotointerpretace	k1gFnSc1	fotointerpretace
</sent>
<sent id="3870">
fotomapa	fotomapa	k1gFnSc1	fotomapa
</sent>
<sent id="3871">
fotomozaika	fotomozaika	k1gFnSc1	fotomozaika
</sent>
<sent id="3872">
fotoplán	fotoplán	k1gInSc1	fotoplán
</sent>
<sent id="3873">
jednosnímková	jednosnímkový	k2eAgFnSc1d1	jednosnímkový
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="3874">
letecká	letecký	k2eAgFnSc1d1	letecká
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="3875">
pozemní	pozemní	k2eAgFnSc1d1	pozemní
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="3876">
průseková	průsekový	k2eAgFnSc1d1	průseková
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="3877">
rentgenová	rentgenový	k2eAgFnSc1d1	rentgenová
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="3878">
řadová	řadový	k2eAgFnSc1d1	řadová
aerotriangulace	aerotriangulace	k1gFnSc1	aerotriangulace
</sent>
<sent id="3879">
semianalytická	semianalytický	k2eAgFnSc1d1	semianalytický
aerotriangulace	aerotriangulace	k1gFnSc1	aerotriangulace
</sent>
<sent id="3881">
autorský	autorský	k2eAgInSc1d1	autorský
koncept	koncept	k1gInSc1	koncept
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3882">
autorský	autorský	k2eAgInSc1d1	autorský
originál	originál	k1gInSc1	originál
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="3883">
barevný	barevný	k2eAgInSc4d1	barevný
originál	originál	k1gInSc4	originál
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3884">
dílčí	dílčí	k2eAgInSc1d1	dílčí
kartografický	kartografický	k2eAgInSc1d1	kartografický
(	(	kIx(	
<g/>
vydavatelský	vydavatelský	k2eAgInSc1d1	vydavatelský
<g/>
)	)	kIx)	
originál	originál	k1gInSc1	originál
</sent>
<sent id="3885">
fólie	fólie	k1gFnSc1	fólie
s	s	k7c7	
rycí	rycí	k2eAgFnSc7d1	rycí
vrstvou	vrstva	k1gFnSc7	vrstva
</sent>
<sent id="3886">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
kresba	kresba	k1gFnSc1	kresba
</sent>
<sent id="3887">
kartografický	kartografický	k2eAgInSc1d1	kartografický
originál	originál	k1gInSc1	originál
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="3888">
kartografický	kartografický	k2eAgInSc4d1	kartografický
originál	originál	k1gInSc4	originál
polohopisu	polohopis	k1gInSc2	polohopis
</sent>
<sent id="3889">
kartografický	kartografický	k2eAgInSc4d1	kartografický
originál	originál	k1gInSc4	originál
vodstva	vodstvo	k1gNnSc2	vodstvo
</sent>
<sent id="3890">
kartografický	kartografický	k2eAgInSc4d1	kartografický
originál	originál	k1gInSc4	originál
výškopisu	výškopis	k1gInSc2	výškopis
</sent>
<sent id="3891">
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
kresba	kresba	k1gFnSc1	kresba
</sent>
<sent id="3892">
kresba	kresba	k1gFnSc1	kresba
přes	přes	k7c4	
rám	rám	k1gInSc4	rám
</sent>
<sent id="3893">
moaré	moaré	k1gNnSc4	moaré
</sent>
<sent id="3894">
montážní	montážní	k2eAgInSc1d1	montážní
originál	originál	k1gInSc1	originál
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3895">
optický	optický	k2eAgInSc4d1	optický
pantograf	pantograf	k1gInSc4	pantograf
</sent>
<sent id="3896">
praktická	praktický	k2eAgFnSc1d1	praktická
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="3897">
revizní	revizní	k2eAgInSc1d1	revizní
originál	originál	k1gInSc1	originál
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="3898">
rycí	rycí	k2eAgInSc4d1	rycí
pantograf	pantograf	k1gInSc4	pantograf
</sent>
<sent id="3899">
sestavitelský	sestavitelský	k2eAgInSc4d1	sestavitelský
originál	originál	k1gInSc4	originál
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="3900">
závěsný	závěsný	k2eAgInSc4d1	závěsný
pantograf	pantograf	k1gInSc4	pantograf
</sent>
<sent id="3901">
animace	animace	k1gFnSc2	animace
</sent>
<sent id="3903">
datový	datový	k2eAgInSc4d1	datový
sklad	sklad	k1gInSc4	sklad
</sent>
<sent id="3904">
digitální	digitální	k2eAgFnSc1d1	digitální
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="3905">
dolování	dolování	k1gNnPc2	dolování
dat	datum	k1gNnPc2	datum
</sent>
<sent id="3907">
drátěný	drátěný	k2eAgInSc4d1	drátěný
model	model	k1gInSc4	model
</sent>
<sent id="3909">
hypertextový	hypertextový	k2eAgInSc4d1	hypertextový
odkaz	odkaz	k1gInSc4	odkaz
</sent>
<sent id="3911">
lokální	lokální	k2eAgFnSc1d1	lokální
(	(	kIx(	
<g/>
počítačová	počítačový	k2eAgFnSc1d1	počítačová
<g/>
)	)	kIx)	
síť	síť	k1gFnSc1	síť
(	(	kIx(	
<g/>
LAN	lano	k1gNnPc2	lano
<g/>
)	)	kIx)	
</sent>
<sent id="3912">
přístupové	přístupový	k2eAgNnSc4d1	přístupové
právo	právo	k1gNnSc4	právo
</sent>
<sent id="3913">
otevřený	otevřený	k2eAgInSc4d1	otevřený
zdroj	zdroj	k1gInSc4	zdroj
</sent>
<sent id="3914">
přihlášení	přihlášení	k1gNnSc2	přihlášení
(	(	kIx(	
<g/>
do	do	k7c2	
systému	systém	k1gInSc2	systém
<g/>
)	)	kIx)	
</sent>
<sent id="3915">
rastrovací	rastrovací	k2eAgInSc1d1	rastrovací
procesor	procesor	k1gInSc1	procesor
(	(	kIx(	
<g/>
RIP	RIP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="3916">
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
(	(	kIx(	
<g/>
počítačová	počítačový	k2eAgFnSc1d1	počítačová
<g/>
)	)	kIx)	
síť	síť	k1gFnSc1	síť
(	(	kIx(	
<g/>
WAN	WAN	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="3918">
škálovatelnost	škálovatelnost	k1gFnSc1	škálovatelnost
</sent>
<sent id="3919">
slabý	slabý	k2eAgMnSc1d1	slabý
klient	klient	k1gMnSc1	klient
</sent>
<sent id="3920">
silný	silný	k2eAgMnSc1d1	silný
klient	klient	k1gMnSc1	klient
</sent>
<sent id="3923">
výuka	výuka	k1gFnSc1	výuka
pomocí	pomocí	k7c2	
počítače	počítač	k1gInSc2	počítač
</sent>
<sent id="3924">
jednotka	jednotka	k1gFnSc1	jednotka
v	v	k7c6	
domě	dům	k1gInSc6	dům
</sent>
<sent id="3926">
automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="3927">
objekt	objekt	k1gInSc1	objekt
</sent>
<sent id="3928">
sémantická	sémantický	k2eAgFnSc1d1	sémantická
přesnost	přesnost	k1gFnSc1	přesnost
</sent>
<sent id="4320">
typ	typ	k1gInSc1	typ
parcely	parcela	k1gFnSc2	parcela
</sent>
<sent id="3930">
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
</sent>
<sent id="4001">
aberace	aberace	k1gFnSc1	aberace
světla	světlo	k1gNnSc2	světlo
</sent>
<sent id="4002">
absolutní	absolutní	k2eAgInSc1d1	absolutní
elipsoid	elipsoid	k1gInSc1	elipsoid
</sent>
<sent id="4003">
absolutní	absolutní	k2eAgInSc1d1	absolutní
tíhový	tíhový	k2eAgInSc1d1	tíhový
bod	bod	k1gInSc1	bod
</sent>
<sent id="4004">
absorpce	absorpce	k1gFnSc1	absorpce
</sent>
<sent id="4005">
adjustace	adjustace	k1gFnSc1	adjustace
měřického	měřický	k2eAgInSc2d1	měřický
náčrtu	náčrt	k1gInSc2	náčrt
</sent>
<sent id="4006">
administrativní	administrativní	k2eAgFnSc1d1	administrativní
hranice	hranice	k1gFnSc1	hranice
</sent>
<sent id="4007">
adresa	adresa	k1gFnSc1	adresa
</sent>
<sent id="4008">
afelium	afelium	k1gNnSc4	afelium
</sent>
<sent id="4009">
agona	agona	k1gFnSc1	agona
</sent>
<sent id="4010">
agrární	agrární	k2eAgFnSc1d1	agrární
operace	operace	k1gFnSc1	operace
</sent>
<sent id="4011">
agregace	agregace	k1gFnSc1	agregace
dat	datum	k1gNnPc2	datum
</sent>
<sent id="4012">
aktivní	aktivní	k2eAgFnSc1d1	aktivní
družice	družice	k1gFnSc1	družice
</sent>
<sent id="4013">
aktualizace	aktualizace	k1gFnSc1	aktualizace
báze	báze	k1gFnSc1	báze
dat	datum	k1gNnPc2	datum
</sent>
<sent id="4014">
aktualizace	aktualizace	k1gFnSc1	aktualizace
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="4015">
aktuálnost	aktuálnost	k1gFnSc1	aktuálnost
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="4017">
almanach	almanach	k1gInSc1	almanach
družic	družice	k1gFnPc2	družice
GPS	GPS	kA	
</sent>
<sent id="4018">
almukantarat	almukantarat	k1gInSc1	almukantarat
</sent>
<sent id="4019">
analogová	analogový	k2eAgFnSc1d1	analogová
data	datum	k1gNnSc2	datum
</sent>
<sent id="4020">
délka	délka	k1gFnSc1	délka
kolmice	kolmice	k1gFnSc1	kolmice
</sent>
<sent id="4022">
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
železniční	železniční	k2eAgFnSc1d1	železniční
mapa	mapa	k1gFnSc1	mapa
(	(	kIx(	
<g/>
JŽM	JŽM	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4023">
kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4024">
mapa	mapa	k1gFnSc1	mapa
malého	malý	k2eAgNnSc2d1	malé
měřítka	měřítko	k1gNnSc2	měřítko
</sent>
<sent id="4025">
mapa	mapa	k1gFnSc1	mapa
středního	střední	k2eAgNnSc2d1	střední
měřítka	měřítko	k1gNnSc2	měřítko
</sent>
<sent id="4026">
mapa	mapa	k1gFnSc1	mapa
velkého	velký	k2eAgNnSc2d1	velké
měřítka	měřítko	k1gNnSc2	měřítko
</sent>
<sent id="4027">
mapér	mapér	k1gInSc1	mapér
</sent>
<sent id="4028">
mapování	mapování	k1gNnSc2	mapování
ve	v	k7c6	
velkých	velký	k2eAgNnPc6d1	velké
měřítkách	měřítko	k1gNnPc6	měřítko
</sent>
<sent id="4029">
měřická	měřický	k2eAgFnSc1d1	měřická
přímka	přímka	k1gFnSc1	přímka
</sent>
<sent id="4030">
metoda	metoda	k1gFnSc1	metoda
měřického	měřický	k2eAgInSc2d1	měřický
stolu	stol	k1gInSc2	stol
</sent>
<sent id="4031">
metoda	metoda	k1gFnSc1	metoda
podrobného	podrobný	k2eAgNnSc2d1	podrobné
měření	měření	k1gNnSc2	měření
</sent>
<sent id="4032">
metoda	metoda	k1gFnSc1	metoda
pravoúhlých	pravoúhlý	k2eAgFnPc2d1	pravoúhlá
souřadnic	souřadnice	k1gFnPc2	souřadnice
</sent>
<sent id="4033">
oměrná	oměrný	k2eAgFnSc1d1	oměrná
míra	míra	k1gFnSc1	míra
</sent>
<sent id="4034">
podrobné	podrobný	k2eAgNnSc4d1	podrobné
měření	měření	k1gNnSc4	měření
</sent>
<sent id="4035">
polární	polární	k2eAgFnSc1d1	polární
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4036">
polární	polární	k2eAgFnSc1d1	polární
souřadnice	souřadnice	k1gFnSc1	souřadnice
</sent>
<sent id="4037">
staničení	staničení	k1gNnSc4	staničení
</sent>
<sent id="4038">
technická	technický	k2eAgFnSc1d1	technická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4039">
Technická	technický	k2eAgFnSc1d1	technická
mapa	mapa	k1gFnSc1	mapa
města	město	k1gNnSc2	město
(	(	kIx(	
<g/>
TMM	TMM	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4040">
Technickohospodářská	technickohospodářský	k2eAgFnSc1d1	technickohospodářská
mapa	mapa	k1gFnSc1	mapa
(	(	kIx(	
<g/>
THM	THM	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4041">
tematická	tematický	k2eAgFnSc1d1	tematická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4042">
topografická	topografický	k2eAgFnSc1d1	topografická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4043">
účelová	účelový	k2eAgFnSc1d1	účelová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4044">
základní	základní	k2eAgFnSc1d1	základní
mapa	mapa	k1gFnSc1	mapa
(	(	kIx(	
<g/>
ZM	ZM	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4045">
základní	základní	k2eAgFnSc1d1	základní
mapa	mapa	k1gFnSc1	mapa
velkého	velký	k2eAgNnSc2d1	velké
měřítka	měřítko	k1gNnSc2	měřítko
(	(	kIx(	
<g/>
ZMVM	ZMVM	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4046">
barevná	barevný	k2eAgFnSc1d1	barevná
soukopie	soukopie	k1gFnSc1	soukopie
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="4047">
barevná	barevný	k2eAgFnSc1d1	barevná
výplň	výplň	k1gFnSc1	výplň
</sent>
<sent id="4048">
barevný	barevný	k2eAgInSc4d1	barevný
náhled	náhled	k1gInSc4	náhled
</sent>
<sent id="4049">
diapozitivní	diapozitivní	k2eAgFnSc1d1	diapozitivní
maska	maska	k1gFnSc1	maska
</sent>
<sent id="4050">
dvoustopé	dvoustopý	k2eAgNnSc4d1	dvoustopé
rýsovací	rýsovací	k2eAgNnSc4d1	rýsovací
pero	pero	k1gNnSc4	pero
</sent>
<sent id="4051">
dvoustopé	dvoustopý	k2eAgNnSc1d1	dvoustopé
volnoosé	volnoosý	k2eAgNnSc1d1	volnoosý
rýsovací	rýsovací	k2eAgNnSc1d1	rýsovací
pero	pero	k1gNnSc1	pero
</sent>
<sent id="4052">
fotomechanické	fotomechanický	k2eAgNnSc1d1	fotomechanický
tónování	tónování	k1gNnSc1	tónování
terénního	terénní	k2eAgInSc2d1	terénní
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="4053">
kartografické	kartografický	k2eAgFnSc2d1	kartografická
práce	práce	k1gFnSc2	práce
</sent>
<sent id="4054">
kartolitografie	kartolitografie	k1gFnSc1	kartolitografie
</sent>
<sent id="4055">
konstrukční	konstrukční	k2eAgInSc1d1	konstrukční
list	list	k1gInSc1	list
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="4057">
kopírovací	kopírovací	k2eAgFnSc1d1	kopírovací
(	(	kIx(	
<g/>
krycí	krycí	k2eAgFnSc1d1	krycí
<g/>
)	)	kIx)	
maska	maska	k1gFnSc1	maska
</sent>
<sent id="4058">
kreslicí	kreslicí	k2eAgInSc4d1	kreslicí
pero	pero	k1gNnSc1	pero
</sent>
<sent id="4059">
kreslicí	kreslicí	k2eAgFnSc1d1	kreslicí
šablona	šablona	k1gFnSc1	šablona
</sent>
<sent id="4060">
Lehmannovy	Lehmannův	k2eAgFnSc2d1	Lehmannova
šrafy	šrafa	k1gFnSc2	šrafa
</sent>
<sent id="4062">
litografie	litografie	k1gFnSc1	litografie
</sent>
<sent id="4063">
litografie	litografie	k1gFnSc2	litografie
barevných	barevný	k2eAgFnPc2d1	barevná
ploch	plocha	k1gFnPc2	plocha
</sent>
<sent id="4064">
mechanická	mechanický	k2eAgFnSc1d1	mechanická
šablona	šablona	k1gFnSc1	šablona
</sent>
<sent id="4065">
negativní	negativní	k2eAgFnSc1d1	negativní
maska	maska	k1gFnSc1	maska
</sent>
<sent id="4066">
negativní	negativní	k2eAgInSc4d1	negativní
rytí	rytí	k1gNnSc1	rytí
do	do	k7c2	
vrstvy	vrstva	k1gFnSc2	vrstva
</sent>
<sent id="4067">
orientačně	orientačně	k6eAd1	
důležitý	důležitý	k2eAgInSc4d1	důležitý
předmět	předmět	k1gInSc4	předmět
</sent>
<sent id="4068">
pikýrovací	pikýrovací	k2eAgFnSc1d1	pikýrovací
jehla	jehla	k1gFnSc1	jehla
</sent>
<sent id="4069">
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
rytí	rytí	k1gNnSc1	rytí
do	do	k7c2	
vrstvy	vrstva	k1gFnSc2	vrstva
</sent>
<sent id="4070">
předloha	předloha	k1gFnSc1	předloha
barev	barva	k1gFnPc2	barva
</sent>
<sent id="4071">
předpis	předpis	k1gInSc1	předpis
oprav	oprava	k1gFnPc2	oprava
</sent>
<sent id="4072">
přenášení	přenášení	k1gNnSc6	přenášení
obsahu	obsah	k1gInSc2	obsah
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4073">
redakční	redakční	k2eAgInSc4d1	redakční
náhled	náhled	k1gInSc4	náhled
</sent>
<sent id="4074">
revize	revize	k1gFnSc1	revize
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4075">
revizní	revizní	k2eAgFnSc1d1	revizní
průsvitka	průsvitka	k1gFnSc1	průsvitka
</sent>
<sent id="4076">
revizní	revizní	k2eAgFnSc1d1	revizní
tematická	tematický	k2eAgFnSc1d1	tematická
průsvitka	průsvitka	k1gFnSc1	průsvitka
</sent>
<sent id="7164">
souřadnicový	souřadnicový	k2eAgInSc4d1	souřadnicový
systém	systém	k1gInSc4	systém
UTM	UTM	kA	
</sent>
<sent id="4079">
rycí	rycí	k2eAgFnSc1d1	rycí
trojnožka	trojnožka	k1gFnSc1	trojnožka
</sent>
<sent id="4080">
rýsovací	rýsovací	k2eAgInSc4d1	rýsovací
pero	pero	k1gNnSc1	pero
</sent>
<sent id="4081">
rytí	rytí	k1gNnPc2	rytí
do	do	k7c2	
vrstvy	vrstva	k1gFnSc2	vrstva
</sent>
<sent id="4082">
sestavitelské	sestavitelský	k2eAgFnSc2d1	sestavitelský
práce	práce	k1gFnSc2	práce
</sent>
<sent id="4083">
sestavování	sestavování	k1gNnSc6	sestavování
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4084">
sloupávací	sloupávací	k2eAgFnSc1d1	sloupávací
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="4085">
sloupávání	sloupávání	k1gNnSc2	sloupávání
(	(	kIx(	
<g/>
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
)	)	kIx)	
</sent>
<sent id="4086">
stínování	stínování	k1gNnSc2	stínování
terénního	terénní	k2eAgInSc2d1	terénní
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="4087">
styková	stykový	k2eAgFnSc1d1	styková
páska	páska	k1gFnSc1	páska
</sent>
<sent id="4088">
šrafování	šrafování	k1gNnSc2	šrafování
terénního	terénní	k2eAgInSc2d1	terénní
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="4089">
technické	technický	k2eAgFnSc2d1	technická
šrafy	šrafa	k1gFnSc2	šrafa
</sent>
<sent id="4090">
transplantace	transplantace	k1gFnSc2	transplantace
mapových	mapový	k2eAgFnPc2d1	mapová
značek	značka	k1gFnPc2	značka
a	a	k8xC	
popisu	popis	k1gInSc2	popis
</sent>
<sent id="4091">
trubičkové	trubičkový	k2eAgNnSc4d1	trubičkové
technické	technický	k2eAgNnSc4d1	technické
pero	pero	k1gNnSc4	pero
</sent>
<sent id="4092">
volnoosé	volnoosý	k2eAgNnSc4d1	volnoosý
rýsovací	rýsovací	k2eAgNnSc4d1	rýsovací
pero	pero	k1gNnSc4	pero
</sent>
<sent id="4093">
volnoosý	volnoosý	k2eAgInSc1d1	volnoosý
rycí	rycí	k2eAgInSc1d1	rycí
přístrojek	přístrojek	k1gInSc1	přístrojek
</sent>
<sent id="4094">
vyrovnávací	vyrovnávací	k2eAgFnSc1d1	vyrovnávací
maska	maska	k1gFnSc1	maska
</sent>
<sent id="4095">
výškopisná	výškopisný	k2eAgFnSc1d1	výškopisná
průsvitka	průsvitka	k1gFnSc1	průsvitka
</sent>
<sent id="4096">
body	bod	k1gInPc7	bod
Základní	základní	k2eAgFnSc2d1	základní
geodynamické	geodynamický	k2eAgFnSc2d1	geodynamická
sítě	síť	k1gFnSc2	síť
ČR	ČR	kA	
</sent>
<sent id="4097">
Síť	síť	k1gFnSc1	síť
permanentních	permanentní	k2eAgFnPc2d1	permanentní
stanic	stanice	k1gFnPc2	stanice
GNSS	GNSS	kA	
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	
<g/>
CZEPOS	CZEPOS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4098">
Evropský	evropský	k2eAgInSc1d1	evropský
terestrický	terestrický	k2eAgInSc1d1	terestrický
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
(	(	kIx(	
<g/>
ETRS	ETRS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4099">
geodynamické	geodynamický	k2eAgFnPc1d1	geodynamická
bodové	bodový	k2eAgFnPc1d1	bodová
pole	pole	k1gFnPc1	pole
(	(	kIx(	
<g/>
GBP	GBP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5869">
následné	následný	k2eAgNnSc4d1	následné
zpracování	zpracování	k1gNnSc4	zpracování
</sent>
<sent id="5870">
navigační	navigační	k2eAgInSc4d1	navigační
signál	signál	k1gInSc4	signál
standardní	standardní	k2eAgFnSc2d1	standardní
přesnosti	přesnost	k1gFnSc2	přesnost
</sent>
<sent id="4101">
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
evropská	evropský	k2eAgFnSc1d1	Evropská
nivelační	nivelační	k2eAgFnSc1d1	nivelační
síť	síť	k1gFnSc1	síť
(	(	kIx(	
<g/>
UELN	UELN	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4102">
pevný	pevný	k2eAgInSc4d1	pevný
bod	bod	k1gInSc4	bod
podrobného	podrobný	k2eAgNnSc2d1	podrobné
polohového	polohový	k2eAgNnSc2d1	polohové
bodového	bodový	k2eAgNnSc2d1	bodové
pole	pole	k1gNnSc2	pole
(	(	kIx(	
<g/>
PBPP	PBPP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4103">
prostorová	prostorový	k2eAgFnSc1d1	prostorová
síť	síť	k1gFnSc1	síť
</sent>
<sent id="4104">
přidružený	přidružený	k2eAgInSc4d1	přidružený
bod	bod	k1gInSc4	bod
</sent>
<sent id="4105">
Tíhový	tíhový	k2eAgInSc4d1	tíhový
systém	systém	k1gInSc4	systém
1995	#num#	k4	
(	(	kIx(	
<g/>
S-Gr	S-Gr	k1gInSc1	S-Gr
<g/>
95	#num#	k4	
<g/>
)	)	kIx)	
</sent>
<sent id="4106">
Výškový	výškový	k2eAgInSc4d1	výškový
systém	systém	k1gInSc4	systém
baltský	baltský	k2eAgInSc4d1	baltský
-	-	kIx~	
po	po	k7c6	
vyrovnání	vyrovnání	k1gNnSc6	vyrovnání
(	(	kIx(	
<g/>
Bpv	Bpv	k1gMnSc1	Bpv
<g/>
)	)	kIx)	
</sent>
<sent id="4107">
Základní	základní	k2eAgFnSc1d1	základní
geodynamická	geodynamický	k2eAgFnSc1d1	geodynamická
síť	síť	k1gFnSc1	síť
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	
<g/>
ZGS	ZGS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4108">
špagety	špagety	k1gFnPc4	špagety
</sent>
<sent id="4109">
blokový	blokový	k2eAgInSc1d1	blokový
náčrt	náčrt	k1gInSc1	náčrt
</sent>
<sent id="4110">
bod	bod	k1gInSc1	bod
terénní	terénní	k2eAgFnSc2d1	terénní
kostry	kostra	k1gFnSc2	kostra
</sent>
<sent id="4111">
centrační	centrační	k2eAgInPc1d1	centrační
prvky	prvek	k1gInPc1	prvek
</sent>
<sent id="4112">
centrum	centrum	k1gNnSc4	centrum
</sent>
<sent id="4113">
číselné	číselný	k2eAgInPc4d1	číselný
geodetické	geodetický	k2eAgInPc4d1	geodetický
údaje	údaj	k1gInPc4	údaj
</sent>
<sent id="4114">
daný	daný	k2eAgInSc4d1	daný
bod	bod	k1gInSc4	bod
</sent>
<sent id="4115">
délková	délkový	k2eAgFnSc1d1	délková
základna	základna	k1gFnSc1	základna
</sent>
<sent id="4116">
délkové	délkový	k2eAgNnSc4d1	délkové
protínání	protínání	k1gNnSc4	protínání
</sent>
<sent id="4117">
elaborát	elaborát	k1gInSc1	elaborát
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4118">
geodetické	geodetický	k2eAgInPc1d1	geodetický
údaje	údaj	k1gInPc1	údaj
</sent>
<sent id="5196">
hustota	hustota	k1gFnSc1	hustota
rastru	rastr	k1gInSc2	rastr
</sent>
<sent id="4120">
hraniční	hraniční	k2eAgFnSc1d1	hraniční
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4121">
hraniční	hraniční	k2eAgInSc4d1	hraniční
znak	znak	k1gInSc4	znak
</sent>
<sent id="4122">
hřbetnice	hřbetnice	k1gFnSc1	hřbetnice
</sent>
<sent id="4123">
interpolace	interpolace	k1gFnSc1	interpolace
z	z	k7c2	
vrstevnic	vrstevnice	k1gFnPc2	vrstevnice
</sent>
<sent id="4124">
interpretace	interpretace	k1gFnSc1	interpretace
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4125">
jednoznačně	jednoznačně	k6eAd1	
identifikovatelný	identifikovatelný	k2eAgInSc4d1	identifikovatelný
bod	bod	k1gInSc4	bod
</sent>
<sent id="4126">
konstrukční	konstrukční	k2eAgInPc4d1	konstrukční
prvky	prvek	k1gInPc4	prvek
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4127">
křížová	křížový	k2eAgFnSc1d1	křížová
míra	míra	k1gFnSc1	míra
</sent>
<sent id="4128">
mapová	mapový	k2eAgFnSc1d1	mapová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="4129">
mapový	mapový	k2eAgInSc1d1	mapový
list	list	k1gInSc1	list
</sent>
<sent id="4130">
mapový	mapový	k2eAgInSc4d1	mapový
podklad	podklad	k1gInSc4	podklad
</sent>
<sent id="4131">
měření	měření	k1gNnSc2	měření
k	k	k7c3	
záměrné	záměrný	k2eAgFnSc3d1	záměrná
přímce	přímka	k1gFnSc3	přímka
</sent>
<sent id="4132">
měřická	měřický	k2eAgFnSc1d1	měřická
síť	síť	k1gFnSc1	síť
</sent>
<sent id="4133">
měřická	měřický	k2eAgFnSc1d1	měřická
značka	značka	k1gFnSc1	značka
</sent>
<sent id="4134">
měřický	měřický	k2eAgInSc4d1	měřický
originál	originál	k1gInSc4	originál
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4135">
místopis	místopis	k1gInSc1	místopis
bodu	bod	k1gInSc2	bod
</sent>
<sent id="4136">
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
(	(	kIx(	
<g/>
H	H	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4137">
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
stabilizace	stabilizace	k1gFnSc1	stabilizace
</sent>
<sent id="4138">
návrhová	návrhový	k2eAgFnSc1d1	návrhová
vrstevnice	vrstevnice	k1gFnSc1	vrstevnice
</sent>
<sent id="4139">
název	název	k1gInSc1	název
mapového	mapový	k2eAgNnSc2d1	mapové
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="4140">
název	název	k1gInSc1	název
mapového	mapový	k2eAgInSc2d1	mapový
listu	list	k1gInSc2	list
</sent>
<sent id="4141">
název	název	k1gInSc1	název
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4142">
operát	operát	k1gInSc1	operát
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4143">
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
měřická	měřický	k2eAgFnSc1d1	měřická
přímka	přímka	k1gFnSc1	přímka
</sent>
<sent id="4144">
označení	označení	k1gNnSc2	označení
mapového	mapový	k2eAgInSc2d1	mapový
listu	list	k1gInSc2	list
</sent>
<sent id="4146">
politická	politický	k2eAgFnSc1d1	politická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4147">
polohopis	polohopis	k1gInSc1	polohopis
</sent>
<sent id="4148">
síť	síť	k1gFnSc1	síť
měřických	měřický	k2eAgFnPc2d1	měřická
přímek	přímka	k1gFnPc2	přímka
</sent>
<sent id="4149">
volná	volný	k2eAgFnSc1d1	volná
měřická	měřický	k2eAgFnSc1d1	měřická
přímka	přímka	k1gFnSc1	přímka
</sent>
<sent id="4150">
výška	výška	k1gFnSc1	výška
(	(	kIx(	
<g/>
h	h	k?	
</sent>
<sent id="4151">
výškopis	výškopis	k1gInSc1	výškopis
</sent>
<sent id="4152">
záměrná	záměrný	k2eAgFnSc1d1	záměrná
přímka	přímka	k1gFnSc1	přímka
</sent>
<sent id="4154">
NULRAD	NULRAD	kA	
</sent>
<sent id="4155">
akreditovaná	akreditovaný	k2eAgFnSc1d1	akreditovaná
metrologická	metrologický	k2eAgFnSc1d1	metrologická
laboratoř	laboratoř	k1gFnSc1	laboratoř
</sent>
<sent id="4156">
bezrozměrná	bezrozměrný	k2eAgFnSc1d1	bezrozměrná
veličina	veličina	k1gFnSc1	veličina
</sent>
<sent id="4158">
etalon	etalon	k1gInSc1	etalon
</sent>
<sent id="4160">
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	
<g/>
měřicí	měřicí	k2eAgFnSc1d1	měřicí
<g/>
)	)	kIx)	
</sent>
<sent id="4161">
justování	justování	k1gNnSc4	justování
</sent>
<sent id="4162">
kalibrace	kalibrace	k1gFnSc1	kalibrace
</sent>
<sent id="4164">
kalibrační	kalibrační	k2eAgInSc4d1	kalibrační
certifikát	certifikát	k1gInSc4	certifikát
</sent>
<sent id="4165">
koherentní	koherentní	k2eAgFnSc1d1	koherentní
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="4167">
konvenčně	konvenčně	k6eAd1	
pravá	pravý	k2eAgFnSc1d1	pravá
hodnota	hodnota	k1gFnSc1	hodnota
veličiny	veličina	k1gFnSc2	veličina
</sent>
<sent id="4168">
legální	legální	k2eAgFnSc1d1	legální
metrologie	metrologie	k1gFnSc1	metrologie
</sent>
<sent id="4169">
měření	měření	k1gNnSc4	měření
</sent>
<sent id="4171">
měřitelná	měřitelný	k2eAgFnSc1d1	měřitelná
veličina	veličina	k1gFnSc1	veličina
</sent>
<sent id="4172">
metrologie	metrologie	k1gFnSc1	metrologie
</sent>
<sent id="4173">
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
systém	systém	k1gInSc1	systém
jednotek	jednotka	k1gFnPc2	jednotka
SI	si	k1gNnSc1	si
</sent>
<sent id="4174">
mimosystémová	mimosystémový	k2eAgFnSc1d1	mimosystémový
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="4175">
míra	míra	k1gFnSc1	míra
</sent>
<sent id="4177">
návaznost	návaznost	k1gFnSc1	návaznost
</sent>
<sent id="4178">
odvozená	odvozený	k2eAgFnSc1d1	odvozená
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="4179">
odvozená	odvozený	k2eAgFnSc1d1	odvozená
veličina	veličina	k1gFnSc1	veličina
</sent>
<sent id="4180">
podíl	podíl	k1gInSc1	podíl
</sent>
<sent id="4181">
pravá	pravý	k2eAgFnSc1d1	pravá
hodnota	hodnota	k1gFnSc1	hodnota
veličiny	veličina	k1gFnSc2	veličina
</sent>
<sent id="4182">
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
metrologie	metrologie	k1gFnSc1	metrologie
</sent>
<sent id="4184">
rozměr	rozměr	k1gInSc1	rozměr
veličiny	veličina	k1gFnSc2	veličina
</sent>
<sent id="4185">
systém	systém	k1gInSc1	systém
měřicích	měřicí	k2eAgFnPc2d1	měřicí
jednotek	jednotka	k1gFnPc2	jednotka
</sent>
<sent id="4186">
vědecká	vědecký	k2eAgFnSc1d1	vědecká
metrologie	metrologie	k1gFnSc1	metrologie
</sent>
<sent id="4187">
základní	základní	k2eAgInSc4d1	základní
(	(	kIx(	
<g/>
měřicí	měřicí	k2eAgInSc4d1	měřicí
<g/>
)	)	kIx)	
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="4188">
základní	základní	k2eAgFnSc1d1	základní
veličina	veličina	k1gFnSc1	veličina
</sent>
<sent id="4189">
značka	značka	k1gFnSc1	značka
(	(	kIx(	
<g/>
měřicí	měřicí	k2eAgFnSc2d1	měřicí
<g/>
)	)	kIx)	
jednotky	jednotka	k1gFnSc2	jednotka
</sent>
<sent id="4190">
doměření	doměření	k1gNnSc4	doměření
</sent>
<sent id="4191">
geodetický	geodetický	k2eAgInSc1d1	geodetický
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
</sent>
<sent id="4193">
tematické	tematický	k2eAgNnSc1d1	tematické
státní	státní	k2eAgNnSc1d1	státní
mapové	mapový	k2eAgNnSc1d1	mapové
dílo	dílo	k1gNnSc1	dílo
</sent>
<sent id="4194">
vztažné	vztažný	k2eAgNnSc4d1	vztažné
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="4195">
základní	základní	k2eAgFnSc4d1	základní
státní	státní	k2eAgNnSc4d1	státní
mapové	mapový	k2eAgNnSc4d1	mapové
dílo	dílo	k1gNnSc4	dílo
</sent>
<sent id="4196">
zeměměřictví	zeměměřictví	k1gNnSc4	zeměměřictví
</sent>
<sent id="4197">
absolutní	absolutní	k2eAgFnSc1d1	absolutní
chyba	chyba	k1gFnSc1	chyba
měření	měření	k1gNnSc1	měření
</sent>
<sent id="4198">
definiční	definiční	k2eAgFnSc1d1	definiční
měřická	měřický	k2eAgFnSc1d1	měřická
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4199">
diferenční	diferenční	k2eAgFnSc1d1	diferenční
měřická	měřický	k2eAgFnSc1d1	měřická
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4200">
dynamické	dynamický	k2eAgNnSc4d1	dynamické
měření	měření	k1gNnSc4	měření
</sent>
<sent id="4201">
indikační	indikační	k2eAgInSc1d1	indikační
měřicí	měřicí	k2eAgInSc1d1	měřicí
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="4202">
korekce	korekce	k1gFnSc2	korekce
</sent>
<sent id="4204">
korigovaný	korigovaný	k2eAgInSc4d1	korigovaný
výsledek	výsledek	k1gInSc4	výsledek
</sent>
<sent id="4205">
měřená	měřený	k2eAgFnSc1d1	měřená
veličina	veličina	k1gFnSc1	veličina
</sent>
<sent id="4206">
měřická	měřický	k2eAgFnSc1d1	měřická
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4232">
analogový	analogový	k2eAgInSc1d1	analogový
měřicí	měřicí	k2eAgInSc1d1	měřicí
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="4208">
měřická	měřický	k2eAgFnSc1d1	měřická
sestava	sestava	k1gFnSc1	sestava
</sent>
<sent id="4209">
náhodná	náhodný	k2eAgFnSc1d1	náhodná
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="4210">
nejistota	nejistota	k1gFnSc1	nejistota
výsledku	výsledek	k1gInSc2	výsledek
měření	měření	k1gNnSc4	měření
</sent>
<sent id="4211">
nekorigovaný	korigovaný	k2eNgInSc4d1	nekorigovaný
výsledek	výsledek	k1gInSc4	výsledek
</sent>
<sent id="4212">
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
měřická	měřický	k2eAgFnSc1d1	měřická
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4213">
nulová	nulový	k2eAgFnSc1d1	nulová
měřická	měřický	k2eAgFnSc1d1	měřická
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4214">
opakovatelnost	opakovatelnost	k1gFnSc1	opakovatelnost
měření	měření	k1gNnSc2	měření
</sent>
<sent id="4216">
postup	postup	k1gInSc1	postup
měření	měření	k1gNnSc2	měření
</sent>
<sent id="4217">
princip	princip	k1gInSc1	princip
měření	měření	k1gNnSc2	měření
</sent>
<sent id="4218">
přesnost	přesnost	k1gFnSc1	přesnost
měření	měření	k1gNnSc2	měření
</sent>
<sent id="4219">
přímá	přímý	k2eAgFnSc1d1	přímá
komparační	komparační	k2eAgFnSc1d1	komparační
měřická	měřický	k2eAgFnSc1d1	měřická
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4220">
přímá	přímý	k2eAgFnSc1d1	přímá
měřická	měřický	k2eAgFnSc1d1	měřická
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4221">
registrační	registrační	k2eAgInSc1d1	registrační
měřicí	měřicí	k2eAgInSc1d1	měřicí
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="4222">
relativní	relativní	k2eAgFnSc1d1	relativní
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="4223">
reprodukovatelnost	reprodukovatelnost	k1gFnSc1	reprodukovatelnost
měření	měření	k1gNnSc2	měření
</sent>
<sent id="4225">
substituční	substituční	k2eAgFnSc1d1	substituční
měřická	měřický	k2eAgFnSc1d1	měřická
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4226">
systematická	systematický	k2eAgFnSc1d1	systematická
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="4227">
transformovaná	transformovaný	k2eAgFnSc1d1	transformovaná
hodnota	hodnota	k1gFnSc1	hodnota
měřené	měřený	k2eAgFnSc2d1	měřená
veličiny	veličina	k1gFnSc2	veličina
</sent>
<sent id="4228">
údaj	údaj	k1gInSc1	údaj
měřidla	měřidlo	k1gNnSc2	měřidlo
</sent>
<sent id="4229">
výběrová	výběrový	k2eAgFnSc1d1	výběrová
směrodatná	směrodatný	k2eAgFnSc1d1	směrodatná
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="4231">
základní	základní	k2eAgFnSc1d1	základní
měřická	měřický	k2eAgFnSc1d1	měřická
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4233">
číslicový	číslicový	k2eAgInSc1d1	číslicový
měřicí	měřicí	k2eAgInSc1d1	měřicí
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="4234">
číslování	číslování	k1gNnSc6	číslování
stupnice	stupnice	k1gFnSc2	stupnice
</sent>
<sent id="4235">
délka	délka	k1gFnSc1	délka
dílku	dílek	k1gInSc2	dílek
</sent>
<sent id="4237">
DTM	DTM	kA	
</sent>
<sent id="4238">
LPIS	LPIS	kA	
</sent>
<sent id="4239">
OMP	OMP	kA	
</sent>
<sent id="4240">
RÚIAN	RÚIAN	kA	
</sent>
<sent id="4242">
délka	délka	k1gFnSc1	délka
stupnice	stupnice	k1gFnSc1	stupnice
</sent>
<sent id="4244">
dílek	dílek	k1gInSc1	dílek
stupnice	stupnice	k1gFnSc2	stupnice
</sent>
<sent id="4247">
Generální	generální	k2eAgFnSc1d1	generální
konference	konference	k1gFnSc1	konference
pro	pro	k7c4	
váhy	váha	k1gFnPc4	váha
a	a	k8xC	
míry	míra	k1gFnSc2	míra
</sent>
<sent id="4248">
historie	historie	k1gFnSc1	historie
měřicího	měřicí	k2eAgNnSc2d1	měřicí
zařízení	zařízení	k1gNnSc2	zařízení
</sent>
<sent id="4249">
hodnota	hodnota	k1gFnSc1	hodnota
dílku	dílek	k1gInSc2	dílek
</sent>
<sent id="4251">
chyba	chyba	k1gFnSc1	chyba
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4252">
jmenovitá	jmenovitý	k2eAgFnSc1d1	jmenovitá
hodnota	hodnota	k1gFnSc1	hodnota
</sent>
<sent id="4253">
kalibrační	kalibrační	k2eAgInSc4d1	kalibrační
interval	interval	k1gInSc4	interval
</sent>
<sent id="4254">
konstanta	konstanta	k1gFnSc1	konstanta
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4255">
lineární	lineární	k2eAgFnSc1d1	lineární
stupnice	stupnice	k1gFnSc1	stupnice
</sent>
<sent id="4256">
měřicí	měřice	k1gFnSc7	měřice
rozpětí	rozpětí	k1gNnSc2	rozpětí
</sent>
<sent id="4257">
metrická	metrický	k2eAgFnSc1d1	metrická
konvence	konvence	k1gFnSc1	konvence
</sent>
<sent id="4258">
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
etalon	etalon	k1gInSc4	etalon
</sent>
<sent id="4259">
mezní	mezní	k2eAgFnSc2d1	mezní
podmínky	podmínka	k1gFnSc2	podmínka
</sent>
<sent id="4260">
náhodná	náhodný	k2eAgFnSc1d1	náhodná
chyba	chyba	k1gFnSc1	chyba
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4261">
největší	veliký	k2eAgFnSc1d3	veliký
dovolená	dovolená	k1gFnSc1	dovolená
chyba	chyba	k1gFnSc1	chyba
(	(	kIx(	
<g/>
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
<g/>
)	)	kIx)	
</sent>
<sent id="4262">
nelineární	lineární	k2eNgFnSc1d1	nelineární
stupnice	stupnice	k1gFnSc1	stupnice
</sent>
<sent id="4263">
neutrálnost	neutrálnost	k1gFnSc1	neutrálnost
</sent>
<sent id="4264">
nula	nula	k1gFnSc1	nula
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4265">
opakovatelnost	opakovatelnost	k1gFnSc1	opakovatelnost
údajů	údaj	k1gInPc2	údaj
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4266">
měřicí	měřicí	k2eAgInSc1d1	měřicí
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="4625">
alhidáda	alhidáda	k1gFnSc1	alhidáda
</sent>
<sent id="4626">
analaktický	analaktický	k2eAgInSc4d1	analaktický
dalekohled	dalekohled	k1gInSc4	dalekohled
</sent>
<sent id="4268">
měřidlo	měřidlo	k1gNnSc4	měřidlo
</sent>
<sent id="4270">
chyba	chyba	k1gFnSc1	chyba
nuly	nula	k1gFnSc2	nula
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4271">
chyba	chyba	k1gFnSc1	chyba
v	v	k7c6	
kontrolním	kontrolní	k2eAgInSc6d1	kontrolní
bodě	bod	k1gInSc6	bod
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4272">
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
</sent>
<sent id="4273">
porovnávací	porovnávací	k2eAgInSc4d1	porovnávací
etalon	etalon	k1gInSc4	etalon
</sent>
<sent id="4274">
pracovní	pracovní	k2eAgInSc4d1	pracovní
etalon	etalon	k1gInSc4	etalon
</sent>
<sent id="4275">
práh	práh	k1gInSc1	práh
pohyblivosti	pohyblivost	k1gFnSc2	pohyblivost
</sent>
<sent id="4276">
primární	primární	k2eAgInSc4d1	primární
etalon	etalon	k1gInSc4	etalon
</sent>
<sent id="4277">
primární	primární	k2eAgFnSc1d1	primární
laboratoř	laboratoř	k1gFnSc1	laboratoř
</sent>
<sent id="4278">
primární	primární	k2eAgFnSc1d1	primární
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4279">
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
stupnice	stupnice	k1gFnSc1	stupnice
</sent>
<sent id="4280">
přenosný	přenosný	k2eAgInSc4d1	přenosný
etalon	etalon	k1gInSc4	etalon
</sent>
<sent id="4281">
přesnost	přesnost	k1gFnSc1	přesnost
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4282">
referenční	referenční	k2eAgInSc4d1	referenční
etalon	etalon	k1gInSc4	etalon
</sent>
<sent id="4283">
referenční	referenční	k2eAgFnPc4d1	referenční
podmínky	podmínka	k1gFnPc4	podmínka
</sent>
<sent id="4284">
registrační	registrační	k2eAgInSc4d1	registrační
médium	médium	k1gNnSc1	médium
</sent>
<sent id="4285">
registrační	registrační	k2eAgInSc4d1	registrační
zařízení	zařízení	k1gNnSc1	zařízení
</sent>
<sent id="4286">
rozsah	rozsah	k1gInSc1	rozsah
stupnice	stupnice	k1gFnSc2	stupnice
</sent>
<sent id="4287">
sada	sada	k1gFnSc1	sada
etalonů	etalon	k1gInPc2	etalon
</sent>
<sent id="4288">
sekundární	sekundární	k2eAgInSc4d1	sekundární
etalon	etalon	k1gInSc4	etalon
</sent>
<sent id="4289">
seřizování	seřizování	k1gNnSc6	seřizování
</sent>
<sent id="4290">
skupinový	skupinový	k2eAgInSc1d1	skupinový
etalon	etalon	k1gInSc1	etalon
</sent>
<sent id="4291">
snímač	snímač	k1gInSc1	snímač
</sent>
<sent id="4292">
součtový	součtový	k2eAgInSc1d1	součtový
měřicí	měřicí	k2eAgInSc1d1	měřicí
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="4293">
specifikované	specifikovaný	k2eAgFnSc2d1	specifikovaná
pracovní	pracovní	k2eAgFnSc2d1	pracovní
podmínky	podmínka	k1gFnSc2	podmínka
</sent>
<sent id="4294">
specifikovaný	specifikovaný	k2eAgInSc4d1	specifikovaný
měřicí	měřicí	k2eAgInSc4d1	měřicí
rozsah	rozsah	k1gInSc4	rozsah
</sent>
<sent id="4295">
správnost	správnost	k1gFnSc1	správnost
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4296">
stálost	stálost	k1gFnSc1	stálost
</sent>
<sent id="4297">
státní	státní	k2eAgInSc4d1	státní
etalon	etalon	k1gInSc4	etalon
</sent>
<sent id="4298">
stupnice	stupnice	k1gFnSc2	stupnice
</sent>
<sent id="4299">
stupnice	stupnice	k1gFnSc1	stupnice
s	s	k7c7	
potlačenou	potlačený	k2eAgFnSc7d1	potlačená
nulou	nula	k1gFnSc7	nula
</sent>
<sent id="4300">
systematická	systematický	k2eAgFnSc1d1	systematická
chyba	chyba	k1gFnSc1	chyba
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4301">
uchovávání	uchovávání	k1gNnSc4	uchovávání
etalonů	etalon	k1gInPc2	etalon
</sent>
<sent id="4302">
ukazovatel	ukazovatel	k1gInSc1	ukazovatel
</sent>
<sent id="4303">
základní	základní	k2eAgFnSc1d1	základní
chyba	chyba	k1gFnSc1	chyba
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4304">
značka	značka	k1gFnSc1	značka
stupnice	stupnice	k1gFnSc1	stupnice
</sent>
<sent id="4305">
indikační	indikační	k2eAgInSc4d1	indikační
zařízení	zařízení	k1gNnSc1	zařízení
</sent>
<sent id="4306">
číselník	číselník	k1gInSc1	číselník
</sent>
<sent id="4310">
ÚSES	ÚSES	kA	
</sent>
<sent id="4311">
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="4314">
planimetrování	planimetrování	k1gNnSc4	planimetrování
</sent>
<sent id="4315">
podílové	podílový	k2eAgNnSc1d1	podílové
spoluvlastnictví	spoluvlastnictví	k1gNnSc1	spoluvlastnictví
</sent>
<sent id="4316">
prohlášení	prohlášení	k1gNnSc6	prohlášení
konkurzu	konkurz	k1gInSc2	konkurz
proti	proti	k7c3	
vlastníku	vlastník	k1gMnSc3	vlastník
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="4317">
příslušnost	příslušnost	k1gFnSc1	příslušnost
k	k	k7c3	
organizační	organizační	k2eAgFnSc3d1	organizační
složce	složka	k1gFnSc3	složka
právnické	právnický	k2eAgFnSc2d1	právnická
osoby	osoba	k1gFnSc2	osoba
</sent>
<sent id="4318">
souhrnné	souhrnný	k2eAgInPc4d1	souhrnný
přehledy	přehled	k1gInPc4	přehled
o	o	k7c6	
půdním	půdní	k2eAgInSc6d1	půdní
fondu	fond	k1gInSc6	fond
z	z	k7c2	
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="4319">
soukromé	soukromý	k2eAgNnSc4d1	soukromé
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
fyzické	fyzický	k2eAgFnSc2d1	fyzická
osoby	osoba	k1gFnSc2	osoba
</sent>
<sent id="4321">
veřejný	veřejný	k2eAgInSc4d1	veřejný
statek	statek	k1gInSc4	statek
</sent>
<sent id="4322">
výpočet	výpočet	k1gInSc1	výpočet
výměr	výměr	k1gInSc4	výměr
</sent>
<sent id="4323">
výpočetní	výpočetní	k2eAgInSc4d1	výpočetní
protokol	protokol	k1gInSc4	protokol
</sent>
<sent id="4324">
výpůjčka	výpůjčka	k1gFnSc1	výpůjčka
</sent>
<sent id="4325">
vytyčení	vytyčení	k1gNnSc6	vytyčení
hranice	hranice	k1gFnSc2	hranice
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="4326">
vytyčovací	vytyčovací	k2eAgInSc1d1	vytyčovací
náčrt	náčrt	k1gInSc1	náčrt
</sent>
<sent id="4327">
záhumenek	záhumenek	k1gInSc1	záhumenek
</sent>
<sent id="4328">
základní	základní	k2eAgInSc4d1	základní
sektorový	sektorový	k2eAgInSc4d1	sektorový
přehled	přehled	k1gInSc4	přehled
o	o	k7c6	
plochách	plocha	k1gFnPc6	plocha
druhů	druh	k1gMnPc2	druh
pozemků	pozemek	k1gInPc2	pozemek
</sent>
<sent id="4329">
zápis	zápis	k1gInSc4	zápis
právních	právní	k2eAgInPc2d1	právní
vztahů	vztah	k1gInPc2	vztah
k	k	k7c3	
nemovitostem	nemovitost	k1gFnPc3	nemovitost
</sent>
<sent id="4331">
zástavní	zástavní	k2eAgInSc4d1	zástavní
právo	právo	k1gNnSc1	právo
</sent>
<sent id="4332">
závazné	závazný	k2eAgInPc1d1	závazný
údaje	údaj	k1gInPc1	údaj
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="4333">
závaznost	závaznost	k1gFnSc1	závaznost
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="4334">
záznam	záznam	k1gInSc1	záznam
nepovolených	povolený	k2eNgFnPc2d1	nepovolená
změn	změna	k1gFnPc2	změna
druhů	druh	k1gMnPc2	druh
pozemků	pozemek	k1gInPc2	pozemek
</sent>
<sent id="4336">
záznam	záznam	k1gInSc4	záznam
právních	právní	k2eAgInPc2d1	právní
vztahů	vztah	k1gInPc2	vztah
k	k	k7c3	
nemovitostem	nemovitost	k1gFnPc3	nemovitost
</sent>
<sent id="4337">
záznam	záznam	k1gInSc1	záznam
pro	pro	k7c4	
další	další	k2eAgNnSc4d1	další
řízení	řízení	k1gNnSc4	řízení
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="4338">
záznam	záznam	k1gInSc1	záznam
pro	pro	k7c4	
další	další	k2eAgNnSc4d1	další
řízení	řízení	k1gNnSc4	řízení
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="4339">
záznam	záznam	k1gInSc1	záznam
změn	změna	k1gFnPc2	změna
</sent>
<sent id="4340">
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
</sent>
<sent id="4341">
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
pozemek	pozemek	k1gInSc1	pozemek
</sent>
<sent id="4342">
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
půdní	půdní	k2eAgInSc1d1	půdní
fond	fond	k1gInSc1	fond
</sent>
<sent id="4343">
zjišťování	zjišťování	k1gNnSc2	zjišťování
průběhu	průběh	k1gInSc2	průběh
hranic	hranice	k1gFnPc2	hranice
</sent>
<sent id="4344">
změna	změna	k1gFnSc1	změna
druhu	druh	k1gInSc2	druh
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="4345">
změna	změna	k1gFnSc1	změna
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
</sent>
<sent id="4346">
způsob	způsob	k1gInSc1	způsob
ochrany	ochrana	k1gFnSc2	ochrana
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	
<g/>
bytu	byt	k1gInSc2	byt
nebo	nebo	k8xC	
nebytového	bytový	k2eNgInSc2d1	nebytový
prostoru	prostor	k1gInSc2	prostor
<g/>
)	)	kIx)	
</sent>
<sent id="4347">
způsob	způsob	k1gInSc1	způsob
ochrany	ochrana	k1gFnSc2	ochrana
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="4348">
železniční	železniční	k2eAgFnSc1d1	železniční
evidence	evidence	k1gFnSc1	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="4349">
GNSS	GNSS	kA	
</sent>
<sent id="4350">
technologie	technologie	k1gFnSc2	technologie
GNSS	GNSS	kA	
</sent>
<sent id="4351">
subregistry	subregistra	k1gFnSc2	subregistra
evidence	evidence	k1gFnSc2	evidence
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="4352">
výsledek	výsledek	k1gInSc1	výsledek
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
úpravy	úprava	k1gFnSc2	úprava
</sent>
<sent id="4353">
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
evidence	evidence	k1gFnSc1	evidence
</sent>
<sent id="4354">
způsob	způsob	k1gInSc1	způsob
užívání	užívání	k1gNnSc2	užívání
nemovitosti	nemovitost	k1gFnSc2	nemovitost
</sent>
<sent id="4355">
způsob	způsob	k1gInSc1	způsob
využití	využití	k1gNnSc2	využití
pozemku	pozemek	k1gInSc2	pozemek
</sent>
<sent id="4356">
způsob	způsob	k1gInSc1	způsob
využití	využití	k1gNnSc2	využití
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	
<g/>
bytu	byt	k1gInSc2	byt
nebo	nebo	k8xC	
nebytového	bytový	k2eNgInSc2d1	nebytový
prostoru	prostor	k1gInSc2	prostor
<g/>
)	)	kIx)	
</sent>
<sent id="4357">
způsob	způsob	k1gInSc1	způsob
využití	využití	k1gNnSc2	využití
budovy	budova	k1gFnSc2	budova
</sent>
<sent id="4358">
odchylka	odchylka	k1gFnSc1	odchylka
délky	délka	k1gFnSc2	délka
</sent>
<sent id="4359">
odchylka	odchylka	k1gFnSc1	odchylka
polohy	poloha	k1gFnSc2	poloha
bodu	bod	k1gInSc2	bod
</sent>
<sent id="4360">
odchylka	odchylka	k1gFnSc1	odchylka
povrchu	povrch	k1gInSc2	povrch
</sent>
<sent id="4361">
odchylka	odchylka	k1gFnSc1	odchylka
profilu	profil	k1gInSc2	profil
</sent>
<sent id="4362">
odchylka	odchylka	k1gFnSc1	odchylka
přímosti	přímost	k1gFnSc2	přímost
</sent>
<sent id="4363">
odchylka	odchylka	k1gFnSc1	odchylka
rovinnosti	rovinnost	k1gFnSc2	rovinnost
</sent>
<sent id="4364">
odchylka	odchylka	k1gFnSc1	odchylka
sklonu	sklon	k1gInSc3	sklon
</sent>
<sent id="4365">
odchylka	odchylka	k1gFnSc1	odchylka
styku	styk	k1gInSc2	styk
</sent>
<sent id="4366">
odchylka	odchylka	k1gFnSc1	odchylka
polohy	poloha	k1gFnSc2	poloha
přímky	přímka	k1gFnSc2	přímka
</sent>
<sent id="4367">
odchylka	odchylka	k1gFnSc1	odchylka
svislosti	svislost	k1gFnSc2	svislost
</sent>
<sent id="4368">
odchylka	odchylka	k1gFnSc1	odchylka
tvaru	tvar	k1gInSc2	tvar
</sent>
<sent id="4369">
odchylka	odchylka	k1gFnSc1	odchylka
úhlu	úhel	k1gInSc2	úhel
</sent>
<sent id="4370">
odchylka	odchylka	k1gFnSc1	odchylka
vodorovnosti	vodorovnost	k1gFnSc2	vodorovnost
</sent>
<sent id="4371">
základní	základní	k2eAgInSc4d1	základní
rozměr	rozměr	k1gInSc4	rozměr
</sent>
<sent id="4373">
státní	státní	k2eAgMnSc1d1	státní
mapové	mapový	k2eAgNnSc4d1	mapové
dílo	dílo	k1gNnSc4	dílo
</sent>
<sent id="4374">
ÚPD	ÚPD	kA	
</sent>
<sent id="4376">
nesouhlas	nesouhlas	k1gInSc1	nesouhlas
</sent>
<sent id="4377">
reprodukovatelnost	reprodukovatelnost	k1gFnSc1	reprodukovatelnost
</sent>
<sent id="4378">
rozpětí	rozpětí	k1gNnSc4	rozpětí
</sent>
<sent id="4379">
rozptýlení	rozptýlení	k1gNnPc2	rozptýlení
</sent>
<sent id="4380">
směrodatná	směrodatný	k2eAgFnSc1d1	směrodatná
polohová	polohový	k2eAgFnSc1d1	polohová
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="4381">
třída	třída	k1gFnSc1	třída
přesnosti	přesnost	k1gFnSc2	přesnost
</sent>
<sent id="4382">
úplná	úplný	k2eAgFnSc1d1	úplná
chyba	chyba	k1gFnSc1	chyba
měření	měření	k1gNnSc1	měření
</sent>
<sent id="4383">
váha	váha	k1gFnSc1	váha
měření	měření	k1gNnSc2	měření
</sent>
<sent id="4384">
vyrovnávací	vyrovnávací	k2eAgInSc4d1	vyrovnávací
počet	počet	k1gInSc4	počet
</sent>
<sent id="4385">
územně	územně	k6eAd1	
plánovací	plánovací	k2eAgFnSc2d1	plánovací
dokumentace	dokumentace	k1gFnSc2	dokumentace
</sent>
<sent id="4386">
zastavěné	zastavěný	k2eAgNnSc4d1	zastavěné
území	území	k1gNnSc4	území
</sent>
<sent id="4387">
zastavěný	zastavěný	k2eAgInSc4d1	zastavěný
stavební	stavební	k2eAgInSc4d1	stavební
pozemek	pozemek	k1gInSc4	pozemek
</sent>
<sent id="4388">
dopravní	dopravní	k2eAgFnSc1d1	dopravní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
</sent>
<sent id="4389">
občanské	občanský	k2eAgNnSc1d1	občanské
vybavení	vybavení	k1gNnSc1	vybavení
</sent>
<sent id="4390">
regulační	regulační	k2eAgInSc1d1	regulační
plán	plán	k1gInSc1	plán
</sent>
<sent id="4391">
technická	technický	k2eAgFnSc1d1	technická
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
</sent>
<sent id="4392">
územní	územní	k2eAgInSc4d1	územní
plán	plán	k1gInSc4	plán
</sent>
<sent id="4393">
veřejná	veřejný	k2eAgFnSc1d1	veřejná
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
</sent>
<sent id="4395">
zásady	zásada	k1gFnPc4	zásada
územního	územní	k2eAgInSc2d1	územní
rozvoje	rozvoj	k1gInSc2	rozvoj
</sent>
<sent id="4398">
hřbet	hřbet	k1gInSc1	hřbet
</sent>
<sent id="4399">
sedlo	sedlo	k1gNnSc4	sedlo
</sent>
<sent id="4400">
sklon	sklon	k1gInSc1	sklon
terénního	terénní	k2eAgInSc2d1	terénní
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="4401">
spádnice	spádnice	k1gFnSc1	spádnice
</sent>
<sent id="4402">
strž	strž	k1gFnSc1	strž
</sent>
<sent id="4403">
terén	terén	k1gInSc1	terén
</sent>
<sent id="4404">
terénní	terénní	k2eAgFnSc1d1	terénní
čára	čára	k1gFnSc1	čára
</sent>
<sent id="4405">
terénní	terénní	k2eAgFnSc1d1	terénní
hrana	hrana	k1gFnSc1	hrana
</sent>
<sent id="4406">
terénní	terénní	k2eAgFnSc1d1	terénní
kostra	kostra	k1gFnSc1	kostra
</sent>
<sent id="4407">
terénní	terénní	k2eAgInSc4d1	terénní
reliéf	reliéf	k1gInSc4	reliéf
</sent>
<sent id="4408">
terénní	terénní	k2eAgInSc4d1	terénní
stupeň	stupeň	k1gInSc4	stupeň
</sent>
<sent id="4409">
terénní	terénní	k2eAgInSc4d1	terénní
tvar	tvar	k1gInSc4	tvar
</sent>
<sent id="4410">
tvarová	tvarový	k2eAgFnSc1d1	tvarová
čára	čára	k1gFnSc1	čára
</sent>
<sent id="4411">
údolnice	údolnice	k1gFnSc1	údolnice
</sent>
<sent id="4412">
úpatnice	úpatnice	k1gFnSc1	úpatnice
</sent>
<sent id="4413">
vrchol	vrchol	k1gInSc1	vrchol
sedla	sedlo	k1gNnSc2	sedlo
</sent>
<sent id="4414">
výšková	výškový	k2eAgFnSc1d1	výšková
členitost	členitost	k1gFnSc1	členitost
terénního	terénní	k2eAgInSc2d1	terénní
reliéfu	reliéf	k1gInSc2	reliéf
(	(	kIx(	
<g/>
georeliéfu	georeliéf	k1gInSc2	georeliéf
<g/>
)	)	kIx)	
</sent>
<sent id="4416">
datum	datum	k1gNnSc4	datum
</sent>
<sent id="4459">
digitální	digitální	k2eAgInSc4d1	digitální
topografický	topografický	k2eAgInSc4d1	topografický
model	model	k1gInSc4	model
</sent>
<sent id="4458">
EuroDEM	EuroDEM	k1gFnSc1	EuroDEM
</sent>
<sent id="4457">
ERM	ERM	kA	
</sent>
<sent id="4456">
EGM	EGM	kA	
</sent>
<sent id="4421">
geodetické	geodetický	k2eAgFnSc2d1	geodetická
datum	datum	k1gInSc4	datum
</sent>
<sent id="4422">
geodetický	geodetický	k2eAgInSc1d1	geodetický
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
systém	systém	k1gInSc1	systém
</sent>
<sent id="4423">
hřeben	hřeben	k1gInSc1	hřeben
</sent>
<sent id="4424">
kartézský	kartézský	k2eAgInSc1d1	kartézský
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
systém	systém	k1gInSc1	systém
</sent>
<sent id="4425">
klesání	klesání	k1gNnSc2	klesání
terénního	terénní	k2eAgInSc2d1	terénní
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="4426">
konverze	konverze	k1gFnSc2	konverze
souřadnic	souřadnice	k1gFnPc2	souřadnice
</sent>
<sent id="4427">
kupa	kupa	k1gFnSc1	kupa
</sent>
<sent id="4428">
kužel	kužel	k1gInSc1	kužel
</sent>
<sent id="4429">
nepřehledný	přehledný	k2eNgInSc4d1	nepřehledný
terén	terén	k1gInSc4	terén
</sent>
<sent id="4430">
preciznost	preciznost	k1gFnSc1	preciznost
</sent>
<sent id="4431">
propadlina	propadlina	k1gFnSc1	propadlina
</sent>
<sent id="4432">
přehledný	přehledný	k2eAgInSc1d1	přehledný
terén	terén	k1gInSc1	terén
</sent>
<sent id="4433">
přesnost	přesnost	k1gFnSc1	přesnost
</sent>
<sent id="4434">
rokle	rokle	k1gFnSc1	rokle
</sent>
<sent id="4435">
rovina	rovina	k1gFnSc1	rovina
</sent>
<sent id="4436">
rovinný	rovinný	k2eAgInSc4d1	rovinný
terén	terén	k1gInSc4	terén
</sent>
<sent id="4437">
rozměr	rozměra	k1gFnPc2	rozměra
souřadnic	souřadnice	k1gFnPc2	souřadnice
</sent>
<sent id="4438">
sklonitý	sklonitý	k2eAgInSc4d1	sklonitý
terén	terén	k1gInSc4	terén
</sent>
<sent id="4439">
složený	složený	k2eAgInSc1d1	složený
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
</sent>
<sent id="4440">
sníženina	sníženina	k1gFnSc1	sníženina
</sent>
<sent id="4441">
souřadnice	souřadnice	k1gFnSc1	souřadnice
(	(	kIx(	
<g/>
sg.	sg.	k?	
<g/>
)	)	kIx)	
</sent>
<sent id="4442">
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
</sent>
<sent id="4443">
spád	spád	k1gInSc1	spád
terénního	terénní	k2eAgInSc2d1	terénní
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="4444">
sráz	sráz	k1gInSc1	sráz
</sent>
<sent id="4445">
stoupání	stoupání	k1gNnSc4	stoupání
terénního	terénní	k2eAgInSc2d1	terénní
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="4446">
svah	svah	k1gInSc1	svah
</sent>
<sent id="4447">
transformace	transformace	k1gFnSc2	transformace
souřadnic	souřadnice	k1gFnPc2	souřadnice
</sent>
<sent id="4448">
úbočí	úbočí	k1gNnSc4	úbočí
</sent>
<sent id="4449">
údolí	údolí	k1gNnSc6	údolí
</sent>
<sent id="4450">
úpatí	úpatí	k1gNnSc6	úpatí
</sent>
<sent id="4451">
vyvýšenina	vyvýšenina	k1gFnSc1	vyvýšenina
</sent>
<sent id="4452">
zarostlý	zarostlý	k2eAgInSc1d1	zarostlý
terén	terén	k1gInSc1	terén
</sent>
<sent id="4453">
zářez	zářez	k1gInSc1	zářez
</sent>
<sent id="4454">
zvlněný	zvlněný	k2eAgInSc4d1	zvlněný
terén	terén	k1gInSc4	terén
</sent>
<sent id="4455">
EBM	EBM	kA	
</sent>
<sent id="4460">
geodatabáze	geodatabáze	k1gFnSc1	geodatabáze
</sent>
<sent id="4462">
proudnice	proudnice	k1gFnSc1	proudnice
</sent>
<sent id="4463">
záplavová	záplavový	k2eAgFnSc1d1	záplavová
čára	čára	k1gFnSc1	čára
</sent>
<sent id="4464">
záplavové	záplavový	k2eAgNnSc4d1	záplavové
území	území	k1gNnSc4	území
</sent>
<sent id="4465">
CEN	cena	k1gFnPc2	cena
</sent>
<sent id="4466">
LIDAR	LIDAR	kA	
</sent>
<sent id="4468">
analýza	analýza	k1gFnSc1	analýza
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4469">
barometrická	barometrický	k2eAgFnSc1d1	barometrická
výška	výška	k1gFnSc1	výška
</sent>
<sent id="4470">
bod	bod	k1gInSc1	bod
základního	základní	k2eAgNnSc2d1	základní
polohového	polohový	k2eAgNnSc2d1	polohové
bodového	bodový	k2eAgNnSc2d1	bodové
pole	pole	k1gNnSc2	pole
</sent>
<sent id="4471">
bod	bod	k1gInSc1	bod
základního	základní	k2eAgNnSc2d1	základní
výškového	výškový	k2eAgNnSc2d1	výškové
bodového	bodový	k2eAgNnSc2d1	bodové
pole	pole	k1gNnSc2	pole
</sent>
<sent id="6675">
nepřímé	přímý	k2eNgNnSc4d1	nepřímé
připojení	připojení	k1gNnSc4	připojení
</sent>
<sent id="6676">
orientace	orientace	k1gFnSc1	orientace
pořadu	pořad	k1gInSc2	pořad
</sent>
<sent id="4474">
geodetické	geodetický	k2eAgFnSc2d1	geodetická
práce	práce	k1gFnSc2	práce
</sent>
<sent id="4475">
geodetické	geodetický	k2eAgNnSc4d1	geodetické
připojení	připojení	k1gNnSc4	připojení
</sent>
<sent id="4476">
geodetický	geodetický	k2eAgInSc4d1	geodetický
systém	systém	k1gInSc4	systém
</sent>
<sent id="4477">
geografické	geografický	k2eAgNnSc4d1	geografické
názvosloví	názvosloví	k1gNnSc4	názvosloví
</sent>
<sent id="4478">
geometrická	geometrický	k2eAgFnSc1d1	geometrická
mapová	mapový	k2eAgFnSc1d1	mapová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="4479">
grafická	grafický	k2eAgFnSc1d1	grafická
transformace	transformace	k1gFnSc1	transformace
</sent>
<sent id="4480">
grafické	grafický	k2eAgFnPc1d1	grafická
měřítko	měřítko	k1gNnSc4	měřítko
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5192">
hlavní	hlavní	k2eAgFnSc1d1	hlavní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="5193">
hlavní	hlavní	k2eAgMnSc1d1	hlavní
redaktor	redaktor	k1gMnSc1	redaktor
</sent>
<sent id="4482">
hraniční	hraniční	k2eAgInSc4d1	hraniční
bod	bod	k1gInSc4	bod
(	(	kIx(	
<g/>
na	na	k7c6	
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
<g/>
)	)	kIx)	
</sent>
<sent id="4483">
hraniční	hraniční	k2eAgFnSc1d1	hraniční
čára	čára	k1gFnSc1	čára
(	(	kIx(	
<g/>
na	na	k7c6	
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
<g/>
)	)	kIx)	
</sent>
<sent id="4484">
hraniční	hraniční	k2eAgInSc4d1	hraniční
polygonový	polygonový	k2eAgInSc4d1	polygonový
pořad	pořad	k1gInSc4	pořad
</sent>
<sent id="4485">
hřebová	hřebový	k2eAgFnSc1d1	hřebová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="4486">
mapové	mapový	k2eAgFnSc2d1	mapová
pole	pole	k1gFnSc2	pole
</sent>
<sent id="4487">
měřená	měřený	k2eAgFnSc1d1	měřená
osnova	osnova	k1gFnSc1	osnova
</sent>
<sent id="4488">
mapový	mapový	k2eAgInSc1d1	mapový
výřez	výřez	k1gInSc1	výřez
</sent>
<sent id="4489">
mimorámové	mimorámové	k2eAgInSc2d1	mimorámové
údaje	údaj	k1gInSc2	údaj
</sent>
<sent id="4490">
místní	místní	k2eAgInSc4d1	místní
měřítko	měřítko	k1gNnSc1	měřítko
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="4491">
neoficiální	oficiální	k2eNgInSc4d1,k2eAgInSc4d1	neoficiální
jméno	jméno	k1gNnSc1	jméno
</sent>
<sent id="4492">
nivelace	nivelace	k1gFnSc1	nivelace
</sent>
<sent id="4493">
niveleta	niveleta	k1gFnSc1	niveleta
</sent>
<sent id="4494">
nula	nula	k1gFnSc1	nula
mořského	mořský	k2eAgInSc2d1	mořský
vodočtu	vodočet	k1gInSc2	vodočet
</sent>
<sent id="4495">
obnova	obnova	k1gFnSc1	obnova
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4496">
obousměrná	obousměrný	k2eAgFnSc1d1	obousměrná
nivelace	nivelace	k1gFnSc1	nivelace
</sent>
<sent id="4497">
oboustranně	oboustranně	k6eAd1	
orietovaný	orietovaný	k2eAgInSc1d1	orietovaný
polygonový	polygonový	k2eAgInSc1d1	polygonový
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="4498">
obvodový	obvodový	k2eAgInSc1d1	obvodový
polygonový	polygonový	k2eAgInSc1d1	polygonový
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="4499">
oficiální	oficiální	k2eAgInSc4d1	oficiální
jméno	jméno	k1gNnSc1	jméno
</sent>
<sent id="4500">
ochranná	ochranný	k2eAgFnSc1d1	ochranná
tyč	tyč	k1gFnSc1	tyč
</sent>
<sent id="4501">
ochranný	ochranný	k2eAgInSc1d1	ochranný
znak	znak	k1gInSc1	znak
</sent>
<sent id="4502">
orientační	orientační	k2eAgInSc4d1	orientační
bod	bod	k1gInSc4	bod
</sent>
<sent id="4503">
podklad	podklad	k1gInSc1	podklad
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4504">
polygonová	polygonový	k2eAgFnSc1d1	polygonová
síť	síť	k1gFnSc1	síť
</sent>
<sent id="4505">
polygonová	polygonový	k2eAgFnSc1d1	polygonová
strana	strana	k1gFnSc1	strana
</sent>
<sent id="4506">
polygonový	polygonový	k2eAgInSc4d1	polygonový
pořad	pořad	k1gInSc4	pořad
</sent>
<sent id="4507">
pomocný	pomocný	k2eAgInSc1d1	pomocný
(	(	kIx(	
<g/>
měřický	měřický	k2eAgInSc1d1	měřický
<g/>
)	)	kIx)	
náčrt	náčrt	k1gInSc1	náčrt
</sent>
<sent id="4508">
popis	popis	k1gInSc1	popis
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4509">
porovnání	porovnání	k1gNnSc6	porovnání
styku	styk	k1gInSc2	styk
(	(	kIx(	
<g/>
mapových	mapový	k2eAgInPc2d1	mapový
listů	list	k1gInPc2	list
<g/>
)	)	kIx)	
</sent>
<sent id="4510">
poříční	poříční	k2eAgInSc1d1	poříční
polygonový	polygonový	k2eAgInSc1d1	polygonový
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="4511">
povrchová	povrchový	k2eAgFnSc1d1	povrchová
měřická	měřický	k2eAgFnSc1d1	měřická
značka	značka	k1gFnSc1	značka
</sent>
<sent id="4512">
pozorovací	pozorovací	k2eAgFnSc1d1	pozorovací
rovina	rovina	k1gFnSc1	rovina
</sent>
<sent id="4513">
pravoúhlé	pravoúhlý	k2eAgFnSc2d1	pravoúhlá
rovinné	rovinný	k2eAgFnSc2d1	rovinná
souřadnice	souřadnice	k1gFnSc2	souřadnice
</sent>
<sent id="4514">
průsvitka	průsvitka	k1gFnSc1	průsvitka
</sent>
<sent id="4515">
liniová	liniový	k2eAgFnSc1d1	liniová
stavba	stavba	k1gFnSc1	stavba
</sent>
<sent id="4516">
měřítková	měřítkový	k2eAgFnSc1d1	měřítková
řada	řada	k1gFnSc1	řada
</sent>
<sent id="4517">
okrajový	okrajový	k2eAgInSc4d1	okrajový
náčrtek	náčrtek	k1gInSc4	náčrtek
</sent>
<sent id="4518">
polohové	polohový	k2eAgNnSc4d1	polohové
připojení	připojení	k1gNnSc4	připojení
</sent>
<sent id="4519">
pomocný	pomocný	k2eAgInSc1d1	pomocný
měřický	měřický	k2eAgInSc1d1	měřický
bod	bod	k1gInSc1	bod
</sent>
<sent id="4520">
posunutý	posunutý	k2eAgInSc4d1	posunutý
klad	klad	k1gInSc4	klad
mapových	mapový	k2eAgInPc2d1	mapový
listů	list	k1gInPc2	list
</sent>
<sent id="4521">
pracovní	pracovní	k2eAgInSc4d1	pracovní
měřítko	měřítko	k1gNnSc1	měřítko
</sent>
<sent id="4522">
předmět	předmět	k1gInSc1	předmět
měření	měření	k1gNnSc2	měření
</sent>
<sent id="4523">
předmět	předmět	k1gInSc1	předmět
šetření	šetření	k1gNnSc2	šetření
</sent>
<sent id="4524">
přechodné	přechodný	k2eAgNnSc4d1	přechodné
stanovisko	stanovisko	k1gNnSc4	stanovisko
</sent>
<sent id="4525">
přepracování	přepracování	k1gNnSc4	přepracování
původní	původní	k2eAgFnSc2d1	původní
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4526">
přesnost	přesnost	k1gFnSc1	přesnost
analogové	analogový	k2eAgFnSc2d1	analogová
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4527">
přesnost	přesnost	k1gFnSc1	přesnost
mapování	mapování	k1gNnSc2	mapování
</sent>
<sent id="4528">
příložný	příložný	k2eAgInSc1d1	příložný
(	(	kIx(	
<g/>
měřický	měřický	k2eAgInSc1d1	měřický
<g/>
)	)	kIx)	
náčrt	náčrt	k1gInSc1	náčrt
</sent>
<sent id="4529">
připojovací	připojovací	k2eAgInSc4d1	připojovací
měření	měření	k1gNnSc1	měření
</sent>
<sent id="4530">
připojovací	připojovací	k2eAgInSc4d1	připojovací
směr	směr	k1gInSc4	směr
</sent>
<sent id="4531">
rajón	rajón	k1gInSc1	rajón
</sent>
<sent id="4532">
údaje	údaj	k1gInSc2	údaj
v	v	k7c6	
mapovém	mapový	k2eAgInSc6d1	mapový
rámu	rám	k1gInSc6	rám
</sent>
<sent id="4533">
rámový	rámový	k2eAgInSc1d1	rámový
náčrt	náčrt	k1gInSc1	náčrt
</sent>
<sent id="4534">
rekognoskace	rekognoskace	k1gFnSc1	rekognoskace
terénu	terén	k1gInSc2	terén
</sent>
<sent id="4535">
relativní	relativní	k2eAgFnSc1d1	relativní
(	(	kIx(	
<g/>
výšková	výškový	k2eAgFnSc1d1	výšková
<g/>
)	)	kIx)	
kóta	kóta	k1gFnSc1	kóta
</sent>
<sent id="4536">
relativní	relativní	k2eAgFnSc1d1	relativní
výška	výška	k1gFnSc1	výška
</sent>
<sent id="4537">
rozestup	rozestup	k1gInSc1	rozestup
vrstevnic	vrstevnice	k1gFnPc2	vrstevnice
</sent>
<sent id="4539">
řešení	řešení	k1gNnPc2	řešení
vrstevnic	vrstevnice	k1gFnPc2	vrstevnice
</sent>
<sent id="4540">
sesuv	sesuv	k1gInSc1	sesuv
</sent>
<sent id="4541">
seznam	seznam	k1gInSc1	seznam
mapových	mapový	k2eAgFnPc2d1	mapová
značek	značka	k1gFnPc2	značka
</sent>
<sent id="4543">
skutečný	skutečný	k2eAgInSc1d1	skutečný
rozměr	rozměr	k1gInSc1	rozměr
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4544">
spádové	spádový	k2eAgFnSc2d1	spádová
šrafy	šrafa	k1gFnSc2	šrafa
</sent>
<sent id="4545">
spádovka	spádovka	k1gFnSc1	spádovka
</sent>
<sent id="4546">
stabilizace	stabilizace	k1gFnSc2	stabilizace
</sent>
<sent id="6106">
subtraktivní	subtraktivní	k2eAgInSc4d1	subtraktivní
míšení	míšení	k1gNnSc1	míšení
barev	barva	k1gFnPc2	barva
</sent>
<sent id="6108">
tečková	tečkový	k2eAgFnSc1d1	tečková
(	(	kIx(	
<g/>
bodová	bodový	k2eAgFnSc1d1	bodová
<g/>
)	)	kIx)	
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="6109">
tisková	tiskový	k2eAgFnSc1d1	tisková
barva	barva	k1gFnSc1	barva
</sent>
<sent id="4548">
standardizované	standardizovaný	k2eAgNnSc4d1	standardizované
geografické	geografický	k2eAgNnSc4d1	geografické
jméno	jméno	k1gNnSc4	jméno
</sent>
<sent id="4549">
stanovený	stanovený	k2eAgInSc4d1	stanovený
rozměr	rozměr	k1gInSc4	rozměr
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4550">
stanovisko	stanovisko	k1gNnSc4	stanovisko
</sent>
<sent id="4551">
střešní	střešní	k2eAgInSc4d1	střešní
přesah	přesah	k1gInSc4	přesah
</sent>
<sent id="4552">
šetření	šetření	k1gNnSc4	šetření
</sent>
<sent id="4553">
topografická	topografický	k2eAgFnSc1d1	topografická
plocha	plocha	k1gFnSc1	plocha
</sent>
<sent id="4554">
topografické	topografický	k2eAgFnSc2d1	topografická
šrafy	šrafa	k1gFnSc2	šrafa
</sent>
<sent id="4555">
topografie	topografie	k1gFnSc1	topografie
</sent>
<sent id="4556">
trvalá	trvalý	k2eAgFnSc1d1	trvalá
signalizace	signalizace	k1gFnSc1	signalizace
</sent>
<sent id="4557">
ukázkový	ukázkový	k2eAgInSc1d1	ukázkový
mapový	mapový	k2eAgInSc1d1	mapový
list	list	k1gInSc1	list
</sent>
<sent id="4558">
určovací	určovací	k2eAgFnSc1d1	určovací
délka	délka	k1gFnSc1	délka
</sent>
<sent id="5091">
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
polygonový	polygonový	k2eAgInSc1d1	polygonový
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="4560">
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
polygonový	polygonový	k2eAgInSc1d1	polygonový
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="4561">
vnější	vnější	k2eAgFnSc1d1	vnější
rámová	rámový	k2eAgFnSc1d1	rámová
čára	čára	k1gFnSc1	čára
</sent>
<sent id="4562">
vrcholová	vrcholový	k2eAgFnSc1d1	vrcholová
(	(	kIx(	
<g/>
výšková	výškový	k2eAgFnSc1d1	výšková
<g/>
)	)	kIx)	
kóta	kóta	k1gFnSc1	kóta
</sent>
<sent id="4563">
vrcholový	vrcholový	k2eAgInSc4d1	vrcholový
úhel	úhel	k1gInSc4	úhel
polygonového	polygonový	k2eAgInSc2d1	polygonový
pořadu	pořad	k1gInSc2	pořad
</sent>
<sent id="4564">
výchozí	výchozí	k2eAgFnSc1d1	výchozí
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4565">
vyrovnání	vyrovnání	k1gNnSc6	vyrovnání
styku	styk	k1gInSc2	styk
(	(	kIx(	
<g/>
mapových	mapový	k2eAgInPc2d1	mapový
listů	list	k1gInPc2	list
<g/>
)	)	kIx)	
</sent>
<sent id="4566">
výšková	výškový	k2eAgFnSc1d1	výšková
kóta	kóta	k1gFnSc1	kóta
</sent>
<sent id="4567">
výšková	výškový	k2eAgFnSc1d1	výšková
úroveň	úroveň	k1gFnSc1	úroveň
</sent>
<sent id="4568">
výškové	výškový	k2eAgNnSc4d1	výškové
připojení	připojení	k1gNnSc4	připojení
</sent>
<sent id="4569">
vzorový	vzorový	k2eAgInSc4d1	vzorový
mapový	mapový	k2eAgInSc4d1	mapový
list	list	k1gInSc4	list
</sent>
<sent id="4570">
vžité	vžitý	k2eAgNnSc4d1	vžité
geografické	geografický	k2eAgNnSc4d1	geografické
jméno	jméno	k1gNnSc4	jméno
</sent>
<sent id="4571">
základní	základní	k2eAgNnSc4d1	základní
měřítko	měřítko	k1gNnSc4	měřítko
(	(	kIx(	
<g/>
mapového	mapový	k2eAgNnSc2d1	mapové
díla	dílo	k1gNnSc2	dílo
<g/>
)	)	kIx)	
</sent>
<sent id="4572">
základní	základní	k2eAgInSc4d1	základní
vrstevnicový	vrstevnicový	k2eAgInSc4d1	vrstevnicový
interval	interval	k1gInSc4	interval
</sent>
<sent id="4573">
zaměřování	zaměřování	k1gNnPc2	zaměřování
změn	změna	k1gFnPc2	změna
</sent>
<sent id="5627">
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
horizont	horizont	k1gInSc1	horizont
</sent>
<sent id="4575">
zdůrazněná	zdůrazněný	k2eAgFnSc1d1	zdůrazněná
vrstevnice	vrstevnice	k1gFnSc1	vrstevnice
</sent>
<sent id="4576">
zenitový	zenitový	k2eAgInSc4d1	zenitový
úhel	úhel	k1gInSc4	úhel
</sent>
<sent id="4577">
zobrazený	zobrazený	k2eAgInSc4d1	zobrazený
bod	bod	k1gInSc4	bod
</sent>
<sent id="4578">
zobrazování	zobrazování	k1gNnSc4	zobrazování
</sent>
<sent id="4579">
měření	měření	k1gNnSc2	měření
podzemních	podzemní	k2eAgInPc2d1	podzemní
prostorů	prostor	k1gInPc2	prostor
</sent>
<sent id="4580">
měřická	měřický	k2eAgFnSc1d1	měřická
lať	lať	k1gFnSc1	lať
</sent>
<sent id="4583">
observace	observace	k1gFnSc1	observace
</sent>
<sent id="4584">
olovnice	olovnice	k1gFnSc1	olovnice
</sent>
<sent id="4585">
paralaktická	paralaktický	k2eAgFnSc1d1	paralaktická
lať	lať	k1gFnSc1	lať
</sent>
<sent id="4586">
podélný	podélný	k2eAgInSc4d1	podélný
profil	profil	k1gInSc4	profil
</sent>
<sent id="4587">
převýšení	převýšení	k1gNnSc4	převýšení
</sent>
<sent id="4588">
příčný	příčný	k2eAgInSc4d1	příčný
profil	profil	k1gInSc4	profil
</sent>
<sent id="4589">
tachymetrická	tachymetrický	k2eAgFnSc1d1	tachymetrická
lať	lať	k1gFnSc1	lať
</sent>
<sent id="4590">
tachymetrie	tachymetrie	k1gFnSc1	tachymetrie
</sent>
<sent id="4591">
topografické	topografický	k2eAgNnSc4d1	topografické
mapování	mapování	k1gNnSc4	mapování
</sent>
<sent id="4592">
výtyčka	výtyčka	k1gFnSc1	výtyčka
</sent>
<sent id="4593">
zenit	zenit	k1gInSc1	zenit
</sent>
<sent id="4594">
HB	HB	kA	
</sent>
<sent id="4595">
HVB	HVB	kA	
</sent>
<sent id="4596">
CHB	CHB	kA	
</sent>
<sent id="4597">
DIGEST	DIGEST	kA	
</sent>
<sent id="4600">
ICRS	ICRS	kA	
</sent>
<sent id="4601">
OGC	OGC	kA	
</sent>
<sent id="4602">
SQL	SQL	kA	
</sent>
<sent id="4603">
WMS	WMS	kA	
</sent>
<sent id="4605">
busolní	busolní	k2eAgInSc4d1	busolní
měření	měření	k1gNnSc1	měření
</sent>
<sent id="4606">
doplňovací	doplňovací	k2eAgInSc4d1	doplňovací
mapování	mapování	k1gNnSc1	mapování
</sent>
<sent id="4607">
důlní	důlní	k2eAgInSc4d1	důlní
mapování	mapování	k1gNnSc1	mapování
</sent>
<sent id="5997">
inerciální	inerciální	k2eAgFnSc1d1	inerciální
měřicí	měřicí	k2eAgFnSc1d1	měřicí
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="4609">
geodetické	geodetický	k2eAgNnSc4d1	geodetické
mapování	mapování	k1gNnSc4	mapování
</sent>
<sent id="4610">
geodetické	geodetický	k2eAgNnSc4d1	geodetické
měření	měření	k1gNnSc4	měření
</sent>
<sent id="4611">
geodetické	geodetický	k2eAgFnSc2d1	geodetická
práce	práce	k1gFnSc2	práce
v	v	k7c6	
terénu	terén	k1gInSc6	terén
</sent>
<sent id="4612">
magnetické	magnetický	k2eAgNnSc1d1	magnetické
měření	měření	k1gNnPc1	měření
v	v	k7c6	
důlním	důlní	k2eAgNnSc6d1	důlní
měřictví	měřictví	k1gNnSc6	měřictví
</sent>
<sent id="4613">
lesnické	lesnický	k2eAgNnSc4d1	lesnické
měření	měření	k1gNnSc4	měření
</sent>
<sent id="4614">
měřič	měřič	k1gInSc1	měřič
</sent>
<sent id="4615">
polohopisné	polohopisný	k2eAgNnSc4d1	polohopisné
mapování	mapování	k1gNnSc4	mapování
</sent>
<sent id="4616">
původní	původní	k2eAgInSc4d1	původní
měření	měření	k1gNnSc1	měření
</sent>
<sent id="4617">
reambulační	reambulační	k2eAgFnSc1d1	reambulační
práce	práce	k1gFnSc1	práce
</sent>
<sent id="4618">
souvislé	souvislý	k2eAgNnSc4d1	souvislé
mapování	mapování	k1gNnSc4	mapování
</sent>
<sent id="4619">
technickohospodářské	technickohospodářský	k2eAgNnSc4d1	technickohospodářský
mapování	mapování	k1gNnSc4	mapování
</sent>
<sent id="4620">
tematické	tematický	k2eAgNnSc4d1	tematické
mapování	mapování	k1gNnSc4	mapování
</sent>
<sent id="4622">
úřední	úřední	k2eAgInSc4d1	úřední
oprávnění	oprávnění	k1gNnSc1	oprávnění
</sent>
<sent id="4623">
výškopisné	výškopisný	k2eAgNnSc4d1	výškopisné
mapování	mapování	k1gNnSc4	mapování
</sent>
<sent id="4624">
základní	základní	k2eAgInSc4d1	základní
mapování	mapování	k1gNnSc1	mapování
</sent>
<sent id="4627">
astronomický	astronomický	k2eAgInSc4d1	astronomický
dalekohled	dalekohled	k1gInSc4	dalekohled
</sent>
<sent id="4628">
autokolimátor	autokolimátor	k1gInSc1	autokolimátor
</sent>
<sent id="4629">
busola	busola	k1gFnSc1	busola
</sent>
<sent id="4630">
busolní	busolní	k2eAgInSc4d1	busolní
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="4632">
dalekohled	dalekohled	k1gInSc1	dalekohled
</sent>
<sent id="7283">
BIPM	BIPM	kA	
</sent>
<sent id="4635">
elevační	elevační	k2eAgInSc4d1	elevační
šroub	šroub	k1gInSc4	šroub
</sent>
<sent id="4636">
geografická	geografický	k2eAgFnSc1d1	geografická
busola	busola	k1gFnSc1	busola
</sent>
<sent id="4637">
gyrokompas	gyrokompas	k1gInSc1	gyrokompas
</sent>
<sent id="4638">
gyroskop	gyroskop	k1gInSc1	gyroskop
</sent>
<sent id="4639">
gyroteodolit	gyroteodolit	k1gInSc1	gyroteodolit
</sent>
<sent id="4640">
jednoosý	jednoosý	k2eAgInSc4d1	jednoosý
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="4641">
kódový	kódový	k2eAgInSc4d1	kódový
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="4642">
kolimátor	kolimátor	k1gInSc1	kolimátor
</sent>
<sent id="4643">
kompas	kompas	k1gInSc1	kompas
</sent>
<sent id="4644">
kompenzátor	kompenzátor	k1gInSc1	kompenzátor
</sent>
<sent id="4645">
libela	libela	k1gFnSc1	libela
</sent>
<sent id="4647">
mikrometrický	mikrometrický	k2eAgInSc4d1	mikrometrický
šroub	šroub	k1gInSc4	šroub
</sent>
<sent id="4649">
minutový	minutový	k2eAgInSc1d1	minutový
teodolit	teodolit	k1gInSc1	teodolit
</sent>
<sent id="4650">
objektiv	objektiv	k1gInSc1	objektiv
</sent>
<sent id="4651">
okulár	okulár	k1gInSc1	okulár
</sent>
<sent id="4652">
osa	osa	k1gFnSc1	osa
alhidády	alhidáda	k1gFnSc2	alhidáda
</sent>
<sent id="4653">
registrační	registrační	k2eAgInSc4d1	registrační
tachymetr	tachymetr	k1gInSc4	tachymetr
</sent>
<sent id="4654">
reiterační	reiterační	k2eAgInSc4d1	reiterační
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="4655">
repetiční	repetiční	k2eAgInSc4d1	repetiční
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="4656">
sklonoměr	sklonoměr	k1gInSc1	sklonoměr
</sent>
<sent id="4657">
stavěcí	stavěcí	k2eAgInSc4d1	stavěcí
šroub	šroub	k1gInSc4	šroub
</sent>
<sent id="4658">
stolová	stolový	k2eAgFnSc1d1	stolová
busola	busola	k1gFnSc1	busola
</sent>
<sent id="4659">
svislý	svislý	k2eAgInSc1d1	svislý
kruh	kruh	k1gInSc1	kruh
</sent>
<sent id="4660">
teodolit	teodolit	k1gInSc1	teodolit
</sent>
<sent id="4661">
teodolit	teodolit	k1gInSc1	teodolit
s	s	k7c7	
automatickým	automatický	k2eAgNnSc7d1	automatické
urovnáváním	urovnávání	k1gNnSc7	urovnávání
indexu	index	k1gInSc2	index
</sent>
<sent id="4662">
univerzální	univerzální	k2eAgInSc4d1	univerzální
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="4663">
vernier	vernier	k1gInSc1	vernier
</sent>
<sent id="4664">
vernierový	vernierový	k2eAgInSc4d1	vernierový
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="4665">
vodorovný	vodorovný	k2eAgInSc1d1	vodorovný
kruh	kruh	k1gInSc1	kruh
</sent>
<sent id="4666">
vteřinový	vteřinový	k2eAgInSc4d1	vteřinový
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="4667">
závěsný	závěsný	k2eAgInSc4d1	závěsný
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="4668">
autoredukční	autoredukční	k2eAgInSc4d1	autoredukční
tachymetr	tachymetr	k1gInSc4	tachymetr
</sent>
<sent id="4669">
elektronický	elektronický	k2eAgInSc4d1	elektronický
dálkoměr	dálkoměr	k1gInSc4	dálkoměr
</sent>
<sent id="4670">
elektronický	elektronický	k2eAgInSc4d1	elektronický
tachymetr	tachymetr	k1gInSc4	tachymetr
</sent>
<sent id="4671">
měřický	měřický	k2eAgInSc4d1	měřický
stůl	stůl	k1gInSc4	stůl
</sent>
<sent id="5062">
orientační	orientační	k2eAgInSc4d1	orientační
směr	směr	k1gInSc4	směr
</sent>
<sent id="4673">
nivelační	nivelační	k2eAgInSc1d1	nivelační
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="5007">
přímé	přímý	k2eAgNnSc4d1	přímé
měření	měření	k1gNnSc4	měření
</sent>
<sent id="7301">
webová	webový	k2eAgFnSc1d1	webová
mapová	mapový	k2eAgFnSc1d1	mapová
aplikace	aplikace	k1gFnSc1	aplikace
</sent>
<sent id="7302">
webový	webový	k2eAgInSc1d1	webový
mapový	mapový	k2eAgInSc1d1	mapový
portál	portál	k1gInSc1	portál
</sent>
<sent id="4676">
pentagon	pentagon	k1gInSc1	pentagon
</sent>
<sent id="4677">
rádiový	rádiový	k2eAgInSc4d1	rádiový
dálkoměr	dálkoměr	k1gInSc4	dálkoměr
</sent>
<sent id="4678">
stativ	stativ	k1gInSc1	stativ
</sent>
<sent id="4679">
světelný	světelný	k2eAgInSc4d1	světelný
dálkoměr	dálkoměr	k1gInSc4	dálkoměr
</sent>
<sent id="4680">
tachymetr	tachymetr	k1gInSc1	tachymetr
</sent>
<sent id="4681">
horizontace	horizontace	k1gFnSc1	horizontace
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="5172">
záměrné	záměrná	k1gFnSc6	záměrná
pravítko	pravítko	k1gNnSc1	pravítko
</sent>
<sent id="4683">
vytyčovací	vytyčovací	k2eAgInSc4d1	vytyčovací
zrcátko	zrcátko	k1gNnSc1	zrcátko
</sent>
<sent id="4684">
vytyčování	vytyčování	k1gNnSc4	vytyčování
</sent>
<sent id="4685">
dálkoměr	dálkoměr	k1gInSc1	dálkoměr
</sent>
<sent id="4687">
nepřímé	přímý	k2eNgNnSc1d1	nepřímé
měření	měření	k1gNnSc1	měření
délek	délka	k1gFnPc2	délka
</sent>
<sent id="4688">
paralaktické	paralaktický	k2eAgNnSc1d1	paralaktické
měření	měření	k1gNnSc1	měření
délek	délka	k1gFnPc2	délka
</sent>
<sent id="4689">
přímé	přímý	k2eAgNnSc1d1	přímé
měření	měření	k1gNnSc1	měření
délek	délka	k1gFnPc2	délka
</sent>
<sent id="4690">
sjednocené	sjednocený	k2eAgInPc4d1	sjednocený
katastrální	katastrální	k2eAgInPc4d1	katastrální
operáty	operát	k1gInPc4	operát
</sent>
<sent id="4691">
citlivost	citlivost	k1gFnSc1	citlivost
</sent>
<sent id="4692">
dynamická	dynamický	k2eAgFnSc1d1	dynamická
chyba	chyba	k1gFnSc1	chyba
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4693">
graduování	graduování	k1gNnSc2	graduování
měřicího	měřicí	k2eAgInSc2d1	měřicí
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="4694">
hystereze	hystereze	k1gFnSc1	hystereze
</sent>
<sent id="4695">
integrační	integrační	k2eAgInSc1d1	integrační
měřicí	měřicí	k2eAgInSc1d1	měřicí
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="4696">
jmenovitý	jmenovitý	k2eAgInSc4d1	jmenovitý
rozsah	rozsah	k1gInSc4	rozsah
stupnice	stupnice	k1gFnSc2	stupnice
</sent>
<sent id="4698">
rozlišitelnost	rozlišitelnost	k1gFnSc1	rozlišitelnost
indikačního	indikační	k2eAgNnSc2d1	indikační
měřidla	měřidlo	k1gNnSc2	měřidlo
</sent>
<sent id="4699">
třída	třída	k1gFnSc1	třída
přesnosti	přesnost	k1gFnSc2	přesnost
měřicích	měřicí	k2eAgInPc2d1	měřicí
přístrojů	přístroj	k1gInPc2	přístroj
</sent>
<sent id="4700">
automatizovaný	automatizovaný	k2eAgInSc4d1	automatizovaný
popis	popis	k1gInSc4	popis
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4701">
číselná	číselný	k2eAgFnSc1d1	číselná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4702">
číslo	číslo	k1gNnSc1	číslo
objektu	objekt	k1gInSc2	objekt
</sent>
<sent id="4703">
dílčí	dílčí	k2eAgInSc1d1	dílčí
mapový	mapový	k2eAgInSc1d1	mapový
list	list	k1gInSc1	list
</sent>
<sent id="4705">
formát	formát	k1gInSc1	formát
mapového	mapový	k2eAgInSc2d1	mapový
listu	list	k1gInSc2	list
</sent>
<sent id="4706">
hlavní	hlavní	k2eAgInSc4d1	hlavní
měřítko	měřítko	k1gNnSc1	měřítko
</sent>
<sent id="4839">
chorografická	chorografický	k2eAgFnSc1d1	chorografický
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4708">
mapa	mapa	k1gFnSc1	mapa
terénního	terénní	k2eAgInSc2d1	terénní
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="4709">
mapa	mapa	k1gFnSc1	mapa
sklonitosti	sklonitost	k1gFnSc2	sklonitost
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="4710">
mapové	mapový	k2eAgNnSc4d1	mapové
dílo	dílo	k1gNnSc4	dílo
</sent>
<sent id="4711">
měřítko	měřítko	k1gNnSc4	měřítko
mapování	mapování	k1gNnSc2	mapování
</sent>
<sent id="4712">
náhradní	náhradní	k2eAgFnSc1d1	náhradní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4713">
neúplný	úplný	k2eNgInSc1d1	neúplný
mapový	mapový	k2eAgInSc1d1	mapový
list	list	k1gInSc1	list
</sent>
<sent id="4714">
obsah	obsah	k1gInSc1	obsah
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4715">
okrajový	okrajový	k2eAgInSc4d1	okrajový
list	list	k1gInSc4	list
</sent>
<sent id="6295">
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4717">
podrobná	podrobný	k2eAgFnSc1d1	podrobná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4718">
polohopisná	polohopisný	k2eAgFnSc1d1	polohopisná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4719">
relativní	relativní	k2eAgFnSc1d1	relativní
hloubka	hloubka	k1gFnSc1	hloubka
</sent>
<sent id="4720">
reliéfní	reliéfní	k2eAgFnSc1d1	reliéfní
čára	čára	k1gFnSc1	čára
</sent>
<sent id="4721">
sklonové	sklonový	k2eAgNnSc4d1	sklonový
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="4722">
skutečné	skutečný	k2eAgNnSc4d1	skutečné
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="4723">
stykový	stykový	k2eAgInSc1d1	stykový
list	list	k1gInSc1	list
</sent>
<sent id="4724">
teoretické	teoretický	k2eAgNnSc4d1	teoretické
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="4726">
úplný	úplný	k2eAgInSc1d1	úplný
list	list	k1gInSc1	list
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4727">
vodstvo	vodstvo	k1gNnSc4	vodstvo
</sent>
<sent id="4728">
vojenská	vojenský	k2eAgFnSc1d1	vojenská
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4729">
vrstevnicová	vrstevnicový	k2eAgFnSc1d1	vrstevnicová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4730">
výchozí	výchozí	k2eAgInSc4d1	výchozí
měřítko	měřítko	k1gNnSc1	měřítko
</sent>
<sent id="4731">
výškopisná	výškopisný	k2eAgFnSc1d1	výškopisná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4732">
Základní	základní	k2eAgFnSc1d1	základní
mapa	mapa	k1gFnSc1	mapa
dálnice	dálnice	k1gFnSc2	dálnice
</sent>
<sent id="4733">
Základní	základní	k2eAgFnSc1d1	základní
mapa	mapa	k1gFnSc1	mapa
depa	depo	k1gNnSc2	depo
metra	metro	k1gNnSc2	metro
</sent>
<sent id="4734">
Základní	základní	k2eAgFnSc1d1	základní
mapa	mapa	k1gFnSc1	mapa
letiště	letiště	k1gNnSc2	letiště
</sent>
<sent id="4735">
Základní	základní	k2eAgFnSc1d1	základní
mapa	mapa	k1gFnSc1	mapa
lomu	lom	k1gInSc2	lom
</sent>
<sent id="4736">
Základní	základní	k2eAgFnSc1d1	základní
mapa	mapa	k1gFnSc1	mapa
sídliště	sídliště	k1gNnSc2	sídliště
</sent>
<sent id="4737">
Základní	základní	k2eAgFnSc1d1	základní
mapa	mapa	k1gFnSc1	mapa
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
</sent>
<sent id="4738">
Základní	základní	k2eAgFnSc1d1	základní
mapa	mapa	k1gFnSc1	mapa
tunelu	tunel	k1gInSc2	tunel
metra	metro	k1gNnSc2	metro
</sent>
<sent id="4739">
Základní	základní	k2eAgFnSc1d1	základní
mapa	mapa	k1gFnSc1	mapa
závodu	závod	k1gInSc2	závod
(	(	kIx(	
<g/>
ZMZ	ZMZ	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4740">
železniční	železniční	k2eAgFnSc1d1	železniční
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="5784">
dílčí	dílčí	k2eAgInSc1d1	dílčí
posun	posun	k1gInSc1	posun
</sent>
<sent id="4826">
faksimile	faksimile	k1gNnSc6	faksimile
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4838">
hypsometrická	hypsometrický	k2eAgFnSc1d1	hypsometrický
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="5090">
určovací	určovací	k2eAgInSc4d1	určovací
směr	směr	k1gInSc4	směr
</sent>
<sent id="4790">
rozlišovací	rozlišovací	k2eAgFnSc4d1	rozlišovací
schopnost	schopnost	k1gFnSc4	schopnost
na	na	k7c6	
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
</sent>
<sent id="5691">
nucená	nucený	k2eAgFnSc1d1	nucená
centrace	centrace	k1gFnSc1	centrace
</sent>
<sent id="5027">
nitkový	nitkový	k2eAgInSc4d1	nitkový
planimetr	planimetr	k1gInSc4	planimetr
</sent>
<sent id="4742">
výškové	výškový	k2eAgFnPc1d1	výšková
měřítko	měřítko	k1gNnSc4	měřítko
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4743">
vyzařování	vyzařování	k1gNnSc2	vyzařování
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
</sent>
<sent id="4744">
aktivní	aktivní	k2eAgInSc4d1	aktivní
dálkový	dálkový	k2eAgInSc4d1	dálkový
průzkum	průzkum	k1gInSc4	průzkum
Země	zem	k1gFnSc2	zem
</sent>
<sent id="4745">
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
opar	opar	k1gInSc4	opar
</sent>
<sent id="4746">
blízké	blízký	k2eAgNnSc4d1	blízké
infračervené	infračervený	k2eAgNnSc4d1	infračervené
pásmo	pásmo	k1gNnSc4	pásmo
</sent>
<sent id="4747">
blízké	blízký	k2eAgNnSc1d1	blízké
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
</sent>
<sent id="4749">
daleké	daleký	k2eAgNnSc1d1	daleké
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
</sent>
<sent id="4750">
dálkový	dálkový	k2eAgInSc4d1	dálkový
průzkum	průzkum	k1gInSc4	průzkum
Země	zem	k1gFnSc2	zem
z	z	k7c2	
kosmu	kosmos	k1gInSc2	kosmos
</sent>
<sent id="4751">
difúzní	difúzní	k2eAgInSc4d1	difúzní
odraz	odraz	k1gInSc4	odraz
</sent>
<sent id="4752">
dlouhovlnné	dlouhovlnný	k2eAgNnSc4d1	dlouhovlnné
vyzařování	vyzařování	k1gNnSc4	vyzařování
</sent>
<sent id="4813">
infračervené	infračervený	k2eAgNnSc4d1	infračervené
krátkovlnné	krátkovlnný	k2eAgNnSc4d1	krátkovlnné
vyzařování	vyzařování	k1gNnSc4	vyzařování
</sent>
<sent id="4755">
frekvenční	frekvenční	k2eAgInSc4d1	frekvenční
pásmo	pásmo	k1gNnSc1	pásmo
</sent>
<sent id="4756">
geometrická	geometrický	k2eAgFnSc1d1	geometrická
oprava	oprava	k1gFnSc1	oprava
</sent>
<sent id="4758">
infračervené	infračervený	k2eAgNnSc1d1	infračervené
pásmo	pásmo	k1gNnSc1	pásmo
</sent>
<sent id="4815">
interferometrická	interferometrický	k2eAgFnSc1d1	interferometrická
základna	základna	k1gFnSc1	základna
(	(	kIx(	
<g/>
DPZ	DPZ	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4760">
infračervený	infračervený	k2eAgInSc4d1	infračervený
obraz	obraz	k1gInSc4	obraz
</sent>
<sent id="4761">
koherentní	koherentní	k2eAgInSc4d1	koherentní
záření	záření	k1gNnSc1	záření
</sent>
<sent id="5959">
aerotriangulace	aerotriangulace	k1gFnSc1	aerotriangulace
</sent>
<sent id="4763">
kosmický	kosmický	k2eAgInSc4d1	kosmický
snímek	snímek	k1gInSc4	snímek
</sent>
<sent id="4764">
letecký	letecký	k2eAgInSc1d1	letecký
dálkový	dálkový	k2eAgInSc1d1	dálkový
průzkum	průzkum	k1gInSc1	průzkum
Země	zem	k1gFnSc2	zem
</sent>
<sent id="4765">
letecký	letecký	k2eAgInSc1d1	letecký
snímek	snímek	k1gInSc1	snímek
</sent>
<sent id="4766">
mikrovlnné	mikrovlnný	k2eAgNnSc1d1	mikrovlnné
záření	záření	k1gNnSc1	záření
</sent>
<sent id="4767">
multispektrální	multispektrální	k2eAgInSc1d1	multispektrální
letecký	letecký	k2eAgInSc1d1	letecký
obrazový	obrazový	k2eAgInSc1d1	obrazový
záznam	záznam	k1gInSc1	záznam
</sent>
<sent id="4768">
multispektrální	multispektrální	k2eAgInSc4d1	multispektrální
měření	měření	k1gNnSc1	měření
</sent>
<sent id="4769">
multispektrální	multispektrální	k2eAgInSc1d1	multispektrální
obrazový	obrazový	k2eAgInSc1d1	obrazový
záznam	záznam	k1gInSc1	záznam
</sent>
<sent id="4770">
obrazový	obrazový	k2eAgInSc1d1	obrazový
detektor	detektor	k1gInSc1	detektor
</sent>
<sent id="4771">
obrazový	obrazový	k2eAgInSc1d1	obrazový
senzor	senzor	k1gInSc1	senzor
</sent>
<sent id="4773">
odraz	odraz	k1gInSc1	odraz
(	(	kIx(	
<g/>
záření	záření	k1gNnSc1	záření
<g/>
)	)	kIx)	
</sent>
<sent id="4774">
odražeč	odražeč	k1gInSc1	odražeč
(	(	kIx(	
<g/>
radarového	radarový	k2eAgNnSc2d1	radarové
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	
</sent>
<sent id="4775">
odražené	odražený	k2eAgNnSc1d1	odražené
záření	záření	k1gNnSc1	záření
</sent>
<sent id="4776">
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
zorné	zorný	k2eAgFnSc2d1	zorná
pole	pole	k1gFnSc2	pole
</sent>
<sent id="4777">
optické	optický	k2eAgNnSc1d1	optické
záření	záření	k1gNnSc1	záření
</sent>
<sent id="4778">
pás	pás	k1gInSc1	pás
propustnosti	propustnost	k1gFnSc2	propustnost
</sent>
<sent id="4779">
pasivní	pasivní	k2eAgInSc1d1	pasivní
dálkový	dálkový	k2eAgInSc1d1	dálkový
průzkum	průzkum	k1gInSc1	průzkum
Země	zem	k1gFnSc2	zem
</sent>
<sent id="4780">
polarizační	polarizační	k2eAgInSc4d1	polarizační
rozlišení	rozlišení	k1gNnSc1	rozlišení
</sent>
<sent id="4781">
radarový	radarový	k2eAgInSc1d1	radarový
dálkový	dálkový	k2eAgInSc1d1	dálkový
průzkum	průzkum	k1gInSc1	průzkum
Země	zem	k1gFnSc2	zem
</sent>
<sent id="4782">
tepelné	tepelný	k2eAgNnSc1d1	tepelné
záření	záření	k1gNnSc1	záření
</sent>
<sent id="4783">
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
</sent>
<sent id="4784">
viditelné	viditelný	k2eAgNnSc1d1	viditelné
záření	záření	k1gNnSc1	záření
</sent>
<sent id="4785">
radarový	radarový	k2eAgInSc1d1	radarový
obrazový	obrazový	k2eAgInSc1d1	obrazový
záznam	záznam	k1gInSc1	záznam
</sent>
<sent id="4786">
radarový	radarový	k2eAgInSc4d1	radarový
stín	stín	k1gInSc4	stín
</sent>
<sent id="4788">
rastrová	rastrový	k2eAgFnSc1d1	rastrová
transformace	transformace	k1gFnSc1	transformace
</sent>
<sent id="4789">
rozlišovací	rozlišovací	k2eAgFnSc4d1	rozlišovací
schopnost	schopnost	k1gFnSc4	schopnost
</sent>
<sent id="4791">
rozptyl	rozptyl	k1gInSc1	rozptyl
záření	záření	k1gNnSc2	záření
</sent>
<sent id="4793">
scéna	scéna	k1gFnSc1	scéna
</sent>
<sent id="4795">
skenovaný	skenovaný	k2eAgInSc4d1	skenovaný
snímek	snímek	k1gInSc4	snímek
</sent>
<sent id="4796">
spektrální	spektrální	k2eAgFnSc4d1	spektrální
citlivost	citlivost	k1gFnSc4	citlivost
</sent>
<sent id="4797">
spektrální	spektrální	k2eAgInSc4d1	spektrální
odraz	odraz	k1gInSc4	odraz
</sent>
<sent id="4798">
spektrální	spektrální	k2eAgInSc4d1	spektrální
pásmo	pásmo	k1gNnSc1	pásmo
</sent>
<sent id="4799">
spektrální	spektrální	k2eAgInSc4d1	spektrální
příznak	příznak	k1gInSc4	příznak
</sent>
<sent id="4800">
spektrální	spektrální	k2eAgInSc4d1	spektrální
rozlišení	rozlišení	k1gNnSc1	rozlišení
</sent>
<sent id="4801">
spektrozonální	spektrozonální	k2eAgInSc4d1	spektrozonální
snímek	snímek	k1gInSc4	snímek
</sent>
<sent id="4802">
šum	šum	k1gInSc1	šum
(	(	kIx(	
<g/>
DPZ	DPZ	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="4803">
tepelné	tepelný	k2eAgNnSc4d1	tepelné
infračervené	infračervený	k2eAgNnSc4d1	infračervené
pásmo	pásmo	k1gNnSc4	pásmo
</sent>
<sent id="4804">
teplota	teplota	k1gFnSc1	teplota
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
</sent>
<sent id="4805">
termální	termální	k2eAgInSc1d1	termální
obrazový	obrazový	k2eAgInSc1d1	obrazový
záznam	záznam	k1gInSc1	záznam
</sent>
<sent id="4807">
úhel	úhel	k1gInSc1	úhel
pohledu	pohled	k1gInSc2	pohled
antény	anténa	k1gFnSc2	anténa
</sent>
<sent id="4808">
vyzařování	vyzařování	k1gNnSc6	vyzařování
půdy	půda	k1gFnSc2	půda
</sent>
<sent id="4809">
zorné	zorný	k2eAgFnSc2d1	zorná
pole	pole	k1gFnSc2	pole
</sent>
<sent id="4810">
zpětný	zpětný	k2eAgInSc4d1	zpětný
rozptyl	rozptyl	k1gInSc4	rozptyl
</sent>
<sent id="4811">
zrcadlový	zrcadlový	k2eAgInSc4d1	zrcadlový
odraz	odraz	k1gInSc4	odraz
</sent>
<sent id="4812">
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
</sent>
<sent id="4814">
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
</sent>
<sent id="4816">
záření	záření	k1gNnSc2	záření
černého	černé	k1gNnSc2	černé
tělesa	těleso	k1gNnSc2	těleso
</sent>
<sent id="4817">
záření	záření	k1gNnSc6	záření
Země	zem	k1gFnSc2	zem
</sent>
<sent id="4819">
zpracování	zpracování	k1gNnSc6	zpracování
obrazu	obraz	k1gInSc2	obraz
</sent>
<sent id="4820">
plavební	plavební	k2eAgFnSc1d1	plavební
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4821">
atlas	atlas	k1gInSc1	atlas
</sent>
<sent id="4822">
batymetrická	batymetrický	k2eAgFnSc1d1	batymetrický
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4823">
dějepisný	dějepisný	k2eAgInSc4d1	dějepisný
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4824">
druh	druh	k1gInSc1	druh
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4825">
důlní	důlní	k2eAgFnSc1d1	důlní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4827">
geografická	geografický	k2eAgFnSc1d1	geografická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4828">
geologická	geologický	k2eAgFnSc1d1	geologická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4829">
geografický	geografický	k2eAgInSc4d1	geografický
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4830">
geomagnetická	geomagnetický	k2eAgFnSc1d1	geomagnetická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4831">
glóbový	glóbový	k2eAgInSc4d1	glóbový
vrchlík	vrchlík	k1gInSc4	vrchlík
</sent>
<sent id="4832">
glóbus	glóbus	k1gInSc1	glóbus
</sent>
<sent id="4833">
gravimetrická	gravimetrický	k2eAgFnSc1d1	gravimetrická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4834">
historická	historický	k2eAgFnSc1d1	historická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4835">
historie	historie	k1gFnSc2	historie
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4836">
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
glóbus	glóbus	k1gInSc4	glóbus
</sent>
<sent id="4837">
hydrologická	hydrologický	k2eAgFnSc1d1	hydrologická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4840">
kapesní	kapesní	k2eAgInSc4d1	kapesní
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4842">
kartogram	kartogram	k1gInSc1	kartogram
</sent>
<sent id="4843">
letecká	letecký	k2eAgFnSc1d1	letecká
navigační	navigační	k2eAgFnSc1d1	navigační
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4844">
mapa	mapa	k1gFnSc1	mapa
světa	svět	k1gInSc2	svět
</sent>
<sent id="4845">
mapová	mapový	k2eAgFnSc1d1	mapová
skládanka	skládanka	k1gFnSc1	skládanka
</sent>
<sent id="4846">
měsíční	měsíční	k2eAgInSc4d1	měsíční
glóbus	glóbus	k1gInSc4	glóbus
</sent>
<sent id="4847">
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
mapa	mapa	k1gFnSc1	mapa
světa	svět	k1gInSc2	svět
</sent>
<sent id="4849">
národní	národní	k2eAgInSc4d1	národní
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4850">
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4851">
obrysová	obrysový	k2eAgFnSc1d1	obrysová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4852">
orientační	orientační	k2eAgFnSc1d1	orientační
mapa	mapa	k1gFnSc1	mapa
města	město	k1gNnSc2	město
</sent>
<sent id="4853">
předpovědní	předpovědní	k2eAgFnSc1d1	předpovědní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4854">
regionální	regionální	k2eAgInSc4d1	regionální
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4855">
reliéfní	reliéfní	k2eAgFnSc1d1	reliéfní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4856">
silniční	silniční	k2eAgFnSc1d1	silniční
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4857">
soubor	soubor	k1gInSc1	soubor
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="4858">
stará	starý	k2eAgFnSc1d1	stará
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4859">
statistická	statistický	k2eAgFnSc1d1	statistická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4860">
školní	školní	k2eAgInSc4d1	školní
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4861">
tematický	tematický	k2eAgInSc4d1	tematický
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4862">
turistická	turistický	k2eAgFnSc1d1	turistická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4863">
tyflografická	tyflografický	k2eAgFnSc1d1	tyflografický
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4865">
zemský	zemský	k2eAgInSc4d1	zemský
glóbus	glóbus	k1gInSc4	glóbus
</sent>
<sent id="4866">
anamorfóza	anamorfóza	k1gFnSc1	anamorfóza
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="4867">
tvarování	tvarování	k1gNnSc6	tvarování
glóbu	glóbus	k1gInSc2	glóbus
</sent>
<sent id="4868">
strukturní	strukturní	k2eAgInSc4d1	strukturní
kartodiagram	kartodiagram	k1gInSc4	kartodiagram
</sent>
<sent id="4869">
síťový	síťový	k2eAgInSc4d1	síťový
kartogram	kartogram	k1gInSc4	kartogram
</sent>
<sent id="4870">
strukturní	strukturní	k2eAgInSc4d1	strukturní
kartogram	kartogram	k1gInSc4	kartogram
</sent>
<sent id="4871">
součtový	součtový	k2eAgInSc4d1	součtový
kartodiagram	kartodiagram	k1gInSc4	kartodiagram
</sent>
<sent id="4872">
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
kartodiagram	kartodiagram	k1gInSc4	kartodiagram
</sent>
<sent id="4873">
histogram	histogram	k1gInSc1	histogram
četnosti	četnost	k1gFnSc2	četnost
</sent>
<sent id="4874">
frekvenční	frekvenční	k2eAgFnSc1d1	frekvenční
křivka	křivka	k1gFnSc1	křivka
</sent>
<sent id="4875">
dopravní	dopravní	k2eAgFnSc1d1	dopravní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4876">
mapa	mapa	k1gFnSc1	mapa
izochron	izochrona	k1gFnPc2	izochrona
</sent>
<sent id="4877">
Mapa	mapa	k1gFnSc1	mapa
světa	svět	k1gInSc2	svět
1	#num#	k4	
<g/>
:	:	kIx,	
<g/>
2	#num#	k4	
500	#num#	k4	
000	#num#	k4	
</sent>
<sent id="4878">
analytická	analytický	k2eAgFnSc1d1	analytická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4879">
administrativní	administrativní	k2eAgFnSc1d1	administrativní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4880">
fyzickogeografická	fyzickogeografický	k2eAgFnSc1d1	fyzickogeografická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4881">
přehledná	přehledný	k2eAgFnSc1d1	přehledná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4882">
letecká	letecký	k2eAgFnSc1d1	letecká
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4884">
sociálně	sociálně	k6eAd1	
geografická	geografický	k2eAgFnSc1d1	geografická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4885">
propagační	propagační	k2eAgFnSc1d1	propagační
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4886">
synoptická	synoptický	k2eAgFnSc1d1	synoptická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4887">
cykloturistická	cykloturistický	k2eAgFnSc1d1	cykloturistická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4888">
příruční	příruční	k2eAgFnSc1d1	příruční
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4889">
školní	školní	k2eAgInSc4d1	školní
zeměpisný	zeměpisný	k2eAgInSc4d1	zeměpisný
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4890">
příruční	příruční	k2eAgInSc4d1	příruční
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4891">
velký	velký	k2eAgInSc4d1	velký
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4892">
laserový	laserový	k2eAgInSc4d1	laserový
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="4893">
bod	bod	k1gInSc1	bod
polohopisu	polohopis	k1gInSc2	polohopis
</sent>
<sent id="4894">
bod	bod	k1gInSc1	bod
výškopisu	výškopis	k1gInSc2	výškopis
</sent>
<sent id="4895">
tendenční	tendenční	k2eAgFnSc1d1	tendenční
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4896">
smyšlená	smyšlený	k2eAgFnSc1d1	smyšlená
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4897">
mapa	mapa	k1gFnSc1	mapa
okolí	okolí	k1gNnSc2	okolí
</sent>
<sent id="4898">
inkunábule	inkunábule	k1gFnSc1	inkunábule
</sent>
<sent id="4899">
planiglóby	planiglób	k1gInPc7	planiglób
</sent>
<sent id="4900">
glóbový	glóbový	k2eAgInSc4d1	glóbový
pás	pás	k1gInSc4	pás
</sent>
<sent id="4901">
aktuální	aktuální	k2eAgFnSc1d1	aktuální
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4902">
atlas	atlas	k1gInSc1	atlas
Měsíce	měsíc	k1gInSc2	měsíc
</sent>
<sent id="4903">
barevná	barevný	k2eAgFnSc1d1	barevná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4904">
fluoreskující	fluoreskující	k2eAgFnSc1d1	fluoreskující
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4905">
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4906">
hydrografická	hydrografický	k2eAgFnSc1d1	hydrografická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4907">
ilustrovaný	ilustrovaný	k2eAgInSc4d1	ilustrovaný
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4908">
jednobarevná	jednobarevný	k2eAgFnSc1d1	jednobarevná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4909">
mapa	mapa	k1gFnSc1	mapa
leteckých	letecký	k2eAgFnPc2d1	letecká
tratí	trať	k1gFnPc2	trať
</sent>
<sent id="6283">
mapa	mapa	k1gFnSc1	mapa
státu	stát	k1gInSc2	stát
</sent>
<sent id="6284">
mapa	mapa	k1gFnSc1	mapa
světadílu	světadíl	k1gInSc2	světadíl
</sent>
<sent id="4913">
námořní	námořní	k2eAgInSc4d1	námořní
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4914">
navigační	navigační	k2eAgFnSc1d1	navigační
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4915">
obrazová	obrazový	k2eAgFnSc1d1	obrazová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4916">
oceánografický	oceánografický	k2eAgInSc4d1	oceánografický
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="4917">
plakátová	plakátový	k2eAgFnSc1d1	plakátová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4918">
planisféra	planisféra	k1gFnSc1	planisféra
</sent>
<sent id="4919">
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4920">
zastaralá	zastaralý	k2eAgFnSc1d1	zastaralá
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4921">
pohledová	pohledový	k2eAgFnSc1d1	pohledová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4922">
pramenná	pramenný	k2eAgFnSc1d1	pramenná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4923">
prospektová	prospektový	k2eAgFnSc1d1	prospektová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4924">
reliéfní	reliéfní	k2eAgInSc4d1	reliéfní
glóbus	glóbus	k1gInSc4	glóbus
</sent>
<sent id="4925">
schematická	schematický	k2eAgFnSc1d1	schematická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4926">
stupňový	stupňový	k2eAgInSc1d1	stupňový
model	model	k1gInSc1	model
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="4927">
svítící	svítící	k2eAgInSc4d1	svítící
glóbus	glóbus	k1gInSc4	glóbus
</sent>
<sent id="4928">
úřední	úřední	k2eAgFnSc1d1	úřední
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4929">
Základní	základní	k2eAgFnSc1d1	základní
mapa	mapa	k1gFnSc1	mapa
ČR	ČR	kA	
</sent>
<sent id="4930">
geografické	geografický	k2eAgNnSc4d1	geografické
jméno	jméno	k1gNnSc4	jméno
</sent>
<sent id="4931">
hodonymum	hodonymum	k1gInSc1	hodonymum
</sent>
<sent id="4932">
hydronymum	hydronymum	k1gInSc1	hydronymum
</sent>
<sent id="4933">
choronymum	choronymum	k1gInSc1	choronymum
</sent>
<sent id="4934">
oronymum	oronymum	k1gInSc1	oronymum
</sent>
<sent id="4935">
toponymum	toponymum	k1gNnSc4	toponymum
</sent>
<sent id="4938">
rukopisná	rukopisný	k2eAgFnSc1d1	rukopisná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4939">
složený	složený	k2eAgInSc4d1	složený
kartodiagram	kartodiagram	k1gInSc4	kartodiagram
</sent>
<sent id="4940">
syntetická	syntetický	k2eAgFnSc1d1	syntetická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4941">
anaglyfová	anaglyfový	k2eAgFnSc1d1	anaglyfový
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4943">
digitální	digitální	k2eAgInSc4d1	digitální
zpracování	zpracování	k1gNnSc1	zpracování
obrazových	obrazový	k2eAgNnPc2d1	obrazové
dat	datum	k1gNnPc2	datum
</sent>
<sent id="4944">
snímkový	snímkový	k2eAgInSc4d1	snímkový
nadir	nadir	k1gInSc4	nadir
</sent>
<sent id="4945">
střední	střední	k2eAgFnSc4d1	střední
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
</sent>
<sent id="4946">
přírodní	přírodní	k2eAgInSc4d1	přírodní
zdroj	zdroj	k1gInSc4	zdroj
záření	záření	k1gNnSc2	záření
</sent>
<sent id="4947">
obrazový	obrazový	k2eAgInSc1d1	obrazový
snímač	snímač	k1gInSc1	snímač
</sent>
<sent id="4949">
GBP	GBP	kA	
</sent>
<sent id="4950">
ZGS	ZGS	kA	
</sent>
<sent id="4951">
grafická	grafický	k2eAgFnSc1d1	grafická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="4952">
busolní	busolní	k2eAgFnSc1d1	busolní
tachymetrie	tachymetrie	k1gFnSc1	tachymetrie
</sent>
<sent id="4953">
číselná	číselný	k2eAgFnSc1d1	číselná
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4954">
číselná	číselný	k2eAgFnSc1d1	číselná
tachymetrie	tachymetrie	k1gFnSc1	tachymetrie
</sent>
<sent id="4955">
délka	délka	k1gFnSc1	délka
</sent>
<sent id="4956">
grafická	grafický	k2eAgFnSc1d1	grafická
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="4957">
grafické	grafický	k2eAgNnSc1d1	grafické
protínání	protínání	k1gNnSc1	protínání
</sent>
<sent id="4958">
hromadné	hromadný	k2eAgNnSc4d1	hromadné
protínání	protínání	k1gNnPc4	protínání
vpřed	vpřed	k6eAd1	
</sent>
<sent id="4959">
kolmice	kolmice	k1gFnSc1	kolmice
</sent>
<sent id="4960">
kolmičkování	kolmičkování	k1gNnSc4	kolmičkování
</sent>
<sent id="4961">
konstrukční	konstrukční	k2eAgFnSc1d1	konstrukční
míra	míra	k1gFnSc1	míra
</sent>
<sent id="4962">
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
míra	míra	k1gFnSc1	míra
</sent>
<sent id="4964">
měřický	měřický	k2eAgInSc4d1	měřický
prvek	prvek	k1gInSc4	prvek
</sent>
<sent id="4965">
měřený	měřený	k2eAgInSc4d1	měřený
směr	směr	k1gInSc4	směr
</sent>
<sent id="4966">
mezník	mezník	k1gInSc1	mezník
</sent>
<sent id="4967">
nitková	nitkový	k2eAgFnSc1d1	Nitková
tachymetrie	tachymetrie	k1gFnSc1	tachymetrie
</sent>
<sent id="4968">
boční	boční	k2eAgFnSc1d1	boční
záměra	záměra	k1gFnSc1	záměra
</sent>
<sent id="4969">
nivelační	nivelační	k2eAgInSc1d1	nivelační
zápisník	zápisník	k1gInSc1	zápisník
</sent>
<sent id="4970">
orientace	orientace	k1gFnSc1	orientace
měřického	měřický	k2eAgInSc2d1	měřický
stolu	stol	k1gInSc2	stol
</sent>
<sent id="4971">
plošná	plošný	k2eAgFnSc1d1	plošná
nivelace	nivelace	k1gFnSc1	nivelace
</sent>
<sent id="4972">
polární	polární	k2eAgInPc1d1	polární
prvky	prvek	k1gInPc1	prvek
</sent>
<sent id="4973">
polohopisný	polohopisný	k2eAgInSc1d1	polohopisný
náčrt	náčrt	k1gInSc1	náčrt
</sent>
<sent id="4974">
protínání	protínání	k1gNnSc4	protínání
</sent>
<sent id="4975">
protínání	protínání	k1gNnPc2	protínání
vpřed	vpřed	k6eAd1	
</sent>
<sent id="4976">
rekognoskační	rekognoskační	k2eAgInSc1d1	rekognoskační
náčrt	náčrt	k1gInSc1	náčrt
</sent>
<sent id="4977">
stanovisko	stanovisko	k1gNnSc1	stanovisko
polárního	polární	k2eAgNnSc2d1	polární
měření	měření	k1gNnSc2	měření
</sent>
<sent id="4978">
stolová	stolový	k2eAgFnSc1d1	stolová
tachymetrie	tachymetrie	k1gFnSc1	tachymetrie
</sent>
<sent id="4979">
terénní	terénní	k2eAgInSc4d1	terénní
bod	bod	k1gInSc4	bod
</sent>
<sent id="4980">
urovnání	urovnání	k1gNnSc2	urovnání
měřického	měřický	k2eAgInSc2d1	měřický
stolu	stol	k1gInSc2	stol
</sent>
<sent id="4981">
vyhledání	vyhledání	k1gNnSc6	vyhledání
bodu	bod	k1gInSc2	bod
</sent>
<sent id="4982">
výškopisný	výškopisný	k2eAgInSc1d1	výškopisný
náčrt	náčrt	k1gInSc1	náčrt
</sent>
<sent id="4983">
výškový	výškový	k2eAgInSc4d1	výškový
bod	bod	k1gInSc4	bod
</sent>
<sent id="4984">
výškový	výškový	k2eAgInSc4d1	výškový
měřický	měřický	k2eAgInSc4d1	měřický
údaj	údaj	k1gInSc4	údaj
</sent>
<sent id="4985">
výškový	výškový	k2eAgInSc4d1	výškový
prvek	prvek	k1gInSc4	prvek
</sent>
<sent id="4986">
záměra	záměra	k1gFnSc1	záměra
</sent>
<sent id="4987">
zaměřování	zaměřování	k1gNnSc6	zaměřování
terénu	terén	k1gInSc2	terén
profilováním	profilování	k1gNnPc3	profilování
</sent>
<sent id="4988">
délková	délkový	k2eAgFnSc1d1	délková
kóta	kóta	k1gFnSc1	kóta
</sent>
<sent id="4989">
doměrek	doměrek	k1gInSc1	doměrek
</sent>
<sent id="4990">
hloubnice	hloubnice	k1gFnSc1	hloubnice
</sent>
<sent id="4991">
hustota	hustota	k1gFnSc1	hustota
bodového	bodový	k2eAgNnSc2d1	bodové
pole	pole	k1gNnSc2	pole
</sent>
<sent id="4992">
hustota	hustota	k1gFnSc1	hustota
polohopisu	polohopis	k1gInSc2	polohopis
</sent>
<sent id="4993">
izobáza	izobáza	k1gFnSc1	izobáza
</sent>
<sent id="4994">
izočára	izočár	k1gMnSc4	izočár
</sent>
<sent id="4995">
izogama	izogama	k1gFnSc1	izogama
</sent>
<sent id="4996">
izogóna	izogóna	k1gFnSc1	izogóna
</sent>
<sent id="4997">
izopléta	izopléta	k1gFnSc1	izopléta
</sent>
<sent id="4999">
konstrukce	konstrukce	k1gFnSc2	konstrukce
vrstevnic	vrstevnice	k1gFnPc2	vrstevnice
</sent>
<sent id="5000">
kóta	kóta	k1gFnSc1	kóta
</sent>
<sent id="5001">
kóta	kóta	k1gFnSc1	kóta
hloubnice	hloubnice	k1gFnSc1	hloubnice
</sent>
<sent id="5002">
kóta	kóta	k1gFnSc1	kóta
vrstevnice	vrstevnice	k1gFnSc1	vrstevnice
</sent>
<sent id="5003">
měření	měření	k1gNnPc2	měření
výšek	výška	k1gFnPc2	výška
</sent>
<sent id="5004">
názvoslovná	názvoslovný	k2eAgFnSc1d1	názvoslovná
komise	komise	k1gFnSc1	komise
</sent>
<sent id="5005">
nepřímé	přímý	k2eNgNnSc4d1	nepřímé
měření	měření	k1gNnSc4	měření
</sent>
<sent id="5006">
optický	optický	k2eAgInSc4d1	optický
centrovač	centrovač	k1gInSc4	centrovač
</sent>
<sent id="5008">
rostlinný	rostlinný	k2eAgInSc4d1	rostlinný
kryt	kryt	k1gInSc4	kryt
</sent>
<sent id="5009">
terminologická	terminologický	k2eAgFnSc1d1	terminologická
komise	komise	k1gFnSc1	komise
</sent>
<sent id="5010">
trigonometrické	trigonometrický	k2eAgNnSc1d1	trigonometrické
měření	měření	k1gNnSc1	měření
výšek	výška	k1gFnPc2	výška
</sent>
<sent id="5013">
interval	interval	k1gInSc1	interval
vrstevnic	vrstevnice	k1gFnPc2	vrstevnice
</sent>
<sent id="5014">
automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
kresba	kresba	k1gFnSc1	kresba
</sent>
<sent id="5015">
cizí	cizit	k5eAaImIp3nS	
jméno	jméno	k1gNnSc4	jméno
</sent>
<sent id="5016">
domácí	domácí	k1gFnSc6	domácí
jméno	jméno	k1gNnSc1	jméno
</sent>
<sent id="5017">
geodetická	geodetický	k2eAgFnSc1d1	geodetická
observatoř	observatoř	k1gFnSc1	observatoř
</sent>
<sent id="5020">
klopná	klopný	k2eAgFnSc1d1	klopný
osa	osa	k1gFnSc1	osa
dalekohledu	dalekohled	k1gInSc2	dalekohled
</sent>
<sent id="5019">
grafický	grafický	k2eAgInSc4d1	grafický
podklad	podklad	k1gInSc4	podklad
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5021">
křížek	křížek	k1gInSc1	křížek
souřadnicové	souřadnicový	k2eAgFnSc2d1	souřadnicová
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="5022">
lokalizace	lokalizace	k1gFnSc1	lokalizace
objektu	objekt	k1gInSc2	objekt
</sent>
<sent id="5023">
měření	měření	k1gNnPc2	měření
oměrných	oměrný	k2eAgFnPc2d1	oměrná
měr	míra	k1gFnPc2	míra
</sent>
<sent id="5024">
měření	měření	k1gNnPc4	měření
směrů	směr	k1gInPc2	směr
ve	v	k7c6	
skupinách	skupina	k1gFnPc6	skupina
</sent>
<sent id="5025">
měření	měření	k1gNnSc2	měření
v	v	k7c6	
řadách	řada	k1gFnPc6	řada
</sent>
<sent id="5026">
měřické	měřický	k2eAgNnSc1d1	měřické
pásmo	pásmo	k1gNnSc1	pásmo
</sent>
<sent id="5028">
observátor	observátor	k1gMnSc1	observátor
</sent>
<sent id="6584">
optické	optický	k2eAgNnSc1d1	optické
měření	měření	k1gNnSc1	měření
délek	délka	k1gFnPc2	délka
</sent>
<sent id="5030">
odvození	odvození	k1gNnSc6	odvození
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5031">
okulárový	okulárový	k2eAgInSc4d1	okulárový
mikrometr	mikrometr	k1gInSc4	mikrometr
</sent>
<sent id="5032">
optická	optický	k2eAgFnSc1d1	optická
osa	osa	k1gFnSc1	osa
alekohledu	alekohled	k1gInSc2	alekohled
</sent>
<sent id="5033">
orientace	orientace	k1gFnSc1	orientace
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5034">
orientace	orientace	k1gFnSc1	orientace
osnovy	osnova	k1gFnSc2	osnova
směrů	směr	k1gInPc2	směr
</sent>
<sent id="5035">
osa	osa	k1gFnSc1	osa
(	(	kIx(	
<g/>
vodního	vodní	k2eAgInSc2d1	vodní
<g/>
)	)	kIx)	
toku	tok	k1gInSc2	tok
</sent>
<sent id="5036">
osnova	osnova	k1gFnSc1	osnova
směrů	směr	k1gInPc2	směr
</sent>
<sent id="5037">
pata	pata	k1gFnSc1	pata
mapové	mapový	k2eAgFnSc2d1	mapová
značky	značka	k1gFnSc2	značka
</sent>
<sent id="5038">
pauzovací	pauzovací	k2eAgInSc4d1	pauzovací
papír	papír	k1gInSc4	papír
</sent>
<sent id="5039">
polární	polární	k2eAgInSc1d1	polární
planimetr	planimetr	k1gInSc1	planimetr
</sent>
<sent id="5040">
přehledný	přehledný	k2eAgInSc1d1	přehledný
náčrt	náčrt	k1gInSc1	náčrt
bodového	bodový	k2eAgNnSc2d1	bodové
pole	pole	k1gNnSc2	pole
</sent>
<sent id="5041">
rovinná	rovinný	k2eAgFnSc1d1	rovinná
pravoúhlá	pravoúhlý	k2eAgFnSc1d1	pravoúhlá
souřadnicová	souřadnicový	k2eAgFnSc1d1	souřadnicová
síť	síť	k1gFnSc1	síť
</sent>
<sent id="5042">
šroubový	šroubový	k2eAgInSc4d1	šroubový
mikrometr	mikrometr	k1gInSc4	mikrometr
</sent>
<sent id="5043">
topografický	topografický	k2eAgInSc4d1	topografický
originál	originál	k1gInSc4	originál
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5044">
triangulační	triangulační	k2eAgInSc1d1	triangulační
list	list	k1gInSc1	list
</sent>
<sent id="5045">
vernierový	vernierový	k2eAgInSc1d1	vernierový
mikroskop	mikroskop	k1gInSc1	mikroskop
</sent>
<sent id="5047">
základní	základní	k2eAgInSc1d1	základní
triangulační	triangulační	k2eAgInSc1d1	triangulační
list	list	k1gInSc1	list
</sent>
<sent id="5048">
automatický	automatický	k2eAgInSc1d1	automatický
kreslicí	kreslicí	k2eAgInSc1d1	kreslicí
stůl	stůl	k1gInSc1	stůl
</sent>
<sent id="5050">
dostřeďovací	dostřeďovací	k2eAgFnSc1d1	dostřeďovací
tyč	tyč	k1gFnSc1	tyč
</sent>
<sent id="5051">
hektarová	hektarový	k2eAgFnSc1d1	hektarová
síť	síť	k1gFnSc1	síť
</sent>
<sent id="5052">
hlavní	hlavní	k2eAgInSc1d1	hlavní
polygonový	polygonový	k2eAgInSc1d1	polygonový
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="5053">
hloubková	hloubkový	k2eAgFnSc1d1	hloubková
stabilizace	stabilizace	k1gFnSc1	stabilizace
</sent>
<sent id="5054">
hloubkový	hloubkový	k2eAgInSc4d1	hloubkový
úhel	úhel	k1gInSc4	úhel
</sent>
<sent id="5055">
kilometrová	kilometrový	k2eAgFnSc1d1	kilometrová
síť	síť	k1gFnSc1	síť
(	(	kIx(	
<g/>
na	na	k7c6	
mapě	mapa	k1gFnSc6	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="5056">
měřený	měřený	k2eAgInSc4d1	měřený
úhel	úhel	k1gInSc4	úhel
</sent>
<sent id="5057">
měřický	měřický	k2eAgInSc4d1	měřický
signál	signál	k1gInSc4	signál
</sent>
<sent id="5059">
neorientovaný	orientovaný	k2eNgInSc4d1	neorientovaný
pořad	pořad	k1gInSc4	pořad
</sent>
<sent id="5060">
nepřipojený	připojený	k2eNgInSc4d1	nepřipojený
pořad	pořad	k1gInSc4	pořad
</sent>
<sent id="5061">
nitkový	nitkový	k2eAgInSc4d1	nitkový
tachymetr	tachymetr	k1gInSc4	tachymetr
</sent>
<sent id="5063">
orientační	orientační	k2eAgInSc4d1	orientační
úhel	úhel	k1gInSc4	úhel
</sent>
<sent id="5064">
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
polygonový	polygonový	k2eAgInSc1d1	polygonový
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="5066">
paralaktická	paralaktický	k2eAgFnSc1d1	paralaktická
polygonometrie	polygonometrie	k1gFnSc1	polygonometrie
</sent>
<sent id="5067">
paralaktický	paralaktický	k2eAgInSc4d1	paralaktický
pořad	pořad	k1gInSc4	pořad
</sent>
<sent id="5068">
paralaktický	paralaktický	k2eAgInSc4d1	paralaktický
úhel	úhel	k1gInSc4	úhel
</sent>
<sent id="5069">
plastika	plastika	k1gFnSc1	plastika
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5070">
polygonizace	polygonizace	k1gFnSc1	polygonizace
</sent>
<sent id="5071">
polygonometrie	polygonometrie	k1gFnSc1	polygonometrie
</sent>
<sent id="5072">
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="5073">
projekt	projekt	k1gInSc1	projekt
mapování	mapování	k1gNnSc2	mapování
</sent>
<sent id="7163">
geometrická	geometrický	k2eAgFnSc1d1	geometrická
metoda	metoda	k1gFnSc1	metoda
měření	měření	k1gNnSc1	měření
</sent>
<sent id="5075">
protínání	protínání	k1gNnPc2	protínání
zpět	zpět	k6eAd1	
</sent>
<sent id="7229">
automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
objektová	objektový	k2eAgFnSc1d1	objektová
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="5078">
připojovací	připojovací	k2eAgFnSc1d1	připojovací
strana	strana	k1gFnSc1	strana
</sent>
<sent id="5079">
redukce	redukce	k1gFnSc1	redukce
délky	délka	k1gFnSc2	délka
na	na	k7c4	
výpočetní	výpočetní	k2eAgFnSc4d1	výpočetní
plochu	plocha	k1gFnSc4	plocha
</sent>
<sent id="5080">
redukce	redukce	k1gFnSc1	redukce
z	z	k7c2	
převýšení	převýšení	k1gNnSc2	převýšení
</sent>
<sent id="5081">
roh	roh	k1gInSc1	roh
mapového	mapový	k2eAgInSc2d1	mapový
listu	list	k1gInSc2	list
</sent>
<sent id="5083">
směrník	směrník	k1gInSc1	směrník
</sent>
<sent id="5084">
stolový	stolový	k2eAgInSc4d1	stolový
tachymetr	tachymetr	k1gInSc4	tachymetr
</sent>
<sent id="5085">
vertikální	vertikální	k2eAgInSc4d1	vertikální
úhel	úhel	k1gInSc4	úhel
</sent>
<sent id="5086">
tachymetrický	tachymetrický	k2eAgInSc4d1	tachymetrický
pořad	pořad	k1gInSc4	pořad
</sent>
<sent id="5087">
tyčová	tyčový	k2eAgFnSc1d1	tyčová
stabilizace	stabilizace	k1gFnSc1	stabilizace
</sent>
<sent id="5089">
určení	určení	k1gNnSc2	určení
měřítka	měřítko	k1gNnSc2	měřítko
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5092">
vetknutý	vetknutý	k2eAgInSc4d1	vetknutý
pořad	pořad	k1gInSc4	pořad
</sent>
<sent id="5093">
vodorovný	vodorovný	k2eAgInSc1d1	vodorovný
úhel	úhel	k1gInSc1	úhel
</sent>
<sent id="5094">
vrcholový	vrcholový	k2eAgInSc4d1	vrcholový
úhel	úhel	k1gInSc4	úhel
</sent>
<sent id="5095">
volný	volný	k2eAgInSc4d1	volný
pořad	pořad	k1gInSc4	pořad
</sent>
<sent id="5096">
výškový	výškový	k2eAgInSc4d1	výškový
pořad	pořad	k1gInSc4	pořad
</sent>
<sent id="5097">
výškový	výškový	k2eAgInSc4d1	výškový
úhel	úhel	k1gInSc4	úhel
</sent>
<sent id="5098">
zajišťovací	zajišťovací	k2eAgInSc4d1	zajišťovací
směr	směr	k1gInSc4	směr
</sent>
<sent id="5099">
základnová	základnový	k2eAgFnSc1d1	základnová
stabilizace	stabilizace	k1gFnSc1	stabilizace
</sent>
<sent id="5100">
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
úprava	úprava	k1gFnSc1	úprava
měřického	měřický	k2eAgInSc2d1	měřický
náčrtu	náčrt	k1gInSc2	náčrt
</sent>
<sent id="5101">
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
síť	síť	k1gFnSc1	síť
</sent>
<sent id="5102">
zobrazovací	zobrazovací	k2eAgInPc1d1	zobrazovací
trojúhelníky	trojúhelník	k1gInPc1	trojúhelník
</sent>
<sent id="5103">
odrazný	odrazný	k2eAgInSc4d1	odrazný
hranol	hranol	k1gInSc4	hranol
</sent>
<sent id="5104">
kalibrační	kalibrační	k2eAgFnSc1d1	kalibrační
a	a	k8xC	
měřicí	měřicí	k2eAgFnSc1d1	měřicí
schopnost	schopnost	k1gFnSc1	schopnost
</sent>
<sent id="5105">
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
měřicí	měřicí	k2eAgFnSc1d1	měřicí
schopnost	schopnost	k1gFnSc1	schopnost
</sent>
<sent id="5106">
normální	normální	k2eAgInSc4d1	normální
rozdělení	rozdělení	k1gNnSc1	rozdělení
</sent>
<sent id="5107">
vážený	vážený	k2eAgInSc4d1	vážený
průměr	průměr	k1gInSc4	průměr
</sent>
<sent id="5109">
číslo	číslo	k1gNnSc4	číslo
mapového	mapový	k2eAgInSc2d1	mapový
listu	list	k1gInSc2	list
</sent>
<sent id="7167">
č.	č.	k?	
<g/>
ev.	ev.	k?	
</sent>
<sent id="7168">
č.	č.	k?	
<g/>
or	or	k?	
<g/>
.	.	kIx.	
</sent>
<sent id="5111">
čtení	čtení	k1gNnSc6	čtení
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5112">
datum	datum	k1gNnSc4	datum
aktualizace	aktualizace	k1gFnSc2	aktualizace
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5113">
datum	datum	k1gNnSc4	datum
redakční	redakční	k2eAgFnSc2d1	redakční
uzávěrky	uzávěrka	k1gFnSc2	uzávěrka
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5114">
datum	datum	k1gNnSc4	datum
vydání	vydání	k1gNnSc2	vydání
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5115">
deformace	deformace	k1gFnSc1	deformace
filmu	film	k1gInSc2	film
</sent>
<sent id="5116">
deformace	deformace	k1gFnSc1	deformace
papíru	papír	k1gInSc2	papír
</sent>
<sent id="5117">
dějiny	dějiny	k1gFnPc4	dějiny
kartografie	kartografie	k1gFnSc2	kartografie
</sent>
<sent id="5118">
délkoměr	délkoměr	k1gInSc1	délkoměr
</sent>
<sent id="5119">
délkové	délkový	k2eAgNnSc1d1	délkové
zkreslení	zkreslení	k1gNnSc1	zkreslení
</sent>
<sent id="5120">
diazografická	diazografický	k2eAgFnSc1d1	diazografický
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="5121">
diazografie	diazografie	k1gFnSc1	diazografie
</sent>
<sent id="5122">
digitální	digitální	k2eAgInSc4d1	digitální
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="5124">
dotisk	dotisk	k1gInSc1	dotisk
</sent>
<sent id="5129">
ekvideformáta	ekvideformáta	k1gFnSc1	ekvideformáta
</sent>
<sent id="7206">
Český	český	k2eAgInSc1d1	český
úřad	úřad	k1gInSc1	úřad
zeměměřický	zeměměřický	k2eAgInSc1d1	zeměměřický
a	a	k8xC	
katastrální	katastrální	k2eAgMnSc1d1	katastrální
(	(	kIx(	
<g/>
ČÚZK	ČÚZK	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5131">
estetika	estetika	k1gFnSc1	estetika
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5133">
ALKIS	ALKIS	kA	
</sent>
<sent id="5134">
ATKIS	ATKIS	kA	
</sent>
<sent id="5135">
EC	EC	kA	
</sent>
<sent id="5136">
ESDI	ESDI	kA	
</sent>
<sent id="5139">
PCC	PCC	kA	
</sent>
<sent id="5140">
SDI	SDI	kA	
</sent>
<sent id="5141">
UML	UML	kA	
</sent>
<sent id="5142">
WCS	WCS	kA	
</sent>
<sent id="5143">
WFS	WFS	kA	
</sent>
<sent id="5144">
dvojitý	dvojitý	k2eAgInSc4d1	dvojitý
pentagon	pentagon	k1gInSc4	pentagon
</sent>
<sent id="5145">
geometrické	geometrický	k2eAgInPc4d1	geometrický
prvky	prvek	k1gInPc4	prvek
</sent>
<sent id="5146">
grafická	grafický	k2eAgFnSc1d1	grafická
triangulace	triangulace	k1gFnSc1	triangulace
</sent>
<sent id="5147">
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
profil	profil	k1gInSc4	profil
</sent>
<sent id="5148">
mapová	mapový	k2eAgFnSc1d1	mapová
příloha	příloha	k1gFnSc1	příloha
</sent>
<sent id="5149">
měřická	měřický	k2eAgFnSc1d1	měřická
pyramida	pyramida	k1gFnSc1	pyramida
</sent>
<sent id="5150">
podrobná	podrobný	k2eAgFnSc1d1	podrobná
triangulace	triangulace	k1gFnSc1	triangulace
</sent>
<sent id="5151">
podzemní	podzemní	k2eAgFnSc1d1	podzemní
prostora	prostora	k1gFnSc1	prostora
</sent>
<sent id="5153">
redukční	redukční	k2eAgInSc4d1	redukční
pravítko	pravítko	k1gNnSc1	pravítko
</sent>
<sent id="5154">
registrační	registrační	k2eAgInSc4d1	registrační
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="5155">
sklon	sklon	k1gInSc1	sklon
záměry	záměra	k1gFnSc2	záměra
</sent>
<sent id="5156">
staničník	staničník	k1gInSc1	staničník
</sent>
<sent id="5158">
technická	technický	k2eAgFnSc1d1	technická
zpráva	zpráva	k1gFnSc1	zpráva
</sent>
<sent id="5159">
topografická	topografický	k2eAgFnSc1d1	topografická
revize	revize	k1gFnSc1	revize
</sent>
<sent id="5160">
trasa	trasa	k1gFnSc1	trasa
vedení	vedení	k1gNnSc2	vedení
</sent>
<sent id="5161">
triangulace	triangulace	k1gFnSc1	triangulace
</sent>
<sent id="5162">
triangulátor	triangulátor	k1gInSc1	triangulátor
</sent>
<sent id="5163">
trigonometrická	trigonometrický	k2eAgFnSc1d1	trigonometrická
výška	výška	k1gFnSc1	výška
</sent>
<sent id="5164">
uzávěr	uzávěr	k1gInSc1	uzávěr
pořadu	pořad	k1gInSc2	pořad
</sent>
<sent id="5165">
vektorizace	vektorizace	k1gFnSc1	vektorizace
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5166">
vyrovnání	vyrovnání	k1gNnSc2	vyrovnání
hranic	hranice	k1gFnPc2	hranice
</sent>
<sent id="5167">
vyrovnání	vyrovnání	k1gNnSc2	vyrovnání
na	na	k7c6	
stanovisku	stanovisko	k1gNnSc6	stanovisko
</sent>
<sent id="5168">
vyrovnání	vyrovnání	k1gNnSc6	vyrovnání
pořadu	pořad	k1gInSc2	pořad
</sent>
<sent id="5169">
vystředění	vystředění	k1gNnSc6	vystředění
zápisníku	zápisník	k1gInSc2	zápisník
</sent>
<sent id="5170">
výška	výška	k1gFnSc1	výška
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="5171">
vytyčovací	vytyčovací	k2eAgInSc4d1	vytyčovací
hranol	hranol	k1gInSc4	hranol
</sent>
<sent id="5173">
zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
</sent>
<sent id="5174">
zobrazovací	zobrazovací	k2eAgInSc4d1	zobrazovací
pravítko	pravítko	k1gNnSc1	pravítko
</sent>
<sent id="5176">
ISPROFIN	ISPROFIN	kA	
</sent>
<sent id="5177">
IZS	IZS	kA	
</sent>
<sent id="5178">
LPF	LPF	kA	
</sent>
<sent id="5179">
PPP	PPP	kA	
</sent>
<sent id="5180">
ÚOZI	ÚOZI	kA	
</sent>
<sent id="5181">
ÚZSVM	ÚZSVM	kA	
</sent>
<sent id="5183">
formát	formát	k1gInSc1	formát
papíru	papír	k1gInSc2	papír
</sent>
<sent id="5184">
fotosazba	fotosazba	k1gFnSc1	fotosazba
</sent>
<sent id="5185">
fotosázecí	fotosázecí	k2eAgInSc1d1	fotosázecí
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="5186">
fotosázecí	fotosázecí	k2eAgInSc1d1	fotosázecí
stroj	stroj	k1gInSc1	stroj
</sent>
<sent id="5188">
globografie	globografie	k1gFnSc1	globografie
</sent>
<sent id="5190">
harmonie	harmonie	k1gFnSc2	harmonie
barev	barva	k1gFnPc2	barva
(	(	kIx(	
<g/>
na	na	k7c6	
mapě	mapa	k1gFnSc6	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="5191">
historické	historický	k2eAgNnSc4d1	historické
jméno	jméno	k1gNnSc4	jméno
</sent>
<sent id="5194">
hodnocení	hodnocení	k1gNnSc6	hodnocení
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5195">
horizontála	horizontála	k1gFnSc1	horizontála
</sent>
<sent id="5198">
imprimatur	imprimatur	k1gNnSc2	imprimatur
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="5199">
inventarizace	inventarizace	k1gFnSc2	inventarizace
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5200">
itinerář	itinerář	k1gInSc1	itinerář
</sent>
<sent id="5201">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="5202">
absolutní	absolutní	k2eAgFnSc1d1	absolutní
orientace	orientace	k1gFnSc1	orientace
</sent>
<sent id="5203">
absolutní	absolutní	k2eAgFnSc1d1	absolutní
výška	výška	k1gFnSc1	výška
letu	let	k1gInSc3	let
</sent>
<sent id="5204">
analogový	analogový	k2eAgInSc4d1	analogový
vyhodnocovací	vyhodnocovací	k2eAgInSc4d1	vyhodnocovací
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="5205">
analytický	analytický	k2eAgInSc1d1	analytický
vyhodnocovací	vyhodnocovací	k2eAgInSc1d1	vyhodnocovací
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="5206">
decentrační	decentrační	k2eAgFnSc1d1	decentrační
distorze	distorze	k1gFnSc1	distorze
objektivu	objektiv	k1gInSc2	objektiv
</sent>
<sent id="5207">
digitální	digitální	k2eAgFnSc1d1	digitální
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="5208">
distorze	distorze	k1gFnSc1	distorze
objektivu	objektiv	k1gInSc2	objektiv
</sent>
<sent id="5209">
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
(	(	kIx(	
<g/>
stereometrická	stereometrický	k2eAgFnSc1d1	stereometrická
<g/>
)	)	kIx)	
komora	komora	k1gFnSc1	komora
</sent>
<sent id="5210">
fototeodolit	fototeodolit	k1gInSc1	fototeodolit
</sent>
<sent id="5212">
hlavní	hlavní	k2eAgInSc1d1	hlavní
snímkový	snímkový	k2eAgInSc1d1	snímkový
bod	bod	k1gInSc1	bod
</sent>
<sent id="5213">
obrazová	obrazový	k2eAgFnSc1d1	obrazová
korelace	korelace	k1gFnSc1	korelace
</sent>
<sent id="5214">
kalibrace	kalibrace	k1gFnSc1	kalibrace
kamery	kamera	k1gFnSc2	kamera
</sent>
<sent id="5215">
konstanta	konstanta	k1gFnSc1	konstanta
kamery	kamera	k1gFnSc2	kamera
</sent>
<sent id="5216">
konvergentní	konvergentní	k2eAgInSc4d1	konvergentní
případ	případ	k1gInSc4	případ
</sent>
<sent id="5217">
letecký	letecký	k2eAgInSc1d1	letecký
měřický	měřický	k2eAgInSc1d1	měřický
snímek	snímek	k1gInSc1	snímek
</sent>
<sent id="5218">
měřická	měřický	k2eAgFnSc1d1	měřická
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="5219">
metoda	metoda	k1gFnSc1	metoda
časové	časový	k2eAgFnSc2d1	časová
základny	základna	k1gFnSc2	základna
</sent>
<sent id="5220">
ortofotosnímek	ortofotosnímek	k1gInSc1	ortofotosnímek
</sent>
<sent id="5221">
ortofotomapa	ortofotomapa	k1gFnSc1	ortofotomapa
</sent>
<sent id="5222">
osa	osa	k1gFnSc1	osa
záběru	záběr	k1gInSc2	záběr
</sent>
<sent id="5223">
podélný	podélný	k2eAgInSc4d1	podélný
překryt	překryt	k2eAgInSc4d1	překryt
leteckých	letecký	k2eAgInPc2d1	letecký
měřických	měřický	k2eAgInPc2d1	měřický
snímků	snímek	k1gInPc2	snímek
</sent>
<sent id="5224">
příčný	příčný	k2eAgInSc4d1	příčný
překryt	překryt	k2eAgInSc4d1	překryt
leteckých	letecký	k2eAgInPc2d1	letecký
měřických	měřický	k2eAgInPc2d1	měřický
snímků	snímek	k1gInPc2	snímek
</sent>
<sent id="5225">
relativní	relativní	k2eAgFnSc1d1	relativní
orientace	orientace	k1gFnSc1	orientace
</sent>
<sent id="5226">
relativní	relativní	k2eAgFnSc1d1	relativní
výška	výška	k1gFnSc1	výška
letu	let	k1gInSc3	let
</sent>
<sent id="5227">
rozlišovací	rozlišovací	k2eAgFnSc4d1	rozlišovací
schopnost	schopnost	k1gFnSc4	schopnost
fotografického	fotografický	k2eAgInSc2d1	fotografický
materiálu	materiál	k1gInSc2	materiál
</sent>
<sent id="5228">
snímkové	snímkový	k2eAgFnPc4d1	snímková
souřadnice	souřadnice	k1gFnPc4	souřadnice
</sent>
<sent id="5229">
stereoskop	stereoskop	k1gInSc1	stereoskop
</sent>
<sent id="5230">
střed	střed	k1gInSc1	střed
snímku	snímek	k1gInSc2	snímek
</sent>
<sent id="5231">
svislý	svislý	k2eAgInSc1d1	svislý
letecký	letecký	k2eAgInSc1d1	letecký
snímek	snímek	k1gInSc1	snímek
</sent>
<sent id="5232">
šikmý	šikmý	k2eAgInSc1d1	šikmý
letecký	letecký	k2eAgInSc1d1	letecký
snímek	snímek	k1gInSc1	snímek
</sent>
<sent id="5233">
autorská	autorský	k2eAgFnSc1d1	autorská
korektura	korektura	k1gFnSc1	korektura
(	(	kIx(	
<g/>
sazby	sazba	k1gFnPc1	sazba
<g/>
)	)	kIx)	
</sent>
<sent id="5234">
domácí	domácí	k2eAgFnSc1d1	domácí
korektura	korektura	k1gFnSc1	korektura
(	(	kIx(	
<g/>
sazby	sazba	k1gFnPc1	sazba
<g/>
)	)	kIx)	
</sent>
<sent id="5235">
jazyk	jazyk	k1gInSc1	jazyk
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5236">
jednobarevný	jednobarevný	k2eAgInSc4d1	jednobarevný
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="5237">
kartograf	kartograf	k1gMnSc1	kartograf
</sent>
<sent id="5238">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
informace	informace	k1gFnSc1	informace
</sent>
<sent id="5239">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
sémiologie	sémiologie	k1gFnSc1	sémiologie
</sent>
<sent id="5240">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
směrnice	směrnice	k1gFnSc1	směrnice
</sent>
<sent id="5241">
Česká	český	k2eAgFnSc1d1	Česká
kartografická	kartografický	k2eAgFnSc1d1	kartografická
společnost	společnost	k1gFnSc1	společnost
</sent>
<sent id="5243">
kartografické	kartografický	k2eAgNnSc4d1	kartografické
písmo	písmo	k1gNnSc4	písmo
</sent>
<sent id="5244">
kartografické	kartografický	k2eAgInPc4d1	kartografický
vyjadřovací	vyjadřovací	k2eAgInPc4d1	vyjadřovací
prostředky	prostředek	k1gInPc4	prostředek
</sent>
<sent id="5245">
kartografický	kartografický	k2eAgMnSc1d1	kartografický
kreslič	kreslič	k1gMnSc1	kreslič
</sent>
<sent id="5246">
kartolitograf	kartolitograf	k1gInSc1	kartolitograf
</sent>
<sent id="5247">
kartologie	kartologie	k1gFnSc1	kartologie
</sent>
<sent id="5248">
karton	karton	k1gInSc1	karton
</sent>
<sent id="5249">
knihtisk	knihtisk	k1gInSc1	knihtisk
</sent>
<sent id="5250">
kolorování	kolorování	k1gNnSc4	kolorování
</sent>
<sent id="5251">
koncept	koncept	k1gInSc1	koncept
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5252">
kontaktní	kontaktní	k2eAgInSc4d1	kontaktní
kopírování	kopírování	k1gNnSc1	kopírování
</sent>
<sent id="5253">
kontura	kontura	k1gFnSc1	kontura
</sent>
<sent id="5254">
koordinátograf	koordinátograf	k1gInSc1	koordinátograf
</sent>
<sent id="5255">
kopírovací	kopírovací	k2eAgFnSc1d1	kopírovací
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="5256">
korektura	korektura	k1gFnSc1	korektura
</sent>
<sent id="5257">
kreslení	kreslení	k1gNnSc2	kreslení
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5258">
krycí	krycí	k2eAgFnSc1d1	krycí
barva	barva	k1gFnSc1	barva
</sent>
<sent id="5259">
křivítko	křivítko	k1gNnSc4	křivítko
</sent>
<sent id="5260">
kurzíva	kurzíva	k1gFnSc1	kurzíva
</sent>
<sent id="5261">
kyanografie	kyanografie	k1gFnSc1	kyanografie
</sent>
<sent id="5262">
levosklonné	levosklonný	k2eAgNnSc4d1	levosklonný
písmo	písmo	k1gNnSc4	písmo
</sent>
<sent id="5263">
stránková	stránkový	k2eAgFnSc1d1	stránková
korektura	korektura	k1gFnSc1	korektura
(	(	kIx(	
<g/>
sazby	sazba	k1gFnPc1	sazba
<g/>
)	)	kIx)	
</sent>
<sent id="5264">
kopírovací	kopírovací	k2eAgInSc4d1	kopírovací
rám	rám	k1gInSc4	rám
</sent>
<sent id="5265">
aditivní	aditivní	k2eAgInSc4d1	aditivní
míšení	míšení	k1gNnSc1	míšení
barev	barva	k1gFnPc2	barva
</sent>
<sent id="5267">
areálová	areálový	k2eAgFnSc1d1	areálová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="5268">
argentografie	argentografie	k1gFnSc1	argentografie
</sent>
<sent id="5269">
atlas	atlas	k1gInSc1	atlas
světa	svět	k1gInSc2	svět
</sent>
<sent id="5270">
autotypický	autotypický	k2eAgInSc4d1	autotypický
bod	bod	k1gInSc4	bod
</sent>
<sent id="5271">
barevná	barevný	k2eAgFnSc1d1	barevná
hypsometrie	hypsometrie	k1gFnSc1	hypsometrie
</sent>
<sent id="5272">
bimetalická	bimetalický	k2eAgFnSc1d1	bimetalická
deska	deska	k1gFnSc1	deska
</sent>
<sent id="5273">
copyright	copyright	k1gInSc1	copyright
(	(	kIx(	
<g/>
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
<g/>
)	)	kIx)	
</sent>
<sent id="5274">
diazografický	diazografický	k2eAgInSc4d1	diazografický
film	film	k1gInSc4	film
</sent>
<sent id="5275">
dotykový	dotykový	k2eAgInSc4d1	dotykový
poledník	poledník	k1gInSc4	poledník
</sent>
<sent id="5276">
druh	druh	k1gInSc1	druh
písma	písmo	k1gNnSc2	písmo
</sent>
<sent id="5277">
elektrografická	elektrografický	k2eAgFnSc1d1	elektrografický
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="5278">
hnědokopie	hnědokopie	k1gFnSc1	hnědokopie
</sent>
<sent id="5279">
hypsografie	hypsografie	k1gFnSc1	hypsografie
</sent>
<sent id="5280">
inverzní	inverzní	k2eAgInSc4d1	inverzní
film	film	k1gInSc4	film
</sent>
<sent id="5281">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
komunikace	komunikace	k1gFnSc1	komunikace
</sent>
<sent id="5282">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
</sent>
<sent id="5283">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
šířka	šířka	k1gFnSc1	šířka
</sent>
<sent id="5284">
kartografické	kartografický	k2eAgNnSc1d1	kartografické
zkreslení	zkreslení	k1gNnSc1	zkreslení
</sent>
<sent id="5285">
kartografický	kartografický	k2eAgInSc1d1	kartografický
pól	pól	k1gInSc1	pól
</sent>
<sent id="5286">
kartografický	kartografický	k2eAgInSc4d1	kartografický
poledník	poledník	k1gInSc4	poledník
</sent>
<sent id="5287">
kartografický	kartografický	k2eAgInSc4d1	kartografický
rovník	rovník	k1gInSc4	rovník
</sent>
<sent id="5288">
kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="5289">
kreslicí	kreslicí	k2eAgFnSc1d1	kreslicí
hlava	hlava	k1gFnSc1	hlava
</sent>
<sent id="5290">
kyanografická	kyanografický	k2eAgFnSc1d1	kyanografický
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="5291">
laminování	laminování	k1gNnPc2	laminování
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5292">
legenda	legenda	k1gFnSc1	legenda
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5293">
plošné	plošný	k2eAgNnSc1d1	plošné
zkreslení	zkreslení	k1gNnSc1	zkreslení
</sent>
<sent id="5294">
rycí	rycí	k2eAgFnSc1d1	rycí
hlava	hlava	k1gFnSc1	hlava
</sent>
<sent id="5295">
sloupcová	sloupcový	k2eAgFnSc1d1	sloupcová
korektura	korektura	k1gFnSc1	korektura
</sent>
<sent id="5296">
světelná	světelný	k2eAgFnSc1d1	světelná
hlava	hlava	k1gFnSc1	hlava
</sent>
<sent id="5297">
úhlové	úhlový	k2eAgNnSc1d1	úhlové
zkreslení	zkreslení	k1gNnSc1	zkreslení
</sent>
<sent id="5687">
SKPOS	SKPOS	kA	
</sent>
<sent id="5688">
trojnožka	trojnožka	k1gFnSc1	trojnožka
</sent>
<sent id="5301">
dodavatel	dodavatel	k1gMnSc1	dodavatel
informací	informace	k1gFnPc2	informace
</sent>
<sent id="5302">
gigabajt	gigabajt	k1gInSc1	gigabajt
</sent>
<sent id="5304">
klepnutí	klepnutí	k1gNnSc6	klepnutí
</sent>
<sent id="5307">
kryptologie	kryptologie	k1gFnSc1	kryptologie
</sent>
<sent id="5308">
linka	linka	k1gFnSc1	linka
</sent>
<sent id="5309">
magnetické	magnetický	k2eAgNnSc1d1	magnetické
záznamové	záznamový	k2eAgNnSc1d1	záznamové
médium	médium	k1gNnSc1	médium
</sent>
<sent id="5311">
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
norma	norma	k1gFnSc1	norma
</sent>
<sent id="5312">
nábojově	nábojově	k6eAd1	
vázaný	vázaný	k2eAgInSc1d1	vázaný
prvek	prvek	k1gInSc1	prvek
</sent>
<sent id="5313">
národní	národní	k2eAgFnSc1d1	národní
norma	norma	k1gFnSc1	norma
</sent>
<sent id="5314">
norma	norma	k1gFnSc1	norma
</sent>
<sent id="5316">
normativní	normativní	k2eAgInSc4d1	normativní
dokument	dokument	k1gInSc4	dokument
</sent>
<sent id="5318">
objektově	objektově	k6eAd1	
orientované	orientovaný	k2eAgNnSc4d1	orientované
programování	programování	k1gNnSc4	programování
</sent>
<sent id="5320">
poklepání	poklepání	k1gNnSc6	poklepání
</sent>
<sent id="5321">
portál	portál	k1gInSc1	portál
</sent>
<sent id="5322">
poskytovatel	poskytovatel	k1gMnSc1	poskytovatel
služeb	služba	k1gFnPc2	služba
</sent>
<sent id="5324">
programové	programový	k2eAgNnSc4d1	programové
vybavení	vybavení	k1gNnSc4	vybavení
</sent>
<sent id="5325">
regionální	regionální	k2eAgFnSc1d1	regionální
norma	norma	k1gFnSc1	norma
</sent>
<sent id="5326">
rozhraní	rozhraní	k1gNnSc4	rozhraní
</sent>
<sent id="5327">
sdílení	sdílení	k1gNnSc4	sdílení
</sent>
<sent id="5329">
technická	technický	k2eAgFnSc1d1	technická
specifikace	specifikace	k1gFnSc1	specifikace
</sent>
<sent id="5330">
technické	technický	k2eAgInPc4d1	technický
prostředky	prostředek	k1gInPc4	prostředek
</sent>
<sent id="5331">
terabajt	terabajt	k1gInSc1	terabajt
</sent>
<sent id="5332">
topologický	topologický	k2eAgInSc4d1	topologický
vztah	vztah	k1gInSc4	vztah
</sent>
<sent id="5333">
webový	webový	k2eAgInSc1d1	webový
server	server	k1gInSc1	server
</sent>
<sent id="5334">
digitalizovaná	digitalizovaný	k2eAgNnPc4d1	digitalizované
obrazová	obrazový	k2eAgNnPc4d1	obrazové
data	datum	k1gNnPc4	datum
</sent>
<sent id="7169">
č.	č.	k?	
<g/>
p.	p.	k?	
</sent>
<sent id="7170">
p.	p.	k?	
<g/>
č.	č.	k?	
</sent>
<sent id="5339">
technická	technický	k2eAgFnSc1d1	technická
podpora	podpora	k1gFnSc1	podpora
</sent>
<sent id="5340">
geografický	geografický	k2eAgInSc1d1	geografický
značkovací	značkovací	k2eAgInSc1d1	značkovací
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	
<g/>
GML	GML	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5341">
Globální	globální	k2eAgFnSc1d1	globální
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
prostorových	prostorový	k2eAgNnPc2d1	prostorové
dat	datum	k1gNnPc2	datum
(	(	kIx(	
<g/>
GSDI	GSDI	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5342">
hypertextový	hypertextový	k2eAgInSc4d1	hypertextový
přenosový	přenosový	k2eAgInSc4d1	přenosový
protokol	protokol	k1gInSc4	protokol
(	(	kIx(	
<g/>
HTTP	HTTP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5343">
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
</sent>
<sent id="5344">
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
pro	pro	k7c4	
prostorové	prostorový	k2eAgFnPc4d1	prostorová
informace	informace	k1gFnPc4	informace
v	v	k7c6	
Evropském	evropský	k2eAgNnSc6d1	Evropské
společenství	společenství	k1gNnSc6	společenství
(	(	kIx(	
<g/>
INSPIRE	INSPIRE	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5345">
Národní	národní	k2eAgFnSc1d1	národní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
prostorových	prostorový	k2eAgNnPc2d1	prostorové
dat	datum	k1gNnPc2	datum
(	(	kIx(	
<g/>
NSDI	NSDI	kA	
<g/>
)	)	kIx)	
-	-	kIx~	
USA	USA	kA	
</sent>
<sent id="5346">
rozšiřitelný	rozšiřitelný	k2eAgInSc4d1	rozšiřitelný
značkovací	značkovací	k2eAgInSc4d1	značkovací
jazyk	jazyk	k1gInSc4	jazyk
(	(	kIx(	
<g/>
XML	XML	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5347">
webová	webový	k2eAgFnSc1d1	webová
mapová	mapový	k2eAgFnSc1d1	mapová
služba	služba	k1gFnSc1	služba
(	(	kIx(	
<g/>
WMS	WMS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5348">
webová	webový	k2eAgFnSc1d1	webová
služba	služba	k1gFnSc1	služba
pokrytí	pokrytí	k1gNnSc4	pokrytí
(	(	kIx(	
<g/>
WCS	WCS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5349">
webová	webový	k2eAgFnSc1d1	webová
služba	služba	k1gFnSc1	služba
vzhledů	vzhled	k1gInPc2	vzhled
</sent>
<sent id="5350">
webové	webový	k2eAgFnSc2d1	webová
služby	služba	k1gFnSc2	služba
</sent>
<sent id="5351">
družicová	družicový	k2eAgFnSc1d1	družicová
geodézie	geodézie	k1gFnSc1	geodézie
</sent>
<sent id="5352">
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
geodézie	geodézie	k1gFnSc1	geodézie
</sent>
<sent id="5353">
geocentrické	geocentrický	k2eAgFnPc4d1	geocentrická
souřadnice	souřadnice	k1gFnPc4	souřadnice
</sent>
<sent id="5354">
geodetická	geodetický	k2eAgFnSc1d1	geodetická
délka	délka	k1gFnSc1	délka
</sent>
<sent id="5355">
geodetická	geodetický	k2eAgFnSc1d1	geodetická
šířka	šířka	k1gFnSc1	šířka
</sent>
<sent id="5356">
geodetická	geodetický	k2eAgFnSc1d1	geodetická
výška	výška	k1gFnSc1	výška
</sent>
<sent id="5357">
geodetické	geodetický	k2eAgFnPc4d1	geodetická
souřadnice	souřadnice	k1gFnPc4	souřadnice
</sent>
<sent id="5359">
geoid	geoid	k1gInSc1	geoid
</sent>
<sent id="5361">
gravimetr	gravimetr	k1gInSc1	gravimetr
</sent>
<sent id="5362">
gravimetrie	gravimetrie	k1gFnSc1	gravimetrie
</sent>
<sent id="5363">
gravitační	gravitační	k2eAgInSc4d1	gravitační
zrychlení	zrychlení	k1gNnSc1	zrychlení
</sent>
<sent id="5364">
Helmertova	Helmertův	k2eAgFnSc1d1	Helmertova
ortometrická	ortometrický	k2eAgFnSc1d1	ortometrický
výška	výška	k1gFnSc1	výška
</sent>
<sent id="5365">
hladinová	hladinový	k2eAgFnSc1d1	hladinová
plocha	plocha	k1gFnSc1	plocha
</sent>
<sent id="5366">
hladinový	hladinový	k2eAgInSc4d1	hladinový
elipsoid	elipsoid	k1gInSc4	elipsoid
</sent>
<sent id="5367">
Krasovského	Krasovský	k2eAgInSc2d1	Krasovský
elipsoid	elipsoid	k1gInSc4	elipsoid
</sent>
<sent id="5368">
kvazigeoid	kvazigeoid	k1gInSc1	kvazigeoid
</sent>
<sent id="5369">
normální	normální	k2eAgFnSc1d1	normální
ortometrická	ortometrický	k2eAgFnSc1d1	ortometrický
výška	výška	k1gFnSc1	výška
</sent>
<sent id="5370">
normální	normální	k2eAgMnSc1d1	normální
tíhové	tíhový	k2eAgNnSc4d1	tíhové
pole	pole	k1gNnSc4	pole
</sent>
<sent id="5371">
normální	normální	k2eAgFnSc4d1	normální
tíhové	tíhový	k2eAgNnSc1d1	tíhové
zrychlení	zrychlení	k1gNnSc1	zrychlení
</sent>
<sent id="5372">
normální	normální	k2eAgFnSc1d1	normální
výška	výška	k1gFnSc1	výška
</sent>
<sent id="5373">
odstředivé	odstředivý	k2eAgNnSc1d1	odstředivé
zrychlení	zrychlení	k1gNnSc1	zrychlení
</sent>
<sent id="5374">
ortometrická	ortometrický	k2eAgFnSc1d1	ortometrický
výška	výška	k1gFnSc1	výška
</sent>
<sent id="5375">
recentní	recentní	k2eAgInPc1d1	recentní
pohyby	pohyb	k1gInPc1	pohyb
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
</sent>
<sent id="5376">
referenční	referenční	k2eAgInSc4d1	referenční
elipsoid	elipsoid	k1gInSc4	elipsoid
</sent>
<sent id="5377">
siločára	siločára	k1gFnSc1	siločára
</sent>
<sent id="5378">
slapová	slapový	k2eAgFnSc1d1	slapová
síla	síla	k1gFnSc1	síla
</sent>
<sent id="5379">
tíhové	tíhový	k2eAgNnSc1d1	tíhové
zrychlení	zrychlení	k1gNnSc1	zrychlení
</sent>
<sent id="5380">
tížnice	tížnice	k1gFnSc1	tížnice
</sent>
<sent id="5381">
tížnicová	tížnicový	k2eAgFnSc1d1	tížnicový
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="5382">
topografická	topografický	k2eAgFnSc1d1	topografická
redukce	redukce	k1gFnSc1	redukce
</sent>
<sent id="5383">
topografické	topografický	k2eAgFnSc2d1	topografická
hmoty	hmota	k1gFnSc2	hmota
</sent>
<sent id="5384">
výška	výška	k1gFnSc1	výška
geoidu	geoid	k1gInSc2	geoid
</sent>
<sent id="5385">
výšková	výškový	k2eAgFnSc1d1	výšková
anomálie	anomálie	k1gFnSc1	anomálie
</sent>
<sent id="5386">
zrychlení	zrychlení	k1gNnSc4	zrychlení
</sent>
<sent id="5387">
LLS	LLS	kA	
</sent>
<sent id="5388">
absolutní	absolutní	k2eAgInSc1d1	absolutní
posun	posun	k1gInSc1	posun
</sent>
<sent id="5389">
celkové	celkový	k2eAgNnSc4d1	celkové
přetvoření	přetvoření	k1gNnSc4	přetvoření
</sent>
<sent id="5390">
celkový	celkový	k2eAgInSc1d1	celkový
posun	posun	k1gInSc1	posun
</sent>
<sent id="5786">
dolní	dolní	k2eAgFnSc1d1	dolní
mezní	mezní	k2eAgFnSc1d1	mezní
vytyčovací	vytyčovací	k2eAgFnSc1d1	vytyčovací
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="5393">
etapové	etapový	k2eAgNnSc4d1	etapové
měření	měření	k1gNnSc4	měření
</sent>
<sent id="5394">
geodetická	geodetický	k2eAgFnSc1d1	geodetická
část	část	k1gFnSc1	část
dokumentace	dokumentace	k1gFnSc1	dokumentace
skutečného	skutečný	k2eAgNnSc2d1	skutečné
provedení	provedení	k1gNnSc2	provedení
stavby	stavba	k1gFnSc2	stavba
</sent>
<sent id="5395">
geodetická	geodetický	k2eAgFnSc1d1	geodetická
mikrosíť	mikrosíť	k1gFnSc1	mikrosíť
</sent>
<sent id="5396">
geodetické	geodetický	k2eAgFnSc2d1	geodetická
měření	měření	k1gNnSc4	měření
pro	pro	k7c4	
dokumentaci	dokumentace	k1gFnSc4	dokumentace
skutečného	skutečný	k2eAgNnSc2d1	skutečné
provedení	provedení	k1gNnSc2	provedení
stavby	stavba	k1gFnSc2	stavba
</sent>
<sent id="5397">
hlavní	hlavní	k2eAgInSc4d1	hlavní
bod	bod	k1gInSc4	bod
oblouku	oblouk	k1gInSc2	oblouk
</sent>
<sent id="5398">
hlavní	hlavní	k2eAgInSc1d1	hlavní
bod	bod	k1gInSc1	bod
trasy	trasa	k1gFnSc2	trasa
</sent>
<sent id="5399">
hlavní	hlavní	k2eAgFnSc1d1	hlavní
osa	osa	k1gFnSc1	osa
</sent>
<sent id="5400">
hlavní	hlavní	k2eAgFnSc1d1	hlavní
polohová	polohový	k2eAgFnSc1d1	polohová
čára	čára	k1gFnSc1	čára
</sent>
<sent id="5401">
hlavní	hlavní	k2eAgFnSc1d1	hlavní
stavební	stavební	k2eAgFnSc1d1	stavební
osa	osa	k1gFnSc1	osa
</sent>
<sent id="5402">
horní	horní	k2eAgFnSc1d1	horní
(	(	kIx(	
<g/>
mezní	mezní	k2eAgFnSc1d1	mezní
<g/>
)	)	kIx)	
vytyčovací	vytyčovací	k2eAgFnSc1d1	vytyčovací
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="5403">
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
bod	bod	k1gInSc4	bod
osy	osa	k1gFnSc2	osa
</sent>
<sent id="5482">
koruna	koruna	k1gFnSc1	koruna
silniční	silniční	k2eAgFnSc2d1	silniční
komunikace	komunikace	k1gFnSc2	komunikace
</sent>
<sent id="5405">
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
měření	měření	k1gNnSc1	měření
geometrických	geometrický	k2eAgInPc2d1	geometrický
parametrů	parametr	k1gInPc2	parametr
</sent>
<sent id="5406">
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
měření	měření	k1gNnSc1	měření
pro	pro	k7c4	
dokumentaci	dokumentace	k1gFnSc4	dokumentace
skutečného	skutečný	k2eAgNnSc2d1	skutečné
provedení	provedení	k1gNnSc2	provedení
stavby	stavba	k1gFnSc2	stavba
</sent>
<sent id="5407">
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
značka	značka	k1gFnSc1	značka
</sent>
<sent id="5408">
liniový	liniový	k2eAgInSc1d1	liniový
stavební	stavební	k2eAgInSc1d1	stavební
objekt	objekt	k1gInSc1	objekt
</sent>
<sent id="5409">
měření	měření	k1gNnSc2	měření
opásáním	opásání	k1gNnPc3	opásání
</sent>
<sent id="5410">
měření	měření	k1gNnPc4	měření
posunů	posun	k1gInPc2	posun
a	a	k8xC	
přetvoření	přetvoření	k1gNnPc2	přetvoření
</sent>
<sent id="5411">
měřicí	měřicí	k2eAgFnSc1d1	měřicí
základna	základna	k1gFnSc1	základna
</sent>
<sent id="5412">
mikrosíť	mikrosíť	k1gFnSc1	mikrosíť
</sent>
<sent id="5413">
místní	místní	k2eAgInSc1d1	místní
výškový	výškový	k2eAgInSc1d1	výškový
systém	systém	k1gInSc1	systém
</sent>
<sent id="5414">
odsazená	odsazený	k2eAgFnSc1d1	odsazená
osa	osa	k1gFnSc1	osa
</sent>
<sent id="5416">
osa	osa	k1gFnSc1	osa
koleje	kolej	k1gFnSc2	kolej
</sent>
<sent id="5417">
osa	osa	k1gFnSc1	osa
kolejnice	kolejnice	k1gFnSc1	kolejnice
</sent>
<sent id="5418">
osa	osa	k1gFnSc1	osa
liniového	liniový	k2eAgInSc2d1	liniový
stavebního	stavební	k2eAgInSc2d1	stavební
objektu	objekt	k1gInSc2	objekt
</sent>
<sent id="5419">
osa	osa	k1gFnSc1	osa
mostu	most	k1gInSc2	most
</sent>
<sent id="5420">
osa	osa	k1gFnSc1	osa
přehrady	přehrada	k1gFnSc2	přehrada
</sent>
<sent id="5422">
plošný	plošný	k2eAgInSc1d1	plošný
stavební	stavební	k2eAgInSc1d1	stavební
objekt	objekt	k1gInSc1	objekt
</sent>
<sent id="5423">
podélný	podélný	k2eAgInSc1d1	podélný
posun	posun	k1gInSc1	posun
</sent>
<sent id="5424">
podrobné	podrobný	k2eAgInPc1d1	podrobný
vytyčování	vytyčování	k1gNnSc4	vytyčování
objektu	objekt	k1gInSc2	objekt
</sent>
<sent id="5425">
pootočení	pootočení	k1gNnSc4	pootočení
</sent>
<sent id="5426">
posun	posun	k1gInSc1	posun
</sent>
<sent id="5427">
pravoúhelníková	pravoúhelníkový	k2eAgFnSc1d1	pravoúhelníkový
vytyčovací	vytyčovací	k2eAgFnSc1d1	vytyčovací
síť	síť	k1gFnSc1	síť
</sent>
<sent id="5428">
projektová	projektový	k2eAgFnSc1d1	projektová
hodnota	hodnota	k1gFnSc1	hodnota
vytyčované	vytyčovaný	k2eAgFnSc2d1	vytyčovaná
veličiny	veličina	k1gFnSc2	veličina
</sent>
<sent id="5429">
provažovaná	provažovaný	k2eAgFnSc1d1	provažovaný
úsečka	úsečka	k1gFnSc1	úsečka
</sent>
<sent id="5430">
průhyb	průhyb	k1gInSc1	průhyb
</sent>
<sent id="5431">
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
geodézie	geodézie	k1gFnSc1	geodézie
</sent>
<sent id="5432">
prvek	prvek	k1gInSc1	prvek
prostorové	prostorový	k2eAgFnSc2d1	prostorová
polohy	poloha	k1gFnSc2	poloha
</sent>
<sent id="5433">
přehled	přehled	k1gInSc1	přehled
vytyčovací	vytyčovací	k2eAgFnSc2d1	vytyčovací
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="5434">
přetvoření	přetvoření	k1gNnSc4	přetvoření
</sent>
<sent id="5435">
příčný	příčný	k2eAgInSc1d1	příčný
posun	posun	k1gInSc1	posun
</sent>
<sent id="5436">
půlící	půlící	k2eAgInSc4d1	půlící
bod	bod	k1gInSc4	bod
oblouku	oblouk	k1gInSc2	oblouk
</sent>
<sent id="5437">
rektifikační	rektifikační	k2eAgFnSc1d1	rektifikační
hodnota	hodnota	k1gFnSc1	hodnota
</sent>
<sent id="5438">
relativní	relativní	k2eAgInSc1d1	relativní
posun	posun	k1gInSc1	posun
</sent>
<sent id="5439">
sedání	sedání	k1gNnSc4	sedání
</sent>
<sent id="5440">
sednutí	sednutí	k1gNnSc4	sednutí
</sent>
<sent id="5441">
skutečná	skutečný	k2eAgFnSc1d1	skutečná
hodnota	hodnota	k1gFnSc1	hodnota
geometrického	geometrický	k2eAgInSc2d1	geometrický
parametru	parametr	k1gInSc2	parametr
</sent>
<sent id="5442">
skutečná	skutečný	k2eAgFnSc1d1	skutečná
hodnota	hodnota	k1gFnSc1	hodnota
veličiny	veličina	k1gFnSc2	veličina
</sent>
<sent id="5443">
skutečná	skutečný	k2eAgFnSc1d1	skutečná
hodnota	hodnota	k1gFnSc1	hodnota
vytyčené	vytyčený	k2eAgFnSc2d1	vytyčená
veličiny	veličina	k1gFnSc2	veličina
</sent>
<sent id="5444">
směrná	směrný	k2eAgFnSc1d1	směrná
hodnota	hodnota	k1gFnSc1	hodnota
vytyčené	vytyčený	k2eAgFnSc2d1	vytyčená
veličiny	veličina	k1gFnSc2	veličina
</sent>
<sent id="5445">
stavební	stavební	k2eAgFnSc1d1	stavební
geodézie	geodézie	k1gFnSc1	geodézie
</sent>
<sent id="5446">
stavební	stavební	k2eAgInSc4d1	stavební
objekt	objekt	k1gInSc4	objekt
s	s	k7c7	
prostorovou	prostorový	k2eAgFnSc7d1	prostorová
skladbou	skladba	k1gFnSc7	skladba
</sent>
<sent id="5447">
světelná	světelný	k2eAgFnSc1d1	světelná
rovina	rovina	k1gFnSc1	rovina
</sent>
<sent id="5448">
světelná	světelný	k2eAgFnSc1d1	světelná
stopa	stopa	k1gFnSc1	stopa
</sent>
<sent id="5449">
trasa	trasa	k1gFnSc1	trasa
</sent>
<sent id="5451">
usměrňovací	usměrňovací	k2eAgInSc4d1	usměrňovací
měření	měření	k1gNnSc1	měření
</sent>
<sent id="5453">
vyhledávání	vyhledávání	k1gNnPc4	vyhledávání
podzemního	podzemní	k2eAgNnSc2d1	podzemní
vedení	vedení	k1gNnSc2	vedení
</sent>
<sent id="5455">
vytyčovací	vytyčovací	k2eAgInSc4d1	vytyčovací
prvek	prvek	k1gInSc4	prvek
</sent>
<sent id="5456">
vytyčovací	vytyčovací	k2eAgFnSc1d1	vytyčovací
přímka	přímka	k1gFnSc1	přímka
</sent>
<sent id="5457">
vytyčovací	vytyčovací	k2eAgFnSc4d1	vytyčovací
síť	síť	k1gFnSc4	síť
</sent>
<sent id="5458">
vytyčovací	vytyčovací	k2eAgInSc4d1	vytyčovací
výkres	výkres	k1gInSc4	výkres
</sent>
<sent id="5459">
vytyčovací	vytyčovací	k2eAgInSc4d1	vytyčovací
výkres	výkres	k1gInSc4	výkres
prostorové	prostorový	k2eAgFnSc2d1	prostorová
polohy	poloha	k1gFnSc2	poloha
</sent>
<sent id="5460">
vytyčovací	vytyčovací	k2eAgFnSc1d1	vytyčovací
základna	základna	k1gFnSc1	základna
</sent>
<sent id="5461">
vytyčovací	vytyčovací	k2eAgFnSc1d1	vytyčovací
značka	značka	k1gFnSc1	značka
</sent>
<sent id="5463">
vytyčování	vytyčování	k1gNnSc4	vytyčování
prostorové	prostorový	k2eAgFnSc2d1	prostorová
polohy	poloha	k1gFnSc2	poloha
objektu	objekt	k1gInSc2	objekt
</sent>
<sent id="5464">
vytyčování	vytyčování	k1gNnSc6	vytyčování
vrstevnice	vrstevnice	k1gFnSc2	vrstevnice
</sent>
<sent id="5465">
vztažná	vztažný	k2eAgFnSc1d1	vztažná
soustava	soustava	k1gFnSc1	soustava
</sent>
<sent id="5466">
zajišťovací	zajišťovací	k2eAgFnSc1d1	zajišťovací
značka	značka	k1gFnSc1	značka
</sent>
<sent id="5467">
zajišťovací	zajišťovací	k2eAgFnSc1d1	zajišťovací
značka	značka	k1gFnSc1	značka
koleje	kolej	k1gFnSc2	kolej
</sent>
<sent id="5468">
základní	základní	k2eAgInSc4d1	základní
měření	měření	k1gNnSc1	měření
</sent>
<sent id="5469">
základní	základní	k2eAgFnSc1d1	základní
vytyčovací	vytyčovací	k2eAgFnSc1d1	vytyčovací
síť	síť	k1gFnSc1	síť
</sent>
<sent id="5470">
zvedání	zvedání	k1gNnSc4	zvedání
</sent>
<sent id="5471">
zvednutí	zvednutí	k1gNnSc6	zvednutí
</sent>
<sent id="5472">
železniční	železniční	k2eAgFnSc1d1	železniční
geodézie	geodézie	k1gFnSc1	geodézie
</sent>
<sent id="5473">
železniční	železniční	k2eAgInSc4d1	železniční
polygonový	polygonový	k2eAgInSc4d1	polygonový
bod	bod	k1gInSc4	bod
</sent>
<sent id="5474">
železniční	železniční	k2eAgInSc1d1	železniční
polygonový	polygonový	k2eAgInSc1d1	polygonový
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="5475">
čára	čára	k1gFnSc1	čára
terénní	terénní	k2eAgFnSc2d1	terénní
kostry	kostra	k1gFnSc2	kostra
</sent>
<sent id="5476">
antropogenní	antropogenní	k2eAgInSc4d1	antropogenní
terénní	terénní	k2eAgInSc4d1	terénní
tvar	tvar	k1gInSc4	tvar
</sent>
<sent id="5477">
číselné	číselný	k2eAgNnSc4d1	číselné
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="5479">
hlásný	hlásný	k2eAgInSc4d1	hlásný
systém	systém	k1gInSc4	systém
MGRS	MGRS	kA	
</sent>
<sent id="5480">
hybridní	hybridní	k2eAgFnSc1d1	hybridní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="5481">
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
bod	bod	k1gInSc4	bod
</sent>
<sent id="5483">
kótování	kótování	k1gNnSc4	kótování
</sent>
<sent id="5484">
lineární	lineární	k2eAgFnSc1d1	lineární
interpolace	interpolace	k1gFnSc1	interpolace
vrstevnic	vrstevnice	k1gFnPc2	vrstevnice
</sent>
<sent id="5485">
lomový	lomový	k2eAgInSc4d1	lomový
bod	bod	k1gInSc4	bod
</sent>
<sent id="5486">
makroreliéf	makroreliéf	k1gInSc1	makroreliéf
</sent>
<sent id="5487">
přehled	přehled	k1gInSc4	přehled
měřických	měřický	k2eAgInPc2d1	měřický
náčrtů	náčrt	k1gInPc2	náčrt
</sent>
<sent id="5489">
analogové	analogový	k2eAgNnSc1d1	analogové
vyjádření	vyjádření	k1gNnSc1	vyjádření
(	(	kIx(	
<g/>
měřené	měřený	k2eAgFnPc1d1	měřená
veličiny	veličina	k1gFnPc1	veličina
<g/>
)	)	kIx)	
</sent>
<sent id="5490">
číselníkový	číselníkový	k2eAgInSc4d1	číselníkový
úchylkoměr	úchylkoměr	k1gInSc4	úchylkoměr
</sent>
<sent id="5492">
krabicová	krabicový	k2eAgFnSc1d1	krabicová
libela	libela	k1gFnSc1	libela
</sent>
<sent id="5493">
měřicí	měřicí	k2eAgInSc4d1	měřicí
klínek	klínek	k1gInSc4	klínek
</sent>
<sent id="5495">
posuvné	posuvný	k2eAgNnSc4d1	posuvné
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="5497">
příměrné	příměrný	k2eAgNnSc4d1	příměrný
pravítko	pravítko	k1gNnSc4	pravítko
</sent>
<sent id="5498">
siloměr	siloměr	k1gInSc1	siloměr
</sent>
<sent id="5499">
úhelník	úhelník	k1gInSc1	úhelník
</sent>
<sent id="5500">
vodováha	vodováha	k1gFnSc1	vodováha
</sent>
<sent id="5501">
lektorování	lektorování	k1gNnSc6	lektorování
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5502">
lemovka	lemovka	k1gFnSc1	lemovka
hranice	hranice	k1gFnSc1	hranice
</sent>
<sent id="5503">
líc	líc	k1gFnSc1	líc
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5505">
lícovací	lícovací	k2eAgInSc4d1	lícovací
čep	čep	k1gInSc4	čep
</sent>
<sent id="5506">
lícovací	lícovací	k2eAgInSc4d1	lícovací
děrovač	děrovač	k1gInSc4	děrovač
</sent>
<sent id="5507">
lícovací	lícovací	k2eAgInSc4d1	lícovací
děrování	děrování	k1gNnSc1	děrování
</sent>
<sent id="5508">
lícovací	lícovací	k2eAgFnSc1d1	lícovací
lišta	lišta	k1gFnSc1	lišta
</sent>
<sent id="5509">
lícovací	lícovací	k2eAgFnSc1d1	lícovací
značka	značka	k1gFnSc1	značka
</sent>
<sent id="5510">
lícování	lícování	k1gNnSc4	lícování
</sent>
<sent id="5511">
linková	linkový	k2eAgFnSc1d1	Linková
síť	síť	k1gFnSc1	síť
</sent>
<sent id="7083">
matnice	matnice	k1gFnSc1	matnice
</sent>
<sent id="5513">
loxodróma	loxodróma	k1gFnSc1	loxodróma
</sent>
<sent id="5514">
maketa	maketa	k1gFnSc1	maketa
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="5515">
maketa	maketa	k1gFnSc1	maketa
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="5516">
makulatura	makulatura	k1gFnSc1	makulatura
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="5517">
maloformátový	maloformátový	k2eAgInSc4d1	maloformátový
ofsetový	ofsetový	k2eAgInSc4d1	ofsetový
stroj	stroj	k1gInSc4	stroj
</sent>
<sent id="5518">
mapa	mapa	k1gFnSc1	mapa
proměnného	proměnný	k2eAgNnSc2d1	proměnné
měřítka	měřítko	k1gNnSc2	měřítko
</sent>
<sent id="5519">
mapová	mapový	k2eAgFnSc1d1	mapová
archiválie	archiválie	k1gFnSc1	archiválie
</sent>
<sent id="5520">
mapová	mapový	k2eAgFnSc1d1	mapová
bibliografie	bibliografie	k1gFnSc1	bibliografie
</sent>
<sent id="5521">
mapová	mapový	k2eAgFnSc1d1	mapová
edice	edice	k1gFnSc1	edice
</sent>
<sent id="5522">
mapová	mapový	k2eAgFnSc1d1	mapová
sbírka	sbírka	k1gFnSc1	sbírka
</sent>
<sent id="5523">
mapová	mapový	k2eAgFnSc1d1	mapová
skříň	skříň	k1gFnSc1	skříň
</sent>
<sent id="5524">
mapová	mapový	k2eAgFnSc1d1	mapová
studovna	studovna	k1gFnSc1	studovna
</sent>
<sent id="5525">
mapové	mapový	k2eAgInPc1d1	mapový
prameny	pramen	k1gInPc1	pramen
</sent>
<sent id="5527">
nucené	nucený	k2eAgNnSc1d1	nucené
lícování	lícování	k1gNnSc1	lícování
</sent>
<sent id="5528">
divergence	divergence	k1gFnSc1	divergence
laserového	laserový	k2eAgInSc2d1	laserový
paprsku	paprsek	k1gInSc2	paprsek
</sent>
<sent id="5529">
laserové	laserový	k2eAgNnSc4d1	laserové
skenování	skenování	k1gNnSc4	skenování
</sent>
<sent id="5530">
laserový	laserový	k2eAgInSc4d1	laserový
skener	skener	k1gInSc4	skener
</sent>
<sent id="5531">
letecké	letecký	k2eAgNnSc4d1	letecké
laserové	laserový	k2eAgNnSc4d1	laserové
skenování	skenování	k1gNnSc4	skenování
(	(	kIx(	
<g/>
LLS	LLS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5532">
ZSJ	ZSJ	kA	
</sent>
<sent id="5533">
lavírování	lavírování	k1gNnSc4	lavírování
</sent>
<sent id="5535">
Gal	Gal	k1gMnSc1	Gal
</sent>
<sent id="5536">
miliGal	miliGal	k1gInSc1	miliGal
</sent>
<sent id="5537">
mapový	mapový	k2eAgInSc4d1	mapový
papír	papír	k1gInSc4	papír
</sent>
<sent id="5538">
mapový	mapový	k2eAgInSc4d1	mapový
soutisk	soutisk	k1gInSc4	soutisk
</sent>
<sent id="5539">
maskování	maskování	k1gNnSc4	maskování
</sent>
<sent id="5540">
matrice	matrice	k1gFnSc2	matrice
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5541">
mědirytina	mědirytina	k1gFnSc1	mědirytina
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5542">
měditisk	měditisk	k1gInSc1	měditisk
</sent>
<sent id="5544">
měřítkový	měřítkový	k2eAgInSc4d1	měřítkový
údaj	údaj	k1gInSc4	údaj
</sent>
<sent id="5545">
metakartografie	metakartografie	k1gFnSc1	metakartografie
</sent>
<sent id="5547">
místopisné	místopisný	k2eAgNnSc1d1	místopisné
názvosloví	názvosloví	k1gNnSc1	názvosloví
</sent>
<sent id="5548">
montáž	montáž	k1gFnSc1	montáž
popisu	popis	k1gInSc2	popis
</sent>
<sent id="5549">
aneroid	aneroid	k1gInSc1	aneroid
</sent>
<sent id="5550">
barometr	barometr	k1gInSc1	barometr
</sent>
<sent id="5551">
barometrické	barometrický	k2eAgNnSc1d1	barometrické
měření	měření	k1gNnSc1	měření
výšek	výška	k1gFnPc2	výška
</sent>
<sent id="5552">
cirkumzenitál	cirkumzenitál	k1gInSc1	cirkumzenitál
</sent>
<sent id="5553">
dálkoměrný	dálkoměrný	k2eAgInSc4d1	dálkoměrný
klín	klín	k1gInSc4	klín
</sent>
<sent id="5554">
geometrická	geometrický	k2eAgFnSc1d1	geometrická
nivelace	nivelace	k1gFnSc1	nivelace
</sent>
<sent id="5555">
Hansenova	Hansenův	k2eAgFnSc1d1	Hansenova
úloha	úloha	k1gFnSc1	úloha
</sent>
<sent id="5556">
invarové	invarový	k2eAgNnSc1d1	invarový
pásmo	pásmo	k1gNnSc1	pásmo
</sent>
<sent id="5557">
horizontální	horizontální	k2eAgFnSc1d1	horizontální
refrakce	refrakce	k1gFnSc1	refrakce
</sent>
<sent id="5558">
hrubé	hrubá	k1gFnSc6	hrubá
čtení	čtení	k1gNnSc1	čtení
</sent>
<sent id="5559">
indexová	indexový	k2eAgFnSc1d1	indexová
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="5560">
invarový	invarový	k2eAgInSc4d1	invarový
drát	drát	k1gInSc4	drát
</sent>
<sent id="5561">
jižník	jižník	k1gInSc1	jižník
</sent>
<sent id="5562">
kódová	kódový	k2eAgFnSc1d1	kódová
nivelační	nivelační	k2eAgFnSc1d1	nivelační
lať	lať	k1gFnSc1	lať
</sent>
<sent id="5563">
koeficient	koeficient	k1gInSc1	koeficient
refrakce	refrakce	k1gFnSc2	refrakce
</sent>
<sent id="5564">
koincidence	koincidence	k1gFnSc1	koincidence
</sent>
<sent id="5565">
kombinované	kombinovaný	k2eAgNnSc4d1	kombinované
protínání	protínání	k1gNnSc4	protínání
</sent>
<sent id="5566">
laboratorní	laboratorní	k2eAgFnSc1d1	laboratorní
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="5567">
laser	laser	k1gInSc1	laser
</sent>
<sent id="5568">
Lidar	Lidar	k1gInSc1	Lidar
</sent>
<sent id="5569">
limbus	limbus	k1gInSc1	limbus
</sent>
<sent id="5570">
magnetická	magnetický	k2eAgFnSc1d1	magnetická
inklinace	inklinace	k1gFnSc1	inklinace
</sent>
<sent id="5572">
měření	měření	k1gNnSc2	měření
trojpodstavcovou	trojpodstavcový	k2eAgFnSc7d1	trojpodstavcový
soupravou	souprava	k1gFnSc7	souprava
</sent>
<sent id="5573">
měření	měření	k1gNnSc4	měření
úhlů	úhel	k1gInPc2	úhel
</sent>
<sent id="5574">
měření	měření	k1gNnPc4	měření
úhlů	úhel	k1gInPc2	úhel
ve	v	k7c6	
všech	všecek	k3xTgFnPc6	
kombinacích	kombinace	k1gFnPc6	kombinace
</sent>
<sent id="5575">
měřická	měřický	k2eAgFnSc1d1	měřická
četa	četa	k1gFnSc1	četa
</sent>
<sent id="5576">
měřická	měřický	k2eAgFnSc1d1	měřická
věž	věž	k1gFnSc1	věž
</sent>
<sent id="5577">
měřický	měřický	k2eAgMnSc1d1	měřický
pomocník	pomocník	k1gMnSc1	pomocník
</sent>
<sent id="5578">
metoda	metoda	k1gFnSc1	metoda
volných	volný	k2eAgNnPc2d1	volné
stanovisek	stanovisko	k1gNnPc2	stanovisko
</sent>
<sent id="5579">
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
zeměměřičů	zeměměřič	k1gMnPc2	zeměměřič
(	(	kIx(	
<g/>
FIG	FIG	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5580">
místní	místní	k2eAgFnSc4d1	místní
síť	síť	k1gFnSc4	síť
</sent>
<sent id="5581">
neosobní	osobní	k2eNgInSc1d1	neosobní
mikrometr	mikrometr	k1gInSc1	mikrometr
</sent>
<sent id="5582">
nitkový	nitkový	k2eAgInSc4d1	nitkový
kříž	kříž	k1gInSc4	kříž
</sent>
<sent id="5583">
nivelace	nivelace	k1gFnSc2	nivelace
kupředu	kupředu	k6eAd1	
</sent>
<sent id="5584">
nivelace	nivelace	k1gFnSc1	nivelace
x-tého	x-tého	k2eAgInSc2d1	x-tého
řádu	řád	k1gInSc2	řád
</sent>
<sent id="5585">
nivelace	nivelace	k1gFnSc1	nivelace
ze	z	k7c2	
středu	střed	k1gInSc2	střed
</sent>
<sent id="5586">
nivelační	nivelační	k2eAgInSc1d1	nivelační
hřeb	hřeb	k1gInSc1	hřeb
</sent>
<sent id="5587">
nivelační	nivelační	k2eAgInSc1d1	nivelační
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="5588">
nivelační	nivelační	k2eAgFnSc1d1	nivelační
přestava	přestava	k1gFnSc1	přestava
</sent>
<sent id="5589">
nivelační	nivelační	k2eAgFnSc4d1	nivelační
síť	síť	k1gFnSc4	síť
</sent>
<sent id="5590">
nivelér	nivelér	k1gInSc1	nivelér
</sent>
<sent id="5591">
observační	observační	k2eAgInSc4d1	observační
pilíř	pilíř	k1gInSc4	pilíř
</sent>
<sent id="5592">
ochrana	ochrana	k1gFnSc1	ochrana
geodetických	geodetický	k2eAgInPc2d1	geodetický
bodů	bod	k1gInPc2	bod
</sent>
<sent id="5593">
opakovaná	opakovaný	k2eAgFnSc1d1	opakovaná
nivelace	nivelace	k1gFnSc1	nivelace
</sent>
<sent id="5595">
osa	osa	k1gFnSc1	osa
libely	libela	k1gFnSc2	libela
</sent>
<sent id="5596">
osobní	osobní	k2eAgFnSc1d1	osobní
rovnice	rovnice	k1gFnSc1	rovnice
</sent>
<sent id="5597">
pasážník	pasážník	k1gInSc1	pasážník
</sent>
<sent id="5598">
podzemní	podzemní	k2eAgFnSc1d1	podzemní
značka	značka	k1gFnSc1	značka
</sent>
<sent id="5599">
přesná	přesný	k2eAgFnSc1d1	přesná
nivelace	nivelace	k1gFnSc1	nivelace
(	(	kIx(	
<g/>
PN	PN	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5600">
přestavová	přestavový	k2eAgFnSc1d1	přestavový
podložka	podložka	k1gFnSc1	podložka
</sent>
<sent id="5601">
redukce	redukce	k1gFnSc1	redukce
délky	délka	k1gFnSc2	délka
na	na	k7c4	
hladinu	hladina	k1gFnSc4	hladina
moře	moře	k1gNnSc1	moře
</sent>
<sent id="5602">
refrakce	refrakce	k1gFnSc1	refrakce
</sent>
<sent id="5605">
směrová	směrový	k2eAgFnSc1d1	směrová
řada	řada	k1gFnSc1	řada
</sent>
<sent id="5606">
světelné	světelný	k2eAgNnSc4d1	světelné
pero	pero	k1gNnSc4	pero
</sent>
<sent id="5608">
šikmá	šikmý	k2eAgFnSc1d1	šikmá
délka	délka	k1gFnSc1	délka
</sent>
<sent id="5610">
technická	technický	k2eAgFnSc1d1	technická
nivelace	nivelace	k1gFnSc1	nivelace
(	(	kIx(	
<g/>
TN	TN	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5611">
teplotní	teplotní	k2eAgFnSc1d1	teplotní
korekce	korekce	k1gFnSc1	korekce
</sent>
<sent id="5613">
transformační	transformační	k2eAgInSc4d1	transformační
klíč	klíč	k1gInSc4	klíč
</sent>
<sent id="5614">
trigonometrická	trigonometrický	k2eAgFnSc1d1	trigonometrická
síť	síť	k1gFnSc1	síť
</sent>
<sent id="5615">
trilaterační	trilaterační	k2eAgFnSc4d1	trilaterační
síť	síť	k1gFnSc4	síť
</sent>
<sent id="5616">
ustanovka	ustanovka	k1gFnSc1	ustanovka
</sent>
<sent id="5617">
uzávěr	uzávěr	k1gInSc1	uzávěr
nivelačního	nivelační	k2eAgInSc2d1	nivelační
pořadu	pořad	k1gInSc2	pořad
</sent>
<sent id="5618">
velmi	velmi	k6eAd1	
přesná	přesný	k2eAgFnSc1d1	přesná
nivelace	nivelace	k1gFnSc1	nivelace
</sent>
<sent id="5619">
vláknová	vláknový	k2eAgFnSc1d1	vláknová
optika	optika	k1gFnSc1	optika
</sent>
<sent id="5620">
výškový	výškový	k2eAgInSc4d1	výškový
systém	systém	k1gInSc4	systém
jadranský	jadranský	k2eAgInSc4d1	jadranský
</sent>
<sent id="5621">
zacílení	zacílení	k1gNnSc6	zacílení
</sent>
<sent id="5624">
záměrná	záměrná	k1gFnSc1	záměrná
přímka	přímka	k1gFnSc1	přímka
(	(	kIx(	
<g/>
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
)	)	kIx)	
</sent>
<sent id="5625">
zapisovatel	zapisovatel	k1gMnSc1	zapisovatel
</sent>
<sent id="5626">
zauzlený	zauzlený	k2eAgInSc1d1	zauzlený
polygonový	polygonový	k2eAgInSc1d1	polygonový
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="5628">
zorné	zorný	k2eAgFnPc4d1	zorná
pole	pole	k1gFnPc4	pole
dalekohledu	dalekohled	k1gInSc2	dalekohled
</sent>
<sent id="5629">
hraniční	hraniční	k2eAgFnSc1d1	hraniční
práce	práce	k1gFnSc1	práce
</sent>
<sent id="5630">
inerciální	inerciální	k2eAgInSc4d1	inerciální
měření	měření	k1gNnSc1	měření
</sent>
<sent id="5631">
pravý	pravý	k2eAgInSc1d1	pravý
horizont	horizont	k1gInSc1	horizont
</sent>
<sent id="5632">
trigonometrická	trigonometrický	k2eAgFnSc1d1	trigonometrická
nivelace	nivelace	k1gFnSc1	nivelace
</sent>
<sent id="5634">
měření	měření	k1gNnPc2	měření
základen	základna	k1gFnPc2	základna
</sent>
<sent id="5635">
stativová	stativový	k2eAgFnSc1d1	stativová
podložka	podložka	k1gFnSc1	podložka
</sent>
<sent id="5636">
náklad	náklad	k1gInSc1	náklad
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5639">
náležitosti	náležitost	k1gFnSc2	náležitost
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5640">
nátiskový	nátiskový	k2eAgInSc1d1	nátiskový
lis	lis	k1gInSc1	lis
</sent>
<sent id="5641">
natočení	natočení	k1gNnSc4	natočení
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="5643">
návrh	návrh	k1gInSc1	návrh
kompozice	kompozice	k1gFnSc2	kompozice
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5645">
nezměněné	změněný	k2eNgFnPc1d1	nezměněná
vydání	vydání	k1gNnSc4	vydání
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5646">
nultý	nultý	k4xOgMnSc1	
poledník	poledník	k1gMnSc1	poledník
</sent>
<sent id="5647">
obnovené	obnovený	k2eAgNnSc1d1	obnovené
vydání	vydání	k1gNnPc1	vydání
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5648">
obrázková	obrázkový	k2eAgFnSc1d1	obrázková
značka	značka	k1gFnSc1	značka
</sent>
<sent id="5649">
obtah	obtah	k1gInSc1	obtah
</sent>
<sent id="5650">
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
redaktor	redaktor	k1gMnSc1	redaktor
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
,	,	kIx,	
atlasu	atlas	k1gInSc2	atlas
<g/>
)	)	kIx)	
</sent>
<sent id="5651">
odrazná	odrazný	k2eAgFnSc1d1	odrazná
předloha	předloha	k1gFnSc1	předloha
</sent>
<sent id="5652">
ofsetová	ofsetový	k2eAgFnSc1d1	ofsetová
(	(	kIx(	
<g/>
tisková	tiskový	k2eAgFnSc1d1	tisková
<g/>
)	)	kIx)	
forma	forma	k1gFnSc1	forma
</sent>
<sent id="5653">
ofsetová	ofsetový	k2eAgFnSc1d1	ofsetová
barva	barva	k1gFnSc1	barva
</sent>
<sent id="5654">
ofsetová	ofsetový	k2eAgFnSc1d1	ofsetová
deska	deska	k1gFnSc1	deska
</sent>
<sent id="5655">
ofsetový	ofsetový	k2eAgInSc4d1	ofsetový
papír	papír	k1gInSc4	papír
</sent>
<sent id="5656">
ofsetový	ofsetový	k2eAgInSc4d1	ofsetový
stroj	stroj	k1gInSc4	stroj
</sent>
<sent id="5657">
ofsetový	ofsetový	k2eAgInSc4d1	ofsetový
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="5659">
okraj	okraj	k1gInSc1	okraj
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5660">
oprava	oprava	k1gFnSc1	oprava
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5661">
optické	optický	k2eAgNnSc1d1	optické
kopírování	kopírování	k1gNnSc1	kopírování
</sent>
<sent id="5662">
orientace	orientace	k1gFnSc1	orientace
podle	podle	k7c2	
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5663">
orografie	orografie	k1gFnSc1	orografie
</sent>
<sent id="5664">
ortodróma	ortodróma	k1gFnSc1	ortodróma
</sent>
<sent id="5665">
ortochromatický	ortochromatický	k2eAgInSc4d1	ortochromatický
film	film	k1gInSc4	film
</sent>
<sent id="5666">
ořezové	ořezová	k1gFnSc2	ořezová
značky	značka	k1gFnSc2	značka
</sent>
<sent id="5667">
oříznutí	oříznutí	k1gNnSc6	oříznutí
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5668">
osa	osa	k1gFnSc1	osa
poledníkového	poledníkový	k2eAgInSc2d1	poledníkový
pásu	pás	k1gInSc2	pás
</sent>
<sent id="6642">
paprskový	paprskový	k2eAgInSc4d1	paprskový
svazek	svazek	k1gInSc4	svazek
</sent>
<sent id="5670">
brod	brod	k1gInSc1	brod
</sent>
<sent id="5671">
cyklus	cyklus	k1gInSc1	cyklus
obnovy	obnova	k1gFnSc2	obnova
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5672">
mikroreliéf	mikroreliéf	k1gInSc1	mikroreliéf
</sent>
<sent id="5673">
morfologická	morfologický	k2eAgFnSc1d1	morfologická
interpolace	interpolace	k1gFnSc1	interpolace
vrstevnic	vrstevnice	k1gFnPc2	vrstevnice
</sent>
<sent id="5674">
oprava	oprava	k1gFnSc1	oprava
ze	z	k7c2	
zobrazení	zobrazení	k1gNnSc2	zobrazení
</sent>
<sent id="5676">
rozvodí	rozvodit	k5eAaImIp3nS	
</sent>
<sent id="5677">
rozvodnice	rozvodnice	k1gFnSc1	rozvodnice
</sent>
<sent id="5678">
spočinek	spočinek	k1gInSc1	spočinek
</sent>
<sent id="5679">
suťový	suťový	k2eAgInSc4d1	suťový
kužel	kužel	k1gInSc4	kužel
</sent>
<sent id="5680">
svahová	svahový	k2eAgFnSc1d1	Svahová
kupa	kupa	k1gFnSc1	kupa
</sent>
<sent id="5681">
svahový	svahový	k2eAgInSc4d1	svahový
hřbet	hřbet	k1gInSc4	hřbet
</sent>
<sent id="5682">
příčné	příčný	k2eAgNnSc4d1	příčné
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="7228">
Geonames	Geonames	k1gInSc1	Geonames
</sent>
<sent id="5692">
rozvinovací	rozvinovací	k2eAgFnSc4d1	rozvinovací
síť	síť	k1gFnSc4	síť
</sent>
<sent id="5693">
DEM	DEM	kA	
</sent>
<sent id="5694">
DGM	DGM	kA	
</sent>
<sent id="5695">
http	http	k1gInSc1	http
</sent>
<sent id="5696">
https	https	k1gInSc1	https
</sent>
<sent id="5697">
URL	URL	kA	
</sent>
<sent id="5699">
obrys	obrys	k1gInSc1	obrys
sídla	sídlo	k1gNnSc2	sídlo
</sent>
<sent id="5701">
pantografická	pantografický	k2eAgFnSc1d1	pantografická
fréza	fréza	k1gFnSc1	fréza
</sent>
<sent id="5702">
perovka	perovka	k1gFnSc1	perovka
</sent>
<sent id="5703">
písmenná	písmenný	k2eAgFnSc1d1	písmenná
značka	značka	k1gFnSc1	značka
</sent>
<sent id="5704">
planimetr	planimetr	k1gInSc1	planimetr
</sent>
<sent id="5705">
plastová	plastový	k2eAgFnSc1d1	plastová
fólie	fólie	k1gFnSc1	fólie
</sent>
<sent id="5706">
plošná	plošný	k2eAgFnSc1d1	plošná
hmotnost	hmotnost	k1gFnSc1	hmotnost
</sent>
<sent id="5710">
podlepování	podlepování	k1gNnPc2	podlepování
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5711">
pole	pole	k1gNnSc4	pole
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="5712">
poledníkový	poledníkový	k2eAgInSc4d1	poledníkový
pás	pás	k1gInSc4	pás
</sent>
<sent id="5713">
politický	politický	k2eAgInSc4d1	politický
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="5714">
polotučné	polotučný	k2eAgNnSc4d1	polotučné
písmo	písmo	k1gNnSc4	písmo
</sent>
<sent id="5715">
povinný	povinný	k2eAgInSc4d1	povinný
výtisk	výtisk	k1gInSc4	výtisk
</sent>
<sent id="5716">
prodejna	prodejna	k1gFnSc1	prodejna
služebních	služební	k2eAgFnPc2d1	služební
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5717">
projektování	projektování	k1gNnPc4	projektování
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="7254">
cloud	cloud	k1gInSc1	cloud
</sent>
<sent id="7255">
cloudové	cloudová	k1gFnSc2	cloudová
zpracování	zpracování	k1gNnSc2	zpracování
dat	datum	k1gNnPc2	datum
</sent>
<sent id="5719">
prosvětlovací	prosvětlovací	k2eAgInSc4d1	prosvětlovací
stůl	stůl	k1gInSc4	stůl
</sent>
<sent id="5720">
průsečík	průsečík	k1gInSc1	průsečík
souřadnicové	souřadnicový	k2eAgFnSc2d1	souřadnicová
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="5721">
průsečík	průsečík	k1gInSc1	průsečík
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="5723">
první	první	k4xOgNnSc4	
vydání	vydání	k1gNnSc4	vydání
</sent>
<sent id="5724">
překryt	překryt	k2eAgInSc4d1	překryt
(	(	kIx(	
<g/>
mapových	mapový	k2eAgInPc2d1	mapový
<g/>
)	)	kIx)	
listů	list	k1gInPc2	list
</sent>
<sent id="5725">
přesnost	přesnost	k1gFnSc1	přesnost
kresby	kresba	k1gFnSc2	kresba
</sent>
<sent id="5726">
přesnost	přesnost	k1gFnSc1	přesnost
soutisku	soutisk	k1gInSc2	soutisk
</sent>
<sent id="5727">
přítisk	přítisk	k1gInSc1	přítisk
</sent>
<sent id="5728">
půllist	půllist	k1gInSc1	půllist
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="5729">
fotografický	fotografický	k2eAgInSc4d1	fotografický
reprodukční	reprodukční	k2eAgInSc4d1	reprodukční
přístroj	přístroj	k1gInSc4	přístroj
</sent>
<sent id="5730">
rámový	rámový	k2eAgInSc4d1	rámový
údaj	údaj	k1gInSc4	údaj
</sent>
<sent id="5731">
redakce	redakce	k1gFnSc2	redakce
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5732">
redakční	redakční	k2eAgInSc4d1	redakční
plán	plán	k1gInSc4	plán
</sent>
<sent id="5733">
redakční	redakční	k2eAgInPc1d1	redakční
pokyny	pokyn	k1gInPc1	pokyn
</sent>
<sent id="5734">
redakční	redakční	k2eAgFnSc1d1	redakční
příprava	příprava	k1gFnSc1	příprava
</sent>
<sent id="5735">
referenční	referenční	k2eAgFnSc1d1	referenční
koule	koule	k1gFnSc1	koule
</sent>
<sent id="5736">
reflektografie	reflektografie	k1gFnSc1	reflektografie
</sent>
<sent id="5737">
rejstřík	rejstřík	k1gInSc1	rejstřík
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="5738">
rejstřík	rejstřík	k1gInSc1	rejstřík
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5739">
reprodukční	reprodukční	k2eAgInSc4d1	reprodukční
film	film	k1gInSc4	film
</sent>
<sent id="5740">
reprodukční	reprodukční	k2eAgFnSc1d1	reprodukční
fotografie	fotografie	k1gFnSc1	fotografie
</sent>
<sent id="5741">
revizor	revizor	k1gMnSc1	revizor
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5743">
rozpis	rozpis	k1gInSc1	rozpis
barev	barva	k1gFnPc2	barva
</sent>
<sent id="5744">
rozpis	rozpis	k1gInSc1	rozpis
písma	písmo	k1gNnSc2	písmo
pro	pro	k7c4	
sazbu	sazba	k1gFnSc4	sazba
</sent>
<sent id="5745">
rub	rub	k1gInSc1	rub
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5746">
rukopis	rukopis	k1gInSc1	rukopis
</sent>
<sent id="5748">
rycí	rycí	k2eAgFnSc1d1	rycí
jehla	jehla	k1gFnSc1	jehla
</sent>
<sent id="5749">
rycí	rycí	k2eAgInSc1d1	rycí
prstenec	prstenec	k1gInSc1	prstenec
</sent>
<sent id="5750">
rycí	rycí	k2eAgInPc1d1	rycí
přístrojky	přístrojek	k1gInPc1	přístrojek
</sent>
<sent id="5751">
rycí	rycí	k2eAgFnSc1d1	rycí
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="5752">
rydlo	rydlo	k1gNnSc4	rydlo
</sent>
<sent id="5753">
autotypická	autotypický	k2eAgFnSc1d1	autotypický
síť	síť	k1gFnSc1	síť
</sent>
<sent id="5754">
kopírovací	kopírovací	k2eAgFnSc4d1	kopírovací
síť	síť	k1gFnSc4	síť
</sent>
<sent id="5755">
satinování	satinování	k1gNnSc2	satinování
(	(	kIx(	
<g/>
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	
</sent>
<sent id="5756">
severozápadní	severozápadní	k2eAgInSc4d1	severozápadní
osvětlení	osvětlení	k1gNnSc1	osvětlení
</sent>
<sent id="5757">
seznam	seznam	k1gInSc1	seznam
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5758">
seznam	seznam	k1gInSc4	seznam
mapových	mapový	k2eAgInPc2d1	mapový
přírůstků	přírůstek	k1gInPc2	přírůstek
</sent>
<sent id="5759">
schéma	schéma	k1gNnSc1	schéma
kladu	klad	k1gInSc2	klad
listů	list	k1gInPc2	list
</sent>
<sent id="5760">
signální	signální	k2eAgInSc4d1	signální
výtisk	výtisk	k1gInSc4	výtisk
</sent>
<sent id="5761">
signatura	signatura	k1gFnSc1	signatura
sídla	sídlo	k1gNnSc2	sídlo
</sent>
<sent id="5763">
sklad	sklad	k1gInSc1	sklad
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5764">
skládací	skládací	k2eAgFnSc2d1	skládací
značky	značka	k1gFnSc2	značka
</sent>
<sent id="5765">
skládání	skládání	k1gNnPc2	skládání
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5766">
sklon	sklon	k1gInSc1	sklon
písma	písmo	k1gNnSc2	písmo
</sent>
<sent id="5767">
skopírování	skopírování	k1gNnSc4	skopírování
</sent>
<sent id="5768">
slepování	slepování	k1gNnPc2	slepování
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5769">
směr	směr	k1gInSc1	směr
výroby	výroba	k1gFnSc2	výroba
papíru	papír	k1gInSc2	papír
</sent>
<sent id="5770">
soukopie	soukopie	k1gFnSc1	soukopie
</sent>
<sent id="5772">
stereomapa	stereomapa	k1gFnSc1	stereomapa
</sent>
<sent id="5773">
stínovaná	stínovaný	k2eAgFnSc1d1	stínovaná
vrstevnice	vrstevnice	k1gFnSc1	vrstevnice
</sent>
<sent id="5774">
stínové	stínový	k2eAgFnSc2d1	stínová
šrafy	šrafa	k1gFnSc2	šrafa
</sent>
<sent id="5775">
stojaté	stojatý	k2eAgNnSc4d1	stojaté
písmo	písmo	k1gNnSc4	písmo
</sent>
<sent id="5776">
strana	strana	k1gFnSc1	strana
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="5777">
strukturní	strukturní	k2eAgFnSc4d1	strukturní
síť	síť	k1gFnSc4	síť
</sent>
<sent id="5778">
stupnice	stupnice	k1gFnSc2	stupnice
šraf	šrafa	k1gFnPc2	šrafa
</sent>
<sent id="5779">
styk	styk	k1gInSc4	styk
mapových	mapový	k2eAgInPc2d1	mapový
listů	list	k1gInPc2	list
</sent>
<sent id="5780">
suchý	suchý	k2eAgInSc4d1	suchý
obtisk	obtisk	k1gInSc4	obtisk
</sent>
<sent id="5781">
symbolická	symbolický	k2eAgFnSc1d1	symbolická
značka	značka	k1gFnSc1	značka
</sent>
<sent id="5783">
štoček	štoček	k1gInSc1	štoček
</sent>
<sent id="5785">
dílčí	dílčí	k2eAgInSc4d1	dílčí
přetvoření	přetvoření	k1gNnSc1	přetvoření
</sent>
<sent id="5787">
evidence	evidence	k1gFnSc2	evidence
podzemních	podzemní	k2eAgNnPc2d1	podzemní
vedení	vedení	k1gNnPc2	vedení
</sent>
<sent id="5788">
geodetické	geodetický	k2eAgInPc4d1	geodetický
podklady	podklad	k1gInPc4	podklad
pro	pro	k7c4	
projektování	projektování	k1gNnSc4	projektování
staveb	stavba	k1gFnPc2	stavba
</sent>
<sent id="5790">
aktivní	aktivní	k2eAgFnSc1d1	aktivní
anténa	anténa	k1gFnSc1	anténa
</sent>
<sent id="5791">
almanach	almanach	k1gInSc1	almanach
</sent>
<sent id="5792">
ambiguita	ambiguita	k1gFnSc1	ambiguita
</sent>
<sent id="5793">
antispoofing	antispoofing	k1gInSc1	antispoofing
</sent>
<sent id="5794">
asistovaný	asistovaný	k2eAgInSc4d1	asistovaný
GPS	GPS	kA	
</sent>
<sent id="5795">
asociace	asociace	k1gFnSc2	asociace
NMEA	NMEA	kA	
</sent>
<sent id="5796">
atomové	atomový	k2eAgFnSc2d1	atomová
hodiny	hodina	k1gFnSc2	hodina
</sent>
<sent id="5797">
automatická	automatický	k2eAgFnSc1d1	automatická
lokalizace	lokalizace	k1gFnSc1	lokalizace
vozidel	vozidlo	k1gNnPc2	vozidlo
</sent>
<sent id="5798">
autonomní	autonomní	k2eAgNnSc1d1	autonomní
určování	určování	k1gNnPc1	určování
polohy	poloha	k1gFnSc2	poloha
</sent>
<sent id="5799">
C	C	kA	
<g/>
/	/	kIx~	
<g/>
A-kód	A-kód	k1gInSc1	A-kód
</sent>
<sent id="5800">
čas	čas	k1gInSc1	čas
do	do	k7c2	
prvního	první	k4xOgNnSc2	
určení	určení	k1gNnSc2	určení
polohy	poloha	k1gFnSc2	poloha
</sent>
<sent id="5801">
čas	čas	k1gInSc1	čas
GPS	GPS	kA	
</sent>
<sent id="5802">
čas	čas	k1gInSc1	čas
v	v	k7c6	
týdnu	týden	k1gInSc6	týden
</sent>
<sent id="5803">
časové	časový	k2eAgNnSc4d1	časové
dělení	dělení	k1gNnSc4	dělení
</sent>
<sent id="5804">
čip	čip	k1gInSc1	čip
</sent>
<sent id="5805">
datum	datum	k1gNnSc4	datum
epochy	epocha	k1gFnSc2	epocha
</sent>
<sent id="5822">
GLONASS	GLONASS	kA	
</sent>
<sent id="5824">
chyba	chyba	k1gFnSc1	chyba
z	z	k7c2	
vícecestného	vícecestný	k2eAgNnSc2d1	vícecestné
šíření	šíření	k1gNnSc2	šíření
</sent>
<sent id="5825">
integrita	integrita	k1gFnSc1	integrita
signálů	signál	k1gInPc2	signál
GNSS	GNSS	kA	
</sent>
<sent id="5826">
ionosférická	ionosférický	k2eAgFnSc1d1	ionosférická
refrakce	refrakce	k1gFnSc1	refrakce
</sent>
<sent id="5827">
ionosférické	ionosférický	k2eAgNnSc4d1	ionosférické
zpoždění	zpoždění	k1gNnSc4	zpoždění
</sent>
<sent id="5828">
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
diference	diference	k1gFnSc1	diference
</sent>
<sent id="5829">
jednofrekvenční	jednofrekvenční	k2eAgInSc4d1	jednofrekvenční
přijímač	přijímač	k1gInSc4	přijímač
GNSS	GNSS	kA	
</sent>
<sent id="5830">
kanál	kanál	k1gInSc1	kanál
standardní	standardní	k2eAgFnSc2d1	standardní
přesnosti	přesnost	k1gFnSc2	přesnost
</sent>
<sent id="5831">
kanál	kanál	k1gInSc1	kanál
vysoké	vysoký	k2eAgFnSc2d1	vysoká
přesnosti	přesnost	k1gFnSc2	přesnost
</sent>
<sent id="5832">
kinematické	kinematický	k2eAgNnSc4d1	kinematické
měření	měření	k1gNnSc4	měření
</sent>
<sent id="5833">
klamání	klamání	k1gNnSc4	klamání
</sent>
<sent id="5834">
kmitočtové	kmitočtový	k2eAgNnSc1d1	kmitočtové
dělení	dělení	k1gNnSc1	dělení
</sent>
<sent id="5835">
kódové	kódový	k2eAgNnSc1d1	kódové
měření	měření	k1gNnSc1	měření
s	s	k7c7	
podporou	podpora	k1gFnSc7	podpora
fázových	fázový	k2eAgNnPc2d1	fázové
měření	měření	k1gNnPc2	měření
</sent>
<sent id="5836">
kódové	kódový	k2eAgNnSc1d1	kódové
dělení	dělení	k1gNnSc1	dělení
</sent>
<sent id="5837">
kódové	kódový	k2eAgNnSc1d1	kódové
měření	měření	k1gNnSc1	měření
</sent>
<sent id="5838">
kombinace	kombinace	k1gFnSc1	kombinace
narrow-lane	narrow-lanout	k5eAaPmIp3nS	
</sent>
<sent id="5839">
kombinace	kombinace	k1gFnSc1	kombinace
wide-lane	wide-lanout	k5eAaPmIp3nS	
</sent>
<sent id="5840">
komerční	komerční	k2eAgFnSc1d1	komerční
služba	služba	k1gFnSc1	služba
</sent>
<sent id="5841">
komise	komise	k1gFnSc2	komise
RTCM	RTCM	kA	
</sent>
<sent id="5842">
kontinuálně	kontinuálně	k6eAd1	
pracující	pracující	k2eAgFnSc1d1	pracující
referenční	referenční	k2eAgFnSc1d1	referenční
stanice	stanice	k1gFnSc1	stanice
</sent>
<sent id="5843">
kosmický	kosmický	k2eAgInSc1d1	kosmický
segment	segment	k1gInSc1	segment
</sent>
<sent id="5844">
L1	L1	k1gFnSc1	L1
</sent>
<sent id="5845">
L2	L2	k1gFnSc1	L2
</sent>
<sent id="5846">
L5	L5	k1gFnSc1	L5
</sent>
<sent id="5847">
lokální	lokální	k2eAgInSc4d1	lokální
rozšiřující	rozšiřující	k2eAgInSc4d1	rozšiřující
systém	systém	k1gInSc4	systém
</sent>
<sent id="5848">
maska	maska	k1gFnSc1	maska
</sent>
<sent id="5849">
měření	měření	k1gNnPc2	měření
Stop-and-Go	Stop-and-Go	k6eAd1	
</sent>
<sent id="5850">
mobilní	mobilní	k2eAgInSc4d1	mobilní
přijímač	přijímač	k1gInSc4	přijímač
</sent>
<sent id="5851">
monitorování	monitorování	k1gNnSc4	monitorování
integrity	integrita	k1gFnSc2	integrita
</sent>
<sent id="5852">
diferenční	diferenční	k2eAgInSc4d1	diferenční
GPS	GPS	kA	
</sent>
<sent id="5853">
diferenční	diferenční	k2eAgFnSc1d1	diferenční
korekce	korekce	k1gFnSc1	korekce
</sent>
<sent id="5854">
diferenční	diferenční	k2eAgInSc4d1	diferenční
měření	měření	k1gNnSc1	měření
</sent>
<sent id="5855">
dobrý	dobrý	k2eAgInSc1d1	dobrý
technický	technický	k2eAgInSc1d1	technický
stav	stav	k1gInSc1	stav
družice	družice	k1gFnSc1	družice
</sent>
<sent id="5857">
dvojfrekvenční	dvojfrekvenční	k2eAgInSc4d1	dvojfrekvenční
přijímač	přijímač	k1gInSc4	přijímač
</sent>
<sent id="5858">
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
diference	diference	k1gFnSc1	diference
</sent>
<sent id="5860">
epocha	epocha	k1gFnSc1	epocha
</sent>
<sent id="5861">
fáze	fáze	k1gFnSc1	fáze
nosné	nosný	k2eAgFnSc2d1	nosná
vlny	vlna	k1gFnSc2	vlna
</sent>
<sent id="5862">
fázové	fázový	k2eAgNnSc1d1	fázové
centrum	centrum	k1gNnSc1	centrum
</sent>
<sent id="5863">
fázové	fázový	k2eAgNnSc4d1	fázové
měření	měření	k1gNnSc4	měření
</sent>
<sent id="5864">
fázový	fázový	k2eAgInSc4d1	fázový
skok	skok	k1gInSc4	skok
</sent>
<sent id="5865">
formát	formát	k1gInSc1	formát
RINEX	RINEX	kA	
</sent>
<sent id="5866">
Galileo	Galilea	k1gFnSc5	Galilea
</sent>
<sent id="5867">
globální	globální	k2eAgInSc1d1	globální
navigační	navigační	k2eAgInSc1d1	navigační
družicový	družicový	k2eAgInSc1d1	družicový
systém	systém	k1gInSc1	systém
(	(	kIx(	
<g/>
GNSS	GNSS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5868">
globální	globální	k2eAgInSc4d1	globální
polohový	polohový	k2eAgInSc4d1	polohový
systém	systém	k1gInSc4	systém
</sent>
<sent id="5871">
navigační	navigační	k2eAgInSc4d1	navigační
signál	signál	k1gInSc4	signál
vysoké	vysoký	k2eAgFnSc2d1	vysoká
přesnosti	přesnost	k1gFnSc2	přesnost
</sent>
<sent id="5872">
navigační	navigační	k2eAgFnSc1d1	navigační
zpráva	zpráva	k1gFnSc1	zpráva
</sent>
<sent id="5873">
navigační	navigační	k2eAgFnSc1d1	navigační
zpráva	zpráva	k1gFnSc1	zpráva
GPS	GPS	kA	
</sent>
<sent id="5874">
nosná	nosný	k2eAgFnSc1d1	nosná
frekvence	frekvence	k1gFnSc1	frekvence
</sent>
<sent id="5876">
odhadnutá	odhadnutý	k2eAgFnSc1d1	odhadnutá
chyba	chyba	k1gFnSc1	chyba
určení	určení	k1gNnSc2	určení
polohy	poloha	k1gFnSc2	poloha
</sent>
<sent id="5877">
odchylka	odchylka	k1gFnSc1	odchylka
hodin	hodina	k1gFnPc2	hodina
přijímače	přijímač	k1gInSc2	přijímač
</sent>
<sent id="5878">
P-kód	P-kód	k1gInSc1	P-kód
</sent>
<sent id="5879">
plný	plný	k2eAgInSc4d1	plný
operační	operační	k2eAgInSc4d1	operační
stav	stav	k1gInSc4	stav
</sent>
<sent id="5880">
počáteční	počáteční	k2eAgInSc1d1	počáteční
operační	operační	k2eAgInSc1d1	operační
stav	stav	k1gInSc1	stav
</sent>
<sent id="5882">
pozemní	pozemní	k2eAgFnSc1d1	pozemní
stanice	stanice	k1gFnSc1	stanice
pro	pro	k7c4	
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	
družicemi	družice	k1gFnPc7	družice
</sent>
<sent id="5883">
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
kruhová	kruhový	k2eAgFnSc1d1	kruhová
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="5884">
průměrování	průměrování	k1gNnSc4	průměrování
</sent>
<sent id="5885">
přesná	přesný	k2eAgFnSc1d1	přesná
polohová	polohový	k2eAgFnSc1d1	polohová
služba	služba	k1gFnSc1	služba
</sent>
<sent id="5886">
přetočení	přetočení	k1gNnSc2	přetočení
čísla	číslo	k1gNnSc2	číslo
týdne	týden	k1gInSc2	týden
GPS	GPS	kA	
</sent>
<sent id="5887">
pseudodružice	pseudodružice	k1gFnSc1	pseudodružice
</sent>
<sent id="5888">
pseudonáhodný	pseudonáhodný	k2eAgInSc4d1	pseudonáhodný
signál	signál	k1gInSc4	signál
</sent>
<sent id="5889">
pseudostatické	pseudostatický	k2eAgNnSc4d1	pseudostatický
měření	měření	k1gNnSc4	měření
</sent>
<sent id="5890">
rámec	rámec	k1gInSc1	rámec
</sent>
<sent id="5891">
referenční	referenční	k2eAgFnSc1d1	referenční
stanice	stanice	k1gFnSc1	stanice
</sent>
<sent id="5892">
relativní	relativní	k2eAgInSc4d1	relativní
určování	určování	k1gNnSc1	určování
polohy	poloha	k1gFnSc2	poloha
</sent>
<sent id="5893">
režim	režim	k1gInSc1	režim
určování	určování	k1gNnSc2	určování
dvojrozměrné	dvojrozměrný	k2eAgFnSc2d1	dvojrozměrná
polohy	poloha	k1gFnSc2	poloha
</sent>
<sent id="5894">
režim	režim	k1gInSc1	režim
určování	určování	k1gNnSc2	určování
třírozměrné	třírozměrný	k2eAgFnSc2d1	třírozměrná
polohy	poloha	k1gFnSc2	poloha
</sent>
<sent id="5895">
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
rozšiřující	rozšiřující	k2eAgInSc4d1	rozšiřující
systém	systém	k1gInSc4	systém
</sent>
<sent id="5896">
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
systém	systém	k1gInSc4	systém
DGPS	DGPS	kA	
</sent>
<sent id="5897">
RTK	RTK	kA	
měření	měření	k1gNnSc1	měření
</sent>
<sent id="5898">
rychlé	rychlý	k2eAgNnSc4d1	rychlé
statické	statický	k2eAgNnSc4d1	statické
měření	měření	k1gNnSc4	měření
</sent>
<sent id="5899">
řešení	řešení	k1gNnSc6	řešení
ambiguit	ambiguít	k5eAaPmNgInS	
za	za	k7c2	
pochodu	pochod	k1gInSc2	pochod
</sent>
<sent id="5900">
řídicí	řídicí	k2eAgInSc1d1	řídicí
segment	segment	k1gInSc1	segment
</sent>
<sent id="5901">
výběrová	výběrový	k2eAgFnSc1d1	výběrová
dostupnost	dostupnost	k1gFnSc1	dostupnost
pro	pro	k7c4	
uživalete	uživalit	k5eAaPmRp2nP,k5eAaImRp2nP	
GPS	GPS	kA	
</sent>
<sent id="5902">
sférická	sférický	k2eAgFnSc1d1	sférická
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="5903">
KMD	KMD	kA	
</sent>
<sent id="5904">
parametr	parametr	k1gInSc1	parametr
geometrické	geometrický	k2eAgFnSc2d1	geometrická
přesnosti	přesnost	k1gFnSc2	přesnost
(	(	kIx(	
<g/>
GDOP	GDOP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5905">
parametr	parametr	k1gInSc1	parametr
horizontální	horizontální	k2eAgFnSc2d1	horizontální
přesnosti	přesnost	k1gFnSc2	přesnost
(	(	kIx(	
<g/>
HDOP	HDOP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5906">
parametr	parametr	k1gInSc1	parametr
přesnosti	přesnost	k1gFnSc2	přesnost
(	(	kIx(	
<g/>
DOP	DOP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5907">
parametr	parametr	k1gInSc1	parametr
přesnosti	přesnost	k1gFnSc2	přesnost
času	čas	k1gInSc2	čas
(	(	kIx(	
<g/>
TDOP	TDOP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5908">
parametr	parametr	k1gInSc1	parametr
přesnosti	přesnost	k1gFnSc2	přesnost
polohy	poloha	k1gFnSc2	poloha
(	(	kIx(	
<g/>
PDOP	PDOP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5909">
parametr	parametr	k1gInSc1	parametr
přesnosti	přesnost	k1gFnSc2	přesnost
relativní	relativní	k2eAgFnSc2d1	relativní
polohy	poloha	k1gFnSc2	poloha
(	(	kIx(	
<g/>
RDOP	RDOP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5910">
parametr	parametr	k1gInSc1	parametr
vertikální	vertikální	k2eAgFnSc2d1	vertikální
přesnosti	přesnost	k1gFnSc2	přesnost
(	(	kIx(	
<g/>
VDOP	VDOP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="5911">
služba	služba	k1gFnSc1	služba
kritická	kritický	k2eAgFnSc1d1	kritická
z	z	k7c2	
hlediska	hledisko	k1gNnSc2	hledisko
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
</sent>
<sent id="5912">
norma	norma	k1gFnSc1	norma
komise	komise	k1gFnSc2	komise
NMEA	NMEA	kA	
</sent>
<sent id="5913">
standardní	standardní	k2eAgFnSc1d1	standardní
polohová	polohový	k2eAgFnSc1d1	polohová
služba	služba	k1gFnSc1	služba
</sent>
<sent id="5914">
statické	statický	k2eAgNnSc4d1	statické
měření	měření	k1gNnSc4	měření
</sent>
<sent id="5915">
studený	studený	k2eAgInSc4d1	studený
start	start	k1gInSc4	start
</sent>
<sent id="5916">
systém	systém	k1gInSc1	systém
EGNOS	EGNOS	kA	
</sent>
<sent id="5917">
systém	systém	k1gInSc1	systém
NAVSTAR	NAVSTAR	kA	
GPS	GPS	kA	
</sent>
<sent id="5918">
špatný	špatný	k2eAgInSc1d1	špatný
technický	technický	k2eAgInSc1d1	technický
stav	stav	k1gInSc1	stav
družice	družice	k1gFnSc1	družice
</sent>
<sent id="5919">
technický	technický	k2eAgInSc1d1	technický
stav	stav	k1gInSc1	stav
družice	družice	k1gFnSc1	družice
</sent>
<sent id="5920">
teplý	teplý	k2eAgInSc1d1	teplý
start	start	k1gInSc1	start
</sent>
<sent id="5921">
trojitá	trojitý	k2eAgFnSc1d1	trojitá
diference	diference	k1gFnSc1	diference
</sent>
<sent id="5922">
troposférická	troposférický	k2eAgFnSc1d1	Troposférická
refrakce	refrakce	k1gFnSc1	refrakce
</sent>
<sent id="5923">
troposférické	troposférický	k2eAgNnSc4d1	troposférické
zpoždění	zpoždění	k1gNnSc4	zpoždění
</sent>
<sent id="5924">
týden	týden	k1gInSc1	týden
GPS	GPS	kA	
</sent>
<sent id="5926">
určení	určení	k1gNnSc4	určení
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	
času	čas	k1gInSc2	čas
bez	bez	k7c2	
znalosti	znalost	k1gFnSc2	znalost
jejich	jejich	k3xOp3gInSc2	
počátečního	počáteční	k2eAgInSc2d1	počáteční
odhadu	odhad	k1gInSc2	odhad
</sent>
<sent id="5927">
uživatelský	uživatelský	k2eAgInSc1d1	uživatelský
segment	segment	k1gInSc1	segment
</sent>
<sent id="5928">
veřejně	veřejně	k6eAd1	
regulovaná	regulovaný	k2eAgFnSc1d1	regulovaná
služba	služba	k1gFnSc1	služba
</sent>
<sent id="5929">
vícecestné	vícecestný	k2eAgNnSc4d1	vícecestné
šíření	šíření	k1gNnSc4	šíření
</sent>
<sent id="5930">
virtuální	virtuální	k2eAgFnSc1d1	virtuální
referenční	referenční	k2eAgFnSc1d1	referenční
stanice	stanice	k1gFnSc1	stanice
</sent>
<sent id="5931">
vyhledávací	vyhledávací	k2eAgFnSc1d1	vyhledávací
a	a	k8xC	
záchranná	záchranný	k2eAgFnSc1d1	záchranná
služba	služba	k1gFnSc1	služba
</sent>
<sent id="5932">
Y-kód	Y-kód	k1gInSc1	Y-kód
</sent>
<sent id="5933">
základní	základní	k2eAgFnSc1d1	základní
frekvence	frekvence	k1gFnSc1	frekvence
</sent>
<sent id="5934">
základní	základní	k2eAgFnSc1d1	základní
služba	služba	k1gFnSc1	služba
</sent>
<sent id="5935">
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
</sent>
<sent id="5936">
zpráva	zpráva	k1gFnSc1	zpráva
NANU	NANU	kA	
</sent>
<sent id="5938">
tečkovač	tečkovač	k1gInSc1	tečkovač
</sent>
<sent id="5940">
technický	technický	k2eAgMnSc1d1	technický
redaktor	redaktor	k1gMnSc1	redaktor
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="5941">
těrkování	těrkování	k1gNnSc4	těrkování
</sent>
<sent id="5942">
tiráž	tiráž	k1gFnSc1	tiráž
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5943">
tisk	tisk	k1gInSc1	tisk
nákladu	náklad	k1gInSc2	náklad
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5944">
tisková	tiskový	k2eAgFnSc1d1	tisková
deska	deska	k1gFnSc1	deska
</sent>
<sent id="5945">
tisková	tiskový	k2eAgFnSc1d1	tisková
forma	forma	k1gFnSc1	forma
</sent>
<sent id="5946">
tiskový	tiskový	k2eAgInSc4d1	tiskový
podklad	podklad	k1gInSc4	podklad
</sent>
<sent id="5947">
tónová	tónový	k2eAgFnSc1d1	tónová
předloha	předloha	k1gFnSc1	předloha
</sent>
<sent id="5948">
transkripce	transkripce	k1gFnSc1	transkripce
</sent>
<sent id="5949">
transliterace	transliterace	k1gFnSc1	transliterace
</sent>
<sent id="5952">
tvorba	tvorba	k1gFnSc1	tvorba
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5953">
typografický	typografický	k2eAgInSc4d1	typografický
bod	bod	k1gInSc4	bod
</sent>
<sent id="5954">
ukázka	ukázka	k1gFnSc1	ukázka
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5955">
ukázka	ukázka	k1gFnSc1	ukázka
písma	písmo	k1gNnSc2	písmo
</sent>
<sent id="5956">
uložení	uložení	k1gNnSc2	uložení
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5957">
určení	určení	k1gNnSc4	určení
měřítka	měřítko	k1gNnSc2	měřítko
</sent>
<sent id="5958">
užívání	užívání	k1gNnSc2	užívání
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5960">
dvoustopé	dvoustopý	k2eAgNnSc4d1	dvoustopé
rydlo	rydlo	k1gNnSc4	rydlo
</sent>
<sent id="5961">
vazba	vazba	k1gFnSc1	vazba
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="5962">
veduta	veduta	k1gFnSc1	veduta
</sent>
<sent id="5965">
viněta	viněta	k1gFnSc1	viněta
</sent>
<sent id="5966">
vydavatel	vydavatel	k1gMnSc1	vydavatel
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="5967">
vydavatel	vydavatel	k1gMnSc1	vydavatel
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5968">
vydavatelské	vydavatelský	k2eAgNnSc4d1	vydavatelské
oprávnění	oprávnění	k1gNnSc4	oprávnění
</sent>
<sent id="5969">
vydavatelský	vydavatelský	k2eAgInSc1d1	vydavatelský
arch	arch	k1gInSc1	arch
</sent>
<sent id="5970">
vyhotovení	vyhotovení	k1gNnSc6	vyhotovení
originálu	originál	k1gInSc2	originál
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="5971">
výrobce	výrobce	k1gMnSc4	výrobce
glóbů	glóbus	k1gInPc2	glóbus
</sent>
<sent id="5974">
výstižnost	výstižnost	k1gFnSc1	výstižnost
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5975">
výška	výška	k1gFnSc1	výška
písma	písmo	k1gNnSc2	písmo
</sent>
<sent id="5976">
výšková	výškový	k2eAgFnSc1d1	výšková
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="5977">
výtisk	výtisk	k1gInSc1	výtisk
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5978">
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
redaktor	redaktor	k1gMnSc1	redaktor
</sent>
<sent id="5979">
využití	využití	k1gNnSc6	využití
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5980">
vyváženost	vyváženost	k1gFnSc1	vyváženost
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5981">
vyvolávání	vyvolávání	k1gNnSc4	vyvolávání
</sent>
<sent id="5982">
vzorník	vzorník	k1gInSc1	vzorník
barev	barva	k1gFnPc2	barva
</sent>
<sent id="5983">
vzorník	vzorník	k1gInSc1	vzorník
sítí	sítí	k1gNnSc2	sítí
</sent>
<sent id="5984">
xerografie	xerografie	k1gFnSc1	xerografie
</sent>
<sent id="5985">
zásoba	zásoba	k1gFnSc1	zásoba
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="5986">
zažehlení	zažehlení	k1gNnSc4	zažehlení
</sent>
<sent id="5987">
zkratka	zkratka	k1gFnSc1	zkratka
</sent>
<sent id="5988">
změna	změna	k1gFnSc1	změna
měřítka	měřítko	k1gNnSc2	měřítko
</sent>
<sent id="5989">
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
sazby	sazba	k1gFnSc2	sazba
</sent>
<sent id="5991">
ortografický	ortografický	k2eAgInSc4d1	ortografický
obraz	obraz	k1gInSc4	obraz
</sent>
<sent id="5992">
zobrazení	zobrazení	k1gNnSc2	zobrazení
(	(	kIx(	
<g/>
geografická	geografický	k2eAgFnSc1d1	geografická
informace	informace	k1gFnSc1	informace
<g/>
)	)	kIx)	
</sent>
<sent id="5993">
shoda	shoda	k1gFnSc1	shoda
</sent>
<sent id="5994">
dráha	dráha	k1gFnSc1	dráha
letu	let	k1gInSc2	let
</sent>
<sent id="5995">
fotogrammetrická	fotogrammetrický	k2eAgFnSc1d1	fotogrammetrická
signalizace	signalizace	k1gFnSc1	signalizace
</sent>
<sent id="5996">
fotogrammetrické	fotogrammetrický	k2eAgNnSc4d1	fotogrammetrické
mapování	mapování	k1gNnSc4	mapování
</sent>
<sent id="5998">
interpretace	interpretace	k1gFnSc1	interpretace
leteckých	letecký	k2eAgInPc2d1	letecký
snímků	snímek	k1gInPc2	snímek
</sent>
<sent id="5999">
letadlová	letadlový	k2eAgFnSc1d1	letadlová
laboratoř	laboratoř	k1gFnSc1	laboratoř
</sent>
<sent id="6000">
letadlový	letadlový	k2eAgInSc4d1	letadlový
nosič	nosič	k1gInSc4	nosič
</sent>
<sent id="6001">
navigace	navigace	k1gFnSc1	navigace
snímkového	snímkový	k2eAgInSc2d1	snímkový
letu	let	k1gInSc2	let
</sent>
<sent id="6002">
pracovní	pracovní	k2eAgInSc1d1	pracovní
cyklus	cyklus	k1gInSc1	cyklus
letecké	letecký	k2eAgFnSc2d1	letecká
měřické	měřický	k2eAgFnSc2d1	měřická
komory	komora	k1gFnSc2	komora
</sent>
<sent id="6003">
snímkový	snímkový	k2eAgInSc4d1	snímkový
let	let	k1gInSc4	let
</sent>
<sent id="6004">
výška	výška	k1gFnSc1	výška
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
roviny	rovina	k1gFnSc2	rovina
</sent>
<sent id="6005">
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
sousedních	sousední	k2eAgFnPc2d1	sousední
letových	letový	k2eAgFnPc2d1	letová
řad	řada	k1gFnPc2	řada
</sent>
<sent id="6006">
vzdušná	vzdušný	k2eAgFnSc1d1	vzdušná
základna	základna	k1gFnSc1	základna
</sent>
<sent id="6007">
základnový	základnový	k2eAgInSc1d1	základnový
poměr	poměr	k1gInSc1	poměr
</sent>
<sent id="6595">
normální	normální	k2eAgFnSc1d1	normální
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="6596">
širokoúhlá	širokoúhlý	k2eAgFnSc1d1	širokoúhlá
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="6010">
klesání	klesání	k1gNnSc4	klesání
</sent>
<sent id="6011">
kolejová	kolejový	k2eAgFnSc1d1	kolejová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6012">
konec	konec	k1gInSc1	konec
oblouku	oblouk	k1gInSc2	oblouk
</sent>
<sent id="6013">
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
měření	měření	k1gNnSc1	měření
ve	v	k7c6	
výstavbě	výstavba	k1gFnSc6	výstavba
</sent>
<sent id="6014">
lom	lom	k1gInSc1	lom
nivelety	niveleta	k1gFnSc2	niveleta
</sent>
<sent id="6015">
mapové	mapový	k2eAgInPc4d1	mapový
podklady	podklad	k1gInPc4	podklad
pro	pro	k7c4	
projektování	projektování	k1gNnSc4	projektování
staveb	stavba	k1gFnPc2	stavba
</sent>
<sent id="6016">
měření	měření	k1gNnSc4	měření
jeřábové	jeřábový	k2eAgFnSc2d1	jeřábová
dráhy	dráha	k1gFnSc2	dráha
</sent>
<sent id="6017">
měření	měření	k1gNnSc2	měření
jeřábového	jeřábový	k2eAgInSc2d1	jeřábový
mostu	most	k1gInSc2	most
</sent>
<sent id="6018">
mezní	mezní	k2eAgFnSc1d1	mezní
vytyčovací	vytyčovací	k2eAgFnSc1d1	vytyčovací
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="6019">
montážní	montážní	k2eAgFnSc1d1	montážní
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="6020">
odchylka	odchylka	k1gFnSc1	odchylka
rozměru	rozměr	k1gInSc2	rozměr
stavebního	stavební	k2eAgInSc2d1	stavební
dílce	dílec	k1gInSc2	dílec
</sent>
<sent id="6021">
protokol	protokol	k1gInSc1	protokol
o	o	k7c6	
kontrolním	kontrolní	k2eAgNnSc6d1	kontrolní
měření	měření	k1gNnSc6	měření
</sent>
<sent id="6022">
protokol	protokol	k1gInSc1	protokol
o	o	k7c4	
vytyčování	vytyčování	k1gNnSc4	vytyčování
</sent>
<sent id="6023">
redukce	redukce	k1gFnSc1	redukce
směrů	směr	k1gInPc2	směr
na	na	k7c4	
výpočetní	výpočetní	k2eAgFnSc4d1	výpočetní
plochu	plocha	k1gFnSc4	plocha
</sent>
<sent id="6025">
rektifikační	rektifikační	k2eAgInSc1d1	rektifikační
posun	posun	k1gInSc1	posun
</sent>
<sent id="6026">
řídící	řídící	k2eAgFnSc1d1	řídící
přímka	přímka	k1gFnSc1	přímka
</sent>
<sent id="6027">
staničení	staničení	k1gNnSc6	staničení
trasy	trasa	k1gFnSc2	trasa
</sent>
<sent id="6028">
svislý	svislý	k2eAgInSc1d1	svislý
posun	posun	k1gInSc1	posun
</sent>
<sent id="6029">
vodorovný	vodorovný	k2eAgInSc1d1	vodorovný
posun	posun	k1gInSc1	posun
</sent>
<sent id="6030">
výškové	výškový	k2eAgFnSc2d1	výšková
terénní	terénní	k2eAgFnSc2d1	terénní
úpravy	úprava	k1gFnSc2	úprava
</sent>
<sent id="6031">
vytyčená	vytyčený	k2eAgFnSc1d1	vytyčená
veličina	veličina	k1gFnSc1	veličina
</sent>
<sent id="6032">
vytyčovací	vytyčovací	k2eAgFnSc1d1	vytyčovací
tolerance	tolerance	k1gFnSc1	tolerance
</sent>
<sent id="6033">
začátek	začátek	k1gInSc1	začátek
oblouku	oblouk	k1gInSc2	oblouk
</sent>
<sent id="6035">
analýza	analýza	k1gFnSc1	analýza
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="6036">
anamorfní	anamorfní	k2eAgFnSc1d1	anamorfní
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="6037">
astronomická	astronomický	k2eAgFnSc1d1	astronomická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6038">
autor	autor	k1gMnSc1	autor
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6040">
autorské	autorský	k2eAgNnSc4d1	autorské
právo	právo	k1gNnSc4	právo
v	v	k7c6	
kartografii	kartografie	k1gFnSc6	kartografie
</sent>
<sent id="6041">
autorský	autorský	k2eAgInSc4d1	autorský
arch	arch	k1gInSc4	arch
</sent>
<sent id="6042">
barva	barva	k1gFnSc1	barva
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6043">
barvový	barvový	k2eAgInSc4d1	barvový
model	model	k1gInSc4	model
</sent>
<sent id="6044">
cenzální	cenzálnit	k5eAaPmIp3nS	
výběr	výběr	k1gInSc1	výběr
prvků	prvek	k1gInPc2	prvek
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6045">
diagramová	diagramový	k2eAgFnSc1d1	diagramový
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6046">
diagramový	diagramový	k2eAgInSc1d1	diagramový
znak	znak	k1gInSc1	znak
</sent>
<sent id="6047">
ekvidistanta	ekvidistant	k1gMnSc4	ekvidistant
</sent>
<sent id="6048">
endonymum	endonymum	k1gInSc1	endonymum
</sent>
<sent id="6049">
figurální	figurální	k2eAgFnSc1d1	figurální
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6050">
funkce	funkce	k1gFnSc2	funkce
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6051">
geodetická	geodetický	k2eAgFnSc1d1	geodetická
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="6052">
geografická	geografický	k2eAgFnSc1d1	geografická
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
</sent>
<sent id="6053">
geografická	geografický	k2eAgFnSc1d1	geografická
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="6055">
grafická	grafický	k2eAgFnSc1d1	grafická
analýza	analýza	k1gFnSc1	analýza
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="6056">
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
</sent>
<sent id="6057">
zeměpisné	zeměpisný	k2eAgFnPc4d1	zeměpisná
souřadnice	souřadnice	k1gFnPc4	souřadnice
</sent>
<sent id="6058">
izočárová	izočárový	k2eAgFnSc1d1	izočárový
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6059">
jazyková	jazykový	k2eAgFnSc1d1	jazyková
mutace	mutace	k1gFnSc1	mutace
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6060">
jazyková	jazykový	k2eAgFnSc1d1	jazyková
verze	verze	k1gFnSc1	verze
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6061">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
bibliografie	bibliografie	k1gFnSc1	bibliografie
</sent>
<sent id="6062">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
dokumentace	dokumentace	k1gFnSc1	dokumentace
(	(	kIx(	
<g/>
mapového	mapový	k2eAgNnSc2d1	mapové
díla	dílo	k1gNnSc2	dílo
<g/>
)	)	kIx)	
</sent>
<sent id="6063">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
interpretace	interpretace	k1gFnSc1	interpretace
</sent>
<sent id="6064">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
služba	služba	k1gFnSc1	služba
</sent>
<sent id="6065">
kartografické	kartografický	k2eAgNnSc1d1	kartografické
modelování	modelování	k1gNnSc1	modelování
</sent>
<sent id="6066">
kartografický	kartografický	k2eAgInSc4d1	kartografický
model	model	k1gInSc4	model
</sent>
<sent id="6067">
katalog	katalog	k1gInSc1	katalog
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="6068">
komplexní	komplexní	k2eAgFnSc1d1	komplexní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6069">
komplexní	komplexní	k2eAgInSc4d1	komplexní
tematický	tematický	k2eAgInSc4d1	tematický
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="6070">
koncepce	koncepce	k1gFnSc1	koncepce
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6071">
kvalita	kvalita	k1gFnSc1	kvalita
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="6072">
lektor	lektor	k1gMnSc1	lektor
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6074">
systém	systém	k1gInSc1	systém
katalogizace	katalogizace	k1gFnSc2	katalogizace
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="6075">
mapa	mapa	k1gFnSc1	mapa
zemského	zemský	k2eAgInSc2d1	zemský
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="6076">
měřítko	měřítko	k1gNnSc1	měřítko
diagramových	diagramový	k2eAgInPc2d1	diagramový
znaků	znak	k1gInPc2	znak
</sent>
<sent id="6077">
měřítko	měřítko	k1gNnSc1	měřítko
glóbu	glóbus	k1gInSc2	glóbus
</sent>
<sent id="6078">
metoda	metoda	k1gFnSc1	metoda
bodově	bodově	k6eAd1	
lokalizovaných	lokalizovaný	k2eAgInPc2d1	lokalizovaný
diagramů	diagram	k1gInPc2	diagram
</sent>
<sent id="6079">
metoda	metoda	k1gFnSc1	metoda
figurálních	figurální	k2eAgInPc2d1	figurální
znaků	znak	k1gInPc2	znak
</sent>
<sent id="6080">
metoda	metoda	k1gFnSc1	metoda
izočar	izočara	k1gFnPc2	izočara
</sent>
<sent id="6081">
metoda	metoda	k1gFnSc1	metoda
kartogramu	kartogram	k1gInSc2	kartogram
</sent>
<sent id="6082">
metoda	metoda	k1gFnSc1	metoda
stínování	stínování	k1gNnSc2	stínování
georeliéfu	georeliéf	k1gInSc2	georeliéf
</sent>
<sent id="6083">
názornost	názornost	k1gFnSc1	názornost
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6084">
normativní	normativní	k2eAgInSc1d1	normativní
výběr	výběr	k1gInSc1	výběr
prvků	prvek	k1gInPc2	prvek
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6085">
oceánografická	oceánografický	k2eAgFnSc1d1	oceánografická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6086">
oficiální	oficiální	k2eAgFnSc4d1	oficiální
geografické	geografický	k2eAgNnSc4d1	geografické
jméno	jméno	k1gNnSc4	jméno
</sent>
<sent id="6087">
orientační	orientační	k2eAgFnSc1d1	orientační
síť	síť	k1gFnSc1	síť
</sent>
<sent id="6089">
prehistorická	prehistorický	k2eAgFnSc1d1	prehistorická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6090">
proměnné	proměnná	k1gFnSc6	proměnná
měřítko	měřítko	k1gNnSc4	měřítko
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="6091">
ediční	ediční	k2eAgFnSc1d1	ediční
rada	rada	k1gFnSc1	rada
</sent>
<sent id="6092">
kulová	kulový	k2eAgFnSc1d1	kulová
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
</sent>
<sent id="6093">
kulová	kulový	k2eAgFnSc1d1	kulová
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
</sent>
<sent id="6094">
kulové	kulový	k2eAgFnSc2d1	kulová
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
souřadnice	souřadnice	k1gFnSc2	souřadnice
</sent>
<sent id="6095">
obecně-zeměpisná	obecně-zeměpisný	k2eAgFnSc1d1	obecně-zeměpisný
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6096">
radarová	radarový	k2eAgFnSc1d1	radarová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6097">
recenze	recenze	k1gFnSc1	recenze
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="6098">
recenzent	recenzent	k1gMnSc1	recenzent
mapy	mapa	k1gFnSc2	mapa
(	(	kIx(	
<g/>
atlasu	atlas	k1gInSc2	atlas
<g/>
)	)	kIx)	
</sent>
<sent id="6099">
redakční	redakční	k2eAgFnSc1d1	redakční
rada	rada	k1gFnSc1	rada
</sent>
<sent id="6100">
redigování	redigování	k1gNnPc4	redigování
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="6101">
redukce	redukce	k1gFnSc1	redukce
prvků	prvek	k1gInPc2	prvek
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6102">
regionální	regionální	k2eAgFnSc1d1	regionální
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6103">
skener	skener	k1gInSc1	skener
</sent>
<sent id="6104">
sociálně-ekonomická	sociálně-ekonomický	k2eAgFnSc1d1	sociálně-ekonomická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6105">
standardizace	standardizace	k1gFnSc2	standardizace
geografických	geografický	k2eAgNnPc2d1	geografické
jmen	jméno	k1gNnPc2	jméno
</sent>
<sent id="6110">
tiskový	tiskový	k2eAgInSc4d1	tiskový
arch	arch	k1gInSc4	arch
</sent>
<sent id="6111">
toponomastika	toponomastika	k1gFnSc1	toponomastika
</sent>
<sent id="6112">
účel	účel	k1gInSc1	účel
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6113">
uživatel	uživatel	k1gMnSc1	uživatel
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6114">
vlastnosti	vlastnost	k1gFnSc2	vlastnost
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6115">
vodohospodářská	vodohospodářský	k2eAgFnSc1d1	vodohospodářská
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6116">
vojenské	vojenský	k2eAgNnSc1d1	vojenské
státní	státní	k2eAgNnSc1d1	státní
mapové	mapový	k2eAgNnSc1d1	mapové
dílo	dílo	k1gNnSc1	dílo
</sent>
<sent id="6117">
výběr	výběr	k1gInSc1	výběr
prvků	prvek	k1gInPc2	prvek
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6118">
význam	význam	k1gInSc1	význam
mapové	mapový	k2eAgFnSc2d1	mapová
značky	značka	k1gFnSc2	značka
</sent>
<sent id="6119">
vztah	vztah	k1gInSc4	vztah
objektů	objekt	k1gInPc2	objekt
</sent>
<sent id="6120">
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
prvků	prvek	k1gInPc2	prvek
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6122">
matematicko-statistická	matematicko-statistický	k2eAgFnSc1d1	matematicko-statistická
analýza	analýza	k1gFnSc1	analýza
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="6123">
metoda	metoda	k1gFnSc1	metoda
kartografické	kartografický	k2eAgFnSc2d1	kartografická
generalizace	generalizace	k1gFnSc2	generalizace
</sent>
<sent id="6646">
projekční	projekční	k2eAgInSc4d1	projekční
centrum	centrum	k1gNnSc1	centrum
</sent>
<sent id="6124">
světlostálá	světlostálat	k5eAaImIp3nS,k5eAaPmIp3nS	
barva	barva	k1gFnSc1	barva
</sent>
<sent id="6125">
antihalace	antihalace	k1gFnSc1	antihalace
</sent>
<sent id="6126">
antihalační	antihalační	k2eAgFnSc1d1	antihalační
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="6128">
arch	arch	k1gInSc1	arch
papíru	papír	k1gInSc2	papír
</sent>
<sent id="6129">
barevná	barevný	k2eAgFnSc1d1	barevná
symbolika	symbolika	k1gFnSc1	symbolika
</sent>
<sent id="6130">
barevný	barevný	k2eAgInSc4d1	barevný
papír	papír	k1gInSc4	papír
</sent>
<sent id="6131">
barva	barva	k1gFnSc1	barva
papíru	papír	k1gInSc2	papír
</sent>
<sent id="6132">
bezdřevý	bezdřevý	k2eAgInSc4d1	bezdřevý
papír	papír	k1gInSc4	papír
</sent>
<sent id="6133">
biblový	biblový	k2eAgInSc4d1	biblový
papír	papír	k1gInSc4	papír
</sent>
<sent id="6134">
diazografický	diazografický	k2eAgInSc4d1	diazografický
papír	papír	k1gInSc4	papír
</sent>
<sent id="6135">
doplňkové	doplňkový	k2eAgFnSc2d1	doplňková
barvy	barva	k1gFnSc2	barva
</sent>
<sent id="6136">
fluorescenční	fluorescenční	k2eAgFnSc1d1	fluorescenční
barva	barva	k1gFnSc1	barva
</sent>
<sent id="6137">
fotografický	fotografický	k2eAgInSc4d1	fotografický
papír	papír	k1gInSc4	papír
</sent>
<sent id="6138">
hlavní	hlavní	k2eAgInSc1d1	hlavní
bod	bod	k1gInSc1	bod
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6139">
hloubkový	hloubkový	k2eAgInSc4d1	hloubkový
bod	bod	k1gInSc4	bod
</sent>
<sent id="6141">
kontrastní	kontrastní	k2eAgFnSc2d1	kontrastní
barvy	barva	k1gFnSc2	barva
</sent>
<sent id="6142">
kreslicí	kreslicí	k2eAgInSc1d1	kreslicí
papír	papír	k1gInSc1	papír
</sent>
<sent id="6144">
kyanografický	kyanografický	k2eAgInSc4d1	kyanografický
papír	papír	k1gInSc4	papír
</sent>
<sent id="6145">
lepenka	lepenka	k1gFnSc1	lepenka
</sent>
<sent id="6146">
lícovací	lícovací	k2eAgInSc4d1	lícovací
arch	arch	k1gInSc4	arch
</sent>
<sent id="6147">
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kartografická	kartografický	k2eAgFnSc1d1	kartografická
asociace	asociace	k1gFnSc1	asociace
(	(	kIx(	
<g/>
ICA	ICA	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="6148">
normalizovaná	normalizovaný	k2eAgFnSc1d1	normalizovaná
barva	barva	k1gFnSc1	barva
</sent>
<sent id="6149">
objektově	objektově	k6eAd1	
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
báze	báze	k1gFnSc1	báze
dat	datum	k1gNnPc2	datum
</sent>
<sent id="6150">
odpadový	odpadový	k2eAgInSc4d1	odpadový
papír	papír	k1gInSc4	papír
</sent>
<sent id="6151">
plošná	plošný	k2eAgFnSc1d1	plošná
barva	barva	k1gFnSc1	barva
</sent>
<sent id="6152">
sjížděcí	sjížděcí	k2eAgInSc4d1	sjížděcí
arch	arch	k1gInSc4	arch
</sent>
<sent id="6153">
směsná	směsný	k2eAgFnSc1d1	směsná
barva	barva	k1gFnSc1	barva
</sent>
<sent id="6154">
smluvená	smluvený	k2eAgFnSc1d1	smluvená
barva	barva	k1gFnSc1	barva
</sent>
<sent id="6155">
syntetický	syntetický	k2eAgInSc4d1	syntetický
papír	papír	k1gInSc4	papír
</sent>
<sent id="6156">
školní	školní	k2eAgInSc4d1	školní
dějepisný	dějepisný	k2eAgInSc4d1	dějepisný
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="6157">
tiskový	tiskový	k2eAgInSc4d1	tiskový
papír	papír	k1gInSc4	papír
</sent>
<sent id="6158">
vlastivědný	vlastivědný	k2eAgInSc4d1	vlastivědný
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="6159">
zajištěný	zajištěný	k2eAgInSc4d1	zajištěný
kreslicí	kreslicí	k2eAgInSc4d1	kreslicí
papír	papír	k1gInSc4	papír
</sent>
<sent id="6160">
základní	základní	k2eAgFnPc4d1	základní
barvy	barva	k1gFnPc4	barva
</sent>
<sent id="6162">
barevné	barevný	k2eAgNnSc4d1	barevné
výtažkování	výtažkování	k1gNnSc4	výtažkování
</sent>
<sent id="6163">
barevný	barevný	k2eAgInSc4d1	barevný
filtr	filtr	k1gInSc4	filtr
</sent>
<sent id="6164">
clonové	clonový	k2eAgNnSc1d1	clonové
číslo	číslo	k1gNnSc1	číslo
</sent>
<sent id="6165">
čáry	čára	k1gFnSc2	čára
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="6166">
číslo	číslo	k1gNnSc4	číslo
vydání	vydání	k1gNnSc2	vydání
</sent>
<sent id="6167">
čtvrtlist	čtvrtlist	k1gInSc1	čtvrtlist
</sent>
<sent id="6168">
datum	datum	k1gInSc1	datum
tisku	tisk	k1gInSc2	tisk
</sent>
<sent id="6169">
dělení	dělení	k1gNnSc2	dělení
na	na	k7c4	
listy	list	k1gInPc4	list
</sent>
<sent id="6170">
denzitometr	denzitometr	k1gInSc1	denzitometr
</sent>
<sent id="6171">
diagram	diagram	k1gInSc1	diagram
</sent>
<sent id="6172">
diapozitiv	diapozitiv	k1gInSc1	diapozitiv
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6173">
dílčí	dílčí	k2eAgInSc1d1	dílčí
kartografický	kartografický	k2eAgInSc1d1	kartografický
elaborát	elaborát	k1gInSc1	elaborát
</sent>
<sent id="6174">
díly	díl	k1gInPc7	díl
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6175">
DL	DL	kA	
film	film	k1gInSc1	film
</sent>
<sent id="6176">
druh	druh	k1gInSc1	druh
měřítka	měřítko	k1gNnSc2	měřítko
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6177">
druhy	druh	k1gInPc1	druh
kartografického	kartografický	k2eAgNnSc2d1	kartografické
zobrazení	zobrazení	k1gNnSc2	zobrazení
</sent>
<sent id="6178">
duplikát	duplikát	k1gInSc1	duplikát
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6179">
ekvidenzita	ekvidenzita	k1gFnSc1	ekvidenzita
</sent>
<sent id="6180">
entropie	entropie	k1gFnSc1	entropie
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6181">
etapizace	etapizace	k1gFnSc1	etapizace
vývoje	vývoj	k1gInSc2	vývoj
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="6182">
expozimetr	expozimetr	k1gInSc1	expozimetr
</sent>
<sent id="6183">
fotolitografie	fotolitografie	k1gFnSc1	fotolitografie
</sent>
<sent id="6185">
geometrická	geometrický	k2eAgFnSc1d1	geometrická
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="6186">
izometrická	izometrický	k2eAgFnSc1d1	izometrická
čára	čára	k1gFnSc1	čára
</sent>
<sent id="6187">
izoplétový	izoplétový	k2eAgInSc1d1	izoplétový
diagram	diagram	k1gInSc1	diagram
</sent>
<sent id="6188">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
fólie	fólie	k1gFnSc1	fólie
</sent>
<sent id="6189">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
grafika	grafika	k1gFnSc1	grafika
</sent>
<sent id="6190">
křivkový	křivkový	k2eAgInSc1d1	křivkový
diagram	diagram	k1gInSc1	diagram
</sent>
<sent id="6191">
kvalitativní	kvalitativní	k2eAgFnSc1d1	kvalitativní
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="6192">
kvantitativní	kvantitativní	k2eAgFnSc1d1	kvantitativní
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="6193">
lícovací	lícovací	k2eAgInPc1d1	lícovací
otvory	otvor	k1gInPc1	otvor
</sent>
<sent id="6194">
nesenzibilizovaný	senzibilizovaný	k2eNgInSc4d1	senzibilizovaný
film	film	k1gInSc4	film
</sent>
<sent id="6196">
ortochromatická	ortochromatický	k2eAgFnSc1d1	ortochromatický
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="6197">
panchromatická	panchromatický	k2eAgFnSc1d1	panchromatická
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="6199">
proporcionální	proporcionální	k2eAgFnSc1d1	proporcionální
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="6200">
prostorový	prostorový	k2eAgInSc1d1	prostorový
diagram	diagram	k1gInSc1	diagram
</sent>
<sent id="6201">
rozptylová	rozptylový	k2eAgFnSc1d1	rozptylová
fólie	fólie	k1gFnSc1	fólie
</sent>
<sent id="6202">
síťový	síťový	k2eAgInSc4d1	síťový
graf	graf	k1gInSc4	graf
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="6203">
složený	složený	k2eAgInSc4d1	složený
kartogram	kartogram	k1gInSc4	kartogram
</sent>
<sent id="6204">
tiskový	tiskový	k2eAgInSc4d1	tiskový
formát	formát	k1gInSc4	formát
</sent>
<sent id="6205">
vektorový	vektorový	k2eAgInSc1d1	vektorový
diagram	diagram	k1gInSc1	diagram
</sent>
<sent id="6206">
větrná	větrný	k2eAgFnSc1d1	větrná
růžice	růžice	k1gFnSc1	růžice
</sent>
<sent id="6207">
větrný	větrný	k2eAgInSc1d1	větrný
diagram	diagram	k1gInSc1	diagram
</sent>
<sent id="6208">
denzita	denzita	k1gFnSc1	denzita
</sent>
<sent id="6209">
elektrografické	elektrografický	k2eAgNnSc1d1	elektrografický
kopírování	kopírování	k1gNnSc1	kopírování
</sent>
<sent id="6210">
fotografická	fotografický	k2eAgFnSc1d1	fotografická
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="6211">
fotomechanická	fotomechanický	k2eAgFnSc1d1	fotomechanický
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="6212">
fotomechanické	fotomechanický	k2eAgNnSc1d1	fotomechanický
kopírování	kopírování	k1gNnSc1	kopírování
</sent>
<sent id="6213">
hloubka	hloubka	k1gFnSc1	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
objektivu	objektiv	k1gInSc2	objektiv
</sent>
<sent id="6214">
hranice	hranice	k1gFnSc1	hranice
areálu	areál	k1gInSc2	areál
(	(	kIx(	
<g/>
na	na	k7c6	
mapě	mapa	k1gFnSc6	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="6215">
hranice	hranice	k1gFnSc1	hranice
porostů	porost	k1gInPc2	porost
</sent>
<sent id="6216">
inverze	inverze	k1gFnSc1	inverze
rytiny	rytina	k1gFnSc2	rytina
</sent>
<sent id="6217">
kartograféma	kartograféma	k1gFnSc1	kartograféma
</sent>
<sent id="6218">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
kurzíva	kurzíva	k1gFnSc1	kurzíva
</sent>
<sent id="6219">
kartomorféma	kartomorféma	k1gFnSc1	kartomorféma
</sent>
<sent id="6220">
kartosyntagma	kartosyntagma	k1gFnSc1	kartosyntagma
</sent>
<sent id="6221">
katastrální	katastrální	k2eAgFnSc1d1	katastrální
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="6222">
kód	kód	k1gInSc1	kód
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6223">
kopie	kopie	k1gFnSc1	kopie
na	na	k7c6	
plastové	plastový	k2eAgFnSc6d1	plastová
fólii	fólie	k1gFnSc6	fólie
</sent>
<sent id="6224">
kopírovací	kopírovací	k2eAgFnSc1d1	kopírovací
montáž	montáž	k1gFnSc1	montáž
</sent>
<sent id="6225">
negativní	negativní	k2eAgFnSc1d1	negativní
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="6226">
podkladová	podkladový	k2eAgFnSc1d1	podkladová
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="6227">
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="6228">
průsvitka	průsvitka	k1gFnSc1	průsvitka
popisu	popis	k1gInSc2	popis
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6229">
rycí	rycí	k2eAgInSc4d1	rycí
hrot	hrot	k1gInSc4	hrot
</sent>
<sent id="6230">
senzitometrická	senzitometrický	k2eAgFnSc1d1	senzitometrická
charakteristika	charakteristika	k1gFnSc1	charakteristika
</sent>
<sent id="6231">
senzitometrická	senzitometrický	k2eAgFnSc1d1	senzitometrická
křivka	křivka	k1gFnSc1	křivka
</sent>
<sent id="6233">
státní	státní	k2eAgFnSc1d1	státní
hranice	hranice	k1gFnSc1	hranice
</sent>
<sent id="6234">
stranově	stranově	k6eAd1	
převrácená	převrácený	k2eAgFnSc1d1	převrácená
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="6235">
stranově	stranově	k6eAd1	
správná	správný	k2eAgFnSc1d1	správná
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="6236">
anamorfní	anamorfní	k2eAgFnSc1d1	anamorfní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6237">
areálová	areálový	k2eAgFnSc1d1	areálová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6238">
globální	globální	k2eAgFnSc2d1	globální
lokalizace	lokalizace	k1gFnSc2	lokalizace
objektu	objekt	k1gInSc2	objekt
</sent>
<sent id="6239">
grafická	grafický	k2eAgFnSc1d1	grafická
lokalizace	lokalizace	k1gFnSc1	lokalizace
objektu	objekt	k1gInSc2	objekt
</sent>
<sent id="6240">
hlavní	hlavní	k2eAgInPc1d1	hlavní
paprsky	paprsek	k1gInPc1	paprsek
zkreslení	zkreslení	k1gNnSc1	zkreslení
</sent>
<sent id="6241">
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6242">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
licence	licence	k1gFnSc1	licence
</sent>
<sent id="6243">
logika	logika	k1gFnSc1	logika
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6245">
mapa	mapa	k1gFnSc1	mapa
povrchové	povrchový	k2eAgFnSc2d1	povrchová
situace	situace	k1gFnSc2	situace
</sent>
<sent id="6246">
mapa	mapa	k1gFnSc1	mapa
tíhových	tíhový	k2eAgFnPc2d1	tíhová
izanomál	izanomála	k1gFnPc2	izanomála
</sent>
<sent id="6247">
mapa	mapa	k1gFnSc1	mapa
vertikálních	vertikální	k2eAgInPc2d1	vertikální
recentních	recentní	k2eAgInPc2d1	recentní
pohybů	pohyb	k1gInPc2	pohyb
</sent>
<sent id="6248">
mapa	mapa	k1gFnSc1	mapa
vlečky	vlečka	k1gFnSc2	vlečka
</sent>
<sent id="6249">
mapa	mapa	k1gFnSc1	mapa
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
</sent>
<sent id="6250">
marginálie	marginálie	k1gFnSc1	marginálie
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6251">
Mercatorova	Mercatorův	k2eAgFnSc1d1	Mercatorova
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6252">
odvětvová	odvětvový	k2eAgFnSc1d1	odvětvová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6253">
podrobná	podrobný	k2eAgFnSc1d1	podrobná
lokalizace	lokalizace	k1gFnSc1	lokalizace
objektu	objekt	k1gInSc2	objekt
</sent>
<sent id="6256">
přílohová	přílohový	k2eAgFnSc1d1	přílohová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6257">
říční	říční	k2eAgFnSc1d1	říční
navigační	navigační	k2eAgFnSc1d1	navigační
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6258">
tečková	tečkový	k2eAgFnSc1d1	tečková
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6259">
technologicko-kalkulační	technologicko-kalkulační	k2eAgInSc1d1	technologicko-kalkulační
list	list	k1gInSc1	list
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="6260">
titulní	titulní	k2eAgInSc4d1	titulní
list	list	k1gInSc4	list
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="6261">
trojrozměrná	trojrozměrný	k2eAgFnSc1d1	trojrozměrná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6262">
účelová	účelový	k2eAgFnSc1d1	účelová
důlní	důlní	k2eAgFnSc1d1	důlní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6264">
vojenskogeografická	vojenskogeografický	k2eAgFnSc1d1	vojenskogeografický
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6266">
základní	základní	k2eAgFnSc1d1	základní
důlní	důlní	k2eAgFnSc1d1	důlní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6267">
černobílá	černobílý	k2eAgFnSc1d1	černobílá
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6268">
glóbusová	glóbusový	k2eAgFnSc1d1	glóbusový
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6269">
hloubnicová	hloubnicový	k2eAgFnSc1d1	hloubnicový
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6270">
horolezecká	horolezecký	k2eAgFnSc1d1	horolezecká
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6271">
izobarická	izobarický	k2eAgFnSc1d1	izobarický
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6272">
kartometrická	kartometrický	k2eAgFnSc1d1	kartometrický
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6274">
lesnická	lesnický	k2eAgFnSc1d1	lesnická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6275">
letištní	letištní	k2eAgFnSc1d1	letištní
překážková	překážkový	k2eAgFnSc1d1	překážková
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6276">
mapa	mapa	k1gFnSc1	mapa
(	(	kIx(	
<g/>
dopravních	dopravní	k2eAgInPc2d1	dopravní
<g/>
)	)	kIx)	
spojů	spoj	k1gInPc2	spoj
</sent>
<sent id="6277">
mapa	mapa	k1gFnSc1	mapa
dopravních	dopravní	k2eAgInPc2d1	dopravní
směrů	směr	k1gInPc2	směr
</sent>
<sent id="6278">
důlní	důlní	k2eAgFnSc1d1	důlní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6279">
mapa	mapa	k1gFnSc1	mapa
hloubkových	hloubkový	k2eAgFnPc2d1	hloubková
vrstev	vrstva	k1gFnPc2	vrstva
</sent>
<sent id="6280">
mapa	mapa	k1gFnSc1	mapa
Marsu	Mars	k1gInSc2	Mars
</sent>
<sent id="6281">
mapa	mapa	k1gFnSc1	mapa
Měsíce	měsíc	k1gInSc2	měsíc
</sent>
<sent id="6282">
mapa	mapa	k1gFnSc1	mapa
plavebních	plavební	k2eAgFnPc2d1	plavební
tratí	trať	k1gFnPc2	trať
</sent>
<sent id="6286">
mapa	mapa	k1gFnSc1	mapa
větrání	větrání	k1gNnSc2	větrání
</sent>
<sent id="6287">
mapa	mapa	k1gFnSc1	mapa
vodních	vodní	k2eAgInPc2d1	vodní
proudů	proud	k1gInPc2	proud
</sent>
<sent id="7162">
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
metoda	metoda	k1gFnSc1	metoda
měření	měření	k1gNnSc1	měření
</sent>
<sent id="6289">
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6290">
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
letecká	letecký	k2eAgFnSc1d1	letecká
mapa	mapa	k1gFnSc1	mapa
světa	svět	k1gInSc2	svět
</sent>
<sent id="6291">
morfografická	morfografický	k2eAgFnSc1d1	morfografický
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6292">
morfometrická	morfometrický	k2eAgFnSc1d1	morfometrická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6293">
námořní	námořní	k2eAgFnSc1d1	námořní
navigační	navigační	k2eAgFnSc1d1	navigační
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6294">
orientační	orientační	k2eAgFnSc1d1	orientační
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6296">
ostrovní	ostrovní	k2eAgFnSc1d1	ostrovní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6297">
otáčivá	otáčivý	k2eAgFnSc1d1	otáčivá
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6298">
paleogeografická	paleogeografický	k2eAgFnSc1d1	paleogeografická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6299">
pásová	pásový	k2eAgFnSc1d1	pásová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6301">
přistávací	přistávací	k2eAgFnSc1d1	přistávací
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6302">
půdní	půdní	k2eAgFnSc1d1	půdní
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6303">
registrační	registrační	k2eAgFnSc1d1	registrační
tematická	tematický	k2eAgFnSc1d1	tematická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6304">
samostatná	samostatný	k2eAgFnSc1d1	samostatná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6306">
školní	školní	k2eAgFnSc1d1	školní
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6307">
šrafovaná	šrafovaný	k2eAgFnSc1d1	šrafovaná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6308">
textová	textový	k2eAgFnSc1d1	textová
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6309">
vyklápěcí	vyklápěcí	k2eAgFnSc1d1	vyklápěcí
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6311">
dekadické	dekadický	k2eAgNnSc4d1	dekadické
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="6312">
diagramová	diagramový	k2eAgFnSc1d1	diagramový
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="6313">
hlavní	hlavní	k2eAgInSc1d1	hlavní
poledník	poledník	k1gInSc1	poledník
</sent>
<sent id="6314">
kartografické	kartografický	k2eAgFnSc2d1	kartografická
metody	metoda	k1gFnSc2	metoda
výzkumu	výzkum	k1gInSc2	výzkum
</sent>
<sent id="6315">
měřítko	měřítko	k1gNnSc4	měřítko
generalizace	generalizace	k1gFnSc2	generalizace
</sent>
<sent id="6316">
měřítko	měřítko	k1gNnSc4	měřítko
mapového	mapový	k2eAgInSc2d1	mapový
znaku	znak	k1gInSc2	znak
</sent>
<sent id="6317">
měřítko	měřítko	k1gNnSc4	měřítko
na	na	k7c6	
poledníku	poledník	k1gInSc6	poledník
</sent>
<sent id="6318">
měřítko	měřítko	k1gNnSc4	měřítko
na	na	k7c6	
rovníku	rovník	k1gInSc6	rovník
</sent>
<sent id="6319">
měřítko	měřítko	k1gNnSc4	měřítko
na	na	k7c6	
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
</sent>
<sent id="6320">
měřítko	měřítko	k1gNnSc4	měřítko
na	na	k7c6	
střední	střední	k2eAgFnSc6d1	střední
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
</sent>
<sent id="6321">
měřítko	měřítko	k1gNnSc4	měřítko
převýšení	převýšení	k1gNnSc2	převýšení
</sent>
<sent id="6322">
měřítko	měřítko	k1gNnSc4	měřítko
ve	v	k7c6	
středu	střed	k1gInSc6	střed
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6323">
metoda	metoda	k1gFnSc1	metoda
barevné	barevný	k2eAgFnSc2d1	barevná
hypsometrie	hypsometrie	k1gFnSc2	hypsometrie
</sent>
<sent id="6324">
metoda	metoda	k1gFnSc1	metoda
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="6325">
zjištěné	zjištěný	k2eAgNnSc4d1	zjištěné
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="6326">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
metoda	metoda	k1gFnSc1	metoda
znázorňování	znázorňování	k1gNnSc1	znázorňování
</sent>
<sent id="6327">
mikrozáznam	mikrozáznam	k1gInSc1	mikrozáznam
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6328">
místní	místní	k2eAgInSc4d1	místní
poledník	poledník	k1gInSc4	poledník
</sent>
<sent id="6329">
modrotisk	modrotisk	k1gInSc1	modrotisk
</sent>
<sent id="6330">
morfometrie	morfometrie	k1gFnSc1	morfometrie
</sent>
<sent id="6331">
reliéfní	reliéfní	k2eAgFnSc1d1	reliéfní
stupňová	stupňový	k2eAgFnSc1d1	stupňová
matrice	matrice	k1gFnSc1	matrice
</sent>
<sent id="6332">
rozlišovací	rozlišovací	k2eAgFnSc4d1	rozlišovací
mez	mez	k1gFnSc4	mez
znaku	znak	k1gInSc2	znak
</sent>
<sent id="6333">
sáhové	sáhový	k2eAgNnSc4d1	sáhové
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="6334">
stránková	stránkový	k2eAgFnSc1d1	stránková
montáž	montáž	k1gFnSc1	montáž
</sent>
<sent id="6335">
tisková	tiskový	k2eAgFnSc1d1	tisková
matrice	matrice	k1gFnSc1	matrice
</sent>
<sent id="6336">
tvarovací	tvarovací	k2eAgInSc4d1	tvarovací
model	model	k1gInSc4	model
</sent>
<sent id="6337">
vydavatelské	vydavatelský	k2eAgNnSc4d1	vydavatelské
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="6338">
základní	základní	k2eAgInSc4d1	základní
poledník	poledník	k1gInSc4	poledník
</sent>
<sent id="6339">
barevná	barevný	k2eAgFnSc1d1	barevná
předloha	předloha	k1gFnSc1	předloha
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6340">
čárový	čárový	k2eAgInSc1d1	čárový
prvek	prvek	k1gInSc1	prvek
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6341">
dokumentační	dokumentační	k2eAgFnSc1d1	dokumentační
práce	práce	k1gFnSc1	práce
</sent>
<sent id="6342">
druhový	druhový	k2eAgInSc4d1	druhový
název	název	k1gInSc4	název
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6343">
externí	externí	k2eAgFnSc1d1	externí
projekce	projekce	k1gFnSc1	projekce
</sent>
<sent id="6344">
fotografický	fotografický	k2eAgInSc4d1	fotografický
obraz	obraz	k1gInSc4	obraz
</sent>
<sent id="6348">
kopírovací	kopírovací	k2eAgFnSc1d1	kopírovací
předloha	předloha	k1gFnSc1	předloha
</sent>
<sent id="6349">
latentní	latentní	k2eAgInSc4d1	latentní
obraz	obraz	k1gInSc4	obraz
</sent>
<sent id="6351">
montážní	montážní	k2eAgFnSc1d1	montážní
podložka	podložka	k1gFnSc1	podložka
</sent>
<sent id="6352">
název	název	k1gInSc1	název
na	na	k7c6	
mapě	mapa	k1gFnSc6	mapa
</sent>
<sent id="6353">
negativní	negativní	k2eAgInSc1d1	negativní
reprodukční	reprodukční	k2eAgInSc1d1	reprodukční
proces	proces	k1gInSc1	proces
</sent>
<sent id="6355">
nestandardizované	standardizovaný	k2eNgNnSc4d1	nestandardizované
geografické	geografický	k2eAgNnSc4d1	geografické
jméno	jméno	k1gNnSc4	jméno
</sent>
<sent id="6356">
nezkreslený	zkreslený	k2eNgInSc4d1	nezkreslený
poledník	poledník	k1gInSc4	poledník
</sent>
<sent id="6357">
nosič	nosič	k1gInSc4	nosič
údajů	údaj	k1gInPc2	údaj
</sent>
<sent id="6358">
O-T	O-T	k1gFnSc1	O-T
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6359">
objekt	objekt	k1gInSc1	objekt
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6360">
oblouk	oblouk	k1gInSc1	oblouk
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
</sent>
<sent id="6361">
odstupňování	odstupňování	k1gNnPc2	odstupňování
barev	barva	k1gFnPc2	barva
</sent>
<sent id="6362">
onomastika	onomastika	k1gFnSc1	onomastika
</sent>
<sent id="6363">
osa	osa	k1gFnSc1	osa
zobrazovací	zobrazovací	k2eAgFnSc2d1	zobrazovací
plochy	plocha	k1gFnSc2	plocha
</sent>
<sent id="6364">
paleokartografie	paleokartografie	k1gFnSc1	paleokartografie
</sent>
<sent id="6365">
parametry	parametr	k1gInPc7	parametr
objektivu	objektiv	k1gInSc2	objektiv
</sent>
<sent id="6366">
planografie	planografie	k1gFnSc1	planografie
</sent>
<sent id="6367">
poledníkový	poledníkový	k2eAgInSc4d1	poledníkový
oblouk	oblouk	k1gInSc4	oblouk
</sent>
<sent id="6368">
anaglyf	anaglyf	k1gInSc1	anaglyf
</sent>
<sent id="6369">
automatická	automatický	k2eAgFnSc1d1	automatická
(	(	kIx(	
<g/>
digitální	digitální	k2eAgFnSc1d1	digitální
<g/>
)	)	kIx)	
aerotriangulace	aerotriangulace	k1gFnSc1	aerotriangulace
</sent>
<sent id="6370">
barevný	barevný	k2eAgInSc4d1	barevný
film	film	k1gInSc4	film
</sent>
<sent id="6371">
barevný	barevný	k2eAgInSc1d1	barevný
infračervený	infračervený	k2eAgInSc1d1	infračervený
film	film	k1gInSc1	film
</sent>
<sent id="6373">
epipolární	epipolární	k2eAgFnSc1d1	epipolární
transformace	transformace	k1gFnSc1	transformace
</sent>
<sent id="6374">
fotogrammetrická	fotogrammetrický	k2eAgFnSc1d1	fotogrammetrická
(	(	kIx(	
<g/>
digitální	digitální	k2eAgFnSc1d1	digitální
<g/>
)	)	kIx)	
pracovní	pracovní	k2eAgFnSc1d1	pracovní
stanice	stanice	k1gFnSc1	stanice
</sent>
<sent id="6376">
infračervený	infračervený	k2eAgInSc4d1	infračervený
film	film	k1gInSc4	film
</sent>
<sent id="6377">
kompenzace	kompenzace	k1gFnSc1	kompenzace
smazu	smaz	k1gInSc2	smaz
(	(	kIx(	
<g/>
obrazu	obraz	k1gInSc2	obraz
<g/>
)	)	kIx)	
</sent>
<sent id="6378">
modelové	modelový	k2eAgFnPc4d1	modelová
souřadnice	souřadnice	k1gFnPc4	souřadnice
</sent>
<sent id="6379">
monokomparátor	monokomparátor	k1gInSc1	monokomparátor
</sent>
<sent id="6380">
mřížková	mřížkový	k2eAgFnSc1d1	mřížková
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="6381">
neměřická	měřický	k2eNgFnSc1d1	měřický
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="6382">
normální	normální	k2eAgInSc4d1	normální
případ	případ	k1gInSc4	případ
stereofotogrammetrie	stereofotogrammetrie	k1gFnSc2	stereofotogrammetrie
</sent>
<sent id="6385">
prvky	prvek	k1gInPc7	prvek
vnější	vnější	k2eAgFnSc2d1	vnější
orientace	orientace	k1gFnSc2	orientace
</sent>
<sent id="6386">
prvky	prvek	k1gInPc7	prvek
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
orientace	orientace	k1gFnSc2	orientace
</sent>
<sent id="6387">
překreslení	překreslení	k1gNnSc6	překreslení
snímku	snímek	k1gInSc2	snímek
</sent>
<sent id="6388">
překreslovač	překreslovač	k1gInSc1	překreslovač
</sent>
<sent id="6389">
přesný	přesný	k2eAgInSc4d1	přesný
fotogrammetrický	fotogrammetrický	k2eAgInSc4d1	fotogrammetrický
skener	skener	k1gInSc4	skener
</sent>
<sent id="6390">
radiální	radiální	k2eAgFnSc1d1	radiální
distorze	distorze	k1gFnSc1	distorze
</sent>
<sent id="6391">
radiální	radiální	k2eAgFnSc1d1	radiální
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
</sent>
<sent id="6392">
stereokomparátor	stereokomparátor	k1gInSc1	stereokomparátor
</sent>
<sent id="6396">
zorný	zorný	k2eAgInSc4d1	zorný
úhel	úhel	k1gInSc4	úhel
</sent>
<sent id="6398">
digitální	digitální	k2eAgInSc4d1	digitální
dálkoměr	dálkoměr	k1gInSc4	dálkoměr
</sent>
<sent id="6399">
digitální	digitální	k2eAgInSc1d1	digitální
nivelační	nivelační	k2eAgInSc1d1	nivelační
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="6401">
laserový	laserový	k2eAgInSc1d1	laserový
družicový	družicový	k2eAgInSc1d1	družicový
dálkoměr	dálkoměr	k1gInSc1	dálkoměr
</sent>
<sent id="6402">
meridiánová	meridiánový	k2eAgFnSc1d1	Meridiánová
konvergence	konvergence	k1gFnSc1	konvergence
</sent>
<sent id="6403">
normála	normála	k1gFnSc1	normála
k	k	k7c3	
elipsoidu	elipsoid	k1gInSc3	elipsoid
</sent>
<sent id="6404">
sféroid	sféroid	k1gInSc1	sféroid
</sent>
<sent id="6405">
svislice	svislice	k1gFnSc1	svislice
</sent>
<sent id="6406">
trojpodstavcová	trojpodstavcový	k2eAgFnSc1d1	trojpodstavcový
souprava	souprava	k1gFnSc1	souprava
</sent>
<sent id="7286">
invarová	invarový	k2eAgFnSc1d1	invarový
nivelační	nivelační	k2eAgFnSc1d1	nivelační
lať	lať	k1gFnSc1	lať
</sent>
<sent id="6408">
určování	určování	k1gNnSc6	určování
geoidu	geoid	k1gInSc2	geoid
</sent>
<sent id="6409">
zenitteleskop	zenitteleskop	k1gInSc1	zenitteleskop
</sent>
<sent id="6410">
BIML	BIML	kA	
</sent>
<sent id="6411">
BNM	BNM	kA	
</sent>
<sent id="6414">
CE	CE	kA	
</sent>
<sent id="6415">
CENELEC	CENELEC	kA	
</sent>
<sent id="6416">
CGML	CGML	kA	
</sent>
<sent id="6417">
CGPM	CGPM	kA	
</sent>
<sent id="6418">
CIML	CIML	kA	
</sent>
<sent id="6419">
CIPM	CIPM	kA	
</sent>
<sent id="6422">
EAM	EAM	kA	
</sent>
<sent id="6423">
EMeTAS	EMeTAS	k1gFnSc1	EMeTAS
</sent>
<sent id="6424">
EOQ	EOQ	kA	
</sent>
<sent id="6425">
EOQC	EOQC	kA	
</sent>
<sent id="6426">
MID	MID	kA	
</sent>
<sent id="6427">
OIML	OIML	kA	
</sent>
<sent id="6428">
WELMEC	WELMEC	kA	
</sent>
<sent id="7212">
bezprojekční	bezprojekční	k2eAgInSc4d1	bezprojekční
zobrazení	zobrazení	k1gNnSc1	zobrazení
</sent>
<sent id="6430">
barevná	barevný	k2eAgFnSc1d1	barevná
polotónová	polotónový	k2eAgFnSc1d1	polotónová
předloha	předloha	k1gFnSc1	předloha
</sent>
<sent id="6431">
černobílá	černobílý	k2eAgFnSc1d1	černobílá
polotónová	polotónový	k2eAgFnSc1d1	polotónová
předloha	předloha	k1gFnSc1	předloha
</sent>
<sent id="6432">
černobílý	černobílý	k2eAgInSc1d1	černobílý
polotónový	polotónový	k2eAgInSc1d1	polotónový
obraz	obraz	k1gInSc1	obraz
</sent>
<sent id="6433">
podtisk	podtisk	k1gInSc1	podtisk
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6434">
poměr	poměr	k1gInSc1	poměr
zmenšení	zmenšení	k1gNnSc2	zmenšení
</sent>
<sent id="6435">
poměr	poměr	k1gInSc1	poměr
zvětšení	zvětšení	k1gNnSc2	zvětšení
</sent>
<sent id="6436">
pomocná	pomocný	k2eAgFnSc1d1	pomocná
(	(	kIx(	
<g/>
rozvinutelná	rozvinutelný	k2eAgFnSc1d1	rozvinutelný
<g/>
)	)	kIx)	
zobrazovací	zobrazovací	k2eAgFnSc1d1	zobrazovací
plocha	plocha	k1gFnSc1	plocha
</sent>
<sent id="6437">
potisknutelná	potisknutelný	k2eAgFnSc1d1	potisknutelná
plocha	plocha	k1gFnSc1	plocha
(	(	kIx(	
<g/>
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	
</sent>
<sent id="6438">
potištěná	potištěný	k2eAgFnSc1d1	potištěná
plocha	plocha	k1gFnSc1	plocha
(	(	kIx(	
<g/>
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	
</sent>
<sent id="6439">
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
reprodukční	reprodukční	k2eAgInSc1d1	reprodukční
proces	proces	k1gInSc1	proces
</sent>
<sent id="6440">
pragmatika	pragmatika	k1gFnSc1	pragmatika
znakového	znakový	k2eAgInSc2d1	znakový
systému	systém	k1gInSc2	systém
</sent>
<sent id="6442">
projekt	projekt	k1gInSc1	projekt
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="6443">
projektování	projektování	k1gNnSc2	projektování
znakových	znakový	k2eAgInPc2d1	znakový
systémů	systém	k1gInPc2	systém
</sent>
<sent id="6444">
průhledná	průhledný	k2eAgFnSc1d1	průhledná
předloha	předloha	k1gFnSc1	předloha
</sent>
<sent id="6445">
průsvitná	průsvitný	k2eAgFnSc1d1	průsvitná
předloha	předloha	k1gFnSc1	předloha
</sent>
<sent id="6446">
předmět	předmět	k1gInSc1	předmět
kartografického	kartografický	k2eAgNnSc2d1	kartografické
znázorňování	znázorňování	k1gNnSc2	znázorňování
</sent>
<sent id="6447">
předmět	předmět	k1gInSc1	předmět
kartografie	kartografie	k1gFnSc2	kartografie
</sent>
<sent id="6448">
přesnost	přesnost	k1gFnSc1	přesnost
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6449">
pseudoizočára	pseudoizočár	k1gMnSc4	pseudoizočár
</sent>
<sent id="6450">
revizní	revizní	k2eAgInSc4d1	revizní
otisk	otisk	k1gInSc4	otisk
(	(	kIx(	
<g/>
ze	z	k7c2	
stroje	stroj	k1gInSc2	stroj
<g/>
)	)	kIx)	
</sent>
<sent id="6451">
revizní	revizní	k2eAgInSc4d1	revizní
podklad	podklad	k1gInSc4	podklad
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6452">
ÚNMZ	ÚNMZ	kA	
</sent>
<sent id="6454">
geografický	geografický	k2eAgInSc4d1	geografický
vzhled	vzhled	k1gInSc4	vzhled
</sent>
<sent id="6455">
severní	severní	k2eAgFnSc1d1	severní
souřadnice	souřadnice	k1gFnSc1	souřadnice
(	(	kIx(	
<g/>
N	N	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="6456">
východní	východní	k2eAgFnSc1d1	východní
souřadnice	souřadnice	k1gFnSc1	souřadnice
(	(	kIx(	
<g/>
E	E	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="6458">
stranově	stranově	k6eAd1	
převrácený	převrácený	k2eAgInSc1d1	převrácený
obraz	obraz	k1gInSc1	obraz
</sent>
<sent id="6459">
stranově	stranově	k6eAd1	
správný	správný	k2eAgInSc1d1	správný
obraz	obraz	k1gInSc1	obraz
</sent>
<sent id="6460">
šestistupňový	šestistupňový	k2eAgInSc1d1	šestistupňový
poledníkový	poledníkový	k2eAgInSc1d1	poledníkový
pás	pás	k1gInSc1	pás
</sent>
<sent id="6461">
tematický	tematický	k2eAgInSc4d1	tematický
obsah	obsah	k1gInSc4	obsah
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6462">
termografický	termografický	k2eAgInSc4d1	termografický
papír	papír	k1gInSc4	papír
</sent>
<sent id="6463">
termovakuový	termovakuový	k2eAgInSc1d1	termovakuový
tvarovací	tvarovací	k2eAgInSc1d1	tvarovací
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="6464">
třístupňový	třístupňový	k2eAgInSc1d1	třístupňový
poledníkový	poledníkový	k2eAgInSc1d1	poledníkový
pás	pás	k1gInSc1	pás
</sent>
<sent id="6465">
trimetalická	trimetalický	k2eAgFnSc1d1	trimetalický
(	(	kIx(	
<g/>
tisková	tiskový	k2eAgFnSc1d1	tisková
<g/>
)	)	kIx)	
deska	deska	k1gFnSc1	deska
</sent>
<sent id="6466">
vyvolávací	vyvolávací	k2eAgInSc1d1	vyvolávací
přístroj	přístroj	k1gInSc1	přístroj
</sent>
<sent id="6468">
antropogenní	antropogenní	k2eAgInSc1d1	antropogenní
jev	jev	k1gInSc1	jev
</sent>
<sent id="6469">
barva	barva	k1gFnSc1	barva
výškové	výškový	k2eAgFnSc2d1	výšková
vrstvy	vrstva	k1gFnSc2	vrstva
</sent>
<sent id="6470">
blokdiagram	blokdiagram	k1gInSc1	blokdiagram
</sent>
<sent id="6471">
blokdiagram	blokdiagram	k1gInSc1	blokdiagram
terénních	terénní	k2eAgFnPc2d1	terénní
hran	hrana	k1gFnPc2	hrana
</sent>
<sent id="6472">
cyklus	cyklus	k1gInSc1	cyklus
údržby	údržba	k1gFnSc2	údržba
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6473">
čára	čára	k1gFnSc1	čára
</sent>
<sent id="6474">
čára	čára	k1gFnSc1	čára
odlivu	odliv	k1gInSc2	odliv
</sent>
<sent id="6475">
čára	čára	k1gFnSc1	čára
pravoúhlé	pravoúhlý	k2eAgFnSc2d1	pravoúhlá
rovinné	rovinný	k2eAgFnSc2d1	rovinná
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="6476">
čárkovaná	čárkovaný	k2eAgFnSc1d1	čárkovaná
čára	čára	k1gFnSc1	čára
</sent>
<sent id="6477">
čtverec	čtverec	k1gInSc1	čtverec
souřadnicové	souřadnicový	k2eAgFnSc2d1	souřadnicová
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="6478">
dokladový	dokladový	k2eAgInSc1d1	dokladový
exemplář	exemplář	k1gInSc1	exemplář
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="6479">
doplňková	doplňkový	k2eAgFnSc1d1	doplňková
tisková	tiskový	k2eAgFnSc1d1	tisková
forma	forma	k1gFnSc1	forma
</sent>
<sent id="6480">
dotyková	dotykový	k2eAgFnSc1d1	dotyková
čára	čára	k1gFnSc1	čára
</sent>
<sent id="6481">
dotyková	dotykový	k2eAgFnSc1d1	dotyková
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
</sent>
<sent id="6482">
dotykový	dotykový	k2eAgInSc4d1	dotykový
bod	bod	k1gInSc4	bod
</sent>
<sent id="6483">
duplikát	duplikát	k1gInSc1	duplikát
filmu	film	k1gInSc2	film
</sent>
<sent id="6484">
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
čára	čára	k1gFnSc1	čára
</sent>
<sent id="6486">
fotografický	fotografický	k2eAgInSc4d1	fotografický
film	film	k1gInSc4	film
</sent>
<sent id="6487">
fotografování	fotografování	k1gNnSc4	fotografování
</sent>
<sent id="6488">
přerušovaná	přerušovaný	k2eAgFnSc1d1	přerušovaná
čára	čára	k1gFnSc1	čára
</sent>
<sent id="6489">
sečná	sečný	k2eAgFnSc1d1	sečná
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
</sent>
<sent id="6490">
střídavá	střídavý	k2eAgFnSc1d1	střídavá
čára	čára	k1gFnSc1	čára
</sent>
<sent id="6491">
tečkovaná	tečkovaný	k2eAgFnSc1d1	tečkovaná
čára	čára	k1gFnSc1	čára
</sent>
<sent id="6492">
tenká	tenký	k2eAgFnSc1d1	tenká
čára	čára	k1gFnSc1	čára
</sent>
<sent id="6493">
tlustá	tlustý	k2eAgFnSc1d1	tlustá
čára	čára	k1gFnSc1	čára
</sent>
<sent id="6494">
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
název	název	k1gInSc1	název
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6495">
základní	základní	k2eAgFnSc2d1	základní
tiskové	tiskový	k2eAgFnSc2d1	tisková
formy	forma	k1gFnSc2	forma
</sent>
<sent id="6496">
barevný	barevný	k2eAgInSc4d1	barevný
tón	tón	k1gInSc4	tón
</sent>
<sent id="6497">
boční	boční	k2eAgFnSc1d1	boční
nakládací	nakládací	k2eAgFnSc1d1	nakládací
hrana	hrana	k1gFnSc1	hrana
</sent>
<sent id="6498">
čistý	čistý	k2eAgInSc4d1	čistý
formát	formát	k1gInSc4	formát
</sent>
<sent id="6499">
dílčí	dílčí	k2eAgInPc1d1	dílčí
doplňky	doplněk	k1gInPc1	doplněk
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="6501">
harmonikové	harmonik	k1gMnPc1	harmonik
skládání	skládání	k1gNnSc1	skládání
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="6502">
hrana	hrana	k1gFnSc1	hrana
ořezu	ořez	k1gInSc2	ořez
</sent>
<sent id="6503">
hranice	hranice	k1gFnSc1	hranice
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
</sent>
<sent id="6504">
hustota	hustota	k1gFnSc1	hustota
šraf	šrafa	k1gFnPc2	šrafa
</sent>
<sent id="6505">
chytač	chytač	k1gInSc1	chytač
</sent>
<sent id="6506">
jas	jas	k1gInSc1	jas
barvy	barva	k1gFnSc2	barva
</sent>
<sent id="6507">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
estetika	estetika	k1gFnSc1	estetika
</sent>
<sent id="6508">
mapové	mapový	k2eAgInPc4d1	mapový
diapozitivy	diapozitiv	k1gInPc4	diapozitiv
</sent>
<sent id="6509">
nakládací	nakládací	k2eAgFnSc1d1	nakládací
hrana	hrana	k1gFnSc1	hrana
</sent>
<sent id="6510">
nepestrá	pestrý	k2eNgFnSc1d1	nepestrá
barva	barva	k1gFnSc1	barva
</sent>
<sent id="6511">
pestrá	pestrý	k2eAgFnSc1d1	pestrá
barva	barva	k1gFnSc1	barva
</sent>
<sent id="6512">
přenosový	přenosový	k2eAgInSc4d1	přenosový
válec	válec	k1gInSc4	válec
</sent>
<sent id="6513">
sytost	sytost	k1gFnSc1	sytost
barvy	barva	k1gFnSc2	barva
</sent>
<sent id="6514">
šedý	šedý	k2eAgInSc4d1	šedý
klín	klín	k1gInSc4	klín
</sent>
<sent id="6515">
tiskový	tiskový	k2eAgInSc4d1	tiskový
válec	válec	k1gInSc4	válec
</sent>
<sent id="6516">
tlakový	tlakový	k2eAgInSc4d1	tlakový
válec	válec	k1gInSc4	válec
</sent>
<sent id="6517">
nátisková	nátiskový	k2eAgFnSc1d1	nátisková
forma	forma	k1gFnSc1	forma
</sent>
<sent id="6518">
polární	polární	k2eAgInSc1d1	polární
diagram	diagram	k1gInSc1	diagram
</sent>
<sent id="6519">
polotónová	polotónový	k2eAgFnSc1d1	polotónová
fotografie	fotografie	k1gFnSc1	fotografie
</sent>
<sent id="6520">
předzcitlivěná	předzcitlivěný	k2eAgFnSc1d1	předzcitlivěný
tisková	tiskový	k2eAgFnSc1d1	tisková
deska	deska	k1gFnSc1	deska
</sent>
<sent id="6521">
přírodní	přírodní	k2eAgInSc4d1	přírodní
jev	jev	k1gInSc4	jev
</sent>
<sent id="6522">
reprodukční	reprodukční	k2eAgMnSc1d1	reprodukční
fotograf	fotograf	k1gMnSc1	fotograf
</sent>
<sent id="6523">
citlivost	citlivost	k1gFnSc1	citlivost
fotografické	fotografický	k2eAgFnSc2d1	fotografická
vrstvy	vrstva	k1gFnSc2	vrstva
</sent>
<sent id="6524">
formát	formát	k1gInSc1	formát
tiskového	tiskový	k2eAgInSc2d1	tiskový
stroje	stroj	k1gInSc2	stroj
</sent>
<sent id="6525">
přibližovací	přibližovací	k2eAgFnSc1d1	přibližovací
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6527">
sjížděcí	sjížděcí	k2eAgFnSc1d1	sjížděcí
tisková	tiskový	k2eAgFnSc1d1	tisková
forma	forma	k1gFnSc1	forma
</sent>
<sent id="6528">
skleněná	skleněný	k2eAgFnSc1d1	skleněná
deska	deska	k1gFnSc1	deska
</sent>
<sent id="6529">
sloupávací	sloupávací	k2eAgInSc4d1	sloupávací
film	film	k1gInSc4	film
</sent>
<sent id="6530">
spotřební	spotřební	k2eAgInSc1d1	spotřební
exemplář	exemplář	k1gInSc1	exemplář
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6531">
starý	starý	k2eAgInSc4d1	starý
atlas	atlas	k1gInSc4	atlas
</sent>
<sent id="6532">
světlotisková	světlotiskový	k2eAgFnSc1d1	světlotisková
forma	forma	k1gFnSc1	forma
</sent>
<sent id="6533">
tisková	tiskový	k2eAgFnSc1d1	tisková
forma	forma	k1gFnSc1	forma
polohopisu	polohopis	k1gInSc2	polohopis
</sent>
<sent id="6534">
tisková	tiskový	k2eAgFnSc1d1	tisková
forma	forma	k1gFnSc1	forma
popisu	popis	k1gInSc3	popis
</sent>
<sent id="6535">
tisková	tiskový	k2eAgFnSc1d1	tisková
forma	forma	k1gFnSc1	forma
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="6536">
tisková	tiskový	k2eAgFnSc1d1	tisková
forma	forma	k1gFnSc1	forma
vodstva	vodstvo	k1gNnSc2	vodstvo
</sent>
<sent id="6537">
topografické	topografický	k2eAgInPc4d1	topografický
předměty	předmět	k1gInPc4	předmět
a	a	k8xC	
jevy	jev	k1gInPc4	jev
</sent>
<sent id="6538">
trojúhelníkový	trojúhelníkový	k2eAgInSc1d1	trojúhelníkový
diagram	diagram	k1gInSc1	diagram
</sent>
<sent id="6539">
vlasová	vlasový	k2eAgFnSc1d1	vlasová
čára	čára	k1gFnSc1	čára
</sent>
<sent id="6540">
výtažkový	výtažkový	k2eAgInSc4d1	výtažkový
filtr	filtr	k1gInSc4	filtr
</sent>
<sent id="6541">
xylografie	xylografie	k1gFnSc1	xylografie
</sent>
<sent id="6542">
Gaussovo	Gaussův	k2eAgNnSc4d1	Gaussovo
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="6543">
magnetická	magnetický	k2eAgFnSc1d1	magnetická
orientace	orientace	k1gFnSc1	orientace
</sent>
<sent id="6544">
AIS	AIS	kA	
</sent>
<sent id="6545">
CzechPOINT	CzechPOINT	k1gFnSc1	CzechPOINT
</sent>
<sent id="6546">
ISÚI	ISÚI	kA	
</sent>
<sent id="6547">
ISZR	ISZR	kA	
</sent>
<sent id="6551">
SMO	SMO	kA	
ČR	ČR	kA	
</sent>
<sent id="6552">
Svaz	svaz	k1gInSc1	svaz
měst	město	k1gNnPc2	město
a	a	k8xC	
obcí	obec	k1gFnPc2	obec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	
<g/>
SMO	SMO	kA	
ČR	ČR	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="6553">
barevný	barevný	k2eAgInSc4d1	barevný
kontrast	kontrast	k1gInSc4	kontrast
</sent>
<sent id="6554">
barevný	barevný	k2eAgInSc1d1	barevný
kruh	kruh	k1gInSc1	kruh
</sent>
<sent id="6555">
hlavní	hlavní	k2eAgFnSc1d1	hlavní
kružnice	kružnice	k1gFnSc1	kružnice
</sent>
<sent id="6556">
hypsografická	hypsografický	k2eAgFnSc1d1	hypsografický
křivka	křivka	k1gFnSc1	křivka
</sent>
<sent id="6557">
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
kartogram	kartogram	k1gInSc4	kartogram
</sent>
<sent id="6558">
kartografie	kartografie	k1gFnSc1	kartografie
měst	město	k1gNnPc2	město
</sent>
<sent id="6559">
kartuš	kartuš	k1gFnSc1	kartuš
</sent>
<sent id="6560">
kinematografická	kinematografický	k2eAgFnSc1d1	kinematografická
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="6562">
komentář	komentář	k1gInSc1	komentář
k	k	k7c3	
mapě	mapa	k1gFnSc3	mapa
</sent>
<sent id="6563">
komerční	komerční	k2eAgFnSc1d1	komerční
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="6564">
kresba	kresba	k1gFnSc1	kresba
skal	skála	k1gFnPc2	skála
</sent>
<sent id="6565">
kreslicí	kreslicí	k2eAgInSc1d1	kreslicí
karton	karton	k1gInSc1	karton
</sent>
<sent id="6566">
kružítko	kružítko	k1gNnSc4	kružítko
</sent>
<sent id="6567">
krytí	krytí	k1gNnSc4	krytí
barvy	barva	k1gFnSc2	barva
</sent>
<sent id="6568">
námořní	námořní	k2eAgFnSc1d1	námořní
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="6569">
pomocná	pomocný	k2eAgFnSc1d1	pomocná
kopie	kopie	k1gFnSc1	kopie
</sent>
<sent id="6570">
soukromá	soukromý	k2eAgFnSc1d1	soukromá
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="6571">
transliterační	transliterační	k2eAgInSc4d1	transliterační
klíč	klíč	k1gInSc4	klíč
</sent>
<sent id="6572">
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
kružnice	kružnice	k1gFnSc2	kružnice
</sent>
<sent id="6574">
busolní	busolní	k2eAgInSc1d1	busolní
(	(	kIx(	
<g/>
polygonový	polygonový	k2eAgInSc1d1	polygonový
<g/>
)	)	kIx)	
pořad	pořad	k1gInSc1	pořad
</sent>
<sent id="6575">
elektronické	elektronický	k2eAgNnSc1d1	elektronické
měření	měření	k1gNnSc1	měření
délek	délka	k1gFnPc2	délka
</sent>
<sent id="6576">
forma	forma	k1gFnSc1	forma
georeliéfu	georeliéf	k1gInSc2	georeliéf
</sent>
<sent id="6577">
klasifikace	klasifikace	k1gFnSc1	klasifikace
měřických	měřický	k2eAgInPc2d1	měřický
snímků	snímek	k1gInPc2	snímek
</sent>
<sent id="6578">
měření	měření	k1gNnPc2	měření
délek	délka	k1gFnPc2	délka
</sent>
<sent id="6579">
měření	měření	k1gNnPc2	měření
délek	délka	k1gFnPc2	délka
invarovými	invarový	k2eAgInPc7d1	invarový
dráty	drát	k1gInPc7	drát
</sent>
<sent id="6580">
měření	měření	k1gNnSc2	měření
pásmem	pásmo	k1gNnSc7	pásmo
</sent>
<sent id="6581">
měření	měření	k1gNnPc2	měření
sklonu	sklon	k1gInSc2	sklon
</sent>
<sent id="6583">
obtížnost	obtížnost	k1gFnSc1	obtížnost
mapování	mapování	k1gNnSc2	mapování
</sent>
<sent id="6585">
přesnost	přesnost	k1gFnSc1	přesnost
metody	metoda	k1gFnSc2	metoda
podrobného	podrobný	k2eAgNnSc2d1	podrobné
měření	měření	k1gNnSc2	měření
</sent>
<sent id="6586">
rádiové	rádiový	k2eAgNnSc1d1	rádiové
měření	měření	k1gNnSc1	měření
délek	délka	k1gFnPc2	délka
</sent>
<sent id="6587">
tachymetrický	tachymetrický	k2eAgInSc1d1	tachymetrický
náčrt	náčrt	k1gInSc1	náčrt
</sent>
<sent id="6588">
trigonometrické	trigonometrický	k2eAgNnSc1d1	trigonometrické
měření	měření	k1gNnSc1	měření
délek	délka	k1gFnPc2	délka
</sent>
<sent id="6589">
úhlový	úhlový	k2eAgInSc4d1	úhlový
uzávěr	uzávěr	k1gInSc4	uzávěr
</sent>
<sent id="6590">
blízká	blízký	k2eAgFnSc1d1	blízká
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="6591">
digitální	digitální	k2eAgFnSc1d1	digitální
letecká	letecký	k2eAgFnSc1d1	letecká
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="6593">
horizontální	horizontální	k2eAgFnSc1d1	horizontální
paralaxa	paralaxa	k1gFnSc1	paralaxa
</sent>
<sent id="6594">
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="6597">
úzkoúhlá	úzkoúhlý	k2eAgFnSc1d1	úzkoúhlá
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="6598">
velmi	velmi	k6eAd1	
blízká	blízký	k2eAgFnSc1d1	blízká
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="6599">
vertikální	vertikální	k2eAgFnSc1d1	vertikální
paralaxa	paralaxa	k1gFnSc1	paralaxa
</sent>
<sent id="6600">
zvlášť	zvlášť	k6eAd1	
širokoúhlá	širokoúhlý	k2eAgFnSc1d1	širokoúhlá
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="6601">
cyan	cyan	k1gInSc1	cyan
</sent>
<sent id="6602">
fyziografická	fyziografický	k2eAgFnSc1d1	fyziografický
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6603">
geomorfologická	geomorfologický	k2eAgFnSc1d1	geomorfologická
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6604">
hieroglyfová	hieroglyfový	k2eAgFnSc1d1	hieroglyfový
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6605">
linkování	linkování	k1gNnSc4	linkování
</sent>
<sent id="6606">
list	list	k1gInSc1	list
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="6607">
magenta	magenta	k1gFnSc1	magenta
</sent>
<sent id="6608">
maska	maska	k1gFnSc1	maska
</sent>
<sent id="6609">
matování	matování	k1gNnSc4	matování
</sent>
<sent id="6610">
měřicí	měřicí	k2eAgFnSc1d1	měřicí
matnice	matnice	k1gFnSc1	matnice
</sent>
<sent id="6611">
mezera	mezera	k1gFnSc1	mezera
mezi	mezi	k7c7	
listy	list	k1gInPc7	list
</sent>
<sent id="6612">
mezikopie	mezikopie	k1gFnSc1	mezikopie
</sent>
<sent id="6613">
meziměřítko	meziměřítko	k1gNnSc4	meziměřítko
</sent>
<sent id="6614">
optický	optický	k2eAgInSc4d1	optický
měnič	měnič	k1gInSc4	měnič
šířky	šířka	k1gFnSc2	šířka
čar	čára	k1gFnPc2	čára
</sent>
<sent id="6615">
procentová	procentový	k2eAgFnSc1d1	procentová
tečková	tečkový	k2eAgFnSc1d1	tečková
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6616">
překrývací	překrývací	k2eAgFnSc1d1	překrývací
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6618">
přibližné	přibližný	k2eAgNnSc4d1	přibližné
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="6619">
raná	raný	k2eAgFnSc1d1	raná
mapa	mapa	k1gFnSc1	mapa
</sent>
<sent id="6620">
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
metoda	metoda	k1gFnSc1	metoda
</sent>
<sent id="6623">
fotogrammetrický	fotogrammetrický	k2eAgInSc4d1	fotogrammetrický
skener	skener	k1gInSc4	skener
</sent>
<sent id="6625">
panchromatický	panchromatický	k2eAgInSc4d1	panchromatický
film	film	k1gInSc4	film
</sent>
<sent id="6626">
podélný	podélný	k2eAgInSc4d1	podélný
sklon	sklon	k1gInSc4	sklon
snímku	snímek	k1gInSc2	snímek
(	(	kIx(	
<g/>
φ	φ	k?	
<g/>
)	)	kIx)	
</sent>
<sent id="6627">
polarizační	polarizační	k2eAgFnPc4d1	polarizační
brýle	brýle	k1gFnPc4	brýle
</sent>
<sent id="6628">
pootočení	pootočení	k1gNnSc6	pootočení
snímku	snímek	k1gInSc2	snímek
(	(	kIx(	
<g/>
κ	κ	k?	
<g/>
)	)	kIx)	
</sent>
<sent id="6629">
příčný	příčný	k2eAgInSc4d1	příčný
sklon	sklon	k1gInSc4	sklon
snímku	snímek	k1gInSc2	snímek
(	(	kIx(	
<g/>
ω	ω	k?	
<g/>
)	)	kIx)	
</sent>
<sent id="6630">
přirozené	přirozený	k2eAgNnSc4d1	přirozené
stereoskopické	stereoskopický	k2eAgNnSc4d1	stereoskopické
vidění	vidění	k1gNnSc4	vidění
</sent>
<sent id="6631">
radiální	radiální	k2eAgInSc1d1	radiální
posun	posun	k1gInSc1	posun
</sent>
<sent id="6632">
řezná	řezný	k2eAgFnSc1d1	řezná
linie	linie	k1gFnSc1	linie
</sent>
<sent id="6633">
snímková	snímkový	k2eAgFnSc1d1	snímková
dvojice	dvojice	k1gFnSc1	dvojice
</sent>
<sent id="6634">
stereoskopické	stereoskopický	k2eAgNnSc1d1	stereoskopické
pokrytí	pokrytí	k1gNnSc1	pokrytí
</sent>
<sent id="6635">
umělé	umělý	k2eAgNnSc4d1	umělé
stereoskopické	stereoskopický	k2eAgNnSc4d1	stereoskopické
vidění	vidění	k1gNnSc4	vidění
</sent>
<sent id="6636">
epipolární	epipolární	k2eAgFnSc1d1	epipolární
geometrie	geometrie	k1gFnSc1	geometrie
</sent>
<sent id="6637">
hranová	hranový	k2eAgFnSc1d1	hranová
ostrost	ostrost	k1gFnSc1	ostrost
</sent>
<sent id="6638">
iradiace	iradiace	k1gFnSc1	iradiace
</sent>
<sent id="6639">
mikrofotogrammetrie	mikrofotogrammetrie	k1gFnSc1	mikrofotogrammetrie
</sent>
<sent id="6640">
obrazová	obrazový	k2eAgFnSc1d1	obrazová
rovina	rovina	k1gFnSc1	rovina
</sent>
<sent id="6641">
osvit	osvit	k1gInSc1	osvit
</sent>
<sent id="6643">
podmínka	podmínka	k1gFnSc1	podmínka
kolinearity	kolinearita	k1gFnSc2	kolinearita
</sent>
<sent id="6644">
podmínka	podmínka	k1gFnSc1	podmínka
komplanarity	komplanarita	k1gFnSc2	komplanarita
</sent>
<sent id="6647">
radiometrické	radiometrický	k2eAgFnPc4d1	radiometrická
úpravy	úprava	k1gFnPc4	úprava
obrazu	obraz	k1gInSc2	obraz
</sent>
<sent id="6648">
rámová	rámový	k2eAgFnSc1d1	rámová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6650">
spojovací	spojovací	k2eAgInSc4d1	spojovací
bod	bod	k1gInSc4	bod
</sent>
<sent id="6651">
barevný	barevný	k2eAgInSc4d1	barevný
nátisk	nátisk	k1gInSc4	nátisk
</sent>
<sent id="6653">
náčrt	náčrt	k1gInSc1	náčrt
orografických	orografický	k2eAgFnPc2d1	orografická
čar	čára	k1gFnPc2	čára
</sent>
<sent id="6654">
nakladač	nakladač	k1gInSc1	nakladač
</sent>
<sent id="6657">
nosič	nosič	k1gInSc1	nosič
kresby	kresba	k1gFnSc2	kresba
</sent>
<sent id="6658">
nosič	nosič	k1gInSc1	nosič
tisku	tisk	k1gInSc2	tisk
</sent>
<sent id="6659">
nosič	nosič	k1gInSc1	nosič
vrstvy	vrstva	k1gFnSc2	vrstva
</sent>
<sent id="6660">
nulátko	nulátko	k1gNnSc4	nulátko
</sent>
<sent id="6661">
odchylky	odchylka	k1gFnSc2	odchylka
nalícování	nalícování	k1gNnSc2	nalícování
</sent>
<sent id="6662">
odsazení	odsazení	k1gNnSc4	odsazení
kresby	kresba	k1gFnSc2	kresba
</sent>
<sent id="6663">
odstředivka	odstředivka	k1gFnSc1	odstředivka
</sent>
<sent id="6664">
odvrstvování	odvrstvování	k1gNnSc4	odvrstvování
</sent>
<sent id="6665">
ostrost	ostrost	k1gFnSc1	ostrost
čáry	čára	k1gFnSc2	čára
</sent>
<sent id="6666">
ovrstvování	ovrstvování	k1gNnSc4	ovrstvování
</sent>
<sent id="6668">
polotónový	polotónový	k2eAgInSc4d1	polotónový
obraz	obraz	k1gInSc4	obraz
</sent>
<sent id="6669">
síťový	síťový	k2eAgInSc4d1	síťový
negativ	negativ	k1gInSc4	negativ
</sent>
<sent id="6670">
strojový	strojový	k2eAgInSc4d1	strojový
nátisk	nátisk	k1gInSc4	nátisk
</sent>
<sent id="6671">
stupnicový	stupnicový	k2eAgInSc4d1	stupnicový
nátisk	nátisk	k1gInSc4	nátisk
</sent>
<sent id="6672">
výtažkový	výtažkový	k2eAgInSc4d1	výtažkový
negativ	negativ	k1gInSc4	negativ
</sent>
<sent id="6673">
druhové	druhový	k2eAgNnSc4d1	druhové
označení	označení	k1gNnSc4	označení
</sent>
<sent id="6677">
předmět	předmět	k1gInSc1	předmět
měření	měření	k1gNnSc2	měření
polohopisu	polohopis	k1gInSc2	polohopis
</sent>
<sent id="6678">
předmět	předmět	k1gInSc1	předmět
měření	měření	k1gNnSc2	měření
výškopisu	výškopis	k1gInSc2	výškopis
</sent>
<sent id="6679">
předpis	předpis	k1gInSc1	předpis
kresby	kresba	k1gFnSc2	kresba
a	a	k8xC	
výpočtu	výpočet	k1gInSc2	výpočet
výměr	výměra	k1gFnPc2	výměra
</sent>
<sent id="6680">
akcidenční	akcidenční	k2eAgInSc4d1	akcidenční
písmo	písmo	k1gNnSc1	písmo
</sent>
<sent id="6681">
bezserifové	bezserifový	k2eAgNnSc4d1	bezserifové
písmo	písmo	k1gNnSc4	písmo
</sent>
<sent id="6682">
cicero	cicero	k1gNnSc4	cicero
</sent>
<sent id="6684">
deleatur	deleatura	k1gFnPc2	deleatura
</sent>
<sent id="6686">
egyptienka	egyptienka	k1gFnSc1	egyptienka
</sent>
<sent id="6688">
klasifikace	klasifikace	k1gFnSc1	klasifikace
(	(	kIx(	
<g/>
tiskových	tiskový	k2eAgInPc2d1	tiskový
<g/>
)	)	kIx)	
písem	písmo	k1gNnPc2	písmo
</sent>
<sent id="6689">
konturové	konturový	k2eAgNnSc4d1	konturové
písmo	písmo	k1gNnSc4	písmo
</sent>
<sent id="6690">
korektor	korektor	k1gInSc1	korektor
</sent>
<sent id="6691">
korekturní	korekturní	k2eAgInSc4d1	korekturní
znaménka	znaménko	k1gNnSc2	znaménko
</sent>
<sent id="6692">
lámání	lámání	k1gNnSc6	lámání
sazby	sazba	k1gFnSc2	sazba
</sent>
<sent id="6694">
písmový	písmový	k2eAgInSc4d1	písmový
výplněk	výplněk	k1gInSc4	výplněk
</sent>
<sent id="6695">
rodina	rodina	k1gFnSc1	rodina
písma	písmo	k1gNnSc2	písmo
</sent>
<sent id="6699">
sloupec	sloupec	k1gInSc1	sloupec
sazby	sazba	k1gFnSc2	sazba
</sent>
<sent id="6700">
stránka	stránka	k1gFnSc1	stránka
sazby	sazba	k1gFnSc2	sazba
</sent>
<sent id="6703">
šířka	šířka	k1gFnSc1	šířka
sloupce	sloupec	k1gInSc2	sloupec
sazby	sazba	k1gFnSc2	sazba
</sent>
<sent id="6704">
typografie	typografia	k1gFnSc2	typografia
</sent>
<sent id="6705">
účaří	účaří	k1gNnPc2	účaří
písma	písmo	k1gNnSc2	písmo
</sent>
<sent id="6707">
velikost	velikost	k1gFnSc1	velikost
písma	písmo	k1gNnSc2	písmo
</sent>
<sent id="6708">
základní	základní	k2eAgInSc4d1	základní
písmo	písmo	k1gNnSc1	písmo
</sent>
<sent id="6709">
datum	datum	k1gInSc1	datum
obrazu	obraz	k1gInSc2	obraz
</sent>
<sent id="6711">
lineární	lineární	k2eAgInSc1d1	lineární
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
systém	systém	k1gInSc1	systém
</sent>
<sent id="6712">
souřadnicová	souřadnicový	k2eAgFnSc1d1	souřadnicová
operace	operace	k1gFnSc1	operace
</sent>
<sent id="6713">
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
obrazu	obraz	k1gInSc2	obraz
</sent>
<sent id="6715">
technické	technický	k2eAgFnSc2d1	technická
datum	datum	k1gInSc4	datum
</sent>
<sent id="6716">
technický	technický	k2eAgInSc1d1	technický
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
</sent>
<sent id="6717">
výškové	výškový	k2eAgFnSc2d1	výšková
datum	datum	k1gInSc4	datum
</sent>
<sent id="6718">
výškový	výškový	k2eAgInSc1d1	výškový
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
</sent>
<sent id="6719">
výškový	výškový	k2eAgInSc1d1	výškový
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
systém	systém	k1gInSc1	systém
</sent>
<sent id="6720">
zobrazovací	zobrazovací	k2eAgInSc1d1	zobrazovací
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
referenční	referenční	k2eAgInSc1d1	referenční
systém	systém	k1gInSc1	systém
</sent>
<sent id="6722">
agregace	agregace	k1gFnSc1	agregace
</sent>
<sent id="6723">
aktualizace	aktualizace	k1gFnSc1	aktualizace
souboru	soubor	k1gInSc2	soubor
</sent>
<sent id="6725">
časové	časový	k2eAgNnSc4d1	časové
schéma	schéma	k1gNnSc4	schéma
</sent>
<sent id="6726">
datový	datový	k2eAgInSc4d1	datový
model	model	k1gInSc4	model
</sent>
<sent id="6727">
datový	datový	k2eAgInSc4d1	datový
typ	typ	k1gInSc4	typ
</sent>
<sent id="6728">
ohraničující	ohraničující	k2eAgInSc4d1	ohraničující
pravoúhelník	pravoúhelník	k1gInSc4	pravoúhelník
</sent>
<sent id="6729">
pole	pole	k1gNnSc2	pole
(	(	kIx(	
<g/>
v	v	k7c6	
geoinformatice	geoinformatika	k1gFnSc6	geoinformatika
<g/>
)	)	kIx)	
</sent>
<sent id="6730">
prostorové	prostorový	k2eAgNnSc4d1	prostorové
schéma	schéma	k1gNnSc4	schéma
</sent>
<sent id="6731">
správa	správa	k1gFnSc1	správa
dat	datum	k1gNnPc2	datum
</sent>
<sent id="6733">
vstupní	vstupní	k2eAgInSc4d1	vstupní
data	datum	k1gNnSc2	datum
</sent>
<sent id="6734">
výstupní	výstupní	k2eAgInSc4d1	výstupní
data	datum	k1gNnSc2	datum
</sent>
<sent id="7285">
digitální	digitální	k2eAgInSc4d1	digitální
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="7284">
EURAMET	EURAMET	kA	
</sent>
<sent id="6988">
hladká	hladký	k2eAgFnSc1d1	hladká
sazba	sazba	k1gFnSc1	sazba
</sent>
<sent id="6735">
DBP	DBP	kA	
</sent>
<sent id="6736">
DMVS	DMVS	kA	
</sent>
<sent id="6737">
EVRS	EVRS	kA	
</sent>
<sent id="6738">
Galileo	Galilea	k1gFnSc5	Galilea
</sent>
<sent id="6739">
IPI	IPI	kA	
</sent>
<sent id="6740">
TMO	tma	k1gFnSc5	tma
</sent>
<sent id="6741">
autotypický	autotypický	k2eAgInSc4d1	autotypický
pozitiv	pozitiv	k1gInSc4	pozitiv
</sent>
<sent id="6742">
barytový	barytový	k2eAgInSc4d1	barytový
papír	papír	k1gInSc4	papír
</sent>
<sent id="6743">
Brailleho	Braille	k1gMnSc2	Braille
písmo	písmo	k1gNnSc4	písmo
</sent>
<sent id="6745">
filmový	filmový	k2eAgInSc1d1	filmový
pozitiv	pozitiv	k1gInSc1	pozitiv
</sent>
<sent id="6746">
grafické	grafický	k2eAgInPc4d1	grafický
(	(	kIx(	
<g/>
zobrazovací	zobrazovací	k2eAgInPc4d1	zobrazovací
<g/>
)	)	kIx)	
prvky	prvek	k1gInPc4	prvek
</sent>
<sent id="6747">
hlazený	hlazený	k2eAgInSc4d1	hlazený
papír	papír	k1gInSc4	papír
</sent>
<sent id="6748">
ilustrační	ilustrační	k2eAgInSc4d1	ilustrační
papír	papír	k1gInSc4	papír
</sent>
<sent id="6750">
kartografické	kartografický	k2eAgNnSc1d1	kartografické
pojetí	pojetí	k1gNnSc1	pojetí
</sent>
<sent id="6751">
kartografický	kartografický	k2eAgInSc4d1	kartografický
plagiát	plagiát	k1gInSc4	plagiát
</sent>
<sent id="6753">
kartometrická	kartometrický	k2eAgFnSc1d1	kartometrický
pomůcka	pomůcka	k1gFnSc1	pomůcka
</sent>
<sent id="6754">
klížený	klížený	k2eAgInSc4d1	klížený
papír	papír	k1gInSc4	papír
</sent>
<sent id="6755">
laminovaný	laminovaný	k2eAgInSc4d1	laminovaný
papír	papír	k1gInSc4	papír
</sent>
<sent id="6756">
mapové	mapový	k2eAgNnSc4d1	mapové
pouzdro	pouzdro	k1gNnSc4	pouzdro
</sent>
<sent id="6757">
mezirámová	mezirámový	k2eAgFnSc1d1	mezirámový
plocha	plocha	k1gFnSc1	plocha
</sent>
<sent id="6758">
natíraný	natíraný	k2eAgInSc4d1	natíraný
papír	papír	k1gInSc4	papír
</sent>
<sent id="6759">
nehlazený	hlazený	k2eNgInSc4d1	hlazený
papír	papír	k1gInSc4	papír
</sent>
<sent id="6760">
neklížený	klížený	k2eNgInSc4d1	klížený
papír	papír	k1gInSc4	papír
</sent>
<sent id="6761">
nepotištěný	potištěný	k2eNgInSc4d1	nepotištěný
papír	papír	k1gInSc4	papír
</sent>
<sent id="6762">
okrajový	okrajový	k2eAgInSc4d1	okrajový
poledník	poledník	k1gInSc4	poledník
</sent>
<sent id="6763">
paperback	paperback	k1gInSc1	paperback
</sent>
<sent id="6764">
papírovina	papírovina	k1gFnSc1	papírovina
</sent>
<sent id="6765">
pásové	pásový	k2eAgNnSc4d1	pásové
kovové	kovový	k2eAgNnSc4d1	kovové
pravítko	pravítko	k1gNnSc4	pravítko
</sent>
<sent id="6767">
perspektivní	perspektivní	k2eAgInSc4d1	perspektivní
pohled	pohled	k1gInSc4	pohled
</sent>
<sent id="6769">
projetí	projetí	k1gNnSc1	projetí
(	(	kIx(	
<g/>
strojem	stroj	k1gInSc7	stroj
<g/>
)	)	kIx)	
</sent>
<sent id="6770">
prosvícení	prosvícení	k1gNnSc4	prosvícení
</sent>
<sent id="6771">
prosvítání	prosvítání	k1gNnSc6	prosvítání
tisku	tisk	k1gInSc2	tisk
</sent>
<sent id="6772">
protilehlý	protilehlý	k2eAgInSc4d1	protilehlý
poledník	poledník	k1gInSc4	poledník
</sent>
<sent id="6774">
předloha	předloha	k1gFnSc1	předloha
pro	pro	k7c4	
rytí	rytí	k1gNnSc4	rytí
</sent>
<sent id="6776">
přenášení	přenášení	k1gNnSc2	přenášení
čtvercovou	čtvercový	k2eAgFnSc7d1	čtvercová
sítí	síť	k1gFnSc7	síť
</sent>
<sent id="6777">
přepočet	přepočet	k1gInSc1	přepočet
měřítka	měřítko	k1gNnSc2	měřítko
</sent>
<sent id="6778">
přetisk	přetisk	k1gInSc1	přetisk
(	(	kIx(	
<g/>
obrazu	obraz	k1gInSc2	obraz
mapy	mapa	k1gFnSc2	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="6779">
přetisk	přetisk	k1gInSc1	přetisk
(	(	kIx(	
<g/>
vytištěné	vytištěný	k2eAgFnPc1d1	vytištěná
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="6780">
přetiskový	přetiskový	k2eAgInSc4d1	přetiskový
papír	papír	k1gInSc4	papír
</sent>
<sent id="6781">
ptačí	ptačit	k5eAaImIp3nS	
perspektiva	perspektiva	k1gFnSc1	perspektiva
</sent>
<sent id="6782">
reflektografický	reflektografický	k2eAgInSc4d1	reflektografický
papír	papír	k1gInSc4	papír
</sent>
<sent id="6783">
retušovací	retušovací	k2eAgInSc4d1	retušovací
prostředek	prostředek	k1gInSc4	prostředek
</sent>
<sent id="6784">
ruční	ruční	k2eAgInSc1d1	ruční
popis	popis	k1gInSc1	popis
</sent>
<sent id="6785">
síťovaná	síťovaný	k2eAgFnSc1d1	síťovaná
plocha	plocha	k1gFnSc1	plocha
</sent>
<sent id="6786">
strojově	strojově	k6eAd1	
hladký	hladký	k2eAgInSc4d1	hladký
papír	papír	k1gInSc4	papír
</sent>
<sent id="6787">
stříkací	stříkací	k2eAgFnSc1d1	stříkací
pistole	pistole	k1gFnSc1	pistole
</sent>
<sent id="6789">
tisková	tiskový	k2eAgFnSc1d1	tisková
plocha	plocha	k1gFnSc1	plocha
</sent>
<sent id="6790">
vojenská	vojenský	k2eAgFnSc1d1	vojenská
perspektiva	perspektiva	k1gFnSc1	perspektiva
</sent>
<sent id="6791">
vykrývací	vykrývací	k2eAgInSc4d1	vykrývací
prostředek	prostředek	k1gInSc4	prostředek
</sent>
<sent id="6793">
analýza	analýza	k1gFnSc1	analýza
rozptylu	rozptyl	k1gInSc2	rozptyl
</sent>
<sent id="6794">
distribuční	distribuční	k2eAgFnSc1d1	distribuční
funkce	funkce	k1gFnSc1	funkce
</sent>
<sent id="6795">
dvojrozměrná	dvojrozměrný	k2eAgFnSc1d1	dvojrozměrná
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="6796">
elipsoid	elipsoid	k1gInSc1	elipsoid
chyb	chyba	k1gFnPc2	chyba
</sent>
<sent id="6797">
Gaussova	Gaussův	k2eAgFnSc1d1	Gaussova
eliminace	eliminace	k1gFnSc1	eliminace
</sent>
<sent id="6798">
Gaussova	Gaussův	k2eAgFnSc1d1	Gaussova
křivka	křivka	k1gFnSc1	křivka
</sent>
<sent id="6799">
kovariance	kovariance	k1gFnSc1	kovariance
</sent>
<sent id="6800">
kovarianční	kovarianční	k2eAgFnSc1d1	kovarianční
matice	matice	k1gFnSc1	matice
</sent>
<sent id="6801">
regresní	regresní	k2eAgFnSc1d1	regresní
analýza	analýza	k1gFnSc1	analýza
</sent>
<sent id="6802">
elipsa	elipsa	k1gFnSc1	elipsa
chyb	chyba	k1gFnPc2	chyba
</sent>
<sent id="6803">
harmonický	harmonický	k2eAgInSc4d1	harmonický
průměr	průměr	k1gInSc4	průměr
</sent>
<sent id="6804">
Gaussovo	Gaussův	k2eAgNnSc4d1	Gaussovo
rozdělení	rozdělení	k1gNnSc4	rozdělení
</sent>
<sent id="6805">
modus	modus	k1gInSc1	modus
</sent>
<sent id="6806">
průměr	průměr	k1gInSc1	průměr
</sent>
<sent id="6807">
metoda	metoda	k1gFnSc1	metoda
nejmenších	malý	k2eAgInPc2d3	nejmenší
čtverců	čtverec	k1gInPc2	čtverec
(	(	kIx(	
<g/>
MNČ	MNČ	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="6808">
stupeň	stupeň	k1gInSc1	stupeň
volnosti	volnost	k1gFnSc2	volnost
</sent>
<sent id="6809">
kvadratický	kvadratický	k2eAgInSc4d1	kvadratický
průměr	průměr	k1gInSc4	průměr
</sent>
<sent id="6810">
hromadění	hromadění	k1gNnPc2	hromadění
chyb	chyba	k1gFnPc2	chyba
</sent>
<sent id="6811">
geometrický	geometrický	k2eAgInSc1d1	geometrický
průměr	průměr	k1gInSc1	průměr
</sent>
<sent id="6812">
medián	medián	k1gInSc1	medián
</sent>
<sent id="6813">
koeficient	koeficient	k1gInSc1	koeficient
šikmosti	šikmost	k1gFnSc2	šikmost
</sent>
<sent id="6814">
koeficient	koeficient	k1gInSc1	koeficient
špičatosti	špičatost	k1gFnSc2	špičatost
</sent>
<sent id="6815">
chyba	chyba	k1gFnSc1	chyba
měření	měření	k1gNnSc2	měření
</sent>
<sent id="6816">
Helmertova	Helmertův	k2eAgFnSc1d1	Helmertova
transformace	transformace	k1gFnSc1	transformace
</sent>
<sent id="6817">
matice	matice	k1gFnSc1	matice
rotace	rotace	k1gFnSc1	rotace
</sent>
<sent id="6818">
matice	matice	k1gFnSc2	matice
vah	váha	k1gFnPc2	váha
</sent>
<sent id="6819">
měřítkový	měřítkový	k2eAgInSc1d1	měřítkový
koeficient	koeficient	k1gInSc1	koeficient
</sent>
<sent id="7494">
nadbytečné	nadbytečný	k2eAgNnSc1d1	nadbytečné
měření	měření	k1gNnSc1	měření
</sent>
<sent id="7495">
osobní	osobní	k2eAgFnSc1d1	osobní
chyba	chyba	k1gFnSc1	chyba
</sent>
<sent id="6821">
Moloděnského	Moloděnského	k2eAgFnSc1d1	Moloděnského
transformace	transformace	k1gFnSc1	transformace
</sent>
<sent id="6822">
normální	normální	k2eAgFnSc1d1	normální
rovnice	rovnice	k1gFnSc1	rovnice
</sent>
<sent id="6823">
podobnostní	podobnostní	k2eAgFnSc1d1	podobnostní
transformace	transformace	k1gFnSc1	transformace
</sent>
<sent id="6824">
projektivní	projektivní	k2eAgFnSc1d1	projektivní
transformace	transformace	k1gFnSc1	transformace
</sent>
<sent id="6825">
shodnostní	shodnostní	k2eAgFnSc1d1	shodnostní
transformace	transformace	k1gFnSc1	transformace
</sent>
<sent id="6826">
transformace	transformace	k1gFnSc2	transformace
</sent>
<sent id="6827">
volná	volný	k2eAgFnSc1d1	volná
síť	síť	k1gFnSc1	síť
</sent>
<sent id="6828">
afinní	afinní	k2eAgFnSc1d1	afinní
transformace	transformace	k1gFnSc1	transformace
</sent>
<sent id="6829">
Helmertova	Helmertův	k2eAgFnSc1d1	Helmertova
sedmiprvková	sedmiprvkový	k2eAgFnSc1d1	sedmiprvkový
transformace	transformace	k1gFnSc1	transformace
</sent>
<sent id="6830">
Jungova	Jungův	k2eAgFnSc1d1	Jungova
transformace	transformace	k1gFnSc1	transformace
</sent>
<sent id="6831">
součinitel	součinitel	k1gInSc1	součinitel
konfidence	konfidence	k1gFnSc2	konfidence
</sent>
<sent id="6834">
DMS	DMS	kA	
</sent>
<sent id="6835">
DVISÚ	DVISÚ	kA	
</sent>
<sent id="6839">
NGPI	NGPI	kA	
</sent>
<sent id="6840">
NIPI	NIPI	kA	
</sent>
<sent id="6841">
OMP-V	OMP-V	k1gFnSc1	OMP-V
</sent>
<sent id="6842">
POSOPI	POSOPI	kA	
</sent>
<sent id="6843">
PVS	PVS	kA	
</sent>
<sent id="6846">
ÚKM-V	ÚKM-V	k1gFnSc1	ÚKM-V
</sent>
<sent id="6850">
VDP	VDP	kA	
</sent>
<sent id="6851">
stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
čtyřbarvotisk	čtyřbarvotisk	k1gInSc1	čtyřbarvotisk
</sent>
<sent id="6852">
dvoubarevný	dvoubarevný	k2eAgInSc4d1	dvoubarevný
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="6854">
kvantifikační	kvantifikační	k2eAgFnSc2d1	kvantifikační
tečky	tečka	k1gFnSc2	tečka
</sent>
<sent id="6855">
ofsetový	ofsetový	k2eAgMnSc1d1	ofsetový
tiskař	tiskař	k1gMnSc1	tiskař
</sent>
<sent id="6856">
plný	plný	k2eAgInSc4d1	plný
tón	tón	k1gInSc4	tón
</sent>
<sent id="6857">
podkladový	podkladový	k2eAgInSc4d1	podkladový
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="6858">
tabulka	tabulka	k1gFnSc1	tabulka
mapových	mapový	k2eAgFnPc2d1	mapová
značek	značka	k1gFnPc2	značka
</sent>
<sent id="6859">
technika	technika	k1gFnSc1	technika
kreslení	kreslení	k1gNnSc2	kreslení
</sent>
<sent id="6860">
termovakuové	termovakuová	k1gFnSc6	termovakuová
tvarování	tvarování	k1gNnSc1	tvarování
</sent>
<sent id="6861">
tisk	tisk	k1gInSc1	tisk
(	(	kIx(	
<g/>
map	mapa	k1gFnPc2	mapa
<g/>
)	)	kIx)	
se	s	k7c7	
zkrácenou	zkrácený	k2eAgFnSc7d1	zkrácená
barevnou	barevný	k2eAgFnSc7d1	barevná
stupnicí	stupnice	k1gFnSc7	stupnice
</sent>
<sent id="6862">
tisk	tisk	k1gInSc1	tisk
nového	nový	k2eAgNnSc2d1	nové
vydání	vydání	k1gNnSc2	vydání
</sent>
<sent id="6863">
tisk	tisk	k1gInSc1	tisk
z	z	k7c2	
hloubky	hloubka	k1gFnSc2	hloubka
</sent>
<sent id="6864">
tisk	tisk	k1gInSc1	tisk
z	z	k7c2	
plochy	plocha	k1gFnSc2	plocha
</sent>
<sent id="6865">
tisk	tisk	k1gInSc1	tisk
z	z	k7c2	
výšky	výška	k1gFnSc2	výška
</sent>
<sent id="6866">
tloušťka	tloušťka	k1gFnSc1	tloušťka
papíru	papír	k1gInSc2	papír
</sent>
<sent id="6867">
tón	tón	k1gInSc1	tón
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="6868">
tříbarvotisk	tříbarvotisk	k1gInSc1	tříbarvotisk
</sent>
<sent id="6869">
typ	typ	k1gInSc1	typ
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6870">
absolutní	absolutní	k2eAgInSc4d1	absolutní
znázornění	znázornění	k1gNnSc1	znázornění
</sent>
<sent id="6871">
abstraktní	abstraktní	k2eAgFnSc1d1	abstraktní
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6872">
areálové	areálový	k2eAgNnSc4d1	areálové
znázornění	znázornění	k1gNnSc4	znázornění
</sent>
<sent id="6874">
barevný	barevný	k2eAgInSc1d1	barevný
fotomechanický	fotomechanický	k2eAgInSc1d1	fotomechanický
výtažek	výtažek	k1gInSc1	výtažek
</sent>
<sent id="6875">
blednutí	blednutí	k1gNnPc2	blednutí
barev	barva	k1gFnPc2	barva
</sent>
<sent id="6876">
bodová	bodový	k2eAgFnSc1d1	bodová
(	(	kIx(	
<g/>
polohová	polohový	k2eAgFnSc1d1	polohová
<g/>
)	)	kIx)	
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6877">
čárová	čárový	k2eAgFnSc1d1	čárová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6878">
fotografická	fotografický	k2eAgFnSc1d1	fotografická
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="6879">
fotografické	fotografický	k2eAgNnSc4d1	fotografické
zmenšování	zmenšování	k1gNnSc4	zmenšování
</sent>
<sent id="6880">
fotografické	fotografický	k2eAgNnSc4d1	fotografické
zvětšování	zvětšování	k1gNnSc4	zvětšování
</sent>
<sent id="6881">
fotomechanické	fotomechanický	k2eAgNnSc4d1	fotomechanický
vinětování	vinětování	k1gNnSc4	vinětování
</sent>
<sent id="6882">
grafické	grafický	k2eAgNnSc1d1	grafické
zmenšování	zmenšování	k1gNnSc1	zmenšování
</sent>
<sent id="6883">
grafické	grafický	k2eAgNnSc1d1	grafické
zvětšování	zvětšování	k1gNnSc1	zvětšování
</sent>
<sent id="6884">
hloubková	hloubkový	k2eAgFnSc1d1	hloubková
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="6887">
krychlová	krychlový	k2eAgFnSc1d1	krychlová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6888">
kulová	kulový	k2eAgFnSc1d1	kulová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6889">
mechanické	mechanický	k2eAgNnSc4d1	mechanické
zmenšování	zmenšování	k1gNnSc4	zmenšování
</sent>
<sent id="6890">
mechanické	mechanický	k2eAgNnSc4d1	mechanické
zvětšování	zvětšování	k1gNnSc4	zvětšování
</sent>
<sent id="6891">
měřítková	měřítkový	k2eAgFnSc1d1	měřítková
věrnost	věrnost	k1gFnSc1	věrnost
</sent>
<sent id="6892">
nové	nový	k2eAgNnSc4d1	nové
vydání	vydání	k1gNnSc4	vydání
</sent>
<sent id="6893">
odvozená	odvozený	k2eAgFnSc1d1	odvozená
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6894">
ochranná	ochranný	k2eAgFnSc1d1	ochranná
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="6895">
opravené	opravený	k2eAgNnSc4d1	opravené
vydání	vydání	k1gNnSc4	vydání
</sent>
<sent id="6896">
optické	optický	k2eAgNnSc1d1	optické
zmenšování	zmenšování	k1gNnSc1	zmenšování
</sent>
<sent id="6897">
optické	optický	k2eAgNnSc1d1	optické
zvětšování	zvětšování	k1gNnSc1	zvětšování
</sent>
<sent id="6898">
platné	platný	k2eAgNnSc1d1	platné
vydání	vydání	k1gNnSc1	vydání
</sent>
<sent id="6899">
proporcionální	proporcionální	k2eAgFnSc1d1	proporcionální
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6900">
půdorysná	půdorysný	k2eAgFnSc1d1	půdorysná
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6901">
schematická	schematický	k2eAgFnSc1d1	schematická
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6902">
směrové	směrový	k2eAgNnSc1d1	směrové
zkreslení	zkreslení	k1gNnSc1	zkreslení
</sent>
<sent id="6903">
tělesová	tělesový	k2eAgFnSc1d1	tělesová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6904">
vlastnosti	vlastnost	k1gFnPc4	vlastnost
papíru	papír	k1gInSc2	papír
</sent>
<sent id="6905">
vydání	vydání	k1gNnSc6	vydání
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="6906">
vyrovnávací	vyrovnávací	k2eAgInSc4d1	vyrovnávací
zobrazení	zobrazení	k1gNnSc1	zobrazení
</sent>
<sent id="6907">
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
na	na	k7c6	
mapě	mapa	k1gFnSc6	mapa
</sent>
<sent id="6908">
zjasnění	zjasnění	k1gNnSc6	zjasnění
barvy	barva	k1gFnSc2	barva
</sent>
<sent id="6910">
zrnění	zrnění	k1gNnSc2	zrnění
(	(	kIx(	
<g/>
tiskové	tiskový	k2eAgFnSc2d1	tisková
desky	deska	k1gFnSc2	deska
<g/>
)	)	kIx)	
</sent>
<sent id="6911">
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
vydání	vydání	k1gNnSc1	vydání
</sent>
<sent id="6912">
arch	arch	k1gInSc1	arch
</sent>
<sent id="6913">
archový	archový	k2eAgInSc4d1	archový
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="6914">
autorský	autorský	k2eAgInSc1d1	autorský
výtisk	výtisk	k1gInSc1	výtisk
</sent>
<sent id="6915">
brožura	brožura	k1gFnSc1	brožura
</sent>
<sent id="6917">
drátovka	drátovka	k1gFnSc1	drátovka
</sent>
<sent id="6918">
imprimovaný	imprimovaný	k2eAgInSc4d1	imprimovaný
rukopis	rukopis	k1gInSc4	rukopis
</sent>
<sent id="6919">
kapsový	kapsový	k2eAgInSc1d1	kapsový
skládací	skládací	k2eAgInSc1d1	skládací
stroj	stroj	k1gInSc1	stroj
</sent>
<sent id="6920">
knihařská	knihařský	k2eAgFnSc1d1	knihařská
maketa	maketa	k1gFnSc1	maketa
</sent>
<sent id="6921">
knihvazačství	knihvazačství	k1gNnSc4	knihvazačství
</sent>
<sent id="6922">
knižní	knižní	k2eAgInSc4d1	knižní
blok	blok	k1gInSc4	blok
</sent>
<sent id="6923">
knižní	knižní	k2eAgFnSc2d1	knižní
desky	deska	k1gFnSc2	deska
</sent>
<sent id="6924">
knižní	knižní	k2eAgFnSc1d1	knižní
složka	složka	k1gFnSc1	složka
</sent>
<sent id="6925">
knižní	knižní	k2eAgFnSc1d1	knižní
vazba	vazba	k1gFnSc1	vazba
</sent>
<sent id="6926">
křížový	křížový	k2eAgInSc4d1	křížový
lom	lom	k1gInSc4	lom
</sent>
<sent id="6927">
lepená	lepený	k2eAgFnSc1d1	lepená
vazba	vazba	k1gFnSc1	vazba
</sent>
<sent id="6928">
lom	lom	k1gInSc1	lom
(	(	kIx(	
<g/>
archu	arch	k1gInSc2	arch
<g/>
)	)	kIx)	
</sent>
<sent id="6929">
měkká	měkký	k2eAgFnSc1d1	měkká
vazba	vazba	k1gFnSc1	vazba
</sent>
<sent id="6930">
nepřímý	přímý	k2eNgInSc4d1	nepřímý
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="6932">
niťovka	niťovka	k1gFnSc1	niťovka
</sent>
<sent id="6933">
nožový	nožový	k2eAgInSc1d1	nožový
skládací	skládací	k2eAgInSc1d1	skládací
stroj	stroj	k1gInSc1	stroj
</sent>
<sent id="6935">
ořezávání	ořezávání	k1gNnPc2	ořezávání
(	(	kIx(	
<g/>
archů	arch	k1gInPc2	arch
<g/>
)	)	kIx)	
na	na	k7c4	
formát	formát	k1gInSc4	formát
</sent>
<sent id="6936">
paralelní	paralelní	k2eAgInSc4d1	paralelní
lom	lom	k1gInSc4	lom
</sent>
<sent id="6937">
podélný	podélný	k2eAgInSc4d1	podélný
lom	lom	k1gInSc4	lom
</sent>
<sent id="6938">
poloplátěná	poloplátěný	k2eAgFnSc1d1	poloplátěná
vazba	vazba	k1gFnSc1	vazba
</sent>
<sent id="6939">
produkční	produkční	k2eAgInSc4d1	produkční
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="6940">
předsádka	předsádka	k1gFnSc1	předsádka
</sent>
<sent id="6941">
příčný	příčný	k2eAgInSc4d1	příčný
lom	lom	k1gInSc4	lom
</sent>
<sent id="6942">
přímý	přímý	k2eAgInSc4d1	přímý
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="6943">
ruční	ruční	k2eAgFnSc1d1	ruční
vazba	vazba	k1gFnSc1	vazba
</sent>
<sent id="6944">
skládací	skládací	k2eAgInSc1d1	skládací
stroj	stroj	k1gInSc1	stroj
</sent>
<sent id="6945">
snášecí	snášecí	k2eAgInSc1d1	snášecí
stroj	stroj	k1gInSc1	stroj
</sent>
<sent id="6946">
spirálová	spirálový	k2eAgFnSc1d1	spirálová
vazba	vazba	k1gFnSc1	vazba
</sent>
<sent id="6947">
šití	šití	k1gNnSc6	šití
(	(	kIx(	
<g/>
knižního	knižní	k2eAgInSc2d1	knižní
bloku	blok	k1gInSc2	blok
<g/>
)	)	kIx)	
</sent>
<sent id="6948">
šroubová	šroubový	k2eAgFnSc1d1	šroubová
vazba	vazba	k1gFnSc1	vazba
</sent>
<sent id="6950">
tuhá	tuhý	k2eAgFnSc1d1	tuhá
vazba	vazba	k1gFnSc1	vazba
</sent>
<sent id="6951">
vývěsný	vývěsný	k2eAgInSc4d1	vývěsný
arch	arch	k1gInSc4	arch
</sent>
<sent id="6952">
zavěšování	zavěšování	k1gNnSc2	zavěšování
(	(	kIx(	
<g/>
knižního	knižní	k2eAgInSc2d1	knižní
bloku	blok	k1gInSc2	blok
<g/>
)	)	kIx)	
</sent>
<sent id="6953">
archiv	archiv	k1gInSc4	archiv
tiskových	tiskový	k2eAgInPc2d1	tiskový
podkladů	podklad	k1gInPc2	podklad
</sent>
<sent id="6954">
archivování	archivování	k1gNnPc2	archivování
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="6955">
barevná	barevný	k2eAgFnSc1d1	barevná
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
stupnice	stupnice	k1gFnSc1	stupnice
</sent>
<sent id="6956">
barevná	barevný	k2eAgFnSc1d1	barevná
stupnice	stupnice	k1gFnSc1	stupnice
výškových	výškový	k2eAgFnPc2d1	výšková
vrstev	vrstva	k1gFnPc2	vrstva
</sent>
<sent id="6957">
hodnotový	hodnotový	k2eAgInSc4d1	hodnotový
stupeň	stupeň	k1gInSc4	stupeň
</sent>
<sent id="6958">
hustotní	hustotní	k2eAgInSc4d1	hustotní
stupeň	stupeň	k1gInSc4	stupeň
</sent>
<sent id="6959">
magnetický	magnetický	k2eAgInSc4d1	magnetický
sever	sever	k1gInSc4	sever
</sent>
<sent id="6960">
mapový	mapový	k2eAgInSc4d1	mapový
sever	sever	k1gInSc4	sever
</sent>
<sent id="6961">
plstěncová	plstěncový	k2eAgFnSc1d1	plstěncový
strana	strana	k1gFnSc1	strana
papíru	papír	k1gInSc2	papír
</sent>
<sent id="6962">
popisovací	popisovací	k2eAgFnSc1d1	popisovací
šablona	šablona	k1gFnSc1	šablona
</sent>
<sent id="6963">
pravoúhlá	pravoúhlý	k2eAgFnSc1d1	pravoúhlá
rovinná	rovinný	k2eAgFnSc1d1	rovinná
síť	síť	k1gFnSc1	síť
</sent>
<sent id="7446">
soubor	soubor	k1gInSc1	soubor
tiskových	tiskový	k2eAgFnPc2d1	tisková
forem	forma	k1gFnPc2	forma
</sent>
<sent id="6965">
sítová	sítový	k2eAgFnSc1d1	sítová
strana	strana	k1gFnSc1	strana
papíru	papír	k1gInSc2	papír
</sent>
<sent id="6966">
skicování	skicování	k1gNnSc4	skicování
</sent>
<sent id="6967">
sklad	sklad	k1gInSc1	sklad
papíru	papír	k1gInSc2	papír
</sent>
<sent id="6968">
sklad	sklad	k1gInSc1	sklad
tiskových	tiskový	k2eAgFnPc2d1	tisková
desek	deska	k1gFnPc2	deska
</sent>
<sent id="6969">
absorpce	absorpce	k1gFnSc1	absorpce
barvy	barva	k1gFnSc2	barva
</sent>
<sent id="6970">
absorpce	absorpce	k1gFnSc1	absorpce
vody	voda	k1gFnSc2	voda
</sent>
<sent id="6971">
archový	archový	k2eAgInSc4d1	archový
samonakladač	samonakladač	k1gInSc4	samonakladač
</sent>
<sent id="6972">
archový	archový	k2eAgInSc4d1	archový
vykladač	vykladač	k1gInSc4	vykladač
</sent>
<sent id="6973">
azobarvivo	azobarvivo	k1gNnSc4	azobarvivo
</sent>
<sent id="6974">
barevnice	barevnice	k1gFnSc1	barevnice
</sent>
<sent id="6975">
barevník	barevník	k1gInSc1	barevník
</sent>
<sent id="6976">
citlivá	citlivý	k2eAgFnSc1d1	citlivá
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="6977">
čtyřbarevný	čtyřbarevný	k2eAgInSc1d1	čtyřbarevný
tisk	tisk	k1gInSc1	tisk
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6979">
edice	edice	k1gFnSc1	edice
</sent>
<sent id="6980">
errata	errata	k1gNnPc4	errata
</sent>
<sent id="6981">
faldovatění	faldovatění	k1gNnSc2	faldovatění
(	(	kIx(	
<g/>
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	
</sent>
<sent id="6982">
fermež	fermež	k1gFnSc1	fermež
</sent>
<sent id="6985">
fotopolymer	fotopolymer	k1gInSc1	fotopolymer
</sent>
<sent id="6987">
halace	halace	k1gFnSc1	halace
</sent>
<sent id="6989">
hřbet	hřbet	k1gInSc1	hřbet
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="6990">
hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
drážka	drážka	k1gFnSc1	drážka
</sent>
<sent id="6991">
hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6992">
hřbetník	hřbetník	k1gInSc1	hřbetník
</sent>
<sent id="6993">
chromokarton	chromokarton	k1gInSc1	chromokarton
</sent>
<sent id="6994">
klimatizace	klimatizace	k1gFnSc1	klimatizace
papíru	papír	k1gInSc2	papír
</sent>
<sent id="6995">
oboustranný	oboustranný	k2eAgInSc4d1	oboustranný
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="6996">
plátěná	plátěný	k2eAgFnSc1d1	plátěná
vazba	vazba	k1gFnSc1	vazba
</sent>
<sent id="6998">
klížení	klížení	k1gNnSc2	klížení
knižního	knižní	k2eAgInSc2d1	knižní
bloku	blok	k1gInSc2	blok
</sent>
<sent id="6999">
klopa	klopa	k1gFnSc1	klopa
</sent>
<sent id="7000">
knihař	knihař	k1gMnSc1	knihař
</sent>
<sent id="7002">
knihařský	knihařský	k2eAgInSc1d1	knihařský
lis	lis	k1gInSc1	lis
</sent>
<sent id="7003">
knižní	knižní	k2eAgInSc4d1	knižní
přebal	přebal	k1gInSc4	přebal
</sent>
<sent id="7004">
soutisk	soutisk	k1gInSc1	soutisk
barev	barva	k1gFnPc2	barva
</sent>
<sent id="7005">
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="7006">
srovnatelnost	srovnatelnost	k1gFnSc1	srovnatelnost
měřítek	měřítko	k1gNnPc2	měřítko
</sent>
<sent id="7007">
střed	střed	k1gInSc1	střed
mapové	mapový	k2eAgFnSc2d1	mapová
značky	značka	k1gFnSc2	značka
</sent>
<sent id="7008">
stupnice	stupnice	k1gFnSc2	stupnice
barev	barva	k1gFnPc2	barva
</sent>
<sent id="7009">
styl	styl	k1gInSc1	styl
atlasu	atlas	k1gInSc2	atlas
</sent>
<sent id="7010">
styl	styl	k1gInSc1	styl
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="7011">
systém	systém	k1gInSc1	systém
kladu	klad	k1gInSc2	klad
listů	list	k1gInPc2	list
</sent>
<sent id="7012">
systém	systém	k1gInSc1	systém
označování	označování	k1gNnSc2	označování
listů	list	k1gInPc2	list
</sent>
<sent id="7013">
šířka	šířka	k1gFnSc1	šířka
písma	písmo	k1gNnSc2	písmo
</sent>
<sent id="7016">
zeměpisný	zeměpisný	k2eAgInSc4d1	zeměpisný
sever	sever	k1gInSc4	sever
</sent>
<sent id="7017">
značková	značkový	k2eAgFnSc1d1	značková
stupnice	stupnice	k1gFnSc1	stupnice
</sent>
<sent id="7018">
geodetické	geodetický	k2eAgInPc1d1	geodetický
základy	základ	k1gInPc1	základ
</sent>
<sent id="7019">
barevná	barevný	k2eAgFnSc1d1	barevná
vydatnost	vydatnost	k1gFnSc1	vydatnost
</sent>
<sent id="7020">
barevný	barevný	k2eAgInSc4d1	barevný
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="7021">
lístkový	lístkový	k2eAgInSc4d1	lístkový
rukopis	rukopis	k1gInSc4	rukopis
</sent>
<sent id="7022">
míšení	míšení	k1gNnPc2	míšení
barev	barva	k1gFnPc2	barva
</sent>
<sent id="7023">
návrh	návrh	k1gInSc1	návrh
úpravy	úprava	k1gFnSc2	úprava
sazby	sazba	k1gFnSc2	sazba
</sent>
<sent id="7024">
obrazec	obrazec	k1gInSc1	obrazec
sazby	sazba	k1gFnSc2	sazba
</sent>
<sent id="7026">
podtitul	podtitul	k1gInSc1	podtitul
</sent>
<sent id="7028">
polygrafie	polygrafie	k1gFnSc1	polygrafie
</sent>
<sent id="7031">
reprodukční	reprodukční	k2eAgInSc4d1	reprodukční
náhled	náhled	k1gInSc4	náhled
</sent>
<sent id="7032">
sítotisková	sítotiskový	k2eAgFnSc1d1	sítotisková
barva	barva	k1gFnSc1	barva
</sent>
<sent id="7034">
stránkování	stránkování	k1gNnSc4	stránkování
</sent>
<sent id="7035">
typograf	typograf	k1gMnSc1	typograf
</sent>
<sent id="7131">
ustalovač	ustalovač	k1gInSc1	ustalovač
</sent>
<sent id="7037">
vakát	vakát	k1gInSc1	vakát
</sent>
<sent id="7202">
MO	MO	kA	
<g/>
/	/	kIx~	
<g/>
MČ	MČ	kA	
</sent>
<sent id="7040">
vyznačovací	vyznačovací	k2eAgInSc4d1	vyznačovací
písmo	písmo	k1gNnSc1	písmo
</sent>
<sent id="7041">
zasychavost	zasychavost	k1gFnSc1	zasychavost
barvy	barva	k1gFnSc2	barva
</sent>
<sent id="7042">
barevný	barevný	k2eAgInSc4d1	barevný
výtažek	výtažek	k1gInSc4	výtažek
</sent>
<sent id="7043">
beztlaký	beztlaký	k2eAgInSc4d1	beztlaký
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="7044">
denzitometrie	denzitometrie	k1gFnSc1	denzitometrie
</sent>
<sent id="7045">
expoziční	expoziční	k2eAgFnSc1d1	expoziční
doba	doba	k1gFnSc1	doba
</sent>
<sent id="7046">
faksimile	faksimile	k1gNnSc4	faksimile
</sent>
<sent id="7047">
formát	formát	k1gInSc1	formát
</sent>
<sent id="7048">
kopie	kopie	k1gFnSc2	kopie
</sent>
<sent id="7114">
neperiodická	periodický	k2eNgFnSc1d1	neperiodická
publikace	publikace	k1gFnSc1	publikace
</sent>
<sent id="7052">
reprodukční	reprodukční	k2eAgFnSc1d1	reprodukční
předloha	předloha	k1gFnSc1	předloha
</sent>
<sent id="7053">
reprodukční	reprodukční	k2eAgFnSc1d1	reprodukční
technika	technika	k1gFnSc1	technika
</sent>
<sent id="7054">
rovinný	rovinný	k2eAgInSc4d1	rovinný
tisk	tisk	k1gInSc4	tisk
</sent>
<sent id="7055">
textová	textový	k2eAgFnSc1d1	textová
předloha	předloha	k1gFnSc1	předloha
</sent>
<sent id="7056">
tisk	tisk	k1gInSc1	tisk
</sent>
<sent id="7058">
závoj	závoj	k1gInSc1	závoj
</sent>
<sent id="7059">
zrnitost	zrnitost	k1gFnSc1	zrnitost
</sent>
<sent id="7060">
držák	držák	k1gInSc1	držák
předlohy	předloha	k1gFnSc2	předloha
</sent>
<sent id="7061">
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
skládací	skládací	k2eAgInSc1d1	skládací
stroj	stroj	k1gInSc1	stroj
</sent>
<sent id="7062">
lepicí	lepicí	k2eAgFnSc1d1	lepicí
fólie	fólie	k1gFnSc1	fólie
</sent>
<sent id="7063">
paleta	paleta	k1gFnSc1	paleta
</sent>
<sent id="7064">
pevnost	pevnost	k1gFnSc1	pevnost
(	(	kIx(	
<g/>
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	
v	v	k7c6	
lomu	lom	k1gInSc6	lom
</sent>
<sent id="7065">
polyester	polyester	k1gInSc1	polyester
</sent>
<sent id="7066">
polyvinylalkohol	polyvinylalkohol	k1gInSc1	polyvinylalkohol
(	(	kIx(	
<g/>
PVA	PVA	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7067">
polyvinylchlorid	polyvinylchlorid	k1gInSc1	polyvinylchlorid
(	(	kIx(	
<g/>
PVC	PVC	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7068">
pořadí	pořadí	k1gNnSc2	pořadí
barev	barva	k1gFnPc2	barva
</sent>
<sent id="7069">
přídavek	přídavka	k1gFnPc2	přídavka
(	(	kIx(	
<g/>
archů	arch	k1gInPc2	arch
<g/>
)	)	kIx)	
papíru	papír	k1gInSc2	papír
</sent>
<sent id="7070">
příprava	příprava	k1gFnSc1	příprava
stroje	stroj	k1gInSc2	stroj
k	k	k7c3	
tisku	tisk	k1gInSc3	tisk
</sent>
<sent id="7071">
ražení	ražení	k1gNnPc2	ražení
</sent>
<sent id="7072">
ražení	ražení	k1gNnPc2	ražení
barvou	barva	k1gFnSc7	barva
</sent>
<sent id="7073">
ražení	ražení	k1gNnPc2	ražení
fólií	fólie	k1gFnPc2	fólie
</sent>
<sent id="7074">
rovinnost	rovinnost	k1gFnSc1	rovinnost
papíru	papír	k1gInSc2	papír
</sent>
<sent id="7076">
řezací	řezací	k2eAgInSc1d1	řezací
stroj	stroj	k1gInSc1	stroj
</sent>
<sent id="7077">
řezací	řezací	k2eAgFnPc4d1	řezací
značky	značka	k1gFnPc4	značka
</sent>
<sent id="7078">
sloupcový	sloupcový	k2eAgInSc1d1	sloupcový
obtah	obtah	k1gInSc1	obtah
</sent>
<sent id="7079">
křídový	křídový	k2eAgInSc4d1	křídový
kartón	kartón	k1gInSc4	kartón
</sent>
<sent id="7080">
lakování	lakování	k1gNnSc4	lakování
</sent>
<sent id="7081">
lepení	lepení	k1gNnSc4	lepení
</sent>
<sent id="7082">
litografický	litografický	k2eAgInSc4d1	litografický
kámen	kámen	k1gInSc4	kámen
</sent>
<sent id="7084">
oblení	oblení	k1gNnPc4	oblení
hřbetů	hřbet	k1gMnPc2	hřbet
bloků	blok	k1gInPc2	blok
</sent>
<sent id="7085">
odřez	odřez	k1gInSc1	odřez
</sent>
<sent id="7087">
optický	optický	k2eAgInSc4d1	optický
hranol	hranol	k1gInSc4	hranol
</sent>
<sent id="7089">
pozemní	pozemní	k2eAgFnSc1d1	pozemní
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
</sent>
<sent id="7092">
rozchod	rozchod	k1gInSc1	rozchod
koleje	kolej	k1gFnSc2	kolej
</sent>
<sent id="7094">
traťová	traťový	k2eAgFnSc1d1	traťová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="7095">
visutá	visutý	k2eAgFnSc1d1	visutá
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
</sent>
<sent id="7098">
laserový	laserový	k2eAgInSc4d1	laserový
dálkoměr	dálkoměr	k1gInSc4	dálkoměr
</sent>
<sent id="7100">
DOP	DOP	kA	
</sent>
<sent id="7101">
GDOP	GDOP	kA	
</sent>
<sent id="7102">
HDOP	HDOP	kA	
</sent>
<sent id="7103">
PDOP	PDOP	kA	
</sent>
<sent id="7104">
RDOP	RDOP	kA	
</sent>
<sent id="7105">
TDOP	TDOP	kA	
</sent>
<sent id="7106">
VDOP	VDOP	kA	
</sent>
<sent id="7208">
Katastrální	katastrální	k2eAgInSc1d1	katastrální
úřad	úřad	k1gInSc1	úřad
(	(	kIx(	
<g/>
KÚ	KÚ	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7107">
archový	archový	k2eAgInSc4d1	archový
ofsetový	ofsetový	k2eAgInSc4d1	ofsetový
stroj	stroj	k1gInSc4	stroj
</sent>
<sent id="7108">
barva	barva	k1gFnSc1	barva
</sent>
<sent id="7109">
digitální	digitální	k2eAgInSc4d1	digitální
tiskový	tiskový	k2eAgInSc4d1	tiskový
stroj	stroj	k1gInSc4	stroj
</sent>
<sent id="7110">
doplněné	doplněná	k1gFnSc6	doplněná
vydání	vydání	k1gNnSc1	vydání
</sent>
<sent id="7111">
fotografický	fotografický	k2eAgInSc4d1	fotografický
materiál	materiál	k1gInSc4	materiál
</sent>
<sent id="7112">
grafická	grafický	k2eAgFnSc1d1	grafická
úprava	úprava	k1gFnSc1	úprava
</sent>
<sent id="7117">
perový	perový	k2eAgInSc4d1	perový
negativ	negativ	k1gInSc4	negativ
</sent>
<sent id="7119">
polyesterová	polyesterový	k2eAgFnSc1d1	polyesterová
fólie	fólie	k1gFnSc1	fólie
</sent>
<sent id="7120">
polyvinylchloridová	polyvinylchloridový	k2eAgFnSc1d1	polyvinylchloridový
fólie	fólie	k1gFnSc1	fólie
</sent>
<sent id="7121">
průsvitnost	průsvitnost	k1gFnSc1	průsvitnost
barvy	barva	k1gFnSc2	barva
</sent>
<sent id="7122">
průsvitnost	průsvitnost	k1gFnSc1	průsvitnost
papíru	papír	k1gInSc2	papír
</sent>
<sent id="7124">
redakční	redakční	k2eAgFnSc1d1	redakční
uzávěrka	uzávěrka	k1gFnSc1	uzávěrka
</sent>
<sent id="7125">
rozsah	rozsah	k1gInSc1	rozsah
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="7126">
šití	šití	k1gNnPc4	šití
na	na	k7c4	
stříšku	stříška	k1gFnSc4	stříška
</sent>
<sent id="7127">
temná	temnat	k5eAaImIp3nS	
komora	komora	k1gFnSc1	komora
</sent>
<sent id="7128">
tónový	tónový	k2eAgInSc4d1	tónový
negativ	negativ	k1gInSc4	negativ
</sent>
<sent id="7129">
třínožový	třínožový	k2eAgInSc1d1	třínožový
řezací	řezací	k2eAgInSc1d1	řezací
stroj	stroj	k1gInSc1	stroj
</sent>
<sent id="7130">
typografická	typografický	k2eAgFnSc1d1	typografická
úprava	úprava	k1gFnSc1	úprava
</sent>
<sent id="7133">
vlepená	vlepený	k2eAgFnSc1d1	vlepená
příloha	příloha	k1gFnSc1	příloha
</sent>
<sent id="7134">
vlhčící	vlhčící	k2eAgFnSc1d1	vlhčící
souprava	souprava	k1gFnSc1	souprava
</sent>
<sent id="7135">
vlnění	vlnění	k1gNnSc2	vlnění
papíru	papír	k1gInSc2	papír
</sent>
<sent id="7137">
vročení	vročení	k1gNnSc4	vročení
</sent>
<sent id="7139">
vývojka	vývojka	k1gFnSc1	vývojka
</sent>
<sent id="7140">
závěrka	závěrka	k1gFnSc1	závěrka
objektivu	objektiv	k1gInSc2	objektiv
</sent>
<sent id="7141">
geovizualizace	geovizualizace	k1gFnSc1	geovizualizace
</sent>
<sent id="7142">
kompozice	kompozice	k1gFnSc1	kompozice
</sent>
<sent id="7143">
mapová	mapový	k2eAgFnSc1d1	mapová
kompozice	kompozice	k1gFnSc1	kompozice
</sent>
<sent id="7144">
pozemní	pozemní	k2eAgFnSc4d1	pozemní
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
vzorků	vzorek	k1gInPc2	vzorek
</sent>
<sent id="7145">
teorie	teorie	k1gFnSc2	teorie
chyb	chyba	k1gFnPc2	chyba
</sent>
<sent id="7146">
APOS	APOS	kA	
</sent>
<sent id="7147">
ASG-EUPOS	ASG-EUPOS	k1gFnSc1	ASG-EUPOS
</sent>
<sent id="7149">
EUPOS	EUPOS	kA	
</sent>
<sent id="7150">
IGFS	IGFS	kA	
</sent>
<sent id="7151">
SAPOS	SAPOS	kA	
</sent>
<sent id="7152">
SINEX	SINEX	kA	
</sent>
<sent id="7157">
mobilní	mobilní	k2eAgInSc4d1	mobilní
laserový	laserový	k2eAgInSc4d1	laserový
skener	skener	k1gInSc4	skener
</sent>
<sent id="7158">
mobilní	mobilní	k2eAgInSc1d1	mobilní
mapovací	mapovací	k2eAgInSc1d1	mapovací
systém	systém	k1gInSc1	systém
</sent>
<sent id="7159">
mračno	mračno	k1gNnSc4	mračno
bodů	bod	k1gInPc2	bod
</sent>
<sent id="7160">
pozemní	pozemní	k2eAgInSc4d1	pozemní
laserový	laserový	k2eAgInSc4d1	laserový
skener	skener	k1gInSc4	skener
</sent>
<sent id="7165">
určení	určení	k1gNnSc4	určení
prostorové	prostorový	k2eAgFnSc2d1	prostorová
(	(	kIx(	
<g/>
trojrozměrné	trojrozměrný	k2eAgFnSc2d1	trojrozměrná
<g/>
)	)	kIx)	
polohy	poloha	k1gFnSc2	poloha
</sent>
<sent id="7166">
geodetické	geodetický	k2eAgFnSc2d1	geodetická
metody	metoda	k1gFnSc2	metoda
</sent>
<sent id="7171">
dálkoměr	dálkoměr	k1gInSc1	dálkoměr
bez	bez	k7c2	
odražeče	odražeč	k1gInSc2	odražeč
</sent>
<sent id="7172">
areál	areál	k1gInSc1	areál
(	(	kIx(	
<g/>
plochy	plocha	k1gFnPc1	plocha
<g/>
)	)	kIx)	
</sent>
<sent id="7173">
bibliografický	bibliografický	k2eAgInSc4d1	bibliografický
záznam	záznam	k1gInSc4	záznam
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="7174">
faktor	faktor	k1gInSc1	faktor
zmenšení	zmenšení	k1gNnSc2	zmenšení
</sent>
<sent id="7175">
faktor	faktor	k1gInSc1	faktor
zvětšení	zvětšení	k1gNnSc2	zvětšení
</sent>
<sent id="7176">
filmový	filmový	k2eAgInSc1d1	filmový
negativ	negativ	k1gInSc1	negativ
</sent>
<sent id="7177">
gnómonická	gnómonický	k2eAgFnSc1d1	gnómonický
projekce	projekce	k1gFnSc1	projekce
</sent>
<sent id="7178">
interval	interval	k1gInSc1	interval
souřadnicové	souřadnicový	k2eAgFnSc2d1	souřadnicová
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="7179">
jižní	jižní	k2eAgFnSc1d1	jižní
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
</sent>
<sent id="7589">
aparatura	aparatura	k1gFnSc1	aparatura
GNSS	GNSS	kA	
</sent>
<sent id="7590">
ionosféra	ionosféra	k1gFnSc1	ionosféra
</sent>
<sent id="7181">
nezkreslená	zkreslený	k2eNgFnSc1d1	nezkreslená
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
</sent>
<sent id="7182">
obratník	obratník	k1gInSc1	obratník
</sent>
<sent id="7183">
ocelotisk	ocelotisk	k1gInSc1	ocelotisk
</sent>
<sent id="7184">
pól	pól	k1gInSc1	pól
</sent>
<sent id="7186">
polygrafická	polygrafický	k2eAgFnSc1d1	polygrafická
výroba	výroba	k1gFnSc1	výroba
</sent>
<sent id="7188">
sazba	sazba	k1gFnSc1	sazba
popisu	popis	k1gInSc2	popis
</sent>
<sent id="7189">
severní	severní	k2eAgFnSc1d1	severní
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
</sent>
<sent id="7190">
síť	síť	k1gFnSc1	síť
(	(	kIx(	
<g/>
nesprávně	správně	k6eNd1	
rastr	rastr	k1gInSc1	rastr
<g/>
)	)	kIx)	
</sent>
<sent id="7191">
technologie	technologie	k1gFnSc1	technologie
polygrafické	polygrafický	k2eAgFnSc2d1	polygrafická
výroby	výroba	k1gFnSc2	výroba
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="7192">
technologie	technologie	k1gFnSc1	technologie
výroby	výroba	k1gFnSc2	výroba
glóbů	glóbus	k1gInPc2	glóbus
</sent>
<sent id="7193">
technologie	technologie	k1gFnSc2	technologie
výroby	výroba	k1gFnSc2	výroba
reliéfních	reliéfní	k2eAgFnPc2d1	reliéfní
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="7194">
tisková	tiskový	k2eAgFnSc1d1	tisková
technika	technika	k1gFnSc1	technika
</sent>
<sent id="7195">
tónování	tónování	k1gNnSc2	tónování
(	(	kIx(	
<g/>
terénního	terénní	k2eAgInSc2d1	terénní
reliéfu	reliéf	k1gInSc2	reliéf
<g/>
)	)	kIx)	
</sent>
<sent id="7196">
východní	východní	k2eAgFnSc1d1	východní
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
</sent>
<sent id="7197">
vzorník	vzorník	k1gInSc1	vzorník
písma	písmo	k1gNnSc2	písmo
</sent>
<sent id="7199">
západní	západní	k2eAgFnSc1d1	západní
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
</sent>
<sent id="7200">
zobrazovací	zobrazovací	k2eAgFnSc1d1	zobrazovací
rovnice	rovnice	k1gFnSc1	rovnice
</sent>
<sent id="7203">
OPO	OPO	kA	
</sent>
<sent id="7204">
ORP	ORP	kA	
</sent>
<sent id="7205">
VÚSC	VÚSC	kA	
</sent>
<sent id="7211">
Zeměměřický	zeměměřický	k2eAgInSc1d1	zeměměřický
úřad	úřad	k1gInSc1	úřad
(	(	kIx(	
<g/>
ZÚ	zú	k0	
<g/>
)	)	kIx)	
</sent>
<sent id="7213">
kulové	kulový	k2eAgNnSc4d1	kulové
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="7214">
nárys	nárys	k1gInSc1	nárys
</sent>
<sent id="7215">
nárysový	nárysový	k2eAgInSc1d1	nárysový
znak	znak	k1gInSc1	znak
</sent>
<sent id="7216">
nátisková	nátiskový	k2eAgFnSc1d1	nátisková
deska	deska	k1gFnSc1	deska
</sent>
<sent id="7217">
němé	němý	k2eAgNnSc4d1	němé
vydání	vydání	k1gNnSc4	vydání
</sent>
<sent id="7218">
neplatné	platný	k2eNgNnSc4d1	neplatné
vydání	vydání	k1gNnSc4	vydání
</sent>
<sent id="7219">
Postelovo	Postelův	k2eAgNnSc4d1	Postelův
azimutální	azimutální	k2eAgNnSc4d1	azimutální
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="7220">
předběžné	předběžný	k2eAgNnSc4d1	předběžné
vydání	vydání	k1gNnSc4	vydání
</sent>
<sent id="7221">
vykrývání	vykrývání	k1gNnSc4	vykrývání
</sent>
<sent id="7222">
geometrický	geometrický	k2eAgInSc1d1	geometrický
prvek	prvek	k1gInSc1	prvek
</sent>
<sent id="7223">
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
geodézie	geodézie	k1gFnSc1	geodézie
</sent>
<sent id="7226">
automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
kartografická	kartografický	k2eAgFnSc1d1	kartografická
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="7276">
ortofoto	ortofota	k1gFnSc5	ortofota
</sent>
<sent id="7231">
KoPÚ	KoPÚ	k1gFnSc1	KoPÚ
</sent>
<sent id="7256">
datová	datový	k2eAgFnSc1d1	datová
věda	věda	k1gFnSc1	věda
</sent>
<sent id="7257">
chromebook	chromebook	k1gInSc1	chromebook
</sent>
<sent id="7258">
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
cloudu	cloud	k1gInSc2	cloud
</sent>
<sent id="7259">
velká	velká	k1gFnSc1	velká
data	datum	k1gNnSc2	datum
</sent>
<sent id="7260">
web	web	k1gInSc1	web
věcí	věc	k1gFnPc2	věc
</sent>
<sent id="7262">
citlivé	citlivý	k2eAgInPc1d1	citlivý
údaje	údaj	k1gInPc1	údaj
</sent>
<sent id="7263">
dálkový	dálkový	k2eAgInSc4d1	dálkový
přístup	přístup	k1gInSc4	přístup
</sent>
<sent id="7264">
informační	informační	k2eAgInSc4d1	informační
systém	systém	k1gInSc4	systém
základních	základní	k2eAgInPc2d1	základní
registrů	registr	k1gInPc2	registr
</sent>
<sent id="7265">
informační	informační	k2eAgInPc4d1	informační
systémy	systém	k1gInPc4	systém
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
(	(	kIx(	
<g/>
ISVS	ISVS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7267">
organizační	organizační	k2eAgFnPc4d1	organizační
složky	složka	k1gFnPc4	složka
státu	stát	k1gInSc2	stát
</sent>
<sent id="7268">
portál	portál	k1gInSc1	portál
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
</sent>
<sent id="7269">
Základní	základní	k2eAgInSc1d1	základní
registr	registr	k1gInSc1	registr
</sent>
<sent id="7270">
listina	listina	k1gFnSc1	listina
</sent>
<sent id="7277">
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	
fotogrammetrii	fotogrammetrie	k1gFnSc4	fotogrammetrie
a	a	k8xC	
dálkový	dálkový	k2eAgInSc4d1	dálkový
průzkum	průzkum	k1gInSc4	průzkum
(	(	kIx(	
<g/>
ISPRS	ISPRS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7278">
S	s	k7c7	
–	–	k?	
SK	Sk	kA	
GS	GS	kA	
</sent>
<sent id="7279">
S	s	k7c7	
–	–	k?	
SK	Sk	kA	
SŠ	SŠ	kA	
</sent>
<sent id="7280">
UAS	UAS	kA	
</sent>
<sent id="7281">
UAV	UAV	kA	
</sent>
<sent id="7282">
RPAS	RPAS	kA	
</sent>
<sent id="7287">
EGN	EGN	kA	
</sent>
<sent id="7288">
ELF	elf	k1gMnSc1	elf
</sent>
<sent id="7289">
ESDIN	ESDIN	kA	
</sent>
<sent id="7290">
GeoInfoStrategie	GeoInfoStrategie	k1gFnSc1	GeoInfoStrategie
</sent>
<sent id="7292">
NaSaPO	NaSaPO	k1gFnSc1	NaSaPO
</sent>
<sent id="7296">
Zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
</sent>
<sent id="7297">
MLS	mls	k1gInSc1	mls
</sent>
<sent id="7298">
MMS	MMS	kA	
</sent>
<sent id="7299">
TLS	TLS	kA	
</sent>
<sent id="7303">
fotogrammetr	fotogrammetr	k1gMnSc1	fotogrammetr
</sent>
<sent id="7304">
geodet	geodet	k1gMnSc1	geodet
</sent>
<sent id="7305">
UA	UA	kA	
</sent>
<sent id="7306">
MK	MK	kA	
200	#num#	k4	
</sent>
<sent id="7307">
MORP	MORP	kA	
50	#num#	k4	
</sent>
<sent id="7308">
MSR	MSR	kA	
</sent>
<sent id="7309">
RPA	RPA	kA	
</sent>
<sent id="7310">
AOPK	AOPK	kA	
</sent>
<sent id="7311">
BIM	bim	k0	
</sent>
<sent id="7313">
ČSTS	ČSTS	kA	
</sent>
<sent id="7314">
ObPÚ	ObPÚ	k1gFnSc1	ObPÚ
</sent>
<sent id="7317">
digitální	digitální	k2eAgFnSc1d1	digitální
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="7318">
digitální	digitální	k2eAgInSc4d1	digitální
obrazový	obrazový	k2eAgInSc4d1	obrazový
záznam	záznam	k1gInSc4	záznam
</sent>
<sent id="7437">
plošné	plošný	k2eAgNnSc1d1	plošné
měřítko	měřítko	k1gNnSc1	měřítko
</sent>
<sent id="7321">
radar	radar	k1gInSc1	radar
se	s	k7c7	
syntetickou	syntetický	k2eAgFnSc7d1	syntetická
aperturou	apertura	k1gFnSc7	apertura
(	(	kIx(	
<g/>
SAR	SAR	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7322">
senzor	senzor	k1gInSc1	senzor
</sent>
<sent id="7323">
sken	sken	k1gInSc1	sken
</sent>
<sent id="7324">
vícesnímková	vícesnímkový	k2eAgFnSc1d1	vícesnímkový
fotogrammetrie	fotogrammetrie	k1gFnSc1	fotogrammetrie
</sent>
<sent id="7325">
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
pro	pro	k7c4	
prostorové	prostorový	k2eAgFnPc4d1	prostorová
informace	informace	k1gFnPc4	informace
</sent>
<sent id="7327">
národní	národní	k2eAgFnSc1d1	národní
sada	sada	k1gFnSc1	sada
prostorových	prostorový	k2eAgInPc2d1	prostorový
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	
<g/>
NaSaPO	NaSaPO	k1gFnPc2	NaSaPO
<g/>
)	)	kIx)	
</sent>
<sent id="7328">
případ	případ	k1gInSc1	případ
použití	použití	k1gNnSc2	použití
</sent>
<sent id="7329">
EPSG	EPSG	kA	
</sent>
<sent id="7330">
ČGS	ČGS	kA	
</sent>
<sent id="7331">
ČPGS	ČPGS	kA	
</sent>
<sent id="7332">
ČPNS	ČPNS	kA	
</sent>
<sent id="7333">
ČPPS	ČPPS	kA	
</sent>
<sent id="7334">
ECGN	ECGN	kA	
</sent>
<sent id="7335">
EPN	EPN	kA	
</sent>
<sent id="7349">
Grid_ETRS	Grid_ETRS	k1gFnSc1	Grid_ETRS
<g/>
89	#num#	k4	
<g/>
-GRS	-GRS	k?	
<g/>
80	#num#	k4	
</sent>
<sent id="7337">
ETJTZÚ	ETJTZÚ	kA	
</sent>
<sent id="7338">
ETRF89	ETRF89	k1gFnSc1	ETRF89
</sent>
<sent id="7339">
ETRF2000	ETRF2000	k1gFnSc1	ETRF2000
</sent>
<sent id="7340">
ETRS89	ETRS89	k1gFnSc1	ETRS89
</sent>
<sent id="7341">
ETRS89-GRS80	ETRS89-GRS80	k1gFnSc1	ETRS89-GRS80
</sent>
<sent id="7342">
ETRS89-LAEA	ETRS89-LAEA	k1gFnSc1	ETRS89-LAEA
</sent>
<sent id="7343">
ETRS89-LCC	ETRS89-LCC	k1gFnSc1	ETRS89-LCC
</sent>
<sent id="7344">
ETRS89-TM33	ETRS89-TM33	k1gFnSc1	ETRS89-TM33
</sent>
<sent id="7345">
ETRS89-TM34	ETRS89-TM34	k1gFnSc1	ETRS89-TM34
</sent>
<sent id="7346">
ETRS	ETRS	kA	
<g/>
89	#num#	k4	
<g/>
-TMzn	-TMzn	k1gInSc4	-TMzn
</sent>
<sent id="7348">
EVRF07	EVRF07	k1gFnSc1	EVRF07
</sent>
<sent id="7350">
Grid_ETRS	Grid_ETRS	k1gFnSc1	Grid_ETRS
<g/>
89	#num#	k4	
<g/>
-LAEA	-LAEA	k?	
</sent>
<sent id="7351">
ISZ	ISZ	kA	
</sent>
<sent id="7352">
NTRIP	NTRIP	kA	
</sent>
<sent id="7353">
PTBP	PTBP	kA	
</sent>
<sent id="7354">
QGZÚ	QGZÚ	kA	
</sent>
<sent id="7355">
RTCM	RTCM	kA	
</sent>
<sent id="7356">
S-Gr	S-Gr	k1gInSc1	S-Gr
<g/>
10	#num#	k4	
</sent>
<sent id="7357">
VESOG	VESOG	kA	
</sent>
<sent id="7358">
WCTS	WCTS	kA	
</sent>
<sent id="7359">
VRS	VRS	kA	
</sent>
<sent id="7361">
ZTL	ZTL	kA	
</sent>
<sent id="7362">
3D	3D	k4	
tisk	tisk	k1gInSc1	tisk
</sent>
<sent id="7363">
automapa	automapa	k1gFnSc1	automapa
</sent>
<sent id="7364">
batymetrická	batymetrický	k2eAgFnSc1d1	batymetrický
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="7365">
geometricky	geometricky	k6eAd1	
nejvhodnější	vhodný	k2eAgNnSc4d3	nejvhodnější
zobrazení	zobrazení	k1gNnSc4	zobrazení
</sent>
<sent id="7366">
glóbový	glóbový	k2eAgInSc1d1	glóbový
segment	segment	k1gInSc1	segment
</sent>
<sent id="7367">
hypsometrická	hypsometrický	k2eAgFnSc1d1	hypsometrický
vrstva	vrstva	k1gFnSc1	vrstva
</sent>
<sent id="7368">
mapový	mapový	k2eAgInSc4d1	mapový
itinerář	itinerář	k1gInSc4	itinerář
</sent>
<sent id="7369">
měřítko	měřítko	k1gNnSc1	měřítko
konceptu	koncept	k1gInSc2	koncept
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="7370">
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
údržba	údržba	k1gFnSc1	údržba
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="7371">
morfografie	morfografie	k1gFnSc1	morfografie
</sent>
<sent id="7372">
rovník	rovník	k1gInSc1	rovník
</sent>
<sent id="7373">
smluvená	smluvený	k2eAgFnSc1d1	smluvená
značka	značka	k1gFnSc1	značka
</sent>
<sent id="7374">
souřadnice	souřadnice	k1gFnSc1	souřadnice
rohů	roh	k1gInPc2	roh
mapového	mapový	k2eAgInSc2d1	mapový
listu	list	k1gInSc2	list
</sent>
<sent id="7375">
systematické	systematický	k2eAgNnSc1d1	systematické
třídění	třídění	k1gNnSc1	třídění
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="7376">
trimetalická	trimetalický	k2eAgFnSc1d1	trimetalický
deska	deska	k1gFnSc1	deska
</sent>
<sent id="7377">
typografický	typografický	k2eAgInSc4d1	typografický
systém	systém	k1gInSc4	systém
</sent>
<sent id="7378">
údaje	údaj	k1gInSc2	údaj
o	o	k7c6	
kartografickém	kartografický	k2eAgNnSc6d1	kartografické
díle	dílo	k1gNnSc6	dílo
(	(	kIx(	
<g/>
o	o	k7c6	
mapě	mapa	k1gFnSc6	mapa
<g/>
,	,	kIx,	
atlasu	atlas	k1gInSc2	atlas
<g/>
)	)	kIx)	
</sent>
<sent id="7379">
umístění	umístění	k1gNnSc6	umístění
popisu	popis	k1gInSc2	popis
</sent>
<sent id="7380">
vakuové	vakuový	k2eAgNnSc1d1	vakuové
tvarování	tvarování	k1gNnSc1	tvarování
</sent>
<sent id="7381">
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
revize	revize	k1gFnSc1	revize
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="7382">
CCD	CCD	kA	
</sent>
<sent id="7385">
INS	INS	kA	
</sent>
<sent id="7386">
NMCA	NMCA	kA	
</sent>
<sent id="7387">
NSDI	NSDI	kA	
</sent>
<sent id="7388">
RMSE	RMSE	kA	
</sent>
<sent id="7389">
WMTS	WMTS	kA	
</sent>
<sent id="7390">
Astronomicko-geodetická	astronomicko-geodetický	k2eAgFnSc1d1	astronomicko-geodetická
síť	síť	k1gFnSc1	síť
ČR	ČR	kA	
(	(	kIx(	
<g/>
AGS	AGS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7391">
Česká	český	k2eAgFnSc1d1	Česká
gravimetrická	gravimetrický	k2eAgFnSc1d1	gravimetrická
síť	síť	k1gFnSc1	síť
(	(	kIx(	
<g/>
ČGS	ČGS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7392">
Česká	český	k2eAgFnSc1d1	Česká
státní	státní	k2eAgFnSc1d1	státní
trigonometrická	trigonometrický	k2eAgFnSc1d1	trigonometrická
síť	síť	k1gFnSc1	síť
(	(	kIx(	
<g/>
ČSTS	ČSTS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7393">
Databáze	databáze	k1gFnSc2	databáze
bodových	bodový	k2eAgFnPc2d1	bodová
polí	pole	k1gFnPc2	pole
(	(	kIx(	
<g/>
DBP	DBP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7394">
hlavní	hlavní	k2eAgFnSc1d1	hlavní
gravimetrická	gravimetrický	k2eAgFnSc1d1	gravimetrická
základna	základna	k1gFnSc1	základna
(	(	kIx(	
<g/>
HGZ	HGZ	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7395">
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
geodetického	geodetický	k2eAgInSc2d1	geodetický
bodu	bod	k1gInSc2	bod
</sent>
<sent id="7396">
česká	český	k2eAgFnSc1d1	Česká
technická	technický	k2eAgFnSc1d1	technická
norma	norma	k1gFnSc1	norma
(	(	kIx(	
<g/>
CSN	CSN	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7397">
evropská	evropský	k2eAgFnSc1d1	Evropská
norma	norma	k1gFnSc1	norma
(	(	kIx(	
<g/>
CEN	cena	k1gFnPc2	cena
<g/>
)	)	kIx)	
</sent>
<sent id="7398">
harmonizace	harmonizace	k1gFnSc2	harmonizace
prostorových	prostorový	k2eAgNnPc2d1	prostorové
dat	datum	k1gNnPc2	datum
</sent>
<sent id="7399">
metainformační	metainformační	k2eAgInSc4d1	metainformační
systém	systém	k1gInSc4	systém
</sent>
<sent id="7400">
na	na	k7c6	
místě	místo	k1gNnSc6	místo
založená	založený	k2eAgFnSc1d1	založená
služba	služba	k1gFnSc1	služba
</sent>
<sent id="7401">
na	na	k7c6	
místě	místo	k1gNnSc6	místo
závislá	závislý	k2eAgFnSc1d1	závislá
služba	služba	k1gFnSc1	služba
</sent>
<sent id="7402">
odvozený	odvozený	k2eAgInSc1d1	odvozený
objekt	objekt	k1gInSc1	objekt
Národní	národní	k2eAgFnSc2d1	národní
sady	sada	k1gFnSc2	sada
prostorových	prostorový	k2eAgInPc2d1	prostorový
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	
<g/>
NaSaPO	NaSaPO	k1gFnPc2	NaSaPO
<g/>
)	)	kIx)	
</sent>
<sent id="7403">
ortogonalizace	ortogonalizace	k1gFnSc1	ortogonalizace
</sent>
<sent id="7404">
otevřené	otevřený	k2eAgFnPc4d1	otevřená
služby	služba	k1gFnPc4	služba
nad	nad	k7c7	
prostorovými	prostorový	k2eAgNnPc7d1	prostorové
daty	datum	k1gNnPc7	datum
</sent>
<sent id="7405">
potvrzení	potvrzení	k1gNnSc4	potvrzení
platnosti	platnost	k1gFnSc2	platnost
dat	datum	k1gNnPc2	datum
</sent>
<sent id="7406">
půdní	půdní	k2eAgInSc1d1	půdní
kryt	kryt	k1gInSc1	kryt
</sent>
<sent id="7407">
prvek	prvek	k1gInSc1	prvek
</sent>
<sent id="7408">
referenční	referenční	k2eAgMnSc1d1	referenční
data	datum	k1gNnSc2	datum
</sent>
<sent id="7409">
technicko-normalizační	technicko-normalizační	k2eAgFnPc4d1	technicko-normalizační
informace	informace	k1gFnPc4	informace
(	(	kIx(	
<g/>
TNI	tnout	k5eAaBmRp2nS,k5eAaPmRp2nS	
<g/>
)	)	kIx)	
</sent>
<sent id="7410">
tematická	tematický	k2eAgFnSc1d1	tematická
prostorová	prostorový	k2eAgFnSc1d1	prostorová
data	datum	k1gNnSc2	datum
</sent>
<sent id="7411">
technická	technický	k2eAgFnSc1d1	technická
norma	norma	k1gFnSc1	norma
</sent>
<sent id="7412">
referenční	referenční	k2eAgInSc4d1	referenční
rozhraní	rozhraní	k1gNnSc1	rozhraní
ISVS	ISVS	kA	
</sent>
<sent id="7413">
standard	standard	k1gInSc1	standard
</sent>
<sent id="7414">
územně	územně	k6eAd1	
plánovací	plánovací	k2eAgFnSc2d1	plánovací
dokumentace	dokumentace	k1gFnSc2	dokumentace
(	(	kIx(	
<g/>
ÚPD	ÚPD	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7416">
syntaktická	syntaktický	k2eAgFnSc1d1	syntaktická
interoperabilita	interoperabilita	k1gFnSc1	interoperabilita
</sent>
<sent id="7417">
Účelová	účelový	k2eAgFnSc1d1	účelová
katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
(	(	kIx(	
<g/>
ÚKM	ÚKM	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7419">
sémantická	sémantický	k2eAgFnSc1d1	sémantická
interoperabilita	interoperabilita	k1gFnSc1	interoperabilita
</sent>
<sent id="7420">
standardizace	standardizace	k1gFnSc2	standardizace
</sent>
<sent id="7421">
základní	základní	k2eAgInSc4d1	základní
objekt	objekt	k1gInSc4	objekt
Národní	národní	k2eAgFnSc2d1	národní
sady	sada	k1gFnSc2	sada
prostorových	prostorový	k2eAgInPc2d1	prostorový
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	
<g/>
NaSaPO	NaSaPO	k1gFnPc2	NaSaPO
<g/>
)	)	kIx)	
</sent>
<sent id="7422">
životní	životní	k2eAgInSc4d1	životní
cyklus	cyklus	k1gInSc4	cyklus
informace	informace	k1gFnSc1	informace
</sent>
<sent id="7423">
využití	využití	k1gNnSc6	využití
půdy	půda	k1gFnSc2	půda
</sent>
<sent id="7424">
volná	volný	k2eAgFnSc1d1	volná
</sent>
<sent id="7426">
bezpilotní	bezpilotní	k2eAgInSc4d1	bezpilotní
letadlo	letadlo	k1gNnSc1	letadlo
</sent>
<sent id="7427">
bezpilotní	bezpilotní	k2eAgInSc4d1	bezpilotní
systém	systém	k1gInSc4	systém
</sent>
<sent id="7428">
dálkově	dálkově	k6eAd1	
řídící	řídící	k2eAgInSc4d1	řídící
pilot	pilot	k1gInSc4	pilot
</sent>
<sent id="7429">
dálkově	dálkově	k6eAd1	
řídicí	řídicí	k2eAgFnPc4d1	řídicí
stanice	stanice	k1gFnPc4	stanice
</sent>
<sent id="7430">
dálkově	dálkově	k6eAd1	
řízené	řízený	k2eAgNnSc1d1	řízené
letadlo	letadlo	k1gNnSc1	letadlo
</sent>
<sent id="7432">
letadlo	letadlo	k1gNnSc4	letadlo
</sent>
<sent id="7433">
letoun	letoun	k1gInSc1	letoun
</sent>
<sent id="7434">
multikoptéra	multikoptér	k1gMnSc4	multikoptér
</sent>
<sent id="7435">
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
dálkově	dálkově	k6eAd1	
řízeného	řízený	k2eAgNnSc2d1	řízené
letadla	letadlo	k1gNnSc2	letadlo
</sent>
<sent id="7438">
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
čára	čára	k1gFnSc1	čára
</sent>
<sent id="7439">
pólová	pólový	k2eAgFnSc1d1	pólová
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
</sent>
<sent id="7440">
pokrytí	pokrytí	k1gNnSc2	pokrytí
území	území	k1gNnSc2	území
mapami	mapa	k1gFnPc7	mapa
</sent>
<sent id="7441">
postupy	postup	k1gInPc4	postup
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="7442">
rok	rok	k1gInSc1	rok
vydání	vydání	k1gNnSc2	vydání
(	(	kIx(	
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	
</sent>
<sent id="7443">
rysky	ryska	k1gFnSc2	ryska
pravoúhlé	pravoúhlý	k2eAgFnSc2d1	pravoúhlá
rovinné	rovinný	k2eAgFnSc2d1	rovinná
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="7444">
rysky	ryska	k1gFnSc2	ryska
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="7445">
schéma	schéma	k1gNnSc1	schéma
hranic	hranice	k1gFnPc2	hranice
</sent>
<sent id="7447">
strojový	strojový	k2eAgInSc4d1	strojový
formát	formát	k1gInSc4	formát
</sent>
<sent id="7448">
šedotisk	šedotisk	k1gInSc1	šedotisk
</sent>
<sent id="7449">
teorie	teorie	k1gFnSc1	teorie
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="7450">
tiskový	tiskový	k2eAgInSc4d1	tiskový
obraz	obraz	k1gInSc4	obraz
</sent>
<sent id="7451">
úhloměr	úhloměr	k1gInSc1	úhloměr
</sent>
<sent id="7452">
utvrzování	utvrzování	k1gNnSc2	utvrzování
osvitem	osvit	k1gInSc7	osvit
</sent>
<sent id="7453">
vyhlazování	vyhlazování	k1gNnSc4	vyhlazování
</sent>
<sent id="7454">
vyjádření	vyjádření	k1gNnSc2	vyjádření
značkou	značka	k1gFnSc7	značka
</sent>
<sent id="7455">
zásady	zásada	k1gFnSc2	zásada
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="7457">
autorizovaná	autorizovaný	k2eAgFnSc1d1	autorizovaná
služba	služba	k1gFnSc1	služba
</sent>
<sent id="7458">
Compass	Compass	k1gInSc1	Compass
</sent>
<sent id="7459">
čas	čas	k1gInSc1	čas
GLONASS	GLONASS	kA	
</sent>
<sent id="7460">
Dopplerův	Dopplerův	k2eAgInSc1d1	Dopplerův
posun	posun	k1gInSc1	posun
</sent>
<sent id="7461">
dostupnost	dostupnost	k1gFnSc1	dostupnost
</sent>
<sent id="7462">
dynamické	dynamický	k2eAgNnSc4d1	dynamické
měření	měření	k1gNnSc4	měření
(	(	kIx(	
<g/>
GNSS	GNSS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7463">
efemeridy	efemerida	k1gFnSc2	efemerida
</sent>
<sent id="7464">
geosynchronní	geosynchronní	k2eAgFnSc1d1	geosynchronní
dráha	dráha	k1gFnSc1	dráha
</sent>
<sent id="7465">
chyba	chyba	k1gFnSc1	chyba
efemerid	efemerida	k1gFnPc2	efemerida
</sent>
<sent id="7466">
interval	interval	k1gInSc1	interval
zhoršené	zhoršený	k2eAgFnSc2d1	zhoršená
přesnosti	přesnost	k1gFnSc2	přesnost
</sent>
<sent id="7468">
kód	kód	k1gInSc1	kód
</sent>
<sent id="7470">
kompatibilita	kompatibilita	k1gFnSc1	kompatibilita
</sent>
<sent id="7471">
konstelace	konstelace	k1gFnSc2	konstelace
družic	družice	k1gFnPc2	družice
</sent>
<sent id="7472">
kontinuita	kontinuita	k1gFnSc1	kontinuita
</sent>
<sent id="7475">
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	
globální	globální	k2eAgInPc4d1	globální
navigační	navigační	k2eAgInPc4d1	navigační
družicové	družicový	k2eAgInPc4d1	družicový
systémy	systém	k1gInPc4	systém
</sent>
<sent id="7476">
odchylka	odchylka	k1gFnSc1	odchylka
hodin	hodina	k1gFnPc2	hodina
</sent>
<sent id="7478">
pokrytí	pokrytí	k1gNnSc2	pokrytí
signálem	signál	k1gInSc7	signál
</sent>
<sent id="7480">
pozemní	pozemní	k2eAgInSc4d1	pozemní
rozšiřující	rozšiřující	k2eAgInSc4d1	rozšiřující
systém	systém	k1gInSc4	systém
</sent>
<sent id="7482">
sklon	sklon	k1gInSc1	sklon
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
</sent>
<sent id="7483">
služba	služba	k1gFnSc1	služba
pátrání	pátrání	k1gNnSc2	pátrání
a	a	k8xC	
záchrana	záchrana	k1gFnSc1	záchrana
</sent>
<sent id="7484">
služba	služba	k1gFnSc1	služba
se	s	k7c7	
zárukou	záruka	k1gFnSc7	záruka
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
</sent>
<sent id="7588">
absolutní	absolutní	k2eAgInSc1d1	absolutní
gravimetr	gravimetr	k1gInSc1	gravimetr
</sent>
<sent id="7486">
veřejná	veřejný	k2eAgFnSc1d1	veřejná
regulovaná	regulovaný	k2eAgFnSc1d1	regulovaná
služba	služba	k1gFnSc1	služba
</sent>
<sent id="7487">
celková	celkový	k2eAgFnSc1d1	celková
chyba	chyba	k1gFnSc1	chyba
měření	měření	k1gNnSc1	měření
</sent>
<sent id="7488">
čas	čas	k1gInSc1	čas
GNSS	GNSS	kA	
</sent>
<sent id="7489">
chyba	chyba	k1gFnSc1	chyba
kosmického	kosmický	k2eAgInSc2d1	kosmický
a	a	k8xC	
řídicího	řídicí	k2eAgInSc2d1	řídicí
segmentu	segment	k1gInSc2	segment
</sent>
<sent id="7490">
chyba	chyba	k1gFnSc1	chyba
přijímače	přijímač	k1gInSc2	přijímač
</sent>
<sent id="7491">
nosnou	nosný	k2eAgFnSc7d1	nosná
vlnou	vlna	k1gFnSc7	vlna
zpřesněné	zpřesněný	k2eAgNnSc4d1	zpřesněné
měření	měření	k1gNnSc4	měření
přijímačem	přijímač	k1gInSc7	přijímač
GNSS	GNSS	kA	
</sent>
<sent id="7497">
atituda	atituda	k1gFnSc1	atituda
</sent>
<sent id="7498">
blokové	bloková	k1gFnSc2	bloková
vyrovnání	vyrovnání	k1gNnSc2	vyrovnání
paprskových	paprskový	k2eAgInPc2d1	paprskový
svazků	svazek	k1gInPc2	svazek
</sent>
<sent id="7499">
distorze	distorze	k1gFnSc1	distorze
obrazu	obraz	k1gInSc2	obraz
</sent>
<sent id="7500">
gimbal	gimbal	k1gInSc1	gimbal
</sent>
<sent id="7501">
hlavní	hlavní	k2eAgInSc1d1	hlavní
bod	bod	k1gInSc1	bod
autokolimace	autokolimace	k1gFnSc2	autokolimace
</sent>
<sent id="7502">
kamera	kamera	k1gFnSc1	kamera
s	s	k7c7	
plošným	plošný	k2eAgInSc7d1	plošný
snímačem	snímač	k1gInSc7	snímač
</sent>
<sent id="7503">
multispektrální	multispektrální	k2eAgInSc4d1	multispektrální
skener	skener	k1gInSc4	skener
</sent>
<sent id="7504">
obraz	obraz	k1gInSc1	obraz
</sent>
<sent id="7505">
obrazový	obrazový	k2eAgInSc1d1	obrazový
bod	bod	k1gInSc1	bod
</sent>
<sent id="7506">
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
</sent>
<sent id="7507">
pansharpening	pansharpening	k1gInSc1	pansharpening
</sent>
<sent id="7508">
pasivní	pasivní	k2eAgInSc1d1	pasivní
senzor	senzor	k1gInSc1	senzor
</sent>
<sent id="7509">
pushbroom	pushbroom	k1gInSc1	pushbroom
senzor	senzor	k1gInSc4	senzor
</sent>
<sent id="7510">
referenční	referenční	k2eAgInSc1d1	referenční
souřadnicový	souřadnicový	k2eAgInSc1d1	souřadnicový
systém	systém	k1gInSc1	systém
nosiče	nosič	k1gInSc2	nosič
(	(	kIx(	
<g/>
platformy	platforma	k1gFnSc2	platforma
<g/>
)	)	kIx)	
</sent>
<sent id="7511">
smaz	smaz	k1gInSc1	smaz
obrazu	obraz	k1gInSc2	obraz
</sent>
<sent id="7512">
střed	střed	k1gInSc1	střed
promítání	promítání	k1gNnSc2	promítání
</sent>
<sent id="7513">
šířka	šířka	k1gFnSc1	šířka
paprsku	paprsek	k1gInSc2	paprsek
</sent>
<sent id="7514">
třířádková	třířádkový	k2eAgFnSc1d1	třířádková
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="7515">
úhel	úhel	k1gInSc1	úhel
dopadu	dopad	k1gInSc2	dopad
</sent>
<sent id="7516">
věrné	věrný	k2eAgInPc4d1	věrný
ortofoto	ortofota	k1gFnSc5	ortofota
</sent>
<sent id="7517">
whiskbroom	whiskbroom	k1gInSc1	whiskbroom
senzor	senzor	k1gInSc4	senzor
</sent>
<sent id="7519">
3D	3D	k4	
katastr	katastr	k1gInSc1	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="7520">
3D	3D	k4	
mapování	mapování	k1gNnSc4	mapování
</sent>
<sent id="7521">
3D	3D	k4	
model	model	k1gInSc1	model
</sent>
<sent id="7522">
3D	3D	k4	
modelování	modelování	k1gNnSc4	modelování
</sent>
<sent id="7524">
informační	informační	k2eAgInSc4d1	informační
modelování	modelování	k1gNnSc1	modelování
staveb	stavba	k1gFnPc2	stavba
(	(	kIx(	
<g/>
BIM	bim	k0	
<g/>
)	)	kIx)	
</sent>
<sent id="7526">
mobilní	mobilní	k2eAgInSc4d1	mobilní
mapování	mapování	k1gNnSc1	mapování
</sent>
<sent id="7527">
průmysl	průmysl	k1gInSc1	průmysl
4.0	4.0	k4	
</sent>
<sent id="7528">
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
realita	realita	k1gFnSc1	realita
</sent>
<sent id="7529">
adresní	adresní	k2eAgInSc4d1	adresní
místo	místo	k1gNnSc1	místo
</sent>
<sent id="7530">
číslo	číslo	k1gNnSc4	číslo
domovní	domovní	k2eAgFnSc2d1	domovní
</sent>
<sent id="7532">
definiční	definiční	k2eAgFnSc1d1	definiční
čára	čára	k1gFnSc1	čára
ulice	ulice	k1gFnSc2	ulice
</sent>
<sent id="7533">
kraj	kraj	k1gInSc1	kraj
</sent>
<sent id="7535">
městský	městský	k2eAgInSc1d1	městský
obvod	obvod	k1gInSc1	obvod
Praha	Praha	k1gFnSc1	Praha
</sent>
<sent id="7536">
místní	místní	k2eAgFnSc2d1	místní
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	
<g/>
LAU	LAU	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7537">
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
územních	územní	k2eAgFnPc2d1	územní
statistických	statistický	k2eAgFnPc2d1	statistická
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	
<g/>
NUTS	NUTS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7538">
obec	obec	k1gFnSc1	obec
s	s	k7c7	
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
</sent>
<sent id="7539">
okres	okres	k1gInSc1	okres
</sent>
<sent id="7540">
poštovní	poštovní	k2eAgFnSc1d1	poštovní
směrovací	směrovací	k2eAgNnSc4d1	směrovací
číslo	číslo	k1gNnSc4	číslo
</sent>
<sent id="7541">
prohlížecí	prohlížecí	k2eAgFnSc1d1	prohlížecí
služba	služba	k1gFnSc1	služba
</sent>
<sent id="7542">
region	region	k1gInSc1	region
NUTS	NUTS	kA	
</sent>
<sent id="7543">
region	region	k1gInSc1	region
soudržnosti	soudržnost	k1gFnSc2	soudržnost
</sent>
<sent id="7544">
správní	správní	k2eAgInSc1d1	správní
obvod	obvod	k1gInSc1	obvod
v	v	k7c6	
Praze	Praha	k1gFnSc6	Praha
</sent>
<sent id="7545">
stahovací	stahovací	k2eAgFnSc1d1	stahovací
služba	služba	k1gFnSc1	služba
</sent>
<sent id="7546">
stát	stát	k1gInSc1	stát
</sent>
<sent id="7547">
stavební	stavební	k2eAgInSc4d1	stavební
objekt	objekt	k1gInSc4	objekt
</sent>
<sent id="7548">
technicko-ekonomické	technicko-ekonomický	k2eAgInPc1d1	technicko-ekonomický
atributy	atribut	k1gInPc1	atribut
</sent>
<sent id="7549">
ulice	ulice	k1gFnSc2	ulice
nebo	nebo	k8xC	
jiné	jiný	k2eAgNnSc4d1	jiné
veřejné	veřejný	k2eAgNnSc4d1	veřejné
prostranství	prostranství	k1gNnSc4	prostranství
</sent>
<sent id="7550">
územní	územní	k2eAgFnSc1d1	územní
identifikace	identifikace	k1gFnSc1	identifikace
</sent>
<sent id="7551">
územní	územní	k2eAgFnSc1d1	územní
správní	správní	k2eAgFnSc1d1	správní
hranice	hranice	k1gFnSc1	hranice
</sent>
<sent id="7552">
územní	územní	k2eAgFnSc1d1	územní
správní	správní	k2eAgFnSc1d1	správní
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="7553">
veřejný	veřejný	k2eAgInSc4d1	veřejný
dálkový	dálkový	k2eAgInSc4d1	dálkový
přístup	přístup	k1gInSc4	přístup
</sent>
<sent id="7554">
volební	volební	k2eAgInSc1d1	volební
okrsek	okrsek	k1gInSc1	okrsek
</sent>
<sent id="7555">
znak	znak	k1gInSc1	znak
orientačního	orientační	k2eAgNnSc2d1	orientační
čísla	číslo	k1gNnSc2	číslo
</sent>
<sent id="7556">
Registr	registr	k1gInSc1	registr
územní	územní	k2eAgFnSc2d1	územní
identifikace	identifikace	k1gFnSc2	identifikace
<g/>
,	,	kIx,	
adres	adresa	k1gFnPc2	adresa
a	a	k8xC	
nemovitostí	nemovitost	k1gFnPc2	nemovitost
(	(	kIx(	
<g/>
RÚIAN	RÚIAN	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7557">
vyšší	vysoký	k2eAgInSc4d2	vyšší
územní	územní	k2eAgInSc4d1	územní
samosprávný	samosprávný	k2eAgInSc4d1	samosprávný
celek	celek	k1gInSc4	celek
</sent>
<sent id="7558">
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
územní	územní	k2eAgFnSc2d1	územní
identifikace	identifikace	k1gFnSc2	identifikace
</sent>
<sent id="7560">
správce	správce	k1gMnSc1	správce
inženýrské	inženýrský	k2eAgFnSc2d1	inženýrská
sítě	síť	k1gFnSc2	síť
</sent>
<sent id="7561">
Národní	národní	k2eAgFnSc1d1	národní
integrační	integrační	k2eAgFnSc1d1	integrační
platforma	platforma	k1gFnSc1	platforma
pro	pro	k7c4	
prostorové	prostorový	k2eAgFnPc4d1	prostorová
informace	informace	k1gFnPc4	informace
(	(	kIx(	
<g/>
NIPPI	NIPPI	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7562">
digitální	digitální	k2eAgFnSc1d1	digitální
mapa	mapa	k1gFnSc1	mapa
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
(	(	kIx(	
<g/>
DMVS	DMVS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7563">
Česká	český	k2eAgFnSc1d1	Česká
podrobná	podrobný	k2eAgFnSc1d1	podrobná
nivelační	nivelační	k2eAgFnSc1d1	nivelační
síť	síť	k1gFnSc1	síť
(	(	kIx(	
<g/>
ČPNS	ČPNS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7564">
Česká	český	k2eAgFnSc1d1	Česká
státní	státní	k2eAgFnSc1d1	státní
nivelační	nivelační	k2eAgFnSc1d1	nivelační
síť	síť	k1gFnSc1	síť
(	(	kIx(	
<g/>
ČSNS	ČSNS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7565">
hraniční	hraniční	k2eAgFnSc1d1	hraniční
dokumentární	dokumentární	k2eAgNnSc4d1	dokumentární
dílo	dílo	k1gNnSc4	dílo
</sent>
<sent id="7566">
GSA	GSA	kA	
</sent>
<sent id="7568">
SBE	SBE	kA	
</sent>
<sent id="7569">
měřická	měřický	k2eAgFnSc1d1	měřická
kampaň	kampaň	k1gFnSc1	kampaň
</sent>
<sent id="7570">
plošná	plošný	k2eAgFnSc1d1	plošná
nivelační	nivelační	k2eAgFnSc1d1	nivelační
síť	síť	k1gFnSc1	síť
(	(	kIx(	
<g/>
PNS	PNS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7571">
podrobné	podrobný	k2eAgFnPc1d1	podrobná
polohové	polohový	k2eAgFnPc1d1	polohová
bodové	bodový	k2eAgFnPc1d1	bodová
pole	pole	k1gFnPc1	pole
(	(	kIx(	
<g/>
PPBP	PPBP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7572">
podrobné	podrobný	k2eAgFnPc1d1	podrobná
výškové	výškový	k2eAgFnPc1d1	výšková
bodové	bodový	k2eAgFnPc1d1	bodová
pole	pole	k1gFnPc1	pole
(	(	kIx(	
<g/>
PVBP	PVBP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7573">
permanentní	permanentní	k2eAgFnSc1d1	permanentní
stanice	stanice	k1gFnSc1	stanice
pro	pro	k7c4	
příjem	příjem	k1gInSc4	příjem
signálů	signál	k1gInPc2	signál
GNSS	GNSS	kA	
</sent>
<sent id="7574">
správce	správka	k1gFnSc6	správka
značky	značka	k1gFnSc2	značka
bodu	bod	k1gInSc2	bod
bodového	bodový	k2eAgNnSc2d1	bodové
pole	pole	k1gNnSc2	pole
</sent>
<sent id="7575">
podrobné	podrobný	k2eAgFnPc1d1	podrobná
tíhové	tíhový	k2eAgFnPc1d1	tíhová
bodové	bodový	k2eAgFnPc1d1	bodová
pole	pole	k1gFnPc1	pole
(	(	kIx(	
<g/>
PTBP	PTBP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7576">
vyhledávací	vyhledávací	k2eAgFnSc1d1	vyhledávací
míra	míra	k1gFnSc1	míra
</sent>
<sent id="7577">
hlavní	hlavní	k2eAgFnSc1d1	hlavní
vrstevnice	vrstevnice	k1gFnSc1	vrstevnice
</sent>
<sent id="7578">
konference	konference	k1gFnSc2	konference
o	o	k7c6	
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
mapě	mapa	k1gFnSc6	mapa
světa	svět	k1gInSc2	svět
</sent>
<sent id="7579">
hustota	hustota	k1gFnSc1	hustota
názvů	název	k1gInPc2	název
</sent>
<sent id="7580">
mez	mez	k1gFnSc1	mez
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="7581">
rok	rok	k1gInSc1	rok
vydání	vydání	k1gNnSc2	vydání
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="7583">
tvarové	tvarový	k2eAgFnSc2d1	tvarová
šrafy	šrafa	k1gFnSc2	šrafa
</sent>
<sent id="7584">
ruční	ruční	k2eAgInSc4d1	ruční
tónování	tónování	k1gNnSc1	tónování
</sent>
<sent id="7585">
stanovení	stanovení	k1gNnPc2	stanovení
závazných	závazný	k2eAgFnPc2d1	závazná
barev	barva	k1gFnPc2	barva
</sent>
<sent id="7586">
předloha	předloha	k1gFnSc1	předloha
změn	změna	k1gFnPc2	změna
</sent>
<sent id="7587">
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	
magnetické	magnetický	k2eAgFnSc6d1	magnetická
deklinaci	deklinace	k1gFnSc6	deklinace
</sent>
<sent id="7593">
nivelační	nivelační	k2eAgInSc1d1	nivelační
polygon	polygon	k1gInSc1	polygon
</sent>
<sent id="7594">
nivelační	nivelační	k2eAgInSc1d1	nivelační
úsek	úsek	k1gInSc1	úsek
</sent>
<sent id="7595">
relativní	relativní	k2eAgInSc4d1	relativní
gravimetr	gravimetr	k1gInSc4	gravimetr
</sent>
<sent id="7596">
Souřadnicový	souřadnicový	k2eAgInSc4d1	souřadnicový
systém	systém	k1gInSc4	systém
1942	#num#	k4	
(	(	kIx(	
<g/>
S-	S-	k1gFnSc1	S-
<g/>
42	#num#	k4	
<g/>
/	/	kIx~	
<g/>
83	#num#	k4	
<g/>
)	)	kIx)	
</sent>
<sent id="7597">
pseudovzdálenost	pseudovzdálenost	k1gFnSc1	pseudovzdálenost
</sent>
<sent id="7598">
průhyb	průhyb	k1gInSc1	průhyb
pásma	pásmo	k1gNnSc2	pásmo
</sent>
<sent id="7599">
tíhový	tíhový	k2eAgInSc4d1	tíhový
gradient	gradient	k1gInSc4	gradient
</sent>
<sent id="7600">
gravimetrický	gravimetrický	k2eAgInSc4d1	gravimetrický
geoid	geoid	k1gInSc4	geoid
</sent>
<sent id="7601">
Světový	světový	k2eAgInSc1d1	světový
geodetický	geodetický	k2eAgInSc1d1	geodetický
systém	systém	k1gInSc1	systém
1984	#num#	k4	
(	(	kIx(	
<g/>
WGS	WGS	kA	
<g/>
84	#num#	k4	
<g/>
)	)	kIx)	
</sent>
<sent id="7602">
trilaterace	trilaterace	k1gFnSc1	trilaterace
</sent>
<sent id="7603">
oscilace	oscilace	k1gFnSc1	oscilace
tížnice	tížnice	k1gFnSc1	tížnice
</sent>
<sent id="7604">
troposféra	troposféra	k1gFnSc1	troposféra
</sent>
<sent id="7605">
geomatik	geomatika	k1gFnPc2	geomatika
</sent>
<sent id="7606">
geoinformatik	geoinformatika	k1gFnPc2	geoinformatika
</sent>
<sent id="7607">
metrolog	metrolog	k1gMnSc1	metrolog
</sent>
<sent id="7608">
topograf	topograf	k1gMnSc1	topograf
</sent>
<sent id="7610">
elektronická	elektronický	k2eAgFnSc1d1	elektronická
uzávěrka	uzávěrka	k1gFnSc1	uzávěrka
</sent>
<sent id="7611">
formát	formát	k1gInSc1	formát
snímku	snímek	k1gInSc2	snímek
</sent>
<sent id="7612">
měřítko	měřítko	k1gNnSc1	měřítko
snímku	snímek	k1gInSc2	snímek
</sent>
<sent id="7613">
neměřická	měřický	k2eNgFnSc1d1	měřický
komora	komora	k1gFnSc1	komora
<g/>
/	/	kIx~	
<g/>
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="7614">
řadová	řadový	k2eAgFnSc1d1	řadová
měřická	měřický	k2eAgFnSc1d1	měřická
komora	komora	k1gFnSc1	komora
</sent>
<sent id="7615">
stereoskopický	stereoskopický	k2eAgInSc4d1	stereoskopický
překryt	překryt	k2eAgInSc4d1	překryt
</sent>
<sent id="7616">
štěrbinová	štěrbinový	k2eAgFnSc1d1	štěrbinová
závěrka	závěrka	k1gFnSc1	závěrka
</sent>
<sent id="7617">
závěs	závěs	k1gInSc1	závěs
měřické	měřický	k2eAgFnSc2d1	měřická
komory	komora	k1gFnSc2	komora
<g/>
/	/	kIx~	
<g/>
kamery	kamera	k1gFnSc2	kamera
</sent>
<sent id="7618">
žaluziová	žaluziový	k2eAgFnSc1d1	žaluziová
závěrka	závěrka	k1gFnSc1	závěrka
</sent>
<sent id="7619">
kartografické	kartografický	k2eAgFnSc2d1	kartografická
souřadnice	souřadnice	k1gFnSc2	souřadnice
</sent>
<sent id="7620">
metodický	metodický	k2eAgInSc4d1	metodický
návod	návod	k1gInSc4	návod
</sent>
<sent id="7621">
občanské	občanský	k2eAgNnSc4d1	občanské
právo	právo	k1gNnSc4	právo
hmotné	hmotný	k2eAgFnSc2d1	hmotná
</sent>
<sent id="7622">
obchodní	obchodní	k2eAgInSc4d1	obchodní
právo	právo	k1gNnSc1	právo
</sent>
<sent id="7623">
relativní	relativní	k2eAgFnSc1d1	relativní
kóta	kóta	k1gFnSc1	kóta
</sent>
<sent id="7624">
směrnice	směrnice	k1gFnSc2	směrnice
</sent>
<sent id="7625">
správní	správní	k2eAgInSc4d1	správní
právo	právo	k1gNnSc1	právo
</sent>
<sent id="7626">
ústavní	ústavní	k2eAgInSc4d1	ústavní
právo	právo	k1gNnSc1	právo
</sent>
<sent id="7628">
veřejné	veřejný	k2eAgNnSc4d1	veřejné
právo	právo	k1gNnSc4	právo
</sent>
<sent id="7629">
vrstevnicový	vrstevnicový	k2eAgInSc4d1	vrstevnicový
interval	interval	k1gInSc4	interval
</sent>
<sent id="7631">
harmonizovaná	harmonizovaný	k2eAgFnSc1d1	harmonizovaná
norma	norma	k1gFnSc1	norma
ČSN	ČSN	kA	
</sent>
<sent id="4000">
aberace	aberace	k1gFnSc1	aberace
</sent>
<sent id="1050">
digitální	digitální	k2eAgInSc4d1	digitální
model	model	k1gInSc4	model
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="1218">
prostorová	prostorový	k2eAgFnSc1d1	prostorová
reference	reference	k1gFnSc1	reference
</sent>
<sent id="1230">
rastr	rastr	k1gInSc1	rastr
</sent>
<sent id="1239">
rozměr	rozměr	k1gInSc1	rozměr
</sent>
<sent id="1240">
rozpoznávání	rozpoznávání	k1gNnPc2	rozpoznávání
obrazců	obrazec	k1gInPc2	obrazec
(	(	kIx(	
<g/>
vzorů	vzor	k1gInPc2	vzor
<g/>
)	)	kIx)	
</sent>
<sent id="1245">
sekundární	sekundární	k2eAgInSc4d1	sekundární
systém	systém	k1gInSc4	systém
</sent>
<sent id="1247">
server	server	k1gInSc1	server
</sent>
<sent id="1252">
služba	služba	k1gFnSc1	služba
</sent>
<sent id="1275">
tezaurus	tezaurus	k1gInSc1	tezaurus
</sent>
<sent id="1284">
údaj	údaj	k1gInSc1	údaj
<g/>
(	(	kIx(	
<g/>
e	e	k0	
<g/>
)	)	kIx)	
</sent>
<sent id="1286">
ukázka	ukázka	k1gFnSc1	ukázka
</sent>
<sent id="1288">
univerzum	univerzum	k1gNnSc1	univerzum
diskurzu	diskurz	k1gInSc2	diskurz
</sent>
<sent id="1297">
virtuální	virtuální	k2eAgFnSc1d1	virtuální
realita	realita	k1gFnSc1	realita
</sent>
<sent id="1314">
vytyčovací	vytyčovací	k2eAgFnSc1d1	vytyčovací
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="1322">
zobrazovač	zobrazovač	k1gInSc1	zobrazovač
</sent>
<sent id="1506">
management	management	k1gInSc1	management
</sent>
<sent id="1511">
odborná	odborný	k2eAgFnSc1d1	odborná
způsobilost	způsobilost	k1gFnSc1	způsobilost
</sent>
<sent id="1515">
požadavek	požadavek	k1gInSc1	požadavek
</sent>
<sent id="1516">
proces	proces	k1gInSc1	proces
</sent>
<sent id="1523">
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
</sent>
<sent id="1527">
technický	technický	k2eAgMnSc1d1	technický
expert	expert	k1gMnSc1	expert
</sent>
<sent id="1532">
zkouška	zkouška	k1gFnSc1	zkouška
</sent>
<sent id="1533">
znak	znak	k1gInSc1	znak
a	a	k8xC	
charakteristika	charakteristika	k1gFnSc1	charakteristika
</sent>
<sent id="2003">
ar	ar	k1gInSc1	ar
</sent>
<sent id="2008">
byt	byt	k1gInSc1	byt
</sent>
<sent id="2044">
nebytový	bytový	k2eNgInSc4d1	nebytový
prostor	prostor	k1gInSc4	prostor
</sent>
<sent id="3006">
AV	AV	kA	
ČR	ČR	kA	
</sent>
<sent id="3013">
BPEJ	BPEJ	kA	
</sent>
<sent id="3015">
CAD	CAD	kA	
</sent>
<sent id="3019">
CET	ceta	k1gFnPc2	ceta
</sent>
<sent id="3024">
ČKAIT	ČKAIT	kA	
</sent>
<sent id="3025">
ČMI	ČMI	kA	
</sent>
<sent id="3027">
ČR	ČR	kA	
</sent>
<sent id="3031">
ČSÚ	ČSÚ	kA	
</sent>
<sent id="3032">
ČÚGK	ČÚGK	kA	
</sent>
<sent id="3033">
ČÚZK	ČÚZK	kA	
</sent>
<sent id="3034">
ČVUT	ČVUT	kA	
</sent>
<sent id="3048">
DPZ	DPZ	kA	
</sent>
<sent id="3051">
EARSeL	EARSeL	k1gFnSc1	EARSeL
</sent>
<sent id="3064">
FÖMI	FÖMI	kA	
</sent>
<sent id="3066">
FÚ	fú	k0	
</sent>
<sent id="3067">
FÚO	FÚO	kA	
</sent>
<sent id="3068">
GaKO	GaKO	k1gFnSc1	GaKO
</sent>
<sent id="3074">
GKP	GKP	kA	
</sent>
<sent id="3075">
GKÚ	GKÚ	kA	
</sent>
<sent id="3083">
GTÚ	GTÚ	kA	
</sent>
<sent id="3084">
GÚ	GÚ	kA	
</sent>
<sent id="3086">
HW	HW	kA	
</sent>
<sent id="3087">
CHKO	CHKO	kA	
</sent>
<sent id="3097">
IGU	IGU	kA	
</sent>
<sent id="3110">
IUGG	IUGG	kA	
</sent>
<sent id="3113">
JEP	JEP	kA	
</sent>
<sent id="3125">
KP	KP	kA	
</sent>
<sent id="3127">
KÚ	KÚ	kA	
</sent>
<sent id="3141">
MJ	mj	kA	
</sent>
<sent id="3143">
MNČ	MNČ	kA	
</sent>
<sent id="3156">
OSŘ	OSŘ	kA	
</sent>
<sent id="3160">
PET	PET	kA	
</sent>
<sent id="3166">
ppm	ppm	k?	
</sent>
<sent id="3167">
PÚ	PÚ	kA	
</sent>
<sent id="3170">
RES	RES	kA	
</sent>
<sent id="3179">
SEČ	SEČ	kA	
</sent>
<sent id="3182">
SI	si	k1gNnSc4	si
</sent>
<sent id="3184">
SJM	SJM	kA	
</sent>
<sent id="3195">
SR	SR	kA	
</sent>
<sent id="6845">
SWOT	SWOT	kA	
</sent>
<sent id="3198">
STU	sto	k4xCgNnSc3	
</sent>
<sent id="3219">
ÚPP	ÚPP	kA	
</sent>
<sent id="3226">
VGHMÚř	VGHMÚř	k1gFnSc1	VGHMÚř
</sent>
<sent id="3230">
VKÚ	VKÚ	kA	
</sent>
<sent id="3231">
VLBI	VLBI	kA	
</sent>
<sent id="3234">
VŠB	VŠB	kA	
-	-	kIx~	
TU	tu	k6eAd1	
</sent>
<sent id="3235">
VTOPÚ	VTOPÚ	kA	
</sent>
<sent id="3238">
VUT	VUT	kA	
</sent>
<sent id="3239">
VÚV	VÚV	kA	
TGM	TGM	kA	
</sent>
<sent id="3240">
VZÚ	VZÚ	kA	
</sent>
<sent id="3242">
WG	WG	kA	
</sent>
<sent id="3244">
WPLA	WPLA	kA	
</sent>
<sent id="3251">
ZKI	ZKI	kA	
</sent>
<sent id="3744">
neoprávněná	oprávněný	k2eNgFnSc1d1	neoprávněná
držba	držba	k1gFnSc1	držba
</sent>
<sent id="3268">
histogram	histogram	k1gInSc1	histogram
</sent>
<sent id="3270">
jas	jas	k1gInSc1	jas
</sent>
<sent id="3274">
stopa	stopa	k1gFnSc1	stopa
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
</sent>
<sent id="3310">
mikrofilm	mikrofilm	k1gInSc1	mikrofilm
</sent>
<sent id="3311">
mikrofilmová	mikrofilmový	k2eAgFnSc1d1	mikrofilmový
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="3313">
mikrofiš	mikrofiš	k1gFnSc1	mikrofiš
</sent>
<sent id="3314">
mikrofišová	mikrofišový	k2eAgFnSc1d1	mikrofišový
kamera	kamera	k1gFnSc1	kamera
</sent>
<sent id="3316">
mikrokarta	mikrokarta	k1gFnSc1	mikrokarta
</sent>
<sent id="3317">
mikrosnímkování	mikrosnímkování	k1gNnSc4	mikrosnímkování
</sent>
<sent id="3362">
redukční	redukční	k2eAgFnSc1d1	redukční
lupa	lupa	k1gFnSc1	lupa
</sent>
<sent id="3378">
astázie	astázie	k1gFnSc1	astázie
</sent>
<sent id="3391">
avigace	avigace	k1gFnSc1	avigace
</sent>
<sent id="3412">
juliánský	juliánský	k2eAgInSc4d1	juliánský
rok	rok	k1gInSc4	rok
</sent>
<sent id="3445">
atomový	atomový	k2eAgInSc4d1	atomový
čas	čas	k1gInSc4	čas
</sent>
<sent id="3467">
efemeridový	efemeridový	k2eAgInSc4d1	efemeridový
čas	čas	k1gInSc4	čas
</sent>
<sent id="3500">
nepřístupná	přístupný	k2eNgFnSc1d1	nepřístupná
délka	délka	k1gFnSc1	délka
</sent>
<sent id="3537">
stupňové	stupňový	k2eAgNnSc1d1	stupňové
dělení	dělení	k1gNnSc1	dělení
</sent>
<sent id="3564">
list	list	k1gInSc1	list
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
</sent>
<sent id="3570">
Dopplerův	Dopplerův	k2eAgInSc1d1	Dopplerův
jev	jev	k1gInSc1	jev
</sent>
<sent id="3571">
doba	doba	k1gFnSc1	doba
šíření	šíření	k1gNnSc2	šíření
časového	časový	k2eAgInSc2d1	časový
signálu	signál	k1gInSc2	signál
</sent>
<sent id="3610">
umělá	umělý	k2eAgFnSc1d1	umělá
družice	družice	k1gFnSc1	družice
Měsíce	měsíc	k1gInSc2	měsíc
</sent>
<sent id="3611">
umělá	umělý	k2eAgFnSc1d1	umělá
družice	družice	k1gFnSc1	družice
Země	zem	k1gFnSc2	zem
(	(	kIx(	
<g/>
UDZ	UDZ	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="3660">
nezemědělská	zemědělský	k2eNgFnSc1d1	nezemědělská
půda	půda	k1gFnSc1	půda
</sent>
<sent id="3665">
obnova	obnova	k1gFnSc1	obnova
katastrálního	katastrální	k2eAgInSc2d1	katastrální
operátu	operát	k1gInSc2	operát
</sent>
<sent id="3672">
ochrana	ochrana	k1gFnSc1	ochrana
půdního	půdní	k2eAgInSc2d1	půdní
fondu	fond	k1gInSc2	fond
</sent>
<sent id="3703">
půda	půda	k1gFnSc1	půda
ležící	ležící	k2eAgFnSc6d1	ležící
ladem	lad	k1gInSc7	lad
</sent>
<sent id="3705">
půdní	půdní	k2eAgInSc1d1	půdní
druh	druh	k1gInSc1	druh
</sent>
<sent id="3707">
půdní	půdní	k2eAgInSc1d1	půdní
typ	typ	k1gInSc1	typ
</sent>
<sent id="3740">
kartografie	kartografie	k1gFnSc1	kartografie
</sent>
<sent id="3745">
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
držba	držba	k1gFnSc1	držba
</sent>
<sent id="3748">
právní	právní	k2eAgInSc4d1	právní
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	
nemovitosti	nemovitost	k1gFnSc3	nemovitost
</sent>
<sent id="3798">
vlastnictví	vlastnictví	k1gNnPc2	vlastnictví
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
</sent>
<sent id="3845">
selenografie	selenografie	k1gFnSc1	selenografie
</sent>
<sent id="3906">
doména	doména	k1gFnSc1	doména
</sent>
<sent id="3910">
informační	informační	k2eAgFnSc1d1	informační
a	a	k8xC	
komunikační	komunikační	k2eAgFnSc1d1	komunikační
technologie	technologie	k1gFnSc1	technologie
</sent>
<sent id="3921">
bezplatný	bezplatný	k2eAgInSc4d1	bezplatný
software	software	k1gInSc4	software
</sent>
<sent id="3922">
vyhledávací	vyhledávací	k2eAgInSc4d1	vyhledávací
dotaz	dotaz	k1gInSc4	dotaz
</sent>
<sent id="4016">
albedo	albedo	k1gNnSc4	albedo
</sent>
<sent id="4056">
konstrukční	konstrukční	k2eAgFnSc1d1	konstrukční
práce	práce	k1gFnSc1	práce
</sent>
<sent id="4153">
DOPNUL	dopnout	k5eAaPmAgInS	
</sent>
<sent id="4157">
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
veličiny	veličina	k1gFnSc2	veličina
</sent>
<sent id="4159">
hodnota	hodnota	k1gFnSc1	hodnota
veličiny	veličina	k1gFnSc2	veličina
</sent>
<sent id="4176">
násobek	násobek	k1gInSc1	násobek
jednotky	jednotka	k1gFnSc2	jednotka
</sent>
<sent id="4183">
referenční	referenční	k2eAgFnSc1d1	referenční
stupnice	stupnice	k1gFnSc1	stupnice
hodnot	hodnota	k1gFnPc2	hodnota
veličiny	veličina	k1gFnSc2	veličina
</sent>
<sent id="4236">
GIT	Gita	k1gFnPc2	Gita
</sent>
<sent id="4241">
ZČU	ZČU	kA	
</sent>
<sent id="4243">
detektor	detektor	k1gInSc1	detektor
</sent>
<sent id="4245">
doba	doba	k1gFnSc1	doba
odezvy	odezva	k1gFnSc2	odezva
</sent>
<sent id="4246">
drift	drift	k1gInSc1	drift
</sent>
<sent id="4250">
charakteristika	charakteristika	k1gFnSc1	charakteristika
přenosu	přenos	k1gInSc2	přenos
</sent>
<sent id="4307">
DG	dg	kA	
</sent>
<sent id="4308">
EK	EK	kA	
</sent>
<sent id="4309">
EUROSTAT	EUROSTAT	kA	
</sent>
<sent id="4313">
planimetrie	planimetrie	k1gFnSc1	planimetrie
</sent>
<sent id="4335">
záznam	záznam	k1gInSc1	záznam
podrobného	podrobný	k2eAgNnSc2d1	podrobné
měření	měření	k1gNnSc2	měření
změn	změna	k1gFnPc2	změna
</sent>
<sent id="4375">
chyba	chyba	k1gFnSc1	chyba
uzávěru	uzávěr	k1gInSc2	uzávěr
</sent>
<sent id="4396">
ČZU	ČZU	kA	
</sent>
<sent id="4472">
čepová	čepový	k2eAgFnSc1d1	čepová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="4542">
signalizace	signalizace	k1gFnSc1	signalizace
</sent>
<sent id="4582">
nivelační	nivelační	k2eAgFnSc4d1	nivelační
lať	lať	k1gFnSc4	lať
</sent>
<sent id="4598">
ESA	eso	k1gNnSc2	eso
</sent>
<sent id="4599">
HTML	HTML	kA	
</sent>
<sent id="4604">
XML	XML	kA	
</sent>
<sent id="4646">
mikrometr	mikrometr	k1gInSc1	mikrometr
</sent>
<sent id="4648">
mikroskopový	mikroskopový	k2eAgInSc4d1	mikroskopový
teodolit	teodolit	k1gInSc4	teodolit
</sent>
<sent id="4697">
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
pásmo	pásmo	k1gNnSc1	pásmo
</sent>
<sent id="4748">
černé	černá	k1gFnSc6	černá
těleso	těleso	k1gNnSc1	těleso
</sent>
<sent id="4754">
fázový	fázový	k2eAgInSc1d1	fázový
posun	posun	k1gInSc1	posun
</sent>
<sent id="4787">
radiometrické	radiometrický	k2eAgNnSc4d1	radiometrické
rozlišení	rozlišení	k1gNnSc4	rozlišení
</sent>
<sent id="4818">
zářivost	zářivost	k1gFnSc1	zářivost
</sent>
<sent id="4912">
model	model	k1gInSc1	model
reliéfu	reliéf	k1gInSc2	reliéf
</sent>
<sent id="4942">
absorpční	absorpční	k2eAgInSc4d1	absorpční
pás	pás	k1gInSc4	pás
</sent>
<sent id="5058">
napínací	napínací	k2eAgFnSc1d1	napínací
tyč	tyč	k1gFnSc1	tyč
</sent>
<sent id="5065">
orientovaný	orientovaný	k2eAgInSc4d1	orientovaný
směr	směr	k1gInSc4	směr
</sent>
<sent id="5082">
skalní	skalní	k2eAgFnSc1d1	skalní
stabilizace	stabilizace	k1gFnSc1	stabilizace
</sent>
<sent id="5088">
tyčový	tyčový	k2eAgInSc4d1	tyčový
signál	signál	k1gInSc4	signál
</sent>
<sent id="5108">
výběrový	výběrový	k2eAgInSc4d1	výběrový
rozptyl	rozptyl	k1gInSc4	rozptyl
</sent>
<sent id="5125">
duktus	duktus	k1gInSc1	duktus
písma	písmo	k1gNnSc2	písmo
</sent>
<sent id="5128">
ediční	ediční	k2eAgInSc4d1	ediční
plán	plán	k1gInSc4	plán
</sent>
<sent id="5137">
GML	GML	kA	
</sent>
<sent id="5138">
NUTS	NUTS	kA	
</sent>
<sent id="5152">
prvek	prvek	k1gInSc1	prvek
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="5175">
HZS	HZS	kA	
</sent>
<sent id="5189">
grotesk	grotesk	k1gInSc1	grotesk
</sent>
<sent id="5211">
gradace	gradace	k1gFnSc1	gradace
</sent>
<sent id="5242">
kartografická	kartografický	k2eAgFnSc1d1	kartografická
výroba	výroba	k1gFnSc1	výroba
</sent>
<sent id="5298">
bajt	bajt	k1gInSc1	bajt
</sent>
<sent id="5300">
certifikační	certifikační	k2eAgFnSc1d1	certifikační
autorita	autorita	k1gFnSc1	autorita
</sent>
<sent id="5303">
grabování	grabování	k1gNnSc4	grabování
</sent>
<sent id="5305">
kryptoanalýza	kryptoanalýza	k1gFnSc1	kryptoanalýza
</sent>
<sent id="5306">
kryptografie	kryptografie	k1gFnSc1	kryptografie
</sent>
<sent id="5310">
metajazyk	metajazyk	k1gInSc1	metajazyk
</sent>
<sent id="5315">
normalizace	normalizace	k1gFnSc2	normalizace
</sent>
<sent id="5317">
notebook	notebook	k1gInSc1	notebook
</sent>
<sent id="5323">
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
</sent>
<sent id="5328">
spojka	spojka	k1gFnSc1	spojka
</sent>
<sent id="5335">
firmware	firmware	k1gInSc1	firmware
</sent>
<sent id="5336">
softwarová	softwarový	k2eAgFnSc1d1	softwarová
licence	licence	k1gFnSc1	licence
</sent>
<sent id="5338">
stolní	stolní	k2eAgInSc1d1	stolní
počítač	počítač	k1gInSc1	počítač
</sent>
<sent id="5358">
geodynamika	geodynamika	k1gFnSc1	geodynamika
</sent>
<sent id="5360">
geopotenciál	geopotenciál	k1gInSc1	geopotenciál
</sent>
<sent id="5415">
odsazení	odsazení	k1gNnSc6	odsazení
oblouku	oblouk	k1gInSc2	oblouk
kružnice	kružnice	k1gFnSc2	kružnice
</sent>
<sent id="5450">
určování	určování	k1gNnSc2	určování
prostorových	prostorový	k2eAgInPc2d1	prostorový
vztahů	vztah	k1gInPc2	vztah
(	(	kIx(	
<g/>
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
)	)	kIx)	
</sent>
<sent id="5452">
vedení	vedení	k1gNnPc4	vedení
technického	technický	k2eAgNnSc2d1	technické
vybavení	vybavení	k1gNnSc2	vybavení
</sent>
<sent id="5491">
digitální	digitální	k2eAgInSc4d1	digitální
vyjádření	vyjádření	k1gNnSc1	vyjádření
(	(	kIx(	
<g/>
naměřené	naměřený	k2eAgFnPc1d1	naměřená
veličiny	veličina	k1gFnPc1	veličina
<g/>
)	)	kIx)	
</sent>
<sent id="5526">
mapový	mapový	k2eAgInSc1d1	mapový
archiv	archiv	k1gInSc1	archiv
</sent>
<sent id="5534">
základní	základní	k2eAgFnSc1d1	základní
sídelní	sídelní	k2eAgFnSc1d1	sídelní
jednotka	jednotka	k1gFnSc1	jednotka
</sent>
<sent id="5546">
minuskule	minuskule	k1gFnSc1	minuskule
</sent>
<sent id="5594">
oprava	oprava	k1gFnSc1	oprava
ze	z	k7c2	
zakřivení	zakřivení	k1gNnSc2	zakřivení
Země	zem	k1gFnSc2	zem
a	a	k8xC	
z	z	k7c2	
refrakce	refrakce	k1gFnSc2	refrakce
</sent>
<sent id="5603">
rektifikace	rektifikace	k1gFnSc1	rektifikace
přístroje	přístroj	k1gInSc2	přístroj
</sent>
<sent id="5604">
setinný	setinný	k2eAgInSc4d1	setinný
stupeň	stupeň	k1gInSc4	stupeň
</sent>
<sent id="5607">
šedesátinný	šedesátinný	k2eAgInSc4d1	šedesátinný
stupeň	stupeň	k1gInSc4	stupeň
</sent>
<sent id="5612">
totální	totální	k2eAgFnSc1d1	totální
stanice	stanice	k1gFnSc1	stanice
</sent>
<sent id="5638">
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
</sent>
<sent id="5658">
ochrana	ochrana	k1gFnSc1	ochrana
autorského	autorský	k2eAgNnSc2d1	autorské
práva	právo	k1gNnSc2	právo
</sent>
<sent id="5675">
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
</sent>
<sent id="5686">
bradlo	bradlo	k1gNnSc4	bradlo
</sent>
<sent id="5698">
W3C	W3C	k1gFnSc1	W3C
</sent>
<sent id="5700">
pantograf	pantograf	k1gInSc1	pantograf
</sent>
<sent id="5771">
stereogram	stereogram	k1gInSc1	stereogram
</sent>
<sent id="5782">
škrabka	škrabka	k1gFnSc1	škrabka
</sent>
<sent id="5789">
celkový	celkový	k2eAgInSc1d1	celkový
rastr	rastr	k1gInSc1	rastr
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
</sent>
<sent id="5806">
dálkový	dálkový	k2eAgInSc4d1	dálkový
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	
ISKN	ISKN	kA	
</sent>
<sent id="5807">
definiční	definiční	k2eAgInSc4d1	definiční
bod	bod	k1gInSc4	bod
budovy	budova	k1gFnSc2	budova
nebo	nebo	k8xC	
vodního	vodní	k2eAgNnSc2d1	vodní
díla	dílo	k1gNnSc2	dílo
</sent>
<sent id="5808">
definiční	definiční	k2eAgInSc4d1	definiční
bod	bod	k1gInSc4	bod
parcely	parcela	k1gFnSc2	parcela
</sent>
<sent id="5809">
elektronický	elektronický	k2eAgInSc4d1	elektronický
podpis	podpis	k1gInSc4	podpis
</sent>
<sent id="5810">
globální	globální	k2eAgInSc4d1	globální
transformační	transformační	k2eAgInSc4d1	transformační
klíč	klíč	k1gInSc4	klíč
</sent>
<sent id="5811">
lokalita	lokalita	k1gFnSc1	lokalita
</sent>
<sent id="5812">
nahlížení	nahlížení	k1gNnSc2	nahlížení
do	do	k7c2	
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="5813">
nový	nový	k2eAgInSc4d1	nový
výměnný	výměnný	k2eAgInSc4d1	výměnný
formát	formát	k1gInSc4	formát
ISKN	ISKN	kA	
</sent>
<sent id="5814">
orientační	orientační	k2eAgFnSc1d1	orientační
mapa	mapa	k1gFnSc1	mapa
parcel	parcela	k1gFnPc2	parcela
</sent>
<sent id="5815">
převod	převod	k1gInSc1	převod
číselného	číselný	k2eAgNnSc2d1	číselné
vyjádření	vyjádření	k1gNnSc2	vyjádření
analogové	analogový	k2eAgFnSc2d1	analogová
mapy	mapa	k1gFnSc2	mapa
v	v	k7c6	
S-JTSK	S-JTSK	k1gFnSc6	S-JTSK
do	do	k7c2	
digitální	digitální	k2eAgFnSc2d1	digitální
podoby	podoba	k1gFnSc2	podoba
</sent>
<sent id="5816">
přídělové	přídělový	k2eAgNnSc4d1	přídělové
řízení	řízení	k1gNnSc4	řízení
</sent>
<sent id="5817">
rekonstruovaný	rekonstruovaný	k2eAgInSc1d1	rekonstruovaný
rastr	rastr	k1gInSc1	rastr
mapového	mapový	k2eAgInSc2d1	mapový
listu	list	k1gInSc2	list
</sent>
<sent id="5818">
souvislý	souvislý	k2eAgInSc1d1	souvislý
rastr	rastr	k1gInSc1	rastr
</sent>
<sent id="5819">
zaručený	zaručený	k2eAgInSc4d1	zaručený
elektronický	elektronický	k2eAgInSc4d1	elektronický
podpis	podpis	k1gInSc4	podpis
</sent>
<sent id="5820">
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
sáh	sáh	k1gInSc4	sáh
</sent>
<sent id="5821">
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
rastr	rastr	k1gInSc1	rastr
výchozího	výchozí	k2eAgInSc2d1	výchozí
mapového	mapový	k2eAgInSc2d1	mapový
podkladu	podklad	k1gInSc2	podklad
</sent>
<sent id="5875">
nosná	nosný	k2eAgFnSc1d1	nosná
vlna	vlna	k1gFnSc1	vlna
</sent>
<sent id="5939">
technická	technický	k2eAgFnSc1d1	technická
redakce	redakce	k1gFnSc1	redakce
</sent>
<sent id="5950">
tučné	tučný	k2eAgNnSc4d1	tučné
písmo	písmo	k1gNnSc4	písmo
</sent>
<sent id="5951">
tuš	tuš	k1gFnSc1	tuš
</sent>
<sent id="5964">
verzálka	verzálka	k1gFnSc1	verzálka
</sent>
<sent id="5973">
výstava	výstava	k1gFnSc1	výstava
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="6024">
rektifikační	rektifikační	k2eAgFnSc1d1	rektifikační
hodnota	hodnota	k1gFnSc1	hodnota
ve	v	k7c6	
strojírenství	strojírenství	k1gNnSc6	strojírenství
</sent>
<sent id="6073">
mapová	mapový	k2eAgFnSc1d1	mapová
dokumentace	dokumentace	k1gFnSc1	dokumentace
</sent>
<sent id="6127">
antikva	antikva	k1gFnSc1	antikva
</sent>
<sent id="6184">
geografie	geografie	k1gFnSc1	geografie
</sent>
<sent id="6195">
normalizovaný	normalizovaný	k2eAgInSc4d1	normalizovaný
formát	formát	k1gInSc4	formát
</sent>
<sent id="6232">
součtové	součtový	k2eAgNnSc4d1	součtové
kružítko	kružítko	k1gNnSc4	kružítko
</sent>
<sent id="6345">
jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
</sent>
<sent id="6346">
kartografické	kartografický	k2eAgNnSc1d1	kartografické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
</sent>
<sent id="6354">
nejstarší	starý	k2eAgFnSc2d3	nejstarší
mapové	mapový	k2eAgFnSc2d1	mapová
památky	památka	k1gFnSc2	památka
</sent>
<sent id="6372">
brýle	brýle	k1gFnPc4	brýle
s	s	k7c7	
tekutými	tekutý	k2eAgInPc7d1	tekutý
krystaly	krystal	k1gInPc7	krystal
</sent>
<sent id="6383">
pozemní	pozemní	k2eAgInSc4d1	pozemní
měřický	měřický	k2eAgInSc4d1	měřický
snímek	snímek	k1gInSc4	snímek
</sent>
<sent id="6412">
BSI	BSI	kA	
</sent>
<sent id="6413">
CAI	CAI	kA	
</sent>
<sent id="6420">
CMI	CMI	kA	
</sent>
<sent id="6421">
DIN	din	k1gInSc1	din
</sent>
<sent id="6429">
WMO	WMO	kA	
</sent>
<sent id="6467">
algoritmická	algoritmický	k2eAgFnSc1d1	algoritmická
generalizace	generalizace	k1gFnSc1	generalizace
</sent>
<sent id="6526">
severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
</sent>
<sent id="6548">
ROB	roba	k1gFnPc2	roba
</sent>
<sent id="6549">
ROS	Rosa	k1gFnPc2	Rosa
</sent>
<sent id="6550">
RPP	RPP	kA	
</sent>
<sent id="6561">
klasifikace	klasifikace	k1gFnSc1	klasifikace
měřítek	měřítko	k1gNnPc2	měřítko
</sent>
<sent id="6573">
kruhová	kruhový	k2eAgFnSc1d1	kruhová
křivítka	křivítko	k1gNnSc2	křivítko
</sent>
<sent id="6617">
převodové	převodový	k2eAgNnSc4d1	převodové
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="6621">
zaokrouhlené	zaokrouhlený	k2eAgNnSc4d1	zaokrouhlené
měřítko	měřítko	k1gNnSc4	měřítko
</sent>
<sent id="6624">
oční	oční	k2eAgFnSc1d1	oční
základna	základna	k1gFnSc1	základna
</sent>
<sent id="6645">
poměr	poměr	k1gInSc1	poměr
signál	signál	k1gInSc4	signál
<g/>
/	/	kIx~	
<g/>
šum	šum	k1gInSc1	šum
</sent>
<sent id="6649">
rámové	rámový	k2eAgInPc1d1	rámový
údaje	údaj	k1gInPc1	údaj
</sent>
<sent id="7207">
Katastrální	katastrální	k2eAgInSc4d1	katastrální
pracoviště	pracoviště	k1gNnSc1	pracoviště
(	(	kIx(	
<g/>
KP	KP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="6667">
panorama	panorama	k1gNnSc4	panorama
</sent>
<sent id="6685">
divis	divis	k1gInSc1	divis
</sent>
<sent id="6687">
jemné	jemný	k2eAgNnSc4d1	jemné
písmo	písmo	k1gNnSc4	písmo
</sent>
<sent id="6693">
majuskule	majuskule	k1gFnSc1	majuskule
</sent>
<sent id="6696">
ruční	ruční	k2eAgFnSc1d1	ruční
sazba	sazba	k1gFnSc1	sazba
</sent>
<sent id="6697">
sazeč	sazeč	k1gMnSc1	sazeč
</sent>
<sent id="6698">
serif	serif	k1gInSc1	serif
</sent>
<sent id="6701">
strojová	strojový	k2eAgFnSc1d1	strojová
sazba	sazba	k1gFnSc1	sazba
</sent>
<sent id="6702">
široké	široký	k2eAgNnSc4d1	široké
písmo	písmo	k1gNnSc4	písmo
</sent>
<sent id="6706">
úzké	úzké	k1gInPc4	úzké
písmo	písmo	k1gNnSc4	písmo
</sent>
<sent id="6710">
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
výška	výška	k1gFnSc1	výška
(	(	kIx(	
<g/>
H	H	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="6714">
střední	střední	k2eAgFnSc1d1	střední
hladina	hladina	k1gFnSc1	hladina
moře	moře	k1gNnSc2	moře
</sent>
<sent id="6721">
zploštění	zploštění	k1gNnSc4	zploštění
</sent>
<sent id="6724">
asociace	asociace	k1gFnSc2	asociace
</sent>
<sent id="6732">
údržba	údržba	k1gFnSc1	údržba
souboru	soubor	k1gInSc2	soubor
</sent>
<sent id="6768">
popisování	popisování	k1gNnSc6	popisování
mapy	mapa	k1gFnSc2	mapa
</sent>
<sent id="6792">
výrobní	výrobní	k2eAgInSc4d1	výrobní
časový	časový	k2eAgInSc4d1	časový
plán	plán	k1gInSc4	plán
</sent>
<sent id="6832">
CENIA	CENIA	kA	
</sent>
<sent id="6833">
CPD	CPD	kA	
</sent>
<sent id="6836">
e-Government	e-Government	k1gInSc1	e-Government
</sent>
<sent id="6838">
KŘ	KŘ	kA	
</sent>
<sent id="6844">
STANAG	STANAG	kA	
</sent>
<sent id="6847">
VaV	VaV	k1gFnSc1	VaV
</sent>
<sent id="6848">
v.v.i.	v.v.i.	k?	
</sent>
<sent id="6849">
VaVaI	VaVaI	k1gFnSc1	VaVaI
</sent>
<sent id="6853">
kartografické	kartografický	k2eAgFnSc2d1	kartografická
techniky	technika	k1gFnSc2	technika
</sent>
<sent id="6886">
kruhová	kruhový	k2eAgFnSc1d1	kruhová
značka	značka	k1gFnSc1	značka
</sent>
<sent id="6949">
tiskárna	tiskárna	k1gFnSc1	tiskárna
</sent>
<sent id="6978">
dávkovač	dávkovač	k1gInSc1	dávkovač
světla	světlo	k1gNnSc2	světlo
</sent>
<sent id="6983">
filmová	filmový	k2eAgFnSc1d1	filmová
kazeta	kazeta	k1gFnSc1	kazeta
</sent>
<sent id="6984">
fotochemie	fotochemie	k1gFnSc1	fotochemie
</sent>
<sent id="6997">
kartotéka	kartotéka	k1gFnSc1	kartotéka
</sent>
<sent id="7001">
knihařské	knihařský	k2eAgFnSc2d1	knihařská
nitě	nit	k1gFnSc2	nit
</sent>
<sent id="7014">
tloušťka	tloušťka	k1gFnSc1	tloušťka
čáry	čára	k1gFnSc2	čára
</sent>
<sent id="7027">
polygraf	polygraf	k1gMnSc1	polygraf
</sent>
<sent id="7029">
popisek	popisek	k1gInSc1	popisek
</sent>
<sent id="7030">
reprodukce	reprodukce	k1gFnSc2	reprodukce
</sent>
<sent id="7201">
LAU	LAU	kA	
</sent>
<sent id="7039">
výtisk	výtisk	k1gInSc1	výtisk
</sent>
<sent id="7057">
tiskové	tiskový	k2eAgNnSc1d1	tiskové
písmo	písmo	k1gNnSc1	písmo
</sent>
<sent id="7075">
řez	řez	k1gInSc1	řez
písma	písmo	k1gNnSc2	písmo
</sent>
<sent id="7086">
opacita	opacita	k1gFnSc1	opacita
</sent>
<sent id="7093">
trať	trať	k1gFnSc1	trať
</sent>
<sent id="7099">
laserový	laserový	k2eAgInSc4d1	laserový
značkovač	značkovač	k1gInSc4	značkovač
</sent>
<sent id="7115">
opakované	opakovaný	k2eAgNnSc1d1	opakované
vydání	vydání	k1gNnSc1	vydání
</sent>
<sent id="7116">
patitul	patitul	k1gInSc1	patitul
</sent>
<sent id="7123">
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
</sent>
<sent id="7136">
vložená	vložený	k2eAgFnSc1d1	vložená
příloha	příloha	k1gFnSc1	příloha
</sent>
<sent id="7148">
EGU	ego	k1gNnSc6	ego
</sent>
<sent id="7156">
systém	systém	k1gInSc1	systém
dálkově	dálkově	k6eAd1	
řízeného	řízený	k2eAgNnSc2d1	řízené
letadla	letadlo	k1gNnSc2	letadlo
(	(	kIx(	
<g/>
RPAS	RPAS	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7210">
Zeměměřický	zeměměřický	k2eAgInSc4d1	zeměměřický
a	a	k8xC	
katastrální	katastrální	k2eAgInSc4d1	katastrální
inspektorát	inspektorát	k1gInSc4	inspektorát
(	(	kIx(	
<g/>
ZKI	ZKI	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7232">
NOZ	NOZ	kA	
</sent>
<sent id="7233">
budoucí	budoucí	k2eAgInSc4d1	budoucí
výměnek	výměnek	k1gInSc4	výměnek
</sent>
<sent id="7234">
dočasná	dočasný	k2eAgFnSc1d1	dočasná
stavba	stavba	k1gFnSc1	stavba
</sent>
<sent id="7235">
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	
<g/>
dle	dle	k7c2	
NOZ	NOZ	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7236">
materiální	materiální	k2eAgFnSc1d1	materiální
publicita	publicita	k1gFnSc1	publicita
</sent>
<sent id="7237">
nájem	nájem	k1gInSc1	nájem
</sent>
<sent id="7238">
nájemce	nájemce	k1gMnSc1	nájemce
</sent>
<sent id="7239">
pacht	pacht	k1gInSc1	pacht
</sent>
<sent id="7240">
pachtýř	pachtýř	k1gMnSc1	pachtýř
</sent>
<sent id="7241">
poznámka	poznámka	k1gFnSc1	poznámka
spornosti	spornost	k1gFnSc2	spornost
</sent>
<sent id="7242">
přestavek	přestavek	k1gInSc1	přestavek
</sent>
<sent id="7243">
přídatné	přídatný	k2eAgNnSc1d1	přídatné
spoluvlastnictví	spoluvlastnictví	k1gNnSc1	spoluvlastnictví
</sent>
<sent id="7244">
reálné	reálný	k2eAgNnSc1d1	reálné
břemeno	břemeno	k1gNnSc1	břemeno
</sent>
<sent id="7245">
rozhrada	rozhrada	k1gFnSc1	rozhrada
</sent>
<sent id="7246">
služebná	služebný	k2eAgFnSc1d1	služebná
parcela	parcela	k1gFnSc1	parcela
</sent>
<sent id="7247">
služebnost	služebnost	k1gFnSc1	služebnost
</sent>
<sent id="7248">
služebnost	služebnost	k1gFnSc1	služebnost
cesty	cesta	k1gFnSc2	cesta
</sent>
<sent id="7249">
spoluvlastnictví	spoluvlastnictví	k1gNnSc4	spoluvlastnictví
</sent>
<sent id="7250">
svěřenecký	svěřenecký	k2eAgInSc1d1	svěřenecký
fond	fond	k1gInSc1	fond
</sent>
<sent id="7251">
užívací	užívací	k2eAgInSc4d1	užívací
právo	právo	k1gNnSc1	právo
jako	jako	k8xC,k8xS	
služebnost	služebnost	k1gFnSc1	služebnost
</sent>
<sent id="7252">
veřejný	veřejný	k2eAgInSc4d1	veřejný
seznam	seznam	k1gInSc4	seznam
</sent>
<sent id="7261">
agendový	agendový	k2eAgInSc1d1	agendový
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
</sent>
<sent id="7266">
orgán	orgán	k1gInSc1	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
</sent>
<sent id="7271">
pozemková	pozemkový	k2eAgFnSc1d1	pozemková
správa	správa	k1gFnSc1	správa
</sent>
<sent id="7272">
restituce	restituce	k1gFnSc1	restituce
</sent>
<sent id="7273">
vyvlastnění	vyvlastnění	k1gNnSc6	vyvlastnění
</sent>
<sent id="7274">
zástava	zástava	k1gFnSc1	zástava
</sent>
<sent id="7275">
zástavní	zástavní	k2eAgInSc4d1	zástavní
právo	právo	k1gNnSc1	právo
k	k	k7c3	
nemovitosti	nemovitost	k1gFnSc3	nemovitost
</sent>
<sent id="7291">
GMES	GMES	kA	
(	(	kIx(	
<g/>
nově	nově	k6eAd1	
Copernicus	Copernicus	k1gInSc1	Copernicus
<g/>
)	)	kIx)	
</sent>
<sent id="7293">
IPR	IPR	kA	
Praha	Praha	k1gFnSc1	Praha
</sent>
<sent id="7294">
SPÚ	SPÚ	kA	
</sent>
<sent id="7295">
Geodetický	geodetický	k2eAgInSc4d1	geodetický
a	a	k8xC	
kartografický	kartografický	k2eAgInSc4d1	kartografický
obzor	obzor	k1gInSc4	obzor
(	(	kIx(	
<g/>
GaKO	GaKO	k1gFnSc4	GaKO
<g/>
)	)	kIx)	
</sent>
<sent id="7312">
ČHMÚ	ČHMÚ	kA	
</sent>
<sent id="7315">
ÚAP	ÚAP	kA	
</sent>
<sent id="7326">
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
prostorových	prostorový	k2eAgNnPc2d1	prostorové
dat	datum	k1gNnPc2	datum
</sent>
<sent id="7384">
ICAO	ICAO	kA	
</sent>
<sent id="7418">
stavba	stavba	k1gFnSc1	stavba
</sent>
<sent id="7425">
znázornění	znázornění	k1gNnSc4	znázornění
</sent>
<sent id="7467">
kanál	kanál	k1gInSc1	kanál
</sent>
<sent id="7473">
M-kód	M-kód	k1gInSc1	M-kód
</sent>
<sent id="7474">
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
služby	služba	k1gFnSc2	služba
GNSS	GNSS	kA	
</sent>
<sent id="7477">
otevřená	otevřený	k2eAgFnSc1d1	otevřená
služba	služba	k1gFnSc1	služba
</sent>
<sent id="7479">
posun	posun	k1gInSc1	posun
hodin	hodina	k1gFnPc2	hodina
</sent>
<sent id="7481">
rychlost	rychlost	k1gFnSc1	rychlost
přenosu	přenos	k1gInSc2	přenos
čipů	čip	k1gInPc2	čip
</sent>
<sent id="7493">
mezní	mezní	k2eAgFnSc1d1	mezní
odchylka	odchylka	k1gFnSc1	odchylka
</sent>
<sent id="7523">
CSCC	CSCC	kA	
</sent>
<sent id="7525">
kyberbezpečnost	kyberbezpečnost	k1gFnSc1	kyberbezpečnost
</sent>
<sent id="7531">
číslo	číslo	k1gNnSc4	číslo
popisné	popisný	k2eAgFnSc2d1	popisná
</sent>
<sent id="7534">
městský	městský	k2eAgInSc1d1	městský
obvod	obvod	k1gInSc1	obvod
/	/	kIx~	
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
</sent>
<sent id="7559">
číslo	číslo	k1gNnSc4	číslo
evidenční	evidenční	k2eAgFnSc2d1	evidenční
</sent>
<sent id="7567">
HGZ	HGZ	kA	
</sent>
<sent id="7582">
skalní	skalní	k2eAgFnPc4d1	skalní
šrafy	šrafa	k1gFnPc4	šrafa
</sent>
<sent id="7609">
centrální	centrální	k2eAgFnSc1d1	centrální
závěrka	závěrka	k1gFnSc1	závěrka
</sent>
<sent id="7627">
územní	územní	k2eAgInSc4d1	územní
systém	systém	k1gInSc4	systém
ekologické	ekologický	k2eAgFnSc2d1	ekologická
stability	stabilita	k1gFnSc2	stabilita
</sent>
<sent id="7630">
znalecký	znalecký	k2eAgInSc4d1	znalecký
posudek	posudek	k1gInSc4	posudek
</sent>
<sent id="7632">
určená	určený	k2eAgFnSc1d1	určená
norma	norma	k1gFnSc1	norma
</sent>
<sent id="7633">
záborový	záborový	k2eAgInSc4d1	záborový
elaborát	elaborát	k1gInSc4	elaborát
</sent>
<sent id="7634">
budoucí	budoucí	k2eAgMnSc1d1	budoucí
zástavní	zástavní	k2eAgNnSc4d1	zástavní
právo	právo	k1gNnSc4	právo
</sent>
<sent id="7635">
budova	budova	k1gFnSc1	budova
bez	bez	k7c2	
čísla	číslo	k1gNnSc2	číslo
popisného	popisný	k2eAgNnSc2d1	popisné
a	a	k8xC	
evidenčního	evidenční	k2eAgNnSc2d1	evidenční
</sent>
<sent id="7636">
budova	budova	k1gFnSc1	budova
s	s	k7c7	
číslem	číslo	k1gNnSc7	číslo
evidenčním	evidenční	k2eAgNnSc7d1	evidenční
</sent>
<sent id="7637">
budova	budova	k1gFnSc1	budova
s	s	k7c7	
číslem	číslo	k1gNnSc7	číslo
popisným	popisný	k2eAgNnSc7d1	popisné
</sent>
<sent id="7638">
číslo	číslo	k1gNnSc4	číslo
evidenční	evidenční	k2eAgFnSc2d1	evidenční
</sent>
<sent id="7639">
číslo	číslo	k1gNnSc4	číslo
orientační	orientační	k2eAgFnSc2d1	orientační
</sent>
<sent id="7640">
chmelnice	chmelnice	k1gFnSc1	chmelnice
</sent>
<sent id="7641">
katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
digitalizovaná	digitalizovaný	k2eAgFnSc1d1	digitalizovaná
</sent>
<sent id="7642">
obec	obec	k1gFnSc1	obec
s	s	k7c7	
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
</sent>
<sent id="7643">
obnova	obnova	k1gFnSc1	obnova
katastrálního	katastrální	k2eAgInSc2d1	katastrální
operátu	operát	k1gInSc2	operát
novým	nový	k2eAgNnSc7d1	nové
mapováním	mapování	k1gNnSc7	mapování
</sent>
<sent id="7644">
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
</sent>
<sent id="7645">
ovocný	ovocný	k2eAgInSc1d1	ovocný
sad	sad	k1gInSc1	sad
</sent>
<sent id="7646">
parcela	parcela	k1gFnSc1	parcela
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
evidence	evidence	k1gFnSc2	evidence
</sent>
<sent id="7647">
podlaží	podlaží	k1gNnSc6	podlaží
</sent>
<sent id="7648">
podzástavní	podzástavní	k2eAgInSc4d1	podzástavní
právo	právo	k1gNnSc1	právo
</sent>
<sent id="7649">
poznámka	poznámka	k1gFnSc1	poznámka
</sent>
<sent id="7650">
poznámka	poznámka	k1gFnSc1	poznámka
k	k	k7c3	
nemovitosti	nemovitost	k1gFnSc3	nemovitost
</sent>
<sent id="7651">
poznámka	poznámka	k1gFnSc1	poznámka
k	k	k7c3	
osobě	osoba	k1gFnSc3	osoba
</sent>
<sent id="7652">
právo	právo	k1gNnSc4	právo
stavby	stavba	k1gFnSc2	stavba
</sent>
<sent id="7653">
Registr	registr	k1gInSc1	registr
územní	územní	k2eAgFnSc2d1	územní
identifikace	identifikace	k1gFnSc2	identifikace
<g/>
,	,	kIx,	
adres	adresa	k1gFnPc2	adresa
a	a	k8xC	
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="7654">
správa	správa	k1gFnSc1	správa
katastru	katastr	k1gInSc2	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
</sent>
<sent id="7655">
správa	správa	k1gFnSc1	správa
svěřenského	svěřenský	k2eAgInSc2d1	svěřenský
fondu	fond	k1gInSc2	fond
</sent>
<sent id="7656">
trvalý	trvalý	k2eAgInSc4d1	trvalý
travní	travní	k2eAgInSc4d1	travní
porost	porost	k1gInSc4	porost
</sent>
<sent id="7657">
ujednání	ujednání	k1gNnPc4	ujednání
o	o	k7c4	
koupi	koupě	k1gFnSc4	koupě
na	na	k7c4	
zkoušku	zkouška	k1gFnSc4	zkouška
</sent>
<sent id="7658">
veřejný	veřejný	k2eAgInSc4d1	veřejný
dálkový	dálkový	k2eAgInSc4d1	dálkový
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	
datům	datum	k1gNnPc3	datum
RÚIAN	RÚIAN	kA	
(	(	kIx(	
<g/>
VDP	VDP	kA	
<g/>
)	)	kIx)	
</sent>
<sent id="7659">
vinice	vinice	k1gFnSc1	vinice
</sent>
<sent id="7660">
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
kresba	kresba	k1gFnSc1	kresba
</sent>
<sent id="7661">
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
</sent>
<sent id="7662">
výhrada	výhrada	k1gFnSc1	výhrada
práva	právo	k1gNnSc2	právo
lepšího	dobrý	k2eAgMnSc2d2	lepší
kupce	kupec	k1gMnSc2	kupec
</sent>
<sent id="7663">
výhrada	výhrada	k1gFnSc1	výhrada
práva	právo	k1gNnSc2	právo
zpětné	zpětný	k2eAgFnSc2d1	zpětná
koupě	koupě	k1gFnSc2	koupě
</sent>
<sent id="7664">
výhrada	výhrada	k1gFnSc1	výhrada
vlastnického	vlastnický	k2eAgNnSc2d1	vlastnické
práva	právo	k1gNnSc2	právo
</sent>
<sent id="7665">
výhrada	výhrada	k1gFnSc1	výhrada
zpětného	zpětný	k2eAgInSc2d1	zpětný
prodeje	prodej	k1gInSc2	prodej
</sent>
<sent id="7666">
výměnný	výměnný	k2eAgInSc4d1	výměnný
formát	formát	k1gInSc4	formát
dat	datum	k1gNnPc2	datum
RÚIAN	RÚIAN	kA	
</sent>
<sent id="7667">
výměnný	výměnný	k2eAgInSc4d1	výměnný
formát	formát	k1gInSc4	formát
ISKN	ISKN	kA	
</sent>
<sent id="7668">
vzdání	vzdání	k1gNnSc6	vzdání
se	s	k7c7	
práva	právo	k1gNnSc2	právo
na	na	k7c4	
náhradu	náhrada	k1gFnSc4	náhrada
škody	škoda	k1gFnSc2	škoda
na	na	k7c6	
pozemku	pozemek	k1gInSc6	pozemek
</sent>
<sent id="7669">
zahrada	zahrada	k1gFnSc1	zahrada
</sent>
<sent id="7670">
zákaz	zákaz	k1gInSc1	zákaz
zcizení	zcizení	k1gNnSc2	zcizení
nebo	nebo	k8xC	
zatížení	zatížení	k1gNnSc2	zatížení
</sent>
<sent id="7671">
zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
plocha	plocha	k1gFnSc1	plocha
a	a	k8xC	
nádvoří	nádvoří	k1gNnSc1	nádvoří
</sent>
<sent id="168">
Digitální	digitální	k2eAgFnSc1d1	digitální
agenda	agenda	k1gFnSc1	agenda
pro	pro	k7c4	
Evropu	Evropa	k1gFnSc4	Evropa
</sent>
<sent id="167">
Gestor	gestor	k1gMnSc1	gestor
agendy	agenda	k1gFnSc2	agenda
</sent>
<sent id="170">
agendový	agendový	k2eAgInSc1d1	agendový
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
</sent>
<sent id="172">
test	test	k1gInSc1	test
<g/>
22	#num#	k4	
</sent>
<sent id="171">
test	test	k1gInSc1	test
<g/>
2	#num#	k4	
</sent>
<sent id="166">
Digitální	digitální	k2eAgFnSc1d1	digitální
agenda	agenda	k1gFnSc1	agenda
pro	pro	k7c4	
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
2	#num#	k4	
</sent>
<sent id="1009">
aplikace	aplikace	k1gFnSc2	aplikace
</sent>
<sent id="1017">
barevnost	barevnost	k1gFnSc1	barevnost
map	mapa	k1gFnPc2	mapa
</sent>
<sent id="1025">
bonita	bonita	k1gFnSc1	bonita
půdy	půda	k1gFnSc2	půda
</sent>
<sent id="1030">
buňka	buňka	k1gFnSc1	buňka
</sent>
<sent id="1039">
data	datum	k1gNnSc2	datum
</sent>
<sent id="1053">
digitální	digitální	k2eAgInSc4d1	digitální
výškový	výškový	k2eAgInSc4d1	výškový
model	model	k1gInSc4	model
</sent>
<sent id="1072">
geografická	geografický	k2eAgFnSc1d1	geografická
informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
</sent>
<sent id="1100">
identifikátor	identifikátor	k1gInSc1	identifikátor
</sent>
<sent id="1101">
implementace	implementace	k1gFnSc2	implementace
</sent>
<sent id="1102">
implicitní	implicitní	k2eAgFnSc1d1	implicitní
hodnota	hodnota	k1gFnSc1	hodnota
</sent>
<sent id="1106">
informatika	informatika	k1gFnSc1	informatika
</sent>
<sent id="3001">
AČR	AČR	kA	
</sent>
<sent id="1109">
interoperabilita	interoperabilita	k1gFnSc1	interoperabilita
</sent>
<sent id="1113">
jev	jev	k1gInSc1	jev
</sent>
<sent id="1138">
kreslicí	kreslicí	k2eAgInSc4d1	kreslicí
zařízení	zařízení	k1gNnSc1	zařízení
</sent>
<sent id="1160">
místo	místo	k1gNnSc4	místo
</sent>
<sent id="1179">
ochrana	ochrana	k1gFnSc1	ochrana
dat	datum	k1gNnPc2	datum
</sent>
<sent id="1181">
oprava	oprava	k1gFnSc1	oprava
</sent>
