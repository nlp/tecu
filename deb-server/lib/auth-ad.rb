class TediImapAuth < WEBrick::HTTPAuth::BasicAuth
  require 'net/ldap'
  include WEBrick::HTTPAuth::Authenticator 

  def initialize(config, default=WEBrick::Config::BasicAuth)
    super
    @userdb = config[:UserDB]
    @authorized = {}
  end

  def authenticate(req, res)
    unless basic_credentials = check_scheme(req)
      challenge(req, res)
    end
    userid, password = basic_credentials.unpack("m*")[0].split(":", 2) 
    password ||= ""
    if userid.empty?
      error("user id was not given.")
      challenge(req, res)
    end
    time_pass = true if @authorized[userid].to_i + 30*60 > Time.now.to_i
    dbpass = @userdb[userid]
    unless time_pass or (dbpass != nil and dbpass == password.crypt(dbpass)) or ldap_auth(userid, password)
      error("%s: password unmatch.", userid)
      challenge(req, res)
    end
    @authorized[userid] = Time.now.to_i
    info("%s: authentication succeeded.", userid)
    req.user = userid
  end

  def ldap_auth(login, pass)
    SERVER = 'ldap.server'  
    PORT = 389                 
    BASE = 'DC=company,DC=cz'
    DOMAIN = 'company.cz'       

    conn = Net::LDAP.new :host => SERVER,
      :port => PORT,
      :base => BASE,
      :auth => { :username => "#{login}@#{DOMAIN}",
                 :password => pass,
                 :method => :simple }
      if conn.bind and user = conn.search(:filter => "sAMAccountName=#{login}").first
        return true
      else
        return nil
      end

  end

end

