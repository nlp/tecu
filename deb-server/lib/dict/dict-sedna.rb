require 'rubygems'
require 'sedna'

class DictSedna
  attr_accessor :container
  attr_accessor :xslt_sheets
  attr_accessor :key_path
  attr_accessor :journal
  attr_accessor :journal_file
  attr_accessor :db_path
  attr_accessor :xslt_path
  attr_accessor :template_path
  attr_accessor :file_path
  attr_accessor :servlet
  attr_accessor :database
  attr_accessor :collection
  attr_accessor :title
  attr_accessor :dictcode
  
  def list_starts_with(key, element='')
    top = @key_path.split('/')[1]
    element = @key_path.split('/')[2..-1].join('/') if element == ''
    if key == '*'
      query = "/#{top}"
    else 
      query = "/#{top}[contains(//#{element},'#{key}')]"
    end
    $stderr.puts query
    return xquery_to_list(query).join("\n")
  end

  def new_key(pref='')
    return "#{pref}-#{Time.now.to_i}-#{rand(1000)}"
  end

  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    @database = database
    @collection = collection
    @dictcode = collection
    @db_path = journal_path

    #@sedna = Sedna.connect({:database=>database})
    @sedna = $sedna

    #if collection does not exist, create it
    if @sedna.query('fn:doc("$collections")//collection[@name="'+@collection+'"]').size == 0
        @sedna.query("CREATE COLLECTION '#{@collection}'")
    end


    @xslt_sheets = Hash.new

    @key_path = key_path
    @servlet = DictServlet
    @admindict = admindict
    @template_path = admindict.template_path if admindict != nil and admindict.template_path != nil

    @journal_file = journal_path+ '/' + collection + '.journal' if @journal_file == nil and @journal_file != 'off'
    if @journal_file != nil and @journal_file != 'off'
      @journal = File.new(@journal_file, 'a')
    end
  end

  def reconnect
    @sedna = Sedna.connect({:database=>@database})
  end

  def connected
    return @sedna.connected?
  end

  def apply_transform(name, xml_data, params=nil)
    if xml_data != nil and xml_data != ''
      xslt = XML::XSLT.new()
      xslt.xsl = get_transform(name)
      xslt.xml = xml_data.to_s
      if params != nil
        if params.is_a?(Array)
          parameters = {}
          params.each{|a|
            if a[1][0,1] == '"'
              #parameters[a[0]] = a[1]#[1..-2] unless a[1][1..-2] == ''
              parameters[a[0]] = a[1][1..-2] unless a[1][1..-2] == ''
            else
              parameters[a[0]] = '"'+a[1].to_s+'"'
            end
          }
        else
          parameters = params
        end
      end
      $stderr.puts parameters
      xslt.parameters = parameters
      s = xslt.serve()
    end
    return s.to_s
  end

  def get_transform( name )
    if ! @xslt_sheets.has_key?(name)
      raise "no such transformation #{name}"
    end
    sheet = @xslt_sheets[ name ]
    return sheet
  end

  def log_action(action, key, data, timestamp, thread, user, version=nil)
    version = @admindict.get_next_version(@dictcode) if version.nil? and not @admindict.nil?
    if @journal != nil
      thread_n = /#<Thread:(.*)>/.match(thread.to_s)[1]
      @journal.puts "<#{action} key='#{key}' timestamp='#{timestamp.to_s}' thread='#{thread_n}' user='#{user}' version='#{version}'>#{data}</#{action}>"
      @journal.flush
    end
  end

  def add(key, data, save_journal=true, version=nil)
    @sedna.load_document(data.to_s, key, @collection)
    log_action('add', key.to_s, data, Time.now.to_i, Thread.current, Thread.current[:auth_user], version) if save_journal
  end

  def get(key)
    key = key.gsub("'", '&apos;')
    $stderr.puts "GET "+key
    begin
      res = @sedna.query("doc('"+key+"', '"+@collection+"')")
      xml = res[0].gsub!('<?xml version="1.0" standalone="yes"?>','')
      return xml
    rescue
      return ''
    end
  end
  
  def starts_with( key )
    r = ''
    top = @key_path.split('/')[1]
    rest = @key_path.split('/')[2..-1].join('/')
    $stderr.puts "collection('#{@collection}')/#{top}[starts-with(#{rest},'#{key}')]"
    @sedna.query("collection('#{@collection}')/#{top}[starts-with(#{rest},'#{key}')]").each{|string|
      doc = REXML::Document.new( string.to_s)
      doc.write(r, 1)
      break
    }
    return r
  end
  
  def delete(key, save_journal=true, version=nil)
    begin
      @sedna.query('DROP DOCUMENT "'+key+'" IN COLLECTION "'+@collection+'"')
      log_action('delete', key.to_s, '', Time.now.to_i, Thread.current, Thread.current[:auth_user],version) if save_journal
    rescue => e
      $stderr.puts e
    end
  end

  def update( key, data, save_journal=true,version=nil )
    return if data.to_s == ''
    begin
       @sedna.transaction{
      @sedna.query('DROP DOCUMENT "'+key+'" IN COLLECTION "'+@collection+'"')
      @sedna.load_document(data.to_s, key, @collection)
      }
      action = 'update'
    rescue => e
      $stderr.puts e
      @sedna.transaction{
      @sedna.load_document(data.to_s, key, @collection)
      }
      action = 'add'
    end
    log_action(action, key.to_s, data, Time.now.to_i, Thread.current, Thread.current[:auth_user],version) if save_journal
  end

  def xquery_to_list(query)
    results = []
    $stderr.puts "for $x in collection('#{@collection}')#{query} return document-uri($x)"
    @sedna.query("for $x in collection('#{@collection}')#{query} return document-uri($x)").each{|s|
      results << s.gsub("'", '&apos;')
    } 
    return results
  end

  def xquery_to_hash(query)
    results = {}
    $stderr.puts 'for $x in fn:collection("'+@collection+'")'+query+' return document-uri($x)'
    @sedna.query('for $x in fn:collection("'+@collection+'")'+query+' return document-uri($x)').each{|key|
      key.gsub!("'", '&apos;')
      data = get(key)
      results[key] = data.to_s
    } 
    return results
  end
  
  def xquery_to_array(query)
    results = []
    $stderr.puts "collection('#{@collection}')#{query}"
    @sedna.query("collection('#{@collection}')#{query}").each{|s|
      results << s.to_s.gsub("'", '&apos;')
    }
    return results
  end

  def xpath2hash(xpath)
    hash = {}
    @sedna.query(xpath).each{|s|
      string = s.to_s.gsub("'", '&apos;')
      if not hash.has_key?(string)
        hash[string] = 1
      else
        hash[string] += 1
      end
    }
    return hash
  end

  def query(query)
    result = @sedna.query(query)
    return result
  end

  def dump(root=nil)
    ret = (root==nil)? '<'+@collection.to_s+'>' : '<'+root.to_s+'>'
    @sedna.query("for $x in collection('#{@collection}') return $x").each{|doc|
      ret += doc.to_s.gsub!('<?xml version="1.0" standalone="yes"?>','')
    }
    ret += (root==nil)? '</'+@collection.to_s+'>' : '</'+root.to_s+'>'
  end

  #libxml, load doc from string
  def load_xml_string(string)
    if string.to_s != ''
      doc = XML::Document.string(string, :encoding => XML::Encoding::UTF_8)
      return doc
    else
      return nil
    end
  end
end

class Dictionary < DictSedna
end

