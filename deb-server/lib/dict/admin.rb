require 'dict/dict-sedna'
require 'cobravsmongoose'

class AdminDict < DictSedna
  attr_accessor :array
  attr_accessor :service_name
  attr_accessor :dict_prefix
  attr_accessor :lock_timeout

  def initialize( collection, journal_path, key_path='', database = 'deb' )
    super
    @service_name = nil
    @dict_prefix = ''
    @array = {}
    @lock_timeout = 24*60*60 #number of seconds
  end


  #get the list of dictionaries
  def list_dicts(service=nil, files = false)
    if service == nil and @service_name != nil
      service = @service_name
    end
    
    res = {}
    if service == nil
      xquery_to_array('/dict').each {|xml|
        code = ''
        name = ''
        doc = load_xml_string(xml)
        code = doc.find('/dict/code').first.content.to_s
        name = doc.find('/dict/name').first.content.to_s
        res[code] = name
      }
    else
      servxml = get('service'+service)
      doc = load_xml_string(servxml.to_s)
      doc.find('/service/dicts/dict').each{|el|
        code = el['code'].to_s
        dictxml = get('dict'+code)
        next if dictxml.to_s == ''
        dictdoc = load_xml_string(dictxml.to_s)
        name = dictdoc.find('/dict/name').first.content.to_s
        res[code] = name
        res[code] = file if files
      }
    end
    #$stderr.puts res.to_s
    return res
  end
  
  #get the list of services 
  def list_services(htpasswd = false)
    res = {}
    xquery_to_array('/service').each {|xml|
      code = ''
      name = ''
      doc = load_xml_string(xml)
      code = doc.find('/service/code').first.content.to_s
      name = ''
      if htpasswd
        name = doc.find('/service/htpasswd').first.content.to_s unless doc.find('/service/htpasswd').first.nil?
      else
        name = doc.find('/service/name').first.content.to_s
      end
      res[code] = name
    }
    $stderr.puts res.to_s
    return res
  end
  
  #get the list of users and passwords for authentization
  def get_passwd(service = nil)
    res = []
    if service == nil and @service_name != nil
      service = @service_name
    end

    if service == nil
      query = '[user]'
    else
      query = '[user[services/service/@code="'+service+'"]]'
    end
    $stderr.puts query
    xquery_to_array(query).each {|xml|
      doc = load_xml_string(xml.to_s)
      next if doc.find('/user/pass').first.nil? 
      login = doc.find('/user/login').first.content.to_s
      pass = doc.find('/user/pass').first.content.to_s
      res << {'user'=>login, 'pass'=>pass}
    }
    #$stderr.puts res.edump('users')
    return res
  end

  #return hash of users and their permissions 
  def get_perms(service=nil)
    res = {}
    query = '/user' if service.nil?
    query = '/user[services/service/@code="'+service+'"]' unless service.nil?
    xquery_to_array(query).each {|xml|
      doc = load_xml_string(xml.to_s)
      userdicts = {}
      login = doc.find('/user/login').first.content.to_s
      res[login] = {}
      doc.find("/user/services/service").each{|el|
        res[login][el['code'].to_s] = 1
        el.find('dict').each{|del|
          permcode = el['code'].to_s+'_'+del['code'].to_s
          res[login][permcode] = del['perm'].to_s
        }
      }
      if doc.find('/user/admin').first != nil and doc.find('/user/admin').first.content == 'true'
        admin = true
      else
        admin = false
      end
      res[login]['admin'] =  admin
      comment = ''
      comment = doc.find('/user/comment').first.content.to_s unless doc.find('/user/comment').first.nil?
      res[login]['user_comment'] = comment
    }
    #$stderr.puts res.to_s
    return res
  end

  #umount & close dictionaries
  def umount(server)
    @array.each_key{ |code|
      #@array[code].container.close
      @array[code] = nil
      server.umount('/'+code)
    }
    @array = nil
  end

  #mount dictionaries
  def remount(server, service=nil, env=nil)
    if service == nil and @service_name != nil
      service = @service_name
    end

    if service == nil
      result = xquery_to_hash('/dict')
    else
      result = {}
      xquery_to_array("/service[code='#{service}']/dicts/dict").each{|xml|
        doc = load_xml_string(xml)
        code = doc.root['code'].to_s
        xmld = get('dict'+code)
        result[code] = xmld.to_s
      }
    end
    
    #@array.each_key{ |code|
    #  server.umount('/'+code)
    #}
    umount(server)
    
    @array = {}
    text = ''

    result.sort.each{ |id,xml|
      code = ''
      file = ''
      doc = load_xml_string(xml)
      code = doc.find('/dict/code').first.content.to_s
      dictname = doc.find('/dict/name').first.content.to_s
      keypath = doc.find('/dict/key').first.content.to_s
      classname = doc.find('/dict/class').first.content.to_s
      dictclass = get_class(classname)
      $stderr.puts code+':'+classname
      text += code+':'+classname + "\n"
      if code != ''  
        @array[code] = dictclass.new(code, @db_path, keypath, 'deb', self)
        @array[code].title = dictname
        @array[code].dictcode = code
        @array[code].xslt_sheets = {}
        doc.find('/dict/xslts/xslt').each{|el|
          xname = el.find('name').first.content
          xfile = el.find('file').first.content
          @array[code].xslt_sheets[xname] = @xslt_path + '/' + xfile
        }
      end
    }
    @array.each_key { |code|
      servlet = @array[code].servlet
      server.mount '/'+code, servlet, @array[code], @array
    }

    text
  end

  #sequences
  def get_next_id(dict)
    #get current number and pattern
    xml = get('seq'+dict)
    if xml != ''
      xml = get('seq'+dict)
      #$stderr.puts xml.to_s
      doc = load_xml_string(xml.to_s)
      pattern = doc.find('/seq/pattern').first.content.to_s
      id = doc.find('/seq/id').first.content.to_i
    else
      doc = load_xml_string('<seq code="'+dict+'"><id>1</id><pattern>[id]</pattern></seq>')
      id = 0
      pattern = '[id]'
    end
    
    #increase id until free slot found
    free_id = false
    while not free_id
      id += 1
      newid = get_seq_pat_id(pattern, id)
      begin
        testdoc = @array[dict].get(newid)
        free_id = true if testdoc.to_s == ''
      rescue
        free_id = true
      end
    end
    
    #save changes
    doc.find('/seq/id').first.content = id.to_s
    update('seq'+dict, doc.to_s, false)
    return newid
  end

  def get_cur_id(dict)
    xml = get('seq'+dict)
    if xml != ''
      $stderr.puts xml.to_s
      doc = load_xml_string(xml.to_s)
      id = doc.find('/id').first.content.to_i
      $stderr.puts id
      return id
    else
      return 0
    end
  end

  def get_next_version(dict)
    return get_next_id('version'+dict)
  end
  
  def get_cur_version(dict)
    return get_cur_id('version'+dict)
  end


  #replace [id] in pattern with number
  def get_seq_pat_id(pattern,id)
    if pattern.include?('[id]')
      patid = pattern.gsub('[id]', id.to_s)
    else
      patid = id.to_s
    end
    return patid
  end

  #lock entry
  def lock_entry(dictionary, entry, user, time=nil)
    if is_locked(dictionary, entry)
      return false
    end
    
    time = Time.now.to_i if time == nil

    xml = '<lock><code>'+dictionary+'</code><login>'+user+'</login><entry>'+entry+'</entry><time>'+time.to_s+'</time></lock>'
    id = 'lock'+dictionary+'_'+entry
    begin
      update(id, xml)
      $stderr.puts "LOCKED dict=#{dictionary}, entry=#{entry}, user=#{user}"
      return true
    rescue => e
      $stderr.puts "exception #{e.class.to_s}: #{e.message}"
      return false
    end
  end
  #lock entry
  def update_lock(dictionary, entry, user, time=nil)
    time = Time.now.to_i if time == nil

    xml = '<lock><code>'+dictionary+'</code><login>'+user+'</login><entry>'+entry+'</entry><time>'+time.to_s+'</time></lock>'
    id = 'lock'+dictionary+'_'+entry
    begin
      update(id, xml)
      $stderr.puts "LOCKED dict=#{dictionary}, entry=#{entry}, user=#{user}"
      return true
    rescue => e
      $stderr.puts "exception #{e.class.to_s}: #{e.message}"
      return false
    end
  end
  
  #unlock entry
  def unlock_entry(dictionary, entry, user)
    if user == who_locked(dictionary, entry)
      delete('lock'+dictionary+'_'+entry)
      return true
    else
      return false
    end
  end

  #unlock all entries of user in one dictionary
  def unlock_user_dic(dictionary, user)
    xquery_to_hash("[lock[code='#{dictionary}' and login='#{user}']]").each{|id,xml|
      delete(id)
    }
    return true
  end
  
  #unlock all entries of user
  def unlock_user(user)
    xquery_to_hash("[lock[login='#{user}']]").each{|id,xml|
      delete(id)
    }
    return true
  end
  
  #unlock all entries in one dictionary
  def unlock_dictionary(dictionary)
    xquery_to_hash("[lock[code='#{dictionary}']]").each{|id,xml|
      delete(id)
    }
    return true
  end
  
  #unlock all entries
  def unlock_all()
    xquery_to_hash("[lock]").each{|id,xml|
      delete(id)
    }
    return true
  end
  
  #unlock all entries with same time
  def unlock_time(dictionary, entry, user)
    begin
      xml = get('lock'+dictionary+'_'+entry)
      hash = CobraVsMongoose.xml_to_hash(xml.to_s)
      time = hash['lock']['time']['$']
      xquery_to_hash("/lock[login='#{user}' and time='#{time}']").each{|id,xml|
        delete(id)
      }
    rescue
    end
    return true
  end

  #check if entry is locked
  def is_locked(dictionary, entry)
    begin
      doc = get('lock'+dictionary+'_'+entry)
      if doc.to_s == ''
        return false
      else
        return true
      end
    rescue
      return false
    end
  end
  
  #return user who locked entry
  def who_locked(dictionary, entry)
    begin
      xml = get('lock'+dictionary+'_'+entry)
      hash = CobraVsMongoose.xml_to_hash(xml.to_s)
      if hash['lock']['time']['$'].to_i < (Time.now.to_i - @lock_timeout)
        $stderr.puts 'lock timeout'
        delete('lock'+dictionary+'_'+entry)
        return false
      end
      return hash['lock']['login']['$']
    rescue
      return false
    end
  end

  #return locks list for dictionary
  def get_locks(dictionary)
    locks = []
    xquery_to_array("/lock[code='#{dictionary}']").each{|xml|
      doc = load_xml_string(xml.to_s)
      locks << {'login'=>doc.find('login').first.content.to_s, 'entry'=>doc.root.find('entry').first.content.to_s, 'time'=>doc.root.find('time').first.content.to_s,}
    }
    return locks
  end

  def get_settings(user, service=nil)
    if service == nil and @service_name != nil
      service = @service_name
    end
    $stderr.puts 'get settings '+service+':'+user
    doc = get('settings_'+service+'_'+user)
    if doc != ''
      hash = CobraVsMongoose.xml_to_hash(doc)
    else
      hash = {'settings'=>{'@user'=>user, '@service'=>service}}
    end
    return hash
  end
  def save_settings(user, hash, service=nil)
    if service == nil and @service_name != nil
      service = @service_name
    end
    $stderr.puts 'save settings '+service+':'+user
    $stderr.puts hash
    xml = CobraVsMongoose.hash_to_xml(hash)
    $stderr.puts xml
    update('settings_'+service+'_'+user, xml)
  end
end
