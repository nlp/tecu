require 'dict/dict-sedna'
require 'calculus'
require 'tempfile'
require 'sqlite3'

class TeCu < DictSedna
  attr_accessor :hypers
  attr_accessor :tree
  attr_accessor :central
  attr_accessor :centralid
  attr_accessor :list_thes
  attr_accessor :alphabet

  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = TecuServlet
    @hypers = []
    @hypers = get_hypers
    @tree = get_top2
    @alphabet = get_alpha
    @central = 'https://tezaurus-centr.ogibeta2.gov.cz/registr'
    @list_thes = list_thesaurus
    @notif_db = SQLite3::Database.new("/var/lib/deb-server/db/notif.sqlite")
    @notif_db.results_as_hash = true
    @lock_timeout = 20*60
  end

  def list_thesaurus
      res = []
      begin
        url = URI.parse(@central)
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true
        req = Net::HTTP::Post.new(url.path)
        req.basic_auth 'read', 'r'
        req.set_form_data({'action' => 'list_thesaurus'})
        resp = http.request(req)
        data = JSON.parse(resp.body)
        data.each{|hash|
          res << hash unless hash["tid"] == "tecu"
        }
      rescue => e
	$stderr.puts e
      end
      return res
  end

  def get_doc(id)
    $stderr.puts "GETDOC "+id
    xml = get(id)
    if xml != ''
      doc = load_xml_string(xml)
      #definice
      #termin pro hyperonyma
      doc.find('/entry/hyper').each{|h|
        h['term'] = get_term(h['id'])
      }
      #cesty
      doc.find('/entry/paths').each{|p| p.remove!}
      paths = get_path(id)
      xmlpaths = XML::Node.new('paths')
      doc.root << xmlpaths
      paths.each{|path|
        path.reverse!
        xmlpath = XML::Node.new('path')
        xmlpaths << xmlpath
        path.each{|n|
          if n != '0'
            entry = XML::Node.new('path_node')
            entry['id'] = n
            entry['term'] = get_term(n)
            xmlpath << entry
          end
        }
        xmlpath.remove! if xmlpath.children.size == 0
      }
      #klikatelne see, also
      doc.find('/entry/sees/see[not(@entry-id)]|/entry/alsos/also[not(@entry-id)]').each{|see|
        query = 'for $x in collection("tecu")/entry[terms/term="'+see.content+'"] return data($x/@id)'
        @sedna.query(query).each{|si|
          see['entry-id'] = si
        }
      }
      #obory
      doc.find('domains').each{|d| d.remove!}
      obory = []
      settings = {}
      @admindict.array['settings'].get_settings['dict']['domains'].each{|d|
        settings[d['domain']] = d
      }
      $stderr.puts settings
      doc.find('/entry/defs/def').each{|d|
        if d['obor'].to_s != ""
          obory << d['obor'].to_s
          unless settings[d['obor']].nil?
            d['obor_color'] = settings[d['obor']]['color']
            d['obor_mark'] = settings[d['obor']]['mark']
          end
        end
      }
      if obory.uniq.length > 0
        doc.root << XML::Node.new('domains')
        obory.uniq.each{|obor|
          nd = XML::Node.new('dom')
          nd.content = obor
          unless settings[obor].nil?
            nd['color'] = settings[obor]['color']
            nd['mark'] = settings[obor]['mark']
          end
          doc.find('domains').first << nd
        }
      end
      xml = doc.to_s
      $stderr.puts xml
    end
    return xml
  end

  def highlight_def(id, xml, servername, timestamp)
    doc = load_xml_string(xml)
    doc.find('/entry/defs/def').each{|df|
      result = `/var/www/termcheck/run.cgi "#{df.content}" "/tezk"`
      result = result.gsub('( ', '(').gsub(/ ([,;\)\.])/, '\1')
      if result != ""
        newdf = '<def num="'+df['num']+'">'+result+'</def>'
        df.parent << XML::Document.string(newdf).root.copy(true)
        df.remove!
      end
    }
    xmlold = get(id)
    docold = load_xml_string(xmlold)
    if doc.root['timestamp'] == timestamp
      $stderr.puts 'SAVE HIGHLIGHT'
      update(id, doc.to_s)
    end
  end

  def get_alpha
    query = 'for $x in collection("tecu")/entry/terms/term return <a>{fn:substring($x/text(),1,1)};{data($x/@lang)}</a>'
    alpha = {}
    @sedna.query(query).each{|r|
      ri = r.sub('<a>','').sub('</a>','').split(';')
      alpha[ri[1]] = [] if alpha[ri[1]].nil?
      alpha[ri[1]] << ri[0].upcase_utf8 unless ['~', '(', '!', '|', ')'].include?(ri[0])
    }
    alphabet = {}
    alpha.each{|lang,ar|
      locale = lang
      locale = 'cs' if lang == 'cz'
      locale = 'en' if lang == 'gb'
      ar << 'CH' if lang == 'cz' or lang == 'sk'
      alphabet[lang] = ar.uniq.localize(locale.to_sym).sort.to_a
    }
    return alphabet
  end

  def get_term(id, dict='')
    xml = @admindict.array["tecu"+dict].get(id)
    $stderr.puts id
    $stderr.puts xml
    term = ''
    if xml != ''
      doc = load_xml_string(xml)
      term = doc.find('/entry/terms/term[@lang="cz"]').to_a.first.content.to_s unless doc.find('/entry/terms/term[@lang="cz"]').to_a.first.nil?
    end
    return term
  end

  def search(search)
    res = []
    lang = 'cz'
    lang = search['lang'] if search['lang'].to_s != ''
    search_type = 'filtr'
    search_cond = []
    if search['suggest'] and search['search'].to_s.length_utf8 > 2
      search_cond << '(some $t in $x/terms/term[@lang="'+lang+'"] satisfies fn:matches(lower-case($t), "(^| |\-|\()'+search['search'].downcase_utf8+'"))' 
      search_type = 'suggest'
    end
    if not search['suggest'] and search['search'].to_s.length_utf8 > 1 and search['search'].to_s != 'CH'
      if search['search'].include?('.') or search['search'].include?('*') or search['search'].include?('?')
        search_cond << '(some $t in $x/terms/term[@lang="'+lang+'"] satisfies fn:matches(lower-case($t), "'+search['search'].downcase_utf8+'"))' 
      else
        search_cond << '(some $t in $x/terms/term[@lang="'+lang+'"] satisfies fn:matches(lower-case($t), "(^| |\-|\()'+search['search'].downcase_utf8+'"))' 
      end
    end
    search_cond << '(some $t in $x/terms/term[@lang="'+lang+'"] satisfies $t="'+search['exact']+'")' if search['exact'].to_s != ""
    if search['search'].to_s.length_utf8 == 1 or search['search'].to_s == 'CH'
      $stderr.puts search
      if (lang == 'cz' or lang == 'sk') and search['search'].downcase_utf8 == 'c'
        search_cond << '(some $t in $x/terms/term[@lang="'+lang+'"] satisfies (starts-with(lower-case($t), "'+search['search'].downcase_utf8+'")) and not(starts-with(lower-case($t), "ch")) )' 
      else
        search_cond << '(some $t in $x/terms/term[@lang="'+lang+'"] satisfies starts-with(lower-case($t), "'+search['search'].downcase_utf8+'"))' 
      end
      search_cond << '($x/@abbrev="true")' if search['not_full'] == 'true' or search['not_full'] == '1'
      search_cond << '(not($x/@abbrev="true"))' if search['not_abbr'] == 'true' or search['not_abbr'] == '1'
      search_type = 'alpha'
    end
    search_cond << '($x/@status="'+search['status']+'")' if search['status'].to_s != ''
    search_cond << '(some $o in $x/domains/dom|$x/defs/def/@obor satisfies (' + search['domain'].map{|e| e='($o="'+e+'")'}.join(' or ') + '))' if search['domain'].is_a?(Array) and search['domain'].size > 0
    search_cond << '($x/meta/update_time >= "'+search['datum_zmeny_od']+'")' if search['datum_zmeny_od'].to_s != '' and not search['datum_zmeny_od'].to_s.include?('undef')
    search_cond << '($x/meta/update_time <= "'+search['datum_zmeny_do']+'")' if search['datum_zmeny_do'].to_s != '' and not search['datum_zmeny_do'].to_s.include?('undef')
    search_cond << '($x/meta/creation_time >= "'+search['datum_zalozeni_od']+'")' if search['datum_zalozeni_od'].to_s != '' and not search['datum_zalozeni_od'].to_s.include?('undef')
    search_cond << '($x/meta/creation_time <= "'+search['datum_zalozeni_do']+'")' if search['datum_zalozeni_do'].to_s != '' and not search['datum_zalozeni_do'].to_s.include?('undef')
    search_cond << '(some $t in $x/defs/def/type satisfies (' + search['typ'].map{|e| e='($t="'+e+'")'}.join(' or ') + '))' if search['typ'].is_a?(Array) and search['typ'].size > 0
    search_cond << '(some $t in $x/defs/def/druh satisfies (' + search['druh'].map{|e| e='($t="'+e+'")'}.join(' or ') + '))' if search['druh'].is_a?(Array) and search['druh'].size > 0
    search_cond << '(some $t in $x/defs/def satisfies contains($t/text, "'+search['definice']+'"))' if search['definice'].to_s != ''
    search_cond << '($x/meta/author="'+search['author']+'" or $x/notes/note/@author="'+search['author']+'")' if search['author'].to_s != ''
    search_cond << '($x/@abbrev="true")' if search['zkratka'].to_s == 'true'
    search_cond << '($x/@abbrev="false" or not($x/@abbrev))' if search['zkratka'].to_s == 'false'
    search_cond << '($x/@id=collection("tecuwork")/entry/@id)' if search['stav'].to_s == 'work'
    search_cond << '(not($x/@id=collection("tecuwork")/entry/@id))' if search['stav'].to_s == 'public'
    search_cond << '(' + search['ids'].map{|e| e='$x/@id="'+e.to_s+'"'}.join(' or ') + ')' if search['ids'].is_a?(Array)

    $stderr.puts search_cond
    if search_cond.size > 0
      if search_type == 'alpha' 
        query = 'for $x in collection("tecu")/entry where ' + search_cond.join(' and ') +' return <a>{$x/terms/term[@lang="'+lang+'" and starts-with(lower-case(.),"'+search['search'].downcase_utf8+'")][1]/text()}|{data($x/@id)}|{data($x/@status)}</a>'
      elsif search_type == 'suggest'
        query = 'for $x in collection("tecu")/entry where ' + search_cond.join(' and ') +' return <a>{$x/terms/term[@lang="'+lang+'" and contains(lower-case(.),"'+search['search'].downcase_utf8+'")][1]/text()}|{data($x/@id)}|{data($x/@status)}</a>'
      else
        query = 'for $x in collection("tecu")/entry where ' + search_cond.join(' and ') +' return <a>{$x/terms/term[@lang="'+lang+'"][1]/text()}|{data($x/@id)}|{data($x/@status)}</a>'
      end
      $stderr.puts query
      @sedna.query(query).each{|e|
        ear = e.gsub(/<\/?a>/,'').split('|')
        res << {'id'=>ear[1], 'head'=>ear[0], 'status'=>ear[2], 'highlight'=>ear[0].gsub(search['search'].to_s.force_encoding('UTF-8'), '<span class="search-highlight">'+search['search'].to_s+'</span>')}
      }
      re_search = /#{search['search']}/u
      #res = res.sort{|a, b|
      #  sort1 = (((a['head']=~re_search).nil?)?1000:(a['head']=~re_search)) <=> (((b['head']=~re_search).nil?)?1000:(b['head']=~re_search))
      #  (sort1 == 0)? a['head']<=>b['head'] : sort1
      #}
      locale = lang
      locale = 'cs' if lang == 'cz'
      locale = 'en' if lang == 'gb'
      collator = TwitterCldr::Collation::Collator.new(locale.to_sym)
      res = res.sort{|a, b| collator.compare(a['head'], b['head'])}
    end
    return res
  end

  def get_deleted
    res = []
    query = 'for $x in collection("tecudel")/entry return <a>{$x/terms/term[1]/text()}|{data($x/@id)}|{data($x/@status)}</a>'
    @sedna.query(query).each{|e|
      ear = e.gsub(/<\/?a>/,'').split('|')
      res << {'id'=>ear[1], 'head'=>ear[0], 'status'=>0}
    }
    return res.sort{|x,y| x['head']<=>y['head']}
  end

  def get_hypers
    res = []
    query = 'for $x in collection("tecu")/entry/hyper return data($x/@id)'
    @sedna.query(query).each{|e|
      res << e
    }
    return res.uniq
  end

  def get_fulltree(hypers)
  end

  def get_subtree(id, hypers)
    res = []
    query = 'for $x in collection("tecu")/entry[hyper/@id="'+id+'"] return <a>{data($x/@id)};{$x/terms/term[1]/text()}</a>'
    #query = '[entry[hyper/@id="'+id+'"]]'
    #xquery_to_hash(query).each{|id, xml|
    @sedna.query(query).each{|r|
      #doc = load_xml_string(xml.to_s)
      id, head, expend = r.sub('<a>','').sub('</a>','').split(';')
      new = {'id'=>id, 'head'=>head, 'expandable'=>0}
      new['expandable'] = 1 if @hypers.include?(id)
      #new['head'] = doc.find('terms/term').first.content.to_s unless doc.find('terms/term').first.nil?
      #new['expandable'] = xquery_to_array('[entry[hyper/@id="'+id+'"]]').length 
      res << new
    }
    return res.sort{|x,y| x['head'].to_s.downcase<=>y['head'].to_s.downcase}
  end

  def export_tree_struct(hypers, up, level)
    res = ''
    if up == '0'
      get_top.each{|h|
        res += '* ' + h['id'] + ': ' + h['head'] + "\n"
        res += export_tree_struct(hypers, h['id'], level+1) if h['expandable'] > 0
      }
    else
      get_subtree(up, hypers).each{|h|
        res += ' '*level + '* ' + h['id'] + ': ' + h['head'] + "\n"
        res += export_tree_struct(hypers, h['id'], level+1) if h['expandable'] > 0 and level < 15
      }
    end
    return res
  end

  def get_export_subtree(id, hypers, to_export)
    get_subtree(id, hypers).each{|h|
      unless to_export.include?(h['id'])
        to_export << h['id']
        if h['expandable'] > 0
          to_export = get_export_subtree(h['id'], hypers, to_export)
        end
      end
    }
    return to_export
  end

  def export_subtree(id, hypers)
    to_export = [id]
    to_export = get_export_subtree(id, hypers, to_export)
    to_csv = []
    rows = []
    to_export.each{|eid|
      xml = get(eid)
      if xml != ''
        doc = load_xml_string(xml)
        rows = [ [eid,doc.root['abbr'],'','','','','','','','','','',''] ]
        ri = 0
        doc.find('terms/term').each{|t|
          rows[ri] = ['','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][2] = t.content
          rows[ri][3] = t['lang']
          ri += 1
        }
        ri = 0
        doc.find('defs/def').each{|t|
          rows[ri] = ['','','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][4] = t.find('text').first.content.to_s unless t.find('text').first.nil?
          rows[ri][5] = t['lang'].to_s
          rows[ri][6] = t['obor'].to_s
          rows[ri][7] = t.find('zdroj').first.content.to_s unless t.find('zdroj').first.nil?
          ri += 1
        }
        ri = 0
        #doc.find('domains/dom').each{|t|
        #  rows[ri] = ['','','','','','','','','','','',''] if rows[ri].nil?
        #  rows[ri][5] = t.content
        #  ri += 1
        #}
        #ri = 0
        #doc.find('refs/ref').each{|t|
        #  rows[ri] = ['','','','','','','','','','','','',''] if rows[ri].nil?
        #  rows[ri][7] = t.content
        #  ri += 1
        #}
        ri = 0
        doc.find('hyper').each{|t|
          rows[ri] = ['','','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][8] = t['id'].to_s #get_term(t['id'])
          ri += 1
        }
        ri = 0
        doc.find('synonyms/synonym').each{|t|
          rows[ri] = ['','','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][9] = t.content
          ri += 1
        }
        ri = 0
        doc.find('antonyms/antonym').each{|t|
          rows[ri] = ['','','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][10] = t.content
          ri += 1
        }
      end
      to_csv += rows
    }
    return to_csv
  end

  def export_csv
    rows = []
    to_csv = []
    query = 'for $x in collection("tecu")/entry return $x'
    @sedna.query(query).each{|xml|
      if xml != ''
        doc = load_xml_string(xml)
        eid = doc.root['id']
        rows = [ [eid,doc.root['abbrev'],'','','','','','','','','','',''] ]
        ri = 0
        doc.find('terms/term').each{|t|
          rows[ri] = ['','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][2] = t.content
          rows[ri][3] = t['lang']
          ri += 1
        }
        ri = 0
        doc.find('defs/def').each{|t|
          rows[ri] = ['','','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][4] = t.find('text').first.content.to_s unless t.find('text').first.nil?
          rows[ri][5] = t['lang'].to_s
          rows[ri][6] = t['obor'].to_s
          rows[ri][7] = t.find('zdroj').first.content.to_s unless t.find('zdroj').first.nil?
          ri += 1
        }
        ri = 0
        ri = 0
        doc.find('hyper').each{|t|
          rows[ri] = ['','','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][8] = t['id'].to_s #get_term(t['id'])
          ri += 1
        }
        ri = 0
        doc.find('synonyms/synonym').each{|t|
          rows[ri] = ['','','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][9] = t.content
          ri += 1
        }
        ri = 0
        doc.find('antonyms/antonym').each{|t|
          rows[ri] = ['','','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][10] = t.content
          ri += 1
        }
      end
      to_csv += rows
    }
    return to_csv
  end

  def get_top
    res = []
    query = '[entry[not(hyper)]]'
    xquery_to_hash(query).each{|id, xml|
      doc = load_xml_string(xml.to_s)
      new = {'id'=>id}
      next if doc.find('terms/term').first.nil?
      new['head'] = doc.find('terms/term').first.content.to_s unless doc.find('terms/term').first.nil?
      new['expandable'] = xquery_to_array('[entry[hyper/@id="'+id+'"]]').length 
      res << new 
    }
    return res.sort{|x,y| x['head'].downcase<=>y['head'].downcase}
  end
  def get_top2
    res = []
    query = '[entry[not(hyper)]]'
    query = '[entry[hyper/@id="0"]]'
    xquery_to_hash(query).each{|id, xml|
      doc = load_xml_string(xml.to_s)
      new = {'id'=>id}
      next if doc.find('terms/term').first.nil?
      new['head'] = doc.find('terms/term').first.content.to_s unless doc.find('terms/term').first.nil?
      new['expandable'] = get_subtree2(id)
      res << new 
    }
    collator = TwitterCldr::Collation::Collator.new(:cs)
    return res.sort{|a, b| collator.compare(a['head'].downcase, b['head'].downcase)}
  end
  def get_subtree2(id)
    res = []
    query = 'for $x in collection("tecu")/entry[hyper/@id="'+id+'"] return <a>{data($x/@id)};{$x/terms/term[1]/text()}</a>'
    @sedna.query(query).each{|r|
      #doc = load_xml_string(xml.to_s)
      id, head, expend = r.sub('<a>','').sub('</a>','').split(';')
      new = {'id'=>id, 'head'=>head}
      new['expandable'] = get_subtree2(id) if @hypers.include?(id)
      res << new
    }
    collator = TwitterCldr::Collation::Collator.new(:cs)
    return res.sort{|a, b| collator.compare(a['head'].downcase, b['head'].downcase)}
  end

  def get_path(id)
    path = []
    xml = @admindict.array["tecu"].get(id)
    if xml != ''
      path = get_up(id, [[]], 0, 0)
    end
    return path.sort{|a,b| b.length<=>a.length}
  end
  def get_up(id, path_array, path_num, max_ar)
    $stderr.puts 'GET UP'+id
    xml = @admindict.array["tecu"].get(id)
    return path_array if xml == ''
    $stderr.puts 'GET UP'+id
    doc = load_xml_string(xml)
    paths = {}
    offset = 0
    doc.root.find('//hyper').each{|h|
      next if paths[h['id']] != nil
      if offset == 0
        paths[h['id']] = path_num
      else
        max_ar += 1
        path_array[max_ar] = path_array[path_num].clone
        paths[h['id']] = max_ar
      end
      offset += 1
    }
    if doc.root.find('//hyper').length > 0
      doc.root.find('//hyper').each{|h|
        act_path = paths[h['id']]
        next if path_array[act_path].include?(h['id'])
        path_array[act_path] << h['id']
        path_array = get_up(h['id'].to_s, path_array, act_path, max_ar)
      }
    else
      path_array[path_num] << '0'
    end
    return path_array
  end

  def get_perm(user, dict=@dictcode)
    return 'w'
    xml = @admindict.get('user'+user)
    doc = load_xml_string(xml.to_s)
    perm_ar = doc.find("//service[@code='tecu']/dict[@code='"+dict+"']").to_a
    if perm_ar.size > 0
      perm = perm_ar.first['perm'].to_s
      return perm
    else
      return nil
    end
  end

  def get_stats
    stat = {}
    #pocet hesel
    stat['entries'] = @sedna.query('count(collection("tecu")/entry)')[0]
    #pocet hesel podle statusu
    #stat['status_pouz'] = @sedna.query('count(collection("tecu")/entry[@status=2])')[0]
    #stat['status_auto'] = @sedna.query('count(collection("tecu")/entry[@status=3 or @status=5])')[0]
    #stat['status_term'] = @sedna.query('count(collection("tecu")/entry[@status=1])')[0]
    #pocet prekladu podle jazyku
    stat['lang'] = []
    @sedna.query('for $x in distinct-values(data(collection("tecu")/entry/terms/term/@lang)) return <a>{$x};{count(collection("tecu")/entry/terms/term[@lang=$x])}</a>').each{|x|
      langstat = x.sub('<a>','').sub('</a>','').split(';')
      stat['lang'] << {'lang'=>langstat[0], 'count'=>langstat[1]}
    }
    #pocet definic
    stat['defs'] = @sedna.query('count(collection("tecu")/entry/defs/def)')[0]
    #pocet polozek obor
    stat['doms'] = []
    @sedna.query('for $x in distinct-values(collection("tecu")/entry/defs/def/@obor) return <a>{$x};{count(collection("tecu")/entry[defs/def/@obor=$x])}</a>').each{|x|
      langstat = x.sub('<a>','').sub('</a>','').split(';')
      stat['doms'] << {'dom'=>langstat[0], 'count'=>langstat[1]}
    }
    return stat
  end

  def get_history
    query = '<notes>{subsequence(for $x in collection("tecu")/entry order by $x/notes/note[last()]/@time descending return <last id="{data($x/@id)}" term="{$x/terms/term[1]/text()}">{$x/notes/note[last()]}</last>,0,30)}</notes>'
    return @sedna.query(query)[0]
  end

  def get_all
    query = 'for $x in collection("tecu")/entry return $x'
    res = '<tezk>'
    @sedna.query(query).each{|xml|
      res += xml.to_s.sub('<?xml version="1.0" standalone="yes"?>','')
      res += "\n"
    }
    res += '</tezk>'
  end

  def get_trans(term, lang)
    file = '/var/lib/deb-server/corpora/data/results/trans_cands_'+lang+'.txt'
    output = []
    File.open(file, 'r').each{|line|
      cands = line.split("\t")
      next unless term == cands[0]
      cands.shift
      cands.each{|cand|
        tran, score = cand[1..-2].split(",")
        output << {'trans' => tran, 'score' => score}
      }
    }
    output = output.sort{|x,y| y['score'] <=> x['score']}.first(10)
    return {'trans_cands' => output}
  end

  def get_hyper_cand(term)
    file = '/var/lib/deb-server/corpora/data/results/hyper_cands.txt'
    output = []
    output_id = []
    File.open(file, 'r').each{|line|
      cands = line.split("|||")
      next unless term == cands[0]
      next if term == cands[1]
      data = {'hyper' => cands[1], 'score' => cands[2]}
      query = 'for $x in collection("tecu")/entry[terms/term="'+cands[1]+'"] return data($x/@id)'
      @sedna.query(query).each{|si|
        data['entry-id'] = si
      }
      #if data['entry-id'].nil?
      #  output << data
      #else
        output_id << data
      #end
    }
    return {'hyper_cands' => output_id + output}
  end

  def get_term_cand
    file = '/var/lib/deb-server/corpora/data/results/term_cands.txt'
    output = []
    File.open(file, 'r').each{|line|
      cands = line.strip.split("\t")
      if xquery_to_list('[entry[terms/term="'+cands[0].to_s+'"]]').length == 0
        output << {'term' => cands[0], 'score' => cands[1]}
      end
    }
    return {'term_cands' => output}
  end

  def update_term_cand
    output = get_term_cand
    @sedna.query('update delete collection("tecu")/entry[count(hyper)=1 and hyper/@id="20000"]')
    output['term_cands'].each{|tc|
      newid = @admindict.get_next_id('tecu').to_s
      xml = "<entry id='#{newid}'><terms><term lang='cz'>#{tc['term'].to_s}</term></terms><hyper id='20000'/></entry>"
      update(newid, xml)
    }
    @tree = get_top2
    return output
  end

  def get_concordance(term)
    query = []
    results = []
    term.split(' ').each{|w|
      query << '[word="'+w+'"]'
    }
    settings = @admindict.array['settings'].get_settings
    num = '10'
    num = settings['dict']['examples'] if settings['dict']['examples'].to_s != ''
    command = "corpquery /corpora/registry/tecu '<s/> containing "+query.join(' ')+"' -h "+num+" -k ',' -s g -c 0"
    $stderr.puts command
    result = `#{command}`
    result.split("\n").each{|l|
      results << l
    }
    return results
  end

  def get_settings(show_pass=false)
    settings = {}
    settings['meta'] = {'url'=>'', 'name'=>'', 'short_name'=>'','organization'=>'', 'description'=>'', 'contact_person'=>'', 'contact_email'=>'', 'contact_phone'=>'', 'komise'=>'', 'poverena_organizace'=>'', 'infocz'=>'','infoen'=>''}
    settings['tech'] = {'show_tree'=>'true', 'backup_path'=>'', 'backup_time'=>'', 'smtp_server'=>'', 'smtp_port'=>'', 'smtp_user'=>'', 'smtp_pass'=>'', 'log_path'=>'', 'backup_url'=>'', 'backup_user'=>'', 'backup_pass'=>''}
    #settings['design'] = {'color_text'=>'', 'color_segment'=>'', 'color_back'=>'', 'lista_text'=>'', 'lista_back'=>'', 'lista_high'=>''}
    settings['dict'] = {'examples'=>'', 'domains'=>[], 'req_trans'=>[], 'types'=>[], 'example_length'=>'', 'druhy'=>[], 'langs'=>[], 'simpleprocess'=>'false', 'skos'=>[], 'dictionaries'=>[], 'alphabet'=>@alphabet, 'notif_days'=>14}
    settings['editors'] = get_editors
    settings['roles'] = [
      {'role'=>'it_admin','label'=>'IT administrátor DS'},
      {'role'=>'operator','label'=>'Operátor provozu DS'},
      {'role'=>'hl_redaktor','label'=>'Hlavní redaktor'},
      {'role'=>'asist_hl','label'=>'Asistent hlavního redaktora'},
      {'role'=>'obor_redaktor','label'=>'Redaktor oboru'},
      {'role'=>'obor_revizor','label'=>'Recenzent oboru'},
      {'role'=>'prekladatel','label'=>'Překladatel jazyka'},
      {'role'=>'recenzent_preklad','label'=>'Recenzent překladů jazyka'},
      {'role'=>'jazyk_specialista','label'=>'Jazykový specialista'},
      {'role'=>'jazyk_korektor','label'=>'Jazykový korektor'},
      {'role'=>'revizor','label'=>'Revizor'},
      {'role'=>'tajemnik','label'=>'Tajemník term. komise'},
      {'role'=>'blok','label'=>'zablokovaný uživatel'},
    ]
    settings['dictionaries'] = []
    @list_thes.each{|th|
      settings['dictionaries'] << {'url'=>th["url"], 'label'=>th["name"]}
    }
    xml = get('meta')
    doc = load_xml_string(xml)
    unless doc.nil?
      settings['meta']['url'] = doc.find('/meta/url').first.content unless doc.find('/meta/url').first.nil?
      settings['meta']['name'] = doc.find('/meta/name').first.content unless doc.find('/meta/name').first.nil?
      settings['meta']['short_name'] = doc.find('/meta/short_name').first.content unless doc.find('/meta/short_name').first.nil?
      settings['meta']['organization'] = doc.find('/meta/organization').first.content unless doc.find('/meta/organization').first.nil?
      settings['meta']['descriptioncs'] = doc.find('/meta/descriptioncs').first.content unless doc.find('/meta/descriptioncs').first.nil?
      settings['meta']['descriptionen'] = doc.find('/meta/descriptionen').first.content unless doc.find('/meta/descriptionen').first.nil?
      settings['meta']['contact_person'] = doc.find('/meta/contact_person').first.content unless doc.find('/meta/contact_person').first.nil?
      settings['meta']['contact_email'] = doc.find('/meta/contact_email').first.content unless doc.find('/meta/contact_email').first.nil?
      settings['meta']['contact_phone'] = doc.find('/meta/contact_phone').first.content unless doc.find('/meta/contact_phone').first.nil?
      settings['meta']['komise'] = doc.find('/meta/komise').first.content unless doc.find('/meta/komise').first.nil?
      settings['meta']['poverena_organizace'] = doc.find('/meta/poverena_organizace').first.content unless doc.find('/meta/poverena_organizace').first.nil?
      settings['meta']['infocs'] = doc.find('/meta/infocs').first.content unless doc.find('/meta/infocs').first.nil?
      settings['meta']['infoen'] = doc.find('/meta/infoen').first.content unless doc.find('/meta/infoen').first.nil?
    end
    xml = get('tech')
    doc = load_xml_string(xml)
    unless doc.nil?
      settings['tech']['show_tree'] = doc.find('/tech/show_tree').first.content unless doc.find('/tech/show_tree').first.nil?
      settings['tech']['log_path'] = doc.find('/tech/log_path').first.content unless doc.find('/tech/log_path').first.nil?
      settings['tech']['backup_path'] = doc.find('/tech/backup_path').first.content unless doc.find('/tech/backup_path').first.nil?
      settings['tech']['backup_user'] = doc.find('/tech/backup_user').first.content unless doc.find('/tech/backup_user').first.nil?
      #settings['tech']['backup_pass'] = doc.find('/tech/backup_pass').first.content unless doc.find('/tech/backup_pass').first.nil?
      settings['tech']['backup_url'] = doc.find('/tech/backup_url').first.content unless doc.find('/tech/backup_url').first.nil?
      settings['tech']['backup_time'] = doc.find('/tech/backup_time').first.content unless doc.find('/tech/backup_time').first.nil?
      settings['tech']['smtp_server'] = doc.find('/tech/smtp_server').first.content unless doc.find('/tech/smtp_server').first.nil?
      settings['tech']['smtp_port'] = doc.find('/tech/smtp_port').first.content unless doc.find('/tech/smtp_port').first.nil?
      settings['tech']['smtp_user'] = doc.find('/tech/smtp_user').first.content unless doc.find('/tech/smtp_user').first.nil?
      settings['tech']['smtp_pass'] = doc.find('/tech/smtp_pass').first.content if show_pass and doc.find('/tech/smtp_pass').first != nil
    end
    #xml = get('design')
    #doc = load_xml_string(xml)
    #unless doc.nil?
    #  settings['design']['color_text'] = doc.find('/design/color_text').first.content unless doc.find('/design/color_text').first.nil?
    #  settings['design']['color_segment'] = doc.find('/design/color_segment').first.content unless doc.find('/design/color_segment').first.nil?
    #  settings['design']['color_back'] = doc.find('/design/color_back').first.content unless doc.find('/design/color_back').first.nil?
    #  settings['design']['lista_text'] = doc.find('/design/lista_text').first.content unless doc.find('/design/lista_text').first.nil?
    #  settings['design']['lista_back'] = doc.find('/design/lista_back').first.content unless doc.find('/design/lista_back').first.nil?
    #  settings['design']['lista_high'] = doc.find('/design/lista_high').first.content unless doc.find('/design/lista_high').first.nil?
    #end
    xml = get('dict')
    doc = load_xml_string(xml)
    unless doc.nil?
      settings['dict']['notif_days'] = doc.find('/dict/notif_days').first.content unless doc.find('/dict/notif_days').first.nil?
      settings['dict']['examples'] = doc.find('/dict/examples').first.content unless doc.find('/dict/examples').first.nil?
      settings['dict']['example_length'] = doc.find('/dict/example_length').first.content unless doc.find('/dict/example_length').first.nil?
      settings['dict']['simpleprocess'] = 'true' if doc.find('/dict/simpleprocess').first != nil and doc.find('/dict/simpleprocess').first.content.to_s == 'true'
      doc.find('/dict/types/type').each{|d| settings['dict']['types'] << d.content}
      doc.find('/dict/druhy/druh').each{|d| settings['dict']['druhy'] << d.content}
      doc.find('/dict/skos/url').each{|d| settings['dict']['skos'] << d.content}
      doc.find('/dict/dictionaries/url').each{|d| settings['dict']['dictionaries'] << d.content}
      settings['dict']['req_trans'] = ['cz']
      doc.find('/dict/req_trans/req_tran').each{|d| settings['dict']['req_trans'] << d.content if d.content != 'cz'}
      doc.find('/dict/domains/domain').each{|d| 
        settings['dict']['domains'] << {'domain'=>d.content.to_s,'color'=>d['color'],'mark'=>d['mark']} 
        settings['roles'] << {'role'=>'obor_redaktor_'+d.content, 'label'=>'Redaktor oboru '+d.content}
        settings['roles'] << {'role'=>'obor_revizor_'+d.content, 'label'=>'Recenzent oboru '+d.content}
      }
      settings['dict']['langs'] = ['cz']
      doc.find('/dict/langs/lang').each{|d| 
        settings['dict']['langs'] << d.content if d.content != 'cz'
        settings['roles'] << {'role'=>'prekladatel_'+d.content, 'label'=>'Překladatel jazyka '+d.content}
        settings['roles'] << {'role'=>'recenzent_preklad_'+d.content, 'label'=>'Recenzent překladů jazyka '+d.content}
      }
    end
    return settings
  end

  def save_settings(data)
    xml = '<meta>'
    unless data['meta'].nil?
      xml += '<infocs>'+CGI.escapeHTML(data['meta']['infocs'].to_s)+'</infocs>'
      xml += '<infoen>'+CGI.escapeHTML(data['meta']['infoen'].to_s)+'</infoen>'
      xml += '<url>'+CGI.escapeHTML(data['meta']['url'].to_s)+'</url>'
      xml += '<name>'+CGI.escapeHTML(data['meta']['name'].to_s)+'</name>'
      xml += '<short_name>'+CGI.escapeHTML(data['meta']['short_name'].to_s)+'</short_name>'
      xml += '<organization>'+CGI.escapeHTML(data['meta']['organization'].to_s)+'</organization>'
      xml += '<descriptioncs>'+CGI.escapeHTML(data['meta']['descriptioncs'].to_s)+'</descriptioncs>'
      xml += '<descriptionen>'+CGI.escapeHTML(data['meta']['descriptionen'].to_s)+'</descriptionen>'
      xml += '<contact_person>'+CGI.escapeHTML(data['meta']['contact_person'].to_s)+'</contact_person>'
      xml += '<contact_phone>'+CGI.escapeHTML(data['meta']['contact_phone'].to_s)+'</contact_phone>'
      xml += '<contact_email>'+CGI.escapeHTML(data['meta']['contact_email'].to_s)+'</contact_email>'
      xml += '<poverena_organizace>'+CGI.escapeHTML(data['meta']['poverena_organizace'].to_s)+'</poverena_organizace>'
      xml += '<komise>'+CGI.escapeHTML(data['meta']['komise'].to_s)+'</komise>'
    end
    xml += '</meta>'
    update('meta', xml)

    backup_pass = ''
    if data['tech']['backup_pass'].to_s != ''
      backup_pass = CGI.escapeHTML(data['tech']['backup_pass'].to_s.gsub(/[^0-9A-Z\-_\.\/]/i,''))
    else
      xml = get('tech')
      if xml != ''
        doc = load_xml_string(xml)
        backup_pass = doc.find('/tech/backup_pass').first.content unless doc.find('/tech/backup_pass').first.nil?
      end
    end
    smtp_pass = ''
    if data['tech']['smtp_pass'].to_s != ''
      smtp_pass = CGI.escapeHTML(data['tech']['smtp_pass'].to_s.gsub(/[^0-9A-Z\-_\.\/]/i,''))
    else
      xml = get('tech')
      if xml != ''
        doc = load_xml_string(xml)
        smtp_pass = doc.find('/tech/smtp_pass').first.content unless doc.find('/tech/smtp_pass').first.nil?
      end
    end
    xml = '<tech>'
    unless data['tech'].nil?
      xml += '<show_tree>'+CGI.escapeHTML(data['tech']['show_tree'].to_s)+'</show_tree>'
      xml += '<smtp_server>'+CGI.escapeHTML(data['tech']['smtp_server'].to_s)+'</smtp_server>'
      xml += '<smtp_user>'+CGI.escapeHTML(data['tech']['smtp_user'].to_s)+'</smtp_user>'
      xml += '<smtp_port>'+CGI.escapeHTML(data['tech']['smtp_port'].to_s)+'</smtp_port>'
      xml += '<smtp_pass>'+smtp_pass+'</smtp_pass>'
      xml += '<backup_path>'+CGI.escapeHTML(data['tech']['backup_path'].to_s.gsub(/[^0-9A-Z\-_\.\/]/i,''))+'</backup_path>'
      xml += '<backup_url>'+CGI.escapeHTML(data['tech']['backup_url'].to_s.gsub(/[^0-9A-Z\-_\.\/]/i,''))+'</backup_url>'
      xml += '<backup_pass>'+backup_pass+'</backup_pass>'
      xml += '<backup_user>'+CGI.escapeHTML(data['tech']['backup_user'].to_s.gsub(/[^0-9A-Z\-_\.\/]/i,''))+'</backup_user>'
      xml += '<log_path>'+CGI.escapeHTML(data['tech']['log_path'].to_s)+'</log_path>'
      xml += '<backup_time>'+CGI.escapeHTML(data['tech']['backup_time'].to_s)+'</backup_time>'
    end
    xml += '</tech>'
    update('tech', xml)

    xml = '<design>'
    unless data['design'].nil?
      xml += '<color_text>'+CGI.escapeHTML(data['design']['color_text'].to_s)+'</color_text>'
      xml += '<color_back>'+CGI.escapeHTML(data['design']['color_back'].to_s)+'</color_back>'
      xml += '<color_segment>'+CGI.escapeHTML(data['design']['color_segment'].to_s)+'</color_segment>'
      xml += '<lista_text>'+CGI.escapeHTML(data['design']['lista_text'].to_s)+'</lista_text>'
      xml += '<lista_back>'+CGI.escapeHTML(data['design']['lista_back'].to_s)+'</lista_back>'
      xml += '<lista_high>'+CGI.escapeHTML(data['design']['lista_high'].to_s)+'</lista_high>'
    end
    xml += '</design>'
    update('design', xml)

    xml = '<dict>'
    unless data['dict'].nil?
      xml += '<notif_days>'+CGI.escapeHTML(data['dict']['notif_days'].to_s)+'</notif_days>'
      xml += '<examples>'+CGI.escapeHTML(data['dict']['examples'].to_s)+'</examples>'
      xml += '<simpleprocess>'+CGI.escapeHTML(data['dict']['simpleprocess'].to_s)+'</simpleprocess>'
      xml += '<example_length>'+CGI.escapeHTML(data['dict']['example_length'].to_s)+'</example_length>'
      xml += '<domains>'
      data['dict']['domains'].each{|d| xml += '<domain color="'+d['color'].to_s+'" mark="'+d['mark'].to_s+'">'+CGI.escapeHTML(d['domain'].to_s)+'</domain>'}
      xml += '</domains>'
      xml += '<druhy>'
      data['dict']['druhy'].each{|d| xml += '<druh>'+CGI.escapeHTML(d)+'</druh>'}
      xml += '</druhy>'
      xml += '<types>'
      data['dict']['types'].each{|d| xml += '<type>'+CGI.escapeHTML(d)+'</type>'}
      xml += '</types>'
      xml += '<langs>'
      data['dict']['langs'].each{|d| xml += '<lang>'+CGI.escapeHTML(d)+'</lang>'}
      xml += '</langs>'
      xml += '<skos>'
      data['dict']['skos'].each{|d| xml += '<url>'+CGI.escapeHTML(d)+'</url>'}
      xml += '</skos>'
      xml += '<dictionaries>'
      data['dict']['dictionaries'].each{|d| xml += '<url>'+CGI.escapeHTML(d)+'</url>'}
      xml += '</dictionaries>'
      xml += '<req_trans>'
      data['dict']['req_trans'].each{|d| xml += '<req_tran>'+CGI.escapeHTML(d)+'</req_tran>'}
      xml += '</req_trans>'
    end
    xml += '</dict>'
    update('dict', xml)

    #update index.html
    indexfile = '/var/lib/deb-server/files/tezaurus/index.html'
    indexdata = File.read(indexfile)
    indexdata.sub!(/<title>[^<]*<\/title>/, '<title>' + CGI.escapeHTML(data['meta']['short_name'].to_s) + '</title>')
    file = File.open(indexfile, 'w')
    file.write(indexdata)
    file.close
  end

  def save_design(data)
    old = get('design')
    oldlogo = ''
    if old != ''
      olddoc = load_xml_string(old)
      if olddoc.find('/design/logo').first != nil
        oldlogo = olddoc.find('/design/logo').first.content.to_s
      end
    end
    xml = '<design>'
    unless data['colors'].nil?
      xml += '<mainColor>'+CGI.escapeHTML(data['colors']['mainColor'].to_s)+'</mainColor>'
      xml += '<mainBckg>'+CGI.escapeHTML(data['colors']['mainBckg'].to_s)+'</mainBckg>'
      xml += '<segmentBckg>'+CGI.escapeHTML(data['colors']['segmentBckg'].to_s)+'</segmentBckg>'
      xml += '<menuColor>'+CGI.escapeHTML(data['colors']['menuColor'].to_s)+'</menuColor>'
      xml += '<menuBckg>'+CGI.escapeHTML(data['colors']['menuBckg'].to_s)+'</menuBckg>'
      xml += '<blueTreeNode>'+CGI.escapeHTML(data['colors']['blueTreeNode'].to_s)+'</blueTreeNode>'
      xml += '<visColor>'+CGI.escapeHTML(data['colors']['visColor'].to_s)+'</visColor>'
      xml += '<positive>'+CGI.escapeHTML(data['colors']['positive'].to_s)+'</positive>'
      xml += '<focusColor>'+CGI.escapeHTML(data['colors']['focusColor'].to_s)+'</focusColor>'
      xml += '<error>'+CGI.escapeHTML(data['colors']['error'].to_s)+'</error>'
    end
    if data['file'].to_s != ''
      xml += '<logo>'+data['file'].sub(/data:image\/[a-z]*;base64/,'')+'</logo>'
    else
      if oldlogo != ''
        xml += '<logo>'+oldlogo+'</logo>'
      end
    end
    xml += '</design>'
    update('design', xml)
  end

  def get_users(userdict, username='', all_users=false)
    query = '/user[login!="nagios"]'
    query = '/user[login="'+username+'"]' if username != ''
    output = []
    begin
    userdict.xquery_to_array(query).each{|xml|
      doc = load_xml_string(xml)
      userhash = {'login'=>'', 'name'=>'', 'email'=>'', 'phone'=>'', 'organization'=>'', 'roles'=>[]}
      userhash['login'] = doc.find('/user/login').first.content.to_s
      next if userhash['login'] == 'read' and not all_users
      userhash['name'] = userhash['login']
      userhash['name'] = doc.find('/user/name').first.content.to_s if doc.find('/user/name').first != nil and doc.find('/user/name').first.content.to_s != ''
      userhash['prijmeni'] = userhash['name'].split(' ').last()
      userhash['email'] = doc.find('/user/email').first.content.to_s unless doc.find('/user/email').first.nil?
      userhash['phone'] = doc.find('/user/phone').first.content.to_s unless doc.find('/user/phone').first.nil?
      userhash['organization'] = doc.find('/user/org').first.content.to_s unless doc.find('/user/org').first.nil?
      doc.find('/user/roles/role').each{|r| userhash['roles'] << r.content}
      output << userhash
    }
    rescue
    end
    return output.sort_by{|a| a['prijmeni'].to_s}
  end

  def prepare_notif_exlink(entry_id, term, exdict, exlink, users)
    userlist = users.xquery_to_list('[user[roles/role="asist_hl"]]')
    time = Time.now.strftime('%Y-%m-%d %H:%M:%S')
    userlist.uniq.each{|user|
      comment = 'aktualizace odkazu v termínu '+term+': slovnik '+exdict+'; Link='+exlink
      comment2 = 'aktualizace odkazu v termínu '+term+': slovnik '+exdict+', '+exlink
      query = "INSERT INTO notif (message, term_id, term, time_received, notif_from, notif_for, role, read) values (?, ?, ?, ?, ?, ?, ?, 0)"
      @notif_db.execute(query, [comment.gsub("&", "&amp;").to_s.encode('utf-8'), entry_id, term.to_s.encode('utf-8'), time.to_s.encode('utf-8'), 'registr', user.to_s.encode('utf-8'), 'asist_hl'])
      send_mail_role('Upozornění z tezauru', comment2, 'asist_hl')
    }
  end

  def list_notifications(user='', term='', role='', time_from='', time_to='', from='', read='')
    res = []
    conds = []
    conds << '(read=0)' if read == '' or read == '0'
    conds << '(read=1)' if read == '1'
    conds << '(notif_for="'+user+'")' if user != ''
    conds << '(term like "%'+term+'%")' if term != ''
    conds << '(time_received >= "'+time_from+' 00:00:00")' if time_from != ''
    conds << '(time_received <= "'+time_to+' 23:59:59")' if time_to != ''
    conds << '(role="'+role+'")' if role != ''
    if from != ''
      if from == 'server'
        conds << '(notif_from="registr")'
      else
        conds << '(not(notif_from="registr"))'
      end
    end
    query = "SELECT * FROM notif WHERE "+conds.join(' and ')
    $stderr.puts query
    @notif_db.execute(query).each{|row|
      $stderr.puts row
      hash = {
        'nid'=>row['nid'],
        'message'=>row['message'],
        'for'=>row['notif_for'],
        'from'=>row['notif_from'],
        'time_received'=>row['time_received'],
        'term_id'=>row['term_id'],
        'term'=>row['term'],
        'role'=>role,
        'read'=>'false'
      } 
      hash['read'] = 'true' if row['read'].to_s == '1'
      res << hash
    }
    return res
  end

  def list_expired_notifications(servername)
    settings = get_settings
    days = 14
    days = settings['dict']['notif_days'].to_i if settings['dict'] != nil and settings['dict']['notif_days'] != nil
    old_date = (Date.today - days).strftime('%Y-%m-%d')
    notifs = list_notifications('', '', '', '', old_date,'') 
    for_user = {}
    notifs.each{|notif|
      if for_user[notif['for']].nil?
        for_user[notif['for']] = 'Upozornění z tezauru starší než '+days.to_s + "dnů:\n\n"+servername.gsub('http:','https:')+'/tezaurus/notifications'+"\n\n"
      end
      for_user[notif['for']] += 'termín: '+notif['term'].to_s + ', zpráva: ' + notif['message'].to_s + ' (' + notif['from'].to_s + ', ' + notif['time_received'].to_s + ")\n\n"
    }
    for_user.each{|user,message|
      send_mail_user('Upozornění z tezauru', message, user)
    }
    return notifs
  end

  def clear_notif(term_id)
    query = "UPDATE notif SET read=1 WHERE term_id=?"
    @notif_db.execute(query, [term_id])
  end

  def read_notif(nid)
    query = "UPDATE notif SET read=1 WHERE nid=?"
    @notif_db.execute(query, [nid])
  end

  def prepare_notif(entry_id, term, role, comment, user_from, users)
    clear_notif(entry_id)
    roles = role.to_s.split(',')
    userlist = []
    roles.each{|r|
      userlist += users.xquery_to_list('[user[roles/role="'+r+'"]]')
    }
    $stderr.puts userlist
    time = Time.now.strftime('%Y-%m-%d %H:%M:%S')
    userlist.uniq.each{|user|
      query = "INSERT INTO notif (message, term_id, term, time_received, notif_from, notif_for, role, read) values (?, ?, ?, ?, ?, ?, ?, 0)"
      @notif_db.execute(query, [comment.gsub("&", "&amp;").encode('utf-8'), entry_id, term.to_s.encode('utf-8'), time.to_s.encode('utf-8'), user_from.to_s.encode('utf-8'), user.to_s.encode('utf-8'), role.to_s.encode('utf-8')])
      send_mail_user('Upozornění z tezauru', 'termin '+term+"\n\n"+comment.encode('utf-8'), user)
    }
  end

  def get_editors
    editors = []
    edits = []
    query = 'for $x in collection("tecu")/entry/notes/note return data($x/@author)'
    @sedna.query(query).each{|au|
      edits << au
    }
    query = 'for $x in collection("tecu")/entry/meta/author return $x/text()'
    @sedna.query(query).each{|au|
      edits << au
    }
    edits.uniq.each{|login|
      name = login
      info = get_users(@admindict.array["tecu_user"], login)
      name = info[0]['name'] unless info[0].nil?
      editors << {'login'=>login, 'name'=>name}
    }
    return editors
  end

  #def math_to_png(math)
  #  math = math.gsub('\\','\\\\')
  #  $stderr.puts math
  #  return Calculus::Expression.new(math, :parse => false).to_png
  #end
  def math_to_png(latex)
      latex = latex.gsub('\\\\','$NL$').gsub('\\','\\\\')#.gsub("\n", " ")
      template = <<-EOT.gsub(/^\s+/, '')
      \\documentclass{article}
      \\usepackage{amsmath}
      \\usepackage[utf8]{inputenc}
      \\begin{document}
      \\thispagestyle{empty}
      $$$
      \\end{document}
      EOT

      latex = '\\begin{equation*}'+"\n"+latex+"\n"+'\\end{equation*}'
      template.sub!('$$$', latex).gsub!('$NL$',"\\\\\\")

      $stderr.puts latex
      $stderr.puts template
      temp_path = Dir.mktmpdir
      sha1 = Digest::SHA1.hexdigest(Time.now.to_s)
      Dir.chdir(temp_path) do
        f = File.open(sha1+".tex", 'w')
        f.write(template)
        f.close
        `latex -interaction=nonstopmode #{sha1}.tex && dvipng -q -T tight -bg White -D 700 -o #{sha1}.png #{sha1}.dvi`
      end
      return File.join(temp_path, "#{sha1}.png") if $?.exitstatus.zero?
  end
  def table_to_png(latex)
    la = latex.split('\\')
    if la[0] != nil
      latex = latex.gsub('\\\\','$NL$').gsub('\\','\\\\')#.gsub("\n", " ")
      template = <<-EOT.gsub(/^\s+/, '')
      \\documentclass{article}
      \\usepackage[utf8]{inputenc}
      \\begin{document}
      \\thispagestyle{empty}
      $$$
      \\end{document}
      EOT

      col = la[0].count('&')+1
      latex = '\\begin{tabular}{|'+"l|"*col+'}'+latex+'\\end{tabular}'
      template.sub!('$$$', latex).gsub!('$NL$',"\\\\\\")

      $stderr.puts latex
      $stderr.puts template
      temp_path = Dir.mktmpdir
      sha1 = Digest::SHA1.hexdigest(Time.now.to_s)
      Dir.chdir(temp_path) do
        f = File.open(sha1+".tex", 'w')
        f.write(template)
        f.close
        `latex -interaction=nonstopmode #{sha1}.tex && dvipng -q -T tight -bg White -D 700 -o #{sha1}.png #{sha1}.dvi`
      end
      return File.join(temp_path, "#{sha1}.png") if $?.exitstatus.zero?
    else
      return nil
    end
  end
  def add_term_links(text, selfid)
    text.gsub!('"','\'')
    result = `/var/lib/deb-server/termcheck/run.cgi "#{text}" "" "#{selfid}"`
    result.gsub!("'",'"')
    $stderr.puts result
    return result
  end
  def add_def_all
    query = 'for $x in collection("tecu")/entry[defs/def/text[not(a)]] return $x'
    @sedna.query(query).each{|xml|
      doc = load_xml_string(xml)
      id = doc.root['id']
      doc.find('defs/def[@lang="cz"]/text[not(a)]').each{|df|
        puts df.to_s
        deftext = df.to_s.sub('<text>','').sub('</text>','').gsub(/<(\/?[^>]*)/,'[\1]')
        newtext = add_term_links(deftext, id)
        newtext.gsub!(/\[ ([^ ]*) \] ?/,'<\1>')
        newtext.gsub!(/ ?\[ \/ ([^ ]*) \]/,'</\1>')
        newtext.gsub!('( ', '(')
        newtext.gsub!(' )', ')')
        newtext.gsub!(' ;', ';')
        newtext.gsub!(' ,', ',')
        puts deftext
        puts newtext
        df.content = newtext
        puts df
      }
      newxml = doc.to_s
      newxml.gsub!(/&lt;a href="\/tezaurus\/term\/([0-9]*)"&gt;/, '<a href="/tezaurus/term/\1">')
      newxml.gsub!('&lt;/a&gt;', '</a>')
      newxml.gsub!('&lt;b&gt;', '<b>')
      newxml.gsub!('&lt;/b&gt;', '</b>')
      newxml.gsub!('&lt;i&gt;', '<i>')
      newxml.gsub!('&lt;/i&gt;', '</i>')
      puts newxml
      update(id, newxml)
    }
  end
  def get_terms
    res = []
    query = 'for $x in collection("tecu")/entry return <a>{data($x/@id)};{$x/terms/term[@lang="cz"][1]/text()}</a>'
    @sedna.query(query).each{|si|
      res << si.sub('<a>','').sub('</a>','')
    }
    return res.join("\n")
  end
  def get_viz(id)
    res = {'term'=>{},'hyper'=>[],'child'=>[]}
    doc = load_xml_string(get(id))
    return res if doc.nil?
    term = ''
    doc.find('/entry/terms/term[@lang="cz"]').each{|t|
      term = t.content.to_s if term == ''
    }
    res['term'] = {'id'=>id, 'term'=>term}
    doc.find('/entry/hyper').each{|h|
      xmlh = get(h['id'].to_s)
      if xmlh != ''
        doch = load_xml_string(xmlh)
        term = ''
        doch.find('/entry/terms/term[@lang="cz"]').each{|t|
          term = t.content.to_s if term == ''
        }
        res['hyper'] << {'id'=>doch.root['id'], 'term'=>term}
      end
    }
    ccount = 0
    xquery_to_array("[entry[hyper/@id='"+id+"']]").each{|xmlc|
      break if ccount >= 10
      ccount += 1
      docc = load_xml_string(xmlc)
      term = ''
      docc.find('/entry/terms/term[@lang="cz"]').each{|t|
        term = t.content.to_s if term == ''
      }
      res['child'] << {'id'=>docc.root['id'], 'term'=>term}
    }
    return res
  end

  def send_mail_role(subject, text, role_to)
    settings = @admindict.array['settings'].get_settings['meta']
    from = settings['contact_email'] unless settings['contact_email'].to_s == ''
    query = 'for $x in collection("tecu_user")/user[roles/role="'+role_to+'"] return $x/email/text()'
    @sedna.query(query).each{|si|
      if si.to_s != '' 
        message = "From: Tezaurus <#{from}>
To: #{si.to_s}
Content-type: text/plain; charset=utf-8
Subject: #{subject}

#{text}
"
        begin
          tecu_mail_send(si.to_s, message)
        rescue => e
          $stderr.puts si.to_s
          $stderr.puts e
          $stderr.puts e.backtrace
        end
      end
    }
  end
  def send_mail_contact(subject, text)
    settings = @admindict.array['settings'].get_settings['meta']
    if settings['contact_email'].to_s != ''
      from = settings['contact_email']
      message = "From: Tezaurus <#{from}>
To: #{from}
Content-type: text/plain; charset=utf-8
Subject: #{subject}

#{text}
"
      begin
        tecu_mail_send(from, message)
      rescue => e
        $stderr.puts from.to_s
        $stderr.puts e
        $stderr.puts e.backtrace
      end
    end
  end

  def send_mail_user(subject, text, user_to)
    settings = @admindict.array['settings'].get_settings['meta']
    from = settings['contact_email'] unless settings['contact_email'].to_s == ''
    query = 'for $x in collection("tecu_user")/user[login="'+user_to+'"] return $x/email/text()'
    @sedna.query(query).each{|si|
      if si.to_s != '' 
        message = "From: Tezaurus <#{from}>
To: #{si.to_s}
Content-type: text/plain; charset=utf-8
Subject: #{subject}

#{text}
"
        begin
          tecu_mail_send(si.to_s, message)
        rescue => e
          $stderr.puts si.to_s
          $stderr.puts e
          $stderr.puts e.backtrace
        end
      end
    }
  end

  def list_history(entry_id)
    result = []
    if entry_id != ''
      query = 'for $x in collection("tecuhist")/entry[@id="'+entry_id+'"] order by $x/@timestamp descending return data($x/@timestamp)'
      @sedna.query(query).each{|si|
        result << {'timestamp'=>si.to_s, 'time'=>Time.at(si.to_i).strftime("%Y-%m-%d %H:%M:%S")}
      }
    end
    return result
  end

  def get_waiting
    res = {'waiting'=>[]}
    query = 'for $x in collection("tecuwork")/entry return <a>{data($x/@id)};{data($x/@wait_for)}</a>'
    @sedna.query(query).each{|r|
      ri = r.sub('<a>','').sub('</a>','').split(';')
      ra = ri[1].to_s.split(',')
      ra.delete_at(ra.index('hl_redaktor') || ra.length) if ra.length > 1
      res['waiting'] << {'id'=>ri[0], 'wait_for'=>ra, 'term'=>get_term(ri[0].to_s, 'work')} unless ra.length == 0
    }
    return res
  end
  def get_deleted
    res = {'deleted'=>[]}
    query = 'for $x in collection("tecudel")/entry return data($x/@id)'
    @sedna.query(query).each{|r|
      res['deleted'] << {'id'=>r, 'term'=>get_term(r, 'del')} 
    }
    return res
  end

  def tecu_mail_send(to, message, copy='')
    require 'net/smtp'
    settings = @admindict.array['settings'].get_settings(true)
    from = 'deb@aurora.fi.muni.cz'
    from = settings['meta']['contact_email'].to_s if settings['meta'] != nil and settings['meta']['contact_email'].to_s != ''
    port = 25
    server = 'localhost'
    username = nil
    password = nil
    if settings['tech'] != nil and settings['tech']['smtp_server'].to_s != ''
      server = settings['tech']['smtp_server'].to_s
      port = settings['tech']['smtp_port'].to_s
      username = settings['tech']['smtp_user']
      password = settings['tech']['smtp_pass']
    end
    #smtp = Net::SMTP.new(server, port)
    #smtp.enable_starttls_auto if port != 25
    #if username.nil? or username == ''
    #  smtp.start(server)
    #else
    #  smtp.start(server, username, password, :plain)
    #end
    #smtp.send_message(message, from, to)
    #smtp.send_message(message, from, copy) if copy != ''
  end

  def who_locked(entry)
    query = "SELECT * FROM locks WHERE entry=?"
    res = false
    @notif_db.execute(query, [entry]).each{|row|
      $stderr.puts row
      if row['time'].to_i < (Time.now.to_i - @lock_timeout)
        query2 = "DELETE FROM locks WHERE entry=? AND time=?"
        @notif_db.execute(query2, [entry, row['time']])
      else
        res = row['login']
      end
    }
    return res
  end
  def lock_entry(entry, user, time)
    time = Time.now.to_i if time == nil
    query = "INSERT INTO locks values (?,?,?)"
    @notif_db.execute(query, [user, entry, time])
  end
  def unlock_entry(entry, user)
    query = "DELETE FROM locks WHERE entry=? AND login=?"
    @notif_db.execute(query, [entry,user])
  end
end
