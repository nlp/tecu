require 'dict/dict-sedna'

class TezCS < DictSedna
  attr_accessor :hypers

  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = CentralServlet
  end

  def update_thesaurus(data_hash, user, confirmed=false)
    xml = '<thesaurus id="'+data_hash['tid']+'" confirmed="'+confirmed.to_s+'">'
    xml += '<url>'+data_hash['url'].to_s+'</url>'
    xml += '<name>'+data_hash['name'].to_s+'</name>'
    xml += '<organization>'+data_hash['organization'].to_s+'</organization>'
    xml += '<description>'+data_hash['description'].to_s+'</description>'
    xml += '<contact_person>'+data_hash['contact_person'].to_s+'</contact_person>'
    xml += '<contact_phone>'+data_hash['contact_phone'].to_s+'</contact_phone>'
    xml += '<contact_email>'+data_hash['contact_email'].to_s+'</contact_email>'
    xml += '</thesaurus>'
    tid = data_hash['tid']
    edits = get_edits(tid)
    edits << {'time'=>Time.now.strftime("%Y-%m-%d %H:%M"), 'author'=>user}
    xml = add_meta(xml, edits)
    if confirmed
      delete(tid+':unconfirmed')
    else
      tid += ':unconfirmed'
    end
    update(tid, xml)
    return tid
  end

  def get_edits(id)
    edits = []
    begin
      xml = get(id)
      doc = load_xml_string(xml.to_s)
      doc.find('//meta/edit').each{|el|
        author = el['author'].to_s
        time = el['time'].to_s
        edits << {'time'=>time, 'author'=>author}
      }
    rescue => e
      $stderr.puts e
    end
    return edits
  end

  def add_meta(xml, edits)
    doc = load_xml_string(xml)
    doc.root << meta = XML::Node.new('meta')
    edits.each{|edit|
      meta << ed = XML::Node.new('edit')
      ed['time'] = edit['time']
      ed['author'] = edit['author']
    }    
    return doc.to_s
  end

  def get_perm(user, dict)
    xml = dict.get(user)
    doc = load_xml_string(xml.to_s)
    perms = []
    doc.find('//perm').each{|e|
      perms << e.content.to_s
    }
    if not doc.find('/user/admin').first.nil? and doc.find('/user/admin').first.content.to_s == 'true'
      perms << 'admin'
    end
    if perms.size > 0
      return perms
    else
      return 'ro'
    end
  end

  def can_confirm(user, dict)
    info = get_users(dict, user)
    return true if info[0] != nil and info[0]['roles'] != nil and info[0]['roles'].include?('referent')
    return false
  end

  def search_term(search)
      res =''
      full = {}
      part = {}
      query = 'for $x in collection("thesaurus")//entry where some $t in $x/terms/term[@lang="cz"] satisfies lower-case($t)=lower-case("' + search.downcase + '") order by $x/../name() return <a>{data($x/../@id)};{data($x/@id)};{$x/terms/term[1]/text()};{$x/defs/def[1]/text}</a>'
      $stderr.puts query
      @sedna.query(query).each{|entry|
        $stderr.puts entry
        resa = entry.sub('<a>','').sub('</a>','').split(';')
        full[resa[0]] = {} if full[resa[0]].nil?
        full[resa[0]][resa[1]] = {'term'=>resa[2], 'def'=>resa[3].to_s.gsub(/<[^>]*>/ui,'')}
      }
      query = 'for $x in collection("thesaurus")//entry where some $t in $x/terms/term[@lang="cz"] satisfies contains(lower-case($t),lower-case("' + search + '")) order by $x/../name() return <a>{data($x/../@id)};{data($x/@id)};{$x/terms/term[1]/text()};{$x/defs/def[1]/text}</a>'
      $stderr.puts query
      @sedna.query(query).each{|entry|
        $stderr.puts entry
        resa = entry.sub('<a>','').sub('</a>','').split(';')
        part[resa[0]] = {} if part[resa[0]].nil?
        part[resa[0]][resa[1]] = {'term'=>resa[2], 'def'=>resa[3].to_s.gsub(/<[^>]*>/ui,'')}
      }
      return full,part
  end

  def search_check(dictid, time, url)
    entries = {}
    query = 'for $x in doc("'+dictid+'", "thesaurus")//entry[notes/note/@time>"'+time+'"] return data($x/@id)'
    $stderr.puts query
    @sedna.query(query).each{|res|
      link = url + 'tezaurus/term/' + res
      $stderr.puts link
      query2 = 'for $x in collection("thesaurus")[//entry[exlinks/exlink="'+link+'"]] return document-uri($x)'
      $stderr.puts query2
      @sedna.query(query2).each{|res2|
        query3 = 'for $x in doc("'+res2+'","thesaurus")//entry[exlinks/exlink="'+link+'"] return data($x/@id)'
        @sedna.query(query3).each{|res3|
          $stderr.puts res2, res3
          entries[res2] = [] if entries[res2].nil?
          entries[res2] << {'dict'=>res2, 'linkfrom'=>res3, 'linkto'=>res, 'linkurl'=>link}
        }

      }
    }
    return entries
  end

  def copy_term(dict, term)
    query = 'for $x in doc("'+dict+'", "thesaurus")//entry[@id="'+term+'"] return $x'
    $stderr.puts query
    @sedna.query(query).each{|res|
      return res
    }
  end
  def get_settings
    settings = {}
    settings['tech'] = {'show_tree'=>'true', 'backup_path'=>'', 'backup_time'=>'', 'smtp_server'=>'', 'log_path'=>'', 'backup_url'=>'', 'backup_user'=>'', 'backup_pass'=>''}
    settings['roles'] = [
      {'role'=>'it_admin','label'=>'IT administrátor CS'},
      {'role'=>'operator','label'=>'Operátor provozu CS'},
      {'role'=>'referent','label'=>'Referent CS'},
      {'role'=>'zpravodaj','label'=>'Zpravodaj CS'},
      {'role'=>'host','label'=>'Host CS'},
    ]
    xml = get('tech')
    doc = load_xml_string(xml)
    unless doc.nil?
      settings['tech']['show_tree'] = doc.find('/tech/show_tree').first.content unless doc.find('/tech/show_tree').first.nil?
      settings['tech']['log_path'] = doc.find('/tech/log_path').first.content unless doc.find('/tech/log_path').first.nil?
      settings['tech']['backup_path'] = doc.find('/tech/backup_path').first.content unless doc.find('/tech/backup_path').first.nil?
      settings['tech']['backup_user'] = doc.find('/tech/backup_user').first.content unless doc.find('/tech/backup_user').first.nil?
      settings['tech']['backup_pass'] = doc.find('/tech/backup_pass').first.content unless doc.find('/tech/backup_pass').first.nil?
      settings['tech']['backup_url'] = doc.find('/tech/backup_url').first.content unless doc.find('/tech/backup_url').first.nil?
      settings['tech']['backup_time'] = doc.find('/tech/backup_time').first.content unless doc.find('/tech/backup_time').first.nil?
      settings['tech']['smtp_server'] = doc.find('/tech/smtp_server').first.content unless doc.find('/tech/smtp_server').first.nil?
    end
    return settings
  end

  def save_settings(data)
    xml = '<tech>'
    unless data['tech'].nil?
      xml += '<show_tree>'+CGI.escapeHTML(data['tech']['show_tree'].to_s)+'</show_tree>'
      xml += '<smtp_server>'+CGI.escapeHTML(data['tech']['smtp_server'].to_s)+'</smtp_server>'
      xml += '<backup_path>'+CGI.escapeHTML(data['tech']['backup_path'].to_s.gsub(/[^0-9A-Z\-_\.\/]/i,''))+'</backup_path>'
      xml += '<backup_url>'+CGI.escapeHTML(data['tech']['backup_url'].to_s.gsub(/[^0-9A-Z\-_\.\/]/i,''))+'</backup_url>'
      xml += '<backup_pass>'+CGI.escapeHTML(data['tech']['backup_pass'].to_s.gsub(/[^0-9A-Z\-_\.\/]/i,''))+'</backup_pass>'
      xml += '<backup_user>'+CGI.escapeHTML(data['tech']['backup_user'].to_s.gsub(/[^0-9A-Z\-_\.\/]/i,''))+'</backup_user>'
      xml += '<log_path>'+CGI.escapeHTML(data['tech']['log_path'].to_s)+'</log_path>'
      xml += '<backup_time>'+CGI.escapeHTML(data['tech']['backup_time'].to_s)+'</backup_time>'
    end
    xml += '</tech>'
    update('tech', xml)
  end
  def get_users(userdict, username='')
    query = '/user[login!="nagios"]'
    query = '/user[login="'+username+'"]' if username != ''
    output = []
    userdict.xquery_to_array(query).sort.each{|xml|
      doc = load_xml_string(xml)
      userhash = {'login'=>'', 'name'=>'', 'email'=>'', 'phone'=>'', 'organization'=>'', 'roles'=>[]}
      userhash['login'] = doc.find('/user/login').first.content.to_s
      userhash['name'] = doc.find('/user/name').first.content.to_s unless doc.find('/user/name').first.nil?
      userhash['email'] = doc.find('/user/email').first.content.to_s unless doc.find('/user/email').first.nil?
      userhash['phone'] = doc.find('/user/phone').first.content.to_s unless doc.find('/user/phone').first.nil?
      userhash['organization'] = doc.find('/user/org').first.content.to_s unless doc.find('/user/org').first.nil?
      doc.find('/user/roles/role').each{|r| userhash['roles'] << r.content}
      output << userhash
    }
    return output
  end

  def get_thes(can_confirm=false, tid="")
      res = []
      query = '[thesaurus]'
      query = '[thesaurus[@id="'+tid+'"]]' if tid != ""
      xquery_to_hash(query).each{|id,xml|
        doc = load_xml_string(xml)
        type_confirmed = 'true'
        type_confirmed = 'false' if id.end_with?(':unconfirmed')
        hash = {'tid'=>doc.root['id'].to_s, 'confirmed'=>type_confirmed, 'name'=>'', 'url'=>'', 'organization'=>'', 'description'=>'', 'contact_person'=>'', 'contact_email'=>'', 'contact_phone'=>'', 'time_register'=>'', 'time_update'=>'', 'terms_count'=>''}
        hash['name'] = doc.find('name').first.content.to_s unless doc.find('name').first.nil?
        hash['url'] = doc.find('url').first.content.to_s unless doc.find('url').first.nil?
        hash['organization'] = doc.find('organization').first.content.to_s unless doc.find('organization').first.nil?
        hash['description'] = doc.find('description').first.content.to_s unless doc.find('description').first.nil?
        hash['time_register'] = doc.find('meta/edit').first['time'].to_s unless doc.find('meta/edit').first.nil?
        hash['time_update'] = doc.find('meta/edit').last['time'].to_s unless doc.find('meta/edit').last.nil?
        begin
          hash['terms_count'] = @sedna.query('count(doc("'+hash['tid']+'","thesaurus")/tecu/entry)')[0]
        rescue =>e
          hash['terms_count'] = 0
        end
        if can_confirm
          hash['contact_person'] = doc.find('contact_person').first.content.to_s unless doc.find('contact_person').first.nil?
          hash['contact_phone'] = doc.find('contact_phone').first.content.to_s unless doc.find('contact_phone').first.nil?
          hash['contact_email'] = doc.find('contact_email').first.content.to_s unless doc.find('contact_email').first.nil?
        end
        res << hash
      }
      return res
  end
end
