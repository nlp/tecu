def system_catch_stdin_stderr(*args)
  args = args.collect {|a| a.to_s}

  pipe_peer_in, pipe_me_out = IO.pipe
  pipe_me_in, pipe_peer_out = IO.pipe
  pipe_me_error_in, pipe_peer_error_out = IO.pipe

  pid = nil
  begin
    Thread.critical = true
    STDOUT.flush
    STDERR.flush

    pid = fork {
      STDIN.reopen(pipe_peer_in)
      STDOUT.reopen(pipe_peer_out)
      STDERR.reopen(pipe_peer_error_out)
      pipe_me_out.close
      pipe_me_in.close
      pipe_me_error_in.close

      begin
        exec(*args)
      rescue
        exit!(255)
      end
    }
  ensure
    Thread.critical = false
  end

  pipe_peer_in.close
  pipe_peer_out.close
  pipe_peer_error_out.close
  pipe_me_out.sync = true

  pipe_me_out.close
  got_stdin = pipe_me_in.read
  pipe_me_in.close unless pipe_me_in.closed?
  got_stderr = pipe_me_error_in.read
  pipe_me_error_in.close unless pipe_me_error_in.closed?

  p, status = Process.waitpid2(pid)
  return [status >> 8, got_stdin, got_stderr]
end

def system_catch_stdin_stderr_with_input(input, *args)
  args = args.collect {|a| a.to_s}

  pipe_peer_in, pipe_me_out = IO.pipe
  pipe_me_in, pipe_peer_out = IO.pipe
  pipe_me_error_in, pipe_peer_error_out = IO.pipe

  pid = nil
  begin
    Thread.critical = true
    STDOUT.flush
    STDERR.flush

    pid = fork {
      STDIN.reopen(pipe_peer_in)
      STDOUT.reopen(pipe_peer_out)
      STDERR.reopen(pipe_peer_error_out)
      pipe_me_out.close
      pipe_me_in.close
      pipe_me_error_in.close

      begin
        exec(*args)
      rescue
        exit!(255)
      end
    }
  ensure
    Thread.critical = false
  end

  pipe_peer_in.close
  pipe_peer_out.close
  pipe_peer_error_out.close

  pipe_me_out.sync = true
  pipe_me_out.print( input ) if input != nil
  pipe_me_out.close
  
  got_stdin = pipe_me_in.read
  pipe_me_in.close unless pipe_me_in.closed?
  got_stderr = pipe_me_error_in.read
  pipe_me_error_in.close unless pipe_me_error_in.closed?

  p, status = Process.waitpid2(pid)
  return [status >> 8, got_stdin, got_stderr]
end
