# Musej� se ukon�ovat v�echny tagy, jm�na tag� a atribut NAME jsou case sensitive,
# Atribut NAME je povinn� u TMPL_* (nelze <TMPL_VAR var>, jako v perlov�m HTML::Template) 
# Ignoruj� se p��padn� XML koment��e etc.
#	(### ty by tam vlastn� mohly b�t, tak�e lep�� by bylo je kop�rovat na v�stup...)
# Pokud je ve zdroji bezd�tn� element tvaru <x></x>, na v�stupu bude <x/>
# Hodnoty atribut� budou na v�stupu v apostrofech (a nap�. "'" se zm�n� na '&apos;')
#	Naopak nutno ' nahrazovat v p��padech <b d='ss<TMPL_VAR NAME="<>a&apos;"/>xx'/>
# TMPL_VAR mus� b�t bu� v textu (�ili to bude regul�rn� element), nebo v hodnot� atributu
# p��padn� TMPL_ROOT bude vyhozen (je to jen pomocn� v�c, aby to nemuselo m�t roottag)

# Zdokumentovat IFNOT, GLOBAL, JOIN, COMMENT for testing, IF umo��uj�c� OR...

### TODO
# Pro TMPL_IF a TMPL_LOOP
#	pokud p�edchoz� sourozenec je text kon��c� na \n
#			nebo je rodi�em TMPL_ROOT a uzel nem� p�edchoz�ho sourozence
#		a pokud z�rove� prvn� d�t� je text za��naj�c� na \n
#		=> smazat \n z d�t�te
#	obdobn� pro n�sleduj�c�ho sourozence a spol. (tedy koncov� tag)
# (proto�e TMPL_* tagy vyh�u, ale voln� m�sta okolo nich z�stanou, 
#	pokud jsem ale pro p�ehlednost/�itelnost �ablony dal TMPL_* tag na samostatn� ��dek
#	je vhodn� tento ��dek vyhodit taky
#
# Je�t� bych teda mohl v na�ten�m dokumentu nahradit TMPL_* x za TMPL_* NAME='x' atp.
#
# �lo by TMPL_ELSE n�jak p�ibl��it origin�lu, nebo origin�ln� poru�uje XML strukturu?
#
# Asi bych m�l p�episovat i dal�� v�ci krom� REXML::Comment

class HTMLTemplate

def initialize (hash = {})
	@params = hash
end

def param (hash)
	@params.update(hash)
end

def handle (node, params)
	return node.to_s if node.class == REXML::Text
	return '<!--' + node.to_s + '-->' if node.class == REXML::Comment

	# P�edpokl�d�me, �e tam ��dn� koment��e a podobn� nejsou
	return unless node.class == REXML::Element

	case node.name
		when 'TMPL_ROOT'
			return node.map{|e| handle e, params}.join
		when 'TMPL_VAR'
			return params[node.attributes['NAME']]
		when 'TMPL_GLOBAL'
			return @params[node.attributes['NAME']]
		when 'TMPL_IF'
			return node.map{|e| handle e, params}.join \
				if node.attributes['NAME'].split('|').inject(false){|orred, item| orred ||= params[item]}
		when 'TMPL_IFNOT'
			return node.map{|e| handle e, params}.join unless params[node.attributes['NAME']]
		when 'TMPL_COMMENT'
			return ''
		when 'TMPL_LOOP'
			# Abych mohl d�lat <TMPL_IF x><TMPL_LOOP x>, mus�m um�t i nil
			return params[node.attributes['NAME']] \
				? params[node.attributes['NAME']].map{|loop_params|
					node.map{|e| handle e, loop_params}.join}.join(node.attributes['JOIN'] || '') \
				: ''
		else
			attrs = ''
			attrs = ' ' + node.attributes.map do |name, value|

				# Tak tohle je imho fakt neintuitivn�. Pokud
				# jsem upravoval value, upravoval jsem p��mo
				# atribut zdrojov�ho xml --- tak�e jsem jej
				# uvnit� TMPL_LOOP nahradil jednou a pak u�
				# byl nahrazen�, tak�e k dal��mu nahrazov�n�
				# nedoch�zelo. No fuck me! ;-)
				newvalue = value.clone
				newvalue.gsub!(/<TMPL_VAR NAME=(["'])((?:(?!\1).)*)\1\/?>/){|s| params[$2]}
				newvalue.gsub!(/\[TMPL_VAR ([^\]]*)\]/){|s| params[$1]}
				newvalue.gsub!(/\[TMPL_GLOBAL (.*)\]/){|s| @params[$1]}
				newvalue.gsub!(/<TMPL_GLOBAL NAME=(["'])((?:(?!\1).)*)\1\/?>/){|s| @params[$2]}
				newvalue.gsub!(/'/, "&apos;")
				"#{name}='#{newvalue}'"
			end.join(" ") if node.has_attributes?

			return "<#{node.name}" + attrs +

				# Bezd�tn� nechceme vracet ve tvaru <a></a>, ale <a/>
				# (A jak to bylo v origin�lu z�ejm� nev�me)
				(node.size == 0 ? '/>' : '>' + node.map{|e| 
                                            handle(e, params).to_s.dup.force_encoding('UTF-8')}.join + "</#{node.name}>")
	end
end

def output (template)
	begin
		template = '<TMPL_ROOT>' + IO.readlines(template, nil)[0] + '</TMPL_ROOT>'
		while template =~ /(<[^>]*)<(TMPL_(VAR|GLOBAL) \w*)>([^>]*>)/
			template.gsub!(/(<[^>]*)<(TMPL_(VAR|GLOBAL) \w*)>([^>]*>)/, '\1[\2]\4')
		end
		template.gsub!(/&(?!([a-zA-Z]+|#[0-9]+|#x[0-9a-fA-F]+);)/, '&amp;')
		template.gsub!(/<TMPL_(VAR|GLOBAL|IF|IFNOT|LOOP) (?!NAME=["'])([^\/>]+)/, '<TMPL_\1 NAME=\'\2\'')
		template.gsub!(/<TMPL_(VAR|GLOBAL) ([^\/>]+)>/, '<TMPL_\1 \2/>')
		handle REXML::Document.new(template).root, @params
	rescue SystemCallError
    nil
	end
end

def output_string(template)
	begin
		template = '<TMPL_ROOT>' + template + '</TMPL_ROOT>'
		while template =~ /(<[^>]*)<(TMPL_(VAR|GLOBAL) \w*)>([^>]*>)/
			template.gsub!(/(<[^>]*)<(TMPL_(VAR|GLOBAL) \w*)>([^>]*>)/, '\1[\2]\4')
		end
		template.gsub!(/&(?!([a-zA-Z]+|#[0-9]+|#x[0-9a-fA-F]+);)/, '&amp;')
		template.gsub!(/<TMPL_(VAR|GLOBAL|IF|IFNOT|LOOP) (?!NAME=["'])([^\/>]+)/, '<TMPL_\1 NAME=\'\2\'')
		template.gsub!(/<TMPL_(VAR|GLOBAL) ([^\/>]+)>/, '<TMPL_\1 \2/>')
		handle REXML::Document.new(template).root, @params
	rescue SystemCallError
    nil
	end
end

end
