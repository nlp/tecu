require 'rubygems'
require 'sedna'
require 'uri'
require 'rexml/document'

#import db from file
#src_file - source xml file
#collection - code of the database for import
#logfile - filename of import log file
#key_path - xpath to key
#roottag - root tag of entry
#delete - delete current database?
#method - parser/grep (grep recommended)
#overwrite - true=overwrite existing entry with new one/false=leave existing entry
def import(collection, src_file, logfile, key_path, roottag, delete=false, overwrite=false)
  sedna = Sedna.connect({:database=>'deb'})
  if delete and sedna.query('fn:doc("$collections")//collection[@name="'+collection+'"]').size > 0
    sedna.query('DROP COLLECTION "'+collection+'"')
  end
  if sedna.query('fn:doc("$collections")//collection[@name="'+collection+'"]').size == 0
    sedna.query('CREATE COLLECTION "'+collection+'"')
  end
  
  log = File.new(logfile, 'w')
  log.puts 'IMPORT STARTED ' + Time.now.to_s

  total_count = `LC_ALL=C grep -c "<#{roottag}[ >\/]" #{src_file}`

  grep_file(src_file, roottag, key_path, collection, log, total_count.to_i, overwrite, sedna)
  
  log.puts 'IMPORT END ' + Time.now.to_s
  log.close
end

def process_key(key)
  return URI.escape(key.strip)
end

def add_document(xml, key_path, collection, log, count, total_count, overwrite, sedna)
  counter = 0
  begin
    last_part = key_path.split('/')[-1]
    key_path += '/text()' if last_part[0,1] != '@'
    doc = REXML::Document.new(xml.to_s)
    REXML::XPath.each( doc, key_path ) do |key|
      if counter > 0 
        raise "index_document: more then one key (#{key_path}) found"
      end
      count += 1
      key = process_key(key.to_s)
      percent = (count.to_f/total_count.to_f)*100
      log.puts count.to_s + '/' + total_count.to_s + '=' + percent.round.to_s + "%  adding #{key}"
     
      if overwrite
        begin
          sedna.query('DROP DOCUMENT "'+key+'" IN COLLECTION "'+collection+'"')
        rescue => e
          log.puts "cannot delete #{key}: #{e.message}"
        end
      end
      begin
        sedna.load_document(xml.to_s, key, collection)
      rescue => e
        log.puts "exception #{e.class.to_s}: #{e.message}"
      end
      counter += 1
    end
    if counter != 1
      log.puts doc
      raise "index_document: no key (#{@key_path}) found"
    end
  rescue => e
    log.puts "exception #{e.class.to_s}: #{e.message}"
    log.puts xml
  end
  return count
end

def grep_file(source_xml, roottag, key_path, container, log, total_count, overwrite, sedna)
  count = 0
  entry = ''
  IO.foreach(source_xml) {|line|
    if line =~ /<#{roottag}[ >\/]/
      #start new
      entry = line
    else
      entry += line
    end
    if line =~ /<\/#{roottag}>/ or line =~ /<#{roottag} [^<]*\/>/
      #parse, save
      if entry != ''
        count = add_document(entry, key_path, container, log, count, total_count, overwrite, sedna)
        entry = ''
      end
    end
  }
  return
end
