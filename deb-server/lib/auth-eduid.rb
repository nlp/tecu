require 'onelogin/ruby-saml'
def init
    request = OneLogin::RubySaml::Authrequest.new
    redirect_to(request.create(saml_settings))
end

def consume
  response = OneLogin::RubySaml::Response.new(params[:SAMLResponse], :settings => saml_settings)

  # We validate the SAML Response and check if the user already exists in the system
  if response.is_valid?
    #        # authorize_success, log the user
    session[:userid] = response.nameid
    session[:attributes] = response.attributes
  else
    authorize_failure  # This method shows an error message
  end
end


def saml_settings
  settings = OneLogin::RubySaml::Settings.new

  settings.assertion_consumer_service_url = "http://#{request.host}/tecu?action=saml_consume"
  settings.sp_entity_id                   = "http://#{request.host}/tecu?action=saml_metadata"
  settings.name_identifier_format         = "urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress"
  settings.idp_sso_target_url             = "https://ds.eduid.cz/wayf.php"

  settings
end
