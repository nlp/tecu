require 'template'
require 'cobravsmongoose'
#require 'unicode'
require 'json'
require 'stringio'
#require 'fastercsv'
require 'open-uri'

class CentralServlet < DictServlet

  def initialize( server, dict, array )
    super
    @servername = nil
    @import_path = '/var/lib/deb-server/files/tecu'
    @var_path = '/var/www/import'
  end


  def do_GET(request, response)
    @servername = 'https://'+request.host if @servername == nil
    user = request.attributes[:auth_user].to_s
    user = 'read' if user == ''

    if request.request_method == 'POST' and request.query['action'].nil?
      if body = JSON.parse(request.body)
        body.each{|k,v|
          if v.is_a?(Hash)
            request.query[k] = v.to_json
          else
            request.query[k] = v
          end
        }
      end
    end


    case request.query['action']
    when 'list_thesaurus'
      tid = ""
      tid = request.query["tid"].to_s.force_encoding("UTF-8") if request.query["tid"].to_s != ""
      res = @dict_array['registr'].get_thes(@dict.can_confirm(user, @dict_array["tecu_user"]), tid)
      response.body = res.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'update_thesaurus'
      if request.query['data'].to_s != ''
        begin
          data_hash = JSON.parse(request.query['data'].to_s)
        rescue
          res = {'error'=>'JSON parsing error'}
        end
        if data_hash != nil 
          if data_hash['tid'].to_s == ''
            data_hash['tid'] = data_hash['short_name'].to_s.gsub(/[^0-9a-z]/i, '').downcase
          end
          if data_hash['tid'].to_s == ''
            data_hash['tid'] = Time.now.to_i.to_s
          end
            $stderr.puts data_hash
            if data_hash['confirmed'] == 'true' and @dict.can_confirm(user, @dict_array["tecu_user"])
              @dict.update_thesaurus(data_hash, user, true)
            else
              @dict.update_thesaurus(data_hash, user, false)
            end
            res = data_hash
        end
      else
        res = {'error'=>'no data'}
      end
      response.body = res.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'confirm_thesaurus'
      if request.query["tid"] != ''
        tid = request.query["tid"].to_s.force_encoding('UTF-8')
        data = @dict_array['registr'].get(tid+":unconfirmed")
        @dict_array['registr'].delete(tid+":unconfirmed")
        data.sub!('confirmed="false"','confirmed="true"')
        @dict_array['registr'].update(tid, data)
        res = @dict_array['registr'].get_thes(@dict.can_confirm(user, @dict_array["tecu_user"]), tid)
      else
        res = {'error'=>'no data'}
      end
      response.body = res.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'delete_thesaurus'
      if request.query["tid"] != ''
        @dict_array['registr'].delete(request.query["tid"].to_s+":unconfirmed")
        @dict_array['registr'].delete(request.query["tid"].to_s)
	res = @dict_array['registr'].get_thes(@dict.can_confirm(user, @dict_array["tecu_user"]), '')
      else
        res = {'error'=>'no data'}
      end
      response.body = res.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'download_all_data'
      res = ''
      @dict_array['registr'].xquery_to_hash('[thesaurus[@confirmed="true"]]').each{|id,xml|
        doc = @dict.load_xml_string(xml)
        unless doc.root.find('url').first.nil?
          url = doc.root.find('url').first.content.to_s.gsub(/\/tezaurus\/?$/,'') + '/tecu?action=download_all'
          $stderr.puts url
          begin
            open(url, {http_basic_authentication: ["read", "r"], ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}) {|io|
              filename = '/var/lib/deb-server/backup/'+id+'-'+Time.now.strftime('%Y-%m-%d-%H%M')+'.xml'
              data = io.read
              data.sub!('<tecu>', '<tecu id="'+id+'">')
              File.write(filename, data)
              @dict_array['thesaurus'].update(id, data)
            }
            res += url
          rescue => e
            $stderr.puts e
            $stderr.puts e.backtrace
          end
        end
      }
      response.body = res
    when 'check_links'
      @dict_array['registr'].xquery_to_hash('[thesaurus[@confirmed="true"]]').each{|id,xml|
        doc = @dict.load_xml_string(xml)
        if @dict_array['thesaurus'].get(id) != ''
          last = doc.root['last_check'].to_s
          unless doc.find('url').first.nil?
            updated = @dict.search_check(id, last, doc.find('url').first.content.to_s)
            $stderr.puts updated
            updated.each{|dict,list|
              xml2 = @dict_array['registr'].get(dict)
              doc2 = @dict.load_xml_string(xml2)
              unless doc2.find('url').first.nil?
                list.each{|uh|
                  url2 = doc2.find('url').first.content.to_s + '/tecu?action=exlink_updated&id='+uh['linkfrom']+'&exdict='+id+'&exlink='+CGI.escape(uh['linkurl'])
                  $stderr.puts url2
                  open(url2, {http_basic_authentication: ["read", "r"], ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}) {|io|
                  }
                }
              end
            }
          end
        end
        doc.root['last_check'] = Time.now.strftime('%Y-%m-%d %H:%M:%S')
        @dict_array['registr'].update(id, doc.to_s)
      }

    when 'search_term'
      response['Content-Type'] = 'application/xml'
      full,part = @dict.search_term(request.query['search'].to_s)
      extid = request.query['extid'].to_s
      res = []
      full.each{|dict,hash|
        next if dict == extid
        xml = @dict_array['registr'].get(dict)
        doc = @dict.load_xml_string(xml)
        unless doc.nil? or doc.find('url').first.nil?
          url = doc.find('url').first.content.to_s
          hash.each{|tid,term|
            res << {'thes_id'=>dict,'url'=>url,'term_id'=>tid,'term'=>term['term'],'definition'=>term['def']}
          }
        end
      }
      part.each{|dict,hash|
        next if dict == extid
        xml = @dict_array['registr'].get(dict)
        doc = @dict.load_xml_string(xml)
        unless doc.nil? or doc.find('url').first.nil?
          url = doc.find('url').first.content.to_s
          hash.each{|tid,term|
            res << {'thes_id'=>dict,'url'=>url,'term_id'=>tid,'term'=>term['term'],'definition'=>term['def']}
          }
        end
      }
      response.body = res.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'copy_term'
      xml = @dict_array['registr'].get(request.query['dict'].to_s)
      doc = @dict.load_xml_string(xml)
      url = ''
      url = doc.find('url').first.content.to_s unless doc.find('url').first.nil?

      xml = @dict.copy_term(request.query['dict'].to_s, request.query['term'].to_s)
      doc = @dict.load_xml_string(xml)
      doc.root << XML::Node.new('exlinks') if doc.find('//exlinks').size == 0
      newlink = XML::Node.new('exlink')
      newlink.content = url + 'tezaurus/term/' + request.query['term'].to_s
      doc.find('//exlinks').first << newlink
      doc.find('/entry/notes').each{|note| note.remove!}

      response.body = doc.to_s
      response['Content-Type'] = 'application/xml; charset=utf-8'

    when 'get_settings'
      response['Content-Type'] = 'application/json; charset=utf-8'
      output = @dict_array['settings'].get_settings
      response.body = output.to_json
    when 'save_settings'
      if request.query['data'].to_s != ''
        begin
          data_hash = JSON.parse(request.query['data'].to_s)
        rescue
          res = {'error'=>'JSON parsing error'}
        end
        if data_hash != nil 
          @dict_array['settings'].save_settings(data_hash)
          res = @dict_array['settings'].get_settings
        end
      else
        res = {'error'=>'no data'}
      end
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = res.to_json
    when 'get_users'
      output = {'users'=>@dict.get_users(@dict_array['tecu_user'],request.query['login'].to_s)}
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = output.to_json
    when 'init'
      res = {'logged_user'=> @dict.get_users(@dict_array['tecu_user'], user)}
      res['logged_user'][0]['lang']='cs'
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = res.to_json
    when 'delete_user'
      if request.query['login'].to_s != ''
        @dict_array['tecu_user'].delete(request.query['login'].to_s)
        res = {'ok'=>'deleted'}
      else
        res = {'error'=>'no data'}
      end
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = res.to_json
    when 'save_user'
      if request.query['data'].to_s != ''
        begin
          data_hash = JSON.parse(request.query['data'].to_s)
        rescue
          res = {'error'=>'JSON parsing error'}
        end
        if data_hash != nil and data_hash['login'].to_s != ''
          newuser = false
          login = data_hash['login']
          if data_hash['password'].to_s != ''
            newpass = data_hash['password']
            pass = newpass.crypt(WEBrick::Utils::random_string(2))
          elsif data_hash['new'] == '1'
            newpass = WEBrick::Utils::random_string(8)
            pass = newpass.crypt(WEBrick::Utils::random_string(2))
            newuser = true
          else
            pass = @server[:Authenticator].userdb[login]
          end
          xml = '<user>'
          xml += '<login>'+login+'</login>'
          xml += '<name>'+data_hash['name'].to_s+'</name>'
          xml += '<email>'+data_hash['email'].to_s+'</email>'
          xml += '<phone>'+data_hash['phone'].to_s+'</phone>'
          xml += '<org>'+data_hash['organization'].to_s+'</org>'
          xml += '<services><service code="tecu"/></services>'
          xml += '<pass>'+pass+'</pass>'
          unless data_hash['roles'].nil?
            xml += '<roles>'
            data_hash['roles'].each{|r| xml += '<role>'+r.to_s+'</role>'}
            xml += '</roles>'
          end
          xml += '</user>'
          $stderr.puts xml
          @dict_array['tecu_user'].update(login, xml)
          @server[:Authenticator].userdb.reload(true)

          res = @dict.get_users(@dict_array['tecu_user'], login)
        end
      else
        res = {'error'=>'no data'}
      end
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = res.to_json

    end
  end
  
  alias do_POST do_GET
end
