require 'template'
require 'cobravsmongoose'
#require 'unicode'
require 'json'
require 'json-diff'
require 'stringio'
#require 'fastercsv'
require 'csv'
require 'open-uri'
require 'net/http'
require 'net/https'
require 'uri'
require 'tempfile'


module WEBrick
  class HTTPResponse
    def create_error_page
      begin
        servername = URI(@request_uri).host.to_s
      rescue
        servername = ''
      end
      @body = File.read("/var/lib/deb-server/files/tezaurus/index.html")
      @body.sub!(/<title>[^<]*<\/title>/, '<title>Tezaurus</title>')
      Dir['/var/lib/deb-server/files/tezaurus/static/css/*css'].select{|css|
        @body.sub!('</head>', '<style>'+File.read(css)+'</style></head>')
      }
      case @status
      when 401
        error = 'p&#x159;&#xED;stup zak&#xE1;z&#xE1;n ('+@reason_phrase+')'
      when 403
        error = 'p&#x159;&#xED;stup zak&#xE1;z&#xE1;n ('+@reason_phrase+')'
      when 404
        error = 'soubor nenalezen ('+@reason_phrase+')'
      else
        error = 'do&#x10D;asn&#xE1; chyba aplikace'
      end
      @body.sub!(/<body>.*<\/body>/,'<body><div id="root"><div><div id="appBar" class="ui small inverted top fixed menu"><div class="menu appBar_menu"><a class="header item" href="/tezaurus"><img alt="" src="/tezaurus/static/media/logo.a33ce1c9.png" class="ui image logo__img">Tezaurus</a><div class="item"><div class="ui category search"><form placeholder="Hledej..." icon="search" input="[object Object]" value="" class="ui form"><div class="field"><div class="ui icon input"><input placeholder="Hledej..." type="text"><i aria-hidden="true" class="search link icon"></i></div></div></form><div class="results transition"><div class="message empty"><div class="header">No results found.</div></div></div></div><a href="/tezaurus/filter"><button class="ui button" style="margin-left: 1em;">Filtr</button></a><div class="languageSelect"><i title="cs" class="cz flag current"></i><i title="en" class="uk flag"></i></div></div></div><div class="right menu"></div></div><div id="mainBody"><div class="main"><div class="ui basic segment main-segment main-segment--padded"><div class="ui text container"><p>Nastala chyba: '+error+'<br>Pokra&#x10D;ujte pros&#xED;m z <a href="http://'+servername+'">hlavn&#xED; str&#xE1;nky</a>.</p></div></div></div></div></div></div>')
    end
  end
end



class TecuServlet < DictServlet

  def reconnect
    @dict_array.each{|id,dict|
      dict.reconnect
    }
  end

  def initialize( server, dict, array )
    super
    @servername = nil
    @hypers = nil
    @import_path = '/var/lib/deb-server/files/tecu'
    @var_path = '/var/www/import'
  end

  def wsdl_message(params)
    res = '<result>'
    params.each{|k,v|
      res += '<'+k+'>'+v.to_s+'</'+k+'>'
    }
    res += '</result>'
    return res
  end

  def hash_to_wsdl(data)
    res = '<result-list>'
    data.each{|h|
      res += '<result-entry>'
      h.each{|k,v|
        res += '<'+k+'>'+v.to_s+'</'+k+'>'
      }
      res += '</result-entry>'
    }
    res += '</result-list>'
    return res
  end

  def normalize_json(hash)
    if hash['entry']['trans'] != nil and hash['entry']['trans']['tr'].is_a?(Hash)
      h = hash['entry']['trans']['tr']
      hash['entry']['trans']['tr'] = [h]
    end
    if hash['entry']['hyper'].is_a?(Hash)
      h = hash['entry']['hyper']
      hash['entry']['hyper'] = [h]
    end
    if hash['entry']['see'].is_a?(Hash)
      h = hash['entry']['see']
      hash['entry']['see'] = [h]
    end
    if hash['entry']['also'].is_a?(Hash)
      h = hash['entry']['also']
      hash['entry']['also'] = [h]
    end
    if hash['entry']['domains'] != nil and hash['entry']['domains']['dom'].is_a?(Hash)
      h = hash['entry']['domains']['dom']
      hash['entry']['domains']['dom'] = [h]
    end
    if hash['entry']['defs'] != nil and hash['entry']['defs']['def'].is_a?(Hash)
      h = hash['entry']['defs']['def']
      hash['entry']['defs']['def'] = [h]
    end
    if hash['entry']['refs'] != nil and hash['entry']['refs']['ref'].is_a?(Hash)
      h = hash['entry']['refs']['ref']
      hash['entry']['refs']['ref'] = [h]
    end
    if hash['entry']['sees'] != nil and hash['entry']['sees']['see'].is_a?(Hash)
      h = hash['entry']['sees']['see']
      hash['entry']['sees']['see'] = [h]
    end
    if hash['entry']['antonyms'] != nil and hash['entry']['antonyms']['antonym'].is_a?(Hash)
      h = hash['entry']['antonyms']['antonym']
      hash['entry']['antonyms']['antonym'] = [h]
    end
    if hash['entry']['homonyms'] != nil and hash['entry']['homonyms']['homonym'].is_a?(Hash)
      h = hash['entry']['homonyms']['homonym']
      hash['entry']['homonyms']['homonym'] = [h]
    end
    if hash['entry']['exlinks'] != nil and hash['entry']['exlinks']['exlink'].is_a?(Hash)
      h = hash['entry']['exlinks']['exlink']
      hash['entry']['exlinks']['exlink'] = [h]
    end
    if hash['entry']['imgs'] != nil and hash['entry']['imgs']['img'].is_a?(Hash)
      h = hash['entry']['imgs']['img']
      hash['entry']['imgs']['img'] = [h]
    end
    if hash['entry']['videos'] != nil and hash['entry']['videos']['video'].is_a?(Hash)
      h = hash['entry']['videos']['video']
      hash['entry']['videos']['video'] = [h]
    end
    if hash['entry']['synonyms'] != nil and hash['entry']['synonyms']['synonym'].is_a?(Hash)
      h = hash['entry']['synonyms']['synonym']
      hash['entry']['synonyms']['synonym'] = [h]
    end
    if hash['entry']['alsos'] != nil and hash['entry']['alsos']['also'].is_a?(Hash)
      h = hash['entry']['alsos']['also']
      hash['entry']['alsos']['also'] = [h]
    end
    if hash['entry']['terms'] != nil and hash['entry']['terms']['term'].is_a?(Hash)
      h = hash['entry']['terms']['term']
      hash['entry']['terms']['term'] = [h]
    end
    if hash['entry']['paths'] != nil and hash['entry']['paths']['path'].is_a?(Hash)
      h = hash['entry']['paths']['path']
      hash['entry']['paths']['path'] = [h]
    end
    if hash['entry']['paths'] != nil and hash['entry']['paths']['path'] != nil
      hash['entry']['paths']['path'].each{|ha|
        if ha['path_node'] != nil and ha['path_node'].is_a?(Hash)
          h = ha['path_node']
          ha['path_node'] = [h]
        end
      }
    end
    if hash['entry']['paths'] != nil and hash['entry']['paths']['path'] != nil and not hash['entry']['paths']['path'][0].key?('path_node')
      hash['entry'].delete('paths')
    end
    if hash['entry']['notes'] != nil and hash['entry']['notes']['note'].is_a?(Hash)
      h = hash['entry']['notes']['note']
      hash['entry']['notes']['note'] = [h]
    end
    #if hash['entry']['@abbrev'] != nil
    #  if hash['entry']['@abbrev'] == 'true'
    #    hash['entry']['@abbrev'] = true
    #  else
    #    hash['entry']['@abbrev'] = false
    #  end
    #end
    #if hash['entry']['@schvaleno'] != nil
    #  if hash['entry']['@schvaleno'] == 'true'
    #    hash['entry']['@schvaleno'] = true
    #  else
    #    hash['entry']['@schvaleno'] = false
    #  end
    #end
    return hash
  end
  
  def compare_version(hash_old, hash_new)
    diff = JsonDiff.diff(hash_old, hash_new, {:include_was=>true})
    changes = []
    notes = []
    diff.each{|dh|
      next if dh['path'].include?('@schvaleno') or dh['path'].include?('paths') or dh['path'].include?('notes') or dh['path'].include?('timestamp') or dh['path'].include?('@number') or dh['path'].include?('schvaleni') or dh['path'].include?('wait_for') or dh['path'].include?('@status') or dh['path'].include?('comment') or dh['path'].include?('@obor_color') or dh['path'].include?('@obor_mark')
      next if dh['op'] == 'add' and dh['value'] == {}
      $stderr.puts dh
      if dh['op'] == 'add'
        field, change = which_field(dh['path'])
        changes << change
        value = ''
        $stderr.puts dh['value']
        $stderr.puts dh['value'].class
        if dh['value'].is_a?Array
          dh['value'] = dh['value'][0]
        end
        if dh['value'].is_a?String
          value = dh['value']
        elsif not dh['value'].is_a?Array and not dh['value']['$'].nil?
          value = dh['value']['$']
        elsif not dh['value'].is_a?Array and not dh['value']['text'].nil?
          value = dh['value']['text']['$']

        end
        note = 'přidání '+field+' '+value
        $stderr.puts note
        notes << note
      end
      if dh['op'] == 'replace'
        field, change = which_field(dh['path'])
        changes << change
        value = ''
        orig = ''
        $stderr.puts dh['value'].class
        if dh['value'].is_a?String or dh['value'].is_a?TrueClass or dh['value'].is_a?FalseClass
          value = dh['value'].to_s
          orig = dh['was'].to_s
          value = 'ano' if value == 'true'
          value = 'ne' if value == 'false'
          orig = 'ano' if orig == 'true'
          orig = 'ne' if orig == 'false'
        elsif not dh['value']['$'].nil?
          value = dh['value']['$']
          orig = dh['was']['$']
        else
          value = dh['value']['text']['$']
          orig = dh['was']['text']['$']
        end
        value = value[0] if value.is_a?Array
        orig = orig[0] if orig.is_a?Array
        note = 'změna '+field+' '+orig+' -> '+value
        $stderr.puts note
        notes << note
      end
      if change == 'def'
        def_num = dh['path'].split('/')[4].to_i
        changes << 'changed_obor='+hash_new['entry']['defs']['def'][def_num]['@obor']
      end
    }
    return notes, changes
  end

  def export_html(res, short, local=false)
    result = '<tecu>'
    res.each{|entry|
      xml = @dict.get_doc(entry['id'])
      doc = @dict.load_xml_string(xml.to_s)
      doc.find('/entry/defs/def/math').each{|de|
        if de.content.to_s != ''
          img = @dict.math_to_png(de.content.to_s)
          newbase = XML::Node.new('math_base64')
          de.parent << newbase
          newbase.content = Base64.strict_encode64(File.read(img))
        end
      }
      doc.find('/entry/defs/def/table').each{|de|
        if de.content.to_s != ''
          img = @dict.table_to_png(de.content.to_s)
          newbase = XML::Node.new('table_base64')
          de.parent << newbase
          newbase.content = Base64.strict_encode64(File.read(img))
        end
      }
      result += doc.to_s.sub('<?xml version="1.0" encoding="UTF-8"?>','')
    }
    result += '</tecu>'
    params = []
    params << ["type",'"short"'] if short
    params << ["local",'"true"'] if local
    result = @dict.apply_transform("preview", result.to_s, params)
    return result
  end


  def which_field(path)
    field = ''
    change = ''
    if path.include?('synonym')
      field = 'synonymum'
      change = 'synonym'
    elsif path.include?('antonym')
      field = 'antonymum'
      change = 'antonym'
    elsif path.include?('homonym')
      field = 'homonymum'
      change = 'homonym'
    elsif path.include?('exlink')
      field = 'externi odkaz'
      change = 'exlink'
    elsif path.include?('/term')
      field = 'preklad'
      change = 'term'
    elsif path.include?('@obor')
      field = 'obor'
      change = 'obor'
    elsif path.include?('text')
      field = 'definice'
      change = 'def'
    elsif path.include?('zdroj')
      field = 'zdroj'
      change = 'def'
    elsif path.include?('law')
      field = 'zdroj ze zakona'
      change = 'def'
    elsif path.include?('math')
      field = 'vzorec'
      change = 'def'
    elsif path.include?('table')
      field = 'tabulka'
      change = 'def'
    elsif path.include?('druh')
      field = 'druh vykladu'
      change = 'def'
    elsif path.include?('type')
      field = 'typ vykladu'
      change = 'def'
    elsif path.include?('def')
      field = 'vyklad'
      change = 'def'
    elsif path.include?('dom')
      field = 'obor'
      change = 'obor'
    elsif path.include?('hyper') and path.include?('@term')
      field = 'hyperonymum'
      change = 'hyper'
    elsif path.include?('hyper')
      field = 'hyperonymum'
      change = 'hyper'
    elsif path.include?('img')
      field = 'obrazek'
      change = 'img'
    elsif path.include?('video')
      field = 'video'
      change = 'video'
    elsif path.include?('abbrev')
      field = 'zkratka'
      change = 'abbrev'
    end
    return field, change
  end

def next_step(changes, status, schvaleni, wait_for, change_notes, user_roles, entry_hash)
  return 'public','' if @dict_array['settings'].get_settings['dict']['simpleprocess'] == 'true'
  $stderr.puts changes
  obor = ''
  changes.each{|ch|
    if ch.include?('changed_obor')
      obor = '_'+ch.split('=')[1]
    end
  }
  if obor == ''
    change_notes.each{|note|
      if note['@changes'].to_s != ''
        note['@changes'].split(';').each{|cn|
          if cn.include?('changed_obor')
            obor = '_'+cn.split('=')[1]
          end
        }
      end
    }
  end
  if obor == ''
    if entry_hash['entry']['defs'] != nil and entry_hash['entry']['defs']['def'] != nil
      entry_hash['entry']['defs']['def'].each{|edef|
        $stderr.puts edef
        $stderr.puts edef.class
        $stderr.puts edef['@obor'].to_s
        obor = '_' + edef['@obor'].to_s if edef['@obor'].to_s != ''
      }
    end
  end
  if status  == 'to_delete' and schvaleni == 'schvaleno'
    return 'delete_tajemnik','tajemnik'
  end
  if status  == 'to_delete' and schvaleni == 'zamitnuto'
    return 'public',''
  end
  if status  == 'to_recover' and schvaleni == 'schvaleno'
    return 'public',''
  end
  if status  == 'to_recover' and schvaleni == 'zamitnuto'
    return 'deleted',''
  end
  if status  == 'delete_tajemnik' and schvaleni == 'schvaleno'
    return 'deleted',''
  end
  if status  == 'delete_tajemnik' and schvaleni == 'zamitnuto'
    return 'public',''
  end
  if status == 'public' or status == ''
    if changes.size > 0
      return 'zmena_entry','asist_hl'
    else
      return 'public',''
    end
  end
  if status == 'before_new'
    return 'vklada_new','obor_redaktor'+obor
  end
  if changes.include?('entry')
    return 'vklada_new','obor_redaktor'+obor
  end
  if status == 'zmena_entry' and schvaleni == 'zamitnuto'
    return 'zmena_zamitnuta',''
  end
  if status == 'zmena_entry' and schvaleni == 'schvaleno'
    return 'vklada_exist','obor_redaktor'+obor
  end
  if status == 'vklada_exist' and schvaleni == 'schvaleno'
    prekladatele = []
    @dict_array['settings'].get_settings['dict']['langs'].each{|lang|
      prekladatele << 'prekladatel_'+lang if lang != 'cz'
    }
    return 'vklada_preklad',prekladatele.join(',')
  end
  if status == 'vklada_exist' and schvaleni == 'zamitnuto'
    return 'zmena_zamitnuta',''
  end
  if status == 'vklada_new' 
    return 'novotvar','jazyk_specialista'
  end
  #if status == 'vklada_new' and schvaleni == 'zamitnuto'
  #  return 'zmena_zamitnuta',''
  #end
  #if status == 'vklada_new' and schvaleni == 'schvaleno'
  #  return 'novotvar','jazyk_specialista'
  #end
  if status == 'novotvar' and schvaleni == 'schvaleno'
    prekladatele = []
    @dict_array['settings'].get_settings['dict']['langs'].each{|lang|
      prekladatele << 'prekladatel_'+lang if lang != 'cz'
    }
    return 'vklada_preklad',prekladatele.join(',')
  end
  if status == 'novotvar' and schvaleni == 'zamitnuto'
    return 'vklada_new','obor_redaktor'+obor
  end
  #if changes.include?('term')
  #  return 'revize_preklad','recenzent_preklad'
  #end
  if status == 'vklada_preklad' and user_roles.include?('hl_redaktor') and schvaleni == 'schvaleno'
    prekladatele = []
    @dict_array['settings'].get_settings['dict']['langs'].each{|lang|
      prekladatele << 'recenzent_preklad_'+lang if lang != 'cz'
    }
    return 'revize_preklad',prekladatele.join(',')
  end
  if status == 'revize_preklad' and user_roles.include?('hl_redaktor') and schvaleni == 'schvaleno'
    return 'kontrola_redaktor','obor_redaktor'+obor
  end
  if status == 'vklada_preklad'
    wait_ar = wait_for.split(',')
    wait_ar_n = []
    wait_ar.each{|wr|
      wait_ar_n << wr if wr.start_with?('prekladatel_') and not user_roles.include?(wr)
    }
    if wait_ar_n.size == 0
      prekladatele = []
      @dict_array['settings'].get_settings['dict']['langs'].each{|lang|
        prekladatele << 'recenzent_preklad_'+lang if lang != 'cz'
      }
      return 'revize_preklad',prekladatele.join(',')
    else
      return 'vklada_preklad',wait_ar_n.join(',')
    end
  end
  if status  == 'revize_preklad' and schvaleni == 'schvaleno'
    wait_ar = wait_for.split(',')
    wait_ar_n = []
    wait_ar.each{|wr|
      wait_ar_n << wr if wr.start_with?('recenzent_preklad_') and not user_roles.include?(wr)
    }
    if wait_ar_n.size == 0
      return 'kontrola_redaktor','obor_redaktor'+obor
    else
      return 'revize_preklad',wait_ar_n.join(',')
    end
  end
  if status  == 'revize_preklad' and schvaleni == 'zamitnuto'
    prekladatele = []
    @dict_array['settings'].get_settings['dict']['langs'].each{|lang|
      prekladatele << 'prekladatel_'+lang if lang != 'cz'
    }
    return 'vklada_preklad',prekladatele.join(',')
  end
  if status  == 'kontrola_redaktor' and schvaleni == 'zamitnuto'
    prekladatele = []
    @dict_array['settings'].get_settings['dict']['langs'].each{|lang|
      prekladatele << 'prekladatel_'+lang if lang != 'cz'
    }
    return 'vklada_preklad',prekladatele.join(',')
  end
  if status  == 'kontrola_redaktor' and schvaleni == 'schvaleno'
    return 'kontrola_recenzent','obor_revizor'+obor
  end
  if status  == 'kontrola_recenzent' and schvaleni == 'zamitnuto'
    return 'kontrola_redaktor','obor_redaktor'+obor
  end
  if status  == 'kontrola_recenzent' and schvaleni == 'schvaleno'
    return 'kontrola_korektor','jazyk_korektor'
  end
  if status  == 'kontrola_korektor' and schvaleni == 'schvaleno'
    return 'kontrola_asistent','asist_hl'
  end
  if status  == 'kontrola_korektor' and schvaleni == 'zamitnuto'
    return 'kontrola_redaktor','obor_redaktor'+obor
  end
  if status  == 'kontrola_asistent' and schvaleni == 'zamitnuto'
    return 'kontrola_redaktor','obor_redaktor'+obor
  end
  if status  == 'kontrola_asistent' and schvaleni == 'schvaleno'
    return 'kontrola_hlavni','hl_redaktor'
  end
  if status  == 'kontrola_hlavni' and schvaleni == 'zamitnuto'
    return 'kontrola_redaktor','obor_redaktor'+obor
  end
  if status  == 'kontrola_hlavni' and schvaleni == 'schvaleno'
    return 'kontrola_tajemnik','tajemnik'
  end
  if status  == 'kontrola_tajemnik' and schvaleni == 'vratit'
    return 'kontrola_hlavni','hl_redaktor'
  end
  if status  == 'kontrola_tajemnik' and schvaleni == 'schvaleno'
    return 'public',''
  end
  if status  == 'kontrola_tajemnik' and schvaleni == 'zamitnuto'
    return 'deleted',''
  end
  return status, wait_for
end

def add_term_links(text, selfid="0")
  text.gsub!('"','\'')
  result = `/var/lib/deb-server/termcheck/run.cgi "#{text}" "" "#{selfid}"` 
  result.gsub!("'",'"')
  $stderr.puts result
  return result
end

def fix_xml(xml)
  xml.gsub!('&lt;/a&gt;', '</a>')
  xml.gsub!(/\&lt;a href=\&quot;([^&]*)\&quot;\&gt;/, '<a href="\1">')
  xml.gsub!(/\&lt;(\/?[a-z]*)\&gt;/, '<\1>')
  return xml
end

def array_to_wsdl(data)
  res = '<result-list>'
  data.each{|v|
    res += '<result-entry>'
    if v.is_a?(Array)
      res += array_to_wsdl(v)
    elsif v.is_a?(Hash)
      res += hash_to_wsdl(v)
    else
      res += v.to_s
    end
    res += '</result-entry>'
  }
  res += '</result-list>'
  return res
end

def add_history(entry_id, xml)
  current_time = Time.now.to_i.to_s
  doc = @dict.load_xml_string(xml)
  doc.root['timestamp'] = current_time
  doc.root['time'] = Time.now.to_s
  @dict_array['tecuhist'].update(entry_id+'_'+current_time, doc.to_s)
end

def finish_csv_term(termhash)
  termxml = '<entry id="'+termhash['id']+'" abbrev="'+termhash['abbr']+'">'
  termxml +='<terms>'
  termhash['terms'].each{|t|
    termxml += '<term lang="'+t['lang'].gsub(/[^a-z]/,'')+'">'+t['term'].gsub('<','&lt;')+'</term>'
  }
  termxml +='</terms>'
  termxml +='<defs>'
  termhash['defs'].each{|t|
    termxml += '<def lang="'+t['lang'].gsub(/[^a-z]/,'')+'" obor="'+t['obor'].gsub('"','')+'"><text>'+t['def'].gsub('<','&lt;')+'</text><zdroj>'+t['zdroj'].gsub('<','&lt;')+'</zdroj></def>'
  }
  termxml +='</defs>'
  termhash['hyp'].uniq.each{|t|
    termxml += '<hyper id="'+t.to_s+'"/>'
  }
  termxml +='<synonyms>'
  termhash['syn'].each{|t|
    termxml += '<synonym>'+t.gsub('<','&lt;')+'</synonym>'
  }
  termxml +='</synonyms>'
  termxml +='<antonyms>'
  termhash['ant'].each{|t|
    termxml += '<antonym>'+t.gsub('<','&lt;')+'</antonym>'
  }
  termxml +='</antonyms>'

  origxml = @dict.get(termhash['id'])
  if origxml != '' 
    doc = @dict.load_xml_string(origxml)
    termxml += '<notes>'
    nn = 1
    doc.find('/entry/notes/note').each{|n|
      termxml += n.to_s.force_encoding('UTF-8')
      nn += 1
    } 
    termxml +='<note time="'+Time.now.strftime('%Y-%m-%d %H:%M:%S')+'" number="'+nn.to_s+'">import z CSV</note>'
    termxml += '</notes>'
    termxml += doc.find('/entry/meta').first.to_s.force_encoding('UTF-8') unless doc.find('/entry/meta').first.nil?
  end
  termxml += "</entry>\n"
  return termxml
end




def do_GET(request, response)
  @servername = 'https://'+request.host if @servername == nil
  user = request.attributes[:auth_user].to_s
  user = 'read' if user == ''
  #reconnect 

  if request.request_method == 'POST' and request.query['action'].nil?
    if body = JSON.parse(request.body)
      body.each{|k,v|
        if v.is_a?(Hash)
          request.query[k] = v.to_json
        else
          request.query[k] = v
        end
      }
    end
  end

  case request.query['action']
  when 'init'
    res = {'logged_user'=> @dict.get_users(@dict_array['tecu_user'], user, true)}
    res['logged_user'][0]['lang']='cs'
    response['Content-Type'] = 'application/json; charset=utf-8'
    response.body = res.to_json

  when 'search2','search'
    search_data = {}
    search_data = JSON.parse(request.query['data'].to_s) unless request.query['data'].to_s == ''
    
    if request.query['search']!=nil
      lang = 'cz'
      lang = request.query['lang'].to_s if request.query['lang'].to_s != ''
      suggest = false
      suggest = true if request.query['suggest'].to_s == 'true'
      res = @dict.search({
        'search'=>request.query['search'].to_s.force_encoding("UTF-8").gsub("(",""),
        'lang'=>lang,
        'suggest'=>suggest,
        'not_abbr'=>request.query['not_abbr'].to_s,
        'not_full'=>request.query['not_full'].to_s
      })
      result = {
        'terminologicky'=>{'name'=>'terminologický','results'=>[]},
        #'pouzivany'=>{'name'=>'používaný','results'=>[]},
        #'nezpracovany'=>{'name'=>'nezpracovaný','results'=>[]}
      }
      res.each{|entry|
        typ = 'terminologicky' #if entry['status'] == 1
        #typ = 'pouzivany' if entry['status'] == 2
        #typ = 'nezpracovany' if entry['status'] == 5
        if entry['head'] == request.query['search'].to_s
          result[typ]['results'].unshift({'id'=>entry['id'], 'title'=>entry['head']})
        else
          result[typ]['results'] << {'id'=>entry['id'], 'title'=>entry['head']}
        end
      }
      response.body = result.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
      return response.body
    end

    if search_data['search'].to_s.length_utf8 == 1 or search_data['search'].to_s.length_utf8 > 2 or search_data['domain'].is_a?(Array) or search_data['datum_zmeny_od'].to_s != '' or search_data['datum_zmeny_do'].to_s != '' or search_data['datum_zalozeni_od'].to_s != '' or search_data['datum_zalozeni_do'].to_s != '' or search_data['typ'].is_a?(Array) or search_data['definice'].to_s != '' or search_data['druh'].is_a?(Array) or search_data['author'].to_s != '' or search_data['zkratka'].to_s != '' or search_data['stav'].to_s != ''
      res = @dict.search(search_data)
      result = {
        'terminologicky'=>{'name'=>'terminologický','results'=>[]},
        #'pouzivany'=>{'name'=>'používaný','results'=>[]},
        #'nezpracovany'=>{'name'=>'nezpracovaný','results'=>[]}
      }
      res.each{|entry|
        typ = 'terminologicky' #if entry['status'] == 1
        #typ = 'pouzivany' if entry['status'] == 2
        #typ = 'nezpracovany' if entry['status'] == 5
        result[typ]['results'] << {'id'=>entry['id'], 'title'=>entry['head']}
      }
      response.body = result.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    else
      response.body = '{"error":true, "msg":"Zadejte text alespoň 3 znaky dlouhý"}'
      response['Content-Type'] = 'application/json; charset=utf-8'
    end
  when 'get_deleted'
      res = @dict.get_deleted
      if request.query['format'].to_s == 'html'
        response.body = @dict.apply_transform('search', hash_to_wsdl(res), [ [ 'deleted', '"true"'] ])
        response['Content-Type'] = 'text/html; charset=utf-8'
      elsif request.query['format'].to_s == 'wsdl'
        response.body = hash_to_wsdl(res)
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = res.to_json
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
  when 'get_subtree'
    if request.query['id'].to_s != ''
      res = @dict.get_subtree(request.query['id'].to_s, @hypers)
      if request.query['format'].to_s == 'wsdl'
        response.body = hash_to_wsdl(res)
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = res.to_json
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    else
      if request.query['format'].to_s == 'wsdl'
        response.body = wsdl_message({'result'=>'error','message'=>"Nezadáno ID"})
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = '{"error":true, "msg":"Nezadáno ID"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    end
  when 'export_csv'
    res = @dict.export_csv
    csvstring = CSV.generate({:col_sep => "\t",:force_quotes=>true}){|csv| 
      csv << ['id', 'zkratka','termin','jazyk terminu','definice','jazyk definice','obor','zdroj','hyperonyma','synonyma','antonyma']
      res.each{|r| csv << r}
    }
    response.body = csvstring
    response['Content-Type'] = 'application/octet-stream'
    response['Content-Disposition'] = 'attachment; filename=tecu_export.csv'
  when 'export_subtree'
    if request.query['id'].to_s != ''
      if request.query['id'].to_s == '0'
        res = []
        @dict.tree.each{|he|
          res += @dict.export_subtree(he["id"], @hypers)
        }
      else
        res = @dict.export_subtree(request.query['id'].to_s, @hypers)
      end
      if request.query['format'].to_s == 'wsdl'
        response.body = array_to_wsdl(res)
        response['Content-Type'] = 'application/xml; charset=utf-8'
      elsif request.query['format'].to_s == 'json'
        response.body = res.to_json
        response['Content-Type'] = 'application/json; charset=utf-8'
      else
        csvstring = CSV.generate({:col_sep => "\t",:force_quotes=>true}){|csv| 
          csv << ['id', 'zkratka','termin','jazyk terminu','definice','jazyk definice','obor','zdroj','hyperonyma','synonyma','antonyma']
          res.each{|r| csv << r}
        }
        response.body = csvstring
        response['Content-Type'] = 'application/octet-stream'
        response['Content-Disposition'] = 'attachment; filename=tecu_export_'+request.query['id'].to_s+'.csv'
      end
    else
      if request.query['format'].to_s == 'wsdl'
        response.body = wsdl_message({'result'=>'error','message'=>"Nezadáno ID"})
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = '{"error":true, "msg":"Nezadáno ID"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    end
  when 'export_tree'
      res = @dict.export_tree_struct(@hypers, '0', 0)
      response.body = res
      response['Content-Type'] = 'text/plain; charset=utf-8'
  when 'get_path'
    if request.query['id'].to_s != ''
      res = @dict.get_path(request.query['id'].to_s)
      if request.query['format'].to_s == 'wsdl'
        response.body = array_to_wsdl(res)
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = res.to_json
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    else
      if request.query['format'].to_s == 'wsdl'
        response.body = wsdl_message({'result'=>'error','message'=>"Nezadáno ID"})
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = '{"error":true, "msg":"Nezadáno ID"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    end
  when 'add_def_all'
    @dict.add_def_all
  when 'get_terms'
    response.body = @dict.get_terms
    response['Content-Type'] = 'text/plain; charset=utf-8'
  when 'vert_terms'
    terms = @dict.get_terms
    out = ''
    terms.split("\n").each{|tl|
      ta = tl.split(';')
      out += '<sent id="'+ta[0].to_s+'">'+"\n"
      ta[1].to_s.split(' ').each{|tw|
        out += tw + "\n"
      }
      out += "</sent>\n"
    }
    tmpf = Tempfile.new('tecu')
    tmpf.write(out)
    out2 = `/opt/majka_pipe/majka-czech-nl.sh < #{tmpf.path}`
    tmpf.close
    file = File.new('/var/lib/deb-server/termcheck/tecu_terms.vert','w')
    out2.gsub!("<s>\n",'')
    out2.gsub!("</s>\n", '')
    file.puts out2
    file.close
    response.body = out2
    response['Content-Type'] = 'text/plain; charset=utf-8'
  when 'get_top'
    res = @dict.get_top
    if request.query['format'].to_s == 'wsdl'
      response.body = hash_to_wsdl(res)
      response['Content-Type'] = 'application/xml; charset=utf-8'
    else
      response.body = res.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    end
  when 'get_tree'
    if @dict.tree
      res = @dict.tree
    else
      res = @dict.get_top2
      @dict.tree = res
    end
    response.body = res.to_json
    response['Content-Type'] = 'application/json; charset=utf-8'
  when 'gettrans'
    term = request.query['term'].to_s.force_encoding("UTF-8")
    lang = request.query['language'] or 'en'
    response['Content-Type'] = 'application/json; charset=utf-8'
    output = @dict.get_trans(term, lang)
    response.body = output.to_json
  when 'get_settings'
    response['Content-Type'] = 'application/json; charset=utf-8'
    output = @dict_array['settings'].get_settings
    response.body = output.to_json
  when 'save_settings'
    if request.query['data'].to_s != ''
      begin
        data_hash = JSON.parse(request.query['data'].to_s)
      rescue
        res = {'error'=>'JSON parsing error'}
      end
      if data_hash != nil 
        @dict_array['settings'].save_settings(data_hash)
        res = @dict_array['settings'].get_settings
      end
    else
      res = {'error'=>'no data'}
    end
    response['Content-Type'] = 'application/json; charset=utf-8'
    response.body = res.to_json
  when 'getdoc'
    key = URI.escape(request.query['id'])
    transform = request.query['tr'].to_s
    format = request.query['format'].to_s
    perm = @dict.get_perm(user)
    begin
      reswork = ''
      if request.query['timestamp'].to_s != ''
        res = @dict_array['tecuhist'].get(key+'_'+request.query['timestamp'].to_s)
      else
        case request.query['type'] 
        when 'del'
          res = @dict_array['tecudel'].get(key)
        when 'edit'
          locked_by = @dict_array['notification'].who_locked(key)
          $stderr.puts 'LOCKED BY '+locked_by.to_s
          if locked_by != false and locked_by != user
            response.body = '{"entry":{"terms":{"term":[]},"error_msg":"'+locked_by.to_s+'"}}'
            response['Content-Type'] = 'application/json; charset=utf-8'
            return response
          end
          if locked_by == false
            @dict_array['notification'].lock_entry(key, user, Time.now.to_i)
          end
          res = @dict_array['tecuwork'].get_doc(key)
          if res == ''
            res = @dict.get_doc(key) 
          else
            res = res.sub('schvaleno="false"','').sub('schvaleno="true"','').sub('<entry','<entry schvaleno="false"')
          end
        else 
          res = @dict.get_doc(key)
          reswork = @dict_array['tecuwork'].get_doc(key)
          res = reswork if res == '' and reswork != '' and user != 'read'
        end
      end
      if res != ''
        if transform != '' or ["pdf","pdf2","rtf","rtf2"].include?(format)
          response["Content-type"] = "text/html; charset=utf-8"
          response["Content-type"] = "text/xml; charset=utf-8" if transform == 'skos'
          doc = @dict.load_xml_string(res.to_s)
          if transform == 'preview' or ["pdf","pdf2","rtf","rtf2"].include?(format)
          doc.find('/entry/defs/def/math').each{|de|
            if de.content.to_s != ''
              img = @dict.math_to_png(de.content.to_s)
              newbase = XML::Node.new('math_base64')
              de.parent << newbase
              newbase.content = Base64.strict_encode64(File.read(img))
            end
          }
          doc.find('/entry/defs/def/table').each{|de|
            if de.content.to_s != ''
              img = @dict.table_to_png(de.content.to_s)
              newbase = XML::Node.new('table_base64')
              de.parent << newbase
              newbase.content = Base64.strict_encode64(File.read(img))
            end
          }
          end
          if format == 'pdf' or format == 'pdf2'
            response["Content-type"] = "application/pdf; name=tecu"+key+".pdf"
            response["Content-Disposition"] = "inline; filename=tecu"+key+".pdf"
            params = [["local",'"true"']]
            params << ["type",'"short"'] if format == 'pdf2'
            html = @dict.apply_transform("preview", doc.to_s, params).force_encoding('UTF-8')
            $stderr.puts html
            require 'pdfkit'
            kit = PDFKit.new(html, :page_size=>'A4', :encoding=>'UTF-8')
            pdf = kit.to_pdf
            res = pdf
          elsif format == 'rtf' or format == 'rtf2'
            require 'html2rtf'
            params = []
            params << ["type",'"short"'] if format == 'rtf2'
            html = @dict.apply_transform("preview", doc.to_s, params).force_encoding('UTF-8')
            document = RTF::Document.new(RTF::Font.new(RTF::Font::ROMAN, 'Times New Roman'))
            noko = Nokogiri::HTML::Document.parse(html)
            document = noko.xpath("/html/body/*").to_rtf(document)
            res = document.to_rtf
            response["Content-type"] = "application/rtf; name=tecu"+key+".rtf"
            response["Content-Disposition"] = "inline; filename=tecu"+key+".rtf"
          else
            res = @dict.apply_transform(transform, doc.to_s, [ [ 'perm', '"'+perm+'"'], [ 'type', '"'+request.query['type'].to_s+'"'], ['servername','"'+@servername.gsub('https','http')+'"'] ])
          end
        elsif request.query['json'].to_s == '1' or format == 'json'
          response['Content-Type'] = 'application/json; charset=utf-8'
          doc = @dict.load_xml_string(res.to_s)
          doc.find('/entry/defs').first.remove! if doc.find('/entry/defs').to_a.size == 1 and doc.find('/entry/defs/def').to_a.size == 0
          doc.find('/entry/defs/def/html').each{|de| de.remove!}
          doc.find('/entry/defs/def/text').each{|de|
            htmlel = XML::Node.new('html')
            newcontent = de.to_s.sub('<text>','').sub('</text>','').sub('<text/>','')
            newcontent.gsub!(/<\/?a[^>]*>/,'') if request.query['type'].to_s == 'edit'
            htmlel.content = newcontent
            de.parent << htmlel
            dec = de.content.to_s
            de.content = dec
          }
          doc.find('/entry/defs/def/math').each{|de|
            if de.content.to_s != ''
              img = @dict.math_to_png(de.content.to_s)
              newbase = XML::Node.new('math_base64')
              de.parent << newbase
              newbase.content = Base64.strict_encode64(File.read(img)) unless img.nil?
            end
          }
          doc.find('/entry/defs/def/table').each{|de|
            if de.content.to_s != ''
              img = @dict.table_to_png(de.content.to_s)
              newbase = XML::Node.new('table_base64')
              de.parent << newbase
              newbase.content = Base64.strict_encode64(File.read(img)) unless img.nil?
            end
          }
          doc.find('/entry/defs/def').each{|de|
            de << XML::Node.new('table') if de.find('table').size == 0
            de << XML::Node.new('table_base64') if de.find('table_base64').size == 0
            de << XML::Node.new('math') if de.find('math').size == 0
            de << XML::Node.new('math_base64') if de.find('math_base64').size == 0
          }
          hash = CobraVsMongoose.xml_to_hash(doc.to_s)
          #normalizace JSON
          hash = normalize_json(hash)

          #wait_for prevzit z pracovni verze
          if reswork != ''
            docwork = @dict.load_xml_string(reswork)
            hash['entry']['@wait_for'] = docwork.root['wait_for'].to_s
            hash['entry']['@status'] = docwork.root['status'].to_s if docwork.root['wait_for'].to_s != ''
            hash['entry']['notes'] = {'note'=>[]}
            docwork.find('//notes/note').each{|note|
              hash['entry']['notes']['note'] << CobraVsMongoose.xml_to_hash(note.to_s)['note']
            }
          end
          if hash['entry']['@wait_for'].nil? or hash['entry']['@wait_for'].to_s == ''
            waitar = ['asist_hl','tajemnik','revizor','obor_redaktor','obor_revizor','hl_redaktor']
          else
            waitar = hash['entry']['@wait_for'].to_s.split(',') unless hash['entry']['@wait_for'].nil?
            waitar << 'hl_redaktor'
          end
          hash['entry']['@wait_for'] = waitar


          res = hash.to_json
        else
          response["Content-type"] = "text/xml; charset=utf-8"
        end
        response.body = res
      end
    rescue => e
      $stderr.puts e
      $stderr.puts e.backtrace
      if request.query['format'].to_s == 'wsdl'
        response.body = wsdl_message({'result'=>'error','message'=>e})
        response['Content-Type'] = 'application/xml; charset=utf-8'
      elsif request.query['format'].to_s == 'json'
        response.body = '{"error":true, "msg":"'+e.to_s+'"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      else
        response.body = e
      end
    end
  when 'delete'
    id = request.query['id'].to_s.force_encoding("UTF-8")
    @dict_array['notification'].unlock_entry(id, user)
    if @dict_array['settings'].get_settings['dict']['simpleprocess'] == 'true'
      begin
        xml = @dict.get(id)
        @dict_array['tecudel'].update(id, xml)
        @dict.delete(id)
        response.body = '{"ok":true, "msg":"deleted", "id": "'+id+'"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        response.body = '{"error":true, "msg":'+e+'}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    else
      xml = @dict.get(id)
      if xml != ''
        doc = @dict.load_xml_string(xml)
        doc.root['wait_for'] = 'tajemnik'
        doc.root['status'] = 'delete_tajemnik'
        doc.root << XML::Node.new('notes') if doc.find('notes').length == 0
        newnote = XML::Node.new('note')
        newnote['author'] = user
        newnote['time'] = Time.now.strftime('%Y-%m-%d %H:%M:%S')
        newnote['number'] = (doc.find('notes/note').size + 1).to_s
        newnote.content = 'navrh na zrušení termínu'
        doc.find('notes').first << newnote
        @dict_array['tecuwork'].update(id, doc.to_s)
        term_cz = doc.find('terms/term[@lang="cz"]').first.content.to_s unless doc.find('terms/term[@lang="cz"]').first.nil?
        @dict_array['notification'].prepare_notif(id, term_cz, 'tajemnik', 'navrh na zrušení termínu', user, @dict_array['tecu_user'])
      end
      response.body = '{"ok":true, "msg":"saved", "id": "'+id+'"}'
      response['Content-Type'] = 'application/json; charset=utf-8'
    end
  when 'undelete'
    if @dict.get_perm(user) != 'w'
      if request.query['format'].to_s == 'wsdl'
        response.body = wsdl_message({'result'=>'error','message'=>"nemáte oprávnění k editaci"})
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = '{"error":true, "msg":"nemáte oprávnění k editaci"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
      return response
    end
    id = request.query['id'].to_s
    begin
      xml = @dict_array['tecudel'].get(id)
      @dict_array['tecu'].update(id, xml)
      @dict_array['tecudel'].delete(id)
      if request.query['format'].to_s == 'wsdl'
        response.body = wsdl_message({'result'=>'undeleted','id'=>id})
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = '{"ok":true, "msg":"undeleted", "id": "'+id+'"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    rescue => e
      $stderr.puts e
      $stderr.puts e.backtrace
      if request.query['format'].to_s == 'wsdl'
        response.body = wsdl_message({'result'=>'error','message'=>e})
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = '{"error":true, "msg":'+e+'}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    end

  when 'gethypcand'
    term = request.query['term']
    response['Content-Type'] = 'application/json; charset=utf-8'
    output = @dict.get_hyper_cand(term.force_encoding("UTF-8"))
    response.body = output.to_json

  when 'gettermcand'
    response['Content-Type'] = 'application/json; charset=utf-8'
    output = @dict.get_term_cand
    response.body = output.to_json

  when 'updatetermcand'
    response['Content-Type'] = 'application/json; charset=utf-8'
    output = @dict.update_term_cand
    response.body = output.to_json

  when 'getconc'
    term = request.query['term']
    response['Content-Type'] = 'application/json; charset=utf-8'
    output = @dict.get_concordance(term.force_encoding("UTF-8"))
    response.body = output.to_json
  when 'ie'
    res = '<div id="mainBody"><ul><li><a href="/tecu?action=download_all" target="_blank">Export XML</a>'
    res += '<li><a href="/tecu?action=download_all&tr=skos" target="_blank">Export SKOS</a>'
    res += '<li><a href="/tecu?action=export_csv" target="_blank">Export CSV</a>'
    res += '<li><a href="/tecu?action=export_tree" target="_blank">Export strom termínů</a>'
    res += '<li>Import: <form method="POST" target="_blank"  enctype="multipart/form-data" action="/tecu"><input type="hidden" name="action" value="import_xml"> <input name="file" type="file"><select name="format"><option value="xml">XML<option value="skos">SKOS<option value="csv">CSV</select><label><input type="checkbox" name="delete"/> smazat existující data</label> <input type="submit" value="Import"/></form>'
    res += '<li>Vložit dokument do korpusu: <form method="POST" target="_blank"  enctype="multipart/form-data" action="/tecu"><input type="hidden" name="action" value="import_corpora"> <input name="file" type="file"><input type="submit" value="Import"/></form>'
    response['Content-Type'] = 'application/json; charset=utf-8'
    response.body = {"__html"=>res}.to_json
  when 'import_corpora'
    data_file = request.query['file']
    data = ''
    data_file.each_data{|d|
      data += d
    }
    tmpfile = Tempfile.new('tecuimport')
    tmpfile.write(data)
    tmpfile.close
    $stderr.puts tmpfile.path
    logfile = '/var/log/deb-server/tecucorp' + Time.now.strftime('%Y%m%d-%H%M%S') +'.log'
    Thread.new{system("/var/lib/deb-server/corpora/scripts/add_new_document.sh "+tmpfile.path+" >"+logfile )}
    response.set_redirect(WEBrick::HTTPStatus::SeeOther, '/tecu?action=showlog&type=import&log=' + File.basename(logfile))
    response.body
  when 'import_xml'
    data_file = request.query['file']
    data = ''
    data_file.each_data{|d|
      data += d
    }
    if request.query['format'].to_s == 'skos'
      data = @dict.apply_transform('fromskos', data, [])
      data = data.sub("</tecu>","\n</tecu>").gsub('><entry',">\n<entry")
    end
    if request.query['format'].to_s == 'csv'
      t = CSV.parse(data.force_encoding('UTF-8'), :col_sep => "\t")
      data = ''
      termid = nil
      termhash = {}
      t.each{|tr|
        next if tr[0].to_s.strip == 'id'
        if tr[0].to_s != ''
          if termid != nil
            #ukoncit term
            data += finish_csv_term(termhash)
          end
          #zacit term
          termhash = {'id'=>tr[0].to_s.strip,'terms'=>[],'defs'=>[], 'abbr'=>tr[1].to_s.strip, 'refs'=>[], 'hyp'=>[], 'syn'=>[], 'ant'=>[]}
          termid = tr[0].to_s.strip
        end
        if tr[2].to_s != ''
          termhash['terms'] << {'term'=>tr[2].to_s.strip, 'lang'=>tr[3].to_s.strip}
        end
        if tr[4].to_s != ''
          termhash['defs'] << {'def'=>tr[4].to_s.strip, 'lang'=>tr[5].to_s.strip, 'obor'=>tr[6].to_s.strip, 'zdroj'=>tr[7].to_s.strip}
        end
        if tr[8].to_s != ''
          termhash['hyp'] << tr[8].to_s.strip
        end
        if tr[9].to_s != ''
          termhash['syn'] << tr[9].to_s.strip
        end
        if tr[10].to_s != ''
          termhash['ant'] << tr[10].to_s.strip
        end

      }
      if termid!=nil
        data += finish_csv_term(termhash)
      end

    end
    $stderr.puts data
    tmpfile = Tempfile.new('tecuimport')
    tmpfile.write(data)
    tmpfile.close
    $stderr.puts tmpfile.path
    logfile = '/var/log/deb-server/tecuimport' + Time.now.strftime('%Y%m%d-%H%M%S') +'.log'
    delete = false
    delete = true if request.query['delete'].to_s == 'on'
    Thread.new{import("tecu", tmpfile.path, logfile, "/entry/@id", "entry", delete, true) }
    response.set_redirect(WEBrick::HTTPStatus::SeeOther, '/tecu?action=showlog&type=import&log=' + File.basename(logfile))
    response.body
  when 'showlog'
    logfile = request.query['log'].to_s
    type = request.query['type'].to_s
    result = %x[tail -n 20 /var/log/deb-server/#{logfile}]
    lastline = ''
    result.to_s.split("\n").each{|line| lastline=line}

    response['Content-Type'] = 'text/html; charset=utf-8'
    response.body = '<html><head><meta http-equiv="Refresh" content="5"/></head><body><pre>'+result.to_s+'</pre>'
    if type == 'import' and lastline[0,8] == 'IMPORT END'
      response.body += '<p>Import should be complete, see log for more details.</p>'
    end
    response.body += '</body></html>'

  when 'mathpng'
    img = @dict.math_to_png(request.query['math'].to_s)
    if img.nil?
      response.body = File.read('/var/lib/deb-server/files/tecu/cross.png')
    else
      response.body = File.read(img)
    end
    response["Content-type"] = "image/png"
  when 'tablepng'
    img = @dict.table_to_png(request.query['table'].to_s)
    if img.nil?
      response.body = File.read('/var/lib/deb-server/files/tecu/cross.png')
    else
      response.body = File.read(img)
    end
    response["Content-type"] = "image/png"

  when 'import'
      $stderr.puts 'UPLOAD'
      data_file = request.query['input_file']
      config_type = request.query['config_type']
      configuration = request.query['configuration']
      multif = request.query['number_of_files'].to_i
      input_file_path = '/tmp/tecu_import_input_file.txt'
      conf_file_path = '/tmp/tecu_import_config_file.py'
      out_file_path = '/tmp/tecu_import_output_file.xml'
      if config_type == 'other' and configuration.to_s == ''
          response.body = '{"error": true, "msg": "missing configuration"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
          response.status = 500
          return response
      elsif config_type == 'other'
          cnf = File.open(conf_file_path, 'r')
          cnf.write(configuration.to_s)
          cnf.close
      elsif config_type == 'html'
          conf_file_path = @import_path + '/html_conf.py'
      elsif config_type == 'csv'
          conf_file_path = @import_path + '/csv_conf.py'
      else
          response.body = '{"error": true, "msg": "unknown configuration type"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
          response.status = 500
          return response
      end
      if data_file.nil?
          response.body = '{"error": true, "msg": "missing input file"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
          response.status = 500
          return response
      else
          inf = File.open(input_file_path, "wb")
          data_file.each_data{|d|
              inf.syswrite d
          }
          inf.close
      end
      if @dict.get_perm(user) != 'w'
          response.body = '{"error": true, "msg": "access error"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
          response.status = 403
          return response
      else
          $stderr.puts "python import.py -i #{input_file_path} -c #{conf_file_path} > #{out_file_path}"
          File.open(input_file_path).delete()
          response.body = '{"msg": "ok"}'
      end

  when 'stats'
    if request.query['format'].to_s == 'wsdl'
      response.body = array_to_wsdl([@dict.get_stats])
      response['Content-Type'] = 'application/xml; charset=utf-8'
    else
      response.body = @dict.get_stats.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    end
    return response
  
  when 'history'
    xml = @dict.get_history
    response["Content-type"] = "text/html; charset=utf-8"
    response.body = @dict.apply_transform('history', xml)

  when 'highlight'
    result = `/var/www/termcheck/run.cgi "#{request.query['text']}" "#{@servername}"`
    response.body = result
    response["Content-type"] = "text/html; charset=utf-8"

  when 'save'
    if @dict.get_perm(user) != 'w'
      if request.query['format'].to_s == 'wsdl'
        response.body = wsdl_message({'result'=>'error','message'=>"nemáte oprávnění k editaci"})
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = '{"error":true, "msg":"nemáte oprávnění k editaci"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
      return response
    end
    id = request.query['id'].to_s.force_encoding('UTF-8')
    @dict_array['notification'].unlock_entry(id, user)
    diffnotes = []
    diffchanges = []
    new_entry = false
    if id == '' and request.query['data'].to_s != ''
      id = @server.info.get_next_id('tecu')
      $stderr.puts 'NEW ID '+id
      diffnotes << 'nový záznam'
      diffchanges << 'entry'
      new_entry = true
    end
    if id != '' and request.query['data'].to_s != ''
      #begin
        data_hash = {'entry'=> JSON.parse(request.query['data'])}
        #if data_hash['entry']['@abbrev'] != nil
        #  if data_hash['entry']['@abbrev'] == 'true'
        #    data_hash['entry']['@abbrev'] = true
        #  else
        #    data_hash['entry']['@abbrev'] = false
        #  end
        #end
        data_hash['entry']['@id'] = id
        current_time = Time.now.to_i.to_s
        data_hash['entry']['@timestamp'] = current_time
        unless data_hash['entry']['hyper'].nil?
          data_hash['entry']['hyper'].each{|h|
            @dict.hypers << h['@id']
          }
        end
        terms = []
        term_cz = ''
        terms_lang = data_hash['entry']['@lang']
        # remove multiple spaces and strip terms
        data_hash['entry']['terms']['term'].each{|x|
            x['$'] = x['$'].squeeze(' ').strip
            terms << x['$']
            next if x['@lang'] != 'cz'
            term_cz = x['$']
            @dict.search({'exact'=>x['$']}).each{|y|
                if y['head'].squeeze(' ').strip == x['$'] and id.to_i != y['id'].to_i
                    #response.status = 500
                    #response['Content-Type'] = 'text/plain; charset=utf-8'
                    msg = "heslo #{y['head']} (#{y['id']}) již v tezauru existuje"
                    response.body = '{"error":true, "msg":"'+msg+'"}'
                    response['Content-Type'] = 'application/json; charset=utf-8'
                    return response
                end
            }
        }
        # fail when adding an existing term
        # remove reflexive hyperonyms
        filtered_hypers = []
        unless data_hash['entry']['hyper'].nil?
        data_hash['entry']['hyper'].each{|x|
            if x['@id'].to_i != id.to_i and not @dict.get_path(x['@id']).flatten.include?(x['@id'])
                filtered_hypers << x
            end
        }
        data_hash['entry']['hyper'] = filtered_hypers
        end
        # remove reflexive sees
        filtered_sees = []
        #data_hash['entry']['sees']['see'].each{|x|
        #    if not terms.include?(x['$']) or x['@lang'] != terms_lang
        #        filtered_sees << x
        #    end
        #}
        #data_hash['entry']['sees']['see'] = filtered_sees
        # remove reflexive alsos
        #filtered_alsos = []
        #data_hash['entry']['alsos']['also'].each{|x|
        #    if not terms.include?(x['$']) or x['@lang'] != terms_lang
        #        filtered_alsos << x
        #    end
        #}
        #data_hash['entry']['alsos']['also'] = filtered_alsos
        origxml = @dict.get(id)
        workxml = @dict_array['tecuwork'].get(id)
        oldxml = (workxml != '')? workxml : origxml
        data_hash['entry']['meta'] = {}
        data_hash['entry']['notes'] = {}
        if oldxml != ''
          #prevzit historii hesla
          old_hash = CobraVsMongoose.xml_to_hash(oldxml)
          diffnotes, diffchanges = compare_version(normalize_json(old_hash), data_hash)
          unless old_hash['entry']['meta'].nil?
              data_hash['entry']['meta'] = old_hash['entry']['meta']
          end
          unless old_hash['entry']['notes'].nil?
            if old_hash['entry']['notes']['note'].is_a?(Array)
              data_hash['entry']['notes']['note'] = old_hash['entry']['notes']['note']
            end
            if old_hash['entry']['notes']['note'].is_a?(Hash)
              data_hash['entry']['notes']['note'] = [old_hash['entry']['notes']['note']]
            end
          end
        end
        unless diffchanges.any?{|s| s.include?('changed_obor=')}
          diffchanges << 'changed_obor='+data_hash['entry']['defs']['def'][0]['@obor'].to_s if data_hash['entry']['defs'] != nil and data_hash['entry']['defs']['def'] != nil
        end
        if data_hash['entry']['meta'] == {}
          data_hash['entry']['meta']['author'] = {'$'=>user}
          data_hash['entry']['meta']['creation_time'] = {'$'=>Time.now.strftime('%Y-%m-%d %H:%M:%S')}
        end
        data_hash['entry']['meta']['update_time'] = {'$'=>Time.now.strftime('%Y-%m-%d %H:%M:%S')}
        data_hash['entry']['notes']['note'] = [] if data_hash['entry']['notes']['note'].nil?
        if data_hash['entry']['schvaleni'].to_s == 'true'
          data_hash['entry']['schvaleni'] = 'schvaleno'
          schvalene_role = data_hash['entry']['@schvaleno_role'].to_s.split(';')
          @dict.get_users(@dict_array['tecu_user'], user)[0]['roles'].each{|sr|
            schvalene_role << sr + '=' + user if sr != "hl_redaktor"
          }
          data_hash['entry']['@schvaleno_role'] = schvalene_role.join(';')
        else
          data_hash['entry']['schvaleni'] = 'zamitnuto'
        end
        #status?
        wait_for_ar = []
        wait_for_ar = data_hash['entry']['@wait_for'] unless data_hash['entry']['@wait_for'].nil?
        old_status = data_hash['entry']['@status'].to_s
        old_status = 'before_new' if new_entry
        data_hash['entry']['schvaleni'] = 'schvaleno' if old_status == 'vklada_preklad' or old_status == 'before_new'
        new_status, wait_for = next_step(diffchanges, old_status, data_hash['entry']['schvaleni'].to_s, wait_for_ar.join(','), data_hash['entry']['notes']['note'], @dict.get_users(@dict_array['tecu_user'], user)[0]['roles'], data_hash)
          $stderr.puts data_hash['entry']['@status'].to_s + '/' + old_status + ' - ' + data_hash['entry']['schvaleni'].to_s
          $stderr.puts new_status + ' - ' + wait_for
          data_hash['entry']['@status'] = new_status
          data_hash['entry']['@wait_for'] = wait_for
          data_hash['entry']['@schvaleno_role'] = '' if new_status == 'public' or new_status == 'deleted' or data_hash['entry']['schvaleni'] == 'zamitnuto'
          #pridat poznamku
          #new_comment,new_xpath = data_hash['entry']['comment'].split(';xpath:')
          diffnotes << 'zveřejnit záznam' if new_status == 'public'
          diffnotes << 'odstranit záznam' if new_status == 'deleted'
          if new_status != 'zmena_entry' and old_status != 'vklada_preklad'
            diffnotes << 'schváleno' if data_hash['entry']['schvaleni'] == 'schvaleno' 
            diffnotes << 'zamítnuto' if data_hash['entry']['schvaleni'] == 'zamitnuto' 
          end
          schvalila_role = ''
          user_roles = @dict.get_users(@dict_array['tecu_user'], user)[0]['roles']
          wait_for_ar.each{|wf|
            schvalila_role = wf if user_roles.include?(wf) and not ['prekladatel','recenzent_preklad','obor_redaktor','obor_revizor'].include?(wf)
          }
          schvalila_role = user_roles[0] if schvalila_role == ''
          select_role = @dict_array['settings'].get_settings['roles'].select{|r| r['role']==schvalila_role}
          diffnotes << select_role[0]['label'] unless select_role.nil?
          unless data_hash['entry']['comment'].nil?
            diffnotes << data_hash['entry']['comment'].to_s
            data_hash['entry'].delete('comment')
          end
          newnote = {
            '@author' => user,
            '@time' => Time.now.strftime('%Y-%m-%d %H:%M:%S'),
            '@number' => (data_hash['entry']['notes']['note'].size+1),
            '@changes' => diffchanges.uniq.join(';'),
            '$' => diffnotes.join(', ')
          }
          @dict_array['notification'].prepare_notif(id, term_cz, wait_for, diffnotes.join(', '), user, @dict_array['tecu_user'])
          data_hash['entry']['notes']['note'] << newnote

          #zkontrolovat pred schvaleni
          if data_hash['entry']['@schvaleno_role'].to_s != ''
            schvaleni_user = {}
            data_hash['entry']['@schvaleno_role'].to_s.split(';').each{|sr|
              sra = sr.split('=')
              if schvaleni_user[sra[0]].nil?
                schvaleni_user[sra[0]] = [sra[1]]
              else
                schvaleni_user[sra[0]] << sra[1] 
              end
            }
            while not schvaleni_user[wait_for].nil? and new_status != 'public' and new_status != 'deleted'
              author = schvaleni_user[wait_for].uniq.join(',')
              role_name = @dict_array['settings'].get_settings['roles'].select{|r| r['role']==wait_for}[0]['label']
              new_status, wait_for = next_step([], data_hash['entry']['@status'].to_s, 'schvaleno', data_hash['entry']['@wait_for'], data_hash['entry']['notes']['note'], @dict.get_users(@dict_array['tecu_user'], user)[0]['roles'], data_hash)
              $stderr.puts new_status + ' - ' + wait_for
              data_hash['entry']['@status'] = new_status
              data_hash['entry']['@wait_for'] = wait_for
              newnote = {
                '@author' => author,
                '@time' => Time.now.strftime('%Y-%m-%d %H:%M:%S'),
                '@number' => (data_hash['entry']['notes']['note'].size+1),
                '$' => 'schváleno, ' + role_name.to_s
              }
              data_hash['entry']['notes']['note'] << newnote
              @dict_array['notification'].prepare_notif(id, term_cz, wait_for, 'schvaleno', author, @dict_array['tecu_user'])
            end
          end
          data_hash['entry']['@schvaleno_role'] = '' if new_status == 'public' or new_status == 'deleted' or data_hash['entry']['schvaleni'] == 'zamitnuto'

          data_hash['entry'].delete('paths')
          #Thread.new{ @dict.highlight_def(id, xml, @servername, current_time) }
          if new_status == 'public' #data_hash['entry']['schvaleni'] == 'schvaleno'
            data_hash['entry'].delete('schvaleni')
            # pridat odkazy na terminy
            if data_hash['entry']['defs'] != nil and data_hash['entry']['defs']['def'] != nil
              data_hash['entry']['defs']['def'].each{|defhash|
                if defhash['text'] != nil and defhash['text']['$'] != nil and defhash['@lang'] == 'cz'
                  deftext = defhash['text']['$'].to_s.gsub(/<(\/?[^>]*)>/,'[\1]')
                  newtext = add_term_links(deftext, id)
                  newtext.gsub!(/\[ ([^ ]*) \] ?/,'<\1>')
                  newtext.gsub!(/ ?\[ \/ ([^ ]*) \]/,'</\1>')
                  newtext.gsub!('( ', '(')
                  newtext.gsub!(' )', ')')
                  newtext.gsub!(' ;', ';')
                  newtext.gsub!(' ,', ',')
                  defhash['text'] = {'$' => newtext}
                end
              }
            end
            xml = CobraVsMongoose.hash_to_xml(data_hash).force_encoding('UTF-8')
            $stderr.puts xml
            xml = fix_xml(xml)
            $stderr.puts xml
            @dict.update(id, xml)
            @dict_array['tecuwork'].delete(id)
            add_history(id, xml)
            #@dict.tree = @dict.get_top2
            response.body = '{"ok":true, "msg":"saved", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          elsif new_status == 'deleted'
            xml = CobraVsMongoose.hash_to_xml(data_hash).force_encoding('UTF-8')
            @dict_array['tecudel'].update(id, xml)
            @dict.delete(id)
            @dict_array['tecuwork'].delete(id)
            response.body = '{"ok":true, "msg":"deleted", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          elsif new_status == 'zmena_zamitnuta'
            if origxml != ''
              orig_hash = normalize_json(CobraVsMongoose.xml_to_hash(origxml))
              orig_hash['entry']['notes'].delete('note') unless orig_hash['entry']['notes'].nil?
              orig_hash['entry']['notes'] = data_hash['entry']['notes']
              orig_hash['entry']['@status'] = 'public'
              orig_hash['entry']['@wait_for'] = ''
              xml = CobraVsMongoose.hash_to_xml(orig_hash).force_encoding('UTF-8')
              @dict.update(id, xml)
            end
            @dict_array['tecuwork'].delete(id)
            response.body = '{"ok":true, "msg":"saved", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          elsif data_hash['entry']['schvaleni'] == 'zamitnuto' and not old_hash.nil? and new_status != 'zmena_entry'
            old_hash['entry']['notes'].delete('note') unless old_hash['entry']['notes'].nil?
            old_hash['entry']['notes'] = data_hash['entry']['notes']
            old_hash['entry']['@status'] = new_status
            old_hash['entry']['@wait_for'] = wait_for
            xml = CobraVsMongoose.hash_to_xml(old_hash).force_encoding('UTF-8')
            @dict_array['tecuwork'].update(id, xml)
            response.body = '{"ok":true, "msg":"saved working", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          else
            data_hash['entry'].delete('schvaleni')
            xml = CobraVsMongoose.hash_to_xml(data_hash).force_encoding('UTF-8')
            @dict_array['tecuwork'].update(id, xml)
            response.body = '{"ok":true, "msg":"saved working", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          end
        #rescue => e
        #  $stderr.puts e
        #  $stderr.puts e.backtrace
        #  if request.query['format'].to_s == 'wsdl'
        #    response.body = wsdl_message({'result'=>'error','message'=>e})
        #    response['Content-Type'] = 'application/xml; charset=utf-8'
        #  else
        #    response.body = '{"error":true, "msg":'+e+'}'
        #    response['Content-Type'] = 'application/json; charset=utf-8'
        #  end
        #end
      else
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>"Nezadáno ID nebo data"})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"error":true, "msg":"Nezadáno ID nebo data"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end
      return response
    when 'return_to_rework'
      if request.query["id"].to_s != ""
        id = request.query["id"].to_s.force_encoding('UTF-8')
        workxml = @dict_array['tecuwork'].get(id)
        data_hash = normalize_json(CobraVsMongoose.xml_to_hash(workxml))
        $stderr.puts data_hash
        if data_hash.nil? or data_hash == {}
          @dict_array['notification'].clear_notif(id)
          response.body = '{"ok":true, "msg":"saved", "id": "'+id+'"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
          return response
        end
        term_cz = ''
        unless data_hash['entry']['terms']['term'].nil?
          data_hash['entry']['terms']['term'].each{|x|
            next if x['@lang'] != 'cz'
            term_cz = x['$']
          }
        end
        data_hash['entry']['notes']['note'] = [] if data_hash['entry']['notes']['note'].nil?
        data_hash['entry']['meta'] = {} if data_hash['entry']['meta'].nil?
        data_hash['entry']['meta']['update_time'] = {'$'=>Time.now.strftime('%Y-%m-%d %H:%M:%S')}
        data_hash['entry']['schvaleni'] = 'vratit'
        #status?
        data_hash['entry']['@status'] = 'kontrola_hlavni'
        data_hash['entry']['@wait_for'] = 'hl_redaktor'
        data_hash['entry']['@schvaleno_role'] = '' 
        newnote = {
          '@author' => user,
          '@time' => Time.now.strftime('%Y-%m-%d %H:%M:%S'),
          '@number' => (data_hash['entry']['notes']['note'].size+1),
          '$' => 'vráceno k přepracování '
        }
        @dict_array['notification'].prepare_notif(id, term_cz, 'hl_redaktor', 'vráceno k přepracování', user, @dict_array['tecu_user'])
        data_hash['entry']['notes']['note'] << newnote
            data_hash['entry'].delete('schvaleni')
            xml = CobraVsMongoose.hash_to_xml(data_hash).force_encoding('UTF-8')
            @dict_array['tecuwork'].update(id, xml)
            response.body = '{"ok":true, "msg":"saved working", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
      end
    when 'precteno'
      if request.query["id"].to_s != ""
        nid = request.query["id"].to_s.force_encoding('UTF-8')
        @dict.read_notif(nid)
      end
      response.body = '{"ok":true, "msg":"saved", "nid": "'+nid+'"}'
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'schvaleno'
      if request.query["id"].to_s != ""
        id = request.query["id"].to_s.force_encoding('UTF-8')
        workxml = @dict_array['tecuwork'].get(id)
        @dict_array['notification'].unlock_entry(id, user)
        data_hash = normalize_json(CobraVsMongoose.xml_to_hash(workxml))
        $stderr.puts data_hash
        if data_hash.nil? or data_hash == {}
          @dict_array['notification'].clear_notif(id)
          response.body = '{"ok":true, "msg":"saved", "id": "'+id+'"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
          return response
        end
        term_cz = ''
        unless data_hash['entry']['terms']['term'].nil?
          data_hash['entry']['terms']['term'].each{|x|
            next if x['@lang'] != 'cz'
            term_cz = x['$']
          }
        end
        waitar = data_hash['entry']['@wait_for'].to_s.split(',') unless data_hash['entry']['@wait_for'].nil?
        data_hash['entry']['@wait_for'] = waitar
        if data_hash['entry']['notes'].nil?
          data_hash['entry']['notes'] = {'note'=>[]}
        else
          if data_hash['entry']['notes']['note'].is_a?(Hash)
            data_hash['entry']['notes']['note'] = [data_hash['entry']['notes']['note']]
          end
        end
        data_hash['entry']['notes']['note'] = [] if data_hash['entry']['notes']['note'].nil?
        data_hash['entry']['schvaleni'] = 'schvaleno'
        data_hash['entry']['meta'] = {} if data_hash['entry']['meta'].nil?
        data_hash['entry']['meta']['update_time'] = {'$'=>Time.now.strftime('%Y-%m-%d %H:%M:%S')}
        schvalene_role = data_hash['entry']['@schvaleno_role'].to_s.split(';')
        @dict.get_users(@dict_array['tecu_user'], user)[0]['roles'].each{|sr|
          schvalene_role << sr + '=' + user if sr != "hl_redaktor"
        }
        data_hash['entry']['@schvaleno_role'] = schvalene_role.join(';')
        #status?
        $stderr.puts data_hash
        new_status, wait_for = next_step([], data_hash['entry']['@status'].to_s, data_hash['entry']['schvaleni'].to_s, data_hash['entry']['@wait_for'].join(','), data_hash['entry']['notes']['note'], @dict.get_users(@dict_array['tecu_user'], user)[0]['roles'], data_hash)
        $stderr.puts data_hash['entry']['@status'].to_s + ' - ' + data_hash['entry']['schvaleni'].to_s
        $stderr.puts new_status + ' - ' + wait_for
        data_hash['entry']['@status'] = new_status
        data_hash['entry']['@wait_for'] = wait_for
        data_hash['entry']['@schvaleno_role'] = '' if new_status == 'public' or new_status == 'deleted' or data_hash['entry']['schvaleni'] == 'zamitnuto'
          schvalila_role = ''
          user_roles = @dict.get_users(@dict_array['tecu_user'], user)[0]['roles']
          waitar.each{|wf|
            schvalila_role = wf if user_roles.include?(wf) and not ['prekladatel','recenzent_preklad','obor_redaktor','obor_revizor'].include?(wf)
          }
          schvalila_role = user_roles[0] if schvalila_role == ''
          schvalila_role = @dict_array['settings'].get_settings['roles'].select{|r| r['role']==schvalila_role}[0]['label']
        newnote = {
          '@author' => user,
          '@time' => Time.now.strftime('%Y-%m-%d %H:%M:%S'),
          '@number' => (data_hash['entry']['notes']['note'].size+1),
          '$' => 'schváleno ('+request.query['comment'].to_s.force_encoding('UTF-8')+'), ' + schvalila_role
        }
        @dict_array['notification'].prepare_notif(id, term_cz, wait_for, 'schvaleno', user, @dict_array['tecu_user'])
        data_hash['entry']['notes']['note'] << newnote
        #predschvaleni
        if data_hash['entry']['@schvaleno_role'].to_s != ''
          schvaleni_user = {}
          data_hash['entry']['@schvaleno_role'].to_s.split(';').each{|sr|
            sra = sr.split('=')
            if schvaleni_user[sra[0]].nil?
              schvaleni_user[sra[0]] = [sra[1]]
            else
              schvaleni_user[sra[0]] << sra[1] 
            end
          }
          while not schvaleni_user[wait_for].nil? and new_status != 'public' and new_status != 'deleted'
            author = schvaleni_user[wait_for].uniq.join(',')
            role_name = @dict_array['settings'].get_settings['roles'].select{|r| r['role']==wait_for}[0]['label']
            new_status, wait_for = next_step([], data_hash['entry']['@status'].to_s, 'schvaleno', data_hash['entry']['@wait_for'], data_hash['entry']['notes']['note'], @dict.get_users(@dict_array['tecu_user'], user)[0]['roles'], data_hash)
            $stderr.puts new_status + ' - ' + wait_for
            data_hash['entry']['@status'] = new_status
            data_hash['entry']['@wait_for'] = wait_for
            newnote = {
              '@author' => author,
              '@time' => Time.now.strftime('%Y-%m-%d %H:%M:%S'),
              '@number' => (data_hash['entry']['notes']['note'].size+1),
              '$' => 'schváleno, ' + role_name.to_s
            }
            data_hash['entry']['notes']['note'] << newnote
            @dict_array['notification'].prepare_notif(id, term_cz, wait_for, 'schvaleno', author, @dict_array['tecu_user'])
          end
        end
        data_hash['entry']['@schvaleno_role'] = '' if new_status == 'public' or new_status == 'deleted' or data_hash['entry']['schvaleni'] == 'zamitnuto'
          if new_status == 'public' #data_hash['entry']['schvaleni'] == 'schvaleno'
            data_hash['entry'].delete('schvaleni')
            xml = CobraVsMongoose.hash_to_xml(data_hash).force_encoding('UTF-8')
            @dict.update(id, xml)
            @dict_array['tecuwork'].delete(id)
            add_history(id, xml)
            response.body = '{"ok":true, "msg":"saved", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          elsif new_status == 'deleted'
            data_hash['entry'].delete('schvaleni')
            xml = CobraVsMongoose.hash_to_xml(data_hash).force_encoding('UTF-8')
            @dict_array['tecudel'].update(id, xml)
            @dict.delete(id)
            @dict_array['tecuwork'].delete(id)
            response.body = '{"ok":true, "msg":"deleted", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          else
            data_hash['entry'].delete('schvaleni')
            xml = CobraVsMongoose.hash_to_xml(data_hash).force_encoding('UTF-8')
            @dict_array['tecuwork'].update(id, xml)
            response.body = '{"ok":true, "msg":"saved working", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          end
      end
    when 'zamitnuto'
      if request.query["id"].to_s != ""
        id = request.query["id"].to_s.force_encoding('UTF-8')
        workxml = @dict_array['tecuwork'].get(id)
        @dict_array['notification'].unlock_entry(id, user)
        data_hash = normalize_json(CobraVsMongoose.xml_to_hash(workxml))
        if data_hash.nil? or data_hash == {}
          @dict_array['notification'].clear_notif(id)
          response.body = '{"ok":true, "msg":"saved", "id": "'+id+'"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
          return response
        end
        term_cz = ''
        data_hash['entry']['terms']['term'].each{|x|
          next if x['@lang'] != 'cz'
          term_cz = x['$']
        }
        waitar = data_hash['entry']['@wait_for'].to_s.split(',') unless data_hash['entry']['@wait_for'].nil?
        data_hash['entry']['@wait_for'] = waitar
        if data_hash['entry']['notes'].nil?
          data_hash['entry']['notes'] = {'note'=>[]}
        else
          if data_hash['entry']['notes']['note'].is_a?(Hash)
            data_hash['entry']['notes']['note'] = [data_hash['entry']['notes']['note']]
          end
        end
        data_hash['entry']['notes']['note'] = [] if data_hash['entry']['notes']['note'].nil?
        data_hash['entry']['schvaleni'] = 'zamitnuto'
        data_hash['entry']['meta'] = {} if data_hash['entry']['meta'].nil?
        data_hash['entry']['meta']['update_time'] = {'$'=>Time.now.strftime('%Y-%m-%d %H:%M:%S')}
        schvalene_role = data_hash['entry']['@schvaleno_role'].to_s.split(';')
        @dict.get_users(@dict_array['tecu_user'], user)[0]['roles'].each{|sr|
          schvalene_role << sr + '=' + user if sr != "hl_redaktor"
        }
        data_hash['entry']['@schvaleno_role'] = schvalene_role.join(';')
        #status?
        new_status, wait_for = next_step([], data_hash['entry']['@status'].to_s, data_hash['entry']['schvaleni'].to_s, data_hash['entry']['@wait_for'].join(','), data_hash['entry']['notes']['note'], @dict.get_users(@dict_array['tecu_user'], user)[0]['roles'], data_hash)
        $stderr.puts data_hash['entry']['@status'].to_s + ' - ' + data_hash['entry']['schvaleni'].to_s
        $stderr.puts new_status + ' - ' + wait_for
        data_hash['entry']['@status'] = new_status
        data_hash['entry']['@wait_for'] = wait_for
        data_hash['entry']['@schvaleno_role'] = '' 
          schvalila_role = ''
          user_roles = @dict.get_users(@dict_array['tecu_user'], user)[0]['roles']
          waitar.each{|wf|
            schvalila_role = wf if user_roles.include?(wf) and not ['prekladatel','recenzent_preklad','obor_redaktor','obor_revizor'].include?(wf)
          }
          schvalila_role = user_roles[0] if schvalila_role == ''
          schvalila_role = @dict_array['settings'].get_settings['roles'].select{|r| r['role']==schvalila_role}[0]['label']
        newnote = {
          '@author' => user,
          '@time' => Time.now.strftime('%Y-%m-%d %H:%M:%S'),
          '@number' => (data_hash['entry']['notes']['note'].size+1),
          '$' => 'zamítnuto: '+request.query['comment'].to_s.force_encoding('UTF-8') + ', ' + schvalila_role
        }
        @dict_array['notification'].prepare_notif(id, term_cz, wait_for, 'zamitnuto', user, @dict_array['tecu_user'])
        data_hash['entry']['notes']['note'] << newnote
          if new_status == 'public' #data_hash['entry']['schvaleni'] == 'schvaleno'
            data_hash['entry'].delete('schvaleni')
            xml = CobraVsMongoose.hash_to_xml(data_hash).force_encoding('UTF-8')
            @dict.update(id, xml)
            @dict_array['tecuwork'].delete(id)
            add_history(id, xml)
            response.body = '{"ok":true, "msg":"saved", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          elsif new_status == 'deleted'
            data_hash['entry'].delete('schvaleni')
            xml = CobraVsMongoose.hash_to_xml(data_hash).force_encoding('UTF-8')
            @dict_array['tecudel'].update(id, xml)
            @dict.delete(id)
            @dict_array['tecuwork'].delete(id)
            response.body = '{"ok":true, "msg":"deleted", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          elsif new_status == 'zmena_zamitnuta'
            data_hash['entry'].delete('schvaleni')
            origxml = @dict.get(id)
            if origxml != ''
              orig_hash = normalize_json(CobraVsMongoose.xml_to_hash(origxml))
              orig_hash['entry']['notes'].delete('note') unless orig_hash['entry']['notes'].nil?
              orig_hash['entry']['notes'] = data_hash['entry']['notes']
              orig_hash['entry']['@status'] = 'public'
              orig_hash['entry']['@wait_for'] = ''
              xml = CobraVsMongoose.hash_to_xml(orig_hash).force_encoding('UTF-8')
              @dict.update(id, xml)
            end
            @dict_array['tecuwork'].delete(id)
            response.body = '{"ok":true, "msg":"saved", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          else
            data_hash['entry'].delete('schvaleni')
            xml = CobraVsMongoose.hash_to_xml(data_hash).force_encoding('UTF-8')
            @dict_array['tecuwork'].update(id, xml)
            response.body = '{"ok":true, "msg":"saved working", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          end
      end
    when 'list_notifications'
      res = @dict_array['notification'].list_notifications(user, request.query['term'].to_s, request.query['role'].to_s, request.query['time_from'].to_s, request.query['time_to'].to_s, request.query['from'].to_s, request.query['read'].to_s)
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = res.to_json
    when 'search_all_terms'
      url = URI.parse(@dict.central)
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      req = Net::HTTP::Post.new(url.path)
      req.basic_auth 'read', 'r'
      req.set_form_data({'action' => 'search_term', 'search' => request.query['search'].to_s, 'extid'=>@dict.centralid})
      resp = http.request(req)
      data = JSON.parse(resp.body)
      meta = @dict_array['settings'].get_settings['meta']
      data.each{|hash|
        hash['url'] = @servername+'/tecu?action=copy_term&dict='+hash['thes_id']+'&term='+hash['term_id']
      }
      #add skos dictionaries
      settings = @dict_array['settings'].get_settings
      settings['dict']['skos'].each{|sk|
        $stderr.puts sk
        url = URI.parse(sk)
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true if url.scheme == 'https'
        req = Net::HTTP::Post.new(url.path)
        resp = http.request(req)
        skosdata = resp.body
        skosdoc = @dict.load_xml_string(skosdata)
        next if skosdoc.nil?
        skosdoc.find('//skos:Concept[skos:prefLabel="'+request.query['search'].to_s+'"]').each{|e|
          skterms = []
          skdefs = []
          skid = e['about'].to_s
          e.find('skos:prefLabel').each{|skl|
            skterms << skl.content.to_s
          }
          e.find('skos:definition').each{|skd|
            skdefs << skd.content.to_s
          }
          data << {
            'term'=> skterms.join(', '),
            'thes_id'=> sk,
            'definition'=> skdefs.join(', '),
            'url'=> @servername+'/tecu?action=copy_skos&dict='+URI.escape(sk)+'&term='+URI.escape(skid)
          }
          $stderr.puts e
        }
      }
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = data.to_json
    when 'list_thesaurus'
      url = URI.parse(@dict.central)
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      req = Net::HTTP::Post.new(url.path)
      req.basic_auth 'read', 'r'
      req.set_form_data({'action' => 'list_thesaurus'})
      resp = http.request(req)
      data = JSON.parse(resp.body)
      res = []
      data.each{|hash|
        res << hash unless hash["tid"] == @dict.centralid
      }
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = res.to_json

    when 'copy_term'
      url = URI.parse(@dict.central)
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      req = Net::HTTP::Post.new(url.path)
      req.basic_auth 'read', 'r'
      req.set_form_data({'action' => 'copy_term', 'dict' => request.query['dict'].to_s, 'term'=>request.query['term'].to_s})
      resp = http.request(req)
      doc = @dict.load_xml_string(resp.body.to_s)
      id = @server.info.get_next_id('tecu')
      doc.root['id'] = id
      doc.root['status'] = 'before_new'
      doc.root['wait_for'] = 'asist_hl,tajemnik,revizor,obor_redaktor,obor_revizor'
      doc.root << XML::Node.new('notes')
      newnote = XML::Node.new('note')
      newnote['author'] = user
      newnote['time'] = Time.now.strftime('%Y-%m-%d %H:%M:%S')
      newnote['number'] = '1'
      newnote.content = 'prevzato z ' + request.query['dict'].to_s
      doc.find('notes').first << newnote
      @dict_array['tecuwork'].update(id, doc.to_s)
      term_cz = ''
      term_cz = doc.find('//term[@lang="cz"]').first.content.to_s unless doc.find('//term[@lang="cz"]').first.nil?
      @dict_array['notification'].prepare_notif(id, term_cz, 'obor_redaktor', 'prevzeti terminu z '+request.query['dict'].to_s.force_encoding('UTF-8'), user, @dict_array['tecu_user'])
      newurl = @servername+'/tezaurus/termEdit?id='+id
      response.set_redirect(WEBrick::HTTPStatus::TemporaryRedirect, newurl)

    when 'copy_skos'
      skdict = request.query['dict'].to_s
      skid = request.query['term'].to_s
      if skdict != '' and skid != ''
        $stderr.puts skdict
        url = URI.parse(skdict)
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true if url.scheme == 'https'
        req = Net::HTTP::Post.new(url.path)
        resp = http.request(req)
        skosdata = resp.body
        skosdoc = @dict.load_xml_string(skosdata)
        if skosdoc.nil?
          response.set_redirect(WEBrick::HTTPStatus::TemporaryRedirect, @servername+'/tezaurus/add')
        end
        $stderr.puts skid
        skosdoc.find('//skos:Concept[@rdf:about="'+skid+'"]').each{|e|
          $stderr.puts e
          doc = @dict.load_xml_string('<entry><terms/><defs/><notes/></entry>')
          id = @server.info.get_next_id('tecu')
          doc.root['id'] = id
          doc.root['status'] = 'before_new'
          doc.root['wait_for'] = 'asist_hl,tajemnik,revizor,obor_redaktor,obor_revizor'
          newnote = XML::Node.new('note')
          newnote['author'] = user
          newnote['time'] = Time.now.strftime('%Y-%m-%d %H:%M:%S')
          newnote['number'] = '1'
          newnote.content = 'prevzato z ' + skdict
          doc.find('notes').first << newnote
          term_cz = ''
          tnum = 1
          if e.find('skos:prefLabel[@xml:lang="cs"]').to_a.size > 0
            e.find('skos:prefLabel[@xml:lang="cs"]').each{|skl|
              newterm = XML::Node.new('term')
              newterm['lang'] = 'cz'
              newterm['number'] = tnum.to_s
              newterm.content = skl.content.to_s
              doc.find('terms').first << newterm
              term_cz = newterm.content.to_s
              tnum += 1
            }
          else
            newterm = XML::Node.new('term')
            newterm['lang'] = 'cz'
            newterm['number'] = tnum.to_s
            newterm.content = e.find('skos:prefLabel').first.content.to_s
            doc.find('terms').first << newterm
            term_cz = newterm.content.to_s
            tnum += 1
          end
          e.find('skos:prefLabel[@xml:lang!="cs"]').each{|skl|
            newterm = XML::Node.new('term')
            newterm['lang'] = skl['lang']
            newterm['lang'] = 'gb' if newterm['lang'] == 'en'
            newterm['number'] = tnum.to_s
            newterm.content = skl.content.to_s
            doc.find('terms').first << newterm
            tnum += 1
          }
          e.find('skos:definition').each{|skd|
            newdef = XML::Node.new('def')
            newdef['lang'] = 'cz'
            newt = XML::Node.new('text')
            newt.content = skd.content.to_s
            newdef << newt 
            doc.find('defs').first << newdef
          }
          $stderr.puts doc
          @dict_array['tecuwork'].update(id, doc.to_s)
          @dict_array['notification'].prepare_notif(id, term_cz, 'obor_redaktor', 'prevzeti terminu z '+skdict.force_encoding('UTF-8'), user, @dict_array['tecu_user'])
          newurl = @servername+'/tezaurus/termEdit?id='+id
          response.set_redirect(WEBrick::HTTPStatus::TemporaryRedirect, newurl)
        }
      else
        response.set_redirect(WEBrick::HTTPStatus::TemporaryRedirect, @servername+'/tezaurus/add')
      end



    when 'download_all'
      response['Content-Type'] = 'application/xml; charset=utf-8'
      if request.query['tr'].to_s == 'skos'
        response.body = @dict.apply_transform('skos', @dict.dump("tecu"), [["servername",'"'+@servername.gsub('https','http')+'"']]).sub('<?xml version="1.0" encoding="utf-8"?>','').sub(/<!DOCTYPE[^>]*>/,'')
      else
        response.body = @dict.dump("tecu")
      end
    when 'get_users'
      output = {'users'=>@dict.get_users(@dict_array['tecu_user'],request.query['login'].to_s)}
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = output.to_json
    when 'delete_user'
      if request.query['login'].to_s != ''
        @dict_array['tecu_user'].delete(request.query['login'].to_s)
        res = {'ok'=>'deleted'}
      else
        res = {'error'=>'no data'}
      end
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = res.to_json
    when 'save_user'
      if request.query['data'].to_s != ''
        begin
          data_hash = JSON.parse(request.query['data'].to_s)
        rescue
          res = {'error'=>'JSON parsing error'}
        end
        if data_hash != nil and data_hash['login'].to_s != ''
          newuser = false
          login = data_hash['login']
          if data_hash['password'].to_s != ''
            newpass = data_hash['password']
            pass = newpass.crypt(WEBrick::Utils::random_string(2))
          elsif data_hash['new'] == '1'
            newpass = WEBrick::Utils::random_string(8)
            pass = newpass.crypt(WEBrick::Utils::random_string(2))
            newuser = true
          else
            pass = @server[:Authenticator].userdb[login]
          end
          xml = '<user>'
          xml += '<login>'+login+'</login>'
          xml += '<name>'+data_hash['name'].to_s+'</name>'
          xml += '<email>'+data_hash['email'].to_s+'</email>'
          xml += '<phone>'+data_hash['phone'].to_s+'</phone>'
          xml += '<org>'+data_hash['organization'].to_s+'</org>'
          xml += '<services><service code="tecu"/></services>'
          xml += '<pass>'+pass+'</pass>'
          unless data_hash['roles'].nil?
            xml += '<roles>'
            data_hash['roles'].each{|r| 
              data_hash['roles'] << 'prekladatel' if r.to_s.start_with?('prekladatel_')
              data_hash['roles'] << 'recenzent_preklad' if r.to_s.start_with?('recenzent_preklad_')
              data_hash['roles'] << 'obor_redaktor' if r.to_s.start_with?('obor_redaktor_')
              data_hash['roles'] << 'obor_revizor' if r.to_s.start_with?('obor_revizor_')
            }
            data_hash['roles'].uniq.each{|r| xml += '<role>'+r.to_s+'</role>'}
            xml += '</roles>'
          end
          xml += '</user>'
          @dict_array['tecu_user'].update(login, xml)
          @server[:Authenticator].userdb.reload(true)

          res = @dict.get_users(@dict_array['tecu_user'], login)
        end
      else
        res = {'error'=>'no data'}
      end
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = res.to_json
    when 'send_meta'
      meta = @dict_array['settings'].get_settings['meta']
      meta['tid'] = @dict.centralid
      url = URI.parse(@dict.central)
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      req = Net::HTTP::Post.new(url.path)
      req.basic_auth 'read', 'r'
      req.set_form_data({'action' => 'update_thesaurus', 'data' => meta.to_json})
      resp = http.request(req) 

      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = resp.body
    when 'exlink_updated'
      if @dict.get(request.query['id'].to_s) != ''
        xml = @dict.get(request.query['id'].to_s)
        doc = @dict.load_xml_string(xml)
        term_cz = ''
        term_cz = doc.find('/entry/terms/term[@lang="cz"]').first.content.to_s unless doc.find('/entry/terms/term[@lang="cz"]').first.nil?
        @dict_array['notification'].prepare_notif_exlink(request.query['id'].to_s.force_encoding('UTF-8'), term_cz, request.query['exdict'].to_s.force_encoding('UTF-8'), request.query['exlink'].to_s.force_encoding('UTF-8'), @dict_array['tecu_user'])
      end
      response['Content-Type'] = 'application/json; charset=utf-8'
      response.body = 'ok'
    when 'export_doc'
      search_data = {}
      if request.query['data'].to_s != ''
        if request.query['data'].to_s[0] == '['
          search_data = {'ids'=>JSON.parse(request.query['data'].to_s)}
        else
          search_data = JSON.parse(request.query['data'].to_s) #unless request.query['data'].to_s == ''
        end
      end
      res = @dict.search(search_data)
      case request.query['type'].to_s
      when 'xml'
        result = '<tecu>'
        res.each{|entry|
          result += @dict.get(entry['id'])
        }
        result += '</tecu>'
        response.body = result
        response['Content-Type'] = 'application/xml; charset=utf-8'
      when 'html'
        response.body = export_html(res, false)
        response['Content-Type'] = 'text/html; charset=utf-8'
      when 'html2'
        response.body = export_html(res, true)
        response['Content-Type'] = 'text/html; charset=utf-8'
      when 'pdf','pdf2'
        short = false
        short = true if request.query['type'] == 'pdf2'
        tmpfile = Tempfile.new('tecupdf')
        html = export_html(res, short, true)
        pid = fork {
          require 'pdfkit'
          kit = PDFKit.new(html, :page_size=>'A4', :encoding=>'UTF-8')
          pdf = kit.to_pdf
          tmpfile.write(pdf)
        }
        $stderr.puts pid
        Process.wait(pid) 
        reconnect
        tmpfile.rewind
        pdf = tmpfile.read
        tmpfile.close
        response.body = pdf
        response['Content-Type'] = 'application/pdf'
        #kit.stylesheets << '/var/lib/deb-server/files/tecu/preview.css'
      when 'rtf', 'rtf2'
        params = []
        short = false
        short = true if request.query['type'] == 'rtf2'
        html = export_html(res, short, true)
        tmpfile = Tempfile.new('tecurtf')
        pid = fork {
          require 'html2rtf'
          document = RTF::Document.new(RTF::Font.new(RTF::Font::ROMAN, 'Times New Roman'))
          noko = Nokogiri::HTML::Document.parse(html)
          document = noko.xpath("/html/body/*").to_rtf(document)
          res = document.to_rtf
          tmpfile.write(res)
        }
        $stderr.puts pid
        Process.wait(pid) 
        reconnect
        tmpfile.rewind
        rtf = tmpfile.read
        tmpfile.close
        response.body = rtf
        response["Content-type"] = "application/rtf; name=tecuexport.rtf"
        response["Content-Disposition"] = "inline; filename=tecuexport.rtf"
      when 'skos'
        result = '<tecu>'
        res.each{|entry|
          result += @dict.get(entry['id'])
        }
        result += '</tecu>'
        response.body = @dict.apply_transform('skos', result, [["servername",'"'+@servername.gsub('https','http')+'"'] ])
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        result = {
          'terminologicky'=>{'name'=>'terminologický','results'=>[]},
          #'pouzivany'=>{'name'=>'používaný','results'=>[]},
          #'nezpracovany'=>{'name'=>'nezpracovaný','results'=>[]}
        }
        res.each{|entry|
          typ = 'terminologicky' #if entry['status'] == 1
          #typ = 'pouzivany' if entry['status'] == 2
          #typ = 'nezpracovany' if entry['status'] == 5
          result[typ]['results'] << {'id'=>entry['id'], 'title'=>entry['head']}
        }
        response.body = result.to_json
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    when 'backup'
      res = 'zaloha: '
      res += 'databaze,' if request.query['databaze'].to_s == 'true'
      res += 'logy,' if request.query['logy'].to_s == 'true'
      res += 'aplikace,' if request.query['aplikace'].to_s == 'true'
      bak_ap = 'n'
      bak_data = 'n'
      bak_log = 'n'
      bak_ap = 'y' if request.query['aplikace'].to_s == 'true'
      bak_data = 'y' if request.query['databaze'].to_s == 'true'
      bak_log = 'y' if request.query['logy'].to_s == 'true'
      fname = "backup"+Time.now.to_i.to_s+".log"
      Thread.new{system("/var/lib/deb-server/bin/tecu_backup.sh "+bak_ap+" "+bak_data+" "+bak_log+" > /var/log/deb-server/"+fname+" 2> /var/log/deb-server/"+fname)}
      response.body = fname.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'get_viz'
      res = @dict.get_viz(request.query['id'].to_s)
      response.body = res.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'send_remark'
      if request.query['data'].to_s != '' 
        data = JSON.parse(request.query['data'].to_s)
        subject = 'Připomínka z tezauru'
        subject += ': ' + data['subject'].to_s if data['subject'].to_s != ''
        @dict.send_mail_role(subject, "Jméno: " + data['name'].to_s + "\nE-mail: "+ data['email'].to_s + "\n\n" + data['text'].to_s, 'asist_hl')
        @dict.send_mail_contact(subject, "Jméno: " + data['name'].to_s + "\nE-mail: "+ data['email'].to_s + "\n\n" + data['text'].to_s)
        response.body = {'ok'=>true, 'msg'=>'zpráva odeslána'}.to_json
        response['Content-Type'] = 'application/json; charset=utf-8'
      else
        response.body = {'error'=>true, 'msg'=>'no data'}.to_json
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    when 'search_skos'
      settings = @dict_array['settings'].get_settings
      res = ''
      xml = @dict.get(request.query['id'].to_s)
      terms = []
      if xml !=''
        doc = @dict.load_xml_string(xml)
        doc.find('//term').each{|term|
          terms << term.content.to_s
        }
      end
      skosentries = '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:skos="http://www.w3.org/2004/02/skos/core#">'
      settings['dict']['skos'].each{|sk|
        $stderr.puts sk
        url = URI.parse(sk)
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true if url.scheme == 'https'
        req = Net::HTTP::Post.new(url.path)
        resp = http.request(req)
        data = resp.body
        skosdoc = @dict.load_xml_string(data)
        next if skosdoc.nil?
        terms.each{|term|
          $stderr.puts term
          skosdoc.find('//skos:Concept[skos:prefLabel="'+term+'"]').each{|e|
            e['fromskos'] = sk
            skosentries += e.to_s
          }
        }
      }
      skosentries += '</rdf:RDF>'
      entries = @dict.apply_transform('fromskos', skosentries, [])
      html = @dict.apply_transform('preview', entries, [])
      $stderr.puts skosentries
      response.body = html
      response['Content-Type'] = 'text/html; charset=utf-8'
    when 'save_design'
      $stderr.puts 'SAVE'
      if request.query['data'].to_s != '' 
        data = JSON.parse(request.query['data'])
        @dict_array['settings'].save_design(data)
        #fork{
          system("/var/lib/deb-server/bin/tecu_design.rb")
          system("/var/lib/deb-server/bin/tecu_client.sh")
          #reconnect
        #}
        response.body = {'ok'=>true, 'msg'=>'uloženo'}.to_json
        response['Content-Type'] = 'application/json; charset=utf-8'
      else
        response.body = {'error'=>true, 'msg'=>'no data'}.to_json
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    when 'get_history'
      response.body = @dict_array['tecu'].list_history(request.query['entry_id'].to_s).to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'get_deleted'
      response.body = @dict_array['tecudel'].get_deleted.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'get_waiting'
      response.body = @dict_array['tecuwork'].get_waiting.to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'expired_notifications'
      response.body = @dict_array['tecu'].list_expired_notifications(@servername).to_json
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'recover'
      if @dict.get_perm(user) != 'w'
        response.body = '{"error":true, "msg":"nemáte oprávnění k editaci"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
      id = request.query['id'].to_s.force_encoding("UTF-8")
      begin
        xml = @dict_array['tecudel'].get(id)
        doc = @dict.load_xml_string(xml)
        doc.root['wait_for'] = 'hl_redaktor'
        doc.root['status'] = 'to_recover'
        doc.root << XML::Node.new('notes') if doc.find('notes').length == 0
        newnote = XML::Node.new('note')
        newnote['author'] = user
        newnote['time'] = Time.now.strftime('%Y-%m-%d %H:%M:%S')
        newnote['number'] = (doc.find('notes/note').size + 1).to_s
        newnote.content = 'navrh na obnoveni zaznamu'
        doc.find('notes').first << newnote
        @dict_array['tecuwork'].update(id, doc.to_s)
        @dict_array['tecudel'].delete(id)
        term_cz = doc.find('terms/term[@lang="cz"]').first.content.to_s unless doc.find('terms/term[@lang="cz"]').first.nil?
        @dict_array['notification'].prepare_notif(id, term_cz, 'hl_redaktor', 'navrh na obnoveni zaznamu', user, @dict_array['tecu_user'])
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
      end
      response.set_redirect(WEBrick::HTTPStatus::TemporaryRedirect, '/tezaurus/deletedTerms')
    when 'fullstat'
      result = ''
      if request.query['year'].to_s != '' and request.query['month'].to_s != ''
        result = `/usr/share/awstats/wwwroot/cgi-bin/awstats.pl -staticlinks -month=#{request.query['month'].to_s} -year=#{request.query['year'].to_s} -output -config=tecu -configdir=/var/lib/deb-server/files/tecu/stats`
      end
      response.body = result.gsub('href="awstats', 'href="/editor/stats/awstats')
      response['Content-Type'] = 'text/html; charset=utf-8'
    end
  end
  
  alias do_POST do_GET
end
