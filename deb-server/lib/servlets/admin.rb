require 'template'
require 'convert'
require 'dump'
require 'import'
require 'pp'

class AdminServlet < WEBrick::HTTPServlet::AbstractServlet
  def initialize( server, infodict, base_path, template_path, xslt_path )
    @infodict = infodict
    @server = server

    @servername = nil
    @template_path = template_path
    @base_path = base_path
    @xslt_path = xslt_path
  end

#devel
#def AdminServlet.get_instance config, *options
#load __FILE__
#AdminServlet.new config, *options
#end

  def get_servername(server, request)
    if server[:SSLEnable]
      servername = 'https://'
    else
      servername = 'http://'
    end
    servername += request['Host'].to_s
  end

  #load dictionary class names from lib/modules/dicts*
  def load_dict_names
    dicts = []
    path = @base_path + '/lib/modules'
    Dir.glob(path + '/dicts*'){|filename|
      File.open(filename).each{|line|
        dicts << {'class' => line.strip} if line.strip != ''
      }
    }
    return dicts.sort{|a,b| a['class']<=>b['class']}
  end

  def checkadmin(user)
    if @server[:Authenticator].userdb.check_permissions(user, 'admin', true)
      return true
    else
      return false
    end
  end

  def allowed_dirs
    allow_dir = ['files', 'files/wn', 'xpi']
    @infodict.list_dicts.each{ |dcode,name|
      #allow_dir << 'files/'+dcode
      allow_dir << 'xpi/'+dcode
    }
    @infodict.list_services.each{|scode,name|
      allow_dir << 'files/'+scode
    }
    return allow_dir
  end

  def save_uploaded_file(filedata, filename, dir)
    allow_dir = allowed_dirs
    dirpath = nil
    $stderr.puts dir + filename
    case dir
      when 'xslt'
        dirpath = @xslt_path
      when 'template'
        dirpath = @template_path
      else
        if allow_dir.include?(dir)
          dirpath = @base_path + '/' + dir
        else
          return false
        end
    end
    if dirpath != nil
      if not File.exists?(dirpath)
        Dir.mkdir(dirpath, 0775)
        File.new(dirpath).chmod(0775)
      end
      f = File.new(dirpath+'/'+filename, 'w')
      f.syswrite(filedata)
      f.chmod(0664)
    else
      return false
    end
  end

  #convert from LMF to DEBVisDic format
  def convert_from_lmf(src)
    xslt = XML::XSLT.new()
    xslt.xslfile = @xslt_path + 'kyoto-lmf2deb.xslt'
    xslt.xml = src
    s = xslt.serve()
    temp = Tempfile.new('lmfconv')
    temp.write(s)
    temp.flush
    temp.close
    return temp.path
  end

  #unmount old dicts, mount new
  def remount
    @infodict.array.each_key{ |code|
      @server.umount('/'+code)
    }
    @infodict.array = {}
    text = ''
    @infodict.xquery_to_array('/dict').sort.each{ |xml|
      code = ''
      file = ''
      doc = @infodict.load_xml_string(xml)
      code = doc.find('/dict/code').first.content.to_s
      keypath = doc.find('/dict/key').first.content.to_s
      classname = doc.find('/dict/class').first.content.to_s
      serverclass = get_class(classname)
      $stderr.puts code+':'+file
      text += code+':'+file + "\n"
      if code != '' and file != '' and File.exists?(@infodict.db_path + '/' + file)
        @infodict.array[code] = serverclass.new( @infodict.db_path, file, keypath )
        @infodict.array[code].xslt_sheets = {}
        doc.find('/dict/xslts/xslt').each{|el|
          xname = el.find('name').first.content
          xfile = el.find('file').first.content
          @infodict.array[code].xslt_sheets[xname] = @xslt_path + '/' + xfile
        }
      end
    }
    @infodict.array.each_key { |code|
      @server.mount '/'+code, SlovServlet, @infodict.array[code]
    }

    text
  end

  #update htpasswd file for services
  def update_htpasswd(userservs=nil)
    @infodict.list_services(true).each{|scode, htfile|
      if htfile.to_s != '' and File.file?(htfile) and (not userservs.nil? and userservs.include?(scode))
        $stderr.puts "update htpassd for #{scode}-#{htfile}"
        htf = File.new(htfile, 'w')
        @infodict.get_passwd(scode).each{|h|
          $stderr.puts h['user']+':'+h['pass']
          htf.puts h['user']+':'+h['pass']
        }
        htf.close
      end
    }
  end

  def do_GET(request, response)
    user = request.attributes[:auth_user].to_s
    @servername = get_servername(@server, request) if @servername == nil
    $stderr.puts @servername
    case request.query['action']
    when 'listusers'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listusers.tmpl')
        return response.body
      end
      response['Content-Type'] = 'text/html; charset=utf-8'
      users = {'no_service'=>[]}
      cssclass = 1
      service_list = @infodict.list_services
      dict_list = {}
      service_list.each{|code,servname|
        dict_list[code] = @infodict.list_dicts(code).sort
        users[code] = []
      }

      #edit one user
      edituser = []
      if request.query['edituser'] != nil and request.query['edituser'] != ''
        begin
          xml = @infodict.get('user'+request.query['edituser'].to_s)
          $stderr.puts xml
          doc = @infodict.load_xml_string(xml)
          login = doc.find('/user/login').first.content.to_s
          name = doc.find('/user/name').first.content.to_s unless doc.find('/user/name').first.nil?
          email = doc.find('/user/email').first.content.to_s unless doc.find('/user/email').first.nil?
          org = doc.find('/user/org').first.content.to_s unless doc.find('/user/org').first.nil?
          addr = doc.find('/user/addr').first.content.to_s unless doc.find('/user/addr').first.nil?
          comment = doc.find('/user/comment').first.content.to_s unless doc.find('/user/comment').first.nil?
          admin =  (doc.find('/user/admin').first != nil and doc.find('/user/admin').first.content == 'true')? true:false
          trainee = (doc.find('/user/trainee').first != nil and doc.find('/user/trainee').first.content == 'true')? true:false
          userperms = []
          service_list.each{ |code,servname|
            perm = @server[:Authenticator].userdb.get_perms[login][code].to_s
            uperms = {'code'=>code, 'cssclass'=>cssclass}
            uperms['perm'] = 1 if perm == '1'
            udicts = []
            dict_list[code].each{ |dc, dname|
              perm = @server[:Authenticator].userdb.get_perms[login][code+'_'+dc].to_s
              dperms = {'serv'=>code, 'dcode'=>dc, 'cssclass'=>cssclass, 'perm'=>perm}
              udicts << dperms
            }
            uperms['dicts'] = udicts
            userperms << uperms
          }
          userperms.sort! { |x,y| x['code'] <=> y['code'] }
          edituser << {'login'=>login, 'name'=>name, 'email'=>email, 'org'=>org, 'addr'=>addr, 'comment'=>comment, 'admin'=>admin, 'perms'=>userperms, 'cssclass'=>cssclass, 'trainee'=>trainee}
        rescue => e
          $stderr.puts e
        end
      end

      #user list
      user_names = {}
      query = '/user'
      query = '/user[services/service/@code="'+request.query['service'].to_s+'"]' if request.query['service'].to_s != ''

      @infodict.xquery_to_array(query).sort.each{ |xml|
        doc = @infodict.load_xml_string(xml)
        login = doc.find('/user/login').first.content.to_s
        name = ''
        name = doc.find('/user/name').first.content.to_s unless doc.find('/user/name').first.nil?
        if request.query['detail'].to_s == '1'
          email = doc.find('/user/email').first.content.to_s unless doc.find('/user/email').first.nil?
          org = doc.find('/user/org').first.content.to_s unless doc.find('/user/org').first.nil?
          addr = doc.find('/user/addr').first.content.to_s unless doc.find('/user/addr').first.nil?
          comment = doc.find('/user/comment').first.content.to_s unless doc.find('/user/comment').first.nil?
          admin =  (doc.find('/user/admin').first != nil and doc.find('/user/admin').first.content == 'true')? true:false
          trainee = (doc.find('/user/trainee').first != nil and doc.find('/user/trainee').first.content == 'true')? true:false
          userperms = []
          service_list.each{ |code,servname|
            perm = @server[:Authenticator].userdb.get_perms[login][code].to_s
            uperms = {'code'=>code, 'cssclass'=>cssclass}
            uperms['perm'] = 1 if perm == '1'
            udicts = []
            dict_list[code].each{ |dc, dname|
              perm = @server[:Authenticator].userdb.get_perms[login][code+'_'+dc].to_s
              dperms = {'serv'=>code, 'dcode'=>dc, 'cssclass'=>cssclass, 'perm'=>perm}
              udicts << dperms
            }
            uperms['dicts'] = udicts
            userperms << uperms
          }
          userperms.sort! { |x,y| x['code'] <=> y['code'] }
        end
        service = false
        doc.find('/user/services/service').each{|el|
          scode = el['code'].to_s
          if users[scode] != nil
            users[scode] << login.to_s 
            service = true
          end
        }
        if not service
          users['no_service'] << login.to_s
        end
        if request.query['detail'].to_s == '1'
          user_names[login] = {'login'=>login, 'name'=>name, 'email'=>email, 'org'=>org, 'addr'=>addr, 'comment'=>comment, 'admin'=>admin, 'perms'=>userperms, 'cssclass'=>cssclass, 'trainee'=>trainee}
        else
          user_names[login] = {'login'=>login, 'name'=>name}
        end
      }
      servlist = []
      service_list.each{ |code,servname|
        dictlist = []
        dict_list[code].sort.each{ |dc, dname|
          dictlist << {'dcode'=>dc, 'serv'=>code}
        }
        servlist << {'code'=>code, 'dicts'=>dictlist}
      }
      userlist = []
      all_users = []
      users.each{|code,list|
        if request.query['service'].to_s == '' or request.query['service'].to_s == code
          suserlist = []
          all_users += list
          list.sort.each{|login|
            suserlist << user_names[login]
          }
          userlist << {'code'=>code, 'list'=>suserlist, 'count'=>list.size}
        end
      }

      if request.query['service'].to_s == ''
       all_userlist = []
       all_users.uniq.sort.each{ |login|
         all_userlist << user_names[login]
       }
       userlist.unshift({'code'=>'all users', 'list'=>all_userlist, 'count'=>all_users.uniq.size})
      end 

      servlist.sort! { |x,y| x['code'] <=> y['code'] }
      tmpl_params = {'users'=>userlist, 'servs'=>servlist, 'edituser'=>edituser}
      tmpl_params['deluser'] = request.query['delete'].to_s if request.query['delete'] != nil and request.query['delete'].to_s != '' 

      api = request.query['api'].to_s
      callback = request.query['callback'].to_s

      if api == '1'
        if callback != ''
          response['Content-Type'] = 'text/html; charset=utf-8'
        else
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
        responsetext = userlist.to_json
        responsetext = callback + '(' + responsetext + ');' if callback != ''
        response.body = responsetext
        return response.body
      end

      response.body = HTMLTemplate.new(tmpl_params).output(@template_path + '/listusers.tmpl')
      
    when 'listusersfull'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listusers.tmpl')
        return response.body
      end
      response['Content-Type'] = 'text/html; charset=utf-8'
      users = []
      cssclass = 1
      service_list = @infodict.list_services
      dict_list = {}
      service_list.each{|code,servname|
        dict_list[code] = @infodict.list_dicts(code).sort
      }

      @infodict.xquery_to_array('/user').sort.each{ |xml|
        cssclass ^= 1
        doc = @infodict.load_xml_string(xml.to_s)
        login = doc.find('/user/login').first.content.to_s
        name = doc.find('/user/name').first.content.to_s unless doc.find('/user/name').first.nil?
        email = doc.find('/user/email').first.content.to_s unless doc.find('/user/email').first.nil?
        org = doc.find('/user/org').first.content.to_s unless doc.find('/user/org').first.nil?
        addr = doc.find('/user/addr').first.content.to_s unless doc.find('/user/addr').first.nil?
        comment = doc.find('/user/comment').first.content.to_s unless doc.find('/user/comment').first.nil?
        admin =  (doc.find('/user/admin').first != nil and doc.find('/user/admin').first.content == 'true')? true:false
        trainee = (doc.find('/user/trainee').first != nil and doc.find('/user/trainee').first.content == 'true')? true:false
        userperms = []
        service_list.each{ |code,servname|
          perm = @server[:Authenticator].userdb.get_perms[login][code].to_s
          uperms = {'code'=>code, 'cssclass'=>cssclass}
          uperms['perm'] = 1 if perm == '1'
          udicts = []
          dict_list[code].each{ |dc, dname|
            perm = @server[:Authenticator].userdb.get_perms[login][code+'_'+dc].to_s
            dperms = {'serv'=>code, 'dcode'=>dc, 'cssclass'=>cssclass, 'perm'=>perm}
            udicts << dperms
          }
          uperms['dicts'] = udicts
          userperms << uperms
        }
        userperms.sort! { |x,y| x['code'] <=> y['code'] }
        users << {'login'=>login, 'name'=>name, 'email'=>email, 'org'=>org, 'addr'=>addr, 'comment'=>comment, 'admin'=>admin, 'perms'=>userperms, 'cssclass'=>cssclass, 'trainee'=>trainee}
      }

      tmpl_params = {'users'=>users}
      response.body = HTMLTemplate.new(tmpl_params).output(@template_path + '/listusersfull.tmpl')
      
    when 'edituser'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listusers.tmpl')
        return response.body
      end
      newuser = false
      api = request.query['api'].to_s
      callback = request.query['callback'].to_s
      
      response['Content-Type'] = 'text/html; charset=utf-8'      
      if request.query['login'] != nil and request.query['login'] != ''
        login =request.query['login']
        
        #new user - check if login exists
        if request.query['new'] != nil and request.query['new'] == '1'
          if @infodict.get('user'+login).to_s != ''
            responsetext = 'login exists'
            responsetext = responsetext.to_json if api == '1'
            responsetext = callback + '(' + responsetext + ');' if callback != ''
            response.body = responsetext
            return response.body
          end
        end
        
        #if password is entered, crypt it
        #if new user, generate new password
        #else use the existing one
        if request.query['pass'] != nil and request.query['pass'].to_s != ''
          newpass = request.query['pass']
          pass = newpass.crypt(WEBrick::Utils::random_string(2))
        elsif request.query['new'] != nil and request.query['new'] == '1'
          newpass = WEBrick::Utils::random_string(8)
          pass = newpass.crypt(WEBrick::Utils::random_string(2))
          newuser = true
        else
          pass = @server[:Authenticator].userdb[login]
        end

        email = request.query['email'].to_s
        PP::pp(request.query, $stderr)
        $stderr.puts request.query

        xml = '<user>'
        xml += '<login>'+login+'</login>'
        xml += '<name>'+request.query['name'].to_s.force_encoding('UTF-8')+'</name>'
        xml += '<email>'+email.to_s.force_encoding('UTF-8')+'</email>'
        xml += '<org>'+request.query['org'].to_s.force_encoding('UTF-8')+'</org>'
        xml += '<addr>'+request.query['addr'].to_s.force_encoding('UTF-8')+'</addr>'
        xml += '<comment>'+request.query['comment'].to_s.force_encoding('UTF-8')+'</comment>'
        xml += '<pass>'+pass+'</pass>'
        xml += '<admin>true</admin>' if request.query['admin'] != nil and request.query['admin'] == 'on'
        xml += '<trainee>true</trainee>' if request.query['trainee'] != nil and request.query['trainee'] == 'on'
        xml += '<services>'
        servs = []
        htservs = []
        @infodict.list_services.each{ |code,name|
          perm = request.query['serv_'+code]
          if perm != nil and perm == 'on'
            serv_det = []
            xml += '<service code="' + code + '">'
            @infodict.list_dicts(code).each{ |dc, dname|
              perm = request.query['dict_'+code+'_'+dc].to_s.strip
              if perm != nil and perm != ''
                xml += '<dict code="'+dc+'" perm="'+perm+'"/>'
                serv_det << dc+':'+perm
              end
            }
            if serv_det.size > 0
              servs << code + '(' + serv_det.join(', ') + ')'
            else
              servs << code
            end
            htservs << code
            xml += '</service>'
          end
        }
        xml += '</services>'
        xml += '</user>'

        @infodict.update('user'+login, xml)
        @server[:Authenticator].userdb.reload(true)
        update_htpasswd(htservs)
        #@server[:Authenticator].userdb.load_permissions()
        
        #send email
        if newuser
          message = IO.read(@template_path + '/mailnew.tmpl')
        elsif newpass !=nil
          message = IO.read(@template_path + '/mailnewpass.tmpl')
        else
          message = IO.read(@template_path + '/mailchange.tmpl')
        end
        message.gsub!('#{user}', login)
        message.gsub!('#{email}', email)
        message.gsub!('#{pass}', newpass) if newpass != nil
        message.gsub!('#{services}', servs.join(', '))
        message.gsub!('#{server}', @servername)
        $stderr.puts message
        if email != ''
          mail_send(email, 'deb@aurora.fi.muni.cz', message, 'debucty@aurora.fi.muni.cz')
        end

      end
      
      if api == '1'
        responsetext = 'user created'.to_json 
        responsetext = callback + '(' + responsetext + ');' if callback != ''
        response.body = responsetext
        return response.body
      end

      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listusers#' + login)
      #response.body += xml
      #response.body += 'new pass='+newpass if newpass != nil
      #response.body += '<br/><a href="/admin?action=listusers">list</a>'

    when 'genpass'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listusers.tmpl')
        return response.body
      end

      api = request.query['api'].to_s
      callback = request.query['callback'].to_s

      if request.query['login'] != nil and request.query['login'] != ''
        login = request.query['login']
        newpass = WEBrick::Utils::random_string(8)
        newcrypt = newpass.crypt(WEBrick::Utils::random_string(2))
        begin
          xml = @infodict.get('user'+login)
        rescue =>e
          if api == '1'
            responsetext = 'invalid login'.to_json 
            responsetext = callback + '(' + responsetext + ');' if callback != ''
            response.body = responsetext
            return response.body
          else
            response.body = "invalid login"
            return response.body
          end
        end
        doc = @infodict.load_xml_string(xml.to_s)
        email = doc.find('/user/email').first.content.to_s
        doc.find('/user/pass').first.content = newcrypt
        @infodict.update('user'+login, doc.to_s)
        @server[:Authenticator].userdb.reload(true)
        
        servs = []
        doc.find('/user/services/service').each{ |el|
          serv_code = el['code']
          serv_det = []
          el.find('dict').each{|del|
            perm = del['perm']
            if perm != ''
              serv_det << del['code']+':'+perm
            end
          }
          if serv_det.size > 0
            servs << serv_code + '(' + serv_det.join(', ') + ')'
          else
            servs << serv_code
          end
        }

        #send email
        message = IO.read(@template_path + '/mailnewpass.tmpl')
        message.gsub!('#{user}', login)
        message.gsub!('#{email}', email)
        message.gsub!('#{pass}', newpass) if newpass != nil
        message.gsub!('#{services}', servs.join(', '))
        message.gsub!('#{server}', @servername)
        $stderr.puts message
        if email != ''
          mail_send(email, 'deb@aurora.fi.muni.cz', message, 'debucty@aurora.fi.muni.cz')
        end

        if api == '1'
          responsetext = 'password reset'.to_json 
          responsetext = callback + '(' + responsetext + ');' if callback != ''
          response.body = responsetext
          return response.body
        end

        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listusers')
        response.body += newcrypt
      else
        if api == '1'
          responsetext = 'invalid login'.to_json 
          responsetext = callback + '(' + responsetext + ');' if callback != ''
          response.body = responsetext
          return response.body
        else
          response.body = "invalid login"
          return response.body
        end
      end
      
    when 'changepass'
      if request.query['new'] != nil and request.query['new'] != ''
        newcrypt = request.query['new'].crypt(WEBrick::Utils::random_string(2))
        xml = @infodict.get('user'+user)
        doc = @infodict.load_xml_string(xml.to_s)
        doc.find('/user/pass').first.content = newcrypt
        @infodict.update('user'+user, doc.to_s)
        @server[:Authenticator].userdb.reload(true)
        update_htpasswd(['cpa'])

        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=changepass&set=1')
        response.body += newcrypt
      end
      params = {'user'=>user}
      if request.query['set'] == '1'
        params['set'] = 1
      end
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = HTMLTemplate.new(params).output(@template_path + '/changepass.tmpl')
      return response.body

    when 'deluser'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listusers.tmpl')
        return response.body
      end
      login = request.query['login'].to_s
      api = request.query['api'].to_s
      callback = request.query['callback'].to_s
      if login != ''
        begin
          xml = @infodict.get('user'+login)
        rescue=>e
          if api == '1'
            responsetext = 'invalid login'.to_json 
            responsetext = callback + '(' + responsetext + ');' if callback != ''
            response.body = responsetext
            return response.body
          else
            response.body = "invalid login"
            return response.body
          end
        end
        doc = @infodict.load_xml_string(xml.to_s)
        email = doc.find('/user/email').first.content.to_s

        @infodict.delete('user'+login)
        @server[:Authenticator].userdb.reload(true)

        #send email
        message = IO.read(@template_path + '/maildel.tmpl')
        message.gsub!('#{user}', login)
        message.gsub!('#{email}', email)
        $stderr.puts message
        if email != ''
          mail_send(email, 'deb@aurora.fi.muni.cz', message, 'debucty@aurora.fi.muni.cz')
        end
        if api == '1'
          responsetext = 'user deleted'.to_json 
          responsetext = callback + '(' + responsetext + ');' if callback != ''
          response.body = responsetext
          return response.body
        end
      else
        if api == '1'
          responsetext = 'invalid login'.to_json 
          responsetext = callback + '(' + responsetext + ');' if callback != ''
          response.body = responsetext
          return response.body
        end
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listusers')

    when 'listdicts'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listdicts.tmpl')
        return response.body
      end
      
      response['Content-Type'] = 'text/html; charset=utf-8'
      dictclasses = load_dict_names
      dictlist = []
      cssclass = 1
      @infodict.xquery_to_array('/dict').sort.each {|xml|
        cssclass ^= 1
        doc = @infodict.load_xml_string(xml)
        code = doc.find('/dict/code').first.content.to_s unless doc.find('/dict/code').first.nil?
        name = doc.find('/dict/name').first.content.to_s unless doc.find('/dict/name').first.nil?
        dclass = doc.find('/dict/class').first.content.to_s unless doc.find('/dict/class').first.nil?
        root = doc.find('/dict/root').first.content.to_s unless doc.find('/dict/root').first.nil?
        key = doc.find('/dict/key').first.content.to_s unless doc.find('/dict/key').first.nil?
        schema = doc.find('/dict/schema').first.content.to_s unless doc.find('/dict/schema').first.nil?
        lookup = doc.find('/dict/lookup').first.content.to_s unless doc.find('/dict/lookup').first.nil?
        reload = doc.find('/dict/reload').first.content.to_s unless doc.find('/dict/reload').first.nil?
        j = 0
        xslts = []
        doc.find("/dict/xslts/xslt").each{|el|
          j += 1
          xname = el.find('name').first.content
          xfile = el.find('file').first.content
          xslts << {'xname'=>xname, 'xfile'=>xfile, 'form_id'=>j.to_s}
        }
        k = 0
        packs = []
        doc.find("/dict/packages/package").each{|el|
          k += 1
          pname = el.find('name').first.content
          id = el.find('code').first.content
          packs << {'code'=>id, 'name'=>pname, 'form_id'=>k.to_s}
        }
        l = 0
        eqtags = []
        doc.find("/dict/eqtags/eqtag").each{|el|
          l += 1
          tagname = el.content.to_s
          eq_dics = el['dics'].to_s
          eq_elem = el['elem'].to_s
          eq_name = el['name'].to_s
          eqtags << {'eqtag'=>tagname, 'form_id'=>l.to_s, 'dics'=>eq_dics, 'elem'=>eq_elem, 'eqname'=>eq_name}
        }
        rows = j 
        rows = (rows < k)? k : rows
        rows = (rows < l)? l : rows

        #check if collection exist
        if @infodict.query('fn:doc("$collections")//collection[@name="'+code+'"]').size > 0
          dictlist << {'code'=>code, 'name'=>name, 'class'=>dclass, 'root'=>root, 'key'=>key, 'schema'=>schema, 'xslts'=>xslts, 'packages'=>packs, 'eqtags'=>eqtags, 'lookup'=>lookup, 'reload'=>reload, 'rows'=>rows+1, 'cssclass'=>cssclass, 'dictclasses'=>dictclasses}
        else
          dictlist << {'code'=>code, 'name'=>name, 'class'=>dclass, 'root'=>root, 'key'=>key, 'schema'=>schema, 'xslts'=>xslts, 'packages'=>packs, 'eqtags'=>eqtags, 'lookup'=>lookup, 'reload'=>reload, 'dontexist'=>1, 'rows'=>rows+1, 'cssclass'=>cssclass, 'dictclasses'=>dictclasses}
        end
      }
      dictlist.sort! { |x,y| x['code'] <=> y['code'] }
      tmpl_params = {'dicts'=>dictlist, 'dictclasses'=>dictclasses}
      tmpl_params['collcreated'] = 1 if request.query['collcreated'] != nil
      tmpl_params['deldict'] = request.query['delete'].to_s if request.query['delete'] != nil and request.query['delete'].to_s != ''
      response.body = HTMLTemplate.new(tmpl_params).output(@template_path + '/listdicts.tmpl')

    when 'editdict'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listdicts.tmpl')
        return response.body
      end
      
      response['Content-Type'] = 'text/html; charset=utf-8'
      if request.query['code'] != nil and request.query['code'] != ''
        code = request.query['code']
       
        newentry = false
        #new dict - check if code exists
        if request.query['new'] != nil and request.query['new'] == '1'
          newentry = true
          if @infodict.get('dict'+code).to_s != ''
            response.body = 'code exists'
            return
          end
        end
        
        xml = '<dict>'
        xml += '<code>'+code+'</code>'
        xml += '<name>'+request.query['name'].to_s+'</name>'
        xml += '<class>'+request.query['class'].to_s+'</class>'
        xml += '<root>'+request.query['root'].to_s+'</root>'
        xml += '<key>'+request.query['key'].to_s+'</key>'
        xml += '<schema><![CDATA['+request.query['schema'].to_s.force_encoding('UTF-8')+']]></schema>'
        xml += '<xslts>'
        for i in 0..request.query['rows'].to_i
          if request.query['xname_'+i.to_s] != nil and request.query['xname_'+i.to_s].to_s != ''
            xml += '<xslt><name>'+request.query['xname_'+i.to_s].to_s+'</name>'
            xml += '<file>'+request.query['xfile_'+i.to_s].to_s+'</file></xslt>'
          end
        end
        if newentry and request.query['class'].to_s == 'WordNet'
          xml += '<xslt><name>preview</name><file>wn-preview.xslt</file></xslt>'
          xml += '<xslt><name>vb</name><file>wnvb.xslt</file></xslt>'
          xml += '<xslt><name>single</name><file>wn-single.xslt</file></xslt>'
          xml += '<xslt><name>visdic</name><file>deb2vis.xslt</file></xslt>'
          xml += '<xslt><name>xml</name><file>xmlpretty.xsl</file></xslt>'
          xml += '<xslt><name>lmf</name><file>kyoto-deb2lmf.xslt</file></xslt>'
        end
        xml += '</xslts>'
        xml += '<packages>'
        for i in 0..request.query['rows'].to_i
          if request.query['pid_'+i.to_s] != nil and request.query['pid_'+i.to_s].to_s != ''
            xml += '<package><code>'+request.query['pid_'+i.to_s].to_s+'</code>'
            xml += '<name>'+request.query['pname_'+i.to_s].to_s+'</name></package>'
          end
        end
        xml += '</packages>'
        xml += '<eqtags>'
        for i in 0..request.query['rows'].to_i
          if request.query['eqt_'+i.to_s] != nil and request.query['eqt_'+i.to_s].to_s != ''
            xml += '<eqtag dics="' + request.query['eqt_dics_'+i.to_s].to_s + '" elem="' + request.query['eqt_elem_'+i.to_s].to_s + '" name="' + request.query['eqt_name_'+i.to_s].to_s + '">'+request.query['eqt_'+i.to_s].to_s+'</eqtag>'
          end
        end
        xml += '</eqtags>'
        xml += '<lookup>' + request.query['lookup_dics'].to_s.force_encoding('UTF-8') + '</lookup>'
        xml += '<reload>' + request.query['reload_dics'].to_s.force_encoding('UTF-8') + '</reload>'
        xml += '</dict>'

        $stderr.puts xml
        
        @infodict.update('dict'+code, xml)
        #remount
        #@server[:Authenticator].userdb.reload()
        #@server[:Authenticator].userdb.load_permissions()
      end
      
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listdicts#' + code)
      
    when 'dummydict'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listdicts.tmpl')
        return response.body
      end
      code = request.query['code'].to_s
      if code != ''
        response['Content-Type'] = 'text/plain; charset=utf-8'
     
        if @infodict.query('fn:doc("$collections")//collection[@name="'+code+'"]').size == 0
          @infodict.query("CREATE COLLECTION '#{code}'")

          response.body = 'collection '+code+' created'
          response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listdicts&collcreated=1')
        else
          response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listdicts')
        end
      else
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listdicts')
      end
    
    
    when 'deldict'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listdicts.tmpl')
        return response.body
      end
      code = request.query['code'].to_s
      if code != ''
        @infodict.delete('dict'+code)
        #remount
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listdicts')

    when 'remount'
      text = remount
      response['Content-Type'] = 'text/plain'
      response.body = text

    when 'listservs'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listservs.tmpl')
        return response.body
      end
      
      response['Content-Type'] = 'text/html; charset=utf-8'
      servlist = []
      cssclass = 1
      @infodict.xquery_to_array('/service').sort.each {|xml|
        cssclass ^= 1;
        doc = @infodict.load_xml_string(xml)
        code = doc.find('/service/code').first.content.to_s unless doc.find('/service/code').first.nil?
        name = doc.find('/service/name').first.content.to_s unless doc.find('/service/name').first.nil?
        htpasswd = doc.find('/service/htpasswd').first.content.to_s unless doc.find('/service/htpasswd').first.nil?
        dicts = []
        @infodict.list_dicts.sort.each{ |dcode,servname|
          dc = {'code'=>dcode, 'cssclass'=>cssclass}
          if doc.find("/service/dicts/dict[@code='#{dcode}']").first != nil
            dc['checked'] = 1
          end
          dicts << dc
        }

        servlist << {'code'=>code, 'name'=>name, 'htpasswd'=>htpasswd, 'dicts'=>dicts, 'cssclass'=>cssclass}
      }
      dicts = []
      @infodict.list_dicts.sort.each{ |dcode,servname|
        dc = {'code'=>dcode, 'cssclass'=>cssclass}
        dicts << dc
      }
      
      tmpl_params = {'services'=>servlist, 'dicts'=>dicts}
      tmpl_params['delserv'] = request.query['delete'].to_s if request.query['delete'] != nil and request.query['delete'].to_s != ''
      response.body = HTMLTemplate.new(tmpl_params).output(@template_path + '/listservs.tmpl')

    when 'editserv'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listusers.tmpl')
        return response.body
      end
      
      response['Content-Type'] = 'text/html; charset=utf-8'      
      if request.query['code'] != nil and request.query['code'] != ''
        code = request.query['code']
        
        #new user - check if login exists
        if request.query['new'] != nil and request.query['new'] == '1'
          if @infodict.get('service'+code).to_s != ''
            response.body = 'service exists'
            return
          end
        end
        
        xml = '<service>'
        xml += '<code>'+code+'</code>'
        xml += '<name>'+request.query['name'].to_s+'</name>'
        xml += '<htpasswd>'+request.query['htpasswd'].to_s.force_encoding("UTF-8")+'</htpasswd>'
        xml += '<dicts>'
        @infodict.list_dicts.each{ |dcode,name|
          dc = request.query['dict_'+dcode]
          if dc != nil and dc == 'on'
            xml += '<dict code="' + dcode + '"/>'
          end
        }
        xml += '</dicts>'
        xml += '</service>'

        @infodict.update('service'+code, xml)
        #@server[:Authenticator].userdb.reload()
        #@server[:Authenticator].userdb.load_permissions()
      end
      
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listservs')
      #response.body += xml
      #response.body += '<br/><a href="/admin?action=listservs">list</a>'

    when 'delserv'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listdicts.tmpl')
        return response.body
      end
      code = request.query['code'].to_s
      if code != ''
        @infodict.delete('service'+code)
        #remount
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listservs')

    when 'upload'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/upload.tmpl')
        return response.body
      end
      
      if data = request.query["data"]
        filename = data.filename
        save_uploaded_file(data, filename, request.query['dir'])
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=browse&dir='+request.query['dir'])
        
      end

      dirs = []
      @infodict.list_dicts.sort.each{ |dcode,name|
        #dirs << {'dir'=>'files/'+dcode}
        dirs << {'dir'=>'xpi/'+dcode}
      }
      @infodict.list_services.each{|scode,name|
        dirs << {'dir'=>'files/'+scode}
      }
      dirs.sort!{|x,y| x['dir']<=>y['dir']}

      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = HTMLTemplate.new('dirs'=>dirs).output(@template_path + '/upload.tmpl')
      
    when 'xpi'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/xpi.tmpl')
        return response.body
      end
      
      dirs = []
      @infodict.list_dicts.sort.each{ |dcode,name|
        dirs << {'dir'=>dcode}
      }

      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = HTMLTemplate.new('dirs'=>dirs).output(@template_path + '/xpi.tmpl')
      
    when 'xul'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/xul.tmpl')
        return response.body
      end
      
      dirs = []
      @infodict.list_dicts.sort.each{ |dcode,name|
        dirs << {'dir'=>dcode}
      }

      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = HTMLTemplate.new('dirs'=>dirs).output(@template_path + '/xul.tmpl')
      
    when 'browse'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/browse.tmpl')
        return response.body
      end
      
      allow_dir = allowed_dirs
      
      if request.query['dir']
        case request.query['dir']
          when 'xslt'
            dirname = 'xslt'
            dir = Dir.new(@xslt_path)
          when 'template'
            dirname = 'template'
            dir = Dir.new(@template_path)
          else
            if allow_dir.include?(request.query['dir'])
                dirname = request.query['dir']
                dir = Dir.new(@base_path + '/' + dirname) 
            end
        end

        response["Content-Type"] = "text/html"
        files = []
        if dir != nil
          dir.sort.each{ |file|
            if file[0,1] != '.'
              hash = {'file'=>file}
              if File.directory?(dir.path + '/' + file)
                files << {'isdir'=>1, 'file'=>dirname+'/'+file}
              else
                files << {'file'=>file}
              end
            end
          }
          response.body = HTMLTemplate.new({'dir'=>dirname, 'files'=>files}).output(@template_path + '/browse.tmpl')
        end
      end

    when 'showfile'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/browse.tmpl')
        return response.body
      end
      
      allow_dir = allowed_dirs
      dir = nil
      if request.query['dir'] and request.query['file']
        case request.query['dir']
          when 'xslt'
            dir = @xslt_path
          when 'template'
            dir = @template_path
          else
            if allow_dir.include?(request.query['dir'])
              dir = @base_path + '/' + request.query['dir']
            end
        end
        filename = request.query['file'].split('/').pop
        if dir != nil and filename[0,1] != '.' and File.exists?(dir + '/' + filename)
          response['Content-Disposition'] = "inline; filename=#{filename}"
          IO.foreach(dir + '/' + filename) { |line| response.body += line}
        end
      end
  
    when 'convert'
      if dictcode = request.query['dict'].to_s and dictcode != ''
        xml = @infodict.get('dict'+dictcode.to_s)
        doc = @infodict.load_xml_string(xml.to_s)
        schema = doc.find('/dict/schema').first.content.to_s
        $stderr.puts schema
        keypath = doc.find('/dict/key').first.content.to_s
        File.umask(0002)
        convert(dictcode, schema, keypath, @base_path + '/xpi')
        out = false
        FileUtils.cd("#{@base_path}/xpi/#{dictcode}") {
          out = system("make clean")
          out = system("make")
          response.body = out.to_s
        }
        response['Content-Type'] = 'text/html'
        if out
          response.body += "<a href='/admin?action=browse&dir=xpi/#{dictcode}'>browse xpi directory</a> <a href='/files/#{dictcode}.xpi'>install xpi</a><br><a href='/admin'>admin</a>"
        else
          response.body += "make: error<br>#{$?}"
        end
        remount
        
      else
        response['Content-Type'] = 'text/plain'
        response.body = 'no dictionary specified'
      end
      
    when 'convert_xul'
      if dictcode = request.query['dict'].to_s and dictcode != ''
        xml = @infodict.get('dict'+dictcode.to_s)
        doc = @infodict.load_xml_string(xml.to_s)
        schema = doc.find('/dict/schema').first.content.to_s
        $stderr.puts schema
        keypath = doc.find('/dict/key').first.content.to_s
        dictname = doc.find('/dict/name').first.content.to_s
        keyid = keypath.gsub(/[\/@]/, '_')
        js = @infodict.apply_transform('convertjs', schema, [ ['dictcode','"'+dictcode+'"'], ['index_path', '"'+keypath+'"'], ['index_id', '"'+keyid+'"'] ])
        xul = @infodict.apply_transform('convertxul', schema, [ ['dictcode','"'+dictcode+'"'], ['dictname','"'+dictname+'"'], ['index_path', '"'+keypath+'"'] ])
        jsfile = File.new(@base_path + '/files/emls/' + dictcode + '.js', 'w')
        jsfile.puts js
        jsfile.close
        xulfile = File.new(@xslt_path + dictcode + '-edit.xslt', 'w')
        xulfile.puts xul
        xulfile.close
        
        list = %Q[<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output encoding="utf-8" method="html" indent="yes"/>

  <xsl:template match="list">
  <html>
  <head><title>#{dictname} list</title>
  </head>
  <body>
    <h1>#{dictname} list</h1>
    <p>
      <a href="/#{dictcode}?action=edit&amp;id=">new entry</a>, <a href="/#{dictcode}?action=list_xml">show XML</a>, <a href="/#{dictcode}?action=make_pdf">generate PDF</a>, <a href="/files/#{dictcode}.pdf">download PDF</a>
    </p>
    <ul>
      <xsl:for-each select="#{keypath[1..-1]}">
        <xsl:sort select="#{keypath[1..-1]}"/>
      <li>
        <xsl:param name="x" select="."/>
        <a href="/#{dictcode}?action=xml&amp;id={.}" title="XML source"><img border="0" src="/files/xml.gif"/></a><xsl:text> </xsl:text>
        <a href="/#{dictcode}?action=preview&amp;id={.}" title="preview"><img border="0" src="/files/preview.gif"/></a><xsl:text> </xsl:text>
        <xsl:if test="//lock[@id=$x]">
          <img border="0" title="locked by {//lock[@id=$x]/@user}" src="/files/lock.gif"/><xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="not(//lock[@id=$x])">
          <a href="/#{dictcode}?action=edit&amp;id={.}" title="edit"><img border="0" src="/files/edit.gif"/></a><xsl:text> </xsl:text>
          <a href="/#{dictcode}?action=delete&amp;id={.}" title="delete"><img border="0" src="/files/delete.gif"/></a>
        </xsl:if>
        &#160;<xsl:value-of select="." />
      </li>
    </xsl:for-each>
    </ul>
  </body>
  </html>
  </xsl:template>
</xsl:stylesheet>
]
        listfile = File.new(@xslt_path + dictcode + '-list.xslt', 'w')
        listfile.puts list
        listfile.close

        keypath_part = keypath.split('/').values_at(2..-1).join('/')
        del = %Q[<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" indent="yes"/>

<xsl:template match="entry">
<html>
  <head>
    <title>#{dictname}, delete entry <xsl:value-of select="#{keypath_part}"/>?</title>
    <meta content='text/html; charset=utf-8' http-equiv='Content-Type'/>
    <link href='/files/emls.css' rel='stylesheet' type='text/css'/>
  </head>
  <body>
    <p><b>Really delete entry <i><xsl:value-of select="#{keypath_part}"/></i>?</b></p>
    <p><a href="/#{dictcode}?action=dodelete&amp;id={#{keypath_part}}">Yes, delete</a> | <a href="/#{dictcode}?action=list">No, cancel</a></p>
  </body>
</html>
</xsl:template>
</xsl:stylesheet>
]
        delfile = File.new(@xslt_path + dictcode + '-del.xslt', 'w')
        delfile.puts del
        delfile.close
        
        response['Content-Type'] = 'text/html'
        response.body = 'Service generated, <a href="https://didone.fi.muni.cz:8001/">show</a><br/>'
        response.body += "<a href='/admin'>admin</a>"
        #if out
        #  response.body += "<a href='/admin?action=browse&dir=xpi/#{dictcode}'>browse xpi directory</a> <a href='/files/#{dictcode}.xpi'>install xpi</a><br><a href='/admin'>admin</a>"
        #else
        #  response.body += "make: error<br>#{$?}"
        #end
        #remount
        
      else
        response['Content-Type'] = 'text/plain'
        response.body = 'no dictionary specified'
      end

    when 'make'
      if dictcode = request.query['dict'].to_s and dictcode != ''
        out = false
        FileUtils.cd("#{@base_path}/xpi/#{dictcode}") {
          File.umask(0002)
          out = system("make")
          response.body = out.to_s
        }
        response['Content-Type'] = 'text/html'
        if out
          response.body += "<a href='/admin?action=browse&dir=xpi/#{dictcode}'>browse xpi directory</a> <a href='/files/#{dictcode}.xpi'>install xpi</a><br><a href='/admin'>admin</a>"
        else
          response.body += "make: error<br>#{$?}"
        end
      else
        response['Content-Type'] = 'text/plain'
        response.body = 'no dictionary specified'
      end
      
    when 'dump'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/dumpdict.tmpl')
        return response.body
      end

      code = request.query['code'].to_s
      outfile = request.query['outfile'].to_s
      if code != ''
        outfile = @base_path + '/files/' + code + '.xml' if outfile == ''
        logfile = '/var/log/deb-server/' + code + Time.now.strftime('%Y%m%d-%H%M') +'.log'
        lmfinfo = nil
        lmfxsl = nil
        lmfroot = nil
        if request.query['lmf'].to_s == 'on'
          lmfxsl = @xslt_path + 'kyoto-deb2lmf.xslt'
          lmfroot = 'WN'
          begin
            lmfinfo = @infodict.get('lmfinfo'+code.to_s).to_s
          rescue
            lmfinfo = '<INFO/>'
          end
        end
        Thread.new { dump(code, outfile, logfile, false, false, lmfxsl, lmfinfo, lmfroot) }
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=showlog&type=dump&log=' + File.basename(logfile) + '&dump=' + File.basename(outfile))
        response.body
      else
        dicts = [{'dict'=>'admininfo'}]
        @infodict.list_dicts.sort.each{ |dcode,name|
          dicts << {'dict'=>dcode}
        }

        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'dicts'=>dicts, 'base_path'=>@base_path+'/files/'}).output(@template_path + '/dumpdict.tmpl')
        return response.body
      end
      
    when 'import'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/dumpdict.tmpl')
        return response.body
      end
      code = request.query['code'].to_s
      srcfile = request.query['srcfile'].to_s
      if srcfile != '' and code != ''
        xml = @infodict.get('dict'+code.to_s)
        doc = @infodict.load_xml_string(xml.to_s)
        root = doc.find('/dict/root').first.content.to_s unless doc.find('/dict/root').first.nil?
        key = doc.find('/dict/key').first.content.to_s unless doc.find('/dict/key').first.nil?
        logfile = '/var/log/deb-server/' + code + Time.now.strftime('%Y%m%d-%H%M') +'.log'
        delete = false
        delete = true if request.query['delete'].to_s == 'on'
        overwrite = false
        overwrite = true if request.query['overwrite'].to_s == 'on'
        if request.query['lmf'].to_s == 'on'
          srcfile = convert_from_lmf(srcfile)
          lmfinfo = ''
          src = File.open(srcfile)
          src.each{|line|
            if line.match('<INFO')
              infoxml = line.to_s
              @infodict.update('lmfinfo'+code.to_s, infoxml)
              break
            end
          }
        end
        $stderr.puts srcfile.to_s + ';' +  code.to_s + ';' +  logfile.to_s+ ';' +  delete.to_s + ';' +overwrite.to_s + ';' + key + ';' + root
        Thread.new{ import(code, srcfile, logfile, key, root, delete, overwrite) }
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=showlog&type=import&log=' + File.basename(logfile))
        response.body
      else
        params = {'base_path'=>@base_path+'/files/'}
        if code != ''
          params['code'] = code
        else
          dicts = []
          @infodict.list_dicts.sort.each{ |dcode,name|
            dicts << {'dict'=>dcode}
          }
          params['dicts'] = dicts
        end

        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new(params).output(@template_path + '/importdict.tmpl')
        return response.body
      end

    when 'showlog'
      logfile = request.query['log'].to_s
      dumpfile = request.query['dump'].to_s
      type = request.query['type'].to_s
      result = %x[tail -n 20 /var/log/deb-server/#{logfile}]
      lastline = ''
      result.to_s.split("\n").each{|line| lastline=line}

      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = '<html><head><meta http-equiv="Refresh" content="5"/></head><body><a href="/admin">admin</a><pre>'+result.to_s+'</pre>'
      if type == 'dump' and lastline[0,8] == 'DUMP END'
        response.body += '<p>Your dump should be ready as a file '+dumpfile
        response.body += ', <a href="/files/' + dumpfile.to_s + '">download</a>'
        response.body += '</p>'
      end
      if type == 'import' and lastline[0,8] == 'IMPORT END'
        response.body += '<p>Import should be complete, see log for more details.</p>'
      end
      response.body += '</body></html>'

    when 'listseq'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listseq.tmpl')
        return response.body
      end
      
      seq = []
      cssclass = 1
      @infodict.list_dicts.sort.each{ |dcode,name|
        cssclass ^= 1
        begin
          seqxml = @infodict.get('seq'+dcode)
          $stderr.puts seqxml
          seqdoc = @infodict.load_xml_string(seqxml.to_s)
          pattern = seqdoc.find('//pattern').first.content.to_s
          id = seqdoc.find('//id').first.content.to_s
        rescue =>e
          $stderr.puts e
          pattern = ''
          id = ''
        end
        seq << {'dict'=>dcode, 'pattern'=>pattern, 'id'=>id, 'cssclass'=>cssclass}
      }
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = HTMLTemplate.new({'sequences'=>seq}).output(@template_path + '/listseq.tmpl')
      return response.body

    when 'editseq'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listseq.tmpl')
        return response.body
      end
      if request.query['seq'] == 'save'
        @infodict.list_dicts.each{ |dcode,name|
          xml = '<seq code="'+dcode+'">'
          xml += '<id>'+request.query['id['+dcode+']']+'</id>'
          xml += '<pattern>'+request.query['pattern['+dcode+']']+'</pattern>'
          xml += '</seq>'
          $stderr.puts xml
          @infodict.update('seq'+dcode, xml)
        }
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listseq')

    when 'listlock'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listlock.tmpl')
        return response.body
      end
      
      lockdict = []
      cssclass = 1
      @infodict.list_dicts.sort.each{ |dcode,name|
        locks = @infodict.get_locks(dcode)
        locks.each{|lhash|
          cssclass ^= 1
          lhash['cssclass'] = cssclass
          lhash['time'] = Time.at(lhash['time'].to_i).strftime("%Y-%m-%d %H:%M:%S")
          lhash['dcode'] = dcode
        }
        lockdict << {'dict'=>name, 'dcode'=>dcode, 'cssclass'=>cssclass, 'locks'=>locks}
      }
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = HTMLTemplate.new({'lockdict'=>lockdict}).output(@template_path + '/listlock.tmpl')
      return response.body

    when 'dellock'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listlock.tmpl')
        return response.body
      end
      if request.query['dict'].to_s != '' and request.query['user'].to_s != '' and request.query['entry'].to_s != ''
        @infodict.unlock_entry(request.query['dict'].to_s, request.query['entry'].to_s, request.query['user'].to_s)
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listlock')
      
    when 'deldiclock'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listlock.tmpl')
        return response.body
      end
      if request.query['dict'].to_s != ''
        @infodict.unlock_dictionary(request.query['dict'].to_s)
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listlock')
      
    when 'deluserlock'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listlock.tmpl')
        return response.body
      end
      if request.query['user'].to_s != '' and request.query['dict'].to_s != ''
        @infodict.unlock_user_dic( request.query['dict'].to_s, request.query['user'].to_s)
      end
      if request.query['user'].to_s != '' and request.query['dict'].to_s == ''
        @infodict.unlock_user(request.query['user'].to_s)
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listlock')
      
    when 'delalllock'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listlock.tmpl')
        return response.body
      end
      @infodict.unlock_all()
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=listlock')

    when 'acclog'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/acclog.tmpl')
        return response.body
      end
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = HTMLTemplate.new().output(@template_path + '/acclog.tmpl')
      return response.body
      
    else
      testperm = checkadmin(user)
      tmpl_params = {}
      tmpl_params['admin'] = 1 if testperm
      response.body = HTMLTemplate.new(tmpl_params).output(@template_path + '/admin.tmpl')
      
    end
 
  end
  
  alias do_POST do_GET
end
