class HelloServlet < WEBrick::HTTPServlet::AbstractServlet
  def do_GET(request, response)
    response.header["Cache-Control"] = "no-cache"
    response['Content-type'] = "text/plain; charset=utf-8"
    response.body += "hello "+request.attributes[:auth_user].to_s
  end
  alias do_POST do_GET
end
