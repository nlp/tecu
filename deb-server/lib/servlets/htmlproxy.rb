require 'webrick'
require 'net/http'
require 'uri'
require 'net/https'
require 'openssl'

class HTMLProxyServlet < WEBrick::HTTPServlet::AbstractServlet 

#
# argumenty:
# 1. (povinny)   - url_template, obsahuje %s
# 2. (nepovinny) - kodovani dotazu, ktere se dava misto %s (impl. utf-8)
# 3. (nepovinny) - kod, ktery se provede na ziskanem HTML textu
  def initialize( server, url_template, enc='utf-8', transform_func=nil )
    @url_template = url_template
    @encoding = enc.gsub(/^force:/,'')
    @force_encoding = !(enc=~/^force:/).nil?
    #$stderr.puts "encoding=#{@encoding}, force=#{@force_encoding}"
    @transform = transform_func
    @last_host = nil
  end

  # sledujeme presmerovani
  def fetch(uri_str, limit = 10)
    # You should choose better exception.
    raise ArgumentError, 'HTTP redirect too deep' if limit == 0

    url = URI.parse(uri_str)
    @last_host = url.host.to_s if url.host != nil
    $stderr.puts url.path
    $stderr.puts url.query
    $stderr.puts url.host
    req = Net::HTTP::Get.new(url.path.to_s + '?'+url.query.to_s)
    req['User-Agent'] = 'DEBDict'
    http = Net::HTTP.new(@last_host, url.port)
    http.use_ssl = true if url.scheme == 'https'
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    response = http.start{|h|
      h.request(req)
    }

    case response
    when Net::HTTPSuccess     then response
    when Net::HTTPRedirection then fetch(response['location'], limit - 1)
    else
      response.error!
    end
  end

  def do_GET(request, response)
    $stderr.puts "action=#{request.query['action']}"
    key = request.query['id']
    $stderr.puts "key=#{key} (in utf-8)"
    key = key.encode(@encoding)
    $stderr.puts "key=#{key} (in #{@encoding})"
    lang = (request.query['lang'].to_s != '')? request.query['lang'] : 'en'
  
    url = sprintf @url_template,URI.escape(key)
    url.gsub!(/\$lang/, lang)
    $stderr.puts url
    text = fetch(url).body
    $stderr.puts "Stazeno\n"

    if @force_encoding
	charset=@encoding
    else
	charset=text[/CONTENT=['"].*?['"]/i]
	charset.gsub!(/.*=(.*)['"]/,'\1') if charset
	charset='iso-8859-2' unless charset
    end
    
    $stderr.puts "charset=#{charset}"
    response['Content-type'] = "text/html; charset=#{charset}"
    if @transform
	@transform.call(text)
	#debtext = text
	#debtext.gsub!("\n","\n    ")
	#$stderr.puts "post-transform: #{debtext}\n"
    end
    response.body = text
  end

  alias do_POST do_GET
end

