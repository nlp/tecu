require 'webrick'

class DictServlet < WEBrick::HTTPServlet::AbstractServlet

  def initialize( server, dict, array )
    @server = server
    @dict = dict
    @dict_array = array
  end

  def get_servername(server, request)
    if server[:SSLEnable]
      servername = 'https://'
    else
      servername = 'http://'
    end
    servername += request['Host'].to_s
  end

  def not_found( response, key, lang )
    response["Content-type"] = "text/html; charset=utf-8"
    response.body += %Q[<?xml version="1.0" encoding="utf-8"?>\n]
    response.body += "<html><body><br><br>"
    response.body += "<font color='red'>"
    if lang=='cs'
        response.body += "Slovo '<b>#{key}</b>' nenalezeno"
    else
        response.body += "Headword '<b>#{key}</b>' not found"
    end
    response.body += "</font>"
    response.body += "</body></html>"
  end

  def do_GET(request, response)
    $stderr.puts request.query['action']
    case request.query['action']
    #### Ukladani do databaze ####
    when 'store'
      data = request.query['data']
      key  = request.query['id']

      if (key == nil) or (key == '')
        key = "#{Time.now.to_i}-#{rand(1000)}"
        @dict.add( key, data )
      else
        @dict.update( key, data )
      end
    
      response["Content-type"] = "text/plain; charset=iso-8859-2"
      response.body = key
    
    ### pozadavek na kartu
    when 'getdoc'
      key       = URI.escape(request.query['id'])
      transform = request.query['tr']
      lang      = request.query['lang']
      $stderr.puts key
    
      begin
        res = @dict.get(key)
          $stderr.puts transform
        $stderr.puts res
        if transform
          response["Content-type"] = "text/html; charset=utf-8"
          res = @dict.apply_transform( transform, res.to_s ) if transform
        $stderr.puts res
        else
          response["Content-type"] = "text/xml; charset=utf-8"
        end
        response.body += %Q[<?xml version="1.0" encoding="utf-8"?>\n]
        response.body += res
        response.body
      rescue => e
        $stderr.puts e
        not_found( response, key, lang )
      end

    ### klic zacina na 
    when 'starts_with'
      key       = request.query['id']
      transform = request.path[1..-1]
      lang      = request.query['lang']
    
      begin
        res = @dict.starts_with(key)
        if res == ''
          not_found( response, key, lang )
        else
          if transform
            response["Content-type"] = "text/html; charset=utf-8"
            res = @dict.apply_transform( transform, res.to_s ) if transform
          else
            response["Content-type"] = "text/xml; charset=utf-8"
          end
          response.body += %Q[<?xml version="1.0" encoding="utf-8"?>\n]
          response.body += res
          response.body
        end
      rescue => e
        not_found( response, key, lang )
      end

    
    ### pozadavek na smazani karty
    when 'deldoc'
      key = request.query['id']
    
      $stderr.puts "deleting: #{key}"
      $stderr.puts @dict.delete(key)
    
      response["Content-type"] = "text/xml; charset=utf-8"
      response.body += %Q[<?xml version="1.0" encoding="utf-8"?>\n]
      response.body += '<deleted/>'
    when 'list_starts_with'
      key       = request.query['id']
      element = ''
      element      = request.query['element'] if request.query['element'] != nil
      if key != ''
        res = @dict.list_starts_with(key, element)
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body += res
        response.body
      end    

    when 'get_settings'
      $stderr.puts 'GET settings'
      set = @server.info.get_settings(request.attributes[:auth_user].to_s, @server.info.service_name)
      response["Content-type"] = "application/json; charset=utf-8"
  		response.body = set.to_json
    when 'save_settings'
      $stderr.puts 'save settings'
      data = URI.unescape(request.query['data']).to_s
      data_hash = JSON.parse(data)
      set = @server.info.save_settings(request.attributes[:auth_user].to_s, data_hash, @server.info.service_name)
      response["Content-type"] = "application/json; charset=utf-8"
  		response.body = data_hash.to_json


    ### neznamy pozadavek
    else
      response["Content-type"] = "text/plain"
      response.body = "chybny pozadavek"
    end
  end

  alias do_POST do_GET
end

