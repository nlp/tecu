require 'rexml/document'
include REXML
require 'fileutils'
require 'uuid'

def get_path(element, names = [])
  #if element.parent == nil
  #  names.shift
  #  return names
  #end
  if element.name == 'element'
    #puts element.attributes['name']
    names.unshift(element.attributes['name'])
  end
  if element.name == 'attribute'
    #puts element.attributes['name']
    names.unshift('@' + element.attributes['name'])
  end
  if element.parent != nil
    get_path(element.parent, names)
  end
  names
end

def is_multi(element)
  if element.parent.name == 'element' and (element.parent.parent.name == 'zeroOrMore' or element.parent.parent.name == 'oneOrMore')
    return true
  end
  return false
end

def is_ref(element)
  if element.parent.attributes['reference'] != nil
    return element.parent.attributes['reference'].to_s
  end
  return nil
end

def convert(dictcode, schema, keypath, dirpath, indexes)
  keypath.gsub!('/text()', '')
  if keypath[0,1] == '/'
    keypath = keypath[1..-1]
  end
  keypath.gsub!('/', '.')
  
  doc = REXML::Document.new(schema)
  roottag = doc.root.attributes['name'].to_s
  
  dtdstr = '<!ENTITY window.title "' + roottag + '">' + "\n"
  xulstr = ''
  js1str = 'var ' + roottag + ' = <' + roottag + "/>\n"
  js2str = 'var ' + roottag + ' = new XML(xmltext);' + "\n";
  
  XPath.each(doc, '//text') {|el|
    name = el.parent.attributes['name']
    if el.parent.name == 'attribute'
      name = '@' + name
    end
    path = get_path(el).join('.')
    dtdpath = path.gsub('@', '_')
    multi = is_multi(el)
    reference = is_ref(el)
    $stderr.puts name + "*" + path + "*" + multi.to_s + "*" + reference.to_s
  
    xulstr += '<row><label value="&' + dtdpath + ';"/>' + "\n"
    if multi
      if reference != nil
        xulstr += '<multilookup id="' + path + '" reference="'+reference+'"/>' + "\n"
        loadelement = 'multilookup_one'
      else
        xulstr += '<multitextbox id="' + path + '"/>' + "\n"
        loadelement = 'multitextbox_one'
      end
      js1str += <<EOF
  var ar = document.getElementById("#{path}").get_array('mtb');
  for (var i = 0; i < ar.length; i++) {
    #{path}[i] = ar[i].value;
  }
EOF
      js2str += <<EOF
  var ar = document.getElementById("#{path}").get_array('mtb');
  for (var i = 0; i < ar.length; i++) {
    document.getElementById("#{path}").del_multibox('box_multi', 'mtb', 'add_button', ar[i].getAttribute('anonid'), document.getElementById("#{path}"));
  }
  for each (el in #{path}) {
    document.getElementById("#{path}").add_multi('#{loadelement}', 'box_multi','mtb','add_button', document.getElementById("#{path}"), el);
  }
EOF
  
    else
      if reference
        xulstr += '<lookup reference="'+reference+'" id="' + path + '"/>' + "\n"
      else
        xulstr += '<textbox id="' + path + '"/>' + "\n"
      end
      js1str += path + ' = document.getElementById("' + path + '").value;' + "\n"
      js2str += 'document.getElementById("' + path + '").value = ' + path + ";\n"
    end
    xulstr += "</row>\n"
    dtdstr += '<!ENTITY ' + dtdpath + ' "' + name + '">' + "\n"
  }
  js1str += 'return ' + roottag + ";\n"
  
  xulfind = ''
  indexes.each{ |index|
    xulfind += <<EOF
    <hbox>
      <label value="&label.lookup; #{index}"/>
      <menulist id="search_text_#{index}" editable="true" onkeypress="if (event.keyCode == event.DOM_VK_RETURN) find(this,'#{index}');">
        <menupopup>
        </menupopup>
      </menulist>
      <button label="&button.find;" oncommand="find(document.getElementById('search_text_#{index}'), '#{index}')"/>
      <button label="&button.load;" oncommand="select_entry(document.getElementById('search_text_#{index}').value)"/>
    </hbox>
EOF
  }
  
  $stderr.puts xulstr
  $stderr.puts dtdstr
  $stderr.puts js1str
  
  #initialize if not existing
  dictdir = dirpath + '/' + dictcode
  $stderr.puts dictdir
  if not File.exists?(dictdir)
    Dir.mkdir(dictdir, 0775)
    File.new(dictdir).chmod(0775)
  end
  initfiles = ['template.js', 'template.dtd', 'template.xul', 'template.css', 'template.xbl', 'content.rdf.template', 'install.rdf.template', 'update.rdf.template', 'chrome.manifest.template', 'locale.rdf.template']
  initfiles.each{ |file|
    #if not File.exists?(dictdir + '/' + file)
      FileUtils.cp(dirpath + '/template/' + file, dictdir + '/' + file)
    #end
  }
  
  
  jsfile = File.new(dictdir + '/' + dictcode + '.js', 'w')
  IO.foreach(dictdir + '/template.js') {|line|
    if line.strip == '[[js1str]]'
      jsfile.puts js1str
    elsif line.strip == '[[js2str]]'
      jsfile.puts js2str
    elsif line =~ /\[\[keypath\]\]/
      jsfile.puts line.gsub('[[keypath]]', keypath)
    else
      jsfile.puts line
    end
  }
  jsfile.close
  
  IO.foreach(dictdir + '/template.dtd') {|line|
    dtdstr += line
  }
  dtdfile = File.new(dictdir + '/' + dictcode + '.dtd', 'w')
  dtdfile.puts dtdstr
  dtdfile.close
  
  xulfile = File.new(dictdir + '/' + dictcode + '.xul', 'w')
  IO.foreach(dictdir + '/template.xul') {|line|
    if line.strip == '[[xulstr]]'
      xulfile.puts xulstr
    elsif line =~ /\[\[xulfind\]\]/
      xulfile.puts line.gsub('[[xulfind]]', xulfind)
    elsif line =~ /\[\[dictcode\]\]/
      xulfile.puts line.gsub('[[dictcode]]', dictcode)
    else
      xulfile.puts line
    end
  }
  xulfile.close
  
  cssfile = File.new(dictdir + '/' + dictcode + '.css', 'w')
  IO.foreach(dictdir + '/template.css') {|line|
    if line =~ /\[\[dictcode\]\]/
      cssfile.puts line.gsub('[[dictcode]]', dictcode)
    else
      cssfile.puts line
    end
  }
  cssfile.close
  
  xblfile = File.new(dictdir + '/' + dictcode + '.xbl', 'w')
  IO.foreach(dictdir + '/template.xbl') {|line|
    if line =~ /\[\[dictcode\]\]/
      xblfile.puts line.gsub('[[dictcode]]', dictcode)
    else
      xblfile.puts line
    end
  }
  xblfile.close

  uuid = UUID.new
  
  if not File.exists?(dictdir + '/Makefile')
    makefile = File.new(dictdir + '/Makefile', 'w')
    makefile.puts <<EOF
FULLNAME=DEB Dictionary #{dictcode}
SHORTNAME=#{dictcode}
BASENAME=#{dictcode}
VERSION=$(shell cat version)
INSTALLDIR=#{@base_path}/files/
ID={#{uuid}}
SERVER=#{@servername}

all:
	make clean
	make install 

include ../Makefile.common
EOF
    makefile.close
  end

  globfile = File.new(dictdir + '/global.js', 'w')
  globfile.puts  "var server_address = '#{@servername}/#{dictcode}';"
  globfile.puts  "var server_name = '#{@servername}';"
  globfile.close
  
  if not File.exists?(dictdir + '/version')
    verfile = File.new(dictdir + '/version', 'w')
    verfile.puts '1.0.0'
    verfile.close
  end

end
