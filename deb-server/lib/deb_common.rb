class Object
  def edump(msg='')
    $stderr.puts("edump #{msg}: " + self.to_s)
    return self
  end
  def ldump
    $logger.debug{ 'ldump: ' + self.to_s } if $logger
    return self
  end
end

$debug_levels = {
  :debug => Logger::DEBUG,
  :info  => Logger::INFO,
  :warn  => Logger::WARN,
  :error => Logger::ERROR,
  :fatal => Logger::FATAL,
}

def mail_send(to, from, message, copy='')
  require 'net/smtp'

  Net::SMTP.start('localhost') do |smtp|
    smtp.send_message(message, from, to)
    smtp.send_message(message, from, copy) if copy != ''
  end
end

def get_class(class_name)
  c = class_name.split(/::|\./).inject(Object) do |cl, name|
    cl.const_get name
  end
  raise TypeError, "Not a class: #{class_name}" unless Class === c
  c
end

def server_shutdown(server, info)
  $stderr.puts "SERVER SHUTDOWN"
  server.shutdown
  #$stderr.puts info.container
  #info.container.close
  #$stderr.puts info.container
  #$stderr.puts env
  #$stderr.puts env.lock_stat
  #$stderr.puts "ENV CLOSE"
  #env.close
  #$stderr.puts "END"
end

def time_stamp
  t = Time.now
  return t.strftime("%Y-%m-%d %H:%M:%S.")+t.usec.to_s
end
