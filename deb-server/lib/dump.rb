require 'rubygems'
require 'sedna'
require 'zlib'
require 'xml/xslt'

#dump db to file
def dump(collection, out_file, logfile, rubyformat = false, gzip = false, convertxslt = nil, moreinfo = nil, roottag = nil)
  sedna = Sedna.connect({:database=>'deb'})

  roottag = collection if roottag.nil?
  
  if gzip
    f = open(out_file + '.gz', 'w')
    out = Zlib::GzipWriter.new(f)
  else
    out = File.new(out_file, 'w')
  end
  log = File.new(logfile, 'w')
  log.puts 'DUMP STARTED ' + Time.now.to_s

  if rubyformat
    out.puts %Q[# include libraries
require 'rubygems'
require 'sedna'

collection = "#{collection}"

sedna = Sedna.connect({:database=>'deb'})
if sedna.query('fn:doc("$collections")//collection[@name="'+collection+'"]').size == 0
  sedna.query('CREATE COLLECTION "'+collection+'"')
end
]
  end

  if not rubyformat
    out.puts '<?xml version="1.0" encoding="UTF-8"?>'
    out.puts "<#{roottag}>"
    out.puts moreinfo unless moreinfo.nil?
  end

  sedna.query('for $x in fn:collection("'+collection+'") return document-uri($x)').each{|id|
    doc = sedna.query('doc("'+id+'", "'+collection+'")').first.to_s.gsub('<?xml version="1.0" standalone="yes"?>','')
    if rubyformat
      out.puts "sedna.load_document('#{doc}', '#{id}', collection)"
    else
      out.puts doc
    end
    log.puts 'dumping ' + id
  }

  if not rubyformat
    out.puts "</#{roottag}>"
  end

  out.close

  if not rubyformat and not convertxslt.nil? and not gzip
    log.puts 'convert with ' + convertxslt
    xslt = XML::XSLT.new()
    xslt.xslfile = convertxslt
    xslt.xml = out_file
    s = xslt.serve()
    temp = Tempfile.new('lmfconv')
    temp.write(s)
    temp.flush
    temp.close
    File.rename(temp.path, out_file)
    File.chmod(0644, out_file)
  end

  log.puts 'DUMP END ' + Time.now.to_s
  log.close
end

