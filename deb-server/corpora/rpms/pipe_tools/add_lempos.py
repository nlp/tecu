#! /usr/bin/pypy
# coding: utf-8
import re


def process_def_file(def_file):
    pos_def_dict = {}

    for line in def_file:
        if not line.startswith('#'):
            key, value = line.strip().split('\t')
            key = re.compile(key)
            pos_def_dict[key] = value

    return pos_def_dict


def main():
    import argparse
    import sys

    parser = argparse.ArgumentParser(description='Process lempos definition file and add to vertical.')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-l', '--lempos_def_file', type=argparse.FileType('r'),
                        required=True, help='Lempos definition file')
    parser.add_argument('-r', '--rest', type=str, default='x',
                        required=True, help='Lempos character for undefined POS (taht anre not present '
                                            'in lempos definition file). Usually "x" is sused')
    args = parser.parse_args()

    pos_def_dict = process_def_file(args.lempos_def_file)
    structure = re.compile('^<.*?>$')

    for line in args.input:
        line = line.strip()
        spl = line.split('\t')

        if not structure.match(line) and len(spl) >= 3:
            try:
                word = spl[0]
                lemma = spl[1]
                tag = spl[2]
                rest = spl[3:]

                new_lemma = '{0}-{1}'.format(lemma, args.rest)

                for key, value in pos_def_dict.iteritems():
                    if key.match(tag):
                        new_lemma = '{0}-{1}'.format(lemma, value)

                if rest:
                    args.output.write('{0}\t{1}\t{2}\t{3}\n'.format(word, new_lemma, tag, '\t'.join(rest)))
                else:
                    args.output.write('{0}\t{1}\t{2}\n'.format(word, new_lemma, tag))
            except IndexError:
                sys.stderr.write("Can't process line: {0}\n".format(line))

        else:
            args.output.write('{0}\n'.format(line))

if __name__ == "__main__":
    main()

