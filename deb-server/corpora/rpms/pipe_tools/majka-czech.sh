#!/bin/bash
set -e 
set -o pipefail

THISDIR="/opt/majka_pipe"
CLEANUP="cleanup_utf8"
UNITOK="unitok_old -a -s -l czech -e utf-8"
VERTCHAIN="vertchain doc 104858"
TAGGER="$THISDIR/desamb.utf8.majka.sh"
GENDERDIR="$THISDIR/czech_gender_lemma"
ADD_GENDER_LEMMA="$GENDERDIR/add_gender_lemma.py $GENDERDIR/cztenten12_clean.gender_list"
ADD_LEPOS="$THISDIR/../pipe_tools/add_lempos.py -l $THISDIR/../pipe_tools/lemposdef/czech_lempos.def -r x"

$CLEANUP | $UNITOK | $VERTCHAIN "$TAGGER" | $ADD_GENDER_LEMMA | $ADD_LEPOS
