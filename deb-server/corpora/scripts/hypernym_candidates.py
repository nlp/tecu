#! /usr/bin/python
# coding: utf-8
import sys
from database import connect_database
from database import close_databse
from database import select_all_terms
from database import select_hypernym_and_kwic_freq
from database import select_term_freq
from math import log


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Generating hypernym candidates')
    parser.add_argument('-c', '--corpus', type=str,
                        required=True,
                        help='Corpus name')
    parser.add_argument('-r', '--ref_corpus', type=str,
                        required=True,
                        help='Reference name')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False, default=False,
                        help='Debugging mode')

    parser.add_argument('-s', '--sorted', type=str,
                        required=False, default='dice',
                        help='Sorting mode [dice, freq]')

    args = parser.parse_args()

    conn = connect_database(args.corpus)
    domain_terms = select_all_terms(conn)
    close_databse(conn)

    conn_ref = connect_database(args.ref_corpus)

    hyperonym_list = []

    for hyponym, hyponym_freq, hyponym_query, hyponym_context in domain_terms:

        kwic = select_hypernym_and_kwic_freq(conn_ref, hyponym)  # Frequency of hyponym and hypernym in context
        for hypernym, kwic_freq, h_query in kwic:

            if args.verbose:
                sys.stderr.write('hypo: {}, hyper: {}, freq: {}\n'.format(hyponym.encode('utf-8'),
                                                                          hypernym.encode('utf-8'),
                                                                          kwic_freq))

            ref_hyponym_freq = sum([x[0] for x in select_term_freq(conn_ref, hyponym)])
            ref_hypernym_freq = sum([x[0] for x in select_term_freq(conn_ref, hypernym)])

            logdice = 14 + log(2.0 * float(kwic_freq) / (ref_hyponym_freq + ref_hypernym_freq), 2)

            hyperonym_list.append((hyponym.encode('utf-8'), hypernym.encode('utf-8'), logdice, kwic_freq, h_query.encode('utf-8')))

            if args.verbose:
                sys.stderr.write('first_freq: {}, second_freq: {}, logdice: {}\n'.format(ref_hyponym_freq,
                                                                                         ref_hypernym_freq,
                                                                                         logdice))
                sys.stderr.write('=========================\n')

    close_databse(conn_ref)

    if args.sorted == 'freq':
        for hyp, hyper, dice, kwic_freq, h_query in sorted(hyperonym_list, key=lambda y: y[3], reverse=True):
            sys.stdout.write('{}|||{}|||{}|||{}|||{}\n'.format(hyp, hyper, dice, kwic_freq, h_query))
    elif args.sorted == 'dice':
        for hyp, hyper, dice, kwic_freq, h_query in sorted(hyperonym_list, key=lambda y: y[2], reverse=True):
            sys.stdout.write('{}|||{}|||{}|||{}|||{}\n'.format(hyp, hyper, dice, kwic_freq, h_query))


if __name__ == "__main__":
    main()
