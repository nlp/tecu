#! /usr/bin/python
# coding: utf-8
import os
import sys
dir_path = os.path.dirname(os.path.realpath(__file__))


def generate_geristry(language, corpus_name, path):
    """
    Create registry according language and its pipeline
    :param language: str
    :param corpus_name: str
    :param path: str
    :return: str
    """
    registry = []
    try:
        registry.append('NAME "{}"'.format(corpus_name))
        registry.append('PATH "/corpora/manatee/{}"'.format(corpus_name))
        registry.append('VERTICAL "| cat {}/{}.vert"'.format(path, corpus_name))
        with open('{}/templates/template.{}'.format(dir_path, language), 'r') as tpf:
            for line in tpf:
                registry.append(line.strip())

    except IOError:
        sys.stderr.write('Language "{}" is not supported! (template not found)'.format(language))
        sys.exit()

    return '\n'.join(registry)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Write vertical to correct file and create registry for it')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-l', '--language', type=str,
                        required=True,
                        help='Language')
    parser.add_argument('-n', '--corpus_name', type=str,
                        required=True,
                        help='Corpus name')
    parser.add_argument('-d', '--dir', type=str,
                        required=False, default='/corpora/vert/testing/',
                        help='Directory to write. Default: /corpora/vert/testing/')
    args = parser.parse_args()

    # Get unique corpus name
    corpus_name = args.corpus_name
    i = 0
    for _, _, files in os.walk(args.dir):
        while corpus_name in files:
            i += 1
            corpus_name = '{}_{}'.format(corpus_name, i)

    # Write .vert file to directory
    with open('{}/{}.vert'.format(args.dir, corpus_name), 'w') as f:
        for line in args.input:
            f.write(line)

    # Create registry for vertical
    registry = generate_geristry(args.language, corpus_name, args.dir)
    with open('/corpora/registry/{}'.format(corpus_name), 'w') as f:
        f.write(registry)


if __name__ == "__main__":
    main()
