#! /usr/bin/python
# coding: utf-8
import os
import re
import sys
import manatee
from database import create_database
from database import connect_database
from database import insert_term
from database import insert_colloc
from database import close_databse
from database import insert_hypernym
from freqs import compute_frequencies

debug = False
dir_path = os.path.dirname(os.path.realpath(__file__))
alphabet = re.compile('[a-zA-ZáčďéěíňóřšťúůýžAČĎÉĚÍŇÓŘŠŤÚŮÝŽ]')


hyp_templates = [
    {'query': '{}([lc=","|lc="a"|lc="nebo"|lc="či"] [k="k1"])* '
              '[lc="a"|lc="i"|lc="nebo"|lc="či"|lc="zejména"|lc="ani"] '
              '[lemma_lc="také"|lemma_lc="též"|lemma_lc="některý"|lemma_lc="nějaký"|lemma_lc="než"]? '
              '[lemma_lc="další"|lemma_lc="jiný"|lemma_lc="ostatní"|lemma_lc="podobný"] '
              '([k="k1"&c="c[1246]"] [k="k2"]{{0,2}})? {} within <s/>',
     'first_term_condition': '',
     'second_term_condition': 'c="c1"',
     'limit': 1,
     'id': 1},
    {'query': '{}([lc=","] [k="k1"])* ([lc="a"|lc="i"|lc="nebo"|lc="či"] '
              '[k="k1"])? [lemma_lc="být"&tag="k5eAaImIp3.*"&lc!="ne.*"] ([k="k1"&c="c[1246]"] '
              '[k="k2"]{{0,2}})? {} within <s/>',
     'first_term_condition': 'c="c1"',
     'second_term_condition': 'c="c[1246]"',
     'limit': 1,
     'id': 2}
]


def get_biggest_index(term_def):
    numbers = re.findall(r'(\d+):', term_def)
    return int(sorted(numbers, reverse=True)[0])


def add_condition(term_def, condition):
    if condition:
        return re.sub(r'(1:\[[^\]]*)\]', r'\1 & {}]'.format(condition), term_def)
    return term_def


def split_restrictions(term_def):
    split = re.match(r'^(.*?\])([^\]]*)$', term_def)
    return split.group(1), split.group(2)


def format_query(first_term, first_term_condition, second_term, second_term_condition, tempate_query,
                 first_term_context, second_term_context):
    query_context = ''
    first_term_c = add_condition(first_term, first_term_condition)
    first_term_c, firs_term_restrictions = split_restrictions(first_term_c)
    second_term_c = add_condition(second_term, second_term_condition)

    biggest_index = get_biggest_index(first_term_c)

    for i in range(6, 0, -1):
        second_term_c = re.sub('{}:'.format(i), '{}:'.format(i + biggest_index), second_term_c)
        second_term_c = re.sub(r'{}\.'.format(i), '{}.'.format(i + biggest_index), second_term_c)

    first_term_numbers = re.findall(r'(\d+):', first_term_c)
    second_term_numbers = re.findall(r'(\d+):', second_term_c)
    first_term_context_parts = re.findall(r'([a-z_]+) \d', first_term_context)
    second_term_context_parts = re.findall(r'([a-z_]+) \d', second_term_context)

    for number, part in zip(first_term_numbers, first_term_context_parts):
        query_context += '{} 0<{} '.format(part, number)
    for number, part in zip(second_term_numbers, second_term_context_parts):
        query_context += '{} 0<{} '.format(part, number)

    second_term_c += firs_term_restrictions

    # print 'first_term_c', first_term_c
    # print 'first_term_numbers', first_term_numbers
    # print 'first_term_context', first_term_context
    # print "first_term_context_parts", first_term_context_parts
    # print 'second_term_c', second_term_c
    # print 'second_term_numbers', second_term_numbers
    # print 'second_term_context', second_term_context
    # print "second_term_context_parts", second_term_context_parts
    # print 'query_context', query_context
    # print "========================="
    return tempate_query.format(first_term_c, second_term_c), query_context


def parts(context):
    attr = re.findall('[a-z_]+', context)
    return len(attr)


def compute_terms(corpus_name, term_cql_list, limit):
    """
    Get terms according query
    :param corpus_name: str
    :param term_cql_list: lsit
    :param limit: int
    :return:
    """
    for query, context in term_cql_list:
        terms = []
        for words, frequency, norm in compute_frequencies(corpus_name, query, context, limit):
                terms.append({'words': words, 'freq': frequency, 'norm': norm})
        yield {'terms': terms, 'query': query, 'context': context}


def get_size(corpus_name):
    """
    Size of corpus oin tokens
    :param corpus_name:
    :return:
    """
    corpus = manatee.Corpus(corpus_name)
    return float(corpus.size())


def load_termdef(language):
    """
    Load term definition file
    :param language: str
    :return: list of tuples (query, context)
    """
    term_cql_list = []
    with open('{}/termdefs/termdef.{}'.format(dir_path, language)) as term_f:
        for line in term_f:
            line = line.strip()
            if not re.match('^#.*?$', line) and line:
                try:
                    spl = line.split('->')
                    query = spl[0].strip()
                    context = spl[1].strip()
                    term_cql_list.append((query, context))
                except IndexError:
                    sys.stderr.write('Problem loading term definition file. Error on line:\n{}i\n'.format(line))

    return term_cql_list


def get_colloc_context(context):
    before = 'lemma_lc -1'
    after = 'lemma_lc {}'.format(parts(context))

    return '{} {} {}'.format(before, context, after)


def add_collocation(colloc, key, value, stopwords):
    if alphabet.match(value):
        try:
            stopwords[value]
        except KeyError:
            colloc[key].add(value)


def compute_colloc(corpus_name, term_cql_list, limit, stopwords, conn):
    """
    Get terms according query
    :param corpus_name: str
    :param term_cql_list: lsit
    :param limit: int
    :param stopwords:
    :param conn: connection to database
    :return:
    """
    for query, context in term_cql_list:
        context = get_colloc_context(context)
        terms_colloc = {}
        for words, _, _ in compute_frequencies(corpus_name, query, context, limit):
            words = words.strip().split('\t')
            try:
                terms_colloc[' '.join(words[1:-1])]
            except KeyError:
                terms_colloc[' '.join(words[1:-1])] = set()
            add_collocation(terms_colloc, ' '.join(words[1:-1]), words[0], stopwords)
            add_collocation(terms_colloc, ' '.join(words[1:-1]), words[-1], stopwords)

        for key, value in terms_colloc.iteritems():
            for item in value:
                insert_colloc(conn, key, item)


def load_stopwords(language):
    """
    Load term definition file
    :param language: str
    :return: list of tuples (query, context)
    """
    stopword_dict = {}
    with open('{}/stopwords/stopwords.{}'.format(dir_path, language)) as stopw_f:
        for line in stopw_f:
            line = line.strip()
            if not re.match('^#.*?$', line) and line:
                try:
                    stopw = line
                    stopword_dict[stopw] = None
                except IndexError:
                    sys.stderr.write('Problem loading stopword file. Error on line:\n{}i\n'.format(line))

    return stopword_dict


def main():
    global debug
    import argparse
    parser = argparse.ArgumentParser(description='Pre-compute terms from reference corpora')
    parser.add_argument('-c', '--corpus', type=str,
                        required=True,
                        help='Corpus')
    parser.add_argument('-l', '--language', type=str,
                        required=True,
                        help='Language abbreviation: eg. cs, en, ...')
    parser.add_argument('-i', '--limit', type=int,
                        required=False, default=0,
                        help='Discarding items with frequency less than LIMIT')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False, default=False,
                        help='Debug mode')
    parser.add_argument('--reference_corpora', action='store_true',
                        required=False, default=False,
                        help='If the corpus is reference, tan do not compute collocations for translation candidates.')

    args = parser.parse_args()
    debug = args.verbose

    # Database
    corp_size = get_size(args.corpus)

    create_database(args.corpus, corp_size, args.language, args.limit)
    conn = connect_database(args.corpus)
    term_cql_list = load_termdef(args.language)
    stopwords = load_stopwords(args.language)

    sys.stderr.write('Computing terms.\n')
    for terms_per_query in compute_terms(args.corpus, term_cql_list, args.limit):
        for i in terms_per_query['terms']:
            insert_term(conn,
                        ' '.join(i['words'].strip().split('\t')),
                        i['freq'],
                        terms_per_query['query'],
                        terms_per_query['context'])

    sys.stderr.write('Computing hypernyms.\n')
    for template in hyp_templates:
        for i in range(len(term_cql_list)):
            for j in range(len(term_cql_list)):

                first_term = term_cql_list[i][0]
                first_term_context = term_cql_list[i][1]
                first_term_parts = parts(first_term_context)

                second_term = term_cql_list[j][0]
                second_term_context = term_cql_list[j][1]

                hypernym_query, hypernym_query_context = format_query(first_term, template['first_term_condition'],
                                                                      second_term,
                                                                      template['second_term_condition'],
                                                                      template['query'], first_term_context,
                                                                      second_term_context)

                if args.verbose:
                    sys.stderr.write('freqs\t{}\t{}\n'.format(hypernym_query, hypernym_query_context))

                for kwic, kwic_freq, _ in compute_frequencies(args.corpus, hypernym_query, hypernym_query_context,
                                                              template['limit'], ):
                    if args.verbose:
                        sys.stdout.write('{}\t{}\n'.format(kwic, kwic_freq))

                    spl = kwic.split('\t')
                    hyponymum = ' '.join(spl[:first_term_parts])
                    hyperonym = ' '.join(spl[first_term_parts:])

                    if args.verbose:
                        sys.stdout.write('hypo: {}, hyper: {}, freq: {}\n'.format(hyponymum, hyperonym, kwic_freq))

                    insert_hypernym(conn, hyponymum, hyperonym, kwic_freq, hypernym_query, hypernym_query_context)

    if not args.reference_corpora:
        sys.stderr.write('Computing Collocs.\n')
        compute_colloc(args.corpus, term_cql_list, args.limit, stopwords, conn)

    # Closing connection
    close_databse(conn)


if __name__ == "__main__":
    main()
