#! /usr/bin/python
# coding: utf-8
import sys
import magic


def get_file_type(file_name):
    """
    Extracts mimetype from file
    :param file_name: str
    :return: str
    """
    file_type = magic.from_file(file_name, mime=True).strip().split('/')[1]

    if file_type == 'msword':
        return 'doc'

    if file_type == 'zip':
        return 'docx'

    return file_type


def main():
    import argparse
    parser = argparse.ArgumentParser(description='File type extractor')
    parser.add_argument('-i', '--input', type=str,
                        required=True,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')

    args = parser.parse_args()

    file_type = get_file_type(args.input)
    file_encoding = get_encoding(args.input)
    args.output.write('{}: {}'.format(file_type, file_encoding))


if __name__ == "__main__":
    main()
