#! /usr/bin/python
# coding: utf-8
import sys
import pexpect
import os
dir_path = os.path.dirname(os.path.realpath(__file__))


class FastText_NN:
    """Class for using the command-line interface to fasttext nn to lookup neighbours.
    It's rather fiddly and depends on exact text strings. But it is at least short and simple."""

    def __init__(self, model_path, nn_number):
        try:
            FASTTEXT_PATH = '{}/../rpms/fastText-0.2.0/fasttext'.format(dir_path)
            self.nn_process = pexpect.spawn('%s nn %s %d' % (FASTTEXT_PATH, model_path, nn_number))
            self.nn_process.expect('Query word?', timeout=90)  # Flush the first prompt out.
        except pexpect.exceptions.EOF:
            sys.stderr.write('Wrong model path')
            sys.exit()

    def get_nn(self, word):
        self.nn_process.sendline(word)
        self.nn_process.expect('Query word?')
        output = self.nn_process.before
        return [(word,float(score)) for word, score in [line.strip().split() for line in output.strip().split('\n')[1:]]]


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Get similar words according gensim.FastText')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-m', '--model', type=str,
                        required=True,
                        help='Fasttex model with pretrained word vectors')
    parser.add_argument('-w', '--word', type=str,
                        required=True,
                        help='Query word')
    parser.add_argument('-n', '--nn_number', type=int,
                        required=False, default=10,
                        help='Number of nearest neighbours')
    args = parser.parse_args()

    model = FastText_NN(args.model, args.nn_number)
    args.output.write('Similar words for "{}":\n'.format(args.word))
    for sim_w, score in model.get_nn(args.word):
        args.output.write('{}: {}\n'.format(sim_w, score))


if __name__ == "__main__":
    main()
