LANGUAGE "Czech"
ENCODING "UTF-8"

TAGSETDOC "https://www.sketchengine.co.uk/tagset-reference-for-czech"
WPOSLIST ",noun,k1.*,adjective,k2.*,pronoun,k3.*,numeral,k4.*,verb,k5.*,adverb,k6.*,preposition,k7.*,conjunction,k8.*"
LPOSLIST ",noun,-n,adjective,-j,pronoun,-d,numeral,-m,verb,-v,adverb,-a,preposition,-p,conjunction,-c"

ATTRIBUTE word {
    MAPTO lempos
    TYPE "FD_FGD"
}
ATTRIBUTE lempos {
    TYPE "FD_FGD"
}
ATTRIBUTE tag {
    TYPE "FD_FGD"
}
ATTRIBUTE   gender_lemma {
    LABEL   "gender lemma"
    TYPE "FD_FGD"
}

ATTRIBUTE   lc {
    LABEL   "word (lowercase)"
    DYNAMIC  utf8lowercase
    DYNLIB   internal
    ARG1     "C"
    FUNTYPE  s
    FROMATTR word
    DYNTYPE  index
    TRANSQUERY  yes
}
ATTRIBUTE   lempos_lc {
    LABEL   "lempos (lowercase)"
    DYNAMIC  utf8lowercase
    DYNLIB   internal
    ARG1     "C"
    FUNTYPE  s
    FROMATTR lempos
    DYNTYPE  index
    TRANSQUERY  yes
}
ATTRIBUTE   lemma {
    DYNAMIC striplastn
    DYNLIB  internal
    ARG1    "2"
    FUNTYPE i
    FROMATTR lempos
    DYNTYPE freq
}
ATTRIBUTE   lemma_lc {
    LABEL   "lemma (lowercase)"
    DYNAMIC  utf8lowercase
    DYNLIB   internal
    ARG1     "C"
    FUNTYPE  s
    FROMATTR lemma
    DYNTYPE  freq
    TRANSQUERY  yes
}

ATTRIBUTE   k {
     DYNAMIC getnextchar
     DYNLIB  internal
     ARG1    "k"
     FUNTYPE c
     FROMATTR tag
     DYNTYPE freq
}
ATTRIBUTE   g {
     DYNAMIC getnextchar
     DYNLIB  internal
     ARG1    "g"
     FUNTYPE c
     FROMATTR tag
     DYNTYPE freq
}
ATTRIBUTE   n {
     DYNAMIC getnextchar
     DYNLIB  internal
     ARG1    "n"
     FUNTYPE c
     FROMATTR tag
     DYNTYPE freq
}
ATTRIBUTE   c {
    DYNAMIC getnextchar
    DYNLIB  internal
    ARG1    "c"
    FUNTYPE c
    FROMATTR tag
    DYNTYPE freq
}
ATTRIBUTE   p {
    DYNAMIC getnextchar
    DYNLIB  internal
    ARG1    "p"
    FUNTYPE c
    FROMATTR tag
    DYNTYPE freq
}
ATTRIBUTE   m {
    DYNAMIC getnextchar
    DYNLIB  internal
    ARG1    "m"
    FUNTYPE c
    FROMATTR tag
    DYNTYPE freq
}

STRUCTURE s
STRUCTURE g {
    DISPLAYTAG 0
    DISPLAYBEGIN "_EMPTY_"
}
