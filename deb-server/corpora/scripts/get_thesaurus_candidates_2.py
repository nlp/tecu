#! /usr/bin/python
# coding: utf-8
import os
import sys
from database import connect_database
from database import select_term_freq
from database import select_corp_size
from fasttext_nn import FastText_NN
from database import close_databse
from freqs import compute_frequencies
dir_path = os.path.dirname(os.path.realpath(__file__))


def get_score(dom_corp_size, ref_corp_size, dom_query_hits, ref_query_hits, vec_sim, smoothing_parameter=1):
    """
    Simple math scoring: https://www.sketchengine.eu/documentation/simple-maths/
    :param dom_corp_size: domain corpus size
    :param ref_corp_size: reference corpus size
    :param dom_query_hits: number of hits in domain corpus
    :param ref_query_hits: number of hits in reference corpus
    :param smoothing_parameter: N from https://www.sketchengine.eu/documentation/simple-maths/
    :return: float
    """
    freq_dom_corpus = get_freq_per_million(dom_corp_size, dom_query_hits)
    freq_ref_corpus = get_freq_per_million(ref_corp_size, ref_query_hits)
    return (float(freq_dom_corpus + smoothing_parameter) / float(freq_ref_corpus + smoothing_parameter)) * vec_sim


def get_freq_per_million(size, query_hits):
    """
    Frequency per million
    :param size: corpus token size
    :param query_hits: int
    :return: float
    """
    return float(query_hits * 1000000) / size


def get_term_hits(conn, candidate):
    """
    Sum or select query hits form SQL Lite database
    :param conn: connection to database
    :param candidate: term
    :return: int, number of hits
    """
    query_hits = select_term_freq(conn, candidate)

    if not query_hits:
        query_hits = 0
    elif len(query_hits) > 1:
        sum_hits = 0
        for i in query_hits:
            sum_hits += i[0]
        query_hits = sum_hits
    else:
        query_hits = query_hits[0][0]

    return query_hits


def score_thesaurus_candidates(thesaurus_candidates, dom_conn, ref_conn, dom_corp_size, ref_corp_size):
    """
    Create score for thesaurus candidates according to simple math formula + similarity of vectors
    :param thesaurus_candidates:
    :param dom_conn:
    :param ref_conn:
    :param dom_corp_size:
    :param ref_corp_size:
    :return:
    """
    result = []
    for candidate, vec_sim in thesaurus_candidates:

        dom_query_hits = get_term_hits(dom_conn, candidate.decode('utf-8'))
        ref_query_hits = get_term_hits(ref_conn, candidate.decode('utf-8'))

        # Computing score
        result.append((candidate, get_score(dom_corp_size, ref_corp_size, dom_query_hits, ref_query_hits, vec_sim)))
    return result


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Query corpus for terms')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')

    parser.add_argument('-c', '--corpus', type=str,
                        required=True,
                        help='Corpus')
    parser.add_argument('-r', '--ref_corpus', type=str,
                        required=True,
                        help='Reference corpus')
    parser.add_argument('-v', '--verbouse', action='store_true',
                        required=False, default=False,
                        help='Debug mode')
    parser.add_argument('-m', '--model', type=str,
                        required=True,
                        help='FastText model')
    parser.add_argument('-n', '--number', type=int,
                        required=False, default=10,
                        help='Number of thesaurus suggestions')
    parser.add_argument('-t', '--original_tecu_terms', type=str,
                        required=False, default='{}/../data/tecu_v1.0_manual_terms_lemmas.cz'.format(dir_path),
                        help='Original tecu v1.0 terms for filtering lemmas')
    args = parser.parse_args()

    # Domain corpus database
    dom_conn = connect_database(args.corpus)

    # Reference corpus database
    ref_conn = connect_database(args.ref_corpus)

    # Corpora sizes
    dom_corp_size = select_corp_size(dom_conn)[0][0]
    ref_corp_size = select_corp_size(ref_conn)[0][0]

    allowed_lemmas = {}
    for lemma, _, _ in compute_frequencies(args.corpus, '[]', 'lemma 0', 1):
        allowed_lemmas[lemma] = None

    with open(args.original_tecu_terms, 'r') as v1t:
        for line in v1t:
            allowed_lemmas[line.strip()] = None

    # Term CQL queries

    fasttext_nn = FastText_NN(args.model, 100)

    for line in args.input:
        term, score = line.strip().split('\t')
        if len(term.split(' ')) == 1:
            thesaurus_candidates = fasttext_nn.get_nn(term)
            counter = 0
            for c, score in sorted(score_thesaurus_candidates(thesaurus_candidates, dom_conn,
                                                              ref_conn, dom_corp_size, ref_corp_size),
                                   key=lambda y: y[1], reverse=True):
                if c in allowed_lemmas and counter <= args.number:
                    counter += 1
                    sys.stdout.write('{}\t{}\t{:.4f}\n'.format(term, c, score))


    # Closing databases connection
    close_databse(dom_conn)
    close_databse(ref_conn)


if __name__ == "__main__":
    main()
