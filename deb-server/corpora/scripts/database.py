#! /usr/bin/python
# coding: utf-8
import sqlite3
import os
import sys
import manatee


def create_database(corpus, corp_size, language, limit):
    """
    Creates SQLite database. Two tables, one for corpus info that was used for creating, one for terms.
    :param db_name: str, name of database
    :param corp_size: int, corpus size in tokens
    :param corpus: str, corpus name
    :param language: str, language code
    :param limit: int, minimal frequency limit
    :return: None
    """
    db_name = '{}/terms_hypernyms_colls.db'.format(manatee.Corpus(corpus).get_conf('PATH'))

    if os.path.exists(db_name):
        sys.stderr.write('Removing old database.\n')
        os.remove(db_name)

    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    c.execute('CREATE TABLE terms (term TEXT, freq INTEGER, query TEXT, context TEXT)')
    c.execute('CREATE INDEX term_index ON terms (term)')
    c.execute('CREATE TABLE corp_info (corp_size INTEGER, name TEXT, language TEXT, freq_limit INTEGER)')
    c.execute('INSERT INTO corp_info VALUES (?,?,?,?)', (corp_size, corpus, language, limit))
    c.execute('CREATE TABLE hypernyms (hyponym TEXT, hypernym TEXT, freq INTEGER, query TEXT, context TEXT)')
    c.execute('CREATE INDEX h_index ON hypernyms (hyponym)')
    c.execute('CREATE TABLE colloc (word TEXT, colloctions TEXT)')
    c.execute('CREATE INDEX c_index ON colloc (word)')

    conn.commit()
    conn.close()


def insert_term(conn, words, freq, query, context):
    """
    Inserting terms into table
    :param conn: obj, connection to database
    :param words: str, term (can be more than one word)
    :param freq: int, frequency of term in corpus
    :param query: query that was used to find this term
    :param context: query context
    :return: None
    """
    c = conn.cursor()
    try:
        c.execute('INSERT INTO terms VALUES (?,?,?,?)', (words.decode('utf-8'), freq, query.decode('utf-8'),
                                                         context.decode('utf-8')))
    except sqlite3.IntegrityError:
        print 'Error same words'
        print 'Inserting: {}\t{}\t{}'.format(words, freq, query)
        print 'Found in database: {}'.format(select_all_terms(conn, words))
        sys.exit()


def insert_colloc(conn, key, value):
    """
    Insert word and its collocation
    :param conn:
    :param key:
    :param value:
    :return:
    """
    c = conn.cursor()
    c.execute('INSERT INTO colloc VALUES (?,?)', (key.decode('utf-8'), value.decode('utf-8')))


def insert_hypernym(conn, hyponym, hypernym, freq, query, context):
    """
    Inserting terms into table
    :param conn: obj, connection to database
    :param hyponym: str, hyponym
    :param hypernym: str, hypernym
    :param freq: int, frequency of term in corpus
    :param query: query that was used to find this term
    :param context: query context
    :return: None
    """
    c = conn.cursor()
    c.execute('INSERT INTO hypernyms VALUES (?,?,?,?,?)', (hyponym.decode('utf-8'), hypernym.decode('utf-8'),
                                                           freq, query.decode('utf-8'),
                                                           context.decode('utf-8')))


def connect_database(corpus):
    """
    Connection to database
    :param corpus: str, corpus name
    :return: obj, connection to SQLite database
    """
    db_name = '{}/terms_hypernyms_colls.db'.format(manatee.Corpus(corpus).get_conf('PATH'))
    conn = sqlite3.connect(db_name)
    return conn


def close_databse(conn):
    """
    Close database
    :param conn: obj, connection to SQLite database
    :return: None
    """
    conn.commit()
    conn.close()


def select_hypernym_and_kwic_freq(conn, words):
    """
    Selecting frequency of term
    :param conn: obj, connection to SQLite database
    :param words: str, term
    :return: list, result form database
    """
    c = conn.cursor()
    c.execute('SELECT hypernym, freq, query FROM hypernyms WHERE hyponym=?', (words,))
    return c.fetchall()


def select_term_freq(conn, words):
    """
    Selecting frequency of term
    :param conn: obj, connection to SQLite database
    :param words: str, term
    :return: list, result form database
    """
    c = conn.cursor()
    c.execute('SELECT freq FROM terms WHERE term=?', (words,))
    return c.fetchall()


def select_corp_size(conn):
    """
    Size of corpus from 'corp_info' table
    :param conn: obj, connection to SQLite database
    :return: ist, result form database
    """
    c = conn.cursor()
    c.execute('SELECT corp_size FROM corp_info')
    return c.fetchall()


def select_all_terms(conn):
    """
    Selecting line matched by term
    :param conn: obj, connection to SQLite database
    :return: list, result form database
    """
    c = conn.cursor()
    c.execute('SELECT * FROM terms')
    return c.fetchall()


def select_term_colls(conn, term):
    """
    Selecting line matched by term
    :param conn: obj, connection to SQLite database
    :return: list, result form database
    """
    c = conn.cursor()
    c.execute('SELECT colloctions FROM colloc WHERE word=?', (term,))
    return c.fetchall()


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Database for reference corpora terms')
    parser.add_argument('-n', '--name', type=str,
                        required=True,
                        help='database name')

    args = parser.parse_args()

    data = [('dĺžka', 45, 'q1'), ('problém', 100, 'q2'), ('mäkký', 500, 'q2')]

    create_database(args.name, 20,  '/corpora/registry/cstenten_17', 'cs', '1')

    conn = connect_database(args.name)
    for w, f, q in data:
        insert_term(conn, w, f, q, '')
    close_databse(conn)

    conn = connect_database(args.name)
    print select_term_freq(conn, 'dĺžka')[0][0]
    print select_corp_size(conn)[0][0]
    close_databse(conn)


if __name__ == "__main__":
    main()
