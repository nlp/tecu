#!/bin/bash

if [ -d "/corpora/registry/tecu" ] 
then
  /var/lib/deb-server/corpora/scripts/fileconvert.py -i $1 | /opt/majka_pipe/majka-czech.sh | /var/lib/deb-server/corpora/scripts/add_new_to_corpus.py -c /corpora/registry/tecu
  compilecorp --recompile-corpus /corpora/registry/tecu
else
  /var/lib/deb-server/corpora/scripts/fileconvert.py -i $1 | /opt/majka_pipe/majka-czech.sh | /var/lib/deb-server/corpora/scripts/data_writer.py -l 'cs' -n 'tecu' -d /corpora/vert
  compilecorp /corpora/registry/tecu
fi
/var/lib/deb-server/corpora/scripts/precompute_terms_hypernyms_colls.py -c /corpora/registry/tecu -l cs

/var/lib/deb-server/corpora/scripts/get_terms.py -c /corpora/registry/tecu -r /corpora/registry/cstenten_17 > /var/lib/deb-server/corpora/data/results/term_cands.txt
/var/lib/deb-server/corpora/scripts/hypernym_candidates.py -c /corpora/registry/tecu -r /corpora/registry/cstenten_17 > /var/lib/deb-server/corpora/data/results/hyper_cands.txt

