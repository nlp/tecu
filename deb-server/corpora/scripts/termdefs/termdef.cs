# Czech terminology CQL.
# Format: <query> -> display context
#== Term pattern 1: Adjective+ Noun ==
1:[k="k1"] -> lemma_lc 0
2:[k="k2"] 1:[k="k1"] & 1.g=2.g & 1.n=2.n & 1.c=2.c -> gender_lemma 0 lemma_lc 1
3:[k="k2"] 2:[k="k2"] 1:[k="k1"] & 1.g=2.g & 1.n=2.n & 1.c=2.c & 1.g=3.g & 1.n=3.n & 1.c=3.c -> gender_lemma 0 gender_lemma 1 lemma_lc 2
4:[k="k2"] 3:[k="k2"] 2:[k="k2"] 1:[k="k1"] & 1.g=2.g & 1.n=2.n & 1.c=2.c & 1.g=3.g & 1.n=3.n & 1.c=3.c & 1.g=4.g & 1.n=4.n & 1.c=4.c -> gender_lemma 0 gender_lemma 1 gender_lemma 2 lemma_lc 3

#== Term pattern 2: Noun (Noun in genitive)? (Adjective in genitive)* (Noun in genitive) ==
1:[k="k1"] 2:[k="k1" & c="c2"] -> gender_lemma 0 lc 1
1:[k="k1"] 2:[k="k1" & c="c2"] 3:[k="k1" & c="c2"] -> gender_lemma 0 lc 1 lc 2
1:[k="k1"] 2:[k="k2" & c="c2"] 3:[k="k1" & c="c2"] & 2.g=3.g & 2.n=3.n & 2.c=3.c -> gender_lemma 0 lc 1 lc 2
1:[k="k1"] 2:[k="k2" & c="c2"] 3:[k="k2" & c="c2"] 4:[k="k1" & c="c2"] & 2.g=3.g & 2.n=3.n & 2.c=3.c & 2.g=4.g & 2.n=4.n & 2.c=4.c -> gender_lemma 0 lc 1 lc 2 lc 3

#== Term pattern 2b: Adjective+ Noun (Noun in genitive)+ ==
2:[k="k2"] 1:[k="k1"] 3:[k="k1" & c="c2"] & 1.g=2.g & 1.n=2.n & 1.c=2.c -> gender_lemma 0 lemma 1 lc 2
2:[k="k2"] 3:[k="k2"] 1:[k="k1"] 4:[k="k1" & c="c2"] & 1.g=2.g & 1.n=2.n & 1.c=2.c & 1.g=3.g & 1.n=3.n & 1.c=3.c -> gender_lemma 0 gender_lemma 1 lemma 2 lc 3

#== Term pattern 2c: Adjective Noun (Adjective in genitive)+ (Noun in genitive) ==
2:[k="k2"] 1:[k="k1"] 3:[k="k2" & c="c2"] 4:[k="k1" & c="c2"] & 1.g=2.g & 1.n=2.n & 1.c=2.c & 3.g=4.g & 3.n=4.n & 3.c=4.c -> gender_lemma 0 lemma 1 lc 2 lc 3
2:[k="k2"] 1:[k="k1"] 3:[k="k2" & c="c2"] 4:[k="k2" & c="c2"] 5:[k="k1" & c="c2"] & 1.g=2.g & 1.n=2.n & 1.c=2.c & 3.g=4.g & 3.n=4.n & 3.c=4.c & 3.g=5.g & 3.n=5.n & 3.c=5.c -> gender_lemma 0 lemma 1 lc 2 lc 3 lc 4