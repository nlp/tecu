#! /usr/bin/python
# coding: utf-8
import sys
import os
import re


dir_path = os.path.dirname(os.path.realpath(__file__))


def load_tecu_translations(file_path):
    language_order = None
    translations = {}
    with open(file_path, 'r') as f:
        for idx, line in enumerate(f):
            line = line.strip().split('\t')
            if idx == 0:
                #Lang order: ['cz', 'en', 'de', 'fr', 'sk', 'ru']
                language_order = line
            else:
                data = line
                translations[data[0]] = {}
                for i in range(1, len(data)):
                    translations[data[0]][language_order[i]] = data[i].split('|')

    return translations


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Evaluate term translation')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-s', '--source_language', type=str,
                        required=False, default='cs',
                        help='Source language. Supported: "cs"')
    parser.add_argument('-t', '--target_language', type=str,
                        required=False, default='en',
                        help='Source language. Supported: "en"')
    args = parser.parse_args()

    translations = load_tecu_translations('{}/../../data/translations.csv'.format(dir_path))
    cand_re = re.compile('\((.+), (\d+)\)')

    args.output.write('Source term\tPosition\tFound translation score\tMax score for term\tResult\n')

    for line in args.input:
        spl = line.strip().split('\t')
        source_term = spl[0]
        transl_cantds = spl[1:]

        result = 'FAIL'
        max_score = 0
        found_score = 0
        position = -1
        first = True

        try:
            for idx, trc in enumerate(transl_cantds):
                cand = cand_re.match(trc).group(1)
                score = int(cand_re.match(trc).group(2))
                if first:
                    max_score = score
                    first = False

                if cand in translations[source_term][args.target_language]:
                    result = 'OK'
                    found_score = score
                    position = idx
                    break
        except KeyError:
            result = 'KeyError'

        args.output.write('{}\t{}\t{}\t{}\t{}\n'.format(source_term, position, found_score, max_score, result))


if __name__ == "__main__":
    main()
