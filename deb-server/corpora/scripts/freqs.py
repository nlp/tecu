#! /usr/bin/python
# coding: utf-8
import manatee
import os
import sys


def compute_frequencies(corpus_name, query, context, limit, subcorpus=None):
    if not os.getenv('MANATEE_REGISTRY'):
        os.environ['MANATEE_REGISTRY'] = '/corpora/registry'

    corp = manatee.Corpus(corpus_name)
    if subcorpus:
        corp = manatee.SubCorpus(corp, subcorpus)
    result = corp.eval_query(query)

    words = manatee.StrVector()
    freqs = manatee.NumVector()
    norms = manatee.NumVector()
    corp.freq_dist(result, context, limit, words, freqs, norms)
    for w, f, n in zip(words, freqs, norms):
        yield (w, f, n)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='print frequency distribution of QUERY according to CONTEXT'
                                                 'discarding items with frequency less than LIMIT'
                                                 'examples:freqs.py susanne \'[lemma="house"]\' \'word -1\''
                                                 '\tfreqs.py susanne \'[lemma="run"]\' \'word/i 0 tag 0 lemma 1\' 2'
                                                 '\tfreqs.py susanne \'[lemma="test"] []? [tag="NN.*"]\' \'word/i -1>0\' 0'
                                                 'freqs.py susanne \'[]\' \'word 0 doc.year 0\' 0 /path/to/file.subc')
    parser.add_argument('corpus', type=str,
                        help='Corpus name')
    parser.add_argument('-q', '--query', type=str,
                        required=True,
                        help='CQL query')
    parser.add_argument('-c', '--context', type=str,
                        required=False, default='word -1',
                        help='Context. Default CONTEXT is "word -1"')
    parser.add_argument('-l', '--limit', type=int,
                        required=False, default=0,
                        help='Limit. Default LIMIT is 0. Discarding items with frequency less than LIMIT')
    parser.add_argument('-s', '--subcorpus', type=str,
                        required=False, default='',
                        help='Subcorpus path.')

    args = parser.parse_args()

    for word, freq, _ in compute_frequencies(args.corpus, args.query, args.context, args.limit, args.subcorpus):
        sys.stdout.write('{}\t{}\n'.format(' '.join([x for x in word.split('\v')]), freq))



if __name__ == "__main__":
    main()
