#! /usr/bin/python
# coding: utf-8
import sys
import os
import re
from collx import collx
from database import connect_database
from database import select_all_terms
from database import select_term_colls
dir_path = os.path.dirname(os.path.realpath(__file__))


def load_dict(language):
    """
    Loading collocation translation dictionary
    :param language: str
    :return: dict
    """
    tr_calls_dict = {}
    with open('{}/colls_translations/tr_colls.{}'.format(dir_path, language)) as tr_calls:
        for line in tr_calls:
            spl = line.strip().split('\t')
            term = spl[0].decode('utf-8')
            colls = set([x.decode('utf-8') for x in spl[1:]])
            tr_calls_dict[term] = colls

    return tr_calls_dict


def get_intersection(colls, tr_colls):
    """
    Count number of intersected collocations
    :param colls: set
    :param tr_colls: set
    :return: int
    """
    return len(colls & tr_colls)


def form_query(term, context):
    term_parts = term.strip().split(' ')
    term_context_parts = context.strip().split(' ')
    query = ''

    for c, q in zip(term_context_parts, term_parts):
        query += '[{}="{}"]'.format(c, q)

    return query


def generate_query_context(term):
    query = ''
    term_parts = term.strip().split(' ')

    for t in term_parts:
        query += '[word="{0}"|lc="{0}"|lemma="{0}"|lemma_lc="{0}|gender_lemma="{0}"]'.format(t)

    return query


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Compute translation candidates according collocations.')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')

    parser.add_argument('-c', '--corpus', type=str,
                        required=True,
                        help='Corpus where colloation words will be gathered')
    parser.add_argument('-n', '--top_n', type=int,
                        required=False, default=10,
                        help='Top N best scored candidates. Default is 10')
    parser.add_argument('-t', '--translation', type=str,
                        required=True,
                        help='Translation to one of available languages: sk, en, de, fr, ru, sk')
    parser.add_argument('--coll_intersect_limit', type=int,
                        required=False, default=3,
                        help='Limit on collocation intersection length')
    parser.add_argument('--collx', action='store_true',
                        required=False, default=False,
                        help='Use collx function for computing term collocations')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False, default=False,
                        help='Debug mode')
    args = parser.parse_args()

    transl_dict = load_dict(args.translation)
    conn = connect_database(args.corpus)

    for line in args.input:
        term = ''
        query = ''
        spl = line.strip().split('\t')

        # Form query for collocation
        if len(spl) == 2:
            term = spl[0]
            context = spl[1]
            query = form_query(term, context)
        elif len(spl) == 1:
            term = spl[0]
            query = generate_query_context(term)

        result_tr = []
        output = []

        if args.collx:
            # Collx approach -> top 100 best scored (log-dice) collocation words for given term
            try:
                colls = set(collx(args.corpus, query))
            except RuntimeError:
                continue
        else:
            # All collocation words
            colls = set([y[0] for y in select_term_colls(conn, term.decode('utf-8'))])

        # Searching for candidates -> intersection between collocating words of given term
        for tr_term, tr_colls in transl_dict.iteritems():
            score = get_intersection(colls, tr_colls)
            result_tr.append((tr_term, score))

        output.append('{}'.format(term))
        for t, s in sorted(result_tr, key=lambda x: x[1], reverse=True)[:args.top_n]:
            if s > args.coll_intersect_limit:
                output.append('({}, {})'.format(t.encode('utf-8'), s))

        if len(output) > 1:
            sys.stdout.write('{}\n'.format('\t'.join(output)))


if __name__ == "__main__":
    main()
