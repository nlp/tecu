#! /usr/bin/python
# coding: utf-8
import sys
import manatee
import re
import os


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Add new document/s to existing corpus.')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-c', '--corpus', type=str,
                        required=True,
                        help='Corpus registry name')
    parser.add_argument('-v', '--debug', action='store_true',
                        required=False, default=False,
                        help='Print debugging information')
    args = parser.parse_args()

    # Path form registry file
    manatee_vertical = manatee.Corpus(args.corpus).get_conf('VERTICAL')
    # Remove "| cat" and similar things
    vert_path = re.match(r'^[^/]*(/[^\s]+)\s*$', manatee_vertical).group(1).strip()
    vert_dir = '/'.join(vert_path.split('/')[:-1])

    ##################################################
    # Creating new filename for old vertical for backup
    i = 0
    name_for_old_vert_backup = vert_path.split('/')[-1]
    for _, _, files in os.walk(vert_dir):
        while name_for_old_vert_backup in files:
            i += 1
            name_for_old_vert_backup = '{}_{}'.format(name_for_old_vert_backup, i)

    old_vert_path = '{}/{}'.format(vert_dir, name_for_old_vert_backup)
    os.rename(vert_path, old_vert_path)

    ##################################################
    # Debug info
    if args.debug:
        sys.stdout.write('vert_path: {}\n'.format(vert_path))
        sys.stdout.write('name_for_old_vert_backup: {}\n'.format(name_for_old_vert_backup))
        sys.stdout.write('vert_dir: {}\n'.format(vert_dir))
        sys.stdout.write('old_vert_path: {}\n'.format(old_vert_path))
        sys.stdout.write('moving {} -> {}\n'.format(vert_path, old_vert_path))

    ##################################################
    # Add new fiel to vertical
    with open(vert_path, 'w') as vert_file_handler:
        with open(old_vert_path, 'r') as f:
            vert_file_handler.write(f.read())
        for line in args.input:
            vert_file_handler.write(line)
    ##################################################


if __name__ == "__main__":
    main()
