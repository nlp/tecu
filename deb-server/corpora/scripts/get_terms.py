#! /usr/bin/python
# coding: utf-8
import os
import sys
import re
from database import connect_database
from database import select_term_freq
from database import select_corp_size
from database import select_all_terms
from database import close_databse
dir_path = os.path.dirname(os.path.realpath(__file__))


def get_score(dom_corp_size, ref_corp_size, dom_query_hits, ref_query_hits, smoothing_parameter=1):
    """
    Simple math scoring: https://www.sketchengine.eu/documentation/simple-maths/
    :param dom_corp_size: domain corpus size
    :param ref_corp_size: reference corpus size
    :param dom_query_hits: number of hits in domain corpus
    :param ref_query_hits: number of hits in reference corpus
    :param smoothing_parameter: N from https://www.sketchengine.eu/documentation/simple-maths/
    :return: float
    """
    freq_dom_corpus = get_freq_per_million(dom_corp_size, dom_query_hits)
    freq_ref_corpus = get_freq_per_million(ref_corp_size, ref_query_hits)
    return float(freq_dom_corpus + smoothing_parameter) / float(freq_ref_corpus + smoothing_parameter)


def get_freq_per_million(size, query_hits):
    """
    Frequency per million
    :param size: corpus token size
    :param query_hits: int
    :return: float
    """
    return float(query_hits * 1000000) / size


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Query corpus for terms')
    parser.add_argument('-c', '--corpus', type=str,
                        required=True,
                        help='Corpus')
    parser.add_argument('-r', '--ref_corpus', type=str,
                        required=True,
                        help='Reference corpus')
    parser.add_argument('-v', '--verbouse', action='store_true',
                        required=False, default=False,
                        help='Debug mode')
    parser.add_argument('--context', action='store_true',
                        required=False, default=False,
                        help='Output format: containing "term  context"')
    args = parser.parse_args()

    debug = args.verbouse

    # Domain corpus database
    dom_conn = connect_database(args.corpus)

    # Reference corpus database
    ref_conn = connect_database(args.ref_corpus)

    # Corpora sizes
    dom_corp_size = select_corp_size(dom_conn)[0][0]
    ref_corp_size = select_corp_size(ref_conn)[0][0]

    # Term CQL queries
    all_terms = []

    for term, freq, query, context in select_all_terms(dom_conn):
        if debug:
            sys.stderr.write('term: {}, freq: {}, query: {}, context: {}\n'.format(term.encode('utf-8'),
                                                                                   freq, query.encode('utf-8'),
                                                                                   context))

        dom_query_hits = freq
        ref_query_hits = select_term_freq(ref_conn, term)

        if not ref_query_hits:
            ref_query_hits = 0
        elif len(ref_query_hits) > 1:
            if debug:
                sys.stderr.write('More than one match {}'.format(ref_query_hits))

            sum_hits = 0
            for i in ref_query_hits:
                sum_hits += i[0]
            ref_query_hits = sum_hits
        else:
            ref_query_hits = ref_query_hits[0][0]

        # Computing score
        all_terms.append((term, get_score(dom_corp_size, ref_corp_size, dom_query_hits, ref_query_hits),
                          re.sub(' \d+', '', context)))

    # Closing databases connection
    close_databse(dom_conn)
    close_databse(ref_conn)

    # Printing terms sorted according score
    for t, score, context in sorted(all_terms, key=lambda x: x[1], reverse=True):
        if args.context:
            sys.stdout.write('{}\t{}\n'.format(t.encode('utf-8'), context))
        else:
            sys.stdout.write('{}\t{}\n'.format(t.encode('utf-8'), score))


if __name__ == "__main__":
    main()
