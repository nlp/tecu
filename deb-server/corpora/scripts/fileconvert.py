#! /usr/bin/python
# coding: utf-8
import re
import sys
import magic
import zipfile
from zipfile import ZipFile
import subprocess
from file_type import get_file_type


class FileConversionException(Exception):
    pass


def make_unix_eols(text):
    """
    Replaces all ends-of-lines with UNIX ends-of-lines (\\n).
    :param text:
    :return:
    """
    return re.sub('\r\n?', '\n', text)


def nice_whitespaces(text):
    """
    Consolidates white spaces in the text, normalizes ends-of-lines.
    :param text:
    :return:
    """
    text = make_unix_eols(text).strip()
    text = re.sub('[\t ]+', ' ', text)
    text = re.sub(' \n', '\n', text)
    return re.sub('\n{2,}', '\n\n', text)


def convert(command, fp):
    convert_timeout = ['timeout', '-k', '10', '30']  # send TERM/KILL to make sure the command ends

    process = subprocess.Popen(convert_timeout + command, stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    process_output, process_errors = process.communicate(fp.read())
    if process.returncode == 0:
        return process_output
    else:
        raise FileConversionException(process_errors)


def any2unicode(fp, filetype=None):
    """
    :param fp: file_descriptor
    :param filetype: str -> type of file
    :return: (str, str) -> text + encoding
    """

    if filetype == 'pdf':
        data = convert(['pdftotext', '-nopgbrk', '-', '-'], fp)
    elif filetype == 'doc':
        data = convert(['antiword', '-mUTF-8', '-'], fp)
    elif filetype == 'docx':
        data = convert(['docx2txt.pl', '-', '-'], fp)
    elif filetype == 'xml':
        data = fp.read()
        data = re.sub('^\s*<\?xml[^>]*>\s*', '', data)
        data = re.sub('<!--.*?-->', '', data, flags=re.DOTALL)
    else:  # txt
        data = fp.read()

    m = magic.Magic(mime_encoding=True)
    detected_encoding = m.from_buffer(data)

    if detected_encoding == 'utf_8':
        detected_encoding = 'utf_8_sig'
    udata = unicode(nice_whitespaces(data), detected_encoding, errors='ignore')
    return udata, detected_encoding


def process_file(my_file, file_name):
    """
    Fle to plaintext
    :param my_file: str, path to file
    :param file_name: str, file name
    :return: None
    """
    file_type = get_file_type(my_file)
    result = ''

    with open(my_file, 'rb') as f:
        udata, detected_encoding = any2unicode(f, filetype=file_type)

    # sys.stderr.write('Detected encoding: {}'.format(detected_encoding))

    result += '<doc name="{}">\n'.format(file_name)
    result += udata.encode('utf-8')
    result += '</doc>\n'
    return result


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Transform PDF, DOC and DOCX to TXT file')
    parser.add_argument('-i', '--input', type=str,
                        required=True,
                        help='Input file')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    args = parser.parse_args()

    if zipfile.is_zipfile(args.input):
        with ZipFile(args.input) as myzip:
            for file_name in myzip.infolist()[1:]:
                my_file = myzip.extract(file_name, '/tmp/')
                args.output.write(process_file(my_file, my_file.strip().split('/')[-1]))

    else:
        args.output.write(process_file(args.input, args.input.strip().split('/')[-1]))


if __name__ == "__main__":
    main()
