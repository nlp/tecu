#! /usr/bin/python
# coding: utf-8
import sys
import manatee


def collx(corp_name, query):
    """
    same as previous implementation: https://ske.fi.muni.cz/bonito/run.cgi/collx?corpname=preloaded/tecu_Czech&q=q[lemma=%22auto%22]&cattr=lc&cfromw=-3&ctow=3&cminfreq=2&cminbgr=2&cmaxitems=100&cbgrfns=d&csortfn=d&format=json
    :corp_name: corpus name
    :term_query: term query Example: [lemma="auto"]
    :return: list of collocated words
    """

    items = []
    cattr = 'lc'     # Attribute name
    csortfn = 'd'    # Code of the sorting function
    cminfreq = 2     # Minimum frequency
    cminbgr = 2      # Minimum bigram frequency
    cfromw = -3      # Beginning of context
    ctow = 3         # End of context
    cmaxitems = 100  # Maximum number of collocation items

    corp = manatee.Corpus(corp_name)
    manatee.setEncoding(corp.get_conf('ENCODING'))
    conc = manatee.Concordance(corp, query, 0, -1)
    conc.sync()
    colls = manatee.CollocItems(conc, cattr, csortfn, cminfreq, cminbgr, cfromw, ctow, cmaxitems)

    while not colls.eos():
        items.append({'str': colls.get_item(),
                      'freq': colls.get_cnt(),
                      'coll_freq': colls.get_freq(),
                      'logd': colls.get_bgr('d')
                      })
        colls.next()

    return [x['str'] for x in items]


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Collocation words wor given lemma')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')

    parser.add_argument('-c', '--corpus_name', type=str,
                        required=True,
                        help='Corpus name')

    args = parser.parse_args()

    for line in args.input:
        for col in collx(args.corpus_name, line.strip()):
            args.output.write('{}\n'.format(col))


if __name__ == "__main__":
    main()
