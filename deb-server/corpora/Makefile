#! /bin/bash

##########################################
# Setup and instalation
setup:
	# manatee
	sudo yum install libtool-ltdl-devel
	sudo yum install wget
	sudo yum install m4
	sudo yum install parallel
	sudo rpm -Uv --force ./rpms/manatee-open_2.163.6_rpms/manatee-open-2.163.6-4.el7.x86_64.rpm ./rpms/manatee-open_2.163.6_rpms/manatee-open-python-2.163.6-4.el7.x86_64.rpm	
	# creating paths 
	sudo mkdir -p /corpora
	sudo chmod 777 /corpora
	sudo mkdir -p /corpora/registry
	sudo mkdir -p /corpora/manatee
	sudo mkdir -p /corpora/vert
	sudo chmod 777 /corpora/registry
	sudo chmod 777 /corpora/manatee
	sudo chmod 777 /corpora/vert
	# ske repo
	sudo scp xmedved1@ske.fi.muni.cz:/etc/yum.repos.d/ske.repo ./
	sudo mv ske.repo /etc/yum.repos.d/
	# ske pipelines
	sudo yum install majka_pipe
	sudo mkdir -p /opt/pipe_tools/
	sudo cp ./rpms/pipe_tools/add_lempos.py /opt/pipe_tools/
	sudo cp -r ./rpms/pipe_tools/lemposdef /opt/pipe_tools/
	sudo cp ./rpms/pipe_tools/majka-czech.sh /opt/majka_pipe/
	# docx2txt
	sudo yum install docx2txt
	# rm ske repo
	sudo rm /etc/yum.repos.d/ske.repo
	# pdf2text
	sudo yum install poppler-utils
	# antiword for .doc files
	sudo wget https://forensics.cert.org/cert-forensics-tools-release-el7.rpm -P ./rpms
	sudo rpm -Uv --force ./rpms/cert-forensics-tools-release*rpm
	sudo yum --enablerepo=forensics install antiword
	# pip
	sudo yum install python-pip
	# python magic for file type recognition
	sudo pip install python-magic
	# facebook original fasttext
	sudo yum install python-devel
	sudo pip install numpy==1.14.6
	cd rpms/fastText_python_bind/ && sudo pip install .
	sudo unzip rpms/v0.2.0.zip -d rpms/
	cd rpms/fastText-0.2.0/ && sudo make
	# python dependecies
	sudo pip install pexpect

setup_corpora: 7zip tecu cstenten

7zip:
	sudo wget https://www.mirrorservice.org/sites/dl.fedoraproject.org/pub/epel/7/x86_64/Packages/p/p7zip-16.02-10.el7.x86_64.rpm -P ./rpms
	sudo wget https://www.mirrorservice.org/sites/dl.fedoraproject.org/pub/epel/7/x86_64/Packages/p/p7zip-plugins-16.02-10.el7.x86_64.rpm -P ./rpms
	sudo rpm -U --quiet rpms/p7zip-16.02-10.el7.x86_64.rpm
	sudo rpm -U --quiet rpms/p7zip-plugins-16.02-10.el7.x86_64.rpm
	sudo yum install -y https://yum.kaos.st/7/release/x86_64/kaos-repo-9.1-0.el7.noarch.rpm
	sudo yum install 7zcat


tecu:
	xzcat data/vert/tecu/tecu3.vert.xz | vert2plain -l 1000000 | /opt/majka_pipe/majka-czech.sh | ./scripts/data_writer.py -l 'cs' -n 'tecu' -d /corpora/vert/
	compilecorp /corpora/registry/tecu
	./scripts/precompute_terms_hypernyms_colls.py -c /corpora/registry/tecu -l cs

cstenten:
	sudo scp -r aurora.fi.muni.cz:/nlp/projekty/tecu/xmedved1/tecu/deb-server/corpora/data/fasttext_models/ ./data/
	sudo scp -r aurora.fi.muni.cz:/nlp/projekty/tecu/xmedved1/tecu/deb-server/corpora/data/vert/cstenten/cstenten17_mj2.vert.01.7z ./data/vert/cstenten/
	sudo mkdir -p /corpora/vert/tenten/
	sudo cp data/vert/cstenten/cstenten17_mj2.vert.01.7z /corpora/vert/tenten
	sudo cp data/vert/cstenten/cstenten_17 /corpora/registry/
	compilecorp /corpora/registry/cstenten_17
	./scripts/precompute_terms_hypernyms_colls.py -c /corpora/registry/cstenten_17 -l cs --reference_corpora

##########################################
# Creates databse of reference corpora terms
test_compute_terms_hyperonyms_collocations:
	./scripts/precompute_terms_hypernyms_colls.py -c /corpora/registry/cstenten_17 -l cs

##########################################
# Test import data and creates new corpora
test_import: test_import_clean
	./scripts/fileconvert.py -i ./data/for_testing/Korpus_funkce.pdf | /opt/majka_pipe/majka-czech.sh | ./scripts/data_writer.py -l 'cs' -n 'Korpus_funkce'
	compilecorp /corpora/registry/Korpus_funkce
	./scripts/precompute_terms_hypernyms_colls.py -c /corpora/registry/Korpus_funkce -l cs

test_import_clean:
	rm -rf /corpora/vert/testing/Korpus_funkce.vert
	rm -rf /corpora/registry/Korpus_funkce
	rm -rf /corpora/manatee/testing/Korpus_funkce

##########################################
# Test add new dokument to existing files
test_add:
	./scripts/fileconvert.py -i ./data/for_testing/Korpus_funkce.pdf | /opt/majka_pipe/majka-czech.sh | ./scripts/add_new_to_corpus.py -c /corpora/registry/Korpus_funkce
	compilecorp --recompile-corpus /corpora/registry/Korpus_funkce
	./scripts/precompute_terms_hypernyms_colls.py -c /corpora/registry/Korpus_funkce -l cs

##########################################
# Terms testing
test_terms:
	./scripts/get_terms.py -c /corpora/registry/tecu -r /corpora/registry/cstenten_17

##########################################
# Hyperonym candidates
test_hypernym:
	./scripts/hypernym_candidates.py -c /corpora/registry/tecu -r /corpora/registry/cstenten_17

##########################################
# Transation candidates
# uses collx function -> 100 best scored collocations according logdice 
test_translation:
	./scripts/get_terms.py -c /corpora/registry/tecu -r /corpora/registry/cstenten_17 --context | ./scripts/create_transaltion_candidates.py -c /corpora/registry/tecu -t en --collx

# Evaluate transaltion
eval: data/results/tras_cands_eval data/results/tras_cands_collx_eval
	echo "tras_cands_eval"; cat data/results/tras_cands_eval | cut -f5 | sort | uniq -c
	echo "tras_cands_collx_eval"; cat data/results/tras_cands_collx_eval | cut -f5 | sort | uniq -c

data/results/tras_cands_eval: data/results/tras_cands
	cat $< | ./scripts/utils/evaluate_transaltion.py > $@

data/results/tras_cands:
	./scripts/get_terms.py -c /corpora/registry/tecu -r /corpora/registry/cstenten_17 --context | ./scripts/create_transaltion_candidates.py -c /corpora/registry/tecu -t en --coll_intersect_limit 1 > $@

data/results/tras_cands_collx_eval: data/results/tras_cands_collx
	cat $< | ./scripts/utils/evaluate_transaltion.py > $@

data/results/tras_cands_collx:
	./scripts/get_terms.py -c /corpora/registry/tecu -r /corpora/registry/cstenten_17 --context | ./scripts/create_transaltion_candidates.py -c /corpora/registry/tecu -t en --collx --coll_intersect_limit 1 > $@

##########################################
# Test word nearest neighbours
dowload_model:
	wget https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.cs.300.bin.gz -P ./data/fasttext_models
	
test_nn:
	./scripts/fasttext_nn.py -m ./data/fasttext_models/cstenten17_lemma_kI_filtered.senteces.bin -w "člověk"

# Get 5 nearest neighbours to all terms
test_get_thezarus_to_corpus_terms:
	#./scripts/get_thesaurus_candidates.py -c /corpora/registry/tecu -r /corpora/registry/tecu -m ./data/fasttext_models/cstenten17_lemma_kI_filtered.senteces.bin -n 5
	./scripts/get_terms.py -c /corpora/registry/tecu -r /corpora/registry/cstenten_17 | ./scripts/get_thesaurus_candidates_2.py  -c /corpora/registry/tecu -r /corpora/registry/cstenten_17 -m ./data/fasttext_models/cstenten17_lemma_kI_filtered.senteces.bin
