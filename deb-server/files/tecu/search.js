var url_start = 'https://' + window.location.hostname + '/tezk';
var url_start = 'https://public21-188.cloud.ics.muni.cz:8040';

function get_results(query, status) {
    var result = new Array();
    $.ajax({
        async: false,
        url: url_start + '/tecu?action=search&search=' + encodeURIComponent(query) + '&status=' + status,
        success: function (data) {
            for (var i=0; i<data.length; i++) {
                result.push({label: data[i].head, value: data[i].head, id: data[i].id});
            }
        }
    });
    return result;
}


$(document).ready(function() {
        $('#search-field').keyup(function() {
            $(this).autocomplete({
                minLength: 3,
                source: function(request, response) {
                    var status = '';
                    if ($('#search-status').data('data-value')!=undefined) {
                      status = $('#search-status').data('data-value');
                    }
                    response(get_results(request.term, status));
                },
                select: function(event, ui) {
                    var status = '';
                    if ($('#search-status').data('data-value')!=undefined) {
                      status = $('#search-status').data('data-value');
                    }
                    //window.location.href = url_start + '/editor/tezaurus.html?opentree=true&id=' + ui.item.id;
                    window.location.href = 'tezaurus.html?opentree=true&id=' + ui.item.id + '&status=' + status;
                },
            })
        });

        $("#search-status li a").click(function() {
            var selText = $(this).text();
            $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
            $(this).parents('#search-status').data('data-value', $(this).parent().attr('data-value'));
            $('#search-field').autocomplete('search');
        });
});
