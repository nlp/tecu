#!/bin/bash

echo "======================================================================" >> update.log
date >> update.log
for f in `ls -rt todo/czechency.org_access_log*`; do
    make stats LOGFILE=$f >> update.log
    make fullstats LOGFILE=$f >> update.log
done

exit 0

