#!/usr/bin/perl -w

use strict;
use utf8;
use open ':std',':utf8';
use POSIX qw(strftime);
use URI::Escape;
use Encode;
use LWP::UserAgent;
use HTTP::Request::Common;

$::basedir='/var/lib/deb-server/files/tecu/stats';
my $start=201908;
my $index_only=(@ARGV and $ARGV[0] eq '-index');
my $chart_max=300; # max = 300 pixels;
my $limit_num_months=12;
my $show_entry_counts=0;
my $debug=0;
my $maphost='localhost';
my $mapuser='read';
my $mappass='r';
my $mappath='public/tecu?action=get_terms';
my $mapcache="$::basedir/mapids.txt";

my (undef,undef,undef,$mday,$mon,$year) = localtime(time);
$mon++; $year+=1900;
my $end=$year*100+($mon-1);
$end=($year-1)*100+12 if ($mon<=1);
$end=$start if ($end < $start);

my %stats=();
read_awstats(\%stats,$start,$end);
my %mapids=();
read_map(\%mapids, $mapcache, "http://$maphost/$mappath", $mapuser, $mappass);

if ($index_only) {
    print "<p><b>Heslo měsíce ";
    #show_top_entry($stats{'entries'}, \%mapids);
    show_top_entry($stats{'hopentries'}, \%mapids);
    print "</p>\n";
    exit (0);
}

print "<h3 id='stats'>Statistiky návštěvnosti</h3>\n";

print "<h4>Počet zobrazených stránek</h4>\n";

show_table_graph('u',$stats{'pages'}, $stats{'pagesavg'});

print "<h4>Počet návštěv</h4>\n";

show_table_graph('p',$stats{'visits'}, $stats{'visitsavg'});

#print "<h4>Skokani měsíce</h4>\n";
#
#show_list_entries($stats{'hopentries'}, \%mapids);

print "<h4>Nejnavštěvovanější hesla</h4>\n";

show_list_entries($stats{'entries'}, \%mapids);

print "<br/>" x 4;

exit (0);

sub show_top_entry {
    my ($table, $rmapids) = @_;
    for my $month (sort {$b<=>$a} keys(%$table)) {
	print nice_date($month,'top')."</b>: ";
	my $entries=$table->{$month};
	ENTRY:
	for my $e (sort {$entries->{$b}<=>$entries->{$a}} keys(%$entries)) {
	    my $text=$e;
	    next ENTRY unless (exists($rmapids->{$e}));
	    $text=$rmapids->{$e};
	    print "<a href='/kontakt.html#stats'>$text</a>";
	    last ENTRY;
	}
	last;
    }
}

sub show_list_entries {
    my ($table, $rmapids) = @_;
    print "<ul>\n";
    for my $month (sort {$b<=>$a} keys(%$table)) {
	my $entries=$table->{$month};
	next unless %$entries;
	print "<li>".nice_date($month).": ";
	my $top=3;
	my @entries=();
	my $text;
	ENTRY:
	for my $e (sort {$entries->{$b}<=>$entries->{$a}} keys(%$entries)) {
	    next ENTRY unless (exists($rmapids->{$e}));
	    $text="<a href='/tezaurus/term/$e' class='statsent' target='_top'>";
	    $text.=$rmapids->{$e};
	    $text.="</a>";
	    $text.=" (".$entries->{$e}.")" if $show_entry_counts;
	    push @entries,$text;
	    $top--;
	    last ENTRY unless ($top);
	}
	print join(', ', @entries)."</li>\n";
    }
    print "</ul>\n";
}

sub count_limit_months {
    my ($rtable,$rtableavg) = @_;
    return ($rtable) if (scalar(keys(%$rtable)) <= $limit_num_months);
    my %newtable = ();
    my $n=0;
    my @avg=();
    my @avgavg=();
    my $maxlimited=0;
    for my $key (sort {$b<=>$a} keys(%$rtable)) {
	$n++;
	if ($n <= $limit_num_months) {
	    $newtable{$key} = $rtable->{$key};
	}
	else {
	    push @avg,$rtable->{$key};
	    push @avgavg,$rtableavg->{$key} if $rtableavg;
	    $maxlimited = $key if $key > $maxlimited;
	}
    }
    my ($sumavg,$sumavgavg)=(0,0);
    for my $i (@avg) {
	$sumavg+=$i;
	print STDERR "$i, " if ($debug);
    }
    $newtable{-$maxlimited} = int($sumavg/scalar(@avg));
    print STDERR " = ".$newtable{-$maxlimited}."\n" if ($debug);
    if ($rtableavg) {
	for my $i (@avgavg) {
	    $sumavgavg+=$i;
	    print STDERR "$i, " if ($debug);
	};
	$rtableavg->{-$maxlimited} = int($sumavgavg/scalar(@avgavg));
	print STDERR " = ".$rtableavg->{-$maxlimited}."\n" if ($debug);
    }
    return (\%newtable,$sumavg);
}

sub show_table_graph {
    my ($color,$table, $tableavg) = @_;
    print "<table class='statsgen'>\n";
    print "<tr><td>";
    print "<table class='stats'>\n";
    print "<tr><td/><td class='statshead'>za&nbsp;měsíc</td><td class='statshead'>za&nbsp;den</td></tr>";
    my ($ltable,$sum) = count_limit_months($table,$tableavg);
    my $max = 0;
    for my $key (sort {$a<=>$b} keys(%$ltable)) {
	print "<tr>";
	print "<th align='left'>".nice_date($key)."</th>";
	print "<td align='right' class='statsimp'>".nice_num($ltable->{$key})."</td>";
	$sum+=$ltable->{$key};
	$max=$ltable->{$key} if ($ltable->{$key} > $max);
	print "<td align='right' class='statsimp'>".nice_num($tableavg->{$key})."</td>" if $tableavg;
	print "</tr>\n";
    }
    print "<tr class='statssum'><th align='left'>Celkem</th><th align='right' class='statsimp'>".nice_num($sum)."</th></tr>\n";
    print "</table>\n";
    print "</td><td width='10%'>&nbsp;</td><td>";
    print "<table class='statsgraph'>\n";
    my $scale=$max/$chart_max; # max $chart_max pixels;
    print "<tr valign='bottom'><td>&nbsp;</td>\n";
    for my $key (sort {$a<=>$b} keys(%$ltable)) {
	print "<td align='middle'><img align='bottom' src='v$color.png' height='"
	    .int($ltable->{$key}/$scale)."' width='14' alt='"
	    .nice_num(int($ltable->{$key}))."' title='"
	    .nice_num(int($ltable->{$key}))."' /></td>";
    }
    print "<td>&nbsp;</td></tr>\n";
    print "<tr valign='middle'><td>&nbsp;</td>\n";
    for my $key (sort {$a<=>$b} keys(%$ltable)) {
	my $date=nice_date($key,'axe');
	$date=~s# #<br/>#g;
	print "<td class='statsaxes'>$date</td>\n";
    }
    print "</tr>\n";
    print "</table>\n";
    print "</td></tr>";
    print "</table>\n";
}

sub nice_date {
    my ($date,$format) = @_;
    $format='default' unless $format;
    my $prefix='';
    my $gcase=1;
    if ($date<0) {
	$prefix='do ';
	$date=-$date;	
	$gcase=2;
    }
    if ($date >= 200000 and $date <= 300000) { #month
	@::months=('', 'leden', 'únor', 'březen', 'duben', 'květen',
	    'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad',
	    'prosinec'
	) unless @::months;
	@::months2=('', 'ledna', 'února', 'března', 'dubna', 'května',
	    'června', 'července', 'srpna', 'září', 'října', 'listopadu',
	    'prosince'
	) unless @::months2;
	my ($mon,$year) = (($date % 100), int($date / 100));
	return ($prefix.$::months2[$mon]) if ($format eq 'top');
	return ($prefix.sprintf('%02d<br/>%04d',$mon,$year)) if ($format eq 'axe');
	return ($prefix.$::months2[$mon].' '.$year) if ($gcase == 2);
	return ($prefix.$::months[$mon].' '.$year);
    }
}

sub nice_num {
    my ($number) = @_;
    my $snumber=sprintf('%03d',($number % 1000));
    $number=int($number/1000);
    while ($number>=1) {
	$snumber = sprintf('%03d',($number % 1000)).','.$snumber;
	$number=int($number/1000);
    }
    $snumber=~s/^0+//;
    return ($snumber);
}

sub read_awstats {
    my ($rstats,$start,$end) = @_;
    my $month=$start;
    my $prevmonth;

    $rstats->{'visits'}={};
    $rstats->{'visitsavg'}={};
    $rstats->{'pages'}={};
    $rstats->{'pagesavg'}={};
    while ($month <= $end) {
	my $file=get_awstats_file($month);
	if ($file) {
	    open(F,"<$file") or die "Cannot read $file: $!\n";
	    my $map=0;
	    my %map=();
	    while (<F>) {
		next unless $map or /^BEGIN_MAP\b/;
		$map=1;
		$map{$1}=$2 if (/^POS_(\w+)\s+(\d+)/);
		last if /^END_MAP\b/;
	    }
	    my %gen=read_awstats_section(\*F,$map{'GENERAL'});
	    $rstats->{'visits'}->{$month}=$gen{'TotalVisits'};
	    my ($visits,$pages)=(0,0);
	    my %days=read_awstats_section(\*F,$map{'DAY'});
	    for my $day (keys %days) {
		next unless ($days{$day}=~/^(\d+)\s+(\d+)\s+(\d+)\s+(\d+)/);
		$pages+=$1;
		$visits+=$4;
	    }
	    $rstats->{'visits'}->{$month}=$visits;
	    $rstats->{'pages'}->{$month}=$pages;
            if (scalar(keys(%days))>0) {
                $rstats->{'visitsavg'}->{$month}=int($visits/scalar(keys(%days)));
	        $rstats->{'pagesavg'}->{$month}=int($pages/scalar(keys(%days)));
            }
	    # for the top read max 20 lines, we need only 3 dict.entries
	    #my %queries=read_awstats_section(\*F,$map{'SIDER'},20);
	    # for the hopper read all
	    my %queries=read_awstats_section(\*F,$map{'SIDER'});
	    my %entries=();
	    for my $q (keys %queries) {
		next if ($q!~m#/tezaurus/term/.#);
		$q=~m#/tezaurus/term/(.*)#;
		my $ee=$1;
		$ee=~s/\\x([0-9a-f])/%$1/gi;
		my $e=uc(decode_utf8(uri_unescape($ee)));
		$e=~s#/+$##;
		$queries{$q}=~/^(\d+)/;
		next unless ($1);
		$entries{$e}=0 unless (exists($entries{$e}));
		$entries{$e}+=$1;
	    }
	    $rstats->{'entries'}->{$month}=\%entries;
	    if ($prevmonth) {
		$rstats->{'hopentries'}->{$month}={};
		my $pv;
		for my $e (keys(%entries)) {
		    $pv=0;
		    $pv=$rstats->{'entries'}->{$prevmonth}->{$e}
			if (exists($rstats->{'entries'}->{$prevmonth}->{$e}));
		    $rstats->{'hopentries'}->{$month}->{$e} = $entries{$e}-$pv;
		}
	    }
	    else {
		$rstats->{'hopentries'}->{$month}=\%entries;
	    }
	    close(F);
	}
	else {
	    warn "No file for month $month!\n";
	}
	$prevmonth=$month;
	$month++;
	$month=(int($month / 100)+1)*100+1 if (($month % 100)>12);
    }
}

sub read_awstats_section {
    my ($fh,$pos,$stopafter) = @_;
    my %section=();

    return (undef) unless seek($fh,$pos,0);
    $stopafter=-1 if (not $stopafter or $stopafter<=0);
    while (<$fh>) {
	chomp;
	next if (/^BEGIN_/);
	last if (/^END_/);
	$section{$1}=$2 if /^(\S+)\s+(.*)/;
	$stopafter--;
	last if ($stopafter==0);
    }
    return (%section);
}

sub get_awstats_file {
    my ($month) = @_;
    my $path=$::basedir.
	sprintf('/awstats%02d%04d.tecu.txt',
		($month % 100), int($month / 100));
    return ('') unless (-r $path);
    return ($path);
}

sub read_map {
    my ($rmap,$cache,$url,$user,$pass) = @_;
    
    my $ua = LWP::UserAgent->new;
    $ua->timeout(10);
    $ua->ssl_opts( verify_hostname => 0);
    my $request = GET $url;
    #$request->authorization_basic($user, $pass);
    my $response = $ua->request($request);
    my $str='';
    my $savecache=0;
    if ($response->is_success) {
	$str=$response->decoded_content();
	open(F,">$cache") and do {
	    print F $str;
	    close(F);
	};
    }
    elsif (-r $cache) {
	open(F,"<$cache") and do {
	    $str=join('',<F>);
	    close(F);
	};
    }
    for my $idt (split(/\n/, $str)) {
	my ($id,$text) = ($idt=~/^(\d+);(.*)$/);
	next unless $text;
	$rmap->{$id} = $text;
    }
}

