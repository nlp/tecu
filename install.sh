#!/bin/bash
yum config-manager --set-enabled PowerTools #for CentOS 8
yum install epel-release git ruby ruby-devel libxml2-devel libxslt-devel cpp gcc make gcc-c++ psmisc awstats texlive-latex-bin dvipng texlive-cm texlive-collection-fontsrecommended libsqlite3x-devel perl-LWP-Protocol-https redhat-rpm-config samba-client
./installbin/sedna-3.5.161-bin-linux-x64.sh #select 2) /opt; /usr

gem install libxml-ruby
gem install ruby-xslt 
gem install json
gem install sedna
gem install nokogiri -v 1.6.7 -- --use-system-libraries
gem install panmind-rtf
gem install pdfkit
gem install calculus
gem install iconv
gem install json-diff
gem install twitter_cldr
gem install wkhtmltopdf-binary
ln -s /usr/local/bin/wkhtmltopdf /bin/wkhtmltopdf

cp -r deb-server /var/lib
mkdir /var/log/deb-server
mkdir /etc/deb-server

groupadd deb
adduser --system  --shell /bin/bash -g deb debsrv
usermod -a -G deb debsrv
chown -R debsrv:deb /var/lib/deb-server /var/log/deb-server
chmod -R ug+rw /var/lib/deb-server /var/log/deb-server
chmod -R g+s /var/lib/deb-server /var/log/deb-server
cp /var/lib/deb-server/config-tecu/init.d/debserver-common /etc/init.d/debserver-common
cp /var/lib/deb-server/config-tecu/cron.d/debserver-common /etc/cron.d/debserver-common
cp /var/lib/deb-server/config-tecu/logrotate.d/debserver-common /etc/logrotate.d/debserver-common
cp /var/lib/deb-server/config-tecu/default/debserver-common /etc/default/debserver-common
cp /var/lib/deb-server/config-tecu/services.conf /etc/deb-server/services.conf
cp /var/lib/deb-server/config-tecu/backup.conf /etc/deb-server/backup.conf
cp /var/lib/deb-server/config-tecu/sednaconf.xml /opt/sedna/etc/sednaconf.xml
/var/lib/deb-server/bin/tecuinit.sh

#certifikat
yum install certbot       

#korpus
cd /var/lib/deb-server/corpora
make setup

#builder
yum install nodejs git.x86_64
gem install sass
adduser -g deb builder
cd /var/lib/deb-server/client
git clone https://gitlab.fi.muni.cz/xmach5/tezaurus.git
cd tezaurus
npm install
/var/lib/deb-server/bin/tecu_clientbuild.sh

/etc/init.d/debserver-common start
