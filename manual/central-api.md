# Centrální systém API

## Zjištění seznamu datových systémů
GET http://tezaury.gov.cz/registr?action=list_thesaurus

odpověď: JSON, pole registrovaných tezaurů

Každý záznam obsahuje položky:
* tid: ID tezauru
* confirmed: true/false, je potvrzeno správcem?
* name: plný název tezauru
* url: adresa veřejné části tezauru
* organization: provozující organizace
* description: popis tezauru
* contact_person: kontaktní osoba
* contact_email: kontaktní email
* contact_phone: kontaktní telefon
* time_register: čas registrace
* time_update: čas poslední aktualizace
* terms_count: počet termínů

Příklad:
```json
  {
    "tid": "cuzk",
    "confirmed": "true",
    "name": "Terminologický slovník zeměměřictví a katastru nemovitostí ČR",
    "url": "http://tezaurus-cuzk.ogibeta2.gov.cz/",
    "organization": "ČÚZK",
    "description": "",
    "contact_person": "",
    "contact_email": "",
    "contact_phone": "",
    "time_register": "2020-05-21 17:42",
    "time_update": "2020-11-20 09:40",
    "terms_count": "4727"
  }
```

## Přidání nového nebo aktualizace datového systému

POST http://tezaury.gov.cz/registr

odeslaná data ve formátu JSON:
```json
{
    "action": "update_thesaurus",
    "data": {
        "tid": "IDtezauru",
        "name": "název tezauru",
        "short_name": "zkrácený název",
        "url": "veřejné URL",
        "contact_person": "kontaktní osoba",
        "contact_email": "kontaktní email", 
        "contact_phone": "kontaktní telefon",
        "descriptioncs": "popis česky",
        "descriptionen": "popis anglicky",
        "infocs": "základní informace česky",
        "infocs": "základní informace anglicky",
        "komise": "komise",
        "organization": "organizace",
        "poverena_organizace": "pověřená organizace",
    }
}
```

odpověď: potvrzení uložených dat ve formátu JSON záznamu tezauru

## Vyhledání termínu v uložených tezaurech
GET http://tezaury.gov.cz/registr?action=search_term&search=termín

příklad: http://tezaury.gov.cz/registr?action=search_term&search=mapa

odpověď: data ve formátu JSON, pole výsledků. Každý výsledek obsahuje položky:
* thes_id: ID tezauru
* url: veřejné URL tezauru
* term_id: ID termínu v tezauru
* term: název termínu
* definition: definice termínu

příklad:
```json
[
{
"thes_id": "cuzk",
"url": "http://tezaurus-cuzk.ogibeta2.gov.cz/",
"term_id": "5814",
"term": "orientační mapa parcel",
"definition": "tvoří ji zpravidla rastrové obrazy katastrální mapy a map dřívějších pozemkových evidencí přibližně transformované do S-JTSK, doplněné definičními body parcel, budov a vodních děl . Orientační mapa v S-JTSK je do obnovy rastrového obrazu katastrální mapy doplňována informativním zobrazením změn v katastrální mapě"
}
]
```

### Stahování dat z datového systému
Centrální systém pravidelně stahuje obsah tezaurů z datových systémů (pro vyhledání termínů a jako záloha).
Datový systém musí poskytnout data ve formátu XML na URL: GET url_tezauru/tecu?action=download_all

Např. http://tezaurus-cuzk.ogibeta2.gov.cz/tecu?action=download_all
